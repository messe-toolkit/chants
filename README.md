## A lire avant de modifier des paroles !

### Structure à suivre :

#### Ajouter un `refrain`

```
R.
Ceci est un refrain
Il peut être sur plusieurs lignes
```

#### Ajouter un `couplet`

```
1.
Ceci est un couplet
Il peut être sur plusieurs lignes aussi

2.
Il a le numéro de couplet suivi d'un point et
d'un saut de ligne
```

#### Ajouter une `intro`

```
I.
L'intro commence par "I."
```

#### Ajouter un `pont/bridge/coda`

```
P.
Un pont bridge ou coda commence par un "P."
```

#### Ajouter une `antienne`

```
A.
Une antienne commence par un "A."
```

#### Ajouter une `stance`

```
S.
La stance commence par "S."
```

### Pour que l'application reconnaisse qu'une strophe est un élement (couplet, refrain, etc...) il faut qu'il soit entouré lignes vides

C'est exemple n'est **pas OK** :
```
1.
Ceci est le couplet 1
2.
Ceci est le couplet 2
```

C'est exemple est **OK** :
```
1.
Ceci est le couplet 1
    <--- Espace ajouté
2.
Ceci est le couplet 2
```

### Faire un retour manuel à la ligne pour les lignes trop longues


C'est exemple n'est **pas OK** :
```
1.
Cette ligne est très très longue, elle risque de mal s'afficher si aucun saut de ligne n'est fait.
```

C'est exemple est **OK** :
```
1.
Cette ligne est très très longue,
mais elle s'affiche bien,
car les bons sauts de ligne ont été fait.
```


#### Afficher un paragraphe sur plusieurs slides

```
1.
Il peut arriver qu'un couplet par exemple
soit très long, et sur plusieurs lignes.
Seulement pour pouvoir maîtriser
le fait d'ajouter la deuxième partie
du couplet sur une deuxième slide
tout en gardant le fait qu'il s'agit d'un seul
et même couplet, on peut ajouter un
\n
Et ainsi cette nouvelle strophe apparaitra
sur une deuxième slide
Tout en restant le couplet 1
```

> Note: Pour qu'une strophe s'affiche bien ne pas dépasser 7 lignes de paroles

#### Sauter une ligne dans un paragraphe sans que cela soit considéré comme une autre strophe

```
1.
Vous pouvez pour cela ajouter le caractère "\s"
à l'endroit où vous voulez insérer un espace blanc.
Pour que les deux paragraphes soit séparés.
Dans une seule et même slide.
\s
Utile quand on veut doubler une partie
mais pas une autre (x2)
```

Cela affichera :

```
Vous pouvez pour cela ajouter le caractère "\s"
à l'endroit où vous voulez insérer un espace blanc.
Pour que les deux paragraphes soit séparés.
Dans une seule et même slide.

Utile quand on veut doubler une partie
mais pas une autre (x2)
```
