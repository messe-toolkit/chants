1.
Dieu, qui nous appelles à vivre
Aux combats de la liberté,

Pour briser nos chaînes
Fais en nous ce que tu dis !
Pour briser nos chaînes
Fais jaillir en nous l'Esprit !

2.
Dieu, qui nous apprends à vivre
Aux chemins de la vérité,

Pour lever le jour
Fais en nous ce que tu dis !
Pour lever le jour
Fais jaillir en nous l'Esprit !

3.
Dieu, qui nous invites à suivre
Le soleil du Ressuscité,

Pour passer la mort
Fais en nous ce que tu dis !
Pour passer la mort
Fais jaillir en nous l'Esprit !

4.
Dieu, qui as ouvert le livre
Où s´écrit notre dignité,

Pour tenir debout
Fais en nous ce que tu dis !
Pour tenir debout
Fais jaillir en nous l'Esprit !