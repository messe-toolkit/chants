R.
Jésus, Chemin de vie,
Tu nous mènes vers le Père.
Chemin de vérité,
Guide-nous à ta lumière !

1.
Notre place auprès du Père,
Tu t'en vas la préparer.
Viens nous prendre à cette fête
Où le coeur sera comblé.
Donne-nous, Seigneur, de croire
Au bonheur que tu promets.

2.
Fils de Dieu parmi ton peuple,
Comment voir ce que tu fais ?
Ton Esprit toujours à l'oeuvre
Nous travaille au plus secret.
Donne-nous, Seigneur, de croire
Au bon grain qui est semé.

3.
Dans un monde où tu fais signe,
Que nos yeux demeurent ouverts !
Tu attends de ton Église
D'être là où Toi tu es.
Donne-nous, Seigneur, de croire
Aux moissons d'éternité.