1.
Pour tous les face-à-face,
Les coeur à coeur avec l’aimé(e),
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour les premiers baisers,
La découverte des visages,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour les désirs comblés,
Les grandes joies que l’on partage,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour les moments de paix,
L’amour qui tient sous les orages,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.

2.
Pour les foyers qui vivent
Le coeur à coeur avec l’enfant,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour les joyeux élans
Qui transfigurent les familles,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour tous les coeurs brûlants
Puisant leur flamme à l’Évangile,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour ceux qui sont confiants
Malgré les vents imprévisibles,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.

3.
Pour les humains qui cherchent
Le coeur à coeur avec autrui,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour ceux qui voient l’ami
Dans le plus pauvre de leurs frères,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour les bonheurs construits
Dans les cités qui désespèrent,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.
Pour les espoirs de vie
Donnés aux peuples sans lumière,
DIEU D’AMOUR, NOUS TE RENDONS GRÂCE.