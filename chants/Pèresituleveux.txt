« S’étant mis à genoux,
Il priait en disant :
« Père, si tu le veux,
Éloigne de moi cette coupe ;
Cependant, que soit faite non pas ma volonté,
Mais la tienne. »
Alors, du ciel, lui apparut un ange
Qui le réconfortait. »