GOÛTEZ ET VOYEZ
COMME EST BON LE SEIGNEUR !

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Le Seigneur regarde les justes,
il écoute, attentif à leurs cris.
Le Seigneur affronte les méchants
pour effacer de la terre leur mémoire.

3.
Malheur sur malheur pour le juste,
mais le Seigneur chaque fois le délivre.
Il veille sur chacun de ses os :
pas un ne sera brisé.

4.
Le mal tuera les méchants ;
ils seront châtiés d’avoir haï le juste.
Le Seigneur rachètera ses serviteurs :
pas de châtiment pour qui trouve en lui son refuge.