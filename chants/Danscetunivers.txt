1.
Dans cet univers
Travaillé par des genèses
La terre bouge encor,
En lutte avec la mort.
Des vivants par milliards ont tracé un chemin,
L’avenir est ouvert à tant d’autres humains.

2.
Dans cet univers
Habité par Dieu lui-même,
Nous sommes invités,
La place est préparée.
Bienheureux qui s’avance au festin de la vie,
L’avenir est ouvert sur un monde infini.

3.
Dans cet univers
On entend l’écho du Verbe ;
Lui-même est descendu,
D’aucuns l’ont reconnu.
Des humains par millions font entendre sa voix,
L’avenir est ouvert à tout homme qui croit.

4.
Dans cet univers
Aujourd’hui toujours plus vaste
Surgissent des appels
De tous les points du ciel ;
D’innombrables vaisseaux s’aventurent très loin,
L’avenir est ouvert aux voyages sans fin.

5.
Dans cet univers
Où l’Esprit demeure à l’oeuvre,
Tenons les yeux levés,
La Pâque est allumée.
Nous marchons vers la rive où Jésus nous attend,
L’avenir est ouvert par le Maître des temps.