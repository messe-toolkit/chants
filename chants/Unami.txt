UN AMI POUR INVENTER LA ROUTE
ET GARDER LA CHALEUR DE SA MAIN DANS MA MAIN.
UN AMI POUR RESTER À L’ÉCOUTE
ET POURSUIVRE AVEC LUI LE CHEMIN.

1.
Un ami pour chanter
Comme l’on peut chanter
Lorsque l’on est aimé
Pour de vrai.
Un ami pour donner
Comme l’on peut donner
Lorsque l’on est aimé
Pour de vrai.

2.
Un ami qui comprend
À temps et contre temps
Et toujours nous apprend
Le présent.
Un ami qui attend
Aussi beau qu’un serment
Le regard d’un enfant
Un printemps.

3.
Un ami de valeur
Comme un révélateur
De c’qu’il y’a de meilleur
Dans ton coeur.
Un ami, un semeur
Le chant d’un bateleur
Un matin prometteur
De bonheur.

4.
Un ami pour choisir
Pour bâtir et fleurir
Une clé pour ouvrir
L’avenir.
Un ami pour partir
Même s’il faut mourir
Puisque c’est pour servir
Et grandir.