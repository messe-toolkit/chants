MARCHEZ À LA SUITE DU CHRIST

MARCHEZ À LA SUITE DU CHRIST, VOUS IREZ PARTOUT
AU-DEVANT DE TOUS LES HOMMES
QUI SOUFFRENT, QUI LUTTENT ET QUI ESPÈRENT.
MARCHEZ À LA SUITE DU CHRIST
POUR ANNONCER L'ÉVANGILE.

1.
Vous entendrez monter de la Terre
Le cri de ceux qui ont faim et soif de liberté.
Accueillez la passion de celui qui veut briser toutes nos chaînes.

2.
Vous entendrez les voix qui s'élèvent
Pour faire tomber tous les murs de haine et d'exclusion
Entrez dans le combat de celui qui vient abattre les frontières.

3.
Vous entendrez l'amour qui se livre
Dans le courage des prophètes qui ébranlent les consciences
Laissez-vous embraser par celui qui vient nous transmettre son souffle.