LE SEIGNEUR A FAIT CONNAÎTRE SA VICTOIRE
ET RÉVÉLÉ SA JUSTICE AUX NATIONS.

ou : ALLÉLUIA ! (6e dimanche de Pâques - B)

1.
Chantez au Seigneur un chant nouveau,
car il a fait des merveilles ;
par son bras très saint, par sa main puissante,
il s’est assuré la victoire.

2.
Le Seigneur a fait connaître sa victoire
et révélé sa justice aux nations ;
il s’est rappelé sa fidélité, son amour,
en faveur de la maison d’Israël.

3.
La terre tout entière a vu
la victoire de notre Dieu.
Acclamez le Seigneur, terre entière,
sonnez, chantez, jouez !