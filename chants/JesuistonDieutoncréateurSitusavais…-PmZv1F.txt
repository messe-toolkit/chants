R.
Si tu savais le don de Dieu, c’est toi qui m’aurais demandé à boire,
Je suis ton Dieu ton créateur, viens reposer sur mon cœur.

1.
Je suis le Dieu qui t’a fait, celui qui t’a tissé dans le sein de ta mère
J’ai tout donné pour ta vie, ne crains plus désormais, car je t’ai racheté.

2.
Si tu traverses les eaux, si tu passes la mort, je serai avec toi
Je t’ai choisi Israël, je t’ai pris par la main, je suis ton Rédempteur.

3.
Je suis le Dieu d’Israël, Créateur et Sauveur, qui a fait toute chose
Seul j’ai déployé les cieux, raffermis l’univers, il n’est pas d’autre Dieu.

4.
Je marcherai devant toi pour désarmer les rois et pour t’ouvrir les portes
J’aplanirai les hauteurs, briserai les verrous, car je suis le Seigneur.

5.
Je t’ouvrirai les trésors et je te donnerai les richesses cachées,
Pour que tu saches, Israël, que je suis le Seigneur, que je suis le seul Dieu.