R.
Dieu de l'univers, Dieu Saint !
Du haut des cieux regarde et vois ton peuple.
Jamais plus nous n'irons loin de toi,
Fais-nous vivre pour te chanter sans fin !

1.
Nous étions dans la nuit,
Aveugles et asservis,
Ta main nous libère.
En prenant nos fardeaux,
Par la croix de ton Fils,
Tu nous as sauvés.

2.
Seigneur, Dieu d’Abraham,
Tu te révèles à nous.
Tu crées une alliance,
Tu nous donnes la vie,
Promesse du salut
Dans le Christ Jésus !

3.
Seigneur, Dieu de Moïse,
Tu dévoiles ton nom,
Nous te rendons grâce.
Tu es juste, Seigneur,
Vraiment pure est ta loi,
Chemin du salut.

4.
Tu rejoins les petits,
Abaissé jusqu’à nous,
Prenant nos faiblesses.
Tu nous donnes de vivre,
Fils enfin retrouvés,
Dans la joie du Père.

5.
Dieu d’amour infini,
Ton peuple rassemblé
Célèbre ta gloire.
Tu te livres en nos mains,
Donnant ta vie pour nous,
Sois notre secours.