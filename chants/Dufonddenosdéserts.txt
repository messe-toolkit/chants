R.
DU FOND DE NOS DÉSERTS,
NOUS VOICI POUR ÉCOUTER.
TA PAROLE DANS NOS VIES
ÉVEILLERA NOS COEURS.
TA PAROLE DANS NOS VIES
ÉVEILLERA NOS COEURS.

1.
Rassemblés pour célébrer Ton nom,
Devant Toi, Seigneur, nous voici.
Éveille-nous par le souffle de l’Esprit,
Nous irons porter Ta louange.

2.
Appelés dans notre quotidien,
Nous cherchons la source de vie.
Éveille-nous par le souffle de l’Esprit,
Nous tiendrons la main de nos frères.

3.
Rassemblés de tous les horizons,
Fais de nous Ton peuple choisi.
Éveille-nous par le souffle de l’Esprit,
Nous serons le sel de la terre.

4.
Fais de nous des messagers de paix
Dans ce monde aux corps déchirés.
Éveille-nous par le souffle de l’Esprit,
Nous saurons les mots qui libèrent.

5.
Viens chez nous, Seigneur des renouveaux,
Purifie nos coeurs et nos vies.
Éveille-nous par le souffle de l’Esprit,
Pour chanter sans fin Ta présence !