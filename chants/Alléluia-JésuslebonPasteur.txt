ALLÉLUIA, ALLÉLUIA.

Jésus, le bon Pasteur,
connaît ses brebis
et ses brebis le connaissent :
pour elles il a donné sa vie.

ALLÉLUIA.