R.
VOICI LE DON QUE DIEU NOUS FAIT
DANS LE MYSTÈRE DE CETTE NUIT
VOICI LE DON QUE DIEU NOUS FAIT
POUR TOUS LES HOMMES DE LA TERRE
PORTÉE PAR MARIE,
VOICI LA LUMIÈRE
LA TERRE A DONNÉ SON FRUIT
PORTÉE PAR MARIE,
VOICI LA LUMIÈRE
LE MESSIE PROMIS.

1.
On l’appelle Emmanuel
Dieu avec nous
le compagnon des hommes.
On l’appelle Emmanuel
C’est la merveille venue du ciel.

2.
On l’appelle Soleil levant
Fils de David
le défenseur des pauvres.
On l’appelle Soleil levant
Son coeur réchauffe tous les vivants.

3.
Il vient en Prince de la Paix
pour établir
son règne de Justice.
Il vient en Prince de la Paix
mais son Royaume reste caché.