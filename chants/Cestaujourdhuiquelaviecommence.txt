1.
Sortez de vos vieux sommeils
Aujourd’hui tout est possible
Aujourd’hui rien n’est pareil
Ce jour nous écrit la Bible

R.
C’EST AUJOURD’HUI QUE LA VIE COMMENCE
C’EST LE MATIN DU PREMIER JOUR.
POUR QUI LE VEUT C’EST UN JOUR DE NAISSANCE
CE MATIN JÉSUS EST DE RETOUR.

2.
Laissez là vos amis frileux
Habillez vos coeurs de fêtes
Aujourd’hui c’est fabuleux
Ce jour c’est vous qui le faites

3.
Ne restez pas les yeux au ciel
La tête au fond des nuages
Ce matin c’est officiel
Aujourd’hui Dieu a notre âge

4.
Ce jour les aveugles ont vu
Ce jour les sourds vont l’entendre
C’est le retour de Jésus
Ne le laissons pas attendre