ROIS DES NATIONS, MAÎTRE DU MONDE,
QUE TON RÈGNE VIENNE.

1.
Grandes, merveilleuses, tes oeuvres,
Seigneur, Dieu de l’univers !

2.
Ils sont justes, ils sont vrais, tes chemins,
Roi des nations.

3.
Qui ne te craindrait, Seigneur ?
À ton nom, qui ne rendrait gloire ?

4.
Oui, toi seul es saint !
Oui toutes les nations viendront
Et se prosterneront devant toi ;
Oui, ils sont manifestés, tes jugements.