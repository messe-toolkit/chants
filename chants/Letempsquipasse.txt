LE TEMPS S’EN VIENT
LE TEMPS S’EN VA
MOI… JE N’EN REVIENS PAS

1.
Les oiseaux de passage
Ont, d’allers en retours
Griffé bien des visages
Envolé tant d’amour
Suis-je resté l’enfant qui n’a pas su grandir ?
Que tant d’années s’effacent ainsi des souvenirs,
ainsi des souvenirs.

2.
Cette photo ancienne
De moi, ai-je conclu
Cette photo ancienne
Ne me ressemble plus
Sommes-nous donc tout neufs devant notre avenir ?
Ce sont les autres, autour, qui se mettent à vieillir,
qui se mettent à vieillir.

3.
L’horloge du quartier
A donné tant de coups
Que beaucoup sont tombés
Sous le chant du coucou
Que sont-ils devenus mes amis du passé ?
Moi, j’ai femme et enfants comme ciel étoilé,
comme ciel étoilé.