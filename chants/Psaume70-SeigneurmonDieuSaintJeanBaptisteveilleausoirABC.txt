SEIGNEUR MON DIEU,
TOI, MON SOUTIEN DÈS AVANT MA NAISSANCE.

1.
En toi, Seigneur, j’ai mon refuge :
garde-moi d’être humilié pour toujours.
Dans ta justice, défends-moi, libère-moi,
tends l’oreille vers moi, et sauve-moi.

2.
Seigneur mon Dieu, tu es mon espérance,
mon appui dès ma jeunesse.
Toi, mon soutien dès avant ma naissance,
tu m’as choisi dès le ventre de ma mère.

3.
Pour beaucoup, je fus comme un prodige ;
tu as été mon secours et ma force.
Je n’avais que ta louange à la bouche,
tout le jour, ta splendeur.

4.
Ma bouche annonce tout le jour
tes actes de justice et de salut.
Mon Dieu, tu m’as instruit dès ma jeunesse,
jusqu’à présent, j’ai proclamé tes merveilles.