1.
Écoute, réponds-moi
Car je suis las, regarde-moi
Veille sur moi, oh Seigneur
Je ne suis que ton serviteur.
Je t’appelle chaque jour
Je te demande ton Amour
J’élève mon âme vers toi
Je te supplie et crie vers Toi.

R.
DIEU QUI ES BON, DIEU QUI PARDONNES
J’ÉLÈVE MON ÂME VERS TOI
DIEU QUI ES BON, DIEU QUI PARDONNES
ÉCOUTE ET ENTENDS MA VOIX.

2.
Toi qui es bon et qui pardonnes
Entends ma voix qui résonne
Écoute Seigneur ma prière
Et réponds-moi, ô tendre Père.

Quand dans la nuit je t’appelle
Tu me réponds, Dieu éternel
Rien ne t’égale, oh mon Roi
Je veux demeurer près de Toi.

3.
Dieu de tendresse et de pitié
Plein d’amour et de vérité
Tu sais m’aider, me consoler
Je veux te chanter, te louer.