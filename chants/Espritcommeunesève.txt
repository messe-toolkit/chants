1.
Esprit comme une sève irriguant le corps de Marie,
Esprit comme une sève irriguant le tronc de ton peuple,
Tu féconderas notre peuple de foi.

R.
Souffle de Dieu, donne le jour.
Souffle de Dieu, donne la vie !

2.
Esprit comme le feu élevant la voix des prophètes,
Esprit comme le feu relevant Jésus de la mort,
Tu l’emporteras sur notre péché.

3.
Esprit comme le vent, nous ne savons pas d’où tu viens,
Esprit comme le vent, nous ne savons pas où tu vas,
Tu nous donneras la fraternité.

4.
Esprit comme le vin, nous tenons de toi notre soif,
Esprit comme le vin, nous tenons de toi notre espoir,
Tu nous rythmeras la fraternité.