1.
Vers toi, Seigneur, j'élève mon âme. (Ps 24)

2.
Le Seigneur a fait des merveilles, nous voici dans la joie. (Ps 125)

3.
Laissons éclater notre joie : Dieu est au milieu de nous ! (Isaïe 12)

4.
Fais-nous revenir à toi, Seigneur, et nous serons sauvés. (Ps 79)