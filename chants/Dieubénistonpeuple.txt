R.
Dieu, bénis ton peuple, donne-lui la paix.

Psaume 28.
Rendez au Seigneur, vous, les dieux,
rendez au Seigneur gloire et puissance.
Rendez au Seigneur la gloire de son nom,
adorez le Seigneur, éblouissant de sainteté.

La voix du Seigneur domine les eaux,
le Seigneur domine la masse des eaux.
Voix du Seigneur dans sa force,
voix du Seigneur qui éblouit.

Le Dieu de la gloire déchaîne le tonnerre.
Et tous dans son temple s'écrient: "Gloire !"
Au déluge le Seigneur a siégé;
il siège, le Seigneur, il est roi pour toujours !