1.
La boulangère en son logis pieux,
Avril venant reçut le grain de Dieu.
L’a mis à l’ombre en son humble grenier.
L’a serré là pendant neuf mois entiers.

FAITES-NOUS LE PAIN,
MARIE, Ô MARIE !
FAITES-NOUS LE PAIN,
CAR NOUS AVONS FAIM!

2.
La boulangère a pris un long chemin
Pour s’en aller à la Maison du Pain.
Pour le pétrir elle a peiné la nuit,
L’a mis au monde, environ à minuit.

CUISEZ-NOUS LE PAIN,
MARIE, Ô MARIE !
CUISEZ-NOUS LE PAIN,
CAR NOUS AVONS FAIM!

3.
L’a cuit trente ans au feu de sa maison,
À la chaleur de sa belle saison,
À la douceur de son coeur le plus doux,
Le tendre Pain, le Pain blond, le Pain roux.

PORTEZ-NOUS LE PAIN,
MARIE, Ô MARIE !
PORTEZ-NOUS LE PAIN,
CAR NOUS AVONS FAIM!

4.
Après trente ans, l’ayant du four ôté,
Son fils unique, en ville, l’a porté
À tous les gens affamés d’alentour,
Le Pain nouveau, le Pain tout chaud d’amour.

SERVEZ-NOUS LE PAIN,
MARIE, Ô MARIE !
SERVEZ-NOUS LE PAIN,
CAR NOUS AVONS FAIM!

5.
Pour trente sols le marchand l’a vendu,
Pour trente sols mille dents l’ont mordu
Au grand repas qui fut un vendredi
Servi pour l’homme à l’heure de midi.

LIVREZ-NOUS LE PAIN,
MARIE, Ô MARIE !
LIVREZ-NOUS LE PAIN,
CAR NOUS AVONS FAIM!

6.
Mais quand l’a vu meurtri, rompu, détruit,
Le Pain vivant qu’elle avait fait de nuit,
Comme un agneau par les loups dévoré,
La Boulangère en grand deuil a pleuré.

PLEUREZ SUR LE PAIN,
MARIE, Ô MARIE !
PLEUREZ SUR LE PAIN,
CAR NOUS AVONS FAIM!