R.
JE TE VOIS SEIGNEUR SUR LA CROIX
POURQUOI ?
DIS-MOI POURQUOI ?
TANT DE VIOLENCE ET TANT DE HAINE VERS TOI
JE NE COMPRENDS PAS

1.
Je sens bien
Que mon seul regard vers Toi
Ne saura soulager Ta souffrance
Mais je sens
Que Ton beau regard vers moi
M'apporte calme et délivrance

2.
Je sens bien
Que mon coeur tourné vers Toi
Ne pourra comprendre ce mystère
Mais je sens
Que Ton coeur tourné vers moi
Dit Ton Amour et Ta lumière

3.
Je sens bien
Que mes mains tendues vers Toi
Ne pourront apaiser Ta détresse
Mais je sens
Que Tes mains tendues vers moi
Sont la plus douce des tendresses