R.
Envoyés dans ce monde où la Pâque est à l'oeuvre.
Hosanna! Hosanna! Bénissons notre Dieu !
C'est lui qui nous appelle !
Envoyés dans ce monde où la croix nous fait signe.
Hosanna! Hosanna! Bénissons notre Dieu !
C'est lui qui nous envoie !

1.
Nous suivons un Seigneur sans arme ni armure ;
Son amour fait pour nous des merveilles ! R I
Nous suivons un Seigneur vêtu comme un esclave ;
Son amour a fait pour nous des merveilles ! R II
Nous suivons un Seigneur qui rassemble les peuples ;
Son amour envers nous s'est montré le plus fort ! R III

2.
Nous suivons un berger tué sur le Calvaire ; (R I)
Nous suivons un berger qui aime les plus pauvres ; (R II)
Nous suivons un berger qui prend soin des plus faibles ; (R III)

3.
Nous suivons un Messie, folie pour les puissances ; (R I)
Nous suivons un Messie, sagesse pour les humbles ; (R II)
Nous suivons un Messie, que le monde rejette ; (R III)

4.
Nous suivons un Agneau que blessent nos offenses ; (R I)
Nous suivons un Agneau remède à nos blessures ; (R II)
Nous suivons un Agneau qui pardonne et qui sauve ; (R III)

5.
Nous suivons un Ami, venu nous rendre libres ; (R I)
Nous suivons un Ami, trahi par ceux qu´il aime ; (R II)
Nous suivons un Ami, qui nous dit d´être frères ; (R III)

6.
Nous suivons le vrai Roi, sans sceptre ni couronne ; (R I)
Nous suivons le vrai Roi, vainqueur de l´adversaire ; (R II)
Nous suivons le vrai Roi, qui nous mène à sa gloire ; (R III)