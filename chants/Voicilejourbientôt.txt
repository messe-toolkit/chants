VOICI LE JOUR BIENTÔT, LE JOUR NOUVEAU
LA TERRE DES VIVANTS, LA TERRE DES VIVANTS.

1.
Voici que chaque loup regagne sa tanière
Voici que chaque épée retrouve son fourreau
L’aurore vient calmer toute angoisse ou colère
Lumière a révélé murmures et complots.
Les voilà dénoncés les lions prêts au carnage
Ils ne porteront plus le noble nom d’ “humains”
Et d’autres jeunes fauves tapis en embuscade
Iront chercher ailleurs les mets de leurs festins.

2.
Voici le jour nouveau, le jour de la Justice
Chacun a une place et le froment offert
Goûtez comme le pain qu’il donne est un délice
Chacun en a la part qui ne nuit à son frère.
Voici le jour nouveau, le grand jour de la Paix
Le jour qui interdit de châtier l’innocent
L’outrage et le mépris sont chassés à jamais
Et la terre est saisie d’un cri d’enfantement.

3.
Voici le jour nouveau, c’est le jour du Seigneur
Son rêve a réveillé les hommes endormis
Voici que s’est levé un peuple serviteur
Qui jamais ne craindra les terreurs de la nuit.