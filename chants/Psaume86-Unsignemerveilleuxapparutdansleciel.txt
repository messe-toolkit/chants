Un signe merveilleux apparut dans le ciel :
une femme revêtue de soleil, la lune sous
ses pieds, et sur sa tête une couronne de
douze étoiles !

Psaume 86

Sion est fondée sur les montagnes, où
resplendit la sainteté du Seigneur.

C’est pourquoi il préfère les portes de ses
murs à toutes les demeures de Jacob, car
c’est lui, le Seigneur, qui l’a fondée.

Un signe merveilleux apparut dans le ciel...

Ils la célèbrent tous en choeur, les chanteurs
et les danseurs : « C’est merveille ce qu’on
dit de toi, Cité de Dieu ! »

Le Seigneur a dénombré les enfants de
Sion, dans le livre des peuples : « Un tel est
né ici et tel autre là-bas. »

Un signe merveilleux apparut dans le ciel...

« Parmi ceux qui me connaissent, j’en
dénombre en Egypte et d’autres à Babylone.

Il en est d’autres en Philistie, et d’autres en
Phénicie, en Ethiopie.

Un signe merveilleux apparut dans le ciel...

Mais Sion, chacun lui donne le nom de
Mère, car chacun d’eux est né en elle ! »

Gloire au Père, au Fils, au Saint-Esprit, pour
les siècles des siècles, Amen !

Un signe merveilleux apparut dans le ciel...