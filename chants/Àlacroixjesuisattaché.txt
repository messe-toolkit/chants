R.
À la croix je suis attaché, avec toi, ô Jésus-Christ.
À la croix je suis attaché, avec toi, ô Jésus-Christ.

1.
L'homme devient juste devant Dieu par la foi en Jésus-Christ.
C'est pourquoi nous avons cru en Jésus-Christ pour devenir des justes par la foi au Christ.

2.
Le Fils de Dieu m'a aimé et s'est livré pour moi.
Soit que je vive, soit que je meure, la grandeur du Christ resplendira dans ma vie.

3.
Pour moi, vivre c'est le Christ, et mourir est un avantage.
Je vis, mais ce n'est plus pour moi, c'est le Christ qui vit en moi.