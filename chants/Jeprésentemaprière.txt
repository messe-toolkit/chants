1.
Je présente ma prière
Comme un enfant, blessé au coeur.
Ne fais pas la sourde oreille
Écoute-moi, Seigneur !

R.
MONTRE-TOI, EN PLEINE LUMIÈRE,
COMME UN SOLEIL LEVANT.
CHAQUE MATIN, FAIS-MOI RENAÎTRE :
TU ES LE DIEU VIVANT!
CHAQUE MATIN, FAIS-MOI RENAÎTRE :
TU ES LE DIEU VIVANT!

2.
Ma colère te provoque,
Je ne supporte aucun refus,
Ni contraintes, ni reproches,
Ni paradis perdu.

3.
Tu me vois dans la détresse,
Elle m’étouffe sans répit.
Sois fidèle à tes promesses,
Viens redresser ma vie.

4.
Tu fracasses les montagnes :
Coups de tonnerre dans le ciel.
Mais tu berces les rivages :
Tu fais danser la mer.

5.
Les reflets de ton visage
Brillent partout dans l’univers.
Ton eau fraîche m’accompagne
Jusque dans mon désert.

6.
Je découvre ta présence :
Souffle qui porte ma chanson.
Ta Parole dès l’enfance
A prononcé mon nom.