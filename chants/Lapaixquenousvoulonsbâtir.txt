1.
La paix que nous voulons bâtir,
Dieu la construit sur la justice.
Il veut que l'homme puisse vivre ;
En mots d'amour il nous le dit.

R.
La paix dans la justice et dans l'amour,
La paix sous le soleil des libertés,
Nous la cueillons sur le rameau « Fraternité » ;
Parmi nous plantons cet olivier !

2.
Jésus nous a montré la voie,
Il a fait face à la violence.
Sur le chemin des résistances
Christ a mené son dur combat.

3.
Un jour il portera du fruit
Le grain tombé en bonne terre.
Plus fort que tous les bruits de guerre,
Que monte au ciel un chant de vie !

4 (au cas où l’on évoque les morts à la guerre)
Sans nombre ils ont connu la mort,
Ceux dont l'audace nous libère.
Leurs noms gravés sur tant de pierres,
Nous les dirons longtemps encore.

5.
La paix que l'univers attend
Tient son secret d'un monde frère.
Ouvrons des portes et des frontières
Pour le bonheur de tout vivant.