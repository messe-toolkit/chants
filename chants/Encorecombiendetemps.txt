1.
Encore combien de temps pour que le vent se lève,
Qui nous réveillera dans un monde nouveau
Où chacun sera bien, vivant comme dans ce rêve
Que je fais bien souvent, où l’on est tous égaux ?

NOUS LE VOULONS CE MONDE
SANS CRI ET SANS HAINE,
NOUS LE VOULONS CE MONDE
OÙ TA MAIN DANS LA MIENNE
SAURONT ENSEMBLE DESSINER.
DES MOTS D’AMOUR, DES MOTS DE PAIX.

2.
Encore combien de temps pour que la nuit s’efface
Enfin, et pour toujours, de nos yeux éblouis
Par un soleil tout neuf éclairant toutes races
D’un seul et grand rayon qu’on appelle la vie ?

3.
Encore combien de temps pour que l’amour explose,
Que la terre à jamais en soit contaminée,
Que tendresse et bonheur règnent sur toutes choses
Et qu’un enfant soit roi de ce monde de paix ?