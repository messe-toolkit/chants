ALLELUIA ! ALLELUIA !

1.
Le salut, la puissance,
la gloire à notre Dieu,
ALLELUIA !
Ils sont justes, ils sont vrais
ses jugements,
ALLELUIA !

2.
Célébrez notre Dieu,
serviteurs du Seigneur,
ALLELUIA !
Vous tous qui le craignez,
les petits et les grands,
ALLELUIA !

3.
Il règne, le Seigneur,
notre Dieu tout-puissant,
ALLELUIA !
Exultons, crions de joie
et rendons-lui la gloire,
ALLELUIA !

4.
Car elles sont venues
les noces de l’Agneau,
ALLELUIA !
Et pour lui son épouse
a revêtu sa parure,
ALLELUIA !