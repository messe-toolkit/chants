R.
UN VENT D'ESPOIR VIENT DE SOUFFLER
NOUS DEVIENDRONS "PAIN DE LA PAQUE"
AINSI L'AMOUR S'EST REFUGIE
ENTRE NOS MAINS

1.
Les jours sont si pesants
Les habitudes telles
Que nous ne voulons plus sortir d'ici
Ce vent à contretemps
Bouscule et nous appelle
A reprendre la route de la vie

2.
Si fort est le courant
Et les "modes" sont telles
Que nos chemins parfois sont sans issue
Ce vent à contretemps
Dérange et interpelle
Et nous projette alors dans l'inconnu

3.
Tout est si angoissant
Et l'aventure est telle
Que nous laisserions tout tomber parfois
Ce vent à contretemps
Etonne et renouvelle
"Nul ne sait d'où il vient ni où il va"