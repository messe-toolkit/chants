SOUS L’ÉCORCE DES MOTS
LE CROQUANT D’UNE AMANDE
SOUS L’ÉCORCE DES MOTS
DES SENTEURS DE LAVANDE
SOUS L’ÉCORCE DES MOTS
POURVU QUE TU L’ENTENDES
SOUS L’ÉCORCE DES MOTS
LA PAROLE EN OFFRANDE

1.
Ne restons pas sur notre faim
Et dégustons cette Parole
Partageons-la comme un bon pain
Les mots nous font la farandole
C’est Dieu qui régale aujourd’hui
Ses mots nous remplissent de vie.

2.
Nous étions tous, sans le savoir
Le tiers-monde de la parole
Et nous avancions sans rien voir
Sans comprendre les Paraboles
Dans nos vies saturées de bruit
Nous n’entendions même plus nos cris.

3.
Savourons bien les mots de Dieu
Ils ne manquent pas sur la table
Ils sont subtils et délicieux
Que ce festin soit agréable
Attardons-nous pour écouter
Prenons du temps pour les goûter.