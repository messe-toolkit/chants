Stance

Bénie soit la Trinité sainte et une,
qui crée et gouverne toutes choses
maintenant et dans les siècles sans fin.

R.

Benedictus Deus et Pater Domini nostri Iesu Christi.

Versets

Ps 33.

1.
Je bénirai le Seigneur en tout temps,
Sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
Que les pauvres m'entendent et soient en fête !

2.
Magnifiez avec moi le Seigneur,
Exaltons tous ensemble son nom.
Qui regarde vers lui resplendira,
Sans ombre ni trouble au visage.

3.
Un pauvre crie ; le Seigneur entend :
Il le sauve de toutes ses angoisses.
L'ange du Seigneur campe à l'entour
Pour libérer ceux qui le craignent.