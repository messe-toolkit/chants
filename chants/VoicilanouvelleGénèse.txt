Voici la nouvelle genèse :
En toi, Vierge immaculée,
La grâce originelle refleurit.
Notre terre n'est plus maudite,
Nous la verrons bientôt
Donner le fruit de vie.
Avec toi, Marie Mère du Sauveur,
Nous glorifions la puissance de Dieu.

J'exulte de joie dans le Seigneur,
Et mon esprit tressaille en mon Dieu.

Il m'a revêtue des vêtements du salut
Et m'a couverte du manteau de la justice.

Le Seigneur fera germer l'action de grâce
Devant toutes les nations.