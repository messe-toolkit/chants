R.
BOUTON D'OR, BOUTON D'OR.
TU T'OUVRES A LA ROSEE DES QUE VIENT L'AURORE.
BOUTON D'OR, BOUTON D'OR.
ACCUEILLE LE SOLEIL POUR BRILLER PLUS FORT !

1.
La paix du Seigneur est comme la rosée,
si j'ouvre mon coeur, elle viendra s'y poser.
Ta paix Seign eur me fait vivre plus fort,
Ma vie fleurit comme un bouton d'or !

2.
L’amou r du Seigneur est comme le soleil,
Si j’ouvre mon coeur, il y fait des merveilles.
Ton amour Seigneur me fait vivre plus fort,
Ma vie fleurit comme un bouton d’or !

3.
La joie du Seigneur je veux la partager,
Mettre dans vos coeurs l’envie de le chanter.
Ta joie Seigneu r nous fait chanter plus fort,
Joyeux comme un champ de boutons d’or !