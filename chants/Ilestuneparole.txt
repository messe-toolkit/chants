1.
Il est une Parole,
Comment vint-elle au jour ?
Il est une Parole...
Qui donc l’a proférée ?
Il est une Parole
Qui peut tout sauver.

2.
Il est une Parole,
Venue parmi les siens,
Il est une Parole
Plus forte que la mort.
Il est une Parole
Qui a pris corps.

3.
Il est une Parole,
Pour l’homme sans appui.
Il est une Parole
Pour qui ne sait plus rien.
Il est une Parole,
Entre nos mains.

4.
Il est une Parole,
Marchant à nos côtés.
Il est une Parole
Brûlante comme un feu,
Il est une Parole
Qui naît de Dieu.

5.
Il est une Parole,
Au monde qui attend.
Il est une Parole
Pour l’homme de désir.
Il est une Parole
Qui doit venir...