1.
Ce que tu prends pour la rosée,
Dit le matin en se levant,
Ce sont les larmes que le vent
Ne pouvait plus porter.
Il les a mises à tes pieds,
En attendant, en attendant
Que tu viennes les assécher
Avant le jour en son couchant.
Pendant la nuit, continue-t-il,
Alors que tu fermais les yeux,
Il s’est vécu tant de misères,
Tant de gens criaient vers leur Dieu.
De leur solitude fragile,
Comme des chiens au ventre creux,
Ils n’étaient plus que pauvres hères
Appelant le secours des cieux.

2.
Ce que tu prends pour la rosée,
Dit le matin en se levant,
Ce sont les larmes que le vent
Ne pouvait plus porter.
Il les a mises à tes pieds,
En attendant, en attendant
Que tu viennes les assécher
En ces jours où naît le printemps.
Pendant l’hiver, continue-t-il,
Alors que tu vivais frileux,
La haine a bâti des congères,
Où tant ont péri par le feu.
Pour justifier du droit d’asile,
Sur un bout de planète bleue,
Ils ont crié de terre à terre,
Puis en dépit, de terre à Dieu.

3.
Ce que tu prends pour la rosée,
Dit le matin, comme un ami,
Ce sont les larmes, qu’aujourd’hui,
Tu pourrais essuyer.