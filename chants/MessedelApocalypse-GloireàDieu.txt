R.
Gloire à Dieu au plus haut des cieux,
Paix sur la terre aux hommes qu’il aime,
Gloire à toi notre Dieu.

1.
Nous te louons,
Nous te bénissons,
Nous t’adorons.

2.
Nous te glorifions,
Nous te rendons grâce
Pour ton immense gloire.

3.
Seigneur Dieu
Roi du ciel,
Dieu le Père tout-puissant.

4.
Seigneur,
Fils unique Jésus-Christ.

5.
Seigneur Dieu,
Agneau de Dieu,
Le Fils du Père.

6.
Toi qui enlèves le péché du monde,
Prends pitié de nous.

7.
Toi qui enlèves le péché du monde,
Reçois notre prière.

8.
Toi qui es assis à la droite du Père,
Prends pitié de nous.

9.
Car toi seul es saint,
Toi seul es Seigneur.

10.
Toi seul es le Très-Haut,
Jésus-Christ,
Avec le Saint-Esprit.

11.
Dans la gloire de Dieu le Père,
Amen.