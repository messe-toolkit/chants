1.
Femme voulue par Dieu
Comme une oeuvre parfaite
En qui reposerait
Le don de son Amour,
Tu exultes de joie
Aux promesses de vie :
Les pauvres en ton enfant
Seront peuple de prêtres,
Fils du Très-Haut.

2.
Femme comblée par Dieu
De sagesse et de grâce
Pour être parmi nous
Reflet de sa bonté,
Tu révèles Celui
Qui étanche la soif :
Le Christ a fait pour toi
Couler en abondance
Un vin nouveau.

3.
Femme guidée par Dieu
Au désert de l’épreuve
Où manque à notre espoir
La force d’un appui,
Tu nous vois chancelants
Sous le poids de la croix :
Ta foi inébranlée
Soutient notre faiblesse
Et nous conduit.

4.
Femme donnée par Dieu
À l’Église naissante
Qui brûle d’accueillir
Le souffle de l’Esprit,
Ton silence nous offre
Un espace de paix :
En toi nous écoutons
La source qui murmure
Au fond des coeurs.

5.
Femme vêtue par Dieu
D’un manteau de lumière
Quand l’ombre de la mort
S’étend sur l’univers,
Tu éclaires la voie
Du Royaume des cieux :
Servante du Seigneur,
Tu règnes dans la gloire
Avec ton Fils.