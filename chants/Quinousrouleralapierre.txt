1.
La pierre est lourde à nos tombeaux,
L´espoir sommeille avec le corps
D´un Dieu mortel,
Qui forcera nos murs scellés,
Sinon Jésus ressuscité ?
Sois l´espérance à notre nuit,
Toi qui nous donnes comme appui
Ta pierre d´angle.
Sois l´espérance à notre nuit,
Toi qui nous donnes comme appui
Ta pierre d´angle.

2.
Dans nos maisons pourquoi la peur,
Pourquoi le doute au fond des coeurs
Si tu es là ?
Qui peut franchir nos murs scellés,
Sinon Jésus ressuscité ?
Sois avec nous le Dieu de paix,
Toi le Seigneur qui nous connais
Plus que nous-mêmes.
Sois avec nous le Dieu de paix,
Toi le Seigneur qui nous connais
Plus que nous-mêmes.

3.
Quel est celui sur nos chemins
Qui nous appelle et nous rejoint
Pour se livrer ?
Qui peut ouvrir nos yeux scellés,
Sinon Jésus ressuscité ?
Reste avec nous, le soir descend,
Tu es encore l´amour brûlant
Au cœur de l´homme.
Reste avec nous, le soir descend,
Tu es encore l´amour brûlant
Au cœur de l´homme.

4.
Sur le rivage où tu parais
Révèle-nous le signe offert
D´un Dieu pascal.
Qui nous apprend la joie scellée,
Sinon Jésus ressuscité ?
Invite-nous à ton repas,
Nous savons bien que tu diras
Comment tu aimes.
Invite-nous à ton repas,
Nous savons bien que tu diras
Comment tu aimes.