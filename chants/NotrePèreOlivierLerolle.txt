Notre Père, qui es aux cieux,
que ton nom soit sanctifié,
que ton règne vienne,
que ta volonté soit faite sur la terre comme au ciel.
Donne-nous aujourd’hui notre pain de ce jour.
Pardonne-nous nos offenses,
comme nous pardonnons aussi à ceux qui nous ont offensés.
Et ne nous laisse pas entrer en tentation
mais délivre-nous du Mal.

Délivre-nous de tout mal, Seigneur,
et donne la paix à notre temps ;
par ta miséricorde, libère-nous du péché,
rassure-nous devant les épreuves,
en cette vie où nous espérons
le bonheur que tu promets
et l’avènement de Jésus Christ, notre Sauveur.

Car c’est à toi qu’appartiennent le règne,
la puissance et la gloire, pour les siècles des siècles !