R.
Viens, lève-toi !
Moi, le Seigneur, je t’appelle.
Avance avec toute l’Église.
Viens, suis-moi, je marche avec toi.

1.
Pour que le monde perçoive la voix des semeurs de vie :
Qui enverrai-je ?
Pour que l’Église proclame l’Évangile sans chercher le repos :
Qui dira: me voici !

2.
Pour que le monde découvre le feu d’un regard d’amour :
Qui enverrai-je ?
Pour que l’Église rassemble des disciples serviteurs des petits :
Qui dira: me voici !

3.
Pour que le monde discerne la paix dans la main tendue :
Qui enverrai-je ?
Pour que l’Église propose sans relâche des chemins de pardon :
Qui dira: me voici !

4.
Pour que le monde rencontre ce Dieu qui entend nos cris :
Qui enverrai-je ?
Pour que l’Église révèle l’abondance du festin de la joie :
Qui dira: me voici !

5.
Pour que le monde connaisse la joie du pardon offert :
Qui enverrai-je ?
Pour que l’Église réveille l’espérance du bonheur à venir :
Qui dira: me voici !

6.
Pour que le monde refuse l’enfer des combats perdus :
Qui enverrai-je ?
Pour que l’Église construise le royaume dont le fruit est la paix :
Qui dira: me voici !

7.
Pour que le monde désire la vie qui jamais ne meurt :
Qui enverrai-je ?
Pour que l’Église prodigue les richesses que promet Jésus Christ :
Qui dira: me voici !

I1.
Je vous ai choisis afin
que vous annonciez la joie de l’Évangile :
Nous voici, seigneur !

I2.
Vous êtes mes frères et mes amis,
soyez témoins de la joie de l’Évangile :
Nous voici, seigneur !

I3.
Celui qui croit fera les mêmes oeuvres que moi:
faites grandir la joie de l’Évangile:
Nous voici, seigneur !