Sermon sur la Montagne - Béatitudes

1.
Louange et gloire à toi, la Lumière du monde !
Viens nous éclairer au long de nos jours.

2.
Sagesse des croyants, Jésus notre Maître,
Montre - nous lavoie qui mène au bonheur!

Paraboles et annonce du Royaume

3.
Louange à toi, Jésus, le semeur du Royaume !
Ouvre nos sillons au grain de la vie.

4.
Les mots que tu nous dis font grandir l'espérance ;
Gloire à toi Jésus, qui parles aujourd'hui.

Le pardon

5.
Tendresse du pardon; tu nous donnes l'eau vive.
Toi qui mets debout fais-nous revenir !

Guérisons

6.
Prophète du Seigneur, tu guéris nos blessures,
Tu es le Sauveur de tous les vivants.

7.
Merveille que tu fais sur la terre des hommes !
Les aveugles voient ton jour se lever.

Envoi en mission - Tempête apaisée

8.
Ton Souffle nous envoie révéler ton visage.
Qui vivra de toi pour dire ton Nom ?

9.
Seigneur, tu nous appelles à partir vers le large,
Nous croyons en toi qui calmes les flots.

Le pain de vie (Jn 6)

10.
Tu multiplies les pains pour la vie de ton peuple ;
Ouvre nos deux mains à l’homme affamé !

11.
Béni sois-tu, Seigneur, qui nous dresses une table !
Le vrai pain du ciel, c’est toi, Jésus Christ !

Identité de Jésus Christ - Transfiguration

12.
Tu es la Vérité, le Chemin vers le Père,
Parle à notre cœur, ô Fils du Très-Haut.

13.
Ton corps transfiguré nous annonce la Pâque.
Fais-nous regarder vers toi, Fils de Dieu.

Derniers dimanches de l’année liturgique

14.
Heureux les invités au Festin du Royaume !
Tu leur donneras le vin de ta joie.

15.
Viendront les jours de paix, les vendanges de gloire ;
Ton Royaume est là où règne l’amour.