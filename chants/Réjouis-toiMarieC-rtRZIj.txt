Réjouis-toi, Marie,
Comblée de grâce,
Le Seigneur est avec toi.
Tu es bénie, Marie, entre les femmes
Et Jésus, le fruit de ton sein, est béni !
Sainte Marie, Mère de Dieu,
Prie pour nous, pécheurs,
Sainte Marie, Mère de Dieu,
Prie pour nous, maintenant
Et à l’heure de la mort.