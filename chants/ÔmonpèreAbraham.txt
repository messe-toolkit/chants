De puits en puits, Abraham trace sa route.
D’étoile en étoile, il poursuit son chemin.

COMME UNE GOUTTE D’EAU
CUEILLIE AU FOND DU PUITS
QUI ROULE VERS LA RIVIÈRE
ET DEVIENT L’OCÉAN,
DES HOMMES AU COEUR NOUVEAU
EN MARCHE AVEC L’ESPRIT
S’EN VONT, DE FRÈRE EN FRÈRE,
VERS LA SOURCE DU VENT.

Partir, faire des choix, risquer l’inconnu…
Abraham fait ce que lui demande le Seigneur
(Genèse 12, 1-4).

1.
Abraham, Abraham,
Ô mon père Abraham
À l’aube d’une Alliance
Dieu t’appelle, Dieu t’appelle
À l’aube d’une Alliance
Seras-tu au rendez-vous ?
Seras-tu au rendez-vous ?

2.
Abraham, Abraham,
Ô mon père Abraham
Va-t-en vers ce pays
Que Dieu donne, que Dieu donne
Va-t-en vers ce pays
Inventer ton avenir,
Inventer ton avenir.

R.

Dieu a fait une promesse à Abraham : « Tu seras le père
d’un peuple immense. » Depuis ce jour, « Abraham eut
foi dans le Seigneur. » (Genèse 15, 6)

3.
Abraham, Abraham,
Ô mon père Abraham
Regarde dans le ciel
Les étoiles, les étoiles
Regarde dans le ciel
Et commence à les compter,
Et commence à les compter.

4.
Abraham, Abraham,
Ô mon père Abraham
Demain ta descendance
Sur la terre, sur la terre
Demain ta descendance
Sera bien plus grande encore,
Sera bien plus grande encore.

R.

La confiance entre l’homme et Dieu est totale
et réciproque. Il s’agit d’une Alliance. Abraham ne craint
plus d’affirmer sa foi en ce Dieu unique qui le protège
et qui lui montre sa tendresse en toute circonstance.

5.
Abraham, Abraham,
Ô mon père Abraham
Tu pars vers l’inconnu
Le coeur libre, le coeur libre
Tu pars vers l’inconnu
Dieu te montre le chemin
Dieu te montre le chemin.

6.
Abraham, Abraham,
Ô mon père Abraham
Tu sèmes sur ta route
L’espérance, l’espérance
Tu sèmes sur ta route
La tendresse de ton Dieu,
La tendresse de ton Dieu.

Refrain