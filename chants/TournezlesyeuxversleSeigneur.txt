R.
Tournez les yeux vers le Seigneur,
Et rayonnez de joie,
Chantez son nom de tout votre cœur,
Il est votre Sauveur,
C’est lui votre Seigneur.

1.
J’ai cherché le Seigneur
Et il m’a écouté.
Il m’a guéri de mes peurs
Et sans fin je le louerai.

2.
Dieu regarde ceux qu’il aime,
Il écoute leur voix,
Il les console de leur peine,
Et il guide leurs pas.

3.
Ceux qui cherchent le Seigneur
Ne seront privés de rien.
S’ils lui ouvrent leur cœur,
Ils seront comblés de bien.