Unis dans le même Esprit,
nous pouvons dire avec confiance
la prière que nous avons reçue du Sauveur :

Notre Père, qui es aux cieux,
que ton nom soit sanctifié,
que ton règne vienne,
que ta volonté soit faite sur la terre comme au ciel.
Donne-nous aujourd’hui notre pain de ce jour.
Pardonne-nous nos offenses,
comme nous pardonnons aussi à ceux qui nous ont offensés.
Et ne nous laisse pas entrer en tentation,
mais délivre-nous du Mal.

Délivre-nous de tout mal, Seigneur,
et donne la paix à notre temps :
soutenus par ta miséricorde,
nous serons libérés de tout péché,
à l’abri de toute épreuve,
nous qui attendons que se réalise
cette bienheureuse espérance :
l’avènement de Jésus Christ, notre Sauveur.

Car c’est à toi qu’appartiennent le règne,
la puissance et la gloire, pour les siècles des siècles !