Stance
Plus une étoile dans la nuit,
le ciel pour nous s’est assombri,
mais nous savons que vient le jour,
il est tout proche son retour.

VIENS, SEIGNEUR JÉSUS,
OH! VIENS, MARANATHA!

1.
Le ciel et la terre passent,
mes paroles demeurent pour toujours.

2.
Maintenant, vous êtes dans la tristesse,
bientôt, dans la joie pour toujours.

3.
Vous me suivez dans les épreuves,
vous serez avec moi pour toujours.

Stance
Dans la détresse et dans la peur,
oui, toute chose passe et meurt,
mais nous savons que vient l’été,
aux branches vertes du figuier.

VIENS, SEIGNEUR JÉSUS,
OH! VIENS, MARANATHA!