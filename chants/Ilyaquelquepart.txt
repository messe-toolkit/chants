R.
IL Y A QUELQUE PART TOUT AU FOND DE NOUS-MÊMES
AU-DELÀ DES CHAGRINS ET DES NUITS ET DES JOURS,
IL Y A QUELQUE PART UNE FLEUR, UN POÈME,
UNE ÉTOILE, UN CHEMIN QUI S’APPELLE L’AMOUR.

1.
Le passé, les débris et le temps qui s’égraine,
Le désir qui s’enfuit, le regard qui s’éteint.
Ce qui n’a pas fleuri le gâchis, la rengaine,
Le pourquoi silencieux qui s’estompe et revient.

2.
Trop de portes forcées, de déserts et de cages,
De maisons encombrées qui n’abritent plus rien.
Trop d’amants séparés, trop d’amours en naufrage ;
Dites-moi d’où me vient malgré tout ce refrain ?

3.
Sur le quai de la vie on oublie ses bagages,
Quand les liens sont brisés, quand la peur nous étreint.
Quand on cherche un projet, un grand rêve, un passage,
Quand on veut retrouver la douceur d’un matin.