1.
Dans une étable obscure
Sous le ciel étoilé
Né d’une vierge pure
Un doux Sauveur est né
Le Seigneur Jésus-Christ
Est né dans une crèche
Quand a sonné minuit.

2.
Sur cette paille fraîche
Malgré le vent qui mord
Dans l’ombre de la crèche
Le Roi du monde dort
Le Seigneur Jésus-Christ
Prions avec les anges
Dans l’ineffable nuit.

3.
Tandis que les rois mages
Tandis que les bergers
Lui portent leurs hommages
Portons-lui nos baisers
Le Seigneur Jésus-Christ
Saura bien nous sourire
En cette heureuse nuit.

4.
Pleins d’une foi profonde
Nous sommes à genoux
Ô Maître, ô Roi du monde
Étends les mains sur nous
Ô Jésus tout petit
Tout l’univers t’acclame
Dans l’adorable nuit.