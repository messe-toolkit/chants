R.
Alléluia, alléluia (bis)

1.
Ta splendeur est chantée
Par tous ceux qui aiment tes volontés
Ta puissance est annoncée
Au milieu de ton peuple.

2.
Ton amour est chanté
Par tous ceux qui aiment tes volontés
Ta tendresse est annoncée
Au milieu de ton peuple.

3.
Ta douceur est chantée
Par tous ceux qui aiment tes volontés
Ton royaume est annoncé
Au milieu de ton peuple.

4.
Ton salut est chanté
Par tous ceux qui aiment tes volontés
Ta victoire est annoncée
Au milieu de ton peuple.