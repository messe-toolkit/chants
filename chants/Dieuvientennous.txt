R.
DIEU VIENT EN NOUS PAR JÉSUS CHRIST,
IL FAIT DE NOTRE COEUR SA DEMEURE.
VERSETS

1.
La présence en nous du Père
du Fils et du Saint-Esprit,
voilà sa demeure.

2.
À l’Esprit qu’il nous a donné
nous savons que Dieu
demeure en nous.

3.
Soyons greffés sur le Seigneur
comme le sarment l’est sur la vigne,
pour demeurer en Dieu.

4.
Tout homme qui mange le corps du Christ
et boit son sang
demeure en Dieu.

5.
Si nous aimons nos frères,
nous demeurons dans la lumière,
et ne trébuchons pas.

6.
La force de Dieu nous habite
lorsque sa Parole
demeure en nous.

7.
Suivre sur cette terre Jésus-Christ,
mort et ressuscité,
c’est demeurer en Dieu éternellement.

8.
Par son côté transpercé d’où jaillit l’Esprit
Jésus-Christ nous ouvre
sa propre demeure.

9.
Dieu est Amour:
Le monde entier est porté
par la demeure trinitaire qui le soutient.