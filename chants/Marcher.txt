R.
NOUS AVONS MARCHÉ,
DES QUATRE COINS DE NOTRE TERRE !
NOUS AVONS VIBRÉ,
DE CES PAROLES DE TA PRIÈRE !
ET NOUS VOICI
TOUS RÉUNIS !

1.
C’est toi Jésus qui nous appelle
De par tes mots, de par tes gestes
Nous voulons vivre
Toujours plus près de toi
Nous voulons vivre
Toujours plus près de toi

2.
Nous arrivons dans la prière
Sur les chemins dans ta lumière
Nous voulons vivre
Toujours plus près de toi
Nous voulons vivre
Toujours plus près de toi
Nous avons marché…