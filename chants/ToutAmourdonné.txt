R.
TOUT AMOUR DONNE
FABRIQUE DES ETOILES
QUE DIEU DANS SA BONTE
DISPOSE SUR SA TOILE
LUMIERE JAILLIT
DU COEUR DE L'HOMME
DONNEE PAR DIEU
POUR L'HOMME.

1.
On peut se faire cadeau
Chaque matin qui nait
Et par de petits riens
Semer la paix
L'envie de faire du beau
En secret
Sous le regard divin
Qui sait.

2.
On peut se faire cadeau
En offrant de son temps
A celui qui avance
A contre-temps
Pour devenir égaux
On s'attend
Et Dieu qui nous devance
Est grand.

3.
On peut se faire cadeau
Dans les liens qui se créent
Un instant peut s'écrire
D'éternité
Et pas besoin de mot
Pour graver
Quand le coeur va choisir
D'aimer.

4.
On peut se faire cadeau
Dans un projet de vie
Qui vous prend tout entier
Qui vous saisi
De se jeter à l'eau
C'est folie
La confiance est chargée
De fruits.