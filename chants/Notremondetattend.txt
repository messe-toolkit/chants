R.
Seigneur, notre monde t'attend,
Notre monde t'attend,
Enlisé dans ses doutes et ses tourments.
Notre monde t'attend,
Notre monde t'attend,
Notre monde t'attend,
Notre monde t'attend

1.
Toi qui t'es fait petit enfant,
Toi qui es né sous notre loi,
Toi, Seigneur, notre printemps,
Viens, viens nous apprendre la joie !

2.
Toi qui t'es fait tout pauvre et nu
Dans notre monde déchiré
Toi, Seigneur, notre salut,
Viens, viens nous apprendre la paix !

3.
Toi qui t'es fait petit enfant,
Au cœur du monde qui t'oublie
Toi, Seigneur, toi le Vivant,
Viens, viens nous apprendre la vie !