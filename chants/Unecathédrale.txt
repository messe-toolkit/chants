R.
Que chaque enfant porte sa pierre
Au chantier de la maison du Père
Une cathédrale s'élève vers le ciel

1.
Ce Dieu qui descend dans nos vies
De liberté il est épris
Chacun est homme à sa manière
Pour servir Dieu comme un ami
\n
C’est dans le coeur des simples gens
Qu’il vient s’asseoir naturellement
Ceux qui accueillent sans condition
Qui ont le sourire en avant.

2.
Que tu sois meneur ou mené
Aux vues de notre société
Il y a toujours un horizon
Un printemps à faire éclater
\n
Les talents que tu as reçus
Un coffre d’or ou trois écus
Autant d’amour à fructifier
À faire valoir aux coins des rues.

3.
Chacun de nous est invité
À marcher vers la sainteté
Et tous les saints de notre histoire
Ont mis l’amour en primauté
\n
Le ciment qui unit nos pierres
Résiste au plus dur de l’hiver
L’amitié versée sans mesure
Devient semence pour la terre.

4.
Les pierres précieuses du pardon
Pourront servir aux fondations
Elles sont les bases pour bâtir
Pour s’élever et tenir bon
\n
Pour ceux qui ne le savent point
Il y a des pierres en tous terrains
Même si l’on doit creuser profond
Jamais personne ne cherche en vain.
