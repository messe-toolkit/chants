Kyrie, eleison

Seigneur Jésus, envoyé pour guérir
les coeurs qui reviennent vers toi :

Kyrie eleison
Kyrie eleison

O Christ, venu appeler les pécheurs :

Christe eleison
Christe eleison

Seigneur, qui sièges à la droite du Père
où tu intercèdes pour nous :

Kyrie eleison
Kyrie eleison

Gloria

Gloria in excelsis Deo
et in terra pax hominibus bonæ voluntatis.
Laudamus te,
benedicimus te,
adoramus te,
glorificamus te,
gratias agimus tibi propter magnam gloriam tuam,
Domine Deus, Rex cælestis,
Deus Pater omnipotens.
Domine Fili Unigenite, Jesu Christe,
Domine Deus, Agnus Dei, Filius Patris,
qui tollis peccata mundi, miserere nobis ;
qui tollis peccata mundi
suscipe deprecationem nostram,
Qui sedes ad dexteram Patris,
miserere nobis.
Quoniam tu solus Sanctus,
tu solus Dominus,
tu solus Altissimus,
Jesu Christe,
cum Sancto Spiritu,
in gloria Dei Patris,
Amen.

Alleluia

ALLELUIA, ALLELUIA,
ALLELUIA, ALLELUIA !

Jubilate Deo omnis terra,
servite Domino in lætitia.

Sanctus

Sanctus, Sanctus, Sanctus,
Dominus Deus Sabaoth.
Plenisunt cæli et terra gloria tua,
Hosanna, Hosanna, Hosanna in excelsis !

Benedictus qui venit in nomine Domini.
Hosanna, Hosanna, Hosanna in excelsis !

Pater noster

Pater noster, qui es in cælis,
sanctificetur nomen tuum ;
adveniat regnum tuum ;
fiat voluntas tua
sicut in cælo et in terra.
Panem nostrum quotidianum
da nobis hodie,
et dimitte nobis debita nostra,
sicut et nos dimittimus
debitoribus nostris,
et ne nos inducas in tentationem
sed libera nos a malo.
Amen.

Agnus Dei

Agnus Dei qui tollis peccata mundi,
miserere nobis, miserere nobis.

Dona nobis pacem, dona nobis pacem,
dona nobis pacem !