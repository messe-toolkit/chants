1.
On a tout à gagner
À faire la fête
On change de planète
Et de palier…
Regarde un peu la tête
Que tu fais…
Ma porte est grand’ ouverte
On se connaît…
Du bout des yeux seulement
C’est sûr,
On peut sauter maintenant
Les murs…
Viens, je t’invite !

R.
ET QUE LA FÊTE COMMENCE
GARE A L’INDIFFÉRENCE
VIVRE ENSEMBLE FAIT DANSER LA VIE
AUJOURD’HUI!
ET QUE LA FÊTE COMMENCE
GARE A L’INDIFFÉRENCE
VIVRE ENSEMBLE, QUOI DE PLUS URGENT
MAINTENANT!

2.
Je t’ai croisé souvent
Sur ton nuage,
Tu vis dans les étages
Avec le vent !
T’as l’air un peu sauvage
D’un gros chat,
Qui serait de passage,
Mais voilà…
Je suis venu t’inviter
Ce soir
J’ai préparé le dîner,
Viens voir
Là où j’habite !

3.
Déjà, les allumés
Du voisinage
Entament le partage des idées !
Des plats et des salades
Comme des fleurs
Inventent le langage
Des couleurs
Venus d’ailleurs ou d’ici
J’sais pas
Les anciens, les tout-petits
Sont là
Vite, tout l’monde à table !