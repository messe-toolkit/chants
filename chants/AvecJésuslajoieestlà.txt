AVEC JÉSUS, LA JOIE EST LÀ.
AVEC JÉSUS, LA JOIE EST LÀ.
AVEC JÉSUS, LA JOIE EST LÀ.
AVEC JÉSUS, LA JOIE EST LÀ.

1.
Il ne faut plus chercher ailleurs !
Jésus est venu apporter la joie à tous
et pour toujours.

2.
Il ne s’agit pas d’une joie seulement espérée
et reportée au paradis qui voudrait qu’ici,
sur terre, nous soyons tristes.