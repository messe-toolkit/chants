SEIGNEUR, QUI SÉJOURNERA SOUS TA TENTE ?

1.
Celui qui se conduit parfaitement,
qui agit avec justice
et dit la vérité selon son coeur.
Il met un frein à sa langue.

2.
Il ne fait pas de tort à son frère
et n’outrage pas son prochain.
À ses yeux, le réprouvé est méprisable
mais il honore les fidèles du Seigneur.

3.
Il ne reprend pas sa parole.
Il prête son argent sans intérêt,
n’accepte rien qui nuise à l’innocent.
Qui fait ainsi demeure inébranlable.