1.
Mais qu’est ce qui a pris à Dieu
De me vouloir tant de tendresse
Je n’ai rien de plus ni de mieux
C’est pourtant moi qui l’intéresse

R.
DE MON COEUR J’IRAI JUSQU’À DIEU
IL EST MA JOIE IL EST MA FÊTE
DIEU EST ÉTRANGE ET MERVEILLEUX
CHEZ LUI JE SUIS BIEN DANS MA TÊTE

2.
Je suis vraiment plein de questions
Je vivais dans l’indifférence
Pourquoi Dieu dans son affection
M’a-t-il donné sa préférence

3.
Mais pourquoi donc m’aimer ainsi
Je ne suis pas mieux que les autres
J’ai le coeur plutôt endurci
Et Dieu me choisit comme apôtre