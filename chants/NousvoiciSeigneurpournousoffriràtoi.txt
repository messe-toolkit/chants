NOUS VOICI, SEIGNEUR,
POUR NOUS OFFRIR À TOI.
PRENDS-NOUS,
TRANSFORME-NOUS,
FAIS DE NOUS CE QUE TU VOUDRAS
AFIN DE NOUS DONNER AU MONDE
COMME BON TE SEMBLERA.

1.
Offrir nos vies pour les nôtres,
Etre une bénédiction pour tous ceux qui nous entourent,
Illuminer chaque jour de ta présence divine.
Aimer et servir là où tu nous as semés.

2.
Offrir nos vies pour le monde,
Combattre toute injustice, annoncer une espérance,
Faire advenir ton royaume afin que règne la paix.
Aimer et servir là où tu nous dis d’agir.

3.
Offrir nos vies pour l’Eglise,
Bâtir ta maison, Seigneur, apporter chacun sa pierre,
En ne formant qu’un seul corps dans l’unité de l’Esprit.
Aimer et servir là où tu nous enverras.