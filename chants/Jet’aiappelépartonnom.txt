R.
Je t'ai appelé par ton nom.
Tu comptes beaucoup à mes yeux.
Tu es précieux pour moi
Car je t'aime.

1.
“Ainsi parle le Seigneur qui t’a créé,
Qui t’a formé dans le sein de ta mère :
“Je t’ai appelé par ton nom;
Tu es à moi ; je ne t’oublierai jamais”.

2.
“Tu traverses les eaux ; je suis avec toi.
Tu ne seras pas noyé ni submergé.
Dans l’épreuve je suis près de toi ;
Moi, ton ami, ton Dieu et ton Sauveur”.

3.
“Je t’aime tant. Tu as du prix à mes yeux.
Je t’ai gravé sur la paume de mes mains.
Ne crains pas, car je suis avec toi.
Le jour, la nuit, tout au long de ta vie”.