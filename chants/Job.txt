POURQUOI, POURQUOI, SEIGNEUR?
POURQUOI LE MALHEUR?

1.
Job était un homme, un ami de Dieu
Ayant, comme on dit, tout pour être heureux.
Il avait des terres, de nombreux troupeaux,
Des amis très nobles, des enfants très beaux.
Le temps d’un orage qui tombe des nues,
Dans une catastrophe, il a tout perdu.
Ses fils et ses filles ont été tués,
Toute sa fortune on lui a volé.

2.
Quand la maladie vient le terrasser
Ses amis lui disent pour le troubler :
“Dieu punit chacun selon ses péchés.”
Mais il leur répond sans se révolter :
“Il faut savoir vivre malgré le malheur
Comme l’on sait vivre avec le bonheur.
Le Seigneur, mon Maître, m’avait tout donné
Mais j’ai tout perdu ; son Nom soit loué !”

3.
Quand il souffre trop, il dit, excédé :
“Maudit soit le jour où ma mère m’a fait.”
Il ne comprend pas, ne voit plus très bien,
Il s’en prend à Dieu, mais Dieu ne dit rien.
Il en vient souvent à souhaiter la mort
Mais il réfléchit et, pris de remords,
Dit : “C’est pas possible ! Dieu n’est pas mauvais !
Je n’fais pas le poids et je veux juger.”

4.
“Oui, je sais que Dieu sera mon Sauveur
Et par Lui, un jour, je serai vainqueur,
Ma peau de misère sera desséchée
Mais c’est dans ma chair que je le verrai.
Mes yeux le verront, le contempleront,
Pas en étranger, en ami très bon.”
Et Dieu répondit : “Je t’ai éprouvé
Mais tu es fidèle, sois récompensé.”