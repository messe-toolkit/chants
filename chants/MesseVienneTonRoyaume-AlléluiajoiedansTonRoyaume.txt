R.
Alléluia, alléluia, joie dans ton royaume, nous marchons vers toi !
Alléluia, alléluia, vienne ta parole, nous croyons en toi !

1.
Bonne Nouvelle pour les pauvres, liberté pour les prisonniers.

2.
Bonne Nouvelle pour tout homme, lumière dans notre nuit.