I.
Malheureuse et battue par les vents,
Ô toi qui n'as pas enfanté,
Les montagnes peuvent s'écarter,
Les collines peuvent chanceler,
Mais Moi, Je te consolerai.

R.
Mon amour ne s'écartera pas de toi,
Mon alliance de paix ne chancellera pas.
Réjouis-toi : tu n'éprouveras plus de honte !
Ton Créateur est ton Époux,
Le Seigneur est son Nom !
\n
Mon amour ne s'écartera pas de toi,
Mon alliance de paix ne chancellera pas.
Réjouis-toi : tu n'éprouveras plus de honte !
Ton Créateur est ton Époux,
Le Seigneur est son Nom, Jérusalem !

1.
Crie de joie, Jérusalem !
Élargis l'espace de ta tente !
Ta race va repeupler la terre
Et nombreux seront tes enfants !
\n
Des saphirs seront tes fondations,
Tes créneaux seront faits de rubis ;
Tes portes et tes pierres
Seront en escarboucle,
Ton enceinte en pierres précieuses !

2.
Tu seras dans la justice,
Tu seras libre de l'oppression !
N'aie crainte, il n'y aura plus de frayeur
Et heureux seront tes enfants !
\n
Un instant, Je t'avais délaissée
Et furieux, J'avais caché ma face
Mais dans mon amour, J'ai eu pitié de toi ;
Jérusalem, clame ta victoire !

P.
Dans la joie, ô Sion, lève-toi !
Dans la paix, tu seras ramenée !
Devant toi, les collines et tous les monts
Crieront de joie,
Et la gloire du Seigneur grandira,
Étendard éternel qui ne périra.
