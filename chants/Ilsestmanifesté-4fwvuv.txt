R.
Il s’est manifesté,
Nous l’avons rencontré,
Venez et voyez !
Venu pour nous sauver,
Il est ressuscité,
Jésus est le Seigneur !

1.
Nos yeux l’ont reconnu
Et nos mains l’ont touché,
Nous avons entendu
La parole de vie.

2.
Vrai Dieu né du vrai Dieu,
Il a pris notre chair.
Jésus, le Fils de l’Homme
Nous conduit vers le Père.

3.
Envoyé par le Père,
Consacré par l'Esprit,
Jésus est la lumière
Qui nous donne la vie.

4.
Dieu nous a tant aimés
Qu’il a donné son Fils.
Non pas pour nous juger,
Mais pour nous racheter.

5.
Il est venu chercher
La brebis égarée,
Jésus, le Bon Berger,
Vient pour nous libérer.

6.
Celui qui croit en lui
A la vie éternelle.
Celui qui croit en lui
Marche dans la lumière.

7.
Vous tous qui avez soif,
Approchez-vous de lui,
Ouvrez grand votre cœur,
Recevez son Esprit.