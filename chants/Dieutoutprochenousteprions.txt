DIEU TOUT PROCHE, NOUS TE PRIONS,
TA PAROLE, NOUS L’ACCUEILLONS.
AU PLUS INTIME DE NOUS-MÊMES
TU TE RÉVÈLES UN DIEU QUI AIME.

1.
Comment, Seigneur, te rendre gloire avec nos lèvres
Si notre coeur est loin de toi ?
Viens nous guérir par tes paroles de Sagesse,
Nous voulons vivre de ta loi.

2.
Arrache-nous aux traditions qui emprisonnent,
Car tes chemins sont liberté.
Dans nos maisons tu fais souffler l’Esprit de force,
Il nous apprend ta vérité.

3.
À ton regard la main de l’homme est toujours pure
Quand elle s’ouvre à son prochain.
Ce don d’amour venu de toi nous transfigure,
Et nous prenons visage humain.