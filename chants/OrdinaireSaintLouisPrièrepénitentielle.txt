1.
Seigneur Jésus, envoyé par le
Père pour guérir et sauver les hommes,
Prends pitié de nous.
Kyrie eleison.

2.
Ô Christ, venu dans le monde
Appeler tous les pécheurs,
Prends pitié de nous.
Christ eleison.

3.
Seigneur Jésus, élevé dans la gloire du Père,
Où tu intercèdes pour nous,
Prends pitié de nous,
Kyrie eleison.