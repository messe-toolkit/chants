R.
Vers Toi notre louange s’élèvera sans fin car il n’est
point d’offrande qui ne soit reçue de Toi Père Saint.

1.
Ce pain pour notre faim, fruit du sel pétri de nos mains.
Le corps ressuscité de Jésus ton Fils Bien Aimé.

2.
Ce vin pour notre joie feu, jailli du grain que l’on broie.
Le sang qui fut versé pour les noces d’éternité.