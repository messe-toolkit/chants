R.
PRENDS TA SOURCE À L’ÉVANGILE,
LÈVE-TOI PARMI TES FRÈRES,
DIEU T’APPELLE AUJOURD’HUI
À PARTAGER L’EAU VIVE.

1.
Au désert de ta vie coule une source…
Réveille en toi le feu de ton baptême!
Que s’éclairent toutes tes nuits !
Au désert de ta vie coule une source…
Sois signe d’Évangile, lumière pour tes frères !

2.
Au désert de ton coeur jaillit l’Espérance…
Réveille en toi l’Esprit de ton baptême!
Que grandisse ta liberté !
Au désert de ton coeur jaillit l’Espérance…
Sois terre d’Évangile, lumière pour tes frères !

3.
Au désert de tes jours souffle une brise…
Réveille en toi la vie de ton baptême!
Que paraisse le jour nouveau !
Au désert de tes jours souffle une brise…
Sois chemin d’Évangile, lumière pour tes frères !