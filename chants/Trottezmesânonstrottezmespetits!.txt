R.
TROTTEZ MES ANONS
TROTTEZ MES PETITS !
LE CHEMIN EST LONG
JE CHANGE DE VIE
TROTTEZ MES ANONS !
TROTTEZ MES PETITS !

1.
Dans le brouillard qui moutonne
D'un joli matin d'automne
Tiré par deux beaux ânons
Je quitte mes compagnons
Et descends par le sentier
Qui va chez le menuisier

2.
Et tout au long de la route
Mon coeur n'a pas un seul doute
Je me sens tout réjoui
J'imagine une autre vie
Mon rêve réalisé
Par les mains du menuisier

3.
Dents de scie, gouge et varloppe
Me mordent au fond de l'échoppe
Et me transforment bientôt
En mangeoire pour animaux
Rempli de paille et de foin
Mon beau rêve est déjà loin

DERNIER REFRAIN
PLEUREZ MES ANONS !
PLEUREZ MES PETITS !
JE TOUCHE LE FOND
EN PERDANT MA VIE
PLEUREZ MES ANONS !
PLEUREZ MES PETITS !

RECITANT
Après deux jours et deux nuits de voyage
le deuxième arbre allait enfin se retrouver sur les galets gris du chantier naval.
Les cris aigus des mouettes les parfums salés de l'océan lui tournaient déjà la tête
Il ne pouvait pas encore se douter de la mauvaise surprise qui l'attendait...
Pas un seul armateur n'avait passé commande pour un trois-mâts...