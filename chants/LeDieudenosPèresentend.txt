« Le Dieu de nos pères entend nos appels. »
Passés de la servitude au service,
les descendants de l’Araméen vagabond
proclament les merveilles de Dieu
dans le pays où ruisselle l’abondance.
« Du pays d’Égypte, il nous a fait sortir ! »
Passé du Jourdain au désert,
Jésus, nouvel Adam, rempli de l’Esprit,
affronte les pièges du Malin
dans le jardin de l’épreuve et de la faim.
« Tu es mon refuge, mon Dieu dont je suis sûr ! »
Il vient, notre Libérateur :
la Parole est dans sa bouche,
la Parole est dans son coeur.
L’Adversaire le met à l’épreuve.
« Tu ne tenteras pas le Seigneur ton Dieu ! »
Le pain sera partagé pour la multitude,
le pouvoir sera donné au Fils par le Père,
et le Messie souffrira pour entrer avec lui dans la gloire.
« Le Seigneur est avec lui dans l’épreuve ! »
Au temps fixé pour l’ultime combat,
la Ville où se dresse le Temple de Dieu
accueillera le Messie, fils d’Adam, fils de Dieu !
« Dieu le ressuscitera d’entre les morts ! »