1.
Rends-moi fidèle, Seigneur,
À ce fil d'espérance
Et ce peu de clarté
Qui suffirent pour chercher.

2.
Rends-moi fidèle, Seigneur,
À ce vin de ta coupe
Et ce pain quotidien
Qui suffirent pour marcher.

3.
Rends-moi fidèle, Seigneur,
À ce brin d'allégresse
Et ce goût du bonheur
Qui suffisent pour chanter.

4.
Rends-moi fidèle, Seigneur,
À ton nom sur mes lèvres
Et ce cri de la foi
Qui suffisent pour veiller.

5.
Rends-moi fidèle Seigneur,
À l'accueil de ton souffle
Et ce don sans retour
Qui suffisent pour aimer.