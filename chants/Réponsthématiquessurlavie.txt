37.
Refrain : Jésus, tu es le pain de Dieu,
* tu nous donnes la vie.
verset : Tu es la manne des temps nouveaux. *
doxologie : Gloire au Père et au Fils et au Saint Esprit.
38
Refrain : En toi est la source de vie,
* par ta lumière nous voyons la lumière.
verset : Seigneur, qu’il est précieux ton amour. *
doxologie : Gloire au Père, au Fils, au Saint Esprit.
39
Refrain : Conduis-nous, Seigneur, aux sources de la vie.
verset : Mon âme a soif de Dieu, le Dieu vivant.
doxologie : Gloire au Père et au Fils et au Saint Esprit.
40
Refrain : Seigneur mon Dieu, mon espérance,
* fais-moi vivre ! car je compte sur toi !
verset : Ton amour a fait pour moi des merveilles. *
doxologie : Gloire au Père et au Fils et au Saint Esprit.