R.
Hic Christus adoratur et passcitur

1.
Jésus Seigneur de la Création, ressurgi de la vallée des morts
Saint Bernard, tes yeux purs l'ont vu, où tout faisait croire à son absence
Dans un pays inhospitalier où régnaient en maîtres les démons
Un apôtre est venu planter la croix en signe de l'amour vainqueur

2.
Coeur baptisé, rempli de l'Esprit, tu as reconnu Jésus présent
Dans le pauvre qui avait froid, plongé dans la sombre nuit de l'abandon
Désormais sur ce passage, une maison se dresse comme un signe
Habitée par des frères, unis dans l'adoration de leur Seigneur

3.
Les homm'en quête d'un vrai bonheur éperdument prennent la route
Mais un jour, osant s'arrêter, ils sont accueillis comme des fils de Dieu
Identifié aux plus malheureux, Jésus nous fait la grâce de mendier
Un peu de temps et d'amitié, prémices du jour où nous serons comblés

4.
Sur la montagne notre louange, au nom de ceux qui sont dans l'ignorance
Notre Père, nous te chantons devant la splendeur de la Création
Ton amour nous a faits pèlerins à la suite de Jésus, ton Fils
Dans l'Esprit, ton Souffle de vie, de cette terre en ton Royaume.