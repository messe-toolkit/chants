Antienne
TOI QUI ES BON,
TOI QUI ES BON ET QUI PARDONNES,
ÉCOUTE-MOI, MON DIEU!

1.
Toi qui es bon et qui pardonnes,
plein d’amour pour tous ceux qui t’appellent,
écoute ma prière, Seigneur,
entends ma voix qui te supplie.

2.
Toutes les nations, que tu as faites,
viendront se prosterner devant toi,
car tu es grand et tu fais des merveilles,
toi, Dieu, le seul.

3.
Toi, Seigneur, Dieu de tendresse et de pitié,
lent à la colère, plein d’amour et de vérité,
regarde vers moi,
prends pitié de moi.