R.
HEUREUX QUI RÈGLE SES PAS
SUR LA PAROLE DE DIEU.

1.
Heureux les hommes intègres dans leurs voies
qui marchent suivant la loi du Seigneur !
Heureux ceux qui gardent ses exigences,
ils le cherchent de tout coeur !

2.
Toi, tu promulgues des préceptes
à observer entièrement.
Puissent mes voies s’affermir
à observer tes commandements !

3.
Sois bon pour ton serviteur, et je vivrai,
j’observerai ta parole.
Ouvre mes yeux,
que je contemple les merveilles de ta loi.

4.
Enseigne-moi, Seigneur, le chemin de tes ordres ;
à les garder, j’aurai ma récompense.
Montre-moi comment garder ta loi,
que je l’observe de tout coeur.