FAITES COMME MOI, AUJOURD’HUI,
PRENEZ SOIN DE VOS AMIS!
AIMEZ-VOUS LES UNS LES AUTRES
CHAQUE JOUR DE VOTRE VIE !

Avant d’être arrêté par les soldats,
avant de mourir sur la croix,
tu as lavé les pieds de tes amis
et tu leur as dit ce jour-là :