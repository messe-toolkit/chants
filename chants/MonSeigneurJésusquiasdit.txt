Mon Seigneur Jésus qui as dit:
« Personne n’a un plus grand amour
que celui qui donne sa vie pour ses amis »,
je désire de tout mon cœur
donner ma vie pour toi,
je te le demande instamment.
Toutefois non ma volonté
mais la tienne.
Je t’offre ma vie,
fais de moi ce qui te plaira le plus.