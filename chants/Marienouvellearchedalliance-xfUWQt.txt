R.
Marie, nouvelle arche d’alliance,
Tu nous ouvres la porte du ciel.

1.
Par toi, Dieu réside parmi nous.
Ô toi, Marie comblée de grâce.
Couverte de l’ombre du Seigneur,
Pour nous, tu as porté Jésus-Christ.

2.
Marie, notre mère immaculée,
Tu fus gardée de toute faute.
D’avance, oui Dieu t’avait choisie,
Pour être la mère de son Fils.