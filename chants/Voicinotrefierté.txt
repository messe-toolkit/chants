VOICI NOTRE FIERTÉ,
LA CROIX DU BIEN-AIMÉ !
VOICI L’ARBRE DE VIE,
LA CROIX DE JÉSUS-CHRIST !

1.
Devant ce bois,
Séparant les eaux de morts,
FLÉCHISSONS LE GENOU !
Devant Jésus,
Nous offrant la liberté,
FLÉCHISSONS LE GENOU !

2.
Devant ce bois,
Révélant le don de Dieu,
FLÉCHISSONS LE GENOU !
Devant Jésus,
Dévoilant son grand amour,
FLÉCHISSONS LE GENOU !

3.
Devant ce bois,
Dénonçant toute douleur,
FLÉCHISSONS LE GENOU !
Devant Jésus,
Rassemblant, les bras ouverts,
FLÉCHISSONS LE GENOU !

4.
Devant ce bois,
Nous montrant le vrai chemin,
FLÉCHISSONS LE GENOU !
Devant Jésus,
Le Pasteur qui nous conduit,
FLÉCHISSONS LE GENOU !