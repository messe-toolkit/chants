Joyeuse Lumière de la sainte gloire du Père céleste immortel,
Saint et bienheureux Jésus christ !

1.
Venant au coucher du soleil, contemplant la lumière du soir,
Nous chantons le Père et le Fils, et le Saint-Esprit de Dieu. R/

2.
Digne es-tu en tout temps d'être loué par de saintes voix,
Fils de Dieu qui donnes la vie : aussi le monde te glorifie. R/