R.
UNE SOURCE COULE EN TOI,
UNE SOURCE CHANTE EN TOI,
EN TOI LE PERE MURMURE SON AMOUR

1.
Caché en toi, le souffle est prière,
Cachée en toi, la source est silence,
Elle enchante la terre
De sa musique.

2.
Caché en toi, le fleuve est promesse,
Cachée en toi, la source est puissante,
Elle irrigue la terre
De son eau vive.

3.
Cachée en toi, la flamme est vivante,
Cachée en toi, la source est lumière,
Elle habille la terre
De ses merveilles.