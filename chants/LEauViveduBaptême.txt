R.
L'EAU VIVE DU BAPTEME
SUR NOS LEVRES ET SUR NOS FRONTS,
L'EAU VIVE DU BAPTEME
A GRAVE EN NOUS TON NOM.
NOUS SOMMES UN PEUPLE NÉ DE TOI,
MARQUÉ DU SIGNE DE TA CROIX,
TA CROIX, SIGNE D'AMOUR.

1.
Pour aller de l'ombre à la lumière
Et passer de la mort à la vie,
Pour aller de l'ombre à la lumière
Nous avons fait confiance à l'Esprit.

2.
Pour aller profond dans ta parole
Et passer de la peur à l'amour,
Pour aller profond dans ta parole,
Nous avons pris ta route au grand jour !

3.
Pour aller plus près du coeur des hommes
Et passer du désert au Jourdain
Pour aller plus près du coeur des hommes,
Nous avons mis nos pas dans les tiens !