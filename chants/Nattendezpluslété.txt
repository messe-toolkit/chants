R.
N’attendez plus l’été, le printemps ni l’automne,
Comme chacun le sait, il n’y a plus de saison.
Ne devenez jamais une grande personne
C’est bien plus qu’une prière, c’est une accusation.

1.
Notre société n’a-t-elle qu’la jeunesse qu’elle mérite,
Fracture-impunité et zone de non-droit
Quand ils n’savent plus pourquoi la violence les habite
Quand même les politiques enfreignent aussi la loi.
Quand la tôle, la drogue, les règlements de compte
Deviennent un art de vivre, une façon d’exister
Quand dans leur Bronx, la haine a remplacé la honte
Repères et valeurs sont perdus à jamais.

R.
N’attendez plus l’été, le printemps ni l’automne,
Comme chacun le sait, il n’y a plus de saison.
Ne devenez jamais une grande personne
C’est bien là ma prière et mon indignation.

2.
Hooligans-loft story, les “raves”… et j’en passe…
Notre monde aujourd’hui est-il devenu fou ?
Et pourtant, mon ami, je veux avoir l’audace
De croire en l’avenir et de vivre debout.
Je veux être de ceux qui parlent aux étoiles,
Qui mettent un soleil au cœur de leurs chansons
Et nos enfants riront de la légende noire,
Où pleure un solitaire qui n’a plus d’horizon.

R.
Re-découvrons l’été, le printemps et l’automne,
Il y a dans nos projets chacune des saisons ;
Il y a même un enfant dans nos grandes personnes
C’est bien là, mon ami, le sens de ma chanson.

DERNIER REFRAIN :
Réinventons l’été, le printemps et l’automne,
Il y a dans nos désirs chacune des saisons.