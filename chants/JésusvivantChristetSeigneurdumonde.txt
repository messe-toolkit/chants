R.
Jésus vivant, Christ et Seigneur du monde,
Tu as brisé les portes du tombeau.
A pleine voix nos chants te répondent :
Tu fais lever les temps nouveaux.
Joie sur terre, paix aux hommes !
Dieu nous a sauvés de la mort.
Joie sur terre, paix aux hommes !
Dieu fait jaillir la vie !

1.
Quelle Pâque a resplendi ?
Quel témoin va nous parler ?
Fais-nous croire aujourd’hui à la voix qui dit :
Christ est ressuscité !

2.
Nos chemins vers Emmaüs,
Toi, Jésus, tu les connais.
Viens nous dire à la table du pain rompu :
Christ est ressuscité !

3.
Par le chant de ton Esprit
Tu nous parles au plus secret,
Et le feu de tes mots nous faire dire aussi :
Christ est ressuscité !