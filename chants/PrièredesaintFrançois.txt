R.
Seigneur, Seigneur,
Fais de moi un instrument de Ta paix.

1.
Où est la haine, que je mette l’Amour.
Où est l’offense, que je mette le Pardon.

2.
Où est la discorde, que je mette l’Union.
Où est l’erreur, que je mette la Vérité.

3.
Où est le doute, que je mette la Foi.
Où est le désespoir, que je mette l’Espérance

4.
Où sont les ténèbres, que je mette la Lumière
Où est la tristesse, que je mette la joie.