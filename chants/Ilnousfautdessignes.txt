R.
Vous êtes les témoins de sa lumière
Votre confiance en lui bouscule notre vie
Vous êtes des flambeaux qui nous éclairent.
Votre confiance en lui bouscule notre vie.

1.
Il nous faut des signes
Pour ouvrir nos yeux
À ce qui désigne
Les chemins de Dieu
Vous êtes les signes
Qu’il est présent,
Vous êtes les signes du Dieu vivant.

2.
Il faut des prophètes
Pour nous réveiller
Quand la peur nous jette
Dans l’obscurité
Vous êtes des prophètes
Sur nos chemins
Vous êtes des prophètes
Du Dieu qui vient.