GLOIRE À DIEU, AU PLUS HAUT DES CIEUX,
ET PAIX SUR LA TERRE AUX HOMMES QU’IL AIME.

1.
Nous te louons, nous te bénissons, nous t’adorons.
Nous te glorifions, nous te rendons grâce, pour ton immense gloire,
Seigneur Dieu, Roi du ciel, Dieu le Père tout-puissant.

2.
Seigneur, Fils unique, Jésus Christ,
Seigneur Dieu, Agneau de Dieu, le Fils du Père ;
Toi qui enlèves le péché du monde, prends pitié de nous ;
Toi qui enlèves le péché du monde, reçois notre prière ;
Toi qui es assis à la droite du Père, prends pitié de nous.

3.
Car toi seul es saint, Toi seul es Seigneur,
Toi seul es le TrèsHaut : Jésus Christ, avec le Saint-Esprit
dans la gloire de Dieu le Père. Amen.