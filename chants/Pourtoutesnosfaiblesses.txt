SEIGNEUR, PRENDS PITIÉ,
PRENDS PITIÉ DE NOUS,
Ô CHRIST, PRENDS PITIÉ,
PRENDS PITIÉ DE NOUS.

1.
Pour toutes nos faiblesses
Dans nos actes qui blessent,
Seigneur, prends pitié de nous.
Pour nos incertitudes,
Nos longues lassitudes,
Ô Christ prends pitié de nous.

2.
Pour nos silences coupables,
Nos élans confortables,
Seigneur, prends pitié de nous.
Pour l’amour qui vacille,
Le goût de ce qui brille,
Ô Christ prends pitié de nous.

3.
Pour trop souvent nous taire
Jusque dans nos prières,
Seigneur, prends pitié de nous.
Pour nos désespérances
Qui oublient ta présence,
Ô Christ prends pitié de nous.

Afeto kpo nublanui na mi
Kristo, kpo nublanui na mi