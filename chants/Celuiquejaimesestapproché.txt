Celui que j’aime s’est approché,
Quand s’éveillait mon âme ;
Le soleil au loin
Franchissait les collines,
Quelques pas près du treillage
M’invitaient à le suivre.

Que tu es belle, ma bien-aimée,
Quand resplendit l’aurore !
Ton jardin est clos
Mais les lis y embaument :
Le vent porte en son sillage
Les parfums les plus rares.

Avant que j’ouvre, il s’est enfui,
Il a blessé mon âme ;
Dans les rues, la nuit,
J’ai cherché son visage :
Dans les champs et dans la ville
Ne fuyait que la brise.

Que tu es belle, ma bien-aimée,
Que ton amour m’attache !
J’ai guetté ton pas
Dans l’enclos de ma vigne :
Les colombes y viennent boire
Quand les ombres s’inclinent.
Vers la lumière je suis montée,
Et j’ai brûlé mon âme ;
Quand le soir survint,
J’étais loin de l’atteindre :
Les chansons de mes compagnes
Sont l’écho de ma plainte.

Vers la lumière, ma bien-aimée,
Tu guideras leur course ;
Je viendrai vers toi
Au jardin de la myrrhe,
Désormais tu seras source,
Je serai ton eau vive.