R.
Ne crains pas, ne crains pas,
Je t’ai racheté.
Ne crains pas, ne crains pas,
Je suis avec toi.

1.
Mon enfant bien-aimé,
Tu es devant moi,
Je suis ton Seigneur Dieu,
Créateur et Sauveur.

2.
J’aime à te regarder,
Tu es toute ma joie,
Je suis ton Seigneur Dieu,
Rédempteur et Sauveur.

3.
Parce que je t’aime,
Tu es dans ma main,
Je suis ton Seigneur Dieu,
Tendresse et amour.

4.
Ceux qui portent mon nom,
Ceux que j’ai formés,
Sont mes œuvres à jamais,
Sont toute ma gloire.