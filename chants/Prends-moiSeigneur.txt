1.
Prends-moi, Seigneur,
dans la richesse divine de ton silence,
plénitude capable de tout combler en mon âme.

2.
Fais taire en moi ce qui n’est pas Toi,
ce qui n’est pas Ta présence
toute pure,
toute solitaire,
toute paisible.

3.
Impose même ton silence à ma prière,
pour qu’elle soit pur élan vers Toi.
Fais descendre ton silence
jusqu’au fond de mon être,
et fais remonter ce silence
vers Toi, en hommage d’amour !