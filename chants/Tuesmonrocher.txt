R.
TU ES MON ROCHER,
MA CITADELLE,
SEIGNEUR, MON APPUI.
TU ES MON ROCHER,
MA FORTERESSE,
SEIGNEUR, ME VOICI.

1.
En toi ma confiance,
Dieu de la vie.
En toi l’espérance,
Me voici.

2.
J’ai du prix pour toi,
Dieu de la vie.
Tu comptes sur moi,
Me voici.

3.
Je ne crains plus rien,
Dieu de la vie.
Tu es mon soutien,
Me voici.

4.
Tu m’ouvres les bras,
Dieu de la vie.
Je me donne à toi,
Me voici.