R.
De nos mains, mes amis,
Nous bâtirons un pont,
Et nous passerons la rivière
De nos vies, mes amis,
Jaillissent d’autres ponts,

1.
Voici le temps où l’homme espère.
Entre les rives du fleuve,
Se dressent les arches d’un pont
Entre nos peurs de faire peau neuve,
Nous bâtirons un pont,
Un pont, comme une envie de croire ;
Un pont, comme un nouvel espoir.

2.
Voici venir une lumière.
Entre montagnes et collines,
Se dressent les arches d’un pont
Entre le rite et la routine
Nous bâtirons un pont,
Un pont, jeté sur le passé ;
Un pont, un défi à lancer.

3.
Voici l’alliance vers la terre.
Entre remparts entre murailles,
Se dressent les arches d’un pont
Entre nos cris nos feux de paille ;
Nous bâtirons un pont,
Un pont, une main qui se tend ;
Un pont, une main qui se prend.

4.
Voici l’Esprit qui régénère.
Entre le ciel et notre terre,
Se dressent les arches d’un pont
Entre tes rêves, toi mon frère,
Nous bâtirons un pont,
Un pont, où s’unissent nos voix ;
Un pont, un pont tendu vers toi.

5.
Voici celui qui nous libère.