R.
AMOUR QUE J'AI TRAHI,
TOI MON DIEU TU ME CONNAIS,
AMOUR QUI M'AS SUIVI,
TON ALLIANCE EST A JAMAIS,
DIEU D'ETERNELLE PAIX !

1.
D'un grand amour tu m'as aimé,
moi qui cherchais des amours folles.
Je n'entends plus ta voix monter,
mon coeur est sourd à ta parole.
Fontaine pour ton peuple,
fais refleurir mes amandiers.

2.
Comme l'épouse qui revient,
je sais, mon Dieu, que tu pardonnes.
Après la honte, après la faim,
je vois la vie que tu me donnes.
Tendresse pour ton peuple,
tu m'as remis sur ton chemin.

3.
Dans le désert tu m'as parlé ;
brûlé de soif je veux renaître.
Soleil et pluie vont redonner
le blé, le vin et l'huile fraîche.
Jeunesse pour ton peuple,
par toi ma terre est fécondée.