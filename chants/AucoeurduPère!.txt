SEIGNEUR JÉSUS, CHEMIN DE VIE,
AU COEUR DU PÈRE, TU NOUS CONDUIS.
QUE TON ESPRIT, SOUFFLE DE VIE,
FASSE DE NOUS DES FILS UNIS !

1.
Enfant du Père des lumières
Entre ses mains, par Toi, remis,
Confiants et devenus prière,
Sans fin nous lui disons “merci”.

2.
Reflets de la beauté du Père,
Au coeur d’un monde bouleversé,
Ton Esprit nous rend solidaires
Du cri des humains dispersés !

3.
Témoins, en Toi, de la Tendresse
Du coeur qui nous a tant aimés,
Nous allons vivre avec nos frères
Son Amour pour l’humanité !