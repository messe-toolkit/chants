1.
Nos enfants ne sont pas nos enfants : Ils sont l’appel à être eux-mêmes,
Ils renaîtront de nos “ je t’aime ”, en eux ils portent leur chemin.
Il leur faut tout parce qu’ils n’ont rien.
Ils ne sauront que ce qu’ils vivent,
Nous serons chez eux des convives, nos enfants.
Ils ne sauront que ce qu’ils vivent,
Nous serons chez eux des convives, nos enfants.

2.
Nos enfants ne sont pas nos enfants,
Ils se faufilent un passage
Entre nos masques et nos visages.
Nous ne faisons que nos devoirs,
En leur donnant notre savoir.
Ils écriront leur propre histoire,
Qu’ils légueront à nos mémoires, nos enfants.

3.
Nos enfants ne sont pas nos enfants,
Génération si décapante,
Aussi fragile qu’insouciante,
Nous serons pour eux un appui ;
Pas d’amour vrai sans vrais conflits,
Nous mériterons leur confiance,
Et nous libérerons leurs chances, nos enfants.

4.
Nos enfants ne sont pas nos enfants,
Et rappelons-nous à leur âge,
Si isolés dans cette cage,
Nous rêvions de grands horizons.
Généreuses étaient nos chansons,
Nos révoltes et nos idées folles ;
Alors, donnons-leur la parole, nos enfants