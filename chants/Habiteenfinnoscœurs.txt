R.
Habite enfin nos cœurs des silences de ta parole,
La présence de ton amour fera naître la paix !
Habite enfin nos cœurs des silences de ta parole,
La présence de ton amour fera naître la paix !

1.
Tu es celui que mon cœur cherche
Mon âme a soif de toi
Seigneur remplis ma vie
De ce feu qui ne s'éteint pas !

2.
En toi nous avons vu l'amour
Et nous y avons cru
Seigneur fais de ma vie
Le visage de ton amour !

3.
Si vous êtes là en mon nom
Je suis au milieu de vous
Seigneur ouvre nos vies
Aux passages de ton amour !