UNIS AU PÈRE, SOEURS ET FRÈRES
D’UN SEUL COEUR FORMÉ PAR LA PRIÈRE
DANS LE SAINT-ESPRIT PAR JÉSUS CHRIST
NOUS RECEVONS TOUT SON AMOUR POUR TOUJOURS

1.
Seigneur, je n’ai pas le coeur fier
ni le regard ambitieux ;
je ne poursuis ni grands desseins,
ni merveilles qui me dépassent.

2.
Non, mais je tiens mon âme
égale et silencieuse ;
mon âme est en moi comme un enfant,
comme un petit enfant contre sa mère.

3.
Garde-moi, mon Dieu :
j’ai fait de toi mon refuge.
Tu m’apprends le chemin de la vie :
devant ta face, débordement de joie !