R.
Alléluia !

1.
Le salut, la puissance,
la gloire à notre Dieu,
Alléluia !
Ils sont justes, ils sont vrais,
ses jugements.
Alléluia !

2.
Célébrez notre Dieu,
serviteurs du Seigneur,
Alléluia !
vous tous qui le craignez,
les petits et les grands.
Alléluia !

3.
Il règne, le Seigneur,
notre Dieu tout-puissant,
Alléluia !
Exultons, crions de joie,
et rendons-lui la gloire !
Alléluia !

4.
Car elles sont venues
les Noces de l’Agneau,
Alléluia !
Et pour lui son épouse
a revêtu sa parure.
Alléluia !