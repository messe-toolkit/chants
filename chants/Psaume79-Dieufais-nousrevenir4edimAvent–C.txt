DIEU, FAIS-NOUS REVENIR ;
QUE TON VISAGE S’ÉCLAIRE,
ET NOUS SERONS SAUVÉS !

1.
Berger d’Israël, écoute,
resplendis au-dessus des Kéroubim !
Réveille ta vaillance
et viens nous sauver.

2.
Dieu de l’univers, reviens !
Du haut des cieux, regarde et vois :
visite cette vigne, protège-la,
celle qu’a plantée ta main puissante.

3.
Que ta main soutienne ton protégé,
le fils de l’homme qui te doit sa force.
Jamais plus nous n’irons loin de toi :
fais-nous vivre et invoquer ton nom !