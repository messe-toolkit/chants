1.
Dieu n’est pas dans le tremblement
Des volcans en colère
Dans le terrible grondement
Du tonnerre, des éclairs

PAS, PAS, PAS DANS L’OURAGAN
COMME ÉLIE L’AVAIT PRÉDIT
PAS, PAS, PAS DANS L’OURAGAN
COMME ÉLIE NOUS L’AVAIT DIT

2.
Dieu devient brise légère
Qui te frôle en passant
Et dans les choses bien ordinaires
C’est là qu’Il est Vivant

3.
Dieu, tu vois, ne s’impose pas
Se fait petit enfant
Mais sa Parole à chaque pas
Te dit qu’Il est Présent