Et arrivèrent près de lui,
la mère et les frères de Jésus.
Ils ne pouvaient le rencontrer
à cause de la foule.
On lui annonça :
« Ta mère et tes frères se tiennent dehors.
Ils veulent te voir. »
Jésus répondant leur dit :
« Ma mère et mes frères sont ceux qui entendent
et qui font la Parole de Dieu. »