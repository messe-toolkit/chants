Voici le temps de chanter Dieu,
de découvrir dans l’univers
sa gloire et sa beauté.
(Voici le temps de chanter Dieu ! : Tous, facultatif)
Voici le temps de dire au monde
quelle parole nous rassemble,
voici le temps de vivre en frères.
(Voici le temps de chanter Dieu ! : Tous, facultatif)
Célébrons aujourd’hui notre Pâque,
et nous serons la voix qui s’émerveille

LE SEIGNEUR EST BON !
ÉTERNEL EST SON AMOUR !

1.
Acclamez le Seigneur, terre entière,
servez le Seigneur dans l’allégresse,
venez à lui avec des chants de joie !

2.
Reconnaissez que le Seigneur est Dieu
il nous a faits, et nous sommes à lui,
nous, son peuple, son troupeau.

3.
Venez dans sa maison lui rendre grâce,
dans sa demeure chanter ses louanges ;
rendez-lui grâce et bénissez son nom !