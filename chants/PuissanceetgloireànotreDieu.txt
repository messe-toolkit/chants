R.
PUISSANCE ET GLOIRE À NOTRE DIEU,
MAÎTRE DE L’HISTOIRE ET SEIGNEUR DE LA VIE !
PUISSANCE ET GLOIRE À NOTRE DIEU,
ALLÉLUIA, ALLÉLUIA ! (bis)

1.
Rendons à César ce qui est à César,
Il tient le pouvoir de bâtir la cité.
Mais César n’est pas seul, nous avons notre part,
L’avenir de la terre à tout homme est confié.

2.
Rendons au Seigneur ce qui est au Seigneur :
À lui notre monde et la marche des temps.
Mais la graine est jetée par la main du semeur,
Dieu demande à chacun de nourrir les vivants.

3.
César est choisi pour servir le pays,
Qu’il offre à son peuple le pain de l’espoir !
Qu’il écoute le pauvre et réponde à son cri ;
Au plus dur des combats, qu’il nous montre la voie !

4.
Rendons au Seigneur ce qui est au Seigneur :
La vie qu’il nous donne est pour lui un trésor.
Au soleil de la Pâque elle prend des couleurs,
Jésus Christ la libère des nuits de la mort.

5.
Sur terre où parfois des César se croient Dieu
Portons la nouvelle d’un Christ aux mains nues.
Que rayonne sa gloire au profond de nos yeux !
L’effigie du Sauveur, qu’elle soit reconnue !

6.
Il marche avec nous, le Seigneur des seigneurs !
Comment ne pas voir tout le bien qu’il nous fait ?
Le merci qu’il attend, c’est l’amour de nos coeurs
Et la soif de bâtir la justice et la paix.