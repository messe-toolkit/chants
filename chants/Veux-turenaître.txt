1.
Veux-tu renaître d’un vrai repentir ?
Ne crains pas de t’ouvrir
À la brûlure de l’Esprit,
Et tes cendres précaires
Deviendront braise
Pour t’éprendre d’un Dieu
Qui embrase la terre.

2.
Veux-tu revivre par lui et guérir ?
Ne crains pas de t’offrir
Au geste ferme du Potier :
En ses mains ta poussière
Deviendra glaise,
L’oeuvre même du Dieu
Qui façonne les siècles.

3.
Veux-tu que germe ta gloire à venir ?
Ne crains pas de mourir
Au monde ancien, aux vains désirs,
Et la grâce nouvelle
Deviendra sève
Pour qu’à l’heure de Dieu
éclose ta lumière.