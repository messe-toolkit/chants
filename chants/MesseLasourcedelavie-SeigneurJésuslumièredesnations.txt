Seigneur Jésus,
Lumière des nations annoncée par les prophètes,
Kyrie eleison, Kyrie eleison.

Ô Christ,
Agneau de Dieu désigné par Jean Baptiste,
Christe eleison, Christe eleison.

Seigneur Jésus,
Verbe fait chair enfanté par Marie,
Kyrie eleison, Kyrie eleison.