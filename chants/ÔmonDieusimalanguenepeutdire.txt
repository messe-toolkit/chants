Ô mon Dieu, si ma langue ne peut dire
à tout moment que je t’aime,
du moins je veux que mon coeur
te le répète autant de fois que je respire.