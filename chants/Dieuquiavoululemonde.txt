R.
DIEU QUI A VOULU LE MONDE
DIEU DES COMMENCEMENTS
QUE NOUS DEVANCE LA COLOMBE
SUR TOUS LES CHEMINS DES VIVANTS

1.
Comme un père, comme un père
Tu nous as tiré de la nuit
Comme un père, comme un père
Tu nous as donné la vie
Tu nous as créés, inachevés
c'est le prix de notre liberté

2.
Comme un père, comme un père
Se découvre par ses enfants
Comme un père, comme un père
Heureux des balbutiements
Tu es bien présent à nos côtés
Quand s’éveille notre liberté

3.
Comme un père, comme un père
Sourit aux progrès de l’enfant
Comme un père, comme un père
Souffre au jour de l’accident
Tu n’as que tes bras à proposer
Tu les of fres à notre liberté.

4.
Tu es Père, tu es Père
Nous sommes tes "fils bien-aimés"
Tu es Père, tu es Père
Et nous voulons te chanter
Vois nos mains levées comme un appel
Chacune est un rayon d’arc-en-ciel.