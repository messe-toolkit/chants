JE SUIS UN DROMADAIRE,
JE PEUX MARCHER LONGTEMPS
SOUS LE SOLEIL ARDENT
DU DESERT.
JE SUIS UN DROMADAIRE,
JE M’APPELLE ALIBER,
CE QUI M’EST ARRIVÉ
EST EXTRAORDINAIRE.

1.
Il est venu d’on ne sait où,
Il m’a passé la corde au cou,
Alors je l’ai suivi
À travers le pays,
Il est mon Maître,
C’est un poète,
Il s’est laissé guider
Par une étoile !

2.
J’ai vu Palmyre et ses palmiers,
C’est là que nous avons croisé
Deux mages un peu bizarres
Qui cherchaient dans le noir
Une lumière
Qui les éclaire !
Tous les trois sont partis
Suivre l’étoile !

3.
Nos maîtres cherchaient un bébé,
Un enfant-Roi tout juste né
Près de Jérusalem,
Au coeur de Bethléem
-C’est incroyable,-
Dans une étable,
Que nous l’avons trouvé
Grâce à l’étoile !

4.
Près des moutons et des bergers,
Nous approchons pour déposer
Jusqu’aux pieds de l’enfant
L’or, la myrrhe et l’encens.
Je m’émerveille,
L’Amour s’éveille
Et c’est un nouveau-né
Sous une étoile !

Refrain Coda
JE SUIS UN DROMADAIRE,
JE ME SENS TOUT PETIT
EN REGARDANT L’ENFANT
ET SA MÈRE.
JE SUIS UN DROMADAIRE,
JE M’APPELLE ALIBER,
CE QUI M’EST ARRIVÉ
EST EXTRAORDINAIRE !