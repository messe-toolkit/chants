R.
Au rendez-vous de ton pardon
Je viendrai me relever
Et je boirai à la fontaine
Où toi, Seigneur,
Tu nous attends.
Et je boirai à la fontaine
Où toi, Seigneur,
Tu nous attends.

1.
Mais d'où viens-tu, Toi qui parles de l'Amour,
Toi qui guéris les cœurs lourds??
Mais d'où viens-tu, toi qui aimes les petits??

2.
Mais que fais-tu?; Toi qui sondes les esprits
Toi qui remets les péchés??
Mais que fais-tu, Toi qui changes l'eau en vin??

3.
Mais qui es-tu, Toi qui souffres sur la croix,
Toi qui es ressuscité??
Mais qui es-tu, Toi qui montes vers le ciel??