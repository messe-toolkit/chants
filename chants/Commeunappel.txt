R.
Comme un appel résonne en toi
Parmi le flot des "comment" des "pourquoi"
Sauter le pas et entrer dans la danse
Cet appel comme une chance

1.
Du nord au sud, dessus dessous on me pille
Je suis sur deux béquilles
Ton oxygène, ton sel, ton sol ton eau claire
Je suis ta terre

2.
Eclat de rêve dans le sourire d'un enfant
Grandir chemin faisant
Cris d'injustice qui refusent de se taire
Je suis ton frère

3.
Caché au creux du vent, d'un souffle léger
Je suis ta liberté
Dans la tempête ma parole est un feu
Je suis ton Dieu