1.
J’ai tellement rêvé de ses musiques
Que ç’en est d’venu tout à fait pathétique
Le jour et la nuit je suis sur ses traces
Humant ses parfums noyés dans l’espace.

MAIS DÉJÀ
LE PRINTEMPS ÉTAIT LÀ
ET MOI, ET MOI, ET MOI
JE NE LE SAVAIS PAS. bis

2.
Lassé des jours froids et sans lumière
Sera-t-il enfin au bout de mes prières
Je voudrais sortir de ce mauvais rêve
Au fond du brouillard que son jour se lève.

3.
Premier bleu du jour, tremble la terre
Il ouvre la vie en roulant la pierre
Chantez le soleil et les colombes
L’avenir est là qui sort d’une tombe.

4.
Quand je le cherchais sous toutes choses
Son coeur était là aux boutons des roses
Moi, je ne voyais rien que mes doutes
Alors qu’il marchait près d’moi sur la route.

OUI DÉJÀ
LE PRINTEMPS ÉTAIT LÀ
ET MOI, ET MOI, ET MOI
JE NE LE SAVAIS PAS.