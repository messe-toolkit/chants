Antienne
RASSEMBLE LE PEUPLE QUI PORTE TON NOM SEIGNEUR.
REMPLIS-LE DE TA LOUANGE

1.
Écoutez, nations, la parole du Seigneur !
Annoncez dans les îles lointaines :
« Celui qui dispersa Israël le rassemble,
il le garde, comme un berger son troupeau.
Le Seigneur a libéré Jacob,
l’a racheté des mains d’un plus fort.

2.
« Ils viennent, criant de joie, sur les hauteurs de Sion :
ils affluent vers la bonté du Seigneur,
le froment, le vin nouveau et l’huile fraîche,
les génisses et les brebis du troupeau.
Ils auront l’âme comme un jardin tout irrigué;
ils verront la fin de leur détresse.

3.
« La jeune fille se réjouit, elle danse ;
jeunes gens, vieilles gens, tous ensemble !
Je change leur deuil en joie,
les réjouis, les console après la peine.
Je nourris mes prêtres de festins ;
mon peuple se nourrit de ma bonté. »
Oracle du Seigneur.