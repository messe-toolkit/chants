1.
Il a brûlé le bois
En forme de potence,
Et arraché les clous
Qui crucifiaient la danse.
Qu'il n'y ait plus de croix,
Ni plus de corde au cou:
Il a brûlé le bois, il a jeté les clous !

Envolez-vous, corbeaux et pies,
La mort est morte de frayeur.
Allez-vous-en chagrins et pluies,
Figuiers et vignes sont en fleurs !

2.
Il a rendu vivants
Les corps pourris en terre,
Il a semé les blés
Qui font mourir les guerres.
Qu'il n'y ait plus de sang,
Ni plus de fers aux pieds :
Il s'est levé vivant, le Moissonneur des blés !

3.
Il a chassé du seuil
Les flûtes de tristesse,
Et mis dans nos jardins
Le rire et l'allégresse.
Qu'il n'y ait plus de deuil,
Ni plus de chaîne aux mains :
Il a chassé du seuil l'enfer avec ses chiens.