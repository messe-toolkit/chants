R.
PEUPLE BÉNI DE DIEU, RÉJOUIS-TOI!
IL VIENT CELUI QUE TU ESPÈRES.
PEUPLE DE BÉNI DE DIEU, RÉVEILLE-TOI!
IL VIENT TE SAUVER.

1.
Ne crains pas les flots de la mer
Quand vient l’heure de l’exode.
N’oublie pas le jour de ton passage,
N’oublie pas le passage de ton Dieu.

2.
Ne crains pas les flèches du mal
Tout au long des longues marches.
N’oublie pas les chants de tes victoires,
N’oublie pas les victoires de ton Dieu.

3.
Ne crains pas de vivre au désert
Tant que dure ton épreuve.
N’oublie pas l’amour de ta jeunesse,
N’oublie pas la jeunesse de ton Dieu.

4.
Ne crains pas les forces de mort
Lorsque règnent les ténèbres.
N’oublie pas le jour du tombeau vide,
N’oublie pas le triomphe de ton Dieu.