R.
NE CRAINS PAS DE PRENDRE CHEZ TOI MARIE,
N’OUBLIE PAS DE REDIRE SON NOM,
NE CRAINS PAS DE PRENDRE CHEZ TOI MARIE,
N’OUBLIE PAS, PRENDS-LA DANS TA MAISON.

1.
Et que son nom ne quitte pas tes lèvres,
Et que son nom soit musique d’amour,
Murmure-le au plus fort des ténèbres,
Et de sa joie deviens le troubadour.

2.
Regarde-la dans tes nuits de dérive,
Regarde-la cette étoile de paix,
Et que tes yeux la suivent comme un guide,
Et que tes pas ne la quittent jamais.

3.
Près de la croix, recueille son silence,
Près de la croix, Marie donne le grain,
Contemple-la, l’Église est en naissance,
Fils adopté, tu n’es plus orphelin.