R.
Comme lui savoir dresser la table,
comme lui nouer le tablier,
se lever chaque jour.
Et servir par amour, comme lui.

1.
Offrir le pain de sa parole
Aux gens qui ont faim de bonheur
Être pour eux des signes du royaume
Au milieu de notre monde.

2.
Offrir le pain de sa présence
Aux gens qui ont faim d’être aimés
Être pour eux des signes d’espérance
Au milieu de notre monde.

3.
Offrir le pain de sa promesse
Aux gens qui ont faim d’avenir
Être pour eux des signes de tendresse
Au milieu de notre monde.

4.
Offrir le pain de chaque cène
Aux gens qui ont faim dans leur coeur,
Être pour eux des signes d’Évangile
Au milieu de notre monde.