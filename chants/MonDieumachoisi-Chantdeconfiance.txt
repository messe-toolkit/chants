MON DIEU M’A CHOISI
C’EST LUI MA CONFIANCE
C’EST DIEU MON AMI
MA CHANCE ET MA DANSE

1.
Mon Dieu est là sur ma route
Marchant sur tous mes chemins
C’est Dieu qui me prend par la main
Quand il fait nuit dans mes doutes

2.
Mon Dieu est plein de patience
Dieu ne me punit jamais
Son pardon me garde en paix
Dieu m’accueille en sa présence

3.
Mon Dieu c’est mon assurance
Je n’ai plus peur de ma peur
Mon Dieu c’est mon grand bonheur
C’est ma chanson d’espérance