Str. 4
Siméon, conduit par l’Esprit,
se hâte vers le Temple
à la rencontre du Messie.

LUMIÈRE DU CHRIST,
NOUS TE BÉNISSONS.

Str. 5
L’Emmanuel, Seigneur souverain
pénètre dans le sanctuaire :
les temps sont accomplis.

LUMIÈRE DU CHRIST,
PURE LUMIÈRE,
NOUS TE GLORIFIONS.

Str. 6
Un jour nouveau éclaire le prophète :
il voit de ses yeux le Sauveur,
il l’annonce à toutes les nations.

LUMIÈRE DU CHRIST,
LUMIÈRE NÉE DE LA LUMIÈRE,
JOIE ÉTERNELLE DANS NOS COEURS
NOUS T’ACCLAMONS.