Dieu, viens à mon aide,
Seigneur, à notre secours !
Seigneur, je t’appelle, accours vers moi,
écoute mon appel quand je crie vers Toi !

Louez le Seigneur tous les peuples ;
fêtez-le, tous les pays !
Son amour envers nous s’est montré le plus fort ;
éternelle est la fidélité du Seigneur !

Au Dieu unique notre Sauveur,
par Jésus-Christ notre Seigneur,
Gloire et puissance dans l’Esprit,
avant les temps et pour les siècles ! Amen!