1.
Viens en nos coeurs, Esprit de Dieu,
Et fais surgir en notre vie
Le feu de Ta clarté.
Viens éveiller nos coeurs d’enfants,
Viens dans nos vies donner la joie,
Lumière sur nos pas.

VIENS DANS NOS COEURS,
ESPRIT DE DIEU,
SOIS LA LUMIÈRE POUR NOS PAS.

2.
Tu es pour nous le réconfort
Et Ta douceur éveille en nous,
Un souffle de fraîcheur.
Dans nos labeurs, sois le repos,
Et dans nos fièvres, la fraîcheur,
Nos pleurs, apaise-les.

3.
Par Ta lumière et ses bienfaits,
Viens nous combler au plus secret,
Réveille Tes enfants.
Sans Ton accueil au don de Dieu,
Quel homme peut tenir debout,
Au feu de son amour ?

4.
Purifie ce qui est souillé,
Abreuve d’eau nos lieux déserts,
Guéris nos coeurs blessés.
Ce qui est raide, assouplis-le,
Ce qui est froid, réchauffe-le,
Nos vies, redresse-les.

5.
À ceux qui vivent de la foi,
Qui font confiance en ton amour,
Accorde Tes bienfaits.
Donne Ta force et Ta bonté,
Qu’ils tiennent bon jusqu’à la fin,
Aux rives de la joie.