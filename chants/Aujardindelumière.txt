AU JARDIN DE LUMIÈRE,
DIEU NOTRE PÈRE,
TU NOUS ACCUEILLERAS :
LES GRAINS TOMBÉS EN TERRE
GERMERONT POUR TOI !

(Strophes optionnelles)

1.
La vie qui fut trop brève,
Fauchée à son éveil,
Sans avoir compris, sans destin accompli,
Au Royaume éternel,
Infinie,
Tu l’accueilleras.

2.
La vie gorgée de sève,
Offerte à tous les vents,
Que la mort surprit au détour d’une nuit,
Dans une aube nouvelle,
Assagie,
Tu l’accueilleras.

3.
La vie des temps de guerres,
Des coups et des combats,
Menacée d’exil, sans repos ni plaisir,
Au plus sûr de la paix,
Aguerrie,
Tu l’accueilleras.

4.
La vie de mauvais rêves,
Malade et tourmentée,
Dépeuplée d’amis, n’ayant su que souffrir,
Au foyer du bonheur,
Ton pays,
Tu l’accueilleras.

5.
La vie de longue peine,
Mûrie au grand soleil,
Qui porta son fruit et s’en est réjouie,
Dans une autre lumière,
Éblouie,
Tu l’accueilleras.

(Strophes finales)

6.
À ceux que tu appelles,
Créés par ton amour,
Dont Toi seul connais la droiture et la foi,
Aux pécheurs d’ici-bas,
Repentis,
Tu pardonneras.

7.
Et ceux qui rendent grâce,
Comblés de tant de biens,
Traversant l’épreuve en la force du Christ,
Dans la gloire du ciel,
Réunis,
Tu les recevras.