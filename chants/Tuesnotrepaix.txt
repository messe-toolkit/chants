1.
Comme une source chante en nos coeurs,
Esprit de Dieu.
Toute tendresse, force et douceur
Est ta présence.

TU ES NOTRE PAIX, TOI LE SOUFFLE DU DIEU VIVANT.
TU ES NOTRE JOIE, TOI ROSÉE D’ÉTONNEMENT.
TU ES NOTRE VIE, ESPRIT SAINT DU CRÉATEUR.
TU ES NOTRE AMOUR, TOI LE FEU DE CHAQUE JOUR.

2.
Comme une brise, souffle en nos coeurs,
Esprit de Dieu.
Viens revêtir de ta plénitude
Notre silence.

3.
Comme une flamme au coeur de la nuit,
Esprit de Dieu.
Tu nous réchauffes et tu nous éclaires,
Sainte Lumière.