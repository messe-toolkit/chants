Toi qui nous as précédés
Au passage vers le Père,
As-tu découvert
Ce visage
Que tu cherchais
Dans la nuit ?

Notre Sauveur l'a promis :
Celui qui cherche trouvera.

Toi qui as bu comme nous
À la coupe du Royaume,
As-tu reconnu
Sur tes lèvres
Le sang versé
Par Jésus ?

Notre Sauveur l'a promis :
Qui boit mon sang sera sauvé.

Toi qui as choisi l'amour
Comme guide sur ta route,
Es-tu parvenu
À la source :
L'Esprit donné
Pour nous tous ?

Notre Sauveur l'a promis :
Vous recevrez mon Esprit Saint.

Toi qui nous as précédés
Au passage vers le Père,
Peux-tu obtenir
Que tes frères
Soient réunis
Près de Dieu ?

Notre Sauveur l'a promis :
Je vous prendrai auprès de moi.