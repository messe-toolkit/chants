1.
Malgré les mots d’amour que nous a laissés le Christ
Malgré toute la foi que nous avons dans le coeur
Il y a des moments où tout paraît tellement triste
Qu’il faut partager sa douleur.

DONNE-MOI LA MAIN,
Ô TOI MON PROCHAIN
POUR VIVRE SUR TERRE,
J’AI BESOIN DE TA LUMIÈRE
DONNE-MOI LA MAIN,
OH TOI MON PROCHAIN
CAR LE PEUPLE DE DIEU
C’EST LA COMMUNION DES SAINTS!

2.
Après avoir quitté les soucis de la terre
Les justes sont assis tout là-haut près du père
Ils voient tous nos tracas et peuvent nous aider
À nous de savoir leur parler.
DONNE-MOI LA MAIN,

MON ANGE GARDIEN
POUR VIVRE SUR TERRE,
J’AI BESOIN DE TA LUMIÈRE
DONNE-MOI LA MAIN,
MON ANGE GARDIEN
CAR LE PEUPLE DE DIEU
C’EST LA COMMUNION DES SAINTS!

3.
Un jour ça s’ra mon tour de passer la frontière
Mon tour d’être jugé sur ce que j’ai pu faire
Mais si je vais là-haut, j’aurai un protégé
Et ce sera à moi de l’aider.

DONNE-MOI LA MAIN,
TOI MON FRÈRE HUMAIN
POUR VIVRE SUR TERRE
TU AS BESOIN DE LUMIÈRE
DONNE-MOI LA MAIN,
TOI MON FRÈRE HUMAIN
CAR LE PEUPLE DE DIEU
C’EST LA COMMUNION DES SAINTS!