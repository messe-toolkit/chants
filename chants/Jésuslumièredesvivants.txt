1.
Jésus, lumière des vivants
Tu quittes la terre des hommes
Et tu retournes vers le Père ;
Mais tu nous dis : ne craignez pas,
Il vous est bon que je m’en aille.

2.
Déjà, les jours sont révolus
Où l’homme touchait tes blessures
Et te prenait dans son étreinte.
Nos yeux de chair ne te voient plus,
Ton Esprit seul nous dit ta gloire.

3.
C’est lui, l’Esprit de vérité
Qui sait les chemins du Royaume
Et nous enseigne toute chose ;
D’un coeur confiant, nous l’appelons :
Qu’il vienne encor sur ton Église !

4.
Et toi, berger du seul troupeau,
Tiens-nous dans la paix du cénacle,
Avec Marie et les apôtres ;
Unis en toi, nous veillerons
Jusqu’au matin de Pentecôte.