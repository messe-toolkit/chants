CHANTEZ LA LOUANGE DU SEIGNEUR,
ALLÉLUIA, ALLÉLUIA !

1.
Avec la Harpe et la Cithare.
Le Saxophone et la Guitare !
Avec le Cor et la Trompette
Le Tambourin, la Clarinette !

2.
Avec les Cordes et les Cymbales,
Le Dulcimer, la Contrebasse !
Avec la Flûte et la Bombarde,
Les grandes Orgues et la Guimbarde !

3.
Avec le Fifre et le Trombone,
La Clavecin, le Xylophone !
Vous les chanteurs, les guitaristes,
Les musiciens, tous les artistes !