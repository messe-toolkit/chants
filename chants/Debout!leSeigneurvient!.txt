1.
Debout ! Le Seigneur vient !
Une voix prophétique
A surgi du désert...
Un désir une attente
Ont mûri nos esprits...
Préparons-nous !

2.
Debout ! Le Seigneur vient !
La parole s’infiltre
Elle ébranle nos coeurs...
Et voici le Royaume
Il approche, il est là...
Réveillons-nous !

3.
Debout ! Le Seigneur vient !
L’espérance nouvelle
Entre à flots dans nos vies...
Son mystère féconde
Un silence de foi...
Purifions-nous !

4.
Debout ! Le Seigneur vient !
Bienheureux les convives
Au festin de l’amour...
Dieu lui-même s’invite
Et nous verse la joie !
Rassemblons-nous !
Le Seigneur vient !