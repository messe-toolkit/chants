1.
Quand il m’arriv’ de ne plus vouloir avancer,
Quand j’ai parfois peur de marcher sur le chemin,
Quand il me semble que le vent souffle trop fort,
Oh je me souviens de tous ceux qui ont osé :

ILS ONT OSÉ LA PAIX, ILS ONT OSÉ LE BIEN,
ILS ONT OSÉ LA JOIE, ILS ONT OSÉ LE BEAU,
MERCI, OH MERCI, D’AVOIR OSÉ, D’AVOIR OSÉ !

2.
Quand il t’arrive de ne plus croire en l’amitié,
Quand tu as si peur de compter tous les matins,
Quand il te semble que le temps revient encore,
Oh oui, souviens-toi de tous ceux qui ont osé :

3.
Quand il m’arrive de regarder vers l’étranger,
Quand j’ai parfois peur de donner un peu de pain,
Quand il me semble que chanter est un effort,
Oh je me souviens de tous ceux qui ont osé :

4.
Pour que ce monde soit plus beau, ensoleillé,
À nous d’avancer, d’être unis comme des copains,
Oui, il me semble que nos dons sont des trésors,
Oh souvenons-nous de tous ceux qui ont osé :