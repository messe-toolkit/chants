R.
IL FAUT TOUJOURS, TOUJOURS Y CROIRE
VOULOIR PLUS HAUT, VOULOIR PLUS LOIN
ET QUAND LA NUIT SEMBLE SI NOIRE
ENVISAGER LE JOUR QUI VIENT

1.
Pour ouvrir un chemin
Dans les broussailles de la vie
Il y a des épines, des cailloux encombrants
Mais aussi, n’oublie pas
Des clairières qui s’ouvrent
Aux matins lumineux.

2.
Pour atteindre un sommet
Sur les montagnes de la vie
Il y a des obstacles, des ravins escarpés
Mais aussi, n’oublie pas
Des cascades qui chantent
Et des soleils levants.

3.
Pour franchir l’océan
Dans les bourrasques de la vie
Il y a des tempêtes, des brouillards angoissants
Mais aussi, n’oublie pas,
Des croisières si bleues
Aux rivages apaisants.

4.
Pour passer le désert
Dans les silences de la vie
Il y a des brûlures et la soif à mourir
Mais aussi, n’oublie pas,
des étoiles qui dansent
Plénitude infinie.