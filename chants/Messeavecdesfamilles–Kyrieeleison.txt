1 (Le Seigneur appelle)

Ô Seigneur notre ami
Tu appelles au bonheur
Ô Seigneur notre ami
Tu connais notre coeur !
Pour nos manques d’amour
Pour nos manques de foi
Seigneur, Seigneur, prends pitié.

SEIGNEUR, SEIGNEUR, PRENDS PITIÉ.

2 (Le Seigneur relève)

Toi le Christ, Toi qui viens
Pour ceux qui sont tombés
Tu viens tendre la main
Et jamais condamner !
Pour nos méchancetés
Et pour tous nos faux-pas
Ô Christ, Ô Christ, prends pitié.

Ô CHRIST, Ô CHRIST, PRENDS PITIÉ.

3 (Le Seigneur envoie)

Ô Seigneur de la vie
Tu veux nous envoyer
Dans le vent de l’Esprit
Pour semer l’amitié !
Viens éloigner le mal
Effacer nos péchés
Seigneur, Seigneur, prends pitié.

SEIGNEUR, SEIGNEUR, PRENDS PITIÉ.