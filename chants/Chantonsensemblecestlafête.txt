R.
Allez, allez, chantez, chantez
Chantons ensemble c'est la fête !

1.
Petits et grands,
Tous les enfants
Sont bienvenus au rendez-vous !
Papa, maman,
Tous les parents,
Sont bienvenus au rendez-vous !

2.
Regarde bienheureux
Écoute bien
Voici Jésus au rendez-vous !
Il t'aime bienheureux
Te connaît bien
Voici Jésus au rendez-vous !

3.
C'est un grand jour
Tout plein d'amour
Voici Jésus qui vient chez nous !
C'est pour toujours,
Toujours, toujours,
Que Jésus vient au rendez-vous !