Antienne - stance
Tu aimeras ton Dieu de tout ton cœur ;
tu aimeras ton frère comme toi-même.

R.
Benedictus Deus et Pater Domini nostri Jesu Christi.

Versets
Ps 17, 2-3 ; 26-29

1.
Je t'aime, Seigneur, ma force :
Seigneur, mon roc, ma forteresse,
Dieu mon libérateur, le rocher qui m'abrite,
Mon bouclier, mon fort, mon arme de victoire !

2.
Tu es fidèle envers l'homme fidèle,
Sans reproche avec l'homme sans reproche ;
Envers qui est loyal, tu es loyal,
Tu ruses avec le pervers.

3.
Tu sauves le peuple des humbles ;
Les regards hautains, tu les rabaisses.
Tu es la lumière de ma lampe,
Seigneur mon Dieu, tu éclaires ma nuit.

Ou bien
Ps 94, 1-9

1.
Venez, crions de joie pour le Seigneur,
Acclamons notre Rocher, notre salut !
Allons jusqu'à lui en rendant grâce,
Par nos hymnes de fête acclamons-le !

2.
Oui, le grand Dieu, c'est le Seigneur,
Le grand roi au-dessus de tous les dieux :
Il tient en main les profondeurs de la terre,
Et les sommets des montagnes sont à lui ;

3.
À lui la mer, c'est lui qui l'a faite,
Et les terres, car ses mains les ont pétries.
Entrez, inclinez-vous, prosternez-vous,
Adorons le Seigneur qui nous a faits.

4.
Aujourd'hui écouterez-vous sa parole ?
« Ne fermez pas votre cœur comme au désert,
Où vos pères m'ont tenté et provoqué,
Et pourtant ils avaient vu mon exploit. »