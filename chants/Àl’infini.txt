1.
À force de passer sous les néons artificiels
À force de marcher dans des impasses des tunnels

Je t’ai perdu de vue
Et toi, m’attendais-tu ?

TU ES LA VRAIE LUMIÈRE,
LA FLAMME QUI ÉCLAIRE MES NUITS
TU ES LA VRAIE LUMIÈRE,
LA FLAMME QUI ÉCLAIRE MA VIE
TU M’AIMES COMME UN PÈRE,
À L’INFINI

2.
À force de viser les étoiles éphémères
À force de filer vers les ombres, les éclairs