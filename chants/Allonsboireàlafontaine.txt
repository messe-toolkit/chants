R.
ALLONS BOIRE À LA FONTAINE
DÉCOUVRIR « LE DON DE DIEU » bis

1.
Il est là, pour la rencontre
Serons-nous au « rendez-vous » ?

2.
C’est Lui qui demande à boire.
Mais, avons-nous vraiment soif ?

3.
C’est Lui qui prend la parole
Mais, saurons-nous l’écouter ?