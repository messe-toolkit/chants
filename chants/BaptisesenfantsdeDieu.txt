STANCE
Baptisés enfants de Dieu,
Rendez grâce !
L'amour tient sa promesse :
Dieu vous donne l'esprit !

R.
L'amour tient sa promesse :
Dieu vous donne l'Esprit !

1.
En nous plongeant dans la mort de ton Fils
Qui descendit dans les eaux du Jourdain,
Tu fais de nous tes enfants bien-aimés :
Béni sois-tu, ô notre Père !
Aujourd'hui, par l'Esprit-Saint,
Confirme-nous en ton Alliance !

2.
Dans le repas, mémorial de la Croix,
Où Jésus offre son Corps et son Sang,
Tu viens unir tes enfants dispersés :
Béni sois-tu, ô notre Père !
En ce jour, par l'Esprit-Saint,
Consacre-nous pour ta louange !

3.
Dans notre monde emporté à tout vent,
Auprès de l'homme ignorant de ton nom,
Tu nous envoies en témoins de l'amour :
Béni sois-tu, ô notre Père !
Par le don de l'Esprit-Saint,
Soutiens la foi de tes disciples !

4.
Dans le combat, ce combat quotidien,
Où le malin nous détourne de toi,
Tu tends les bras, nous offrant ton pardon :
Béni sois-tu, ô notre Père !
D'un seul cœur, en l'Esprit-Saint,
Nous proclamons que tu fais grâce !