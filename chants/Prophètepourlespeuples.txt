R.
EVEILLE L’AURORE,
SOIS LE SEL DE LA TERRE
LUMIERE POUR TON FRERE,
TEMOIN DU DIEU VIVANT !

1.
Si dans ta vie une voix t’interpelle,
N’entends-tu pas ton Dieu qui cherche à te parler.
Moissonneur du blé levé,
Dieu a besoin de tes mains.
Lève-toi prophète pour les peuples !

2.
Si en chemin un ami te fait signe,
Ne vois-tu pas ton Dieu qui vient te rencontrer.
Pain rompu qui donne vie
Dieu a parlé à ton coeur.
Lève-toi prophète pour les peuples !

3.
Si un matin le silence t’appelle
N’entends-tu pas ton Dieu qui parle au fond de toi.
Bâtisseur de l’avenir
Dieu a besoin de tes mains
Lève-toi prophète pour les peuples !

4.
Si dans la nuit une étoile se lève,
Ne vois-tu pas que Dieu te montre le chemin
Compagnon des nuits sans fin,
Dieu habite parmi nous.
Lève-toi prophète pour les peuples !