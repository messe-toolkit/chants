1.
J’étais fermé dans mes verrous
Et prisonnier de mes façades
Mon avenir pendu au clou
La vie m’avait rendu malade.

R.
HEUREUX CELUI QUI PEUT PARLER.

2.
J’en aurais hurlé en plein jour
Je me roulais des nuits entières
Avec la tête pour tambour
Et mes doigts pris dans la portière.

3.
J’avais une tonne sur le coeur
Isolé, perdu comme une île
J’étais matraqué de chaleur
Par un soleil sonnant la tuile.

4.
Alors Seigneur, je te l’ai dit
J’ai dit que j’ai méprisé l’Homme
Cette trahison de ma vie
Je te la chante dans ce psaume.

5.
Ton nom à toit c’est le pardon
C’est comme ça que je te nomme
C’est toi qui m’as rendu mon nom
Et de nouveau je suis un homme.