Antienne - stance
Qui s'élève sera abaissé, qui s'abaisse sera élevé.

R.
Jesu Christe, in te confido.

Versets

Ps 36

1.
Quand le Seigneur conduit les pas de l'homme,
ils sont fermes et sa marche lui plaît.
S'il trébuche, il ne tombe pas
car le Seigneur le soutient de sa main.

2.
Évite le mal, fais ce qui est bien,
et tu auras une habitation pour toujours,
car le Seigneur aime le bon droit,
il n'abandonne pas ses amis.

3.
Les justes posséderont la terre
et toujours l'habiteront.
Le Seigneur est le salut pour les justes,
leur abri au temps de la détresse.

Ou bien

Ps 102

1.
Bénis le Seigneur, ô mon âme,
bénis son nom très saint, tout mon être !
Bénis le Seigneur, ô mon âme,
n'oublie aucun de ses bienfaits !

2.
Car il pardonne toutes tes offenses
et te guérit de toute maladie ;
il réclame ta vie à la tombe
et te couronne d'amour et de tendresse ;

3.
Le Seigneur est tendresse et pitié,
lent à la colère et plein d'amour ;
il n'agit pas envers nous selon nos fautes,
ne nous rend pas selon nos offenses.

4.
Aussi loin qu'est l'orient de l'occident,
il met loin de nous nos péchés ;
comme la tendresse du père pour ses fils,
la tendresse du Seigneur pour qui le craint !