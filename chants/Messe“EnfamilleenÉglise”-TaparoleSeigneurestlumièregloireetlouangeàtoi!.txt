1.
Ta Parole, Seigneur, est lumière,
Gloire et louange à toi.
Ta Parole, Seigneur nous libère,
Gloire et louange à toi.
Ta Parole aujourd’hui nous fait vivre,
Gloire et louange à toi.

2.
Donne-nous de goûter ta Parole,
Gloire et louange à toi.
Qu’elle éclaire aujourd’hui notre route,
Gloire et louange à toi.
Que nos coeurs à ta voix se réveillent,
Gloire et louange à toi.

3.
Ta Parole pour nous fait merveille,
Gloire et louange à toi.
Ton amour envers nous est fidèle,
Gloire et louange à toi.
Réalise pour nous tes promesses,
Gloire et louange à toi.

4.
Apprends-nous, ô Seigneur, la sagesse,
Gloire et louange à toi.
Guide-nous quand s’égarent nos routes,
Gloire et louange à toi.
Fais revivre en nos coeurs l’espérance,
Gloire et louange à toi.