Agneau pasteur,
Tu portes et emportes le péché du monde,
Aie pitié de nous, aie pitié de nous !
Agneau de Dieu, qui enlèves le péché du monde,
prends pitié de nous, prends pitié de nous.

Agneau pasteur,
Tu veilles attendant les enfants prodigues,
Aie pitié de nous, aie pitié de nous !
Agneau de Dieu, qui enlèves le péché du monde,
prends pitié de nous, prends pitié de nous.

Agneau pasteur,
Tu veux ardemment le bonheur des hommes,
Donne-nous la paix, donne-nous la paix !
Agneau de Dieu, qui enlèves le péché du monde,
donne-nous la paix, donne-nous la paix.