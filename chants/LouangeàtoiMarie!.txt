LOUANGE À TOI, MARIE !
DIEU T’A CHOISIE POUR METTRE AU MONDE
JÉSUS LE CHRIST,
SON FILS BIEN-AIMÉ.

1.
Si l’ange n’était pas venu
Féconder ton silence,
Ton peuple jamais n’aurait su
Que Dieu lui-même prend naissance.

2.
Si ton enfant n’était pas né,
Accouché sur la paille,
Nos yeux n’auraient jamais trouvé
Le roi-messie dans une étable.

3.
Si tu ne lui avais appris
À marcher sur la route,
Nos pas n’auraient jamais suivi
Jésus qui a guidé la foule !

4.
Si tu ne lui avais donné
À manger et à boire,
Jésus n’aurait jamais livré
Le Pain de Vie sur notre table !

5.
Debout, si tu n’étais restée
Près de lui au calvaire,
Il n’aurait jamais pu confier
À son Église ton coeur de mère !

6.
Si tu n’avais pas raffermi
Dans leur foi les apôtres,
Comment auraient-ils accueilli
L’Esprit du feu de Pentecôte ?

7.
Marie, tu nous as devancés
Dans ton corps de lumière.
Nos voix pourront toujours chanter
L’éternité de ta jeunesse !