R.
OUVREZ VOS MAINS,
OUVREZ VOS COEURS
L’ESPRIT DU SEIGNEUR.
FERA DE NOUS DES BÂTISSEURS
OUVREZ VOS MAINS,
OUVREZ VOS COEURS
QUE L’ESPRIT DE DIEU
ATTISE LE FEU.

1.
La lueur a jailli de l’ombre
Dieu nous a donné rendez-vous
Et de la peur qui nous encombre
Naît un amour plus grand que nous.

2.
Et dans le pain qui se partage
Quand grandit la fraternité
Reconnaître enfin le visage
Du Dieu vivant qui s’est donné.

3.
L’Évangile au coeur de l’annonce
Embrase nos vies comme un feu,
Trace un chemin parmi les ronces
“Si tu savais le Don de Dieu”.