1.
Je vous donne la vie,
Je vous donne ma vie
Pour que le monde l’ait en abondance
Je vous donne la vie.

R.
Recevez,
Je vous dis: recevez,
Tous ces fruits comme un don,
Une offrande, une grâce,
Je vous dis: recevez.

2.
Je vous donne la paix
Je vous donne ma paix
Pour que le monde connaisse la délivrance
Je vous donne la paix.

3.
Je vous donne la joie
Je vous donne ma joie
Pour que le monde chante mon espérance
Je vous donne la joie.

4.
Je vous donne l’amour
Je vous donne mon amour
Pour que le monde comprenne ma puissance
Je vous donne l’amour.

5.
Je vous donne l’Esprit
Je vous donne mon Esprit
Pour que le monde sache qui je suis
Je vous donne l’Esprit.

R.
Recevez,
Je vous dis: recevez,
Mon Esprit comme un don,
Une offrande, une grâce,
Je vous dis: recevez.