R.
Notre enfant tu es déjà là,
Notre enfant tu es déjà nous,
Déjà l'un et l'autre à la fois,
Mais déjà toi autre que nous.

1.
Nous voilà, aujourd’hui,
À nous offrir le meilleur de nous-mêmes.
Notre amour, a fleuri,
Et tu en es déjà la graine.
Merci…

R.
Notre enfant tu es déjà là,
Notre enfant tu es déjà nous,
Déjà l'un et l'autre à la fois,
Mais déjà toi autre que nous.

2.
Si en toi, sont écrits,
Et nos talents et aussi nos faiblesses.
Relevons, le défi,
D’accueillir tes propres richesses.
Promis…

R.
Notre enfant tu es déjà là,
Notre enfant tu es déjà nous,
Déjà l'un et l'autre à la fois,
Mais déjà toi autre que nous.

3.
Grâce à toi, nous savons,
Qu’il est dur et bon de marcher ensemble.
Qu’il est beau, l’horizon,
Et le chemin qui nous rassemble.
Merci…

R.
Notre enfant tu es déjà là,
Notre enfant tu es déjà nous,
Déjà l'un et l'autre à la fois,
Mais déjà toi autre que nous.

4.
En ce jour, plein d’éclats,
Nous te confions au Seigneur notre Père.
Nous ferons, qu’en ses bras,
Tu te saches enfant de lumière.
Promis…

R.
Notre enfant tu es déjà là,
Notre enfant tu es déjà nous,
Déjà l'un et l'autre à la fois,
Mais déjà toi autre que nous.