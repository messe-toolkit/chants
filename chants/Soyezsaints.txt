Je poursuis ma course tâchant d’atteindre là
où le Christ m’a destiné en me prenant.
Tout mon souci est d’oublier ce que je laisse en arrière,
de tendre constamment vers ce qui est en avant.
Je cours droit au but : au prix de la vocation
à laquelle Dieu m’a appelé dans le Christ Jésus.

SOYEZ SAINTS, PARCE QUE MOI JE SUIS SAINT !
DÈS L’ÉTERNITÉ, DIEU NOUS A CHOISIS,
POUR NOUS FAIRE IMMACULÉS,
SAINTS DEVANT LUI DANS L’AMOUR !

1.
Pour être enracinés et fondés en la charité
Cela suppose une sortie de soi.

2.
Dégagée de tout par la simplicité du regard,
L’âme est établie en ce bienheureux état.

3.
Lorsque tout est ordonné à Dieu,
L’âme ne vibre plus que sous la touche mystérieuse de l’Esprit Saint.

4.
Sous la touche mystérieuse de l’Esprit Saint,
Elle est transformée en «Louange de gloire».