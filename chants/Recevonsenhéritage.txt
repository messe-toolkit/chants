R.
Recevons en héritage l’Amour de notre Dieu ;
Partageons la paix qui vient du Seigneur.

1.
Par Jésus Christ,
qui nous rend fidèles à sa Parole
et nous comble de sa grâce.

2.
Par Jésus Christ
qui nous fait renaître à l’amour du Père
et nous donne la vie en son nom.

3.
Par Jésus Christ,
au coeur ouvert d’où jaillissent
des fleuves d’eau vive.