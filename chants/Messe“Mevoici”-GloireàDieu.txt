GLOIRE À DIEU AU PLUS HAUT DES CIEUX
ET PAIX SUR LA TERRE AUX HOMMES QU’IL AIME
GLOIRE À DIEU AU PLUS HAUT DES CIEUX
GLOIRE, GLOIRE, GLOIRE À DIEU.

1.
Nous te louons, nous te bénissons,
Nous t’adorons, nous te glorifions
Et nous te rendons grâce
Pour ton immense gloire.

2.
Tu es le Dieu, Seigneur Tout-Puissant
Seigneur Fils unique, Jésus-Christ
Seigneur Agneau de Dieu
Le Fils du Père.

3.
Toi qui enlèves tous les péchés
Viens nous sauver du mal, prends pitié
Assis auprès du Père
Écoute nos prières.

4.
Car toi seul es Saint, toi seul es Seigneur
Toi seul es le Très-Haut, Jésus-Christ
Avec le Saint-Esprit
Dans la gloire du Père.