R.
Dieu de l'epérance
Au bout de la nuit!
La mort est un passage
Qui nous mène à la vie.

1.
Dieu,
Tu nous invites à tout quitter,
A tout laisser pour venir à Toi.

2.
Dieu,
Tu nous invites à tout donner,
A partager ce qui vient de Toi.

3.
Dieu,
Tu nous invites à oublier,
A pardonner par amour pour Toi.