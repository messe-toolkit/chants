1.
Semer l’espoir en qui a peur
Semer la joie parmi les pleurs
Semer la paix chez l’agresseur
Et, au désert, semer des fleurs.
Offrir du pain à l’affamé
Offrir à boire à l’assoiffé
Offrir sa chance à l’opprimé
Et le sourire à l’attristé.
AIMER JUSQU’À L’ÉTERNITÉ
AIMER JUSQU’À TOUJOURS DONNER
AIMER JUSQU’À NE PLUS COMPTER.
AIMER JUSQU’À L’ÉTERNITÉ
AIMER JUSQU’À TOUJOURS DONNER
AIMER JUSQU’À NE PLUS COMPTER.
TOUT PARDONNER.

2.
Porter ses choix sur l’infini
Porter secours à l’indécis
Porter bonheur à un ami
Et affection à l’incompris.
Fleurir le front de l’audacieux
Fleurir le coeur de l’amoureux
Fleurir la main du généreux
Et le projet du courageux.

3.
Soigner les plaies de l’humilié
Soigner la peur de l’immigré
Soigner l’ennui du mal-aimé
Et le moral de l’éprouvé.
Aider celui qui s’est perdu
Aider celui qui est fourbu
Aider celui qu’on a exclu
Et qui déjà ne compte plus