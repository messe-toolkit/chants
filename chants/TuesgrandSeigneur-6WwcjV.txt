R.
Tu es grand, Seigneur, ô Roi des nations !
Ô Christ ressuscité, sans fin nous te louons,
À toi l’honneur et la majesté !
Ô Christ ressuscité, sans fin nous te louons,
Nous proclamons ta fidélité !

1.
Premier-né d’entre les morts,
Le souverain et Roi des nations,
Ton sang versé, Agneau immolé
Nous a lavés de tous nos péchés.

2.
Digne es-tu, Jésus Sauveur,
De recevoir le Livre de Vie.
Tu es venu pour nous racheter,
Petits et grands de toutes nations.

3.
Tu as fait de nous des rois,
Pour notre Dieu un peuple de saints.
Que soit béni le Dieu de bonté,
Gloire à l’Agneau portant nos péchés.

4.
Plus de mort et plus de nuit,
L’Agneau lui-même est notre clarté,
Et de son cœur la vie a jailli,
Source limpide et fleuve de vie.

5.
Viens, Seigneur, ne tarde plus,
Viens consoler ton peuple assoiffé.
Nous attendons le monde nouveau,
Dieu avec nous pour l’éternité.