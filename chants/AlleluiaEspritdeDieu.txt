Alléluia. Alléluia. (ter)
PENTECÔTE
Esprit de Dieu viens changer notre monde, alléluia, alléluia.
Que le cœur des croyants brûle de ton amour.
Alléluia.
ÉPIPHANIE
Nous avons vu se lever son étoile, alléluia, alléluia.
Et nous sommes venus adorer le Seigneur.
Alléluia.
PÂQUES
Que resplendisse pour nous ta lumière, alléluia, alléluia.
Notre Pâque immolée, c'est le Christ, le Seigneur.
Alléluia.