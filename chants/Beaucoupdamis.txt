BEAUCOUP BEAUCOUP D’AMIS
QUI SONT VENUS ICI
MAIS OUI, C’EST POUR CHANTER.
BONJOUR LA FÊTE!
BONJOUR LA FÊTE!
BEAUCOUP BEAUCOUP D’AMIS
QUI SONT VENUS ICI
MAIS OUI, C’EST POUR CHANTER.
NOTRE FRATERNITÉ.

1.
Ensemble tous les gens
Venez ! c’est important
Approchez, approchez !
C’est la grande amitié.

2.
Les filles, les garçons
Ont quitté leurs maisons
Pour venir aujourd’hui
Rencontrer des amis.

3.
En prenant le repas
On partage la joie
Les peines, les soucis
C’est pas pour aujourd’hui.

4.
On donne un coup de main
Ca fait vraiment du bien
De voir sur cette terre
Des gens qui vivent en frères.

5.
Messages de bonheur
Des mots tout en couleur
Grand lâcher de ballons
Au-dessus des maisons !