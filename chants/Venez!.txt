1.
VENEZ, OUVREZ LES BRAS
AUX FRUITS DE LA MOISSON!
Venez, ouvrez les bras aux fruits de la moisson!
Béni soit le Seigneur,
Maintenant et toujours,
Du soleil renaissant
Au profond du couchant.

2.
DEBOUT TOUS LES SOUFFRANTS
CLOUÉS SUR VOTRE BOIS !
Debout tous les souffrants cloués sur votre bois !
Les mendiants fatigués,
Aux espoirs desséchés,
De vos lits de poussière,
Il vous a relevés.

3.
VENEZ, OUVREZ LES MAINS
EN QUÊTE D’INFINI !
Venez, ouvrez les mains en quête d’infini !
Le royaume promis
Est semé aujourd’hui,
Dans la libre clarté,
Pour la Pâque future.