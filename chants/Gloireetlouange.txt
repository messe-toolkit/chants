Antienne
GLOIRE ET LOUANGE À NOTRE DIEU,
PAIX À TOUS LES HOMMES QU’IL AIME!
GLOIRE ET LOUANGE À NOTRE DIEU,
PAIX À TOUS LES HOMMES QU’IL AIME!

1.
Nous te louons, nous t’acclamons ;
Ta gloire nous la proclamons.
Seigneur du ciel nous t’adorons ;
Père très saint nous te chantons.

2.
Sauveur du monde Jésus Christ,
Agneau de Dieu le Fils béni.
Toi qui enlèves le péché,
Écoute-nous et prends pitié.

3.
Toi le seul Saint, le seul Seigneur,
Toi le Très-Haut, Jésus vainqueur,
Avec le Père et l’Esprit Saint,
Dieu glorieux loué sans fin.