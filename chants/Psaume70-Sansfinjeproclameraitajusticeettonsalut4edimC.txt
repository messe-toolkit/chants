SANS FIN, JE PROCLAMERAI
TA JUSTICE ET TON SALUT.

1.
En toi, Seigneur, j’ai mon refuge :
garde-moi d’être humilié pour toujours.
Dans ta justice, défends-moi, libère-moi,
tends l’oreille vers moi, et sauve-moi.

2.
Sois le rocher qui m’accueille,
toujours accessible ;
tu as résolu de me sauver :
ma forteresse et mon roc, c’est toi !

3.
Seigneur mon Dieu, tu es mon espérance,
mon appui dès ma jeunesse.
Toi, mon soutien dès avant ma naissance,
tu m’as choisi dès le ventre de ma mère.

4.
Ma bouche annonce tout le jour
tes actes de justice et de salut.
Mon Dieu, tu m’as instruit dès ma jeunesse,
jusqu’à présent, j’ai proclamé tes merveilles.