1.
Comme une pluie matinale
Où déjà perce le soleil
Sous l'arc-en-ciel de l'Alliance
Descends sur nous Esprit de Dieu.

2.
Par la douceur de ta force
Que germe en nous la liberté
Pour convertir en offrande
Les âpretés de notre vie.

3.
Dans notre cœur fais éclore
L'amour que porte le Semeur
Et nous pourrons entre frères
Le partager à l'infini.