Refrain:
Dis seulement une parole
Et nous serons guéris ! (bis)

1.
Change nos regards et pardonne nos faiblesses
Quand il se fait tard, fais naître le jour !
Change nos regards en un regard de tendresse
Et nos coeurs de pierre en un coeur d'amour !

2.
Donne-nous un peu, l'eau de la samaritaine
Pour renaître mieux, à ce don de Dieu !
Ouvre encore nos yeux, avec l'eau de ta fontaine
Et nos coeurs de pierre, aux beautés de Dieu !

3.
Reviens nous chercher, nous avons peur sur la berge,
Viens nous relever, nous voulons marcher !
Reviens t'inviter pour transformer notre auberge
Et nos coeurs de pierre, ressuscite-les !