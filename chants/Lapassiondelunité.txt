1.
Une lumière a brillé
Sur les terres d’Occident,
Un grand souffle a traversé
Leur histoire.
Dieu choisit tous les peuples,
Ses paroles de vie
Ensemencèrent l’Europe.

2.
Pourquoi les champs sans moisson,
Et la foi qui s’obscurcit ?
Pourquoi tant de divisions
Entre frères?
Jamais Dieu ne se lasse,
Chaque jour est pour lui
Un nouveau temps de semailles

3.
Notre coeur reste captif
Encombré de trop de biens,
Mais les saints suivent le Christ,
Les mains vides .
Rien n’arrête leur marche,
Rien n’entrave leur pas,
Dieu les attend, ils se hâtent.

4.
Sainte Brigitte a quitté
Fils et filles de sa chair.
Et devant le Crucifié,
Elle est seule.
La souffrance divine,
La douleur de Marie,
Dans tout son être s’impriment

5.
Sa vie entière est marquée
Par l’appel du Bien-Aimé..
La passion de l’unité.
Brûle en elle.
Que le feu se répande !
Avec force et douceur,
L’amour en croix le demande.

6.
De l’occident à l’Orient
Et du Nord jusqu’au midi
Marchera vers le Vivant
Un seul peuple.
Dieu bénit tous les hommes !
Paix à ceux qui sont loin
Et paix à ceux qui sont proches .