Salut, Marie, pleine de grâce, le Seigneur est avec toi.
L'Esprit-Saint viendra sur toi, Marie,
Et la force du Très-Haut te prendra sous son ombre.
Je suis la servante du Seigneur, qu'il me soit fait selon ta Parole.
Vierge sage entre toutes, merveille dans les cieux comme une aurore lumineuse.
Fille de Sion, tu es grâce et beauté, brillante comme une étoile, splendide comme le soleil.