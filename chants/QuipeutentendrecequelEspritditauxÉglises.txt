1.
Qui peut entendre
ce que l'Esprit
dit aux Églises ?
Qui pourra nous le dire ?
Si légère est la brise
et si vive
la blessure du silence.

2.
Qui peut attendre
que l'Esprit saint
parle aux Églises ?
Qui de nous peut se taire ?
Si tenace est la peine
et si vaine
la brûlure d'impatience.

3.
Qui peut comprendre
ce que l'Esprit
souffle aux Églises ?
Qui pourra croire encore ?
Mais surgit la parole
que nous donne
le murmure d'espérance.