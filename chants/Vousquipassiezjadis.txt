1.
Vous qui passiez jadis
Sur nos chemins
Comme l’annonce d’une enfance à venir,
Découvrez-nous sa grâce,
Soeurs aimés dans le Christ,
Partagez-nous encor
Votre secret.

2.
Vous connaissez le prix
Des jours obscurs
Et l’espérance dont la foi les remplit ;
«N’attendez pas de signes»,
Nous répond votre vie,
L’événement d’amour
Est quotidien.

3.
Vous savez bien le poids
De nos échecs
Et la souffrance qui refuse la croix ;
Vous connaissez la coupe
Des enfants sans espoir,
Vous mesurez la peur
Qui les étreint.

4.
La compassion de Dieu
Vous a blessées
Au fond de l’âme comme un glaive de feu ;
Vous deveniez l’image
De l’Agneau mystérieux
Quand vous portiez la mort
Des mal-aimés.

5.
Et maintenant voici
Le grand repos
Dans la lumière d’un bonheur accueilli :
Vous contemplez le Père,
Soeurs élus dans le Christ,
Et son amour offert
À tous les hommes.