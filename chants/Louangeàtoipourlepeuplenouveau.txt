LOUANGE À TOI POUR LE PEUPLE NOUVEAU,
PEUPLE DE L’ALLIANCE ÉCLAIRÉ PAR TES MOTS !
ÉCLAIRÉ PAR TES MOTS !

1.
Dans la maison qui nous rassemble
Tu es, Seigneur, au milieu de nous.
Viens nous apprendre à vivre ensemble,
Que nous soyons l’Église habitée par l’amour !

2.
Tu es parti de ton village
Pour demeurer où l’Esprit t’envoie.
Tes gestes forts sont-ils d’un sage ?
Folie pour ta famille, ô Jésus, quelle croix !

3.
Ils sont nombreux, ceux qui te cherchent ;
Restés dehors, que vont-ils trouver ?
Ouvrons nos portes et nos fenêtres,
Qu’ils voient sur nos visages un reflet de ta paix !