1.
Dans ton amour pitié pour moi,
je suis un homme au coeur blessé ;
Dieu plus grand que notre coeur,
fais-moi connaître mon péché,
mes yeux verront ce que tu vois.
Pitié pour moi, pitié Seigneur !

2.
Dans ton amour, tu viens à moi,
tu es cet Homme au sang versé ;
Dieu plus grand que notre coeur,
inonde-moi de ta clarté,
ma nuit saura quelle est ta joie.
Pitié pour moi, pitié Seigneur !

3.
Par ton amour entraîne-moi,
Esprit de l'homme relevé ;
Dieu plus grand que notre coeur,
j'aurai visage de sauvé,
mon coeur nouveau exultera.
Pitié pour moi, pitié Seigneur !