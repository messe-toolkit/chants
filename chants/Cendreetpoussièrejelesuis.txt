R.
Cendre et poussière, je le suis,
Mais je garde au coeur une flamme.
Dieu nous as faits pour vivre en lui,
Transformés au feu de la Pâque,
Transformés au feu de la Pâque.

1.
Cadeau du ciel sur notre terre,
La vie me vient de l'infini.
Mais au jardin de l'éphémère
Nos jours déclinent vers la nuit.
Quand verrai-je à tout jamais
Le soleil d'éternité ?

2.
Pour avancer vers la lumière
L'Esprit me guide et me soutient ;
Dans le secret de la prière
Mes yeux découvrent le Chemin :
Jésus Christ ressuscité,
Fils de l'homme à nos côtés :

3.
Dans le désert et son silence
Il a connu nos cris de faim.
Qu'il me nourrisse d'espérance,
Par sa Parole et par son Pain.
Avec lui je marcherai,
Jusqu'au soir je l'attendrai.

4.
Viendra l’aurore en sa demeure,
Le temps nouveau de son Festin.
Dieu me dira parmi son peuple
Ce que j'ai fait pour tous les siens.
Au Royaume de l'amour
Grande joie de voir le jour !