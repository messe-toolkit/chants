1.
Fier chevalier, l’éclat de ton armure,
Comme un soleil, attire tous les yeux ;
Ta loyauté, ton âme tout pure,
Nous ont conquis, et nous voici joyeux.

R.
Saint Georges, guide-nous
Sur la route claire et belle,
Saint Georges, guide-nous,
Rends-nous fermes prêts à tout.

2.
Enseigne-nous la force plus qu’humaine,
Qui fait passer avant tous les plaisirs
Le seul devoir au prix de mille peines.
Donne à nos cœurs les plus vaillants désirs.

3.
Garde à nos yeux le charme d’un sourire,
Quand nous souffrons, au plein de notre effort,
Et, dûssions-nous subir un long martyre,
Tiens nos cœurs droits quand faibliront nos corps.

4.
Ô grand vainqueur, de ton séjour de gloire,
Assiste-nous, quand ici nous luttons,
Conduis nos pas aux routes de victoire.
Jusqu’à la mort, s’il faut, nous te suivrons.