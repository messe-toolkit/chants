« Moi, je suis la porte. Si quelqu'un entre en passant
par moi, il sera sauvé ; il pourra entrer ;
il pourra sortir et trouver un pâturage. »