Que celui qui a deux vêtements partage avec qui n’en a pas.
Que celui qui a de quoi manger fasse de même.
Que celui qui veut être le premier soit le dernier de tous
Et le serviteur de tous

Ne vous faites pas pour vous-mêmes de trésors sur terre
Où mites et vers dévorent, où les voleurs cambriolent
Mais faîtes-vous pour vous-mêmes des trésors dans le Ciel
Car où est ton trésor, là sera ton coeur.