Refrain :
Peuples chantez son nom
Exultez terre entière
Il est ressuscité !..

1.
Allons cueillir la Pâque, allons danser nos vies,
Sur les chemins de joie, d’amour, de liberté ;
Il éclaire la route, il est notre lumière,
Il est ressuscité !..

2.
Qu’éclate la victoire, voici la liberté,
Le Christ ressuscité triomphe de la mort ;
Victoire de l’amour, heure de vérité,
Il est ressuscité !..

3.
Témoignage infini d’amour de Dieu, le Père
Pour libérer l’esclave il a livré son fils ;
Germe de vie nouvelle, la faute devient chance
Il est ressuscité !..

4.
Victoire de l’amour, victoire de la vie,
Ô Dieu, ce soir, accueille le feu de tous nos coeurs,
La flamme qui s’élève vers Lui en une offrande
Il est ressuscité !..

5.
Ténèbres illuminées, le soleil n’est pas mort,
La croix n’est plus la croix, la mort n’est plus la mort,
Notre corps est vivant avec Lui dans l’amour
Il est ressuscité !..