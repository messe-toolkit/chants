ANTIENNE
Allez donc aux croisées des chemins :
tous ceux qui passeront, invitez-le aux noces. (Mt 22, 9)

R.
Venite, exultemus Domino, venite, adoremus.

Ps 104, 1-6 ; 41-45.

Rendez grâce au Seigneur, proclamez son nom,
Annoncez parmi les peuples ses hauts faits
Chantez et jouez pour lui,
Redites sans fin ses merveilles ;

Glorifiez-vous de son nom très saint :
Joie pour les cœurs qui cherchent Dieu !
Cherchez le Seigneur et sa puissance,
Recherchez sans trêve sa face ;

Souvenez-vous des merveilles qu'il a faites,
De ses prodiges, des jugements qu'il prononça,
Vous, la race d'Abraham son serviteur,
Les fils de Jacob qu'il a choisis.

Il ouvre le rocher : l'eau jaillit,
Un fleuve coule au désert.
Il s'est ainsi souvenu de la parole sacrée
Et d'Abraham, son serviteur ;

Il a fait sortir en grande fête son peuple,
Ses élus, avec des cris de joie !
Il leur a donné les terres des nations,
En héritage, le travail des peuples,

Ps 144, 1-2 ; 8-14.

Je t'exalterai, mon Dieu, mon Roi,
Je bénirai ton nom toujours et à jamais !
Chaque jour je te bénirai,
Je louerai ton nom toujours et à jamais.

Le Seigneur est tendresse et pitié,
Lent à la colère et plein d'amour ;
La bonté du Seigneur est pour tous,
Sa tendresse, pour toutes ses œuvres.

Que tes œuvres, Seigneur, te rendent grâce
Et que tes fidèles te bénissent !
Ils diront la gloire de ton règne,
Ils parleront de tes exploits,

Annonçant aux hommes tes exploits,
La gloire et l'éclat de ton règne :
Ton règne, un règne éternel,
Ton empire, pour les âges des âges.

Le Seigneur est vrai en tout ce qu'il dit,
Fidèle en tout ce qu'il fait.
Le Seigneur soutient tous ceux qui tombent,
Il redresse tous les accablés.