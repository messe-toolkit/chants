R.
IL N’AVAIT QUE CINQ PAINS
L’ENFANT DE L’ÉVANGILE
ET TROIS PETITS POISSONS
AUX ÉCAILLES D’ARGENT.
ET TROIS PETITS POISSONS
AUX ÉCAILLES D’ARGENT.

1.
Chacun reçut sa part
Il restait six corbeilles
Comme autant de soleils
Que de pains partagés.

2.
Jésus nous dit ainsi
Qu’il a besoin des autres
Pour donner à manger
À son peuple affamé.

3.
Chacun au fond du coeur
Peut chanter la merveille
De l’amour partagé
Par l’enfant du soleil.

4.
Jésus nous dit ainsi
Que dans un coeur fidèle
L’amour peut se donner
Et se multiplier.

5.
Il n’avait que cinq pains
L’enfant de l’évangile
Mais la joie dans ses yeux
Brillait de mille feux.