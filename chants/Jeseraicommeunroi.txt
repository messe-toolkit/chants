1.
Si tu me donnes pour maison
L’immense toit du ciel
Si tu me donnes pour compagnons
Les rayons du soleil
Si tu viens me désaltérer
De l’eau des sources et des ruisseaux
Et si je peux me reposer
Dans un champ de coquelicots.

JE SERAI COMME UN ROI.
JE SERAI COMME UN ROI.

2.
Si tu me donnes pour chanter
Le banjo des grillons
Si tu me donnes pour avancer
La ligne d’horizon
Si tu viens me faire rêver
Par le flot bleu de l’océan
Et si je peux encore marcher
Dans le grand souffle du vent.

3.
Si tu me donnes pour espoir
La lumière du couchant
Si tu me donnes pour regard
Les grands yeux d’un enfant
Si tu viens me donner la paix
Par le sortilège des mots
Et si je peux en vérité
Découvrir un monde nouveau.