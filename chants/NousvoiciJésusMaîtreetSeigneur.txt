R.
NOUS VOICI, JÉSUS MAÎTRE ET SEIGNEUR,
TU NOUS ENGAGES À TON SERVICE.
ENVOIE-NOUS, JÉSUS LE SERVITEUR,
POUR LE SERVICE DE NOS FRÈRES.

1.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui ploient sous le fardeau.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous le vrai repos.

2.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui tombent en chemin,
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous force et soutien.

3.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui ont l’esprit troublé.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous douceur et paix.

4.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui souffrent dans leur corps.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous le réconfort.

5.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui sont désespérés.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous l’ardeur d’aimer.

6.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui sont handicapés.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous aide et respect.

7.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux que l’âge a dévastés.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous de l’amitié.

8.
Je vous le dis, à vous qui m’avez suivi :
Qu’ils viennent à moi tous ceux qui se sont égarés.
Je vous les confie, à vous qui êtes mes amis :
Qu’ils trouvent auprès de vous la vérité.