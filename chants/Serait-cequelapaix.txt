SERAIT-CE QUE LA PAIX DÉSERTERAIT MON ÂME?
JE CHERCHE EN TOI, MON DIEU, UN SECOURS, UN ABRI.
TOI QUI SAIS QUE LE VENT PEUT ÉTEINDRE LA FLAMME,
ENTRE TES MAINS, MON DIEU, JE REMETS MON ESPRIT.

1.
Et puis je ne sais plus quel uniforme je porte,
Ni quel est mon drapeau, ni dans quel camp je suis.
Je ne sais même plus si j’ai claqué la porte,
C’est la guerre dans ma tête et le charivari…

2.
Rebelle parvenu qui persiste et qui signe
Mais qui reste pourtant obstinément chercheur,
Je sens bien ta confiance et veux en être digne,
Je t’attendrai, mon Dieu, je resterai veilleur.

3.
Remèdes illusoires et marchands de chimères,
Des diktats de la mode, aux clinquants des clichés,
Qui me dira pourquoi le vrai est éphémère
Et le bonheur qui dure est-il toujours suspect ?