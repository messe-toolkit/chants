R.
Vous m’avez reconnu à la fraction du pain :
Prenez-le;
Vous m’avez reconnu dans ce pain en vos mains :
Mangez-le;
Avec vous, je conclus une alliance nouvelle,
Avec vous, je conclus une alliance éternelle.

1.
Voici le commencement
“Le Verbe s’est fait chair”
Voici le commencement
Je suis venu sur terre
Couché dans une mangeoire. (bis)

2.
Me voici devant la porte
M’entends-tu donc frapper ?
Me voici devant la porte
Si tu me laisses entrer
Je mangerai chez toi. (bis)

3.
Me voici parmi la foule
Comment la rassasier ?
Me voici parmi la foule
Je veux tout lui donner
Je suis le pain de vie. (bis)

4.
Me voici au bord du puits
Si je te donne à boire
Me voici au bord du puits
Et si tu veux y croire
Je suis source d’eau vive. (bis)

5.
Me voici aux pieds des hommes
Laissez-moi vous laver
Me voici aux pieds des hommes
Laissez-moi vous aimer
Je suis le serviteur. (bis)

6.
Me voici vers Emmaüs
De quoi avez-vous peur ?
Me voici vers Emmaüs
Écoutez votre coeur
Je suis ressuscité. (bis)

7.
Me voici Ressuscité
Qui viendra au festin ?
Me voici Ressuscité
Qui recevra mon pain ?
Vous serez mes témoins. (bis)