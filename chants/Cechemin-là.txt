IL PASSE PAR ICI
PASSE, PASSERA PAR LÀ
IL PASSE PAR ICI
PASSE, PASSERA PAR LÀ
ET DANS NOS COEURS AUSSI, CE CHEMIN-LÀ (bis)
LÈVE-TOI POUR LA PAIX
TENDS LES BRAS POUR LA JOIE
AUTOUR DE TOI, L’AMOUR VIENDRA (bis)

1.
Pour Martin Luther King
Dom Helder Camara
Ghandi et Mandela
À Taizé, frère Roger
Sainte Claire, Saint François
D’Assise ou d’Atlanta
Ils ont choisi, tu sais
Le chemin de la paix

2.
Thérèse à Calcutta
L’Abbé Pierre à Paris
Sainte Jeanne Jugan
Ou Joseph Wresinski
Pour Soeur Emmanuelle
Sous des chiffons, le ciel
Ils ont choisi tu vois
Le chemin de la joie

3.
Saint Étienne et Blandine
Saint Priest de Clermont
Les moines de Tibhirine
Et Irénée de Lyon
Hier comme aujourd’hui
Prêts à donner leur vie
Ils ont choisi un jour
Le chemin de l’amour