1.
Tu as dit à tes parents !
« Ma famille est sans frontières,
Les lépreux et les enfants,
Les étrangers sont mes frères.
Toi qui écoutes et qui vis
Les Paroles de mon Père,
Tu es bien plus qu’un ami ;
Tu deviens mon propre frère. »

2.
Tu es tellement passionné
Pour nous dire ce que tu crois.
Toi, le fils du charpentier
Où as-tu appris tout ça ?
Tu guéris un possédé
C’est pourtant jour de Sabbat ;
Bousculant les préjugés
De nos docteurs de la Loi.

3.
Tu nous dis que le Sabbat
Nous est donné pour prier
Pour fortifier notre foi,
Surtout pas pour nous brimer.
Ce jour-là quand tu choisis,
De guérir, de faire le bien,
Tu dis : « On sert Dieu aussi,
Quand on aide son prochain ? »

4.
Tout le monde, autour de toi
Se demande si tu es
Un prophète d’autrefois
Qui serait ressuscité.
Mais c’est Pierre qui nous dit
Qui tu es, en vérité,
C’est le Père, par l’Esprit,
Qui le lui a révélé.

R.
Le Messie que l’on attend
C’est toi, Jésus.
Tu es le Fils du Dieu vivant
Jésus ! Jésus !