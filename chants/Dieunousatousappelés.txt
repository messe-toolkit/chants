R.
Nous sommes le corps du Christ,
Chacun de nous est un membre de ce corps.
\s
Chacun reçoit la grâce de l'Esprit
pour le bien du corps entier. (bis)

1.
Dieu nous a tous appelés
à tenir la même espérance,
pour former un seul corps
baptisé dans l'Esprit.
\n
Dieu nous a tous appelés
à la même sainteté,
pour former un seul corps
baptisé dans l'Esprit.

2.
Dieu nous a tous appelés
des ténèbres à sa lumière,
pour former un seul corps
baptisé dans l'Esprit.
\n
Dieu nous a tous appelés
à l'amour et au pardon,
pour former un seul corps
baptisé dans l'Esprit.

3.
Dieu nous a tous appelés
à chanter sa libre louange,
pour former un seul corps
baptisé dans l'Esprit.
\n
Dieu nous a tous appelés
à l'union avec son Fils,
pour former un seul corps
baptisé dans l'Esprit.

4.
Dieu nous a tous appelés
à la paix que donne sa grâce,
pour former un seul corps
baptisé dans l'Esprit.
\n
Dieu nous a tous appelés
sous la croix de Jésus Christ,
pour former un seul corps
baptisé dans l'Esprit.

5.
Dieu nous a tous appelés
au salut par la renaissance,
pour former un seul corps
baptisé dans l'Esprit.
\n
Dieu nous a tous appelés
au salut par l'Esprit Saint,
pour former un seul corps
baptisé dans l'Esprit.

6.
Dieu nous a tous appelés
à la gloire de son Royaume,
pour former un seul corps
baptisé dans l'Esprit.
\n
Dieu nous a tous appelés
pour les noces de l'Agneau,
pour former un seul corps
baptisé dans l'Esprit.