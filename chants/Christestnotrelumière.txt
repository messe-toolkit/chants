R.
CHRIST EST NOTRE LUMIÈRE,
NOTRE SOLEIL LEVANT !

1.
Qui regarde vers lui resplendira
sans ombre ni trouble au visage.

2.
Il vient nous visiter :
en lui la joie de notre coeur !