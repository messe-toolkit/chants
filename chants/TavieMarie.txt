1.
Ton nom chante comme en lumière
Bénie sois-tu, Marie.
Tu rayonnes sur notre terre
Bénie sois-tu, Marie :

TA VIE, MARIE
DONNE SENS À NOS VIES.
TA VIE, MARIE
NOUS ENTRAÎNE À LA VIE.

2.
Tes yeux pleurent sur tant de guerres
Reste avec nous, Marie.
Tu nous portes dans nos misères
Reste avec nous, Marie.

3.
Tes mains s’ouvrent comme en prière
Veille sur nous, Marie.
Tu nous guides et tu nous éclaires
Veille sur nous, Marie.

4.
Tu nous aimes, Toi notre mère
Heureuse es-tu, Marie.
Tu nous mènes vers Dieu le Père
Heureuse es-tu, Marie.