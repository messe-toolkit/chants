R.
DIEU, TU M'AS SEDUIT, TU M'AS DIT: "LEVE TOI!
VA TROUVER MON PEUPLE, AUJOURD'HUI JE T'ENVOIE.
FAIS FACE A TOUS LES VENTS,TU N'ES PAS UN ENFANT,
REMPART DE MA CITE,AVEC TOI JE SERAI."

1.
Prophète en mon pays
sur des voies de non violence,
j'annonce un temps de paix
qui fleurit sous ton Alliance.
Toi, Seigneur, tu me conduis
vers la terre de la vie.

2.
Prophète révolté
par les dieux de l'éphémère,
je crie ta vérité
dans le temple de lumière.
A ma place de veilleur
que je sois ton défenseur !

3.
Prophète dont la foi
voudrait vaincre les montagnes,
je tremble malgré moi
quand je vois le mal qui gagne.
Dans ma nuit où donc es-tu ?
Comment dire ta venue ?

4.
Prophète auprès des grands
je proclame ton message ;
ils rient de mes espoirs,
je suis seul sous les sarcasmes.
Toi, la force des témoins,
j'ai confiance dans ta main.

5.
Prophète au coeur blessé
par la haine de mes frères,
je suis le condamné
des puissants et des grands prêtres.
Quand ton peuple est en folie
sauve-moi de l'ennemi !

6.
Prophète des nations
pour bâtir un monde juste,
je dis quel est ton Nom
et la flamme qui me brûle.
Dieu qui aimes les vivants,
montre-toi le Dieu présent !