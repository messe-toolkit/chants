R.
Exultez de joie, peuples de l´univers,
Jubilez, acclamez votre Roi !
Exultez de joie, Dieu a pris notre chair,
Jubilez, chantez alléluia !

1.
Car un enfant nous est né,
Car un fils nous a été donné,
Dieu éternel et prince de paix,
Maintenant et à jamais.

2.
Dieu, nul ne l´a jamais vu ,
En son Fils il nous est apparu.
Il nous a révélé sa bonté
Par Jésus le Bien-Aimé.

3.
Pour nous, pour notre salut,
Pour sauver ce qui était perdu.
Pour qu´enfin la paix règne sur terre,
Le Fils de Dieu s´est fait chair.

4.
Quand les temps furent accomplis,
Dieu posa son regard sur Marie.
Sans cesser d´être le Tout-Puissant,
Il se fit petit enfant.

5.
Le même hier et demain,
Aujourd´hui il vit au cœur des siens.
Annonçons l´œuvre de son amour :
´Dieu avec nous´ pour toujours !´