PRENDRE DU TEMPS POUR DIEU
TOUT SIMPLEMENT POUR L’ACCUEILLIR.
S’ASSEOIR AUX PIEDS DE DIEU
ÉCOUTER SA PAROLE.

1.
Prendre le temps à Béthanie
En compagnie de Marthe et de Marie
L’une s’active et l’autre pas
Se demander et dire pourquoi :
Quel est le mieux pour moi ?

2.
Être soi-même juste un instant,
Se libérer de nos « chemins faisant »
Quand tout s’agite autour de nous
S’asseoir, et pour tenir debout
S’offrir un rendez-vous.

3.
Prendre son temps pour s’arrêter
Prendre son temps, se mettre à écouter
Cette Parole qui nourrit
Qui te console et te grandit
Dieu au coeur de ta vie.