JE ME DONNE TOUT À TOI
ESPRIT SAINT, ESPRIT DE DIEU
GARDE-MOI TOUTE MA VIE
GUIDE-MOI EN TOUTES CHOSES.

1.
C’est Toi qui as formé dans le sein de Marie
Celui qui est venu, Jésus pour nous sauver.
C’est Toi qui l’as conduit, animé dans sa vie,
En tout ce qu’il a dit, en tout ce qu’il a fait.

2.
C’est Toi qui es présent dans le Christ innocent
Quand il est humilié, souffrant au Golgotha.
C’est Toi qui es en Lui, quand il meurt sur la croix,
C’est Toi qui vis en Lui, au matin du Vivant.

3.
C’est Toi qui nous façonnes et formes en nous Jésus
Qui nous unis en toi, Église du Seigneur
C’est Toi qui, aujourd’hui, fais brûler en nos coeurs
Un feu rempli d’amour éclairant l’Inconnu.