Stance
Esprit du Dieu vivant,
toi qui souffles où tu veux,
lève-toi sur les disciples rassemblés !
Esprit de Pentecôte,
ouragan de feu
dans les cénacles portes closes,
Viens chasser nos peurs pour les audaces du Royaume,

ESPRIT DE LIBERTÉ,
NUL NE SAIT D’OÙ TU VIENS,
NUL NE VOIT OÙ TU VAS;
ESPRIT DE SAINTETÉ,
MONTRE-NOUS JÉSUS,
LE CHEMIN VERS LE PÈRE !
LE CHEMIN VERS LE PÈRE !

1.
Voici dans notre Église un chant nouveau,
Christ est libéré de son tombeau.
La paix que tu insuffles à ses témoins
Clame notre foi au Dieu qui vient.

2.
Voici qu’Il nous précède en Galilée,
Là nous le verrons en vérité.
L’amour que tu répands dans notre coeur
Nous rend messagers de son bonheur.

3.
Voici qu’en notre monde il paraîtra,
Toute chair alors le connaîtra.
Réveille l’espérance des croyants,
Toi qui nous prépares à son Levant !