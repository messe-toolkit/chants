D’ÂGE EN ÂGE, SEIGNEUR,
TU AS ÉTÉ NOTRE REFUGE.

1.
Tu fais retourner l’homme à la poussière ;
tu as dit : « Retournez, fils d’Adam ! »
À tes yeux, mille ans sont comme hier,
c’est un jour qui s’en va, une heure dans la nuit.

2.
Tu les as balayés : ce n’est qu’un songe ;
dès le matin, c’est une herbe changeante :
elle fleurit le matin, elle change ;
le soir, elle est fanée, desséchée.

3.
Apprends-nous la vraie mesure de nos jours :
que nos coeurs pénètrent la sagesse.
Reviens, Seigneur, pourquoi tarder ?
Ravise-toi par égard pour tes serviteurs.

4.
Rassasie-nous de ton amour au matin,
que nous passions nos jours dans la joie et les chants.
Que vienne sur nous la douceur du Seigneur notre Dieu !
Consolide pour nous l’ouvrage de nos mains.