R.
Source au cœur de l´homme,
Ton nom, Seigneur, qu'il est grand !
Souffle sur le monde,
Ton nom, Seigneur, Dieu vivant !

1.
Dans les combats de la terre,
Dans les luttes pour le droit :
Ton nom est une justice
Qui se lève sur nos temps.

2.
Dans les sanglots de la terre,
Dans les larmes et dans le sang :
Ton nom est une musique
Qui renverse les tombeaux.

3.
Dans les clartés de la terre,
Dans les rêves et dans les cris :
Ton nom est une semence
Qui s´éveille dans la nuit.

4.
Dans les pardons de la terre
Dans les gestes pour la paix :
Ton nom est une fontaine
Qui déborde dans nos mains.

5.
Dans les rejets de la terre
Dans les haines et dans l'oubli :
Ton nom est une tendresse
Qui façonne un cœur aimant.