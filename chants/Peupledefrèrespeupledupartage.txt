R.
Peuple de frères, peuple du partage,
Porte l'Évangile et la paix de Dieu.

1.
Dans la nuit se lèvera une lumière,
L'espérance habite la terre :
La terre où germera le salut de Dieu !
Dans la nuit se lèvera une lumière,
Notre Dieu réveille son peuple.

2.
L'amitié désarmera toutes nos guerres,
L'espérance habite la terre :
La terre où germera le salut de Dieu !
L'amitié désarmera toutes nos guerres,
Notre Dieu pardonne à son peuple.

3.
La tendresse fleurira sur nos frontières,
L'espérance habite la terre :
La terre où germera le salut de Dieu !
La tendresse fleurira sur nos frontières,
Notre Dieu se donne à son peuple.

4.
Un soleil se lèvera sur nos calvaires,
L'espérance habite la terre :
La terre où germera le salut de Dieu !
Un soleil se lèvera sur nos calvaires,
Notre Dieu fait vivre son peuple.