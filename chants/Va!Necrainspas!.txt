VA! NE CRAINS PAS !
CELUI QUI TE REGARDE EST AMOUREUX.
VA! NE CRAINS PAS !
ET VOGUE VERS LA TERRE OÙ IL FAIT DIEU.

1.
Il fait Dieu au milieu de tes frères,
Ils sont là, Dieu les choisit pour toi.
Cheveux blancs ou bien longue crinière,
À leur danse, viens ajouter ton pas.

2.
Il fait Dieu au coeur de ta prière,
Il est là qui s’invite chez toi.
Viens souvent puiser à la rivière
L’eau de source qui ne se tarit pas.

3.
Il fait Dieu sur bien d’autres rivages,
L’aventure t’ouvre des horizons.
Trouve-le au-delà des visages,
En ce monde, deviens sa vraie chanson.