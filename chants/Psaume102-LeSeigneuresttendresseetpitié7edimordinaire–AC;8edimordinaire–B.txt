LE SEIGNEUR EST TENDRESSE ET PITIÉ.

1.
Bénis le Seigneur, ô mon âme,
bénis son nom très saint, tout mon être !
Bénis le Seigneur, ô mon âme,
n’oublie aucun de ses bienfaits !

2.
Car il pardonne toutes tes offenses
et te guérit de toute maladie ;
il réclame ta vie à la tombe
et te couronne d’amour et de tendresse.

3.
Le Seigneur est tendresse et pitié,
lent à la colère et plein d’amour ;
il n’agit pas envers nous selon nos fautes,
ne nous rend pas selon nos offenses.

4.
Aussi loin qu’est l’orient de l’occident,
il met loin de nous nos péchés ;
comme la tendresse du père pour ses fils,
la tendresse du Seigneur pour qui le craint !