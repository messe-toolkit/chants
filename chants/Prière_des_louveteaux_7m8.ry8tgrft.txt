Seigneur Jésus qui nous aimez si tendrement,
Donnez-nous la grâce d’aimer comme vous,
oui comme vous.

Rendez nos cœurs joyeux pour
 chanter vos merveilles,
Nos mains habiles pour servir,
Nos yeux très doux pour consoler,
Et nos oreilles toutes attentives
à vous écouter.

Accordez-nous de vivre toujours
de notre mieux, de vivre toujours
de notre mieux.
Amen.