1.
Me voici humblement devant toi
Accueillant ta grâce qui se déverse en moi
Car voici ce que j'ai à t'offrir
Un esprit brisé, une terre délaissée,
un soupir.

R.
Tu seras la louange au cœur de mes épreuves
Tu seras la réponse à tout ce que je vis
Tu seras la main qui viendra pour me relever
Tu seras ma vie, mon chemin, ma vérité

2.
Relevé, restauré et guéri
Là dans ta présence, en ta tendresse immense
En ce lieu où mon cœur est béni
Les flots de l'amour sont déversés sur moi,
pour toujours.

P.
Du haut du ciel Tu me bénis
Tu étends ta main avec puissance
Tu fends la mer Tu me saisis
Sans peur, je marche avec confiance (bis)

3.
J'abandonne, en Ton cœur ma prière
Je viens déposer, ma joie et ma misère
Dans mes nuits dans un cri, je t'espère
Et dans mon angoisse,
Sans fin je redirai, par ta grâce :