R.
COMME AU VILLAGE DE MARTHE ET MARIE,
OÙ TU AIMAIS DEMEURER,
TOI, JÉSUS-CHRIST, COMME À BÉTHANIE,
VIENS EN NOS COEURS TE REPOSER.

1.
Tu connais mes pas, tu sais mes sentiers,
Viens me visiter dès aujourd’hui.
Assoiffé de Toi, je te supplierai,
Tu me donneras l’eau de l’Esprit.

2.
Tu me parleras, je t’écouterai,
Tu m’enseigneras dans le secret.
Affamé de Toi, je t’accueillerai,
Tu me combleras de toute paix.

3.
Touche mon regard, dis-moi ta clarté,
Toi qui viens guérir mes yeux blessés.
Rassasié de Toi, je contemplerai,
Tu me rempliras de ta beauté.