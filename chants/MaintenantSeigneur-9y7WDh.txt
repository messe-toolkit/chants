R.
Maintenant, Seigneur,
Tu peux me laisser m´en aller dans la paix.
Maintenant, Seigneur,
Tu peux me laisser reposer.

1.
Tu peux laisser s´en aller ton serviteur
En paix selon ta parole,
Car mes yeux ont vu le salut que tu prépares
À la face des peuples.

2.
Lumière pour éclairer les nations
Et gloire d´Israël ton peuple.
Gloire au Père, et au Fils, et au Saint-Esprit
Pour les siècles des siècles.