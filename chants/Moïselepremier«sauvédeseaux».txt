MOÏSE, LE PREMIER « SAUVÉ DES EAUX »,
ENTRAÎNE TOUT SON PEUPLE
À TRAVERS LA MER,
VERS LA LIBERTÉ.

1.
Une muraille d’eau à droite,
Une muraille d’eau à gauche :
Dieu a sauvé son peuple
De la méchanceté de Pharaon.

2.
Et les Hébreux sont libérés
De la menace du danger.
Dieu les a fait passer
De leur esclavage à la liberté.

3.
La traversée de la Mer Rouge
Est libération pour le peuple.
L’eau vive du baptême
Libère de l’esclavage du péché.

MOÏSE, LE PREMIER « SAUVÉ DES EAUX »
PRÉFIGURE LE CHRIST,
QUI, PAR L’EAU DU BAPTÊME,
NOUS REND LA LIBERTÉ.