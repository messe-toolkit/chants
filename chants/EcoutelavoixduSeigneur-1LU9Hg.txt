1.
Écoute la voix du Seigneur, prête l’oreille de ton cœur.
Qui que tu sois, ton Dieu t’appelle, qui que tu sois, il est ton Père.

Toi qui aimes la vie, Ô toi qui veux le bonheur
Réponds en fidèle ouvrier, de sa très douce volonté.
Réponds en fidèle ouvrier, de l’évangile et de sa paix.

©Droits d'auteur