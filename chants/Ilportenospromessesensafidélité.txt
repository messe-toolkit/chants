Stance
Qui dira le désir infini de Dieu pour son humanité ?
Qui dira la tendresse infinie de Jésus pour son épousée ?
Il porte nos promesses en sa fidélité.

R.
VOUS QUI SAVEZ LE BONHEUR D’ÊTRE AU MONDE,
VIVEZ LA JOIE D’ÊTRE LE CORPS DU CHRIST.

1.
Le Christ aime l’Église,
Il en prend soin.

2.
L’homme s’attache à sa femme,
Tous deux ne feront plus qu’un.

3.
Il est grand ce mystère,
Mystère du Christ et à l’Église.