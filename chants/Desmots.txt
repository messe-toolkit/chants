R.
TOUTES CES LETTRES POUR FORMER DES MOTS À PARTAGER.
DES MOTS QUI BLESSENT, DES MOTS QUI RESTENT,
DES MOTS QUI TOUCHENT ET QUI FONT MOUCHE,
DES MOTS POUR APAISER, POUR OSER DEMANDER.
DES MOTS POUR INVENTER, DES MOTS POUR TÉMOIGNER.

1.
Le “A” pour Accueillir le Monde qui “M” la diversité
Le “O” pour Ouvrir sa porte à toutes ces couleurs mêlées
Et le “U” crie, ravi : « Unis dans l’amitié ! »
Chantons avec de l’ “R” et avec ceux qui espèrent !

2.
Le “P” pour Partager, partager quoi ? tout l’Amour du “A”
Qui a Reçu le “R” dans un grand « Oui ! » au “O” si fier
Les “L” de la Lumière étaient là à briller
Le “E” Enthousiasmé les a rejoints pour chanter !

3.
Le “E” Éclaire une route sans “N”, continuons d’avancer
Se laisser guider par le “V” pour Vivre et s’engager
Emporter avec soi plusieurs milliers de lettres
Dont le “O” pour Offrir et le “I” pour Inviter !