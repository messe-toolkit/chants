Stance
Arbre planté au milieu du jardin,
il regarde vers toi,
l'homme blessé par le serpent :
sur toi, Dieu a fait mûrir
le fruit qui donne la vie.

R.
Croix de Jésus Christ,
notre espérance et notre gloire !

Versets
1° Sur toi le Fils de l'homme a été élevé,
il attire à lui tout l'univers.
2° En te voyant, le soleil s'est obscurci :
tu fais resplendir la Lumière du monde.
3° Dans la grandeur et la puissance,
apparaîtra le signe du Fils de l'homme.