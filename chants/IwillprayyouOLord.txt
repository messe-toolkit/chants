I WILL PRAY YOU O LORD
EXPRESS MY THANKS TO YOU MY LORD
CAUSE YOU’RE MY STRENGTH, YOU’RE MY SONG,
YOU’VE BECOME MY SALVATION
I BLESS AND I’VE CHOSEN YOU, LIVING GOD.

1.
Lord who loves me
Takes care of me
When I was born, you were here
Pater you’ve always been here.

2.
I bless the God for my history
You’ve chosen to live in me
We keep in touch for eternity
You will never leave me

---------------

JE TE PRIERAI, SEIGNEUR,
JE TE REMERCIERAI, SEIGNEUR,
PARCE QUE TU ES MA FORCE, TU ES MON CHANT,
TU ES MON SALUT.
JE TE BÉNIS ET JE TE CHOISIS, DIEU VIVANT

1.
Seigneur qui m’aime,
Qui prend soin de moi,
Quand je suis né, tu étais là,
Père, tu as toujours été là.

2.
Je bénis le Dieu pour mon histoire.
Tu as choisi de vivre en moi,
On reste en contact pour l’éternité.
Tu ne me quitteras jamais.