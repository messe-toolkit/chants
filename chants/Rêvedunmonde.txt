Refrain :
Rêve d’un monde,
Monde plus beau à faire ensemble.
Rêve d’un monde,
Monde nouveau.

1.
Aimer la vie, aimer ensemble,
Donner sa vie, donner ensemble,
Choisir la vie, choisir ensemble,
Aimer la vie, toujours donner sa vie
Pour un monde plus beau !
J’ai fait le...

2.
Payer de soi, payer ensemble
Risquer de soi, risquer ensemble,
Chercher en soi, chercher ensemble,
Payer de soi, toujours risquer de soi
Pour un monde plus beau,
J’ai fait le...

3.
Changer nos coeurs, changer ensemble,
Rester veilleur, rester ensemble,
Marcher sans peur, marcher ensemble,
Changer nos coeurs, toujours rester veilleur
Pour un monde plus beau,
J’ai fait le...

4.
Bâtir demain, bâtir ensemble,
Ouvrir les mains, ouvrir ensemble,
Fleurir soudain, fleurir ensemble,
Bâtir demain, toujours ouvrir les mains
Pour un monde plus beau,
J’ai fait le...