1.
Ça sent bon la paille, ça sent bon le foin,
Ça sent bon la laine, ça sent bon le pain.
Ça sent bon les langes, le bébé tout chaud,
Ça sent bon les planches et les animaux.

C’EST UN AIR DE CHANSON
C’EST UN AIR VENU DU CIEL
QU’ON RESPIRE À PLEINS POUMONS
À NOËL !
C’EST UN AIR DE CHANSON
C’EST UN AIR VENU DU CIEL
QU’ON RESPIRE À PLEIN COEUR
À NOËL !

2.
Ça sent bon la sève, ça sent les copeaux,
Ça sent bon le chêne, ça sent le bouleau.
La sueur de l’homme, le travail bien fait,
Ça sent bon la cire, le linge plié.

3.
Puis ça sent les larmes et le sang versé,
Ça sent la poussière, le corps déchiré.
Ça sent l’agonie, la mort et le bois,
La fleur de la vie poussant sur la croix.