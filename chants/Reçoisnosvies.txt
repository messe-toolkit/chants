Reçois nos vies

Paroles et musique : Communauté de l’Emmanuel (Astrid Broeders)
No. 23-08

Reçois nos vies,
Nous voici devant toi.
Reçois nos vies,
Tu nous attires à toi.
Reçois nos vies,
Seigneur, tout vient de toi.

Nous voulons te suivre
Chaque instant de notre vie,
Marcher avec toi, ô Seigneur,
Tels que nous sommes,
Montre-nous la route,
Montre-nous le chemin.
Reçois nos vies
Toi, seul tu es Dieu !