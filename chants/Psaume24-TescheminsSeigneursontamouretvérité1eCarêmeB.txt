TES CHEMINS, SEIGNEUR,
SONT AMOUR ET VÉRITÉ
POUR QUI GARDE TON ALLIANCE.

1.
Seigneur, enseigne-moi tes voies,
fais-moi connaître ta route.
Dirige-moi par ta vérité, enseigne-moi,
car tu es le Dieu qui me sauve.

2.
Rappelle-toi, Seigneur, ta tendresse,
ton amour qui est de toujours.
Dans ton amour, ne m’oublie pas,
en raison de ta bonté, Seigneur.

3.
Il est droit, il est bon, le Seigneur,
lui qui montre aux pécheurs le chemin.
Sa justice dirige les humbles,
il enseigne aux humbles son chemin.

TA PAROLE, SEIGNEUR, EST VÉRITÉ,
ET TA LOI, DÉLIVRANCE.

L’homme ne vit pas seulement de pain,
mais de toute parole qui sort de la bouche de Dieu.