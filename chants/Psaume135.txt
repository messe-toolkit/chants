1.
Rendez grâce au Seigneur : il est bon,
Car éternel est son amour !
Rendez grâce au Dieu des dieux,
Car éternel est son amour !
Rendez grâce au Seigneur des seigneurs,
Car éternel est son amour !

2.
Lui seul a fait de grandes merveilles,
Car éternel est son amour !
lui qui fit les cieux avec sagesse,
Car éternel est son amour !
qui affermit la terre sur les eaux,
Car éternel est son amour !

3.
Lui qui a fait les grands luminaires,
Car éternel est son amour !
le soleil qui règne sur le jour,
Car éternel est son amour !
la lune et les étoiles, sur la nuit,
Car éternel est son amour !

4.
Lui qui frappa les Égyptiens dans leurs aînés,
Car éternel est son amour !
et fit sortir Israël de leur pays,
Car éternel est son amour !
d’une main forte et d’un bras vigoureux,
Car éternel est son amour !

5.
Lui qui fendit la mer Rouge en deux parts,
Car éternel est son amour !
et fit passer Israël en son milieu,
Car éternel est son amour !
y rejetant Pharaon et ses armées,
Car éternel est son amour !

6.
Lui qui mena son peuple au désert,
Car éternel est son amour !
qui frappa des princes fameux,
Car éternel est son amour !
et fit périr des rois redoutables,
Car éternel est son amour !

7.
Pour donner leur pays en héritage,
Car éternel est son amour !
en héritage à Israël, son serviteur,
Car éternel est son amour !
il se souvient de nous, les humiliés,
Car éternel est son amour !

8.
Il nous tira de la main des oppresseurs,
Car éternel est son amour !
à toute chair, il donne le pain,
Car éternel est son amour !
rendez grâce au Dieu du ciel,
Car éternel est son amour !