Sur les chemins de dissemblance
Quelqu’un nous a cherchés.
Reconnaissant l’Autre Présence
L’enfant s’est éveillé :
Au fond du cœur il a bougé
Pour se tourner vers la lumière.

Dans l’infinie miséricorde
Où brûlent nos péchés,
Notre existence enfin s’accorde
Au chant du Bien-Aimé :
L’enfant perdu s’est relevé
Pour avancer vers la lumière.

Quand nous étreint la solitude,
Quelqu’un nous ressaisit.
Bien au-delà des certitudes
La force de l’Esprit.
L’enfant blessé a tressailli,
Sa nuit ruisselle de lumière.

Passe le temps de l’espérance,
Voici l’éternité !
Pour achever la ressemblance,
Marie s’est approchée :
L’enfant de Dieu s’en vient paré
D’une tunique de lumière.