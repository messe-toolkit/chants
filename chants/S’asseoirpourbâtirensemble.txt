S’ASSEOIR POUR BÂTIR ENSEMBLE
POUR ACCUEILLIR DANS NOS VIES
CELUI QUE NOS COEURS CONTEMPLENT
ET NOUS ENVOIE PAR L’ESPRIT.

1.
Partir pour le découvrir
« Sans Lui ! Je n’ai de repos ! »
Notre force pour grandir
Emmène-nous Dieu Très Haut !

2.
Choisir de Lui appartenir
Notre souhait d’avenir
Devenir meilleur humain
Mettre nos pas dans les siens

3.
Aimer comm’Jésus nous aime
Emprunter Sa voie pérenne
Refuser la misère
Embraser toute la Terre

4.
Construire sur des bonnes bases
S’assurer d’être bien en phase
Élevés vers les cieux
Chantons la Gloire de Dieu

DEBOUT ! ALLONS ! TOUS ENSEMBLE
ANNONCER PAR NOTRE VIE
CELUI QUE NOS COEURS CONTEMPLENT
ET NOUS ENVOIE PAR L’ESPRIT.