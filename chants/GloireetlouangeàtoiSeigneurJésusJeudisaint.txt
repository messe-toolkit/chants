GLOIRE ET LOUANGE À TOI,
SEIGNEUR, JÉSUS.

Tu nous donnes un commandement nouveau :
Aimez-vous les uns les autres
Comme je vous ai aimés.