MON ÂME EXULTE EN MON DIEU.

1.
Mon âme exalte le Seigneur,
exulte mon esprit en Dieu, mon Sauveur !
Il s’est penché sur son humble servante ;
désormais tous les âges me diront bienheureuse.

2.
Le Puissant fit pour moi des merveilles ;
Saint est son nom !
Sa miséricorde s’étend d’âge en âge
sur ceux qui le craignent.

3.
Il comble de biens les affamés,
renvoie les riches les mains vides.
Il relève Israël son serviteur,
il se souvient de son amour.