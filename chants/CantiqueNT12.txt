Antienne ;
Alléluia ! Alléluia ! Alléluia !

1.
Salut, puissance, honneur, louange à notre Dieu !
Ils sont justes et vrais ses jugements.

2.
Célébrez le Seigneur, serviteurs du Seigneur !
Vous tous qui le craignez, petits et grands.

3.
Il règne notre Dieu, le Seigneur tout puissant !
Exultons pleins de joie, rendons-lui gloire !

4.
Voici venir déjà les noces de l'Agneau :
Son épouse pour lui s'est faite belle.

5.
Gloire au Père, au Fils et au Saint Esprit,
Au Dieu qui est, qui était et qui vient, pour les siècles des siècles, Amen !