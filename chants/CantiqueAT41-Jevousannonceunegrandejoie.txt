JE VOUS ANNONCE UNE GRANDE JOIE :
AUJOURD'HUI VOUS EST NÉ LE SAUVEUR DU
MONDE !
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA.

1.
Toutes les oeuvres du Seigneur,
bénissez le Seigneur :
À lui, haute gloire, louange éternelle !

2.
Vous, les anges du Seigneur,
bénissez le Seigneur :
À lui, haute gloire, louange éternelle !

3.
Vous, les cieux,
bénissez le Seigneur,
et vous, les eaux par-dessus le ciel,
bénissez le Seigneur,
et toutes les puissances du Seigneur,
bénissez le Seigneur !

4.
Et vous, le soleil et la lune,
bénissez le Seigneur,
et vous, les astres du ciel,
bénissez le Seigneur,
vous toutes, pluies et rosées,
bénissez le Seigneur !

5.
Vous tous, souffles et vents,
bénissez le Seigneur,
et vous, le feu et la chaleur,
bénissez le Seigneur,
et vous, la fraîcheur et le froid,
bénissez le Seigneur !

6.
Et vous, le givre et la rosée,
bénissez le Seigneur,
et vous, le gel et le froid,
bénissez le Seigneur,
et vous, la glace et la neige,
bénissez le Seigneur !

7.
Et vous, les nuits et les jours,
bénissez le Seigneur,
et vous, la lumière et les ténèbres,
bénissez le Seigneur,
et vous, les éclairs, les nuées,
bénissez le Seigneur !
À lui, haute gloire, louange éternelle !

8.
Que la terre bénisse le Seigneur :
À lui, haute gloire, louange éternelle !

9.
Et vous, montagnes et collines,
bénissez le Seigneur,
et vous, les plantes de la terre,
bénissez le Seigneur,
et vous, sources et fontaines,
bénissez le Seigneur !

10.
Et vous, océans et rivières,
bénissez le Seigneur,
baleines et bêtes de la mer,
bénissez le Seigneur,
Vous tous, les oiseaux dans le ciel,
bénissez le Seigneur,
vous tous, fauves et troupeaux,
bénissez le Seigneur !
À lui, haute gloire, louange éternelle !

11.
Et vous, les enfants des hommes,
bénissez le Seigneur :
À lui, haute gloire, louange éternelle !

12.
Toi, Israël,
bénis le Seigneur !
et vous, les prêtres,
bénissez le Seigneur,
vous, ses serviteurs,
bénissez le Seigneur !

13.
Les esprits et les âmes des justes,
bénissez le Seigneur,
les saints et les humbles de coeur,
bénissez le Seigneur,
Ananias, Azarias et Misaël,
bénissez le Seigneur !
À lui, haute gloire, louange éternelle !

14.
Bénissons le Père, le Fils, et l'Esprit-Saint :
À lui, haute gloire, louange éternelle !
Béni sois-tu, Seigneur, au firmament du ciel :
À toi, haute gloire, louange éternelle !