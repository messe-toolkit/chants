1.
Tant que durent mes jours
Au temps qui passe,
Je veux chanter le Dieu qui dure
D'âge en âge.
Dieu fort sur les puissants de la terre,
Dieu du peuple choisi,
Heureux qui te prend pour appui !

2.
Tant que dure ma foi
Au Dieu de Pâque,
Je veux chanter le Dieu qui passe
D'âge en âge.
Seigneur du premier jour de la terre,
Dieu fidèle à tes choix,
Heureux qui espère ta joie !

3.
Tant que passe vers nous
Le Dieu de grâce,
Je veux chanter l'amour qui sauve
D'âge en âge.
Salut pour l'opprimé de la terre,
Pain des cœurs affamés,
Délivre nos cœurs prisonniers.

4.
Tant que dure ma nuit
De marche aveugle,
Je veux crier le jour qui lève
D'âge en âge.
Soleil à l'horizon de nos terres,
Dieu qui passes en ami,
Éclaire l'obscur de nos vies.