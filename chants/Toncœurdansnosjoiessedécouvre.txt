1.
Ton cœur dans nos joies se découvre,
Seigneur, l'avons-nous reconnu ?
Comment ramener vers leur source
Les biens que nos mains ont reçus ?

2.
Un seul peut traduire en louange
Le don et le poids de ce jour ;
Un seul vient combler ton attente,
Seigneur et répond à l'amour.

3.
Jésus, au profond de nous-mêmes,
Saisit notre chant dans le sien ;
Ainsi ta Parole éternelle
Prend corps dans nos mots quotidiens.

4.
Sa voix rend plus haute et plus vaste,
Au souffle puissant de l'Esprit,
L'obscure espérance des âges,
Qui passe et reflue sur nos vies.

5.
Les cris et les pleurs qu'on étouffe,
Par lui monteront jusqu'à toi ;
Ses mains t'offriront notre coupe
Remplie de douleur et de joie.

6.
Qu'il soit ta lumière en ce monde,
Ton cœur et le nôtre à la fois ;
Qu'il soit aujourd'hui ta réponse,
Seigneur, comme il est notre voix.