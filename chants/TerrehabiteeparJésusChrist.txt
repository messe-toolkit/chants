1.
La terre avec ses arbres et ses oiseaux,
La terre avec ses lacs et ses ruisseaux,
Trésor de Dieu
Remis entre nos mains,
Planète bleue,
Promesse pour demain !
TERRE HABITÉE PAR JÉSUS CHRIST
TOUJOURS À L’OEUVRE PARMI NOUS,
TERRE OÙ LA BRISE DE L’ESPRIT
MURMURE AU MONDE UN CHANT D’AMOUR,
NOTRE TERRE AUJOURD’HUI! (bis)

2.
La terre avec ses monts et ses vallées,
La terre où le bon grain pourra germer,
Grenier de Dieu,
Richesse du semeur,
Planète bleue,
Maison de vrai bonheur !

3.
La terre au grand manteau de lys des champs,
La terre aux fruits nouveaux pour les vivants,
Verger de Dieu
Que l’homme peut flétrir,
Planète bleue,
Vas-tu toujours fleurir ?

4.
La terre avec sa lèpre et son ivraie,
La terre où tant de vies sont menacées,
Jardin de Dieu
Sans cesse à rajeunir,
Planète bleue,
Quel est ton avenir ?

5.
La terre où disparaissent des forêts,
La terre aux mégapoles et aux déserts,
Cité de Dieu
Que nous devons bâtir,
Planète bleue,
À nous de te servir !