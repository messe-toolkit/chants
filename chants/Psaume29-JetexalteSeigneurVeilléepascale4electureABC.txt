JE T’EXALTE, SEIGNEUR : TU M’AS RELEVÉ.

1.
Quand j’ai crié vers toi, Seigneur,
mon Dieu, tu m’as guéri ;
Seigneur, tu m’as fait remonter de l’abîme
et revivre quand je descendais à la fosse.

2.
Fêtez le Seigneur, vous, ses fidèles,
rendez grâce en rappelant son nom très saint.
Sa colère ne dure qu’un instant,
sa bonté, toute la vie.

3.
Avec le soir, viennent les larmes,
mais au matin, les cris de joie.
Tu as changé mon deuil en une danse,
mes habits funèbres en parure de joie.

4.
Que mon coeur ne se taise pas,
qu’il soit en fête pour toi,
et que sans fin, Seigneur, mon Dieu,
je te rende grâce !

Ancienne antienne :
JE T’EXALTE, SEIGNEUR, TOI QUI ME RELÈVES.