1.
Est-ce l’histoire d’un grain de blé semé en terre
Un grain de blé, un grain de riz, une poussière
Un grain d’amour, un grain de vie, un grand mystère
Trouvant sa place, trouvant son nid pour la vie

C’EST LA VIE QUI FRAPPE À LA PORTE
C’EST LA VIE QUI CHANTE ET QUI SOURIT
ALLONS VITE OUVRIR LA PORTE
BIENVENUE, BIENVENUE LA VIE

2.
Un peu de terre, trois gouttes d’eau, de la chaleur
De la patience et du silence, c’est bientôt l’heure
Petit bourgeon ou premier cri, première fleur
Chacun son tour, petit à petit vit sa vie

3.
Au creux de toi, tu sens aussi que la vie
Prend des couleurs, prend du bonheur et tu grandis
Et tout autour, que les parents, que les amis
Donnent de la joie, donnent de l’amour à la vie

4.
Passe le temps, passent les jours et les saisons
Les champs de blés, les chants d’amour, les floraisons
S’en vient la pluie, le soleil fond à l’horizon
Danse l’automne et tourbillonne la vie

5.
C’est bien l’histoire d’un grain de blé semé en terre
Poussant la vie, poussant la mort, poussant la pierre
Un grain d’amour, un grain de vie, un grand mystère
Trouvant sa place au nouveau jour : c’est la Vie