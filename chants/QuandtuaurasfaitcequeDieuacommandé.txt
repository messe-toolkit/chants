Antienne - stance
Quand tu auras fait ce que Dieu a commandé, tu diras : Je suis un serviteur inutile.

R.
Dona nobis pacem cordium, Domine Deus.

Versets

Ps 115

1.
Je crois, et je parlerai,
moi qui ai beaucoup souffert,
moi qui ai dit dans mon trouble :
" L'homme n'est que mensonge. "

2.
Comment rendrai-je au Seigneur
tout le bien qu'il m'a fait ?
J'élèverai la coupe du salut,
j'invoquerai le nom du Seigneur.

3.
Il en coûte au Seigneur
de voir mourir les siens !
Ne suis-je pas, Seigneur, ton serviteur,
ton serviteur, le fils de ta servante ?

4.
Je t'offrirai le sacrifice d'action de grâce,
j'invoquerai le nom du Seigneur.
Je tiendrai mes promesses au Seigneur,
oui, devant tout son peuple.

Ou bien

Ps 99

1.
Acclamez le Seigneur, terre entière,
servez le Seigneur dans l'allégresse,
venez à lui avec des chants de joie !

2.
Reconnaissez que le Seigneur est Dieu :
il nous a faits, et nous sommes à lui,
nous, son peuple, son troupeau.

3.
Oui, le Seigneur est bon,
éternel est son amour,
sa fidélité demeure d'âge en âge.