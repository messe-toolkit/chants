VERS TOI, SEIGNEUR,
L’ÉLAN DE MA VIE,
TU ES LE DIEU DE MA FOI.

1.
Ton coeur ne peut nous décevoir,
Tu es plus fort que nos oublis.
Seigneur plus grand que nos espoirs,
Tu es fidèle à tes amis.

2.
Mon Dieu, prends-moi sur tes chemins,
Par tes sentiers conduis mes pas.
Révèle-moi le vrai Chemin,
Celui qui sauve et mène à toi !

3.
Mes yeux t’espèrent tout le jour,
Dieu de tendresse et de bonté.
Enseigne-moi ta voie d’amour,
Qui me libère en vérité.

4.
Seigneur, tu es le Dieu de vie ;
Heureux tout homme qui le croit !
Son âme s’ouvre à l’infini,
Dans ta maison tu le reçois.