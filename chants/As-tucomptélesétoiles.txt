1.
As-tu compté les étoiles
Et les astres radieux
Déployant aux nuits sans voiles
Leur cortège dans les cieux ?
Dieu qui leur donna
La vie et l’éclat
Dieu qui leur fixa
La course et le pas
Sait aussi quel est leur nombre
Et ne les oublie pas.

2.
As-tu compté les abeilles
Butinant parmi les fleurs ?
Papillons, mouches vermeilles
Sans soucis et travailleurs ?
Dieu qui les vêtit
Couleur paradis
Dieu qui leur fournit
Vivres et logis
Sait aussi quel est leur nombre
Et ne les oublie pas.

3.
As-tu compté les fleurettes
Souriant au gai printemps ?
Bouton-d’or et pâquerettes
Fleurs des bois et fleurs des champs?
Celui qui leur fit
Ces riches habits
Celui qui leur mit
Ces frais coloris
Sait aussi quel est leur nombre
Et ne les oublie pas.

4.
As-tu compté les nuées
Passant dans les champs du ciel ?
Et les gouttes de rosée
Aux reflets de l’arc-en-ciel ?
Dieu qui fait le temps
Sombre ou éclatant
Le ruisseau chantant
Et les flots grondant
Sait aussi quel est leur nombre
Et ne les oublie pas.

5.
Sais-tu combien sur la terre
Vivent d’enfants comme toi ?
Dans le luxe ou la misère
Fils de pauvres, fils de rois ?
Dieu les connaît tous
Et les aime tous
Dieu les garde tous
Et Dieu les veut tous
Sait aussi quel est leur nombre
Et ne les oublie pas ?