R.
BÂTIR UN AVENIR SOLIDAIRE,
CREUSER DES SILLONS D’HUMANITÉ,
OUVRIR DES LIEUX DE VIE AVEC NOS FRÈRES :
C’EST NOTRE ESPÉRANCE PARTAGÉE.

1.
Que chacun trouve sa place en nos villages,
Nous verrons grandir un monde citoyen ;
Monde neuf qui se construit sur le partage :
Vive ensemble donnera visage humain. (bis)

2.
Que jaillissent des projets sur notre Terre
Au service de la paix dans les nations !
Mais la paix sans la justice n’est qu’un rêve ;
Aujourd’hui, tissons des liens de communion ! (bis)

3.
Solidaires pour planter les droits de l’homme
Nous savons le poids du mot “Fraternité”.
Les exclus, les étrangers de toutes sortes
Nous diront comment ils voient leurs libertés. (bis)

4.
Accueillir ce que la terre nous apporte,
Respecter tous ses trésors durablement,
Promouvoir la vie qui monte et qui prend forme,
C’est ainsi que nous serons un peuple grand. (bis)

5.
Près de Dieu nous puiserons notre espérance ;
Créateur il est à l’oeuvre à nos côtés.
Grâce à lui nous avançons dans la confiance,
Chaque jour est un défi à relever. (bis)

6.
Jésus-Christ nous met au coeur son Évangile,
Sa parole crie l’amour qu’il a donné.
En sauveur, il veut que l’homme puisse vivre
Et marcher sur un chemin de dignité. (bis)