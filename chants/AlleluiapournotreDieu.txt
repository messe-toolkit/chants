R.
ALLELUIA, POUR NOTRE DIEU,
ALLELUIA, JESUS SEIGNEUR !
ALLELUIA ! ALLELUIA !
ALLELUIA ! ALLELUIA !

1.
Enfants de Dieu, venons à la fête,
allons vers lui, sa porte est ouverte.
Dieu nous accueille en sa maison,
il nous connaît par notre nom,

2.
Enfants de Dieu, Jésus nous rencontre ;
écoutons le, sachons lui répondre,
Dieu nous apprend quel est son Nom
il est Amour, il est pardon,

3.
Enfants de Dieu, portons nos demandes,
levons vers lui nos mains suppliantes
Dieu tend l'oreille aux plus petits,
il sait entendre tous nos cris.

4.
Enfants de Dieu, voici notre offrande,
que sur nos dons sa grâce descende,
Jésus s'est fait vivante hostie,
offrons par lui nos humbles vies.

5.
Enfants de Dieu,venons rendre grâce,
bénissons le, chantons notre Pâques.
Pour nous, son fils est descendu,
Sa croix nous donne le salut.

6.
Enfants de Dieu, gardons en mémoire,
ce que Dieu fait pour dire sa gloire.
Il est vivant le crucifié,
Par lui le monde est rénové,

7.
Enfants de Dieu, venons à la table,
nous recevrons le pain du partage,
Le blé de Dieu nous rendra forts,
nous serons membres de son corps,

8.
Enfants de Dieu, buvons à la source,
goûtons joyeux au vin de la coupe,
L'esprit de Dieu nous comblera,
la joie du Christ en nous naîtra.

9.
Enfants de Dieu, portons la Nouvelle,
soyons témoins de Dieu qui nous aime
C'est par l'amour que nous dirons
si notre coeur est communion.