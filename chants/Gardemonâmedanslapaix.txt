R.
Garde mon âme dans la paix, près de loi, Seigneur

Psaume 130.
Seigneur, je n'ai pas le coeur fier
ni le regard ambitieux;

Je ne poursuis ni grands desseins,
ni merveilles qui me dépassent,

Non, mais je tiens mon âme
égale et silencieuse;

Mon âme est en moi comme un enfant,
comme un petit enfant contre sa mère,

Attends le Seigneur, Israël,
maintenant et à jamais,