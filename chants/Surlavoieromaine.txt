1.
Un matin, sur la voie romaine
Je me suis posé un peu lourd
Essoufflé par ces jours de peine
Après de multiples détours
Mon coeur a besoin de repos
Mon coeur a besoin de repos
De repos et d’amour.
Un matin, sur la voie romaine
Encore étourdi un peu sourd
J’ai senti couler dans mes veines
Un sang nouveau, un flux d’amour
« Toi et les tiens, gardez-moi !
Juste quelques jours ! »

Si tu voyais chez moi, mes frères
Ne savent même plus voler
Ils sont accrochés aux gouttières
Qu’attendent-ils les yeux fermés ?
Si tu voyais chez moi, les Hommes
Ne font plus le rêve de voler
Ils marchent droit les yeux baissés
Pourtant la ville est tellement belle
Il suffit de changer d’échelle
Mais qui osera… qui osera…
Ouvrir ses ailes ?

2.
Plein soleil sur la voie romaine
J’ai cessé de battre des ailes
Tu m’as dit : « Je connais ta peine
Viens la chanter sous notre ciel
Ton coeur a besoin de repos
Ton coeur a besoin de repos
De repos et d’amour »
Plein soleil sur la voie romaine
Le coeur battant contre la pierre
J’ai senti couler dans mes veines
Comme la sève après l’hiver
« Toi et les tiens, gardez-moi !
Encore quelques jours ! »

Si tu voyais chez moi, mes frères
Ne savent même plus chanter
Au coeur des bruits pleins de poussière
Auraient-ils peur de déranger ?
Si tu voyais chez moi, les Hommes
N’ont aucune raison de chanter
Le masque sombre, ils sont pressés
Fuyant l’instinct, le naturel
Leur silence est-il sans appel ?
Mais qui osera… qui osera…
Ouvrir ses ailes ?

3.
Un instant, sur la voie romaine
Comme un fou j’ai imaginé
Que sous ton ciel sans trop de peine
Je pourrais apprendre à voler
Mon coeur s’est laissé prendre au piège
Mon coeur s’est laissé prendre au piège
Au piège de l’amour
Loin de vous, de la voie romaine
Je me suis posé un peu lourd
Le sang s’est glacé dans mes veines
Ce n’était qu’un aller-retour
« Toi et les tiens, n’êtes plus
Qu’un rêve trop court ! »

Si tu voyais d’ici, la ville
Est comme un désert blanc et froid
J’ai beau chanter, c’est inutile
Les Hommes ne s’arrêtent pas
Pourtant je chante nuit et jour
Et je m’essouffle sur les toits
Mes frères s’endorment près de moi
Mon coeur s’épuise sous le ciel
Il reste en moi ton étincelle
Mais quand viendra
Le temps pour moi,
Le temps pour moi…
D’ouvrir mes ailes