1.
Encore un peu de temps, le Seigneur sera là
Trouvera-t-il en nous la foi qu'il espère ?
Si nous avons bâti nos maisons sur le sable
Quand il fallait creuser le roc de vérité.
Pourrons-nous soutenir l'assaut du torrent.

2.
Encore un peu de temps, le Seigneur sera là
Tous nos secrets viendront en pleine lumière.
Si nous avons dormi, prisonniers de nos rêves
Quand il fallait veiller aux heures d'agonie,
Pourrons-nous supporter l'éclat de son jour ?

3.
Encore un peu de temps, le Seigneur sera là
Pour annoncer la paix aux hommes qu'il aime.
Si nous avons dressé entre nous des barrières
Quand il fallait ouvrir sa porte à l'étranger,
Pourrons-nous contempler la face de Dieu ?

4.
Encore un peu de temps, le Seigneur sera là
Comme apparaît l'ami fidèle aux promesses.
Si nous avons déçu l'espérance des pauvres
Quand il fallait donner la preuve de l'amour,
Pourrons-nous accueillir la main qui se tend ?

5.
Encore un peu de temps, le Seigneur sera là :
Eucharistie de joie qui sauve le monde.
Si nous avons laissé son regard de tendresse
Illuminer enfin l´abîme de nos coeurs,
Nous pourrons célébrer la Pâque éternelle.
