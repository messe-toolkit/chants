R.
A toi la parole, Dieu écrit ta liberté,
A toi la parole, il nous a aimés.
Il tient parole, le livre n'est plus fermé
Notre Dieu s'est donné

1.
Ils sont une humanité à attendre une parole
Qui viendrait les libérer, leur apprendrait à s'aimer.
Ils sont des hommes affamés, à attendre une parole
Qui voudrait les rassasier, leur apprendre à partager.

2.
Ils sont un peuple en colère, à attendre une parole
Qui viendrait comme la paix, leur apprendre à pardonner.
Ils sont au fond de la nuit, à attendre une parole
Qui ouvrirait le passage, allumerait des feux de joie.

3.
Ils sont un peuple emmuré, à attendre une parole
Qui saurait les dénouer, inviterait à danser
Ils sont des cris étouffés, à attendre une parole
Qui saurait les écouter, leur apprendrait à parler.