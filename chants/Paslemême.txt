1.
J’ai couru comme un fou avant que le verrou
Ne referme la porte en fer forgé…
J’ai poussé le gardien et j’ai tué le chien
Dont les crocs s’apprêtaient à m’étrangler.
J’ai franchi le couloir, avançant dans le noir
Au milieu des cris de désespoir…
J’ai sauté sur un mur hérissé de ferrures acérées,
M’entaillant comme des lames…
J’ai rampé à genou dans des conduits d’égouts
Pour éviter l’acier de leurs armes…
J’ai marché essoufflé jusqu’à la voie ferrée
Là, j’ai bien cru que c’était gagné !
Qu’ai-je pris, qu’ai-je fait ?
Sans histoire, je vivais,
Qu’ai-je dit, dites-moi qu’ai-je fait ?

R.
PAS LE MÊME PAYS, PAS LE MÊME PARTI
PAS LE MÊME DIEU, NON PAS LE MÊME.
PAS LES MÊMES IDÉES, PAS LES MÊMES HABITS,
PAS LA MÊME LANGUE, NON PAS LA MÊME.
PAS LE MÊME ACCENT, PAS LE MÊME GENRE,
PAS LE MÊME SANG, NON PAS LE MÊME.
PAS LA MÊME COULEUR, PAS LA MÊME ODEUR,
PAS LA MÊME PEAU, NON PAS LA MÊME.

2.
J’ai agrippé des mains la portière d’un train
Qui roulait dans la nuit finissante…
La patrouille déjà… j’ai senti dans le bras
La brûlure d’une balle traçante…
Sur le toit, allongé, du wagon enneigé,
À bout de force je suis tombé…
Je me lève et j’ai froid…
Une maison là-bas…
Un homme est devant moi…
Je suis sauvé !
Par pitié cachez-moi pour l’amour ou le droit !
Je ne veux plus encore être torturé !
Il m’assomme, violent, il appelle des gens qui me frappent
Et m’enchaînent en riant.
Qu’ai-je pris, qu’ai-je fait ?
Sans histoire, je vivais,
Qu’ai-je dit, dites-moi qu’ai-je fait ?