R.
Béni soit l'Esprit de Dieu,
Qui nous mène vers le Père !
Que Jésus, lumière et feu,
Soit pour nous la joie parfaite !

1.
Joie de chercher Dieu
Dès le jour de nos baptêmes !
Joie d'ouvrir nos coeurs
À des voix qui le révèlent !
Joie de l'écouter
En tout lieu d'humanité !

2.
Joie de vivre en Dieu,
Il nous aime avec tendresse !
Joie d'aller vers ceux
Qui ont soif d'une sagesse !
Joie d'être attentifs
À leur faim de la vraie Vie !

3.
Joie de croire en Dieu
Même au creux de nos détresses !
Joie d'un regard neuf
Sur nos jours si éphémères !
Joie de dépasser
Les brouillards de nos années !

4.
Joie de trouver Dieu
Dans les luttes et les exodes !
Joie d'aller au feu
Dans le champ des droits de l'homme !
Joie d'être habitées
Par Justice et Vérité !

5.
Joie de louer Dieu
Pour ce monde et ses merveilles !
Joie d'être audacieux
Dans nos choix pour la planète !
Joie de s'engager
Sur la terre à réveiller !

6.
Joie d'offrir à Dieu
L'humble encens de nos prières !
Joie des chants joyeux
Qui relèvent l'homme en peine !
Joie des coeurs brûlants,
Pleins d'espoir pour les vivants !

7.
Joie d'annoncer Dieu
Par des mots et par des signes !
Joie de dire "Heureux
Les témoins de l'Évangile !"
Joie quand notre vie
Sait parler de l'infini !

8.
Joie d'attendre Dieu,
L'Oméga de toutes choses !
Joie de voir nos voeux
Exaucés au Jour des Noces !
Joie d'être invitées
Au festin du Bien-Aimé !
