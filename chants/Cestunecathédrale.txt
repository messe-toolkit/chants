Refrain :
C’est une cathédrale,
Elancée vers le ciel,
C’est une cathédrale,
Inondée de soleil,
Qui nous dit l’essentiel...

1.
Si l’amour est prière,
Nous serons les vitraux.
“ Il ” sera lumière,
Et tout sera plus beau...

2.
Si l’amour est silence,
Dans la nef et le choeur.
“ Il ” sera la présence,
Et nous serons veilleurs…

3.
Si l’amour est message,
Comme un tympan sculpté.
Nous serons le visage,
Et l’espace sacré…

4.
Si l’amour est mystère,
Au coeur de la maison.
Nous serons chaque pierre,
“ Il ” sera le maçon…