Stance
Fils de l’homme livré aux mains des hommes,
comment ne pas trembler
quand tu nous parles de ta mort ?
Comment te suivre au dur sentier
où le plus grand est le dernier ?
Comment siéger tout près de toi
sans boire à cette coupe que tu bois ?

Refrain
Pour te servir dans tous nos frères,
Donne-nous un coeur aimant !
Pour t’accueillir comme Dieu-même,
Donne-nous un coeur d’enfant.

Versets

1.
Seigneur, je n’ai pas le coeur fier
ni le regard hautain.
Je n’ai pas pris un chemin de grandeur
ni de prodiges qui me dépassent.

2.
Non, je tiens mon âme
en paix et silence.
Mon âme est en moi comme un enfant,
comme un petit enfant contre sa mère.

3.
Les grands font sentir leur pouvoir.
Parmi vous il ne doit pas en être ainsi ;
celui qui veut devenir grand
sera votre serviteur.

4.
Le fils de l’homme n’est pas venu pour être servi,
mais pour servir,
et donner sa vie
en rançon pour la multitude.