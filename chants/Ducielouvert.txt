R.
Du ciel ouvert a retenti la voix du Père :
Voici mon Fils bien-aimé.

1.
Aujourd'hui le Seigneur fut baptisé dans le Jourdain,
Et Jean vit l'Esprit-Saint descendre des cieux,
Tel une colombe et reposer sur lui.

2.
C'est lui qui nous baptise dans l'eau et l'Esprit-Saint,
C'est lui le Fils de Dieu ! Mers, fleuves et rivières,
Sources et fontaines, bénissez le Seigneur.

3.
Rendons gloire au Père tout puissant,
À son Fils Jésus-Christ, Sauveur du monde,
À l'Esprit qui habite en nos cœurs.