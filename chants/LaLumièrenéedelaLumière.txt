Stance : La lumière née de la lumière
apparaît dans la nuit de Judée ;
la Parole éternelle du Père
nous appelle avec les bergers.
Allons voir dans les bras de Marie
Dieu qui se fait l’un de nous,
pour que nous vivions de sa vie.

Ref. Notre Sauveur est né pour nous,
paix sur la terre et gloire à Dieu !

1.
Écoutez la voix des guetteurs,
leur appel retentit en un seul cri de joie.

2.
La lumière s’est levée pour le juste,
et pour l’homme au coeur droit, la joie.

3.
Le Seigneur a consolé son peuple,
éclatez en cris de joie.

4.
Acclamez le Seigneur, terre entière,
sonnez musique et chants de joie.