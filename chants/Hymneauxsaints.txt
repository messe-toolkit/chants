1.
Comme vous, préférer la vraie route
Et croiser nos chemins différents.
Avec vous cheminer à l’écoute,
Annoncer la Parole à son temps.
Et prier : qu’il n’y ait de voyage
Sans l’amour qui s’avance avec vous ;
Pour porter clairement le message :
Le Royaume est offert, comme vous !

2.
Comme vous, s’effacer devant l’autre ;
Lui donner le regard attendu.
Avec vous, devenu coeur de pauvre,
Mendier pour celui qui est nu.
Et prier : qu’il n’y ait de richesse
Que l’amour partagé avec vous ;
Oublier de ce temps la sagesse,
Pour aimer la folie, comme vous !

3.
Comme vous, quand le mal nous sépare,
Rassembler sur les ponts de l’amour.
Avec vous, pour les coeurs qui s’égarent,
Éclairer le chemin du retour.
Et prier : qu’il n’y ait de parole
Que l’Esprit n’ait soufflée avec vous ;
Pour oser accomplir notre rôle :
Réunir le troupeau, comme vous !

4.
Comme vous, si nos jours sont prière,
Alléger de nos mains les fardeaux.
Avec vous déplacer chaque pierre
De la vie qui endure aux tombeaux.
Et prier : qu’il n’y ait de misère
Qui ne soit sanctifiée avec vous.
Et déjà, par l’amour de la terre,
Exulter dans le Ciel, comme vous !
vous, comme vous !