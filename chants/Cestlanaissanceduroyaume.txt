R.
Es-tu celui qui doit venir,
Ou devons-nous encore attendre ?
Es-tu celui qui doit venir
Redonner vie à notre cendre !

1.
Un grand soleil en liberté
A refleuri la nuit des hommes,
Tous les aveugles ont vu l´été
C´est la naissance du Royaume.

2.
Tous les chemins sont liberté
Pour une Pâque où Dieu se donne,
Tous les boiteux s´en vont danser
C´est la naissance du Royaume.

3.
Une Parole en liberté :
Allez la dire à tous les hommes,
Tous les muets s´en vont chanter
C´est la naissance du Royaume.