R.
JE VOUDRAIS PARCOURIR LA TERRE,
ALLER JUSQU’AU BOUT DU MONDE.
JE VOUDRAIS DIRE À TOUS NOS FRÈRES
L’AMOUR DU DIEU VIVANT.

1.
Cet amour est Bonne Nouvelle,
Car l’Amour, c’est Dieu.
Mots d’amour, Verbe qui appelle,
Car Dieu est Amour.

2.
Puits d’amour, profondeur immense,
Car l’Amour, c’est Dieu.
Coeur aimant d’un amour intense,
Car Dieu est Amour.

3.
Vie d’amour comme un héritage,
Car l’Amour, c’est Dieu.
Pain de vie donné en partage,
Car Dieu est Amour.

4.
Choix d’amour d’un Dieu qui se donne,
Car l’Amour, c’est Dieu.
Sang versé d’un Dieu qui pardonne,
Car Dieu est Amour.

5.
Cri d’amour sur la croix qui sauve,
Car l’Amour, c’est Dieu.
Ciel ouvert pour la joie du pauvre,
Car Dieu est Amour.

6.
Feu d’amour, brasier de lumière,
Car l’Amour, c’est Dieu.
Vent divin, brise familière,
Car Dieu est Amour.

7.
Chant d’amour, rythme d’allégresse,
Car l’Amour, c’est Dieu.
Grande joie, don de la Promesse,
Car Dieu est Amour.