1.
Chaque fois qu’une fleur poussera dans le désert
Chaque fois que vos coeurs s’ouvriront à la lumière
Tous sauront que vous êtes mes apôtres

CHAQUE FOIS QUE VOUS AUREZ DE L’AMOUR POUR LES AUTRES,
TOUS SAURONT QUE VOUS ÊTES MES APÔTRES

2.
Chaque fois que le soleil percera en plein hiver
Chaque fois que vos mains casseront une misère
Tous sauront que vous êtes mes apôtres