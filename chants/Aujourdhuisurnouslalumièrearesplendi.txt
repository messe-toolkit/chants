Stance
Aujourd’hui sur nous
la lumière a resplendi ;
le Sauveur est né
pour les hommes dans la nuit :
Fils de Dieu admirable,
couché dans une étable,
Roi du monde à jamais,
Prince de la paix !

R.
GRANDE JOIE
DANS LE CIEL ET SUR LA TERRE !
GRANDE JOIE
POUR JÉSUS QUI VIENT DE NAÎTRE !
LOUANGE ET GLOIRE À DIEU ! (bis)

1.
Sur les collines de Judée
Le chant des anges a résonné.
Bergers, levez-vous, ne craignez pas !
Vers l’Enfant nouveau-né hâtez le pas !

2.
Près de Joseph et de Marie
Vos yeux découvrent le Messie.
Bergers, allez dire à Bethléem :
« Cet Enfant est vraiment l’Emmanuel ! »