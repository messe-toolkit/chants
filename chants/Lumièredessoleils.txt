R.
Gloire, louange, honneur à toi, Seigneur !

1.
Lumière des soleils,
Souffle du vent,
Transparence des eaux,
Sel de la terre
Toi, le tout de l’entier,
La part du pauvre,
Le toujours de l’amour,

2.
Fontaine de l’Esprit,
Source du cœur,
Insondable Océan
De la mémoire,
Toi, le Tout-Autre, ici,
Semblable à l’homme
Et la chair
De sa chair,

3.
Silence et Verbe unis,
Grâce de Dieu,
Plénitude à jamais,
Âme de l’âme,
Toi, l’Abîme de joie,
Le Chant de l’être,
Le Noël
Éternel.