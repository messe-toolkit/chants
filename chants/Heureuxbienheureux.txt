R.
Heureux, bienheureux,
Qui écoute la parole de Dieu.
Heureux, bienheureux,
Qui la garde dans son cœur.

1.
Heureux ceux qui ont une âme de pauvre
Car le royaume des cieux est à eux.
Heureux les doux
Car ils possèderont la terre.

2.
Heureux les affligés
Car ils seront consolés
Heureux les affamés et assoiffés de justice
Car ils seront rassasiés.

3.
Heureux les miséricordieux
Car ils obtiendront miséricorde.
Heureux les cœurs purs
Car ils verront Dieu.

4.
Heureux les artisans de paix
Car ils seront appelés fils de Dieu.
Heureux les persécutés pour la justice
Car le royaume des cieux est à eux.

5.
Heureux serez-vous quand on vous insultera
Et qu'on vous persécutera,
Et que l'on dira faussement contre vous
Toute sorte de mal à cause de moi.
Soyez dans la joie, soyez dans l'allégresse,
Dans les cieux vous serez comblés ! (bis)