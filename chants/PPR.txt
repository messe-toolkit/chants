P, P, R,
P, P, R.
NOTRE BAPTÊME FAIT DE NOUS (DES…)
P, P, R,
P, P, R :
PRÊTRES, PROPHÈTES ET ROIS !

1.
Nous sommes Prêtres,
Et nous avons reçu la mission de la prière :
Prier Dieu pour les hommes,
Bénir les hommes au nom de Dieu.

2.
Nous sommes Prophètes,
Et nous avons reçu la mission de la Parole :
Annoncer, enseigner,
Parler aux hommes au nom de Dieu.

3.
Nous sommes Rois,
Et nous avons reçu la mission de la justice :
Pour servir dans la paix,
L’intelligence et la sagesse.