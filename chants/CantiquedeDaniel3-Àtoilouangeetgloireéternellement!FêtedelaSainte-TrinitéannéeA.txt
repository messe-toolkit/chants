À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

1.
Béni sois-tu, Seigneur, Dieu de nos pères :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

2.
Béni soit le nom très saint de ta gloire :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

3.
Béni sois-tu dans ton saint temple de gloire :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

4.
Béni sois-tu sur le trône de ton règne :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

5.
Béni sois-tu, toi qui sondes les abîmes :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

6.
Toi qui sièges au-dessus des Kéroubim :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !

7.
Béni sois-tu au firmament, dans le ciel :
À TOI, LOUANGE ET GLOIRE ÉTERNELLEMENT !