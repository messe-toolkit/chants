R.
TOUTES CES MÉLODIES EN PAYSAGES,
CES NOTES COMPOSÉES D’OMBRES ET DE JOURS,
NOUS TE LES APPORTONS COMME PRIÈRE.

1.
C’est le chant des prophètes
Qui vient à contretemps,
À contre-vent,
Éclabousser le ciel
D’une poignée d’étoiles.

2.
C’est le chant de la brise
Qui joue sa note bleue,
Soupir de Dieu,
Pour parfumer nos fronts
D’une pensée d’étoiles.

3.
C’est le chant de la plainte
Qui joue sa mélopée,
Désaccordé,
Pour espérer l’éveil
D’une portée d’étoiles.

4.
C’est le chant du cortège
Qui vient à pleine voix,
Éclats de foi,
Émerveiller le jour
D’une moisson d’étoiles.