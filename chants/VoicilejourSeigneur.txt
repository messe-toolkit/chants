1.
Voici le jour, Seigneur,
Où je vais à la mort.
Comme la graine
Qui s’enfonce dans le sol.

2.
Pourrai-je encore, Seigneur,
Revenir à la vie ?
Tout est possible
Pour celui qui croit en moi.

3.
Comment peux-tu, Seigneur,
Me sauver de la mort ?
L’amour est maître
De la vie et de la mort.

4.
Mon pauvre corps, Seigneur,
Va descendre au tombeau.
Au jour de Pâques
J’ai ouvert tous les tombeaux.

5.
Et que dis-tu, Seigneur,
Si je meurs avec toi ?
Aujourd’hui même
Tu seras en Paradis.