1.
Entre la terre et le ciel, les rameaux de l’arc-en-ciel,
Tu découvres le chemin de l’espérance.
Dieu nous donne force et paix de son Royaume :
De ses mains, prends sur toi d’être lumière auprès des autres ! (bis)

2.
Entre la faim et l’épi, tous les sillons de la vie,
Tu découvres le chemin de la confiance.
Dieu nous donne le bon grain de son Royaume :
De ses mains, prends sur toi d’être le germe dans la pâte ! (bis)

3.
Entre les vents et la mer, les oiseaux dans le désert,
Tu découvres le chemin de renaissance.
Dieu nous donne l’Esprit Saint de son Royaume :
De ses mains, prends sur toi d’être service auprès des pauvres ! (bis)

4.
Entre Parole et discours, les parfums de chaque jour,
Tu découvres le chemin de toute alliance.
Dieu nous donne l’Evangile du Royaume :
De ses mains, prends sur toi d’être justice pour tes frères ! (bis)

5.
Entre les armes et les pleurs, le figuier sur les hauteurs,
Tu découvres le chemin de la souffrance.
Dieu nous donne la douceur de son Royaume :
De ses mains, prends sur toi d’être sourire auprès des faibles ! (bis)

6.
Entre granit et maisons, les récoltes du pardon,
Tu découvres le chemin de la louange.
Dieu nous donne le bonheur de son Royaume :
De ses mains, prends sur toi d’être prière pour les hommes ! (bis)