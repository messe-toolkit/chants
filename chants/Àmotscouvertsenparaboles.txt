Stance
À mots couverts, en paraboles,
Tu nous racontes le Royaume.
De la barque où tu nous parles,
Ô Fils de Dieu,
Ta voix rejoint notre rivage.
Béni sois-tu, Seigneur,
Pour la semence dans nos coeurs !

R.
Que nous soyons la terre où ta Parole germera,
Terre fertile où l’amour lèvera !

Versets

1.
Qu’en sera-t-il des grains tombés sur le chemin ?
Sont-ils fermés à tout espoir de lendemains ? R Que nous soyons la terre…

2.
Qu’en sera-t-il des grains tombés sur le rocher ?
Tu leur accordes un temps d’eau vive pour germer.

3.
Qu’en sera-t-il des grains tombés dans les épines ?
Sous le buisson des mille riens vont-ils mourir ?

4.
Qu’en sera-t-il des grains tombés en bonne terre?
Ils porteront beaucoup de fruit dans ta lumière.