N’OUBLIEZ JAMAIS QUE LE PAUVRE,
C’EST JÉSUS-CHRIST.
FAITES TOUT PAR AMOUR,
ET DITES TOUJOURS:
“DIEU SOIT BÉNI !” (bis)

1.
Écoutez, tant de voix retentissent,
Tant de cris des blessés de la vie !
Revêtez la tenue de service,
Les petits vous font signe aujourd’hui.

2.
L’homme bon a pitié, il partage.
Sa justice à jamais se maintient.
Que l’amour en ce jour se propage.
Préparez au Seigneur un chemin.

3.
Venez tous, vous les hommes au coeur simple,
En Jésus, déposez vos fardeaux,
Approchez de son coeur doux et humble,
Près de lui recueillez le repos.

4.
Le Seigneur est présent d’âge en âge,
Regardez ce que Dieu fait pour vous.
Découvrez en tout temps son passage :
La tendresse a brisé les verrous.