1.
Dans la vigne du Seigneur
Tu as grandi comme un sarment,
Ô saint Vincent,
Ô saint Vincent.
Avec la sève de l'Esprit
Tu as porté beaucoup de fruit.

R.
Gloire à Dieu, gloire à Dieu,
Qui promet le vin des Noces,
Gloire à Dieu, gloire à Dieu,
Pour la joie dans son Royaume !

2.
Dans la vigne du Seigneur
Tu as servi les plus petits
Jusqu'à mourir,
Jusqu'à mourir.
En ressemblance au Crucifié,
Ta grappe mûre est vendangée.

3.
Dans la vigne du Seigneur
Viens nous apprendre à mieux servir
Le Dieu de vie,
Le Dieu de vie.
C'est lui le Maître des saisons,
Qui fait de nous ses vignerons.

4.
Dans la vigne du Seigneur
Nous découvrons l'amour parfait
Du Bien-Aimé,
Du Bien-Aimé.
Pour lui nous sommes de grand prix,
Hors de sa Vigne il nous le dit.