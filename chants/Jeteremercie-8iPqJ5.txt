R.
Je Te remercie, ô Dieu notre Père, je te remercie pour le don de la vie,
Je Te remercie pour l’Eucharistie.

1.
Je Te remercie de m’avoir créé, de m’avoir tiré du néant à la vie,
De m’avoir marqué de ta divine empreinte, tu me combles de ta miséricorde.

2.
Je Te remercie pour toutes les grâces, dont tu me combles ma vie durant
Et qui m’éclairent comme la lumière du soleil; par elles tu montres le chemin de vie.

3.
Je Te remercie pour ton fils Jésus-Christ qui me nourrit de l’Eucharistie,
Et mon âme sent les battements de ton coeur! Tu épanouis en moi la vie divine.

4.
Je Te remercie d’éclairer ma vie, par ton Esprit saint qui me fortifie,
Et tous ces dons par lesquels Tu me conduis, qui m’inspirent une vie de charité.

5.
Je Te remercie pour tous tes sacrements, source infinie de miséricorde,
Toutes ces grâces dont mon âme s’illumine; par ton amour, Tu me transformes en Toi.