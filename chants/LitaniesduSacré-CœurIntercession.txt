Litanies du Sacré-Cœur

R.
Ô DIEU, NOTRE PÈRE DES CIEUX, PRENDS PITIÉ DE NOUS.
Ô DIEU, FILS RÉDEMPTEUR DU MONDE, PRENDS PITIÉ DE NOUS
Ô DIEU, ESPRIT SAINT, PRENDS PITIÉ DE NOUS.
Ô DIEU, UNIQUE, TRINITÉ SAINTE, PRENDS PITIÉ DE NOUS.

Introduction au Mystère de l’Incarnation

1.
Cœur du Christ, Fils du Père éternel, NOUS T’ADORONS.
Cœur du Christ, formé par le Saint Esprit dans le sein de la Vierge Marie, NOUS T’ADORONS.

2.
Cœur du Christ, uni substantiellement au Verbe de Dieu, NOUS T’ADORONS
Cœur du Christ, Temple saint de Dieu, NOUS T’ADORONS.

3.
Cœur du Christ, Tabernacle du Très-Haut, NOUS T’ADORONS.
Cœur du Christ, en qui réside la plénitude de la divinité, NOUS T’ADORONS.

4.
Cœur du Christ, Fournaise ardente de charité, NOUS T’ADORONS
Cœur du Christ, avide de faire la volonté du Père, NOUS T’ADORONS.

5.
Cœur du Christ, en qui le Père se complaît, NOUS T’ADORONS.
Cœur du Christ, Sanctuaire de la justice et de l’amour
NOUS T’ADORONS.

Introduction au Mystère de la Passion

6.
Cœur du Christ, blessé par ceux que tu voulais sauver, NOUS T’ADORONS
Cœur du Christ, abandonné par tes amis, NOUS T’ADORONS.

7.
Cœur du Christ, broyé à cause de nos péchés, NOUS T’ADORONS
Cœur du Christ, agonisant jusqu’à la sueur de sang, NOUS T’ADORONS.

8.
Cœur du Christ, obéissant jusqu’à la mort, NOUS T’ADORONS
Cœur du Christ, dont le sacrifice nous sauve, NOUS T’ADORONS.

9.
Cœur du Christ, ouvert par la lance, NOUS T’ADORONS.
Cœur du Christ, dont l’Esprit d’Amour se répand sur nous
NOUS T’ADORONS.

Introduction au Mystère de la Résurrection

10.
Cœur du Christ, ressuscité et source de notre vie, NOUS T’ADORONS
Cœur du Christ, par qui nous est communiqué l’Esprit, NOUS T’ADORONS.

11.
Cœur du Christ, Source de lumière et de tout Amour, NOUS T’ADORONS
Cœur du Christ, en qui tous les cœurs se rencontrent, NOUS T’ADORONS.

12.
Cœur du Christ, Bienveillant pour ceux qui t’invoquent, NOUS T’ADORONS
Cœur du Christ, Salut de ceux qui espèrent en toi
NOUS T’ADORONS.

Prière d’intercession

Finale des Litanies du Sacré-Cœur

1.
Unis à ceux qui ne te connaissent pas, NOUS TE PRIONS
Unis à ceux qui te cherchent avec droiture, NOUS TE PRIONS.

2.
Unis à ceux qui ont perdu confiance en toi, NOUS TE PRIONS
Unis à ceux que la souffrance a éloigné de toi, NOUS TE PRIONS.

3.
Unis à tous les peuples de la terre, NOUS TE PRIONS.
Avec le Saint Père, les évêques, les prêtres, les consacrés et tous les chrétiens. NOUS TE PRIONS.

JÉSUS, DOUX ET HUMBLE DE CŒUR, RENDS NOTRE CŒUR SEMBLABLE AU TIEN.