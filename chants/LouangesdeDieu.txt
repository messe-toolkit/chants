Tu es saint, Seigneur,
seul Dieu, qui fais des merveilles.
Tu es fort,
tu es grand,
tu es très haut,
tu es tout-puissant,
toi, Père saint, roi du ciel et de la terre.
Tu es trine et un, Seigneur, Dieu des dieux,
tu es le bien, tout bien, le souverain bien,
Seigneur Dieu vivant et vrai.

Tu es amour, charité ;
tu es sagesse,
tu es humilité,
tu es patience,
tu es beauté,
tu es sécurité,
tu es quiétude,
tu es joie et allégresse,
tu es notre espérance,
tu es justice,
tu es tempérance,
tu es tout, notre richesse à suffisance.

Tu es beauté,
tu es mansuétude,
tu es protecteur,
tu es notre gardien et défenseur,
tu es force,
tu es refuge.

Tu es notre espérance,
tu es notre foi,
tu es notre charité,
tu es toute notre douceur,
tu es notre vie éternelle, grand et admirable,
Seigneur Dieu tout-puissant, miséricordieux Sauveur.