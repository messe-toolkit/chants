1.
Ô Jésus, délivre-moi
du désir d’être aimé,
du désir d’être élevé,
du désir d’être honoré,
du désir d’être loué,
du désir d’être préféré,
du désir d’être consulté,
du désir d’être approuvé
du désir d’être connu,

2.
Ô Jésus, délivre-moi
de la peur d’être humilié,
de la peur d’être méprisé,
de la peur d’être blâmé,
de la peur d’être calomnié,
de la peur d’être oublié,
de la peur d’être lésé,
de la peur d’être raillé,
de la peur d’être soupçonné.