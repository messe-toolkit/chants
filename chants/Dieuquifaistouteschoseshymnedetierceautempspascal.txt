1.
Dieu qui fais toutes choses nouvelles
Quand passe le vent de l’Esprit,
Viens encore accomplir tes merveilles
Aujourd’hui.
Viens encore accomplir tes merveilles
Aujourd’hui.

2.
N’as-tu pas sanctifié les prémices
Du Peuple choisi par amour ?
Illumine tous ceux que l’Église
Met au jour.
Illumine tous ceux que l’Église
Met au jour.

3.
Dans la brise ou l’éclat du tonnerre
L’Esprit nous entraîne en son chant.
C’est ta voix, ô Seigneur, sur la terre
Maintenant.
C’est ta voix, ô Seigneur, sur la terre
Maintenant.