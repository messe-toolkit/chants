GRANDE BÉNÉDICTION TRINITAIRE
Que Dieu notre Père nous bénisse et nous garde :
Amen, Amen, Alléluia !
Que le visage du Christ s'illumine pour nous :
Amen, Amen, Alléluia !
Que l'Esprit Saint nous prenne en grâce et nous donne la paix :
Amen, Amen, Alléluia !

BÉNÉDICTION BRÈVE TRINITAIRE
Béni soit Dieu qui nous bénit,
le père, le Fils, le Saint-Esprit,
maintenant et pour toujours.

ACCLAMATION FINALE TRINITAIRE
Ô notre Dieu, Amour éternel, gloire et louange à Toi, alléluia !
Père très bon, source de la vie, …
Seigneur Jésus, Sauveur du monde…
Esprit de vie, force de l’Église, …
Ô notre Dieu, Amour éternel, gloire et louange à Toi, alléluia !

ACCLAMATION FINALE AU CHRIST
À Jésus Christ la gloire et la puissance maintenant et toujours, Amen!
Il vient sur les nuées, et tout homme le verra, Amen!
Il est, Il était et Il vient, Seigneur Dieu, Maître de tout, Amen!