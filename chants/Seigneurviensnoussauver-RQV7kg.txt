R.
Seigneur, viens nous sauver, Dieu avec nous, Emmanuel,
Seigneur, viens nous sauver, viens, Seigneur Jésus !

1.
Dans le désert monte un cri : « Voici qu’il vient, l’Agneau de Dieu;
Aplanissez les chemins devant ses pas ! »

2.
La femme vierge a conçu, par elle un Fils nous est donné,
Celui qui vient nous sauver : L’Emmanuel !

3.
Verbe fait chair en nos vies, pour le salut de tous les hommes.
Tu viens briller dans nos nuits, Astre dans Haut !

4.
En revêtant notre chair, tu as aimé l’humanité.
Nous t’attendons, ô Jésus, Marana tha !