R.
Convertissez-vous et croyez à la Bonne Nouvelle
Car son royaume est proche !

1.
Le Fils de l’Homme, quand Il reviendra, trouvera-t-Il la foi sur la terre ?
Qui soutiendra le jour de sa venue ?
Veillez ! Car Il viendra soudain, Celui que vous cherchez.

2.
Ô vous qui étiez abattus, prenez courage !
Le Seigneur est fidèle : voici qu’Il vient ! S’Il se fait attendre, espérez sa venue,
Car Il est là à votre porte : Il ne tardera plus.

3.
Cherchez le Seigneur, tant qu’on peut le trouver,
Invoquez-le tant qu’Il est proche. C’est à l’heure où vous n’y pensez pas
Que le Fils de l’Homme viendra. Heureux le serviteur qu’Il trouvera veillant !