1.
Heureux qui garde un coeur de pauvre,
Il porte en lui la joie de Dieu !
Bienheureux, bienheureux !
En marche nuit et jour vers le Royaume,
Il met sa foi dans le Seigneur.
Bienheureux, bienheureux !

2.
Heureux qui chasse la violence
Avec les mots de la douceur !
Bienheureux, bienheureux !
En marche vers la terre d’espérance,
Il est l’image du Sauveur.
Bienheureux, bienheureux !

3.
Heureux qui pleure avec son frère
Et le relève de sa main !
Bienheureux, bienheureux !
En marche avec celui qui désespère,
Il est pour lui un vrai soutien.
Bienheureux, bienheureux !

4.
Heureux le coeur du pacifique,
Il voit le monde en arc-en-ciel !
Bienheureux, bienheureux !
En marche pour bâtir la terre libre,
Il est un fils du Dieu de paix.
Bienheureux, bienheureux !

5.
Heureux qui fait miséricorde
Au plus petit qui l’a blessé !
Bienheureux, bienheureux !
En marche vers le Maître qui pardonne,
Il apprendra qu’il est aimé.
Bienheureux, bienheureux !

6.
Heureux qui cherche la justice,
Elle est le pain de l’affamé!
Bienheureux, bienheureux !
En marche dans le champ des injustices,
Quand sera-t-il un rassasié ?
Bienheureux, bienheureux !

7.
Heureux le coeur dans la lumière,
Il est reflet de Jésus Christ !
Bienheureux, bienheureux !
En marche à la rencontre de son Père,
Il est désir de vivre en Lui.
Bienheureux, bienheureux !

8.
Heureux qui lutte et qui résiste
Alors que frappent les puissants !
Bienheureux, bienheureux !
En marche sur les voies de l’Évangile,
Il ose croire au Dieu vivant.
Bienheureux, bienheureux !