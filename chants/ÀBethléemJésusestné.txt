1.
A Bethléem Jésus est né,
Le Fils de Dieu nous est donné.
Au creux de notre humanité
Il nous apporte sa clarté,
Il nous apporte sa clarté.

2.
Les bergers la nuit dans les champs
Ont entendu les plus beaux chants :
Ils annonçaient l’Événement :
Dieu s’est fait chair en notre temps,
Dieu s’est fait chair en notre temps.

3.
L’Enfant Jésus, c’est le Messie,
Il accomplit la prophétie.
D’un même cœur, d’un même esprit
Proclamons Dieu en Jésus Christ,
Proclamons Dieu en Jésus Christ.