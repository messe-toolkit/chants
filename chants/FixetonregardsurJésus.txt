FIXE TON REGARD SUR JÉSUS
NE DÉTOURNE PAS LES YEUX,
IL T’APPELLE, ENTENDS SA VOIX
TEL UN MURMURE, AU FOND DE TOI.

1.
Quand dans la nuit, le vent t’est contraire
Lève les yeux, et cherche-le.
Ne doute pas, sois sûr qu’il est là.
Lève les yeux, regarde-le.

2.
Quand sur la route, tu trébuches et tombes
Lève les yeux, et cherche-le.
Saisis sa main, ne la lâche pas
Lève les yeux, regarde-le.

3.
Invoque-le : « Seigneur, sauve-moi ! »
Lève les yeux, et cherche-le.
Il t’a saisi, et t’attire à lui.
Lève les yeux, regarde-le.