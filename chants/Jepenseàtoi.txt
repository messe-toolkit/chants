JE PENSE À TOI, JE PENSE À TOI
DANS LE SILENCE ET LE SECRET.
JE PENSE À TOI, JE PENSE À TOI
TU GRANDIS DANS MES PENSÉES. (bis)

1.
Ce que je vois autour de moi
Est plein de vie et de beauté
Ce qui est grand ou tout petit
Me dit mon Dieu que tu es là.

2.
Ce que j’entends au fond de moi
Est plein de vie et de beauté
Ce que j’écoute et qui grandit
Me dit mon Dieu que tu es là.

3.
Ce que j’attends au creux de moi
Est plein de vie et de beauté
Ce que je sens quand je te prie
Me dit mon Dieu que tu es là.