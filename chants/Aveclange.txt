1.
Avec l’Ange qui t’annonçait
le choix de Dieu,
Avec l’Ange qui saluait
l’Immaculée.

Avec Élisabeth saisie
de ta visite,
Avec Élisabeth louant
ta pure foi.

R.
NOUS VENONS À TOI,
VIERGE MARIE,
TOUTE BÉNIE DE DIEU. bis

2.
Avec Siméon dans le temple
bouleversé,
Avec Siméon prédisant
ton avenir.
Avec l’admirable Joseph
en son service,
Avec l’admirable Joseph
en sa ferveur.

3.
Avec Jean présent près de toi
dessous la Croix,
Avec Jean désigné pour être
ton nouveau fils.
Avec l’Église tout entière
te contemplant,
Avec l’Église qui t’honore
comme sa mère.