R.
Garde-moi, mon Seigneur,
J´ai fait de toi mon refuge.
J´ai dit au Seigneur : ´ Tu es mon Dieu,
Je n´ai d´autre bonheur que toi,
Seigneur, tu es toute ma joie !´

1.
Je bénis le Seigneur
qui s´est fait mon conseil,
Et qui même la nuit instruit mon cœur.
Je garde le Seigneur devant moi sans relâche,
Près de lui, je ne peux chanceler.

2.
Aussi mon cœur exulte et mon âme est en fête,
En confiance je peux reposer.
Tu m´ouvres le chemin de la vie éternelle,
Avec Toi, débordement de joie !

3.
Tu es le seul Seigneur, mon partage et ma vie
La part qui me revient fait mon bonheur.
Je reçois de tes mains le plus bel héritage,
Car de toi, Seigneur, dépend mon sort.