1.
Il a passé la mort,
le Vainqueur qui t’appelle
et te dit : « Viens dehors,
créature de Dieu,
créature nouvelle,
viens à la pure lumière ! »

IL EST LE MESSIE, LE FILS DE DIEU,
LE MAÎTRE ET SEIGNEUR.
QUI OUVRE LES TOMBEAUX.
IL A VAINCU LE MONDE:
IL EST LA RÉSURRECTION ET LA VIE !

2.
Il a passé la nuit,
le Veilleur qui t’éveille
et te dit : « Aujourd’hui,
le Soleil s’est levé !
Le Seigneur fait merveille,
sors de la froide ténèbre ! »

3.
Il a passé la mer,
le Lutteur qui délivre
et qui dit à la chair :
« C’est l’Esprit qui fait corps !
C’est l’Esprit qui fait vivre !
Laisse mon souffle te prendre ! »