R.
NOUS CROYONS EN TOI, SEIGNEUR,
FAIS GRANDIR EN NOUS LA FOI.
OUVRE LARGE NOTRE COEUR,
FAIS CHANTER EN NOUS TA JOIE.

1.
Dieu vivant que nul ne voit,
Créateur de l'Univers,
Dieu du ciel et de la terre,
Dieu lumière pour nos pas.

2.
Fils de Dieu né de Marie,
Messager de l'Eternel,
Crucifié sur le calvaire,
Jésus-Christ, soleil de vie.

3.
Feu nouveau des baptisés,
Dieu de Pâques et de réveil,
Vent de brise et de tempête,
Paix d'un peuple libéré.