ALLÉLUIA, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA !
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA !

Nous te louons, nous t’acclamons,
Jésus, vivant, toi le ressuscité !
Nous te louons, nous t’acclamons,
Verbe de vie, la Parole incarnée.

Gloire à Toi, Seigneur,
Ton Évangile est vérité.
Gloire à Toi, Seigneur,
Ton Évangile est liberté.