R.
TON APPEL SUR LE CHEMIN DU SERVITEUR,
Ô JÉSUS, C’EST UNE JOIE DE LE CONNAÎTRE !
QU’IL RÉSONNE AU PLUS PROFOND DE NOTRE COEUR,
Ô JÉSUS, NOUS TE SUIVRONS PARMI TES FRÈRES.

1.
Dans ton Église de croyants
L’appel à vivre nous attend.
Témoins choisis, portons le feu
À toute chair en soif de Dieu!

2.
Dans notre monde aimé de toi
Risquons sans peur des mots de foi !
La vérité des gestes forts
Arrache l’homme à bien des morts.

3.
Au plein du jour, au creux des nuits
Te rencontrer n’a pas de prix.
Laissons l’Esprit nous ressourcer
Là où l’eau vive peut couler.

4.
Dans l’ouragan des divisions
Osons souffler la communion!
La paix fleurit avec le temps,
Ayons l’audace des patients.

5.
Comment chanter “soleil levant”
Auprès des coeurs désespérants ?
Que nos silences soient remplis
D’un Dieu qui sauve toute vie !

6.
Nos yeux seront émerveillés
Par tous les signes que tu fais :
Il nous faudra les découvrir
Et tous ensemble te bénir.

7.
Au grand festin d’eucharistie
Voici ton peuple qui revit.
Par ta parole et par ton pain
Qu’il soit fidèle et peuple saint !

8.
Qu’il ait la force d’appeler
Ceux qui seront de bons bergers !
Alors nos champs seront féconds
Pour les semailles et les moissons.