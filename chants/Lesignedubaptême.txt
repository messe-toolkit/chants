R.
TU NOUS DONNES LA VIE
QUI JAMAIS NE FINIT,
COMME UN PÈRE, TU NOUS AIMES.
TU NOUS DONNES LA VIE
QUI JAMAIS NE FINIT,
LE SIGNE DU BAPTÊME.

1.
Ton appel, nous l’avons bien reçu,
Comme une invitation
Écrite à notre nom.
Heureux ceux qui ouvrent leur coeur
Aux messages de Dieu.

2.
Ta parole, nous l’avons méditée
En ouvrant nos silences
Aux mots de l’Évangile.
Heureux ceux qui savent garder
Des moments de prière.

3.
Notre foi, nous l’avons proclamée
Devant tous les croyants,
Église rassemblée.
Heureux, si nous faisons confiance
Au Seigneur de l’Alliance.

4.
Ton eau vive nous a submergés,
Car nous sommes plongés
Dans l’amour infini.
Heureux les nouveaux baptisés,
Ils renaissent de l’eau.

5.
L’huile sainte est passée sur nos fronts,
Comme un parfum précieux,
La présence de Dieu.
Heureux ceux qui ont accueilli
Une force nouvelle.

6.
Ta lumière nous a réchauffés,
Une petite flamme,
Promesse d’un grand feu.
Heureux ceux qui laissent passer
La lumière de Dieu.

7.
Ta présence, nous a envahis
Nous voulons l’annoncer :
Tu es ressuscité !
Heureux les témoins de Jésus
Sur les routes du monde.