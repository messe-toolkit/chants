1.
Mendiant du jour, je te prends dans mes mains,
Comme on prend dans sa main la lampe pour la nuit ;
Et tu deviens la nuée qui dissout les ténèbres.

2.
Mendiant du feu, je te prends dans mes mains,
Comme on prend dans sa main la flamme pour l’hiver,
Et tu deviens l’incendie qui embrase le monde.

3.
Mendiant d’espoir, je te prends dans mes mains,
Comme on prend dans sa main la source pour l’été,
Et tu deviens le torrent d’une vie éternelle.

4.
Mendiant de toi, je te prends dans mes mains,
Comme on prend dans sa main la perle d’un amour,
Et tu deviens le trésor pour la joie du prodigue.

5.
Mendiant de Dieu, je te prends dans mes mains,
Mais tu prends dans ta main la mienne pour ce jour,
Et je deviens l’envoyé aux mendiants de la terre.