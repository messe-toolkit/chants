BÉNI SOIT LE SEIGNEUR :
IL ÉLÈVE LES HUMBLES.

1.
Les justes sont en fête, ils exultent ;
devant la face de Dieu ils dansent de joie.
Chantez pour Dieu, jouez pour son nom.
Son nom est Le Seigneur ; dansez devant sa face.

2.
Père des orphelins, défenseur des veuves,
tel est Dieu dans sa sainte demeure.
À l’isolé, Dieu accorde une maison ;
aux captifs, il rend la liberté.

3.
Tu répandais sur ton héritage une pluie généreuse,
et quand il défaillait, toi, tu le soutenais.
Sur les lieux où campait ton troupeau,
tu le soutenais, Dieu qui es bon pour le pauvre.