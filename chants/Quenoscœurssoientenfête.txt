Solistes(s) : Que nos coeurs soient en fête pour Dieu !
Célébrons le Seigneur, célébrons ses amis.

id. ou quelques voix : Célébrons le Seigneur, célébrons ses amis.

Quelques voix : GLORIA, GLORIA, GLORIA TIBI DOMINE !

Tous : GLORIA, GLORIA, GLORIA TIBI DOMINE !

Solistes(s) : Le plan du Seigneur demeure pour toujours,
les projets de son coeur subsistent d’âge en âge !

Tous : GLORIA, GLORIA, GLORIA TIBI DOMINE !

Solistes(s) : Heureux le peuple dont le Seigneur est le Dieu,
heureux le peuple qui connaît l’ovation !

Tous : GLORIA, GLORIA, GLORIA TIBI DOMINE !