R.
Il vient,
Messager de paix,
Il a donné sa vie pour la vie des hommes.
(bis)

1.
Il vient comme un enfant,
Qui le reconnaîtra
Sous l'habit du mendiant ?
(bis)

2.
Il vient comme un voleur,
Personne ne connait
Ni le moment, ni l’heure.
(bis)

3.
Il vient sans prévenir,
Heureux qui veillera
Sachant qu'il va venir.
(bis)

4.
Il vient et il est là,
Déjà il apparaît
Ne le voyez vous pas ?
(bis)