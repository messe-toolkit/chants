R.
J’AI CHOISI
DE ME LAISSER CHOISIR PAR TOI,
SEIGNEUR,
J’AI CHOISI
DE ME LAISSER CHOISIR PAR TOI.

1.
Dès le sein de ma mère,
Tu m’as mis de côté,
Et déjà, comme un père,
Tu me protégeais.

2.
Arriva ma naissance,
Tu me fis un baiser,
Tu comblas d’innocence
Mes jeunes années !

3.
Ton regard de tendresse
Pénétra dans mon coeur,
Tu me fis la promesse
D’un parfait bonheur.

4.
Ton appel en mon être
Grandissait chaque jour,
Tu devins mon seul Maître,
Mon unique amour.

5.
J’ai choisi de te suivre,
De marcher sur tes pas.
Ton visage m’invite,
À vivre avec toi.

6.
Me voici, plein d’audace,
Me voici devant toi,
Ma réponse à ta grâce
Me comble de joie !