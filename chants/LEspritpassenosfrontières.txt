L’ESPRIT PASSE NOS FRONTIÈRES,
SOUFFLE ET FEU EN LIBERTÉ !
CONDUIS-NOUS, Ô NOTRE PÈRE,
AUX CHEMINS DE L’UNITÉ.

1.
À jamais ces mots résonnent :
« Que la paix soit avec vous ! »
Et le Souffle qui pardonne
Se répand, mission pour nous.

2.
Revêtus de la Puissance
De Jésus monté vers toi,
Nous portons son espérance,
En témoins de notre foi.

3.
Sont variés les dons de grâce :
C’est toujours le même Esprit.
Nous venons de toute race,
Abreuvés du seul Esprit.

4.
Le Royaume est bien justice,
Paix et joie dans l’Esprit Saint.
Qui, au Christ, rend ce service
Plaît à toi et aux humains.

5.
Par l’Esprit qui nous rassemble,
Prendront fin les divisions.
À toi, Père, un jour ensemble,
Rendront gloire les nations.