R.
TU DESSINES AUJOURD’HUI,
MARIE,
LE VISAGE DE DIEU,
AUX MARCHEURS DANS LA NUIT,
VERS LE SOLEIL DE DIEU,
MARIE.

1.
Aurais-je assez de temps
Pour ce que j’ai à dire ?
T’écouter patiemment,
Habiter ton sourire.

2.
Qui peut donc rallumer,
Le grand feu sous la cendre ?
Qui peut m’accompagner,
M’attendre, pour L’entendre ?

3.
Debout près de la croix,
Marie de nos blessures,
Marie de tous nos choix,
Au coeur des Écritures.