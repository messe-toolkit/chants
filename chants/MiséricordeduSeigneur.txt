Miséricorde du Seigneur

Paroles et musique : Communauté de l’Emmanuel (Roberto Volpe)No. 23-05.

1.
Dieu, Un et Trine, mystère indicible,
Tu nous dévoiles ta face.
Ô Miséricorde, justice admirable,
Dieu de tendresse et de grâce.
Salut de nos âmes, lumière joyeuse,
Notre espérance invincible.

R.
Miséricorde du Seigneur,
Viens, viens à notre aide.
Viens, envahis de ta lumière
Les cœurs de ceux qui se perdent.

2.Ô Fils unique, cloué au calvaire
Pour nous sauver des ténèbres.
Sagesse divine, folie et scandale,
Source de vie et de grâce.
Visage du Père, amour sans mesure,
Force des humbles et des pauvres.