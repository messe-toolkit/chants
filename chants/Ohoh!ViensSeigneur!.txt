Oh oh Viens Seigneur
Oh oh Dans notre coeur !
Oh oh Viens Jésus
Nous donner le salut
Oh oh Viens Seigneur
Oh oh Dans notre coeur
Oh oh Viens Jésus
Nous donner le salut

Dans la crèche, tu vas naître,
Tu te fais petit enfant.
Dans nos coeurs, viens renaître,
Nous t’appelons : toi, l’enfant Roi.