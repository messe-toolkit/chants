R.
Tu nous précèdes au désert,
Quarante jours, quarante nuits,
tu nous conduis.
Ouvre nos yeux, change nos cœurs,
Quarante jours, quarante nuits, avec l’Esprit

1.
Pitié, Seigneur, dans ton amour,
tu peux parler et pardonner ;
Des profondeurs, je crie vers toi :
Seigneur, écoute mon appel.
Conduis mes pas jusqu’à la Pâque.

2.
Ouvre mes yeux d’aveugle-né.
Oui, je connais tout mon péché ;
Mais près de toi est le pardon :
Seigneur, écoute mon appel.
Conduis mes pas jusqu’à la Pâque.

3.
Tu veux de moi la vérité.
Purifie-moi, je serai pur ;
J’attends, Seigneur, le jour nouveau :
Seigneur, écoute mon appel.
Conduis mes pas jusqu’à la Pâque.

4.
Rends-moi la joie d’être sauvé.
Je chanterai rien que pour toi ;
Tu es, Seigneur, le Dieu d’amour :
Seigneur, écoute mon appel.
Conduis mes pas jusqu’à la Pâque.