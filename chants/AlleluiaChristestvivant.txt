1.
Christ est vraiment ressuscité ! (bis)
Alléluia, alléluia ! (bis)
ALLÉLUIA, CHRIST EST VIVANT ! (bis)

2.
La mort n’a plus aucun pouvoir, (bis)
Alléluia, alléluia ! (bis)
ALLÉLUIA, CHRIST EST VIVANT ! (bis)

3.
Seigneur, ta vie nous est donnée, (bis)
Alléluia, alléluia ! (bis)
ALLÉLUIA, CHRIST EST VIVANT ! (bis)