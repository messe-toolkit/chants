1.
Dieu, Au-delà de tout créé,
Nous ne pouvions que t'appeler l'inconnaissable !
Béni sois-tu pour l'autre voix qui sait ton nom,
Qui vient de toi
Et donne à notre humanité
De rendre grâce !

2.
Toi, que nul homme n'a pu voir,
Nous te voyons prendre ta part
De nos souffrances.
Béni sois-tu d'avoir montré
Sur le visage Bien-Aimé
Du Christ offert à nos regards
Ta gloire immense !

3.
Toi, que nul homme n'entendit,
Nous t'écoutons, Parole enfouie
Là où nous sommes !
Béni sois-tu d'avoir semé
Dans l'univers à consacrer
Des mots qui parlent aujourd'hui
Et nous façonnent !

4.
Toi, que nul homme n'a touché,
Nous t'avons pris : l'Arbre est dressé
En pleine terre !
Béni sois-tu d'avoir remis
Entre les mains des plus petits
Ce corps où rien ne peut cacher
Ton cœur de Père !