1.
Agneau de Dieu, Corps livré pour nous,
Tu enlèves le péché du monde,
Agneau de Dieu, Corps livré pour nous,
Prends pitié de nous !

2.
Agneau de Dieu, Sang versé pour nous,
Christ aujourd’hui vainqueur du monde,
Agneau de Dieu, Sang versé pour nous,
Prends pitié de nous !

3.
Agneau de Dieu, signe de l’Amour,
Pain du ciel qui nous fait vivre,
Agneau de Dieu, signe de l’Amour,
Donne-nous la paix !