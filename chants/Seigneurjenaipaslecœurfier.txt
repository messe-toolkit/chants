R.
MON ÂME EST EN MOI COMME UN ENFANT,
COMME UN PETIT ENFANT CONTRE SA MÈRE.

1.
Seigneur, je n’ai pas le coeur fier
ni le regard ambitieux ;
je ne poursuis ni grands desseins,
ni merveilles qui me dépassent.

2.
Non, mais je tiens mon âme
égale et silencieuse.
Attends le Seigneur, Israël,
maintenant et à jamais.