1.
Au commencement, Dieu dit : “Que paraisse le jour !”
Voici que s’est levée la lumière du monde :
Le Christ est sorti du tombeau,
Inaugurant les cieux nouveaux et la terre nouvelle.
Et Dieu vit que cela était bon.

2.
Au commencement, l’Esprit se mouvait sur les eaux.
Voici que s’est ouverte une source féconde :
Coulant du côté transpercé,
Le sang sauveur nous a lavés dans les eaux du baptême ;
Et Dieu vit que cela était bon.

3.
Dans les derniers temps, le Verbe est venu chez les siens ;
Aux hommes, il a rendu leur parure de gloire ;
Image et splendeur du Très-Haut,
Il devenait le premier-né d’un grand nombre de frères.
Et Dieu vit que cela était bon.

4.
Nous avons reçu de lui le Royaume à venir ;
Par la Résurrection, l’oeuvre immense s’achève.
Entrons nous aussi dans la paix,
Dans le repos promis par Dieu, dans l’alliance éternelle,
Par le Christ, le Vivant, à jamais !