Stance
Pour partir en voyage
vers la nouvelle terre,
il nous faudra quitter
le pays de nos pères
et Te trouver.

TU ES LE CHEMIN,
TU ES LA VÉRITÉ,
TU ES LA VIE.

1.
Toi, l’Homme-CHEMIN
que l’on découvre pas à pas,
enseigne-nous tes sommets enneigés.

2.
Viens, marche avec nous
que nous parlions en VÉRITÉ.
Évite-nous les sentiers embrumés.

3.
Oui, brise le pain
que dans ta mort tu as pétri.
Indique-nous le versant de LA VIE.