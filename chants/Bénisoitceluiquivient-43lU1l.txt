Béni soit celui qui vient sauver le monde !
Le Christ, l'Agneau de Dieu,
le Roi de l'univers !
Gloire, honneur et puissance
à l'Agneau vainqueur !
Pour les siècles des siècles. Amen !
Pour les siècles des siècles. Amen !