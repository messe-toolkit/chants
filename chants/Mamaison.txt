R.
MA MAISON SERA TA MAISON
PORTE OUVERTE AUX QUATRE HORIZONS
ELLE ACCUEILLE EN TOUTES SAISONS
MA MAISON PORTE TON PRÉNOM.

1.
Il faut ton coeur, il faut tes mains
Et que chacun trouve sa place
Bâtir c’est ouvrir un chemin
Aucun prénom ne se remplace.

2.
Il faut le temps de nos efforts
Pelles et pioches débroussaillent
Rusés, malins comme un castor
Dans tous les coins on y travaille.

3.
Toutes les pierres de couleurs
Posent des arcs-en-ciel de fête
Pour accueillir à tour de coeur
On laissera la porte ouverte.

4.
Elle est bâtie sur le rocher
De sa fenêtre on voit le monde
Au diapason de l’amitié
Il faudra que nos coeurs répondent.

5.
« Je viens pour habiter chez toi »
Parole gravée sur la porte
C’est l’amour même qui envoie
Gagner la vie en quelque sorte.

6.
Ferme ta porte ouvre ton coeur
Ton Père est là dans le secret
Enseigne-moi ton nom, Seigneur
Et viens en moi pour demeurer.