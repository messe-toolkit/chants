Béni soit Dieu, le Père de notre Seigneur Jésus Christ,
le Père plein de tendresse, le Dieu de qui vient tout réconfort.
Dans toutes nos détresses, il nous réconforte ;
ainsi nous pouvons réconforter tous ceux qui sont dans la détresse,
grâce au réconfort que nous recevons nous-mêmes de Dieu.