1.
Dieu de silence, tu appelles
Dans les pages du livre.
Les mots de vie nous redisent
De quel amour tu nous aimes.

2.
Qui peut comprendre ta parole
S'il n'est prêt à répondre ?
Fais que la source d’eau vive
Nous fortifie de sa grâce.

3.
Toi qui sans cesse te révèles,
Aujourd’hui tu t’approches.
Que l'esprit donne à l'Église
De proclamer ta louange !

4.
Monte vers toi l'action de grâce
Dont tressaille ton verbe :
C’est lui qui ouvre le livre
Où resplendit sa présence.