GLOIRE ET LOUANGE À TOI,
SEIGNEUR JÉSUS.

Dieu a tellement aimé le monde
qu’il a donné son Fils unique.
Tout homme qui croit en lui
possède la vie éternelle.