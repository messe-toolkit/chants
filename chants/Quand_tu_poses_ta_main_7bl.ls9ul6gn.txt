1.
Je viens me prosterner, émerveillé
Par Ta beauté Ô mon Dieu
Je viens m’agenouiller, le cœur inondé
Par Tes bienfaits Ô mon Dieu

R.
Quand Tu poses Ta main
Comme on ouvre un chemin
Ton cœur se donne à moi
Amour parfait, immérité
\n
Quand vers Toi je reviens
Mes peurs ne sont plus rien
J’étais perdu sans Toi
Mais me voici ressuscité
Quand Tu poses Ta main

2.
Perdu dans mes péchés, désespéré
Je me tournais vers les Cieux
Posé dans le silence, en Ta présence
Pour T’invoquer Ô mon Dieu