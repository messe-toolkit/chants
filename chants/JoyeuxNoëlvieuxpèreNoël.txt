JOYEUX NOËL
VIEUX PÈRE NOËL,
JOYEUX NOËL !
JOYEUX NOËL
VIEUX PÈRE NOËL,
JOYEUX NOËL !

1.
Ca fait beaucoup d’années-traîneau
Que tu m’apportes des cadeaux :
Pour ce Noël ne m’offre rien,
Mais viens quand même sous mon sapin,
Je veux garder en souvenir
Ton seul plaisir d’offrir.

2.
Je t’ai laissé sous le sapin
Un peu de blé de mon jardin :
Emporte-le pour les enfants
De l’Éthiopie ou du Soudan,
Emporte-le pour les enfants
Qui n’en mangent pas souvent.

3.
Je t’ai laissé sous le sapin
Quelques jouets que j’aime bien :
Emporte-les pour les enfants
De Roumanie ou du Liban,
Emporte-les pour les enfants
Qui n’ont plus de parents.