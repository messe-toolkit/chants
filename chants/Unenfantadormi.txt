1.
Un enfant a dormi dans nos berceaux
Un garçon venu du Dieu Très-Haut.
Jésus, Fils du Père,
Saurons-nous ouvrir les yeux,
A ton étoile ?

2.
Des bergers sont venus jusqu’au petit.
“ Paix à vous ! ” ont dit les messagers.
Jésus, Fils du Père,
Saurons-nous fêter Noël,
A ton étoile ?

3.
Des savants ont marché depuis l’Orient
Apportant de grands et beaux présents.
Jésus, Fils du Père,
Saurons-nous confier nos pas,
A ton étoile ?

4.
Un enfant a logé dans nos maisons
Un garçon aimé de ses parents.
Jésus, Fils du Père,
Saurons-nous courir vers toi,
A ton étoile ?