JE TE CHANTERAI, SEIGNEUR,
TANT QUE JE VIVRAI.

1.
Heureux qui s’appuie sur le Dieu de Jacob,
qui met son espoir dans le Seigneur son Dieu,
lui qui a fait et le ciel et la terre.

2.
Il garde à jamais sa fidélité,
Il fait justice aux opprimés ;
aux affamés il donne le pain.

3.
Le Seigneur redresse les accablés,
le Seigneur aime les justes,
le Seigneur protège l’étranger.

4.
Il soutient la veuve et l’orphelin.
D’âge en âge, le Seigneur régnera :
ton Dieu, à Sion, pour toujours !