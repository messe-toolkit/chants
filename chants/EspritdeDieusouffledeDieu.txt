R.
Esprit de Dieu, souffle de Dieu,
Force de Dieu,
Viens dans le monde, viens en nous !

1.
Tu as bercé ce monde immense
Et fait de la terre un jardin
Aux pas de l’homme et de la femme,
Pour y mener le jeu de la rencontre
Vers les pas de leur Dieu.

2.
Tu as conduit par la tendresse
Un peuple choisi par bonté.
Sa longue marche au long des siècles
Comme à tâtons découvre le mystère :
Que tout homme est aimé.

3.
Tu nous rassembles en un seul peuple
Dès aujourd’hui du monde entier,
Pour célébrer l’amour du Père
Par sa Parole, don de sa tendresse,
Jésus-Christ, Bien-Aimé !

4.
Tu as semé dans nos mémoires
Le feu des artisans de paix,
Et tu nous mènes vers les pauvres :
Voici nos mains levées pour le royaume,
Nos chemins redressés !