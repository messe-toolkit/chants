1.
Celui qui t’a créée
Ne froisse pas la page
Il se réjouit de toi
Tes cheveux, ton visage
Et laisse ton sourire
Réchauffer nos banlieues
Laisse flotter tes cheveux
Éclater ton sourire.

BATS-TOI POUR TOI
POUR LA VIE QUE TU VEUX
BATS-TOI POUR MOI
ET BATS-TOI POUR TOUS CEUX QUI CROIENT EN TOI.

2.
Celui qui t’a formée
Du profond de la terre
Il ne désire de toi
Qu’un bonheur en prière
Laisse ta liberté
Luire en tes pas de danse
Laisse épanouir ta chance
Et choisis d’exister.

3.
Celui qui te protège
Par-delà l’univers
Il te libère des pièges
De tout décret pervers
Éblouis-nous de toi
En inventant ta vie
Et en marchant à ton pas
Au rythme d’infini.