1.
L’enfant habitait
En appartement
Au vingtième étage
Tout près des nuages
Avec ses parents
La fleur se cachait
Au fond d’un jardin
Dans un vieux village
Venu d’un autre âge
Aux calmes matins
Et jamais l’enfant
Ne voyait de fleurs
Et jamais la fleur
Ne voyait d’enfant.

2.
L’enfant s’en allait
Une fois par an
Faire un long voyage
Jouer sur les plages
Et saisir le vent.
Je ne sais comment
Il trouva la fleur
Dans ce vieux village
Après un orage
Venu brusquement…
Mais jamais l’enfant
N’oublia la fleur,
Mais jamais la fleur
N’oublia l’enfant.

3.
Sans l’avoir cueillie
L’enfant s’en alla
Comme les nuages
Qui toujours voyagent
Poussés par le vent.
Mais depuis ce jour,
Éternellement
Sur toutes les pages
De cet enfant sage
La fleur va chantant
Car tous les enfants
Font chanter les fleurs
Et toutes les fleurs
Chantent les enfants.