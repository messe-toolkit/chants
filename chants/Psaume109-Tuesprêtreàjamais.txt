TU ES PRÊTRE À JAMAIS,
SELON L’ORDRE DE MELKISÉDEK.

1.
Oracle du Seigneur à mon seigneur :
« Siège à ma droite,
et je ferai de tes ennemis
le marchepied de ton trône. »

2.
De Sion, le Seigneur te présente
le sceptre de ta force :
« Domine jusqu'au coeur de l’ennemi. »

3.
Le jour où paraît ta puissance,
tu es prince, éblouissant de sainteté :
« Comme la rosée qui naît de l’aurore,
je t’ai engendré. »

4.
Le Seigneur l’a juré
dans un serment irrévocable :
« Tu es prêtre à jamais
selon l’ordre du roi Melkisédek. »