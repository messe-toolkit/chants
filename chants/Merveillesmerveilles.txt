R.
MERVEILLES, MERVEILLES,
QUE FIT LE SEIGNEUR!
JÉSUS VIENT DE NAÎTRE:
C’EST LUI LE SAUVEUR!

1.
Le fils invisible
S’est manifesté
Jésus vient de naître:
C’est lui le Sauveur !
Le Verbe adorable
Aujourd’hui est né.

2.
La splendeur du Père
Prit chair en Marie
Le Fils de lumière
Est né dans la nuit

3.
Et chantent les anges :
« Bergers levez-vous !
Venez rendre hommage
A Dieu parmi vous » !

4.
A Toi la louange
Enfant éternel !
Tu nais dans l’étable
Tu règnes au ciel !

5.
Ô toi que la terre
Ne peut contenir
La vierge, ta mère
Te berce en ses bras.

6.
Image du Père,
Son Fils bien-aimé :
Tu viens sur la terre
Nous rendre la paix.

7.
Ta sainte naissance,
Ô Emmanuel,
Redonne espérance
Nous ouvre le ciel.

8.
Au nom de ta mère,
Nous te supplions,
Tu es notre frère,
Jésus sauve nous.

9.
La vierge Marie
Gardait dans son coeur
Le très doux mystère
De Jésus sauveur.