R.
UN BOUT DE PAIN
POUR NOURRIR LA JOIE DE CROIRE.
UN BOUT DE PAIN,
UN SOLEIL DANS NOTRE HISTOIRE.

1.
Nous l’attendions comme un partage,
Une rencontre et un matin.
Nous l’attendions comme un espoir
Pour qu’Il nous montre le chemin.

2.
Sur notre route et dans nos nuits
Une Lumière : “Il” nous rejoint.
« Reste avec nous, il se fait tard »,
Pour que s’éclairent nos chemins.

3.
Comme un berger, comme un passeur
Près de la rive du Jourdain.
« Si nous savions le Don de Dieu »
La Source Vive, le chemin.