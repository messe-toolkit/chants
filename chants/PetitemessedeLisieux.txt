KYRIE
Seigneur, Seigneur, prends pitié de nous.
Ô Christ, ô Christ, prends pitié de nous.
Seigneur, Seigneur, prends pitié de nous.
SANCTUS
Saint ! Saint ! Saint, le Seigneur, Dieu de l'univers !
Le ciel et la terre sont remplis de sa gloire.
Hosanna au plus haut des cieux.
Béni soit celui qui vient au nom du Seigneur.
Hosanna au plus haut des cieux.
AGNUS
Agneau de Dieu qui enlèves le péché du monde, prends pitié de nous.
Agneau de Dieu qui enlèves le péché du monde, prends pitié de nous.
Agneau de Dieu qui enlèves le péché du monde, donne-nous la paix.