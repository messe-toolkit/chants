R.
GLOIRE À TOI QUI NOUS DÉLIVRES
DES IMPASSES DU PÉCHÉ!
GLOIRE À TOI QUI FAIS REVIVRE
PAR TON FILS RESSUSCITÉ !

1.
Dieu d’amour et de tendresse, Tu relèves le pécheur.
Par Jésus le Crucifié ton pardon nous est donné :
C’est l’eau vive du Seigneur qui jaillit dans notre coeur.

2.
Dieu d’amour et de partage, Tu nous donnes ton Esprit.
Par Jésus le Premier-Né notre souffle est rénové.
Joie pascale en nous grandit, nous allons porter du fruit.

3.
Dieu d’amour et de justice, Tu nous mènes à tes combats.
Par Jésus le Serviteur Tu révèles ta grandeur.
Chaque jour tu nous diras le chemin qui va vers toi.