Nous T’adorons, nous T’aimons, tendre Père,
Glorifie Ton nom sur la terre,
Glorifie Ton nom, (bis)
Glorifie Ton nom sur la terre.

Nous T’adorons, nous T’aimons, ô Jésus,
Glorifie Ton nom dans nos vies,
Glorifie Ton nom, (bis)
Glorifie Ton nom dans nos vies.

Nous T’adorons, nous T’aimons, Saint-Esprit,
Glorifie Jésus dans l’Église,
Glorifie Jésus, (bis)
Glorifie Jésus dans l’Église.