R.
NOTRE VIE EST COMME UNE GERBE
DONT TOUS NOS COEURS SONT LES ÉPIS,
MÛRISSANT AU SOLEIL SUPERBE
DE TON ESPRIT !

1.
Notre baptême est une semence,
Germe de vie, ineffable don !
Reçois, Seigneur, notre confiance,
Fais lever la moisson!

2.
Sur notre terre et puis dans nos âmes
Il faut du temps pour mûrir le grain
Reçois, Seigneur, notre patience
Et donne-nous ton pain.

3.
Quand notre vie nous semble stérile
Par ton amour, tu nous formeras
Reçois, Seigneur, notre espérance,
Accorde-nous la foi !

4.
Dans la lumière de ton évangile
À notre tour, nous irons semer
Reçois, Seigneur, notre promesse,
Accorde-nous la paix !