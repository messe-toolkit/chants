A
Venez, peuple fidèle,
venez à la source d’immortalité :
approchez-vous du saint mystère,
entrez dans la Pâque du Christ !
B
Avançons-nous
avec crainte et confiance !
C
Les mains et le coeur purifiés,
prenons part à l’offrande qui nous sauve !
D
L’Agneau de Dieu s’est livré pour nous,
il s’est offert au Père.
E
Honneur et gloire à l’Agneau de Dieu.
Adorons-le : c’est le Seigneur !
Chantons pour lui avec les anges :
F
Alléluia, Alléluia, Alléluia !