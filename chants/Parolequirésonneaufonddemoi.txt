R.
PAROLE QUI RÉSONNE AU FOND DE MOI,
TU FAIS TOMBER LES MURS DE MON SILENCE.
PAROLE QUI RÉSONNE AU FOND DE MOI,
TU FAIS JAILLIR LE CRI DE MA NAISSANCE;
PAROLE QUI RÉSONNE AU FOND DE MOI.

1.
J’entends vibrer le chant de notre monde,
Milliers de sons, milliers de bruits.
Enfin, Seigneur, mes lèvres te répondent,
À toi qui viens je dis merci.

2.
J’entends ta voix, Prophète de lumière,
Tes mots d’amour, tes mots de vie.
Mon coeur écoute et j’ouvre mes oreilles,
Pour toi ma langue se délie.

3.
J’entends crier les pauvres de la terre :
Clameurs de faim, clameurs d’espoir !
Qui leur dira : « le Maître vous libère »
Sinon l’amour qui parle en moi ?