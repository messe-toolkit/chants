R.
JE REVIENS
TES BRAS SONT GRANDS OUVERTS
ET TU NE SAIS QUE FAIRE
POUR M’ACCUEILLIR CHEZ TOI,
LÀ-BAS.
JE REVIENS
TU CONNAIS LE PARDON
JE CONNAIS TA MAISON
IL FAIT SI BON CHEZ TOI,
LÀ-BAS.

1.
Tant besoin
Comme un enfant prodigue
Au coeur lourd de fatigue
Avec ses désaveux, mon Dieu.
Besoin de toi, tu vois,
Parce que tu es mon Dieu.

2.
Tant besoin
Comme un Zachée dans l’arbre
Transforme un coeur de marbre
En un coeur généreux, mon Dieu.
Besoin de toi, tu vois,
Parce que tu es mon Dieu.

3.
Tant besoin
Une Samaritaine
M’a appris qu’un : « Je t’aime »
Est d’abord don de Dieu, mon Dieu.
Besoin de toi, tu vois,
Parce que tu es mon Dieu.