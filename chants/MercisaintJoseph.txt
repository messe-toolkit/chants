1.
Merci saint Joseph, toi le charpentier,
D’avoir pris Marie, celle que tu aimes.
Tu entoureras l’enfant nouveau-né,
L’enfant de Marie, celle que tu aimes !

MERCI DU FOND DE NOS COEURS,
MERCI POUR TANT DE BONHEUR,
ET NOUS TE CHANTONS EN CHOEUR,
JOSEPH, SERVITEUR DU SEIGNEUR,
HUMBLE SERVITEUR DU SEIGNEUR.

2.
Merci saint Joseph, toi qui fais confiance,
En prenant Marie mère du Sauveur,
Tu entoureras Jésus, l’espérance,
L’enfant de Marie pour notre bonheur !

3.
Merci saint Joseph, père de nos familles,
Quand tu prends chez toi Marie et Jésus,
Tu veilles sur nous comme un grand ami,
Et tu nous invites à aimer Jésus.