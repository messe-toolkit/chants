Roi céleste, Consolateur, Esprit de Vérité,
toi qui es partout présent et qui remplit tout,
trésor de bien qui donne la vie,
viens et demeure en nous,
purifie-nous de toute souillure
et sauve nos âmes,
toi qui es bonté.

L’Esprit et l’Epouse disent « Viens ! »
Que vienne ta grâce, que ce monde passe !
Et tu seras tout en tous !

Viens lumière véritable et joie de l’esprit !
Viens, nuée qui distille la rosée !
Viens, et donne-nous de goûter à la joie de ton effusion !
Viens, et réjouis-nous de l’abondance de tes dons !
Viens, Esprit consolateur et demeure en nous !