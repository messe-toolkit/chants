STANCE
J'ai longtemps cherché, Seigneur, longtemps cherché le bonheur,
Sur des chemins fuyants : il m'échappait, comme l'eau s'écoule entre les doigts.
Mais un jour j'ai compris que toi seul, ô mon Dieu, toi seul es ma joie.

R.
Seigneur, tu es l'amour et la vie, ô ma joie.

1.
Tu m'as ouvert les yeux, tu m'as ouvert le cœur.

2.
Tu m'as donné le monde, pour y vivre ton amour.

3.
Chaque jour ceux qui te cherchent, te reçoivent en vérité.