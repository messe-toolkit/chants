1.
La lumière a jailli
Voici des temps nouveaux
La mort devient la Vie
Elle n’est plus au tombeau.

SORTEZ DANS LES RUES,
LES VILLES ET LES VILLAGES
POUR ANNONCER
L’INCROYABLE NOUVELLE .
SORTEZ DANS LES RUES,
LA JOIE SUR VOS VISAGES,
POUR ANNONCER
LE CHRIST RESSUSCITÉ !

2.
Madeleine est venue
Le coeur plein de chagrin.
Dans un élan soudain
Elle reconnaît Jésus.

3.
Pierre et Jean ont couru
Pour voir la Vérité.
D’un seul coeur ils ont cru
Ils sont tout bouleversés.

4.
Aujourd’hui dans la foi
Vivez en vrais témoins :
Oui, marchez dans ses pas
C’est lui votre seul chemin.