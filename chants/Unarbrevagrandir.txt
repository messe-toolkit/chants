R.
UN ARBRE VA GRANDIR
PLANTÉ AU COEUR DES HOMMES
UN ARBRE VA SURGIR
ET RÉVEILLER LE MONDE
UN ARBRE VA GRANDIR
ET TRANSFORMER LE MONDE
AVEC TOUS CEUX QUI SÈMENT
ON LE VERRA FLEURIR

1.
Un arbre avec des noms couleur jardin
Un arbre issu d’un peuple de témoins
Prophètes et croyants
D’hier, de maintenant
Racines au fil du temps
Sève d’un peuple de vivants

2.
Un arbre avec un corps solide et fort
Un arbre au coeur qui bat après la mort
Chemin vers notre Dieu
Partout et en tous lieux
La vie un don précieux
Sève d’un peuple bienheureux

3.
Un arbre avec des bras tendus si haut
Un arbre pour qui la vie est un cadeau
Prières de merci
De jour comme de nuit
Des mains qui sont unies
Sève d’un peuple qui fleurit

4.
Un arbre qui fait danser chaque saison
Un arbre, une promesse, une moisson
Semence à travers champs
Demain, en ce moment
Des graines de plein-vent
Sève d’un peuple de Printemps