1.
Entre les mains de notre Père
Où l’homme est appelé
Du fond de sa misère,
Nous te laissons partir ;
Le Dieu qui a pétri
Au corps de Jésus Christ
Ta chair et ton esprit
Saura bien t’accueillir :
Ta place est pour l’éternité
Entre les mains de notre Père.

2.
Entre les mains de notre Père
Plus douces que nos mains,
Plus fortes que la terre,
Nous déposons ton corps ;
Le Dieu qui a donné
L’amour et l’amitié
Ne peut nous séparer
A jamais par la mort :
Un jour nous ne serons plus qu’un
Entre les mains de notre Père.

3.
Entre les mains de notre Père
Qui voit chaque douleur,
Qui sait toute prière,
Nous retrouvons l’espoir :
Le Dieu qui est venu
Nous dire par Jésus
La joie de son salut
Ne peut pas décevoir !
Comment ne pas reprendre coeur
Entre les mains de notre Père ?