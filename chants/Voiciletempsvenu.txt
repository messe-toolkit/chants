R.
Voici le temps venu : je monte vers mon Père, alléluia.

1.
Ne vous attristez pas ; que votre cœur ne se trouble pas !
Je prierai pour vous le Père, afin qu'il vous garde lui-même.

2.
Il vous donnera un autre Paraclet, l'Esprit de vérité, pour être avec vous à jamais :
Votre cœur se réjouira.

3.
Rendons gloire au Père tout-puissant, à son Fils Jésus-Christ, le Seigneur,
À l'Esprit qui habite en nos cœurs, au Dieu qui est, qui était et qui vient.