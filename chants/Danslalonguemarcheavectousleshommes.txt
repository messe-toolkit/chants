1.
Dans la longue marche avec tous les hommes,
Dans la longue marche avec tous les hommes,
Pour ouvrir des chemins nouveaux.
Nous avons osé reconnaître l'Esprit de Dieu,
Nous avons appris à connaître la main de Dieu.

2.
Dans la longue marche avec ceux qui luttent,
Dans la longue marche avec ceux qui luttent,
Pour défendre les opprimés.
Nous avons osé reconnaître l'Esprit de Dieu,
Nous avons appris à connaître l´amour de Dieu.

3.
Dans la longue marche avec ceux qui peinent,
Dans la longue marche avec ceux qui peinent,
Et travaillent à se libérer.
Nous avons osé reconnaître l'Esprit de Dieu,
Nous avons appris à reconnaître la vie de Dieu.

4.
Dans la longue marche avec ceux qui souffrent,
Dans la longue marche avec ceux qui souffrent,
Et regardent le Crucifié.
Nous croyons qu'en nous peut renaître l´Esprit de Dieu,
Nous avons appris à connaître le cœur de Dieu.

5.
Dans la longue marche avec ceux qui chantent,
Dans la longue marche avec ceux qui chantent,
Jésus Christ le Ressuscité.
Nous savons qu´en nous peut renaître l'Esprit de Dieu,
Nous avons appris à connaître la ]oie de Dieu.