R.
Je verserai sur vous une eau pure,
Et vous serez purifiés, dit le Seigneur.
Je vous donnerai un cœur nouveau.

1.
Pitié pour moi, mon Dieu, dans ton amour,
Selon ta grande miséricorde, efface mon péché.
Lave-moi tout entier de ma faute,
Purifie-moi de mon offense.

2.
Oui, je connais mon péché,
Ma faute est toujours devant moi.
Contre toi, et toi seul, j'ai péché,
Ce qui est mal à tes yeux, je l'ai fait.

3.
Fais que j'entende les chants et la fête :
Ils danseront, les os que tu broyais.
Rends-moi la joie d'être sauvé ;
Que l'Esprit généreux me soutienne.