R.
NE T’ATTENDS PLUS,
COMMENCE LA BATAILLE
ET NOUS IRONS FLEURIR NOS LENDEMAINS
SEMER L’ESPOIR DANS LES DÉSERTS DE LARMES
AMI, SI TU ME TENDS LA MAIN

1.
J’ai ton regard comme une certitude
Et ton visage habite ma chanson
Hissons la voile et cap sur le grand sud
Avec l’Amour qui battra pavillon

2.
Toutes nos voix chant’ront d’un même coeur
Nous relèv’rons l’homme qui est à genoux
Pas de fusil pour un monde meilleur
Car après tout, AIMER c’est être fou