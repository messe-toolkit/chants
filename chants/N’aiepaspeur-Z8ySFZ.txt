N’aie pas peur, laisse-toi regarder par le Christ;
Laisse-toi regarder car Il t’aime. (bis)

1.
Il a posé sur moi son regard, un regard plein de tendresse.
Il a posé sur moi son regard, un regard long de promesse.

©Droits d'auteur