Dans la lumière de ce jour de Noël, Père, nous te chantons :

JOIE AU CIEL, EXULTE LA TERRE !

1.
Aujourd’hui, le Seigneur vient consoler son peuple
et la joie renaît dans le monde.

2.
Aujourd’hui, le Seigneur manifeste sa tendresse
à ceux qui viennent s’agenouiller devant l’Enfant de la crèche.

3.
Aujourd’hui, le Verbe de Dieu prend sur lui notre humanité
et devient l’un de nous.

4.
Aujourd’hui, la lumière traverse nos ténèbres !