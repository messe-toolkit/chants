R.
Quand on mange ce pain,
Jésus vient dans nos cœurs ;
Quand on mange ce pain,
On n’a plus jamais peur.

1.
Jésus nous aime sans compter ;
Il est l’amour.
Il est l’amour.
Jésus nous aime sans tricher :
Il est l’amour.
Il est l’amour.

2.
Jésus porte avec sa croix,
Il est l’amour.
Il est l’amour.
Jésus nous porte dans la foi :
Il est l’amour.
Il est l’amour.

3.
Jésus nous guide vers la vie :
Il est l’amour.
Il est l’amour.
Jésus nous guide vers Marie :
Il est l’amour.
Il est l’amour.