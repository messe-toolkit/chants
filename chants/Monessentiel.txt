JE VEUX RETROUVER MON ESSENTIEL
PLUS PRÈS DE TOI MON DIEU
RÉAPPRIVOISER LE TEMPS,
JE VEUX RETROUVER MON ESSENTIEL
PLUS PRÈS DE TOI MON DIEU
DONNER À MA VIE UN SENS, AVEC TOI J’AI CONFIANCE

1.
Chercheur d’or, je suis chercheur d’or
En quête de sensationnel, d’émotions fortes et d’actuel
Je vis le moment présent à cent pour cent sans prendre le temps
D’apprendre à m’arrêter, tendre la main à l’étranger

2.
Chercheur d’or, je suis chercheur d’or
Dans ce désert de poussières, où je ne ramasse que des pierres
Si aujourd’hui avoir c’est être, alors je ne veux plus être
Me poser discerner, retrouver ma simplicité

3.
Chercheur d’or, je suis chercheur d’or
Éprouvé par les questions de la mode et de mes passions
Je voudrais me fondre dans la masse et pourtant trouver ma place
Je me sens appelé à montrer mon identité