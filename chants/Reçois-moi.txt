R.
REÇOIS-MOI
PRENDS-MOI AVEC TOI
QUE JE VIVE EN TOI
ET TOI EN MOI

Eucharistie

1.
Par ce pain que tu partages avec moi,
Corps livré pour tous sur la Croix,
Par ce pain que tu partages avec moi,
Corps qui unit tout en lui,

2.
Par ce vin que Tu partages avec moi,
Sang versé pour tous sur la Croix,
Par ce vin que Tu partages avec moi,
Sang de l’Alliance éternelle,

Sacrement du frère

3.
Par le frère que tu mets sur ma route,
Et que tu m’invites à aimer,
Par le frère que tu mets sur ma route,
Reflet vivant de ta Face,

Vie quotidienne

4.
Dans les gestes que je fais chaque jour,
Où j’apprends le don dans la joie,
Dans les gestes que je fais chaque jour,
Vie unifiée par l’Amour,

Prière

5.
Dans la paix de ce silence avec Toi,
Coeur à coeur brûlant de désir,
Dans la paix de ce silence avec Toi,
Souffle discret de l’Esprit,

Baptême

6.
Par cette eau bénie versée sur mon front,
Signe que je suis ton enfant,
Par cette eau bénie versée sur mon front,
Vie nouvelle en ton Amour,

Confirmation

7.
Par cette huile parfumée sur mon front,
Signe que je suis ton disciple,
Par cette huile parfumée sur mon front,
Vie nouvelle en ton Esprit,

Mariage

8.
Par ce oui qui nous unit aujourd’hui,
Devant ton Eglise assemblée,
Par ce oui qui nous unit aujourd’hui,
Sceau de l’Amour en nos vies,

Entrée dans la vie consacrée

9.
En ce jour où je te donne ma vie,
Pour que ton Esprit la conduise,
En ce jour où je te donne ma vie,
Comme une offrande d’amour,

Sacrement de l’Ordre

10.
Bon Pasteur de ton Eglise, ô Jésus,
Tu m’as appelé, me voici !
Bon Pasteur de ton Eglise, ô Jésus,
Fais de moi ton envoyé

Sacrement des malades

11.
Par cette huile sur mon front et mes mains,
Feu de ton Esprit en mon corps,
Par cette huile sur mon front et mes mains,
Force nouvelle en ma vie,

Funérailles

12.
En ce jour où tu accueilles ma vie,
M’offrant ta tendresse infinie,
En ce jour où tu accueilles ma vie,
Paix du repos en ton Coeur.