1.
La joie de l’Évangile,
C’est le don que Dieu nous fait :
Message qui délivre,
Bienheureux qui le transmet !
Le monde a soif de renaissance,
Allumons chez lui une espérance !

JOIE DE JÉSUS CHRIST,
BRÛLE-NOUS DE TA LUMIÈRE,
JOIE DE JÉSUS CHRIST,
METS LE FEU À NOTRE TERRE !

2.
La joie de l’Évangile
Nous transforme en vérité ;
Reflet de vie divine
Qui demande à s’embraser.
Le monde a soif de renaissance,
Allumons chez lui une espérance !

3.
La joie de l’Évangile
Dans le Souffle du Vivant,
Sa flamme en nous jubile
Pour la fête des croyants.
Le monde a soif de renaissance,
Allumons chez lui une espérance !

4.
La joie de l’Évangile,
Quel rocher pour notre foi !
Sa force en nous résiste
Aux clameurs du désespoir.
Le monde a soif de renaissance,
Allumons chez lui une espérance !

5.
La joie de l’Évangile,
Chant nouveau des libérés,
L’étoile qui scintille
Sur les ruines du passé.
Le monde a soif de renaissance,
Allumons chez lui une espérance !

6.
La joie de l’Évangile :
Un amour qui mène loin,
Vécu dans une Église
Réveillée par ses témoins !
Le monde a soif de renaissance,
Allumons chez lui une espérance !

7.
La joie de l’Évangile
“porte-ouverte sur dehors” !
L’audace des disciples
Qui ne craignent pas la mort.
Le monde a soif de renaissance,
Allumons chez lui une espérance !

8.
La joie de l’Évangile
Met en marche vers l’humain ;
Heureux qui prophétise
Comme un bon Samaritain !
Le monde a soif de renaissance,
Allumons chez lui une espérance !

9.
La joie de l’Évangile
Chaque jour est un défi :
Nos gestes soient fertiles
Pour les pauvres et les petits !
Le monde a soif de renaissance,
Allumons chez lui une espérance !