POURQUOI MONSIEUR LE JARDINIER.
OUI, POURQUOI, POURQUOI PLEUREZ-VOUS?
POURQUOI MONSIEUR LE JARDINIER.
OUI, POURQUOI, POUR QUI VOUS PLEUREZ?

1.
J’avais préparé là, le plus beau des jardins
La terre était fertile, un merveilleux terrain
J’avais tout vérifié, surveillé les labours
Mis de côté des graines, assez pour chaque jour

POURQUOI MONSIEUR…

2.
Et je l’avais confié à chacune à chacun
Aux enfants de partout qui venaient au matin
Ils avaient les outils et ils avaient la clé
Et quand ils le voulaient, ils pouvaient y planter

POURQUOI MONSIEUR…

3.
Un ouragan violent est venu, triste jour
Arracher leur travail, et tout casser autour
Alors ils sont partis sans même se retourner
Et ceux qui sont restés contre moi ont crié

ALORS MONSIEUR LE JARDINIER.
JE CROIS QUE VOUS AVEZ PLEURÉ
ALORS MONSIEUR LE JARDINIER.
JE CROIS QUE VOUS AVEZ PLEURÉ

4.
Quelques années plus tard, ils ont serré les poings
Ils ont voulu revoir chaque part du terrain
Je les ai vus se battre, s’arracher cette terre
Et remplacer les fleurs par les fruits de la guerre

ALORS MONSIEUR…

5.
Ce jardin est très loin des récoltes rêvées
Pourtant je reste là et je veux espérer
J’ai encore tant de plants, tant de graines à semer
Tant de travail à faire, tant d’amour à donner

ALORS MONSIEUR…