Vers toi, Seigneur,
Que monte aujourd’hui notre prière !
Tourne ton regard
vers ce monde que Tu aimes.