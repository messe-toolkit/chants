R.
Allez- vous en sur les places et sur les parvis !
Allez-vous en sur les places y chercher mes amis,
Tous mes enfants de lumière qui vivent dans la nuit,
Tous les enfants de mon Père séparés de Lui,
Allez-vous en sur les places
Et soyez mes témoins chaque jour.

1.
En quittant cette terre, je vous ai laissé
Un message de lumière, qu’en avez-vous donc fait ?
Quand je vois aujourd’hui mes enfants révoltés,
Aigris et douloureux d’avoir pleuré !

2.
En quittant cette terre, je vous ai donné
La justice de mon Père
L’avez-vous partagée ?
Quand je vois, aujourd’hui, mes enfants qui ont peur
Sans amour, et sans foi et sans honneur.

3.
En quittant cette terre, je vous avais dit:
Aimez-vous comme des frères,
M’avez-vous obéi ?
Quand je vois aujourd’hui, mes enfants torturés,
Sans amis, sans espoir, abandonnés.