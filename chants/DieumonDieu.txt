REFRAIN 1.
(prière du matin)
Dieu, mon Dieu, je te prie, je te reçois
Dans ce jour qui se lève.
Dieu, mon Dieu, je te prie, je te reçois
Jusqu’ au fond de mes rêves.

REFRAIN 1.
(prière du soir)
Dieu, mon Dieu, je te prie, je te reçois
Dans ce jour qui s'achève.
Dieu, mon Dieu, je te prie, je te reçois
Jusqu’ au fond de mes rêves.

1.
Toi, le Créateur,
Ô toi l’Eternel,
Toi le Tout-Puissant,
Ô toi le Très-Haut !

2.
Toi qui guéris,
Ô toi mon refuge,
Toi qui sancitfies,
Ô toi, notre justice !

3.
Toi, le bienfaiteur,
Ô toi, l'infini,
Toi le gardien,
Ô toi, qui élèves !

REFRAIN 2.
Dieu, mon Dieu, je me tourne vers toi,
Ta louange sans cesse à mes lèvres.