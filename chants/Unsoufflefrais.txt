JE NE VOIS PAS LE VENT,
MAIS JE SAIS CE QU’IL FAIT,
J’AIME QUAND LE VENT
ME DONNE UN SOUFFLE FRAIS. bis

1.
Quand je ne sais pas
Parler sans colère,
Que vienne souffler
Le vent qui m’apaise.

2.
Quand je ne veux pas
Donner un peu de temps,
Que vienne souffler
Le vent si patient.

3.
Quand je n’aime pas
Jouer sans tricher,
Que vienne souffler
Le vent qui dit vrai.