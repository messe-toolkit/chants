R.
COMMENT VAS-TU MONSIEUR LE MONDE,
EST-C'QUE ÇA TOURNE, TOURNE, TOURNE, TOURNE ROND ?
ÉCOUTE UN PEU QUELQUES SECONDES,
LES DÉSIRS FOUS ET LES PROJETS DE NOS CHANSONS.

1.
Entre soleil et la grisaille,
génération vidéo-clip, tag et zapping,
"Dieu-audimat " : l'os à scandale…
Prends nos chansons pour te refaire un vrai brushing !

2.
Pourquoi flasher l'info-spectacle
En version rap, version "show-biz", funk ou "branché",
Tu peux ranger tes "pubs-miracles",
Tes mots sont creux et ton futur c'est du passé.

3.
Tes grands discours ont froid à l'âme,
A moitié "stone" et tu grelottes d'exister !
Ranime un peu en toi la flamme
Pour redonner au moins une chance à la Paix !

4.
Où êtes-vous liseurs d'étoiles :
Tous les rêveurs encor' tournés vers l'au-delà ?
N'attendez plus, hissez la voile,
Dès aujourd'hui; dès maintenant, dès ici-bas !