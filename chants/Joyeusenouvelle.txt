R.
Joyeuse nouvelle : il est né, le messie, le Seigneur !
Ou
Joyeuse nouvelle : il est né, le messie, le Sauveur.

1.
L'ange du Seigneur apparut aux bergers et la gloire de Dieu les illumina
Ne craignez pas, dit l'ange, c'est la joie que j'annonce.

2.
Voilà le signe qui vous est donné : vous trouverez un nouveau-né,
Enveloppé de langes et couché dans une crèche : c'est le Sauveur du monde.

3.
Gloire à Dieu au plus haut des cieux, gloire à Jésus notre Sauveur,
Gloire à l'Esprit qui habite en nos cœurs, Alléluia, Alléluia.