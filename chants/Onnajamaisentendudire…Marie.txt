1.
On n’a jamais entendu dire
Que ceux qui se tournent vers vous
Pour le meilleur et pour le pire
Souvenez-vous…
On n’a jamais entendu dire
Quoi qu’il en soit et malgré tout
Aux nuits de peurs, aux jours de rire
Souvenez-vous…

NOUS SES ENFANTS, SOUVENONS-NOUS:
QUE MARIE RESTE NOTRE MÈRE
ON N’A JAMAIS ENTENDU DIRE LE CONTRAIRE
TOUT SIMPLEMENT SOUVENONS-NOUS.

2.
On n’a jamais entendu dire
Quand le bateau prend de partout
Quand les mirages nous attirent
Souvenons-nous…
On n’a jamais entendu dire
Lorsque revient le soleil d’août
Ou lorsque l’hiver nous déchire.
Souvenons-nous…

3.
Et lorsque les hommes délirent
Et que ce monde devient fou.
Et que tout conspire à nous nuire
Souvenons-nous…
Et puis, quoiqu’on puisse me dire
Je reste confiant jusqu’au bout
Et qui pourra me contredire
Surtout pas vous.