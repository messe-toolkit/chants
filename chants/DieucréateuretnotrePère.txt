R.
Dieu créateur et notre Père,
Toi qui tiens l'univers en ta main,
Ouvre nos coeurs, ouvre nos lèvres,
Que nos voix te célèbrent sans fin !

1.
Pour toi, Seigneur, tu nous as faits,
Et ta Parole nous recrée.
Fais-nous la grâce au long du jour
De dire au monde ton amour.

2.
Béni sois-tu pour ton appel
À te connaître et te louer !
Fais-nous la grâce au long du jour
De te chanter avec amour.

3.
Révèle-nous par ton Esprit
La voie qui mène à Jésus Christ.
Fais-nous la grâce au long du jour
De te servir avec amour.