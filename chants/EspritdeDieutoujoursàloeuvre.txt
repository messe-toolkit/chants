R.
Christ et Seigneur toujours présent,
Tu fais revivre les déserts
Christ et Seigneur, buisson ardent,
Ton feu s’étend sur l’univers.

1.
Esprit de Dieu toujours à l’oeuvre,
Sans toi le Christ est du passé,
Témoin d’hier parmi son peuple,
Quelqu’un pétri d’humanité.

Mais avec toi, Esprit de force,
Le Fils de l’Homme est un Vivant,
Et sa Nouvelle n’est pas morte,
Son cri résonne en notre temps.

2.
Esprit de Dieu qui fais l’Église,
Sans toi nous sommes dispersés,
Nous n’avons rien de ces disciples
Que le Sauveur veut rassembler.

Mais avec toi qui donnes souffle
Grandit l’Église-communion,
Par d’humbles voies qui nous déroutent
Vers l’unité nous avançons.

3.
Esprit de Dieu sur notre terre,
Sans toi le grain ne peut germer
Ni s’épanouir à la lumière
Pour devenir un champ de blé.

Mais avec toi, puissante Brise,
Des grains d’amour s’éveilleront,
Froments nouveaux de l’Évangile,
Promesses riches de moissons.

4.
Esprit de Dieu qui régénères,
Sans toi qui donc voudrait servir,
Tracer des routes qui libèrent
Et préparer un avenir ?

Mais avec toi qui mènes au large
Nous choisirons d’aller plus loin ;
Jésus demeure en notre barque,
Il est la Vie et le Chemin.

5.
Esprit de Dieu dans notre monde,
Sans toi nul être ne vivra.
Loin de l’eau vive très féconde,
A quelle source il puisera ?

Mais avec toi c’est le Royaume
Qui prend naissance et qui fleurit.
Voici des jours de Pentecôte
Où notre Pâque s’accomplit.