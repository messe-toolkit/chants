Que sert-il à l’homme
De posséder richesses
en overdose
S’il n’aime personne
Si son coeur
est prisonnier des choses
Que sert-il à l’homme
De gagner l’univers
entre autre choses
S’il y perd son âme
La raison qui l’entraîne,
l’ultime cause
Comme un enfant
J’entends parfois
Cette douce voix
qui m’appelle
Viens, toi qui es fatigué
Toi qui es chargé
Et je te donne du repos
Toi qui as tant marché
Toi qui a tant peiné
Entends ma voix qui te dit
Viens, toi qui es prisonnier
Toi qui es altéré
Et je te donnerai de l’eau
Suis-moi et viens goûter
Combien doux est mon fardeau
Avant ton passage
Tout était rangé, trié, classé
Une vie si froide, si sage
Comme dans un musée de cire, figée
Surtout ne pas oser
Surtout ne pas risquer
la blessure du coeur
Se protéger des autres
Dresser des murs que tu as renversé
en un clin d’oeil alors
Devenir pauvre au monde
Mais être riche en toi,
moi le mendiant
Me dépouiller des ombres
Entrer dans la lumière en titubant
Et derrière le vent
Encore une fois
Cette douce voix qui m’appelle
Je ne te force pas
Je ne t’oblige pas
C’est à toi de faire ton choix