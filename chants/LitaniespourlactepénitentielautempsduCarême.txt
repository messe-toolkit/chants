Carême Année A

Seigneur Jésus,
Source d’eau vive
pour ceux qui ont soif de ta parole,
prends pitié de nous,
prends pitié de nous.

O Christ,
Chemin de lumière
pour ceux qui te cherchent dans la nuit,
prends pitié de nous,
prends pitié de nous.

Seigneur Jésus,
Vivant auprès du Père,
pour recevoir ceux qui traversent la mort,
prends pitié de nous,
prends pitié de nous.

Carême Année B

Seigneur Jésus,
poussé au désert par l’Esprit
pour trouver ta nourriture
dans la parole de Dieu,
prends pitié de nous,
prends pitié de nous.

O Christ,
transfiguré sur la montagne
pour nous conforter dans l’espérance
de ta résurrection,
prends pitié de nous,
prends pitié de nous.

Seigneur Jésus,
prenant résolument la route de Jérusalem
pour accomplir avec courage
la volonté du Père,
prends pitié de nous,
prends pitié de nous.

Carême Année C

Seigneur Jésus,
tu conduis tes frères
sur le chemin du pardon :
prends pitié de nous,
prends pitié de nous.

O Christ,
tu ne veux pas la mort du pécheur
mais qu’il vive :
prends pitié de nous,
prends pitié de nous.

Seigneur Jésus,
tu intercèdes pour nous
auprès du Père de toute miséricorde :
prends pitié de nous,
prends pitié de nous.