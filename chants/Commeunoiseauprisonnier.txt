R.
Comme un oiseau dans la cage,
Mon cœur est prisonnier !
Et, dans le creux des nuages,
J'ai envie de m'envoler !

1.
Car mon cœur n'est pas fait pour la terre !
Ici, je ne peux pas chanter

2.
Mais il n'est pas de lieu, en ce monde !
Pour moi, il n'est pas de repos.

3.
Et pourvu que je garde mes ailes !
Un jour je pourrai m'en aller.

DERNIER REFRAIN
Car, tout au fond de ma cage
Mon cœur sait bien qu'un jour
Je rejoindrai mes nuages…
Mon grand ciel… Et mon amour.