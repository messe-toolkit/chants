1.
Tandis que le monde proclame
L'oubli du Dieu de majesté,
Dans tous nos cœurs l'amour acclame,
Seigneur Jésus, Ta royauté.

R.
Parle, commande, règne,
Nous sommes tous à Toi.
Jésus, étends Ton règne,
De l'univers, sois Roi.

2.
Vrai Roi, Tu l'es par la naissance,
Vrai Fils de Dieu, le Saint des saints ;
Et ceux qui bravent Ta puissance,
Jésus, sont l'œuvre de tes mains.

3.
Vrai Roi, Tu l'es par la conquête,
Au Golgotha, brisant nos fers ;
Ton sang répandu nous rachète,
Ta croix triomphe des enfers.

4.
Vrai Roi, Tu l'es par Ton Église,
A qui tu donnes sa splendeur ;
En elle notre foi soumise
Voit vivre encore le Rédempteur.

5.
Vrai Roi, Tu l'es par Ton vicaire,
Dont tu défends l'autorité ;
Par lui tu répands la lumière
De l'infaillible vérité.

6.
Vrai Roi, Tu l'es dans cette hostie
Où Tu te livres chaque jour ;
Tu règnes par l'Eucharistie,
Gagnant les cœurs à Ton Amour.

7.
Vrai Roi, Tu l'es sur cette terre,
Mais que bientôt brille à nos yeux,
Loin de la nuit et du mystère,
Ton beau Royaume dans les cieux.