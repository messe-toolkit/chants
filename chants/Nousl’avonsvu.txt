1.
Au fond des rues et des ruelles
Nous l’avons vu
Dans ces témoins qui interpellent
Et entendu
Au fond des yeux et des visages
Nous l’avons vu
L’homme créé à son image
Et entendu.

NOUS L’AVONS VU C’EST UNE CHANCE
IL NOUS INVITE EN SA MAISON
ET ENTENDU, CETTE ESPÉRANCE
NOUS FAIT CHANTER DE JOIE SON NOM.

2.
Dans la nature et ses merveilles
Nous l’avons vu
Au fond d’une vie qui s’éveille
Et entendu.
Et même aux quatre coins du monde
Nous l’avons vu
Dans l’olivier et la colombe
Et entendu.

3.
Dans l’absence de sa présence
Nous l’avons vu
Dans ce profond et grand silence
Et entendu.
Dans le pardon, dans le partage
Nous l’avons vu
Ou dans les paroles d’un sage
Et entendu.