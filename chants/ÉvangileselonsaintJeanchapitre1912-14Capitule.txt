Le Verbe était la vraie Lumière,
qui éclaire tout homme en venant dans le monde.
À tous ceux qui l’ont reçu,
il a donné de pouvoir devenir enfants de Dieu,
eux qui croient en son nom.
Ils ne sont pas nés du sang, ni d’une volonté charnelle,
ni d’une volonté d’homme :
ils sont nés de Dieu.
Et le Verbe s’est fait chair,
il a habité parmi nous,
et nous avons vu sa gloire,
la gloire qu’il tient de son Père comme Fils unique,
plein de grâce et de vérité.