R.
BERGER DU PEUPLE DE L’ALLIANCE,
TA MAIN NOUS CONDUIT.
FAIS-NOUS GRANDIR DANS LA CONFIANCE,
JÉSUS NOTRE VIE.

1.
Fils de Dieu, le vrai Pasteur,
Tu nous connais par notre nom ;
Avec toi nous avançons
À la recherche des eaux vives.
Envoie sur ton Église
l’Esprit de sainteté !

2.
Fils de Dieu, tu parles au coeur,
Et ta parole est notre pain ;
Ouvre-nous au grand Festin
Où les vivants sont tous des frères.
Révèle à notre terre
Ton chant d’humanité.

3.
Fils de Dieu, tu es passé
Par les ténèbres de la mort.
Dans ton âme et dans ton corps
Tu as souffert jusqu’à l’extrême.
Pourquoi pareil baptême ?
Comment ne pas trembler ?

4.
Fils de Dieu ressuscité,
Nos yeux regardent vers ton jour.
Que ta paix demeure en nous,
Elle est semence de lumière.
Tu mènes vers ton Père
Le peuple des sauvés.

5.
Fils de Dieu, le seul Pasteur,
Tu veux un monde rassemblé.
Serons-nous de ces bergers
Toujours ouverts à tes attentes ?
Seigneur, viens nous apprendre
Les voies de l’unité.