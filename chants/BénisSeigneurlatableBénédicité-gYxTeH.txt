Bénis, bénis, bénis, Seigneur, la table,
Ceux qui l’ont préparé.
Viens prendre ce repas avec nous
Pour nous réjouir de ta présence
Et recevoir ta bénédiction.
Au nom du Père et du Fils
Et du Saint Esprit de Dieu.
Amen, amen, amen !