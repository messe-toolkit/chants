R.
Voici le don que Dieu nous fait
Dans le mystère de cette nuit
Voici le don que Dieu nous fait
Pour tous les hommes de la terre
Portée par Marie, voici la lumière
La terre a donné son fruit
Portée par Marie, voici la lumière
Le Messie promis.

1.
On l'appelle Emmanuel
Dieu avec nous
Le compagnon des hommes
On l'appelle Emmanuel
C'est la merveille
Venue du ciel.

2.
On l'appelle Soleil levant
Fils de David
Le défenseur des pauvres
On l'appelle Soleil levant
Son cœur réchauffe tous les vivants.

3.
Il vient en Prince de la Paix
Pour établir
Son règne de Justice
Il vient en Prince de la Paix
Mais son Royaume reste caché.