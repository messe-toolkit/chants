R.
Dieu soit béni pour nos pères dans la foi,
Imitateurs de Jésus Christ !
Béni soit Dieu de nous avoir choisis
Aujourd'hui
Pour suivre leur exemple pas à pas
Dans la joie !

1.
Ils ont dans notre histoire fait briller la lumière
Sur les aveugles et les tombeaux ;
À nous sur leur chemin de passer le flambeau :
Qu'en nos obscurités le Christ soit lumière !

2.
Ils ont dans notre histoire fait renaître une source
Qui attendait le doigt de Dieu ;
À nous sur leur chemin de nous faire audacieux :
Que marchent nos déserts vers Dieu, notre source !

3.
Ils ont dans notre histoire fait tomber les idoles
Pour rendre à tous la dignité ;
À nous sur leur chemin de savoir accepter
Que soit détruite en nous la loi des idoles !

4.
Ils ont dans notre histoire proclamé l'Évangile
De Jésus Christ, le fils de Dieu ;
À nous sur leur chemin d'attiser comme un feu
La flamme d'un amour selon l'Évangile !