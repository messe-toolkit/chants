R.
Garde ma vie simple et tranquille
Entre tes mains au seuil du jour
Entre tes mains vase d’argile
Garde ma vie en ton amour.

1.
Enraciné dans la prière
Tenir le soc dans le sillon
Ne plus regarder en arrière
La vraie grandeur, c’est le pardon.

2.
Tu m’as confié la terre entière
Je veux y semer tes chansons
Pour révéler ton cœur de Père
Et la joie de porter ton nom.

3.
Devenir signe sur la terre
Libérer en moi les prisons
Accueillir enfin ton mystère
Pour demeurer en ta maison.