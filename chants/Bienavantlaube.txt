Antienne - stance
Bien avant l'aube, Jésus se leva ;
il partit dans un endroit désert pour y prier.

R.
Confitemini Domino, quoniam bonus.
Confitemini Domino, alleluia.

Versets

Ps 15, 5-10.

1.
Seigneur, mon partage et ma coupe :
De toi dépend mon sort.
La part qui me revient fait mes délices ;
J'ai même le plus bel héritage !

2.
Je bénis le Seigneur qui me conseille :
Même la nuit mon cœur m'avertit.
Je garde le Seigneur devant moi sans relâche ;
Il est à ma droite : je suis inébranlable.

3.
Mon cœur exulte, mon âme est en fête,
Ma chair elle-même repose en confiance :
Tu ne peux m'abandonner à la mort
Ni laisser ton ami voir la corruption.