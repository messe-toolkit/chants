R.
BRISE DE PAIX, SOUFFLE DE VIE,
VIENS DANS NOS COEURS,
VIENS DANS NOS COEURS,
ESPRIT DE DIEU.

1.
Fais grandir en nous la foi,
La foi de nos baptêmes
LA FOI DE NOS BAPTEMES,
Aujourd'hui Jésus t'envoie
Ton vent nous fait renaître,
TON VENT NOUS FAIT RENAITRE,
Confirme-nous dans ton amour
CONFIRME-NOUS DANS TON AMOUR.

2.
Fais chanter en nous le feu,
Le feu de ta lumière
LE FEU DE TA LUMIERE,
Flamme vive dans nos yeux
Réchauffe notre terre,
RECHAUFFE NOTRE TERRE,
Confirme-nous dans ton amour
CONFIRME-NOUS DANS TON AMOUR.

3.
Guide-nous sur tes chemins,
Chemins de délivrance
CHEMINS DE DELIVRANCE.
Nous verrons des lendemains
Fleuris par l'espérance,
FLEURIS PAR L'ESPERANCE.
Confirme-nous dans ton amour,
CONFIRME-NOUS DANS TON AMOUR !

4.
Brûle-nous de la vraie soif,
La soif de tes eaux vives
LA SOIF DE TES EAUX VIVES,
De ta source jaillira
La joie de l'autre rive,
LA JOIE DE L'AUTRE RIVE,
Confirme-nous dan ston amour
CONFIRME-NOUS DANS TON AMOUR.