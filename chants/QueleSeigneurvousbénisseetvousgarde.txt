R.
QUE LE SEIGNEUR VOUS BÉNISSE ET VOUS GARDE,
QU’IL VOUS LAISSE ENTREVOIR SON VISAGE
ET VOUS DONNE LA PAIX.

1.
Je vous bénis durant ma vie
Autant et plus que je le puis,
Maintenant, demain, et après ma mort,
De toutes les bénédictions.

2.
Je prie Jésus, notre Seigneur,
Que par son coeur tout plein d’amour,
Il confirme en vous ces bénédictions
Avec les anges et tous les saints.

3.
Gardez la sainte pauvreté,
Persévérez jusqu’à la fin.
Demeurez toujours les amis de Dieu,
Fidèles à votre engagement.

4.
Ses fils et filles dans l’Esprit,
Que le Seigneur vous soit présent ;
Puissiez-vous aussi demeurer en lui.
Amen, Amen, Alléluia !