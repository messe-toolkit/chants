R.
SEIGNEUR, NOS VOIX TE CHANTENT,
SEIGNEUR, NE SOIS PAS LOIN,
AU JOUR OU TU T'ABSENTES
MONTRE-NOUS LE CHEMIN.

1.
Au jour où tu t'élèves,
Reste avec nous.
Jusqu'à la fin du monde
Donne ta paix !
Fleurisse l'olivier
Alentour de la table,
Jusqu'à la fin du monde
Donne ta paix.

2.
Au jour où tu te caches,
Brûle en nos coeurs.
Jusqu'à la fin du monde
Donne ta paix !
Que chante notre nuit,
Nous guettons ta lumière,
Jusqu'à la fin du monde
Donne ta paix !

3.
Au jour où tu nous manques,
Guide nos pas.
Jusqu'à la fin du monde
Donne ta paix !
Que tombe sur nos vies
Une pluie généreuse,
Jusqu'à la fin du monde
Donne ta paix !