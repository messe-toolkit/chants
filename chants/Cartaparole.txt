1.
Quand je m’éloigne de Toi
Quand j’ai lâché ta main
Et rejeté ton aide
Seigneur, tu m’attends et ta voix
Me rappelle que rien
Ne nous séparera.

CAR TA PAROLE EST UNE LAMPE
QUI BRILLE SUR CHACUN DE MES PAS
QUAND JE SUIS DANS L’OMBRE
ELLE EST LUMIÈRE
ELLE EST VIVANTE
ET ELLE ME RELÈVE
CHAQUE FOIS QUE JE TOMBE.

2.
Quand j’ai perdu le désir
De te donner ma vie
Et partout de te suivre
Seigneur tu m’apprends à tenir
Au plus noir de la nuit
Tout pourra refleurir.

3.
Quand je n’ai plus que des mots
Quand le coeur m’abandonne
Pour te dire «je veux croire»
Seigneur, Tu es le Dieu très haut
Qui donne et qui pardonne
Et je vis à nouveau.

4.
Quand j’aurai enfin percé
Ce grand, si grand mystère
Cet amour dont tu m’aimes
Seigneur, tu me feras aimer
À ton image ô Père
De toute éternité.