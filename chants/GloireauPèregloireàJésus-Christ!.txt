1. (Actes 2, 1-14 ; 2, 32-36)
Ô Jour de louange :
L’Esprit nous est donné.
En toute langue
Monte un chant nouveau.
Les promesses s’accomplissent
Pour le monde entier :
À tous les peuples,
Le bonheur de Dieu !

GLOIRE AU PÈRE,
GLOIRE À JÉSUS CHRIST !
QUE L’ÉGLISE
CHANTE POUR LEURS NOMS!
PASSE LE SOUFFLE,
EN LUI TOUTE VIE !
SUR LE MONDE,
UN JOUR NOUVEAU SE LÈVERA !

2.
(Actes 2, 37-44)
L’Esprit est à l’oeuvre
Au coeur de notre coeur.
C’est lui la flamme
Qui répand l’amour.
C’est par lui que l’Évangile
Lève dans nos vies.
Qui peut nous prendre
La joie partagée ?

3.
(Actes 4, 1-3 ; 4, 15-21)
Heureux les disciples
Témoins de Jésus Christ !
En lui leur force
Quand vient le combat.
C’est en vain que l’Adversaire
S’est dressé contre eux :
Jésus les garde
Fermes dans la foi !

4.
(Actes 6, 1-6)
Voyez la détresse
Des pauvres sans appui.
Ils sont vos frères,
Des enfants de Dieu.
Servez-les avec tendresse,
Partagez, aimez !
Le Fils de l’homme
Vous montre la voie.

5.
(Actes 8, 9-13 ; 8, 18-20)
Seigneur, en ta grâce,
Le jour de chaque jour !
En ta tendresse,
L’appel quotidien !
Que l’Esprit nous garde libres
Et heureux d’aimer :
Pour tes disciples,
Nul autre chemin.

6.
(Actes 8, 26-39)
Heureux qui te cherche,
Scrutant les mots de vie.
Heureux qui trouve
L’appui d’un témoin !
L’Esprit Saint, de proche en proche,
Dans les coeurs agit
Et dans l’Église
Ton Nom se répand.

7.
(Actes 9, 1-9)
Voici que ta force,
Seigneur, triomphe en Saul.
Il tombe à terre,
Vaincu par l’amour.
Il deviendra ton apôtre,
Témoin de la foi.
Pour nous encore,
Sa voix retentit.

8.
(Actes10,9-16; 10,19-21; 10,27-29)
L’Esprit nous rend libres,
Un monde neuf renaît,
Et tous les hommes
Sont enfants de Dieu.
Jésus Christ, dans sa tendresse,
Est Seigneur de tous
Et son offrande
Fait de nous son Corps.

9.
(Actes 14, 1-7)
Vivez en confiance,
Allez où va l’Esprit.
Il est des routes
Que vous ignorez.
C’est l’Esprit qui vous les ouvre,
Repoussez la peur.
Allez sans crainte,
Jésus est Vivant !

10.
(Actes 15, 1-15.17)
À Dieu la louange,
Lui seul nous donne vie,
Et son alliance
Est offerte à tous.
Que l’Esprit nous mène ensemble
Jusqu’au jour nouveau :
Gardons confiance,
L’amour régnera !