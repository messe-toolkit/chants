Bienheureux qui reçoit la Parole,
Dans son champ le bon grain germera.
Bienheureux l´ouvrier du Royaume,
Dieu l´invite au banquet de sa joie.

1.
Bienheureux le coeur de pauvre,
Son trésor est Jésus Christ.
Dans sa vie, l´amour l´emporte,
Terre et ciel lui sont promis.

2.
Bienheureux le pacifique,
“Ô fils de Dieu” sera son nom.
Bienheureux s’il ose vivre
l’aventure du pardon.

3.
Bienheureux ceux qui se lèvent
pour justice et liberté;
Bienheureux sur une terre
où le pain est partagé.

4.
Bienheureux celui qui cherche
le soleil de vérité;
Bienheureux dans la lumière
de Jésus ressuscité.

5.
Bienheureux ceux qui résistent
quand le ciel paraît fermé;
ils proclament l’évangile
par le cri du sang versé.