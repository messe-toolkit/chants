R.
RETOURNE-TOI VERS MOI, SEIGNEUR,
SOUTIENS LA MARCHE DE MES PAS,
DIS-MOI CE TEMPS OU TU M’ESPÈRES,
LE TEMPS DU FESTIN PARTAGÉ.

1.
À ton appel, je suis venu
Suivre ton peuple,
Mais j’ai encore quelques filets
De peine.
Je n’ose tout abandonner.

2.
À ton appel, je suis venu
Suivre ton peuple,
Mais je n’ai plus où reposer
Ma tête.
Mes nuits se troublent d’inconnu.

3.
À ton appel, je suis venu
Suivre ton peuple,
Mais de quel blé nourrir ma faim
De vivre?
Si tu ne m’offres de ton pain.

4.
À ton appel, je suis venu
Suivre ton peuple,
Mais je repense à mes amis
D’errance.
Que dans leurs yeux s’ouvre ton ciel.