Ô SEIGNEUR, ENVOIE TON ESPRIT
QUI RENOUVELLE LA FACE DE LA TERRE !

1.
Bénis le Seigneur, ô mon âme ;
Seigneur mon Dieu, tu es si grand !
Quelle profusion dans tes oeuvres, Seigneur !
La terre s’emplit de tes biens.

2.
Tu reprends leur souffle, ils expirent
et retournent à leur poussière.
Tu envoies ton souffle : ils sont créés ;
tu renouvelles la face de la terre.

3.
Gloire au Seigneur à tout jamais !
Que Dieu se réjouisse en ses oeuvres !
Que mon poème lui soit agréable ;
moi, je me réjouis dans le Seigneur.