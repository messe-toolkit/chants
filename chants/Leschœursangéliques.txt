1.
Voici la Noël ! au fond d'une étable,
Repose endormi le Dieu tout aimable
Dans son pauvre et laid berceau
que l'enfant divin est beau.
La vierge marie contemple ravie son Jésus
Son doux enfant Fils du tout–puissant.
Et là-haut, dans les cieux, retentit le chant joyeux :

Les chœurs angéliques ont chanté : « Noël » !
Mêlons nos cantiques aux accents du ciel :
« Noël » ! « Noël » ! Noël » ! Chantons tous « Noël »
« Noël » ! « Noël » ! Chantons tous « Noël » !
Quel amour extrême ! Notre Dieu lui-même
Se fait l'un de nous, se fait l'un de nous
Il n'est que tendresse aimable faiblesse,
Pour nous sauver tous, pour nous sauver tous

2.
Vêtus de clarté, des anges sans nombre,
Planaient dans les cieux illuminant l'ombre
Ils chantaient : « Gloire au grand Dieu,
paix sur la terre en tout lieu ».
Le divin Messie est né de Marie
Et les bergers sont venus adorer Jésus
Au sauveur tout comme eux, présentons nos humbles vœux.

Les chœurs angéliques ont chanté : « Noël » !
Mêlons nos cantiques aux accents du ciel :
« Noël » ! « Noël » ! Noël » ! Chantons tous « Noël » !
« Noël » ! « Noël » ! Chantons tous « Noël » !
Dans la pauvre étable le Dieu charitable
Vient déjà s'offrir vient déjà s'offrir.
Pour sauver les hommes, c'est Dieu qui se donne,
Éternel amour, éternel amour...

3.
Réjouissons-nous, car jésus lui-même
Reviens parmi nous parce qu'il nous aime,
Un petit morceau de pain,
posé dans nos pauvre mains,
C'est la crèche encore, où le monde adore Jésus-Christ
Dieu avec nous Dieu « Emmanuel ».
Dans la joie tous en chœur, bénissons le Dieu sauveur.