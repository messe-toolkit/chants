Refrain :
Il était un peu poète, bohémien et “va-nu-pieds”,
On l'appelait le prophète, il en riait ;
Sa vie n'était qu'un voyage pour échapper
Au pays qui met en cage la Liberté !

1.
Son accent le trahissait, son teint brun, ses yeux topaze
Sur sa guitare, on lisait tout simplement ces deux phrases :
“Dis-moi où est ton trésor, je te dirai quelle est ta route.”
Et : “La raison du plus fort peut être remise en doute !”

2.
Sa guitare n'arrêtait pas, il chantait sur quelques notes.
Toi, ta vie, elle sert à quoi ? Surtout touche pas à mon pote
De Beyrouth à Managua ! Qui chang'ra l'sens de l'histoire,
Solidarnosc, tu vaincras, tu marches vers la victoire !

3.
On l'a arrêté, je crois, pour délit de sale mine ;
On lui a coupé les doigts et brisé sa mandoline ;
Crucifié, comme hors la loi, pour trente deniers, peut-être ;
Mais je veux être sa voix, c'est mon ami, c'est mon maître.