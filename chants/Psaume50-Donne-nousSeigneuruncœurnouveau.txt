Antienne
DONNE-NOUS, SEIGNEUR, UN COEUR NOUVEAU;
METS EN NOUS, SEIGNEUR, UN ESPRIT NOUVEAU.

1.
Pitié pour moi, mon Dieu, dans ton amour,
selon ta grande miséricorde, efface mon péché.

Répons I
DONNE-NOUS, SEIGNEUR, UN COEUR NOUVEAU;

Lave-moi tout entier de ma faute,
purifie-moi de mon offense.

Répons II
METS EN NOUS, SEIGNEUR, UN ESPRIT NOUVEAU.

2.
Crée en moi un coeur pur, ô mon Dieu,
renouvelle et raffermis au fond de moi mon esprit. R1
Ne me chasse pas loin de ta face,
ne me reprends pas ton esprit saint. R2

3.
Rends-moi la joie d’être sauvé ;
que l’esprit généreux me soutienne. R1
Aux pécheurs j’enseignerai tes chemins,
vers toi reviendront les égarés. R2