Refrain :
Pour bâtir, pour bâtir l’avenir d’une terre plus belle,
Donnons-nous rendez-vous !
Pour cueillir, pour cueillir l’espérance d’une pâque nouvelle,
Donnons-nous rendez-vous !

1.
Donnons-nous rendez-vous pour inventer les fêtes,
Pour inventer nos vies, déjà la danse est prête ;
Tout au long des chemins, répandons la nouvelle,
Nous ferons pour toujours, une terre plus belle.

2.
Donnons-nous rendez-vous, brisons la solitude
Et cassons le confort des vieilles habitudes ;
Tout au long des chemins, répandons la nouvelle,
Nous ferons pour toujours, une terre plus belle.

3.
Donnons-nous rendez-vous, nous prendrons la parole,
Il est venu le temps des rires, des farandoles ;
Tout au long des chemins, répandons la nouvelle,
Nous ferons pour toujours, une terre plus belle.

4.
Donnons-nous rendez-vous, le jour va se lever,
Notre avenir est mûr, il faut le moissonner ;
Tout au long des chemins, répandons la nouvelle,
Nous ferons pour toujours, une terre plus belle.