L’Agneau pascal a été immolé.
En partageant le pain du passage,
Célébrons la grande fête du Seigneur ressuscité !
En partageant le vin du Royaume,
Célébrons la grande fête de l’Église sanctifiée !

VOICI LE PAIN DE DROITURE ET DE VÉRITÉ.
VOICI LE VIN QUI NOUS DONNE LA JOIE DES CIEUX !

1.
J’avancerai jusqu’à l’autel de Dieu,
Vers Dieu qui est toute ma joie.

2.
Seigneur, mon partage et ma coupe,
de toi dépend mon sort.

3.
Non, je ne mourrai pas, je vivrai
pour annoncer les actions du Seigneur.

4.
Je me glorifierai dans le Seigneur,
que les pauvres m’entendent et soient en fête