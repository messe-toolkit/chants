ALLÉLUIA, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA.

1.
Rendez grâce au Seigneur : Il est bon !
Éternel est son amour !
Qu’ils le disent, ceux qui craignent le Seigneur :
Éternel est son amour !

2.
Le bras du Seigneur se lève,
le bras du Seigneur est fort !
Non, je ne mourrai pas, je vivrai
pour annoncer les actions du Seigneur.

3.
La pierre qu’ont rejetée les bâtisseurs
est devenue la pierre d’angle :
c’est là l’oeuvre du Seigneur,
la merveille devant nos yeux.