Temps du Carême

1.
Gloire au Christ,
Parole éternelle du Dieu vivant.
Gloire à toi, Jésus Christ,
Ta Parole nous fait vivre.

2.
Gloire au Christ,
Lumière éternelle du Dieu vivant.
Gloire à toi, Jésus Christ,
Ta Parole nous éclaire.

3.
Gloire au Christ,
Sagesse éternelle du Dieu vivant.
Gloire à toi, Jésus Christ,
Ta Parole nous rassure.