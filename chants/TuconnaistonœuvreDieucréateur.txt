Stance
Tu connais ton oeuvre, Dieu créateur :
Souviens-toi que nous sommes poussière;
Ferme les yeux sur les péchés des hommes,
et ne condamne pas le pauvre qui t'implore :

R.
N'OUBLIE PAS TES MISÉRICORDES, SEIGNEUR, PARDONNE-MOI !

1.
Pitié pour moi, Ô Dieu, pitié pour moi, en toi s'abrite mon âme.

2.
J'appelle vers Dieu, le Très-Haut, le Dieu qui a tout fait pour moi.

Verset
Seigneur, ne nous traite pas selon nos péchés; ne nous punis pas selon nos fautes.