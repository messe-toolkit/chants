SORTEZ, SOYEZ SAINTS COMME LE SEIGNEUR EST SAINT.
ALLEZ, RAYONNEZ SON AMOUR ET SA BONTÉ.

1.
Soyez bénis de Dieu, vous qui avez puisé
à la source de Vie, à son eucharistie.
Allez, oeuvrez, soyez artisans de paix.
Aimés et pardonnés, vous êtes sanctifiés.

2.
Soyez remplis de Dieu, vous qui avez reçu
l’enseignement du Christ, sa Parole de Vie.
Allez, parlez en son Nom et vous verrez :
l’Esprit rappellera ce qu’il a enseigné.

3.
Soyez témoins de joie, le monde en a besoin.
Témoins de charité, respirez sa bonté.
Allez, donnez sans compter et vous verrez :
l’Esprit Saint agira, vous serez fortifiés.

4.
Soyez remplis de foi, le monde en a besoin.
Faites le bien partout là où vous passerez.
Allez, vivez en enfants de lumière,
rejetez les ténèbres et vous serez sauvés.