1.
Il est tombé l’oiseau blessé,
personne pour le relever.
Il est tombé l’oiseau blessé,
personne pour le relever.
Il a pleuré l’oiseau tombé,
il s’est mis à pleurer sans bruit.
Il est tombé l’oiseau blessé,
personne pour le relever.

2.
Mon frère est un oiseau chassé,
tombé, blessé, que j’ai trouvé.
Mon frère est un oiseau chassé,
tombé, blessé, que j’ai trouvé.
Mais son coeur s’est mis à danser
quand je me suis penché vers lui.
Mon frère est un oiseau chassé,
tombé, blessé, que j’ai trouvé.

3.
Il a chanté, l’oiseau tombé,
quand une main l’a caressé.
Il a chanté, l’oiseau tombé,
quand une main l’a caressé.
Il a dansé, l’oiseau trouvé,
et puis s’est envolé sans bruit.
Il a chanté, l’oiseau tombé,
quand une main l’a caressé.