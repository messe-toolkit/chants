« Allez ! De toutes les nations faites des disciples :
baptisez-les au nom du Père, et du Fils, et du Saint-Esprit,
apprenez-leur à observer
tout ce que je vous ai commandé.
Et moi, je suis avec vous
tous les jours jusqu’à la fin du monde. »