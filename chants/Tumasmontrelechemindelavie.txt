Tu m’as montré, Seigneur, le chemin de la vie.

Ou:
Tu nous montres, Seigneur, le chemin de la vie.

Et pour l’Offertoire :
Nous t’apportons, Seigneur, les présents de la vie.