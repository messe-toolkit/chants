R.
Je vous appelle mes amis
C'est moi Jésus qui vous le dit
Je n'ai pas des secrets pour vous
Mon Père aussi est avec nous.

1.
Quand j'ai un ami
Je l'écoute parler
Quand j'ai un ami
Je peux mieux travailler
Quand j'ai un ami
Je m'amuse avec lui
Quand j'ai un ami
Je suis sûr de la vie.

2.
Quand j'ai un ami
J'aime bien le revoir
Quand j'ai un mai
Je reprends vite espoir
Quand j'ai un ami
Je peux rire avec lui
Quand j'ai un ami
Je peux aimer la vie.

3.
Quand j'ai un ami
Je lui dis mes ennuis
Quand j'ai un ami
Je vais l'aider aussi
Quand j'ai un ami
Je partage avec lui
Quand j'ai un ami
Je peux vivre ma vie.

4.
Quand j'ai un ami
Mon cœur est plein de joie
Quand j'ai un ami
Je vois plus loin que moi
Quand j'ai un ami
Je m'entraîne avec lui
Quand j'ai un ami
Je me sens plein de vie.

5.
Jésus notre ami
Tu connais notre vie
Jésus notre ami
Tu partages ton pain
Tu nous donnes ta joie
Jésus notre ami
Tu nous donnes ta vie.