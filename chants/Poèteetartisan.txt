ET J’ENTENDS ENCORE MAMAN ME DIRE:
« RECOMMENCE, POUR TOUT IL FAUT DU TEMPS »
NI CACHÉE, NI FOU, NI CONQUÉRANT
JE VEUX ÊTRE EN TOUT
POÈTE ET ARTISAN

1.
Je l’ai vue penchée sous son toit
Face à la lumière
Un regard, puis des heures entières
Font naître le ciel
La vase et le sel
Sous ses doigts
J’aime le parfum d’eau et de bois
De son savoir-faire
Pour bien faire, il lui faudra
Des jours ou des mois,
Une vie entière, peut-être
Je crois en sa voie
Je vois dans ses mains qui façonnent
Sa façon d’être là

2.
Je les imagine en plein vent
Courbés sur la pierre
Quelle folie ! Et tant de poussière !
Un ange surgit
Les ailes épanouies
Baigné de lumière
J’aime voir sur ces linteaux blancs
Tout leur savoir-faire
Pour bien faire, il leur faudra
Des jours ou des mois,
Une vie entière, un siècle
Je cherche leur voie
Dans leurs gestes précis résonne
Le royaume ici-bas

3.
Chaque jour sur un bout de bois
Je cultive ma terre
Pour bien faire, il me faudra
Des jours ou des mois,
Une vie entière, qu’importe !
Le Ciel me rend libre
De travailler au corps à corps
La matière sous mes doigts
J’ai choisi le chemin d’un souffleur de verre
Le travail et l’instinct d’un tailleur de pierres