1.
Parole du Seigneur de l’univers :
Je vous rassemblerai de tous pays,
Je vous ramènerai dans votre terre.

R.
Dieu vivant, nous sommes ton peuple,
Tu nous rassembles et nous conduis (bis) !

2.
Parole du Seigneur de l’univers :
Je répandrai sur vous une eau très pure,
Je vous purifierai de vos souillures.

R.
Dieu vivant, nous sommes ton peuple,
Tu nous baptises dans l’Esprit (bis).

3.
Parole du Seigneur de l’univers :
Je vous ferai le don d’un coeur nouveau,
Je vous insufflerai l’esprit nouveau.

R.
Dieu vivant, nous sommes ton peuple,
Tu renouvelles notre coeur (bis).

4.
Parole du Seigneur de l’univers :
Je vous enlèverai le coeur de pierre
Et je vous donnerai un coeur de chair.

R.
Dieu vivant, nous sommes ton peuple,
Tu transfigures notre vie (bis).

5.
Parole du Seigneur de l’univers :
Je vous ferai marcher selon mes lois
Et vous serez fidèles à mes préceptes.

R.
Dieu vivant, nous sommes ton peuple,
Tu nous apprends ta Loi d’amour (bis).