R.
DONNE-NOUS AUJOURD’HUI
NOTRE PAIN DE CE JOUR,
DONNE-NOUS AUJOURD’HUI
DE VIVRE EN TON AMOUR.

1.
Notre Père, Tu nourris les oiseaux,
Tu habilles les fleurs,
Notre Père, le soleil comme l’eau,
Notre souffle et nos coeurs,
Nous tenons tout de Toi !

2.
Notre Père, Tu connais notre coeur
Et Tu vois notre vie,
Notre Père, la tristesse et la peur
Sont le prix de l’oubli,
Nous crions jusqu’à Toi !

3.
Notre Père, Tu entends notre cri,
Tu comprends notre faim,
Notre Père, c’est ton Fils Jésus-Christ
Qui devient notre pain,
Notre vin, notre joie !

4.
Notre Père, fais de nous ton levain,
Donne-nous ton Esprit,
Notre Père, en offrant notre pain,
En offrant notre vie
Nous serons tes enfants !