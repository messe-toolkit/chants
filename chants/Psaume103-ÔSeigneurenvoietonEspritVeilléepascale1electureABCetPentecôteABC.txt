Ô SEIGNEUR, ENVOIE TON ESPRIT
QUI RENOUVELLE LA FACE DE LA TERRE !

Veillée pascale, après la 1re lecture - A, B, C

1.
Bénis le Seigneur, ô mon âme ;
Seigneur mon Dieu, tu es si grand !
Revêtu de magnificence,
tu as pour manteau la lumière !

2.
Tu as donné son assise à la terre :
qu’elle reste inébranlable au cours des temps.
Tu l’as vêtue de l’abîme des mers :
les eaux couvraient même les montagnes ;

3.
Dans les ravins tu fais jaillir des sources
et l’eau chemine aux creux des montagnes ;
les oiseaux séjournent près d’elle :
dans le feuillage on entend leurs cris.

4.
De tes demeures tu abreuves les montagnes,
et la terre se rassasie du fruit de tes oeuvres ;
tu fais pousser les prairies pour les troupeaux,
et les champs pour l’homme qui travaille.

5.
Quelle profusion dans tes oeuvres, Seigneur !
Tout cela, ta sagesse l’a fait ;
la terre s’emplit de tes biens.
Bénis le Seigneur, ô mon âme !

Pentecôte (Messe de la veille au soir) - A, B, C

1.
Bénis le Seigneur, ô mon âme ;
Seigneur mon Dieu, tu es si grand !
Revêtu de magnificence,
Tu as pour manteau la lumière !

2.
Quelle profusion dans tes oeuvres, Seigneur !
Tout cela, ta sagesse l’a fait ;
la terre s’emplit de tes biens.
Bénis le Seigneur, ô mon âme !

3.
Tous, ils comptent sur toi
pour recevoir leur nourriture au temps voulu.
Tu donnes : eux, ils ramassent ;
tu ouvres la main : ils sont comblés.

4.
Tu reprends leur souffle, ils expirent
et retournent à leur poussière.
Tu envoies ton souffle : ils sont créés ;
tu renouvelles la face de la terre.

Pentecôte (Messe du jour) - A, B, C

1.
Bénis le Seigneur, ô mon âme ;
Seigneur mon Dieu, tu es si grand !
Quelle profusion dans tes oeuvres, Seigneur !
La terre s’emplit de tes biens.

2.
Tu reprends leur souffle, ils expirent
et retournent à leur poussière.
Tu envoies ton souffle : ils sont créés ;
tu renouvelles la face de la terre.

3.
Gloire au Seigneur à tout jamais !
Que Dieu se réjouisse en ses oeuvres !
Que mon poème lui soit agréable ;
moi, je me réjouis dans le Seigneur.