R.
SAUVEUR DE TOUTE VIE,
RELÈVE TON AMI!
TOI SEUL ES NOTRE ESPOIR,
NOUS CROYONS EN TOI.

1.
Pour le salut de ton ami
Tu es venu dans Béthanie
Aux nuits de mort.
Dans nos maisons désemparées,
Entends nos cris désespérés :
Où donc est le Dieu fort ?

2.
Auprès de Marthe et de Marie,
Dans la douleur la foi jaillit
Car tu es là !
Tu es au monde l’Envoyé
Par qui la mort est condamnée:
« Qui croit en moi vivra! »

3.
C’est dans l’Esprit que nous disons :
« Tu es Jésus résurrection,
Le Fils de Dieu. »
Mais toi, le Maître et le Seigneur,
Tu es aussi cet homme en pleurs,
Tendresse dans les yeux.

4.
Un mot de toi, la nuit s’éteint,
Voici l’aurore qui revient,
Lazare vit.
À ton appel nous surgirons
Et tous nos liens se briseront :
Debout ! La Pâque luit.