R.
Sans amour, sans amour,
Je ne suis qu’une cymbale !
Un tambour, un tambour,
Qui éclate et qui se tait !

1.
J’aurais beau savoir les langues de la terre,
Sans amour cela n’est rien !
J’aurais beau tenir la science et les mystères,
Sans amour cela n’est rien !

2.
J’aurais beau guérir et faire des miracles,
Sans amour, cela n’est rien !
J’aurais beau prédire et lire les oracles,
Sans amour, cela n’est rien !

3.
J’aurais beau parler en maître de sagesse,
Sans amour, cela n’est rien !
J’aurais beau donner aux pauvres ma richesse,
Sans amour, cela n’est rien !

4.
J’aurais beau livrer ma chair au creux des flammes
Sans amour, cela n’est rien !
J’aurais beau pouvoir déplacer les montagnes,
Sans amour, cela n’est rien !

5.
J’aurais beau m’offrir tout entier pour mes frères
Sans amour, cela n’est rien !
J’aurais beau passer mes veilles en prières,
Sans amour, cela n’est rien !