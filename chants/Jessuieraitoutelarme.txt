R.
J´essuierai toute larme de leurs yeux,
Je les conduirai aux sources d´eau vive.
J´essuierai toute larme de leurs yeux,
Je les conduirai aux sources d´eau vive.

1.
Ils n´auront plus ni faim ni soif,
Ceux qui ont lavé leur manteau,
Ils n´auront plus ni faim ni soif,
Lavés dans le sang de l´Agneau.

2.
Ils ne craindront plus le soleil,
Ceux qui habitent la maison.
Ils ne craindront plus le soleil,
Ceux qui habitent le pardon.

3.
Ils chanteront le jour, la nuit,
Ceux qui ont place devant Dieu.
Ils chanteront le jour, la nuit,
Ceux qui le servent dans les cieux.