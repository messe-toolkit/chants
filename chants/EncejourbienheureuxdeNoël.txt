En ce jour bienheureux de Noël,
louange à Toi, notre Sauveur, né de la Vierge Marie,
Roi des Anges, Roi des hommes !
Gloire au Père, Gloire au Fils, Gloire à l’Esprit-Saint !
Amen!