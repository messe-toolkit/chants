R.
SUR LES CAILLOUX SUR LES CHEMINS
TA PAROLE TOMBE
DANS NOS SILLONS ELLE EST LE GRAIN
QUI NOURRIT LE MONDE

1.
Tu dis : "Relève-toi"
Au petit, au boiteux
Et moi qui crois tenir debout,
Je n'y comprends plus rien.

2.
Tu dis : "Ecoute Dieu
Qui chante au fond de toi
Et pas seulement dans les cieux".
Je n'y comprends plus rien

3.
Tu dis : "Heureux les pauvres,
Un Royaume est à eux".
Et moi, ma foi, qui vis très bien,
Je n'y comprends plus rien.

4.
Si je ferme ma vie
Au souffle de ta voix,
Même en disant : "Seigneur, Seigneur",
Je n'y comprends plus rien.