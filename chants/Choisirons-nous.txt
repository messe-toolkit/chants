R.
Il est avec nous,
Celui qui nous appelle,
Et nous marchons vers notre Pâque.

1.
Choisirons-nous de tout laisser
Pour revenir au lieu du coeur ?
Oserons-nous l’heureuse aventure
Sur les pas du Seigneur ?

2.
Christ est la porte et le chemin,
Il nous libère du péché,
Nous fait renaître des eaux profondes
Où tout homme est sauvé.

3.
Que les veilleurs dans la maison
Soient les témoins d’un Dieu présent,
Et que leurs voix en tout lieu proclament
L’Evangile à ce temps.

4.
Nul n’est trop loin pour s’approcher,
Mais qui viendra s’il n’est rejoint
Dans ses épreuves, dans sa recherche,
Par l’ami, son prochain ?

5.
La joie s’étend dès maintenant
Dans la demeure du Très-Haut
Pour ceux qui s’ouvrent à la lumière
Et au souffle nouveau.

6.
Allons au coeur de notre Dieu,
Laissons l’amour nous purifier,
En nous jaillit le chant de louange
Des enfants pardonnés.