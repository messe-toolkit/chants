STANCE
Voici le temps, éveille-toi,
le Seigneur de tous les temps vient chez toi !

R.
VOICI LE TEMPS,
ÉVEILLE-TOI !

1.
Le temps où Dieu se choisit une Mère.
Le temps où Dieu fait germer notre terre.

2.
Le temps où Dieu fait luire une Lumière.
Le temps où Dieu fait-homme devient frère.

3.
Le temps où Dieu habite notre monde.
Le temps où Dieu se donne en Vie profonde

4.
Le temps où Dieu se fait Espoir de l’homme.
Le temps où Dieu devient la Gloire de l’homme

5.
Le temps où Dieu se dit "Bonne Nouvelle".
Le temps où Dieu est Alliance Éternelle.