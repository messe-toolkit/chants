14e dimanche du temps ordinaire – A
31e dimanche du temps ordinaire – C

MON DIEU, MON ROI,
JE BÉNIRAI TON NOM TOUJOURS ET À JAMAIS !

1.
Je t’exalterai, mon Dieu, mon Roi,
je bénirai ton nom toujours et à jamais !
Chaque jour je te bénirai,
je louerai ton nom toujours et à jamais.

2.
Le Seigneur est tendresse et pitié,
lent à la colère et plein d’amour ;
la bonté du Seigneur est pour tous,
sa tendresse, pour toutes ses oeuvres.

3.
Que tes oeuvres, Seigneur, te rendent grâce
et que tes fidèles te bénissent !
Ils diront la gloire de ton règne,
ils parleront de tes exploits.

4.
Le Seigneur est vrai en tout ce qu’il dit,
fidèle en tout ce qu’il fait.
Le Seigneur soutient tous ceux qui tombent,
il redresse tous les accablés.

5e dimanche de Pâques – C

MON DIEU, MON ROI,
JE BÉNIRAI TON NOM TOUJOURS ET À JAMAIS !

1.
Le Seigneur est tendresse et pitié,
lent à la colère et plein d’amour ;
la bonté du Seigneur est pour tous,
sa tendresse, pour toutes ses oeuvres.

2.
Que tes oeuvres, Seigneur, te rendent grâce
et que tes fidèles te bénissent !
Ils diront la gloire de ton règne,
ils parleront de tes exploits.

3.
Ils annonceront aux hommes tes exploits,
la gloire et l’éclat de ton règne :
ton règne, un règne éternel,
ton empire, pour les âges des âges.