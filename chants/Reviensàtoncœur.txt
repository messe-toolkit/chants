REVIENS À TON COEUR
ET DE TON COEUR À DIEU
CAR LE CHEMIN N’EST PAS LONG
DE TON COEUR À DIEU.

1.
Pour trouver Dieu
Approcher sa lumière
Prends le chemin du coeur
Comme un enfant
Tout simplement.

2.
Pour goûter Dieu
Découvrir sa Parole
Prends le chemin du coeur
Comme un enfant
Tout simplement.

3.
Pour aimer Dieu
Recevoir sa tendresse
Prends le chemin du coeur
Comme un enfant
Tout simplement.

4.
Pour vivre en Dieu
Accueillir sa présence
Prends le chemin du coeur
Comme un enfant
Tout simplement.