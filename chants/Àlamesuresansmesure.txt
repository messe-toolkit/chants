A la mesure sans mesure
De ton immensité
Tu nous manques, Seigneur.
Dans le tréfonds de notre coeur
Ta place reste marquée
Comme un grand vide, une blessure.

A l'infini de ta présence
Le monde est allusion,
Car tes mains l'ont formé.
Mais il gémit, en exilé,
Et crie sa désolation
De n'éprouver que ton silence.

Dans le tourment de ton absence,
C'est toi déjà, Seigneur,
Qui nous a rencontrés.
Tu n'es jamais un étranger,
Mais l'hôte plus intérieur
Qui se révèle en transparence.

Cachés au creux de ton mystère,
Nous te reconnaissons
Sans jamais te saisir.
Le pauvre seul peut t'accueillir,
D'un coeur brûlé d'attention,
Les yeux tournés vers ta lumière.