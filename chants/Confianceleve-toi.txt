R.
CONFIANCE, LÈVE-TOI, IL T’APPELLE !
JÉSUS MARCHE SUR TON CHEMIN.
OSE CRIER : « JÉSUS SAUVEUR! »
CONFIANCE, LÈVE-TOI, IL T’APPELLE !

1.
Il nous confie cette terre
Où la paix s’espère à jamais
Par l’audace de l’Esprit.
Confiance, lève-toi !
Et crions par tous nos actes :
« Jésus sauveur aujourd’hui ! »

2.
Il nous confie son pardon
Capable de détruire la haine
Par le combat de l’Esprit.
Confiance, lève-toi !
Et crions par nos douceurs :
« Jésus sauveur aujourd’hui ! »

3.
Il nous confie son amour,
Sa puissance engloutit la mort
Par la force de l’Esprit.
Confiance, lève-toi !
Et crions par nos jeunesses :
« Jésus sauveur aujourd’hui ! »

4.
Il nous confie son Église
Peuple debout pour l’avenir
Par la flamme de l’Esprit.
Confiance, lève-toi !
Et crions par notre foi :
« Jésus sauveur aujourd’hui ! »

5.
Il nous confie sa beauté
Qui transfigure et nous éclaire
Par la lumière de l’Esprit.
Confiance, lève-toi !
Et crions par nos regards :
« Jésus sauveur aujourd’hui ! »