RÉJOUIS-TOI, DANSE DE JOIE !

1.À jamais, je chanterai la victoire
De l'amour qui nous rend libres pour toujours !
Mon cœur dira les merveilles)
Que le créateur a fait pour moi.) Bis

RÉJOUIS-TOI, DANSE DE JOIE
TON SAUVEUR VIENT CHEZ TOI !
RÉJOUIS-TOI, DANSE DE JOIE
IL VIENT POUR TOI !

2.
Pleins de joie, nous accueillons ta tendresse
Toi qui es le Dieu fidèle de ton peuple !
Tu ne retiens pas nos fautes)
Ta miséricorde nous guérit.) Bis

3.
Mon Dieu, mon libérateur je te chante
Ma joie d'être ton enfant bien-aimé.
Tu m'as donné l'héritage)
Et je veillerai sur tous tes biens.) Bis