1.
Seigneur, je viens vers toi,
déjà, Tu me connais. (x2)
Toi qui sondes les cœurs,
toi qui sondes les reins,
Seigneur accueille-moi,
prends ma vie dans tes mains.

2.
Seigneur, Tu es ma joie,
déjà, Tu m'as guéri. (x2)
Esprit de Vérité,
Esprit d'Humilité,
Pour toujours apprends-moi,
le chemin de la Vie.