1.
Toi, tu nous considères
Chacun comme ton préféré.
Tu nous aimes plus qu’un père,
Et même plus qu’une mère.

NOUS SOMMES TES ENFANTS BIEN-AIMÉS ! (bis)
APPRENDS-NOUS LA JOIE D’AIMER.

2.
Toi, tu nous considères
Chacun comme ton préféré.
Tu nous aimes plus qu’un frère,
Et même plus qu’une épouse.

NOUS SOMMES TES ENFANTS BIEN-AIMÉS ! (bis)
APPRENDS-NOUS À PARDONNER.

3.
Toi, tu nous considères
Chacun comme ton préféré.
Tu nous aimes à la folie,
Et même plus que la vie.

NOUS SOMMES TES ENFANTS BIEN-AIMÉS ! (bis)
APPRENDS-NOUS À TOUT DONNER.