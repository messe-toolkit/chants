Si quelqu'un veut me servir,
qu'il me suive ;
et là où je suis,
sera aussi mon serviteur.