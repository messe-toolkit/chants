1.
Tous les hommes, je crois, recherchent le bonheur
Mais loin de toi, souvent…
Avons-nous oublié, pardonne-nous Seigneur,
Qu’en nous tu es présent.

NOËL, NOËL, QUE REVIENNE NOËL.
NOËL, NOËL, L’EMMANUEL.

2.
Tous les hommes, je crois, recherchent la lumière
Mais loin de toi, souvent…
Ton étoile, pourtant, est si belle et si claire,
Elle brille à chaque instant.

3.
Tous les hommes, je crois, recherchent un chemin
Mais loin de toi, souvent…
Trouverons-nous celui qui conduit au festin
Où toi tu nous attends ?

4.
Tous les hommes, je crois, recherchent la vraie vie
Mais loin de toi, souvent…
Puissions-nous chaque jour goûter l’eau de ton puits
Qui nous fait plus vivants.