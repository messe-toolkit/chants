R.
Fils premier-né, Jésus, Seigneur des vivants
Fils premier-né, Jésus le Seigneur des temps
Christ ressuscité, louange et gloire à toi, alléluia !

1.
Christ-avec-nous, vivant tu t'es levé.
Quand Pâques luit, la mort n'a plus d'emprise
Gloire au Dieu puissant par qui tes liens sont brisés à tout jamais !

2.
Christ-avec-nous, ton feu nous a brûlés.
Qu'il se répande au cœur de notre monde
Gloire au Dieu de joie qui nous envoie t'annoncer Jésus sauveur.

3.
Christ-avec-nous, tu chasses notre nuit.
Voici le jour plus fort que les ténèbres !
Gloire au créateur qui reste à l'œuvre aujourd'hui ! Nous revivons !

4.
Christ-avec-nous, ta main nous a saisis.
Où donc es-tu quand nul ne voit ta face ?
Gloire au Dieu très saint qui fait souffler son Esprit, et nous croyons !