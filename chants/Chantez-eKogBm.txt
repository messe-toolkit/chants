R.
Chantez ce que Dieu a fait
Chantez la beauté de sa grâce
Dansez, écoutez son coeur
Son amour est la source de vie
Chantez ce que Dieu a fait
Chantez honorez sa présence
Dansez écoutez son coeur
Le Seigneur est la souce de vie
Le Seigneur est la souce de vie

1.
Saint, saint, saint est le Seigneur
Son armée remplit les cieux
Tous les anges à l’unisson
Louent sa gloire et son Saint Nom.

2.
Son Esprit répand sur nous
Les torrents de ses bontés
L’univers à son éclat
Retentit en cris de joie.