JE T’AIME, SEIGNEUR, MA FORCE.

1.
Je t’aime, Seigneur, ma force :
Seigneur, mon roc, ma forteresse,
Dieu mon libérateur, le rocher qui m’abrite,
mon bouclier, mon fort, mon arme de victoire !

2.
Louange à Dieu !
Quand je fais appel au Seigneur,
je suis sauvé de tous mes ennemis.

3.
Vive le Seigneur ! Béni soit mon Rocher !
Qu’il triomphe, le Dieu de ma victoire !
Il donne à son roi de grandes victoires,
il se montre fidèle à son messie.