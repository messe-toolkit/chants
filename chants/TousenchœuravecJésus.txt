TOUS EN CHOEUR
TOUS EN CHOEUR
C’EST BIEN ÇA LE BONHEUR
TOI JÉSUS, TU NOUS CONDUIS
VIVONS COMME DES AMIS

1.
Je voudrais bien, un jour, tu sais
Te dire tous mes secrets
J'ai plein d'idées pour te parler
Je voudrais me confier
Alors j’écris un chant nouveau
La mélodie, les mots
Aujourd'hui avec les copains
On reprend ce refrain

2.
Dès le réveil, au saut du lit
Je pense à mes amis
Sans eux les jours seraient bien longs
Bien tristes les saisons
Heureusement ils sont tous là
Cela me met en joie
Aujourd'hui avec les copains
On reprend ce refrain

3.
On se retrouve à la récré
C’est trop bien de jouer
Et partager toutes nos idées
C’est le temps du caté
Et là, on chante, on danse, on prie
C’est le temps de la vie
Aujourd'hui avec les copains
On reprend ce refrain

4.
Tu nous parles en chemin, en vrai
Tu nous dis tes secrets
En nous montrant tous les trésors
De nos vies et encore
Lorsque nos pas sont bien trop lourds
Tu viens semer l’Amour
Aujourd'hui avec les copains
On reprend ce refrain