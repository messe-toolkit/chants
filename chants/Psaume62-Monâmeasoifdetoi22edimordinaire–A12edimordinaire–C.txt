MON ÂME A SOIF DE TOI,
SEIGNEUR, MON DIEU !

1.
Dieu, tu es mon Dieu, je te cherche dès l’aube :
mon âme a soif de toi ;
après toi languit ma chair,
terre aride, altérée, sans eau.

2.
Je t’ai contemplé au sanctuaire,
j’ai vu ta force et ta gloire.
Ton amour vaut mieux que la vie :
tu seras la louange de mes lèvres !

3.
Toute ma vie je vais te bénir,
lever les mains en invoquant ton nom.
Comme par un festin je serai rassasié ;
la joie sur les lèvres, je dirai ta louange.

4.
Oui, tu es venu à mon secours :
je crie de joie à l’ombre de tes ailes.
Mon âme s’attache à toi,
ta main droite me soutient.