R.
Prends pitié de nous :
Conduis-nous vers le Père !
Prends pitié de nous :
Guide-nous dans la paix !

1.
Agneau glorieux,
Agneau que nous avions rejeté,
Agneau devenu notre Berger.

2.
Agneau glorieux,
Agneau que nous avions immolé,
Agneau devenu notre Berger.

3.
Agneau glorieux,
Agneau que nous avions crucifié,
Agneau devenu notre Berger.

4.
Agneau glorieux,
Agneau que nous avions transpercé,
Agneau devenu notre Berger.