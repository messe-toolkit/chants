Seigneur, qui prends nos péchés,
Agneau de Dieu sur la terre,
Écoute et prends pitié,
Entends le cri de nos prières.
Tu nous as dit : « Aimez-vous comme des Frères
Et il y aura la paix sur la terre. »
Seigneur, qui donnes la paix,
Agneau de Dieu sur la terre,
Écoute et prends pitié,
Donnons la paix entre frères.