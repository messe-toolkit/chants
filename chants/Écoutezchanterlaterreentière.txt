R.
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA.
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA.
ÉCOUTEZ CHANTER LA TERRE ENTIÈRE
TERRE ENTIÈRE ÉCOUTEZ-NOUS CHANTER
LA LIBERTÉ N’EST PLUS PRISONNIÈRE
IL EST VIVANT, VIVANT, RESSUSCITÉ !

1.
Le vent a soufflé sur les cendres
Le feu s’est mis à chanter
Notre hiver vient d’aller se pendre
Et l’espoir, je l’ai vu briller
Ressuscité ! Il est ressuscité !

2.
Le glas condamne l’ancien monde
Le rideau s’est déchiré
La vie a jailli de la tombe
Notre Seigneur est jardinier
Ressuscité ! Il est ressuscité !

3.
Les pauvres ont droit aux premières places
Les boiteux viendront danser
Le mur du ciel n’a plus d’impasse
Toutes nos chaînes sont tombées
Ressuscité ! Il est ressuscité !