« Dans toute ville où vous entrerez
Et où vous serez accueillis,
Mangez ce qui vous est présenté.
Guérissez les malades qui s’y trouvent et dites-leur :
« Le règne de Dieu s’est approché de vous. »