R.
PAIN DE TOUS LES JOURS,
VIN DE NOS LABEURS,
FRUIT DE LA TERRE,
CRI DE TOUS LES HOMMES.
PAIN DE TOUS LES JOURS,
VIN DE NOS LABEURS,
DON DE NOTRE DIEU,
VIE EN ABONDANCE!

1.
Pain de tous les grains de nos pays,
Pétri de joies et de larmes…
Pain de tous les grains de nos chemins,
Cuit au four de nos différences…
Pain de tous les grains de nos vies,
Corps du Christ et pain de l’homme,
Corps du Christ qui fait l’Église.

2.
Vin de tous les grains de nos pays,
Mêlé de joies et de larmes…
Vin de tous les grains de nos chemins,
Au pressoir de nos différences…
Vin de tous les fruits de nos vies,
Sang du Christ et vin de l’homme,
Sang du Christ qui fait l’Église.