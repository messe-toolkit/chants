R.
Fais lever le soleil, notre terre attend l´aurore ;
Fais lever le soleil, notre vie promet d´éclore !

1.
Prophète du Seigneur, lève-toi parmi tes frères !
Debout comme un veilleur, tu annonces la lumière.
Ton étoile dans la nuit est témoin du jour qui luit.

2.
Prophète de l´amour, tu révèles sa présence.
Dieu parle sans discours, guide-nous vers son silence.
Par l´Esprit du Dieu vivant, ton amour sera ferment.

3.
Prophète de l´espoir, va répandre la nouvelle :
Jésus défie la mort, sa jeunesse est éternelle.
Dans un monde à découvrir, sois le chant de l´avenir.