R.
ELEVONS NOTRE COEUR POUR LE TOURNER VERS LE SEIGNEUR.
ELEVONS NOTRE COEUR POUR LE TOURNER VERS LE SEIGNEUR.

1.
Au milieu des blés
Que le vent fait frissonner
Au milieu des bleuets
Quand le soleil va se lever
Mon coeur aussi est appelé

2.
Au milieu des jeux
Quand on peut rire et chanter
Au milieu de l’école
Quand maman vient pour me chercher
Mon coeur aussi est appelé

3.
Au milieu des rues
Quand on revient de travailler
Au milieu des maisons
Quand c’est la fin de la journée
Mon coeur aussi est appelé

4.
Au milieu des soirs
Quand vient l’heure de se coucher
Au milieu des câlins
Quand papa vient pour me border
Mon coeur aussi vient pour prier