ALLONS TOUS ENSEMBLE VERS LA MONTAGNE DE DIEU
LEVONS LES YEUX VERS CELUI QUI NOUS RASSEMBLE
ET SUR LA ROUTE NOUS ACCOMPAGNE
ALLONS TOUS ENSEMBLE VERS LA MONTAGNE DE DIEU
LEVONS LES YEUX VERS CELUI QUI NOUS RASSEMBLE
GLOIRE ET LOUANGE À NOTRE DIEU !

1.
Partons ensemble à la rencontre
Du créateur de l’univers (bis)
Voici la montagne où se montre
Le Dieu d’amour et de lumière
Voici la montagne où se montre
Toute la tendresse du Père
Allons, allons sur la montagne !

2.
Ni dans un tremblement de terre
Ni le tonnerre, ni le feu (bis)
C’est dans une brise légère
C’est dans un souffle silencieux
C’est dans une brise légère
Que se révèle notre Dieu
Allons, allons sur la montagne !

3.
Allons là-haut planter nos tentes
Allons veiller près de Jésus (bis)
Dans sa splendeur éblouissante
Que le vieux monde n’a pas vue
Dans sa splendeur éblouissante
Nous découvrons notre salut
Allons, allons sur la montagne !

4.
L’heure est venue de le prier
Dans notre montagne intérieure (bis)
En esprit et en vérité
Écouter la voix du Seigneur
En esprit et en vérité
Aimer Dieu de tout notre coeur
Allons, allons sur la montagne !