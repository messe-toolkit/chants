BON PASTEUR DES BREBIS DE TON ÉGLISE,
SEUL BERGER DES HUMAINS DU MONDE ENTIER,
TU NOUS MÈNES À LA SOURCE DES EAUX VIVES
VERS LE CIEL OÙ TON DIEU FAIT REPOSER.

1.
Tu nous conduis par un chemin de justice,
La voie que tu as prise en ton abaissement,
Chemin qui fait honneur à ta mission divine,
Nous ramener tous auprès du Dieu Vivant.

2.
Si je traverse des ravins de ténèbres,
La nuit que tu connus sur ton gibet dressé,
Vraiment je ne crains rien car toi tu es fidèle
A briser la mort et nous ressusciter.

3.
Vibrante joie car tu prépares une table,
Ma coupe est débordante au jour de ton festin ;
Répands sur mon visage un grand parfum durable,
Signe d’un amour qui mène au plus lointain.

4.
Bonheur et grâce m’accompagnent sans cesse,
Tous les jours de ma vie j’habite en ta maison ;
Et ta maison, Seigneur, me chante ta lumière,
Toi Jésus Pasteur, tu mènes au Dieu très bon.

5.
Tu es la porte pour l’agneau plein de grâce,
Par toi chaque brebis est libre de sortir ;
Ils sont connus de toi dans tous les pâturages,
Tu penses à leurs noms, tu es leur avenir.