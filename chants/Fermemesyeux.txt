1.
Ferme mes yeux pour revoir tes merveilles
en ce moment que le jour fuit !
Allume dans la nuit
une clarté nouvelle !

2.
Que le silence alentour me console
de la faiblesse de ma foi,
puisque j’écoute en moi
résonner ta parole !

3.
Jusqu’à demain, si se lève l’aurore,
je t’abandonne mon esprit !
Ta grâce me suffit,
c’est elle que j’implore.