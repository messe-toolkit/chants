Quand des frères s’unissent
pour chanter sa gloire,
Dieu les bénit.
Tous vous reconnaîtront
pour mes disciples
si vous vous aimez les uns les autres.
En toi, Seigneur, la force de tes amis,
en toi leur appui.
Dans le silence de la prière
ils parlaient à Dieu
et Dieu leur répondait.