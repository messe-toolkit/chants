R.
Dieu a tant aimé le monde qu’il nous a donné son fils, alléluia,
celui qui demeure en Lui porte beaucoup de fruits. alléluia.

1.
Moi je suis la vigne,
mon Père le vigneron
tout sarment qui ne porte pas,
qui ne porte pas de fruits,
mon Père l’enlève,
il est jeté dehors, et se dessèche.

2.
Moi, moi je vous le dis :
tout sarment doit porter fruit
celui qui se refuse
se refuse à porter fruit,
mon Père l’arrache,
il est jeté au feu où il brûlera.

3.
Moi, moi je vous le dis :
écoutez la vérité
tout sarment qui commence
qui commence à porter fruit,
mon Père l’émonde,
alors son fruit abonde, il est sa joie.

4.
Le sarment ne peut porter fruit
par lui-même
il lui faut demeurer sur la vigne,
vous, de même
si vous ne demeurez en moi,
vous ne pouvez porter fruit.

5.
Moi je suis la vigne,
vous êtes les sarments
celui qui demeure en moi,
moi je demeure en lui,
mon Père le connaît
il donnera du fruit sève vivante.

6.
Ce qui fait la joie du Père et sa gloire
C’est que toutes vos vies portent leur fruit
demandez-lui ce que vous voudrez,
vous l’aurez
et vous serez mes amis.

7.
Aimons-nous les uns les autres c’est la vie
Ils sont enfants de Dieu tous ceux qui aiment
Dieu est amour, il nous a donné son Fils
et nous vivons de sa vie.