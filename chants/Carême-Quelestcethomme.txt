1.
Quel est cet homme qui s'avance
Parmi les enfants du malheur ?
C'est Dieu qui vient pour rendre cœur
À ceux qu'écrasait son silence.
Frères, si nous avons cherché
La perle et le trésor caché
Aux rivages de cette terre,
Perdons aujourd'hui notre temps :
Dieu nous accueille sans argent
À sa table toujours offerte.

2.
Il a dressé l'itinéraire
Et sa croix proscrit le détour
Quand manifeste au carrefour
Le chœur des mendiants de la terre.
Frères, si nous avons pressé
Le pas, marchant d'un cœur léger
Vers la table où Dieu nous invite
Ne nous leurrons pas pour autant :
Dieu nous accueille repentants
Et sa gloire attend nos mains vides.

3.
Offrant son corps, il nous entraîne
Au festin connu de lui seul :
Sa mort nous tisse le linceul
Où tailler leur nappe à nos cènes.
Frères, si nous avons gardé
L'espoir que notre humanité
Cicatrise enfin ses blessures,
C'est que Dieu lui-même a pétri
Sur le Calvaire de son Fils
Un froment versé sans mesure.

4.
Nous irons voir fleurir le sable
Et jaillir la source au désert :
La croix se change en arbre vert
Dont le fruit garnit notre table.