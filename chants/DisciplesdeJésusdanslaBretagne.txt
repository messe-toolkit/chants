Stance
Disciples de Jésus dans la Bretagne,
Pasteurs pour les brebis et les agneaux,
Vous êtes des témoins à qui Dieu parle,
Ô vous, saints Melaine, Samson et Malo ;
Évêques du Seigneur et vraies lumières,
Ô vous, Saints Melaine, Samson et Malo !

R.
GLOIRE AU DIEU VIVANT QUI VOUS A AIMÉS !
GLOIRE À JÉSUS CHRIST QUI VOUS A CONSACRÉS,
GLOIRE À L’ESPRIT SAINT QUI FAIT VIVRE
EN SA LUMIÈRE !

1.
Lumière d’un appel qui met en marche :
Pour Dieu vous quittez tout avec ferveur.
Partir vers d’autres terres vous engage
À voir les assoiffés de vrai bonheur.
Les Celtes et les Bretons sont à l’écoute,
Ils vont au Dieu d’amour qui met en route.

2.
Lumière d’une vie dans la prière :
Vos coeurs sont embrasés d’un feu nouveau.
Par vous sont édifiés des monastères
Où nuit et jour on chante le Très-Haut.
En chaque lieu de source et de fontaine
Vous bénissez les eaux qui font renaître.

3.
Lumière avec les mots de l’Évangile :
L’annonce du salut vous mène au loin.
Partout dans les campagnes et dans les villes
Vous êtes messagers de Dieu qui vient.
L’Eucharistie rassemble un peuple immense,
Que vivent les fidèles en abondance !

4.
Lumière aux temps marqués par la misère :
Sans cesse vous plaidez pour les petits.
La force des puissants ne vous arrête,
Justice et vérité sont vos défis.
Parfois vous le payez par un exode,
La croix est un sommet dans le Royaume.

5.
Lumière avec vos soins qui font revivre :
Les guérisons fleurissent où vous passez.
L’aveugle voit le jour et veut vous suivre,
Le sourd et le boiteux se sont dressés.
Priez pour nous pécheurs en mal de vivre
Et délivrez tout homme à la dérive.

6.
Lumière d’une Église ouverte au large :
Que monte à l’horizon un flot de vie !
Vos voix dans les conciles ont des audaces
Pour réveiller vos frères endormis !
Le sel de vos paroles est une grâce
Pour les croyants qui marchent vers la Pâque.

7.
Lumière au clair-obscur de l’espérance :
Déjà vos lendemains sont pleins de joie.
Heureux de dire un ciel qui vous enchante,
Vous parlez avec foi de l’au-delà.
Votre moisson est belle aux yeux du Père.
Entrez dans sa maison, la table est prête !

Stance + Refrain