1.
Pour la vie que tu donnes
Le soleil et les étoiles
L’eau et la rivière
Sans qui rien ne porte de fruits
Pour l’image, la ressemblance
Pour ton fils offert au monde
Pour ton nom en héritage
Merci à toi notre Père.

AU NOM DU PÈRE DU FILS ET DU SAINT ESPRIT
L’AMOUR EST RÉVÉLÉ.
AU NOM DU PÈRE DU FILS ET DU SAINT ESPRIT
IL FAIT NOTRE UNITÉ.

2.
Pour ta venue au monde
Les signes donnés à l’homme
Pour ton évangile
Qui est source de vérité.
Pour ta croix qui nous rassemble
Ta vie plus forte que la mort
Pour l’Esprit Saint que tu nous envoies
Merci à toi Jésus-Christ.

3.
Pour la joie que tu donnes
La vie naissante dans nos coeurs
La force qui nous anime
Et qui fait de nous des témoins.
Pour la foi qui nous habite
Et la communion dans l’amour
L’unité dans le royaume
Merci à toi Esprit Saint.