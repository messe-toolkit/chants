R.
Le Seigneur a planté sa tente
Sur nos sols rocailleux
Pour nourrir notre attente
De la cité de Dieu.
Il a fait chez nous sa demeure
Et de nous ses amis
Pour s’offrir à chaque heure
Jusqu’à nous donner sa vie.

1.
Il est depuis toujours
Le Verbe auprès de Dieu.
En lui était la vie ;
Rien sans lui ne s’est fait.

2.
Il a pris notre chair
De la Vierge Marie.
Il a vécu la vie
D’un homme parmi nous.

3.
Il a plongé dans l’eau
Du baptême de Jean.
Il nous baptise en lui,
Faisant de nous son corps.

4.
Il aimait la montagne.
Il y disait : “Heureux !”
Heureux qui l’accompagne
Dans sa montée vers Dieu.

5.
Il a semé la paix
Dans nos tranchées de guerre.
Il a semé la joie
Dans nos sillons de larmes.

6.
Il a rompu le pain
Pour nous donner son corps.
Il a choisi le vin
Pour nous donner son sang.

7.
Il a dit que l’amour,
C’était donner sa vie.
Il est mort sur la croix
Pour fair(e) ce qu’il a dit.

8.
Il est sorti vivant
De son tombeau de mort.
Il nous donne sa vie
Plus forte que la mort.

9.
Il reviendra un jour
Nous prendre auprès de Dieu.
Il bâtira la ville
Où régnera la Paix.