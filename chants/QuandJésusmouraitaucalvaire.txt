1.
Quand Jésus mourait au calvaire
Rejeté par toute la terre,
Debout, la Vierge, sa mère,
Souffrait, souffrait auprès de lui.

2.
Qui pourrait savoir la mesure
Des douleurs que votre âme endure,
Ô Mère, alors qu'on torture
L'enfant, l'enfant qui vous est pris ?

3.
Se peut-il que tant de souffrance
Ne nous laisse qu'indifférence,
Tandis que par nos offenses,
Nous lui, nous lui donnons la mort ?

4.
Mais nos pauvres larmes humaines
Sont bien peu devant votre peine.
Que votre Fils nous obtienne
D'y joindre, d'y joindre un vrai remords !

5.
Pour qu'enfin l'amour nous engage
Et nous livre à lui davantage,
Gravez en nous ce visage
Que vous, que vous avez chéri.

6.
Quand viendra notre heure dernière,
Nous aurons besoin d'une mère
Pour nous mener de la terre
En votre, en votre Paradis.