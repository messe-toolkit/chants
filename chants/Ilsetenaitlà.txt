1.
Moi, la fille au coeur lourd
Cet homme, je l’ai tant aimé
Heureuse et fière à ses côtés
Je l’écoutais parler d’Amour
Quand je l’ai vu
Comme un esclave ensanglanté
J’avoue, j’étais prête à le suivre
Mais ce matin-là
J’ai choisi de survivre
Tandis que l’aurore se levait

IL SE TENAIT LÀ, DEVANT MOI
DANS LA LUMIÈRE,
JE NE VOYAIS QUE LUI
CROYEZ-MOI!
JE JURE QUE C’ÉTAIT LUI !
J’AI VU LE CIEL !
J’AI PLEURÉ TOUT BAS:
« MAÎTRE, MAÎTRE ! TU ES LÀ ! »
SON AMOUR S’EST POSÉ SUR MOI

2.
Moi, j’avais suivi mon frère
Cet homme était notre espoir
Il avait annoncé sa Gloire
Nous l’avons vu tomber à terre
Quand j’ai compris
Qu’il n’y avait plus rien à tenter
Comme un voleur, je suis parti
Mais sur la route
Le ciel s’est éclairci
Et comme si l’aurore se levait

IL SE TENAIT LÀ, DEVANT MOI
DANS LA LUMIÈRE,
JE NE VOYAIS QUE LUI
CROYEZ-MOI!
JE JURE QUE C’ÉTAIT LUI !
J’AI VU LE CIEL !
JE N’Y CROYAIS PAS:
« MAÎTRE, MAÎTRE ! C’EST BIEN TOI? »
IL M’A SOURI COMME AUTREFOIS

3.
Moi, j’étais le plus fidèle
Cet homme a changé ma vie
Pourtant j’ai déserté, j’ai fui
Je n’étais pas un criminel
Quand j’ai appris
Qu’il devait être exécuté
Je suis parti à la dérive
Mais ce matin-là
Juste sur l’autre rive
Tandis que l’aurore se levait

IL SE TENAIT LÀ, DEVANT MOI
DANS LA LUMIÈRE,
JE NE VOYAIS QUE LUI
CROYEZ-MOI!
JE JURE QUE C’ÉTAIT LUI !
J’AI VU LE CIEL !
J’AI PRIÉ SI FORT :
« MAÎTRE, MAÎTRE ! PARDONNE-MOI… »
IL M’A DIT QU’IL M’AIMAIT ENCORE !