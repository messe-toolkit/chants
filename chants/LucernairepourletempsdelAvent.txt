JOIE ET LUMIÈRE DE LA GLOIRE ÉTERNELLE
DU PÈRE, LE TRÈS-HAUT, LE TRÈS-SAINT !
Ô JÉSUS CHRIST !

Versets pour le 1er dimanche

1.
Lumière éternelle du Père
illumine ceux qui habitent les ténèbres
et l’ombre de la mort.

2.
Clarté du jour nouveau,
Chasse en nous toute nuit
Pour qu’advienne le salut.

3.
Soleil de justice,
Lève toi sur les terres dévastées :
Sois notre paix

Versets pour le 2e dimanche

1.
Lumière jaillie dans notre nuit,
Chasse en nous les ténèbres du mal.

2.
Aurore du jour nouveau,
Disperse les ombres du doute.

3.
Flamme vive de l’amour,
Maintiens en nous la vigilance.

Versets pour le 3e dimanche

1.
Dans les ténèbres, lumière,
Dans la tristesse, joie.
Le Seigneur vient.

2.
Dans les doutes, lumière,
Dans le désespoir, courage.
Le Seigneur vient.

3.
Aux aveugles, lumière,
Aux pauvres de ce monde, bonheur.
Le Seigneur vient.

Versets pour le 4e dimanche

1.
Lumière de Dieu
Qui embrases la terre.
D’une clarté nouvelle rassemblant tous les peuples !

2.
Lumière de Dieu
Qui ouvres les portes de la nuit
Et traverses le pays de l’ombre et de la mort !

3.
Lumière de Dieu
Qui rayonnes la bonté du créateur
Sur le monde soumis aux ténèbres.