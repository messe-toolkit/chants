1.
Sauve-nous, Seigneur, quand nous veillons ;
Garde-nous quand nous dormons ;
Nous veillerons avec le Christ,
Et nous reposerons en paix.

2.
Entre tes mains, Seigneur Jésus, je remets mon esprit.

R.
Sainte Lumière, Splendeur du Père, louange à toi, Jésus-Christ !

1.
Maintenant Seigneur, laisse aller ton serviteur dans la paix selon ta parole.

2.
Car j'ai vu de mes yeux ton salut destiné à tous les peuples.

3.
Lumière pour éclairer les nations et gloire de ton peuple Israël.