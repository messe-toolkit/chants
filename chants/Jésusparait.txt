1.
Jésus paraît. Il vient au fleuve.
Il dit à Jean saisi d'effroi :
« Baptise-moi. »
Les flots s'émeuvent.
Qui donc pourrait laver
Ce corps sacré ?

2.
Qu'importe, Jean, que tu sois digne ?
Laisse accomplir ce qu'à l'instant
Nul ne comprend.
Il donne un signe
Pour vivre dès ce jour
Sa loi d'amour.

3.
Pourquoi frémir, abîmes sombres,
Quand le dragon qui vous hantait
Se voit défait ?
Au cœur de l'ombre
Descend le Fils de Dieu
Lumière et feu.