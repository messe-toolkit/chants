R.
Mendiants d'espérance,
Nous voici devant Toi :
Peuple façonné à ton image !
Mendiants d'avenir,
Nous voici devant Toi :
Peuple rassemblé pour le partage !

1.
Redis-nous les mots qui pardonnent,
Dieu fait chair pour notre vie !

2.
Brûle-nous du feu qui réveille,
Dieu fait chair pour notre vie !

3.
Donne-nous le pain qui fait vivre,
Dieu fait chair pour notre vie !