« TU ES BÉNIE ENTRE TOUTES LES FEMMES,
ET LE FRUIT DE TES ENTRAILLES EST BÉNI.

Heureuse celle qui a cru à l’accomplissement des paroles
Qui lui furent dites de la part du Seigneur. »