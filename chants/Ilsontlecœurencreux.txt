1.
Ils ont le cœur en creux
Ceux qui te cherchent,
Les yeux tournés vers le Soleil levant.
Ils quittent leurs habits de nuit.
Ils voudraient habiter où tout repose en Toi.

2.
Ils ont le cœur brisé
Ceux qui t’espèrent.
Ils sont venus en soulevant ta croix.
Ils sèchent leurs habits de pleurs.
Ils voudraient habiter où tout repose en Toi.

3.
Ils ont le cœur pétri
Ceux qui t’écoutent,
Renouvelés dans un creuset d’amour.
Ils lavent leurs habits terreux
Ils voudraient habiter où tout repose en Toi.

4.
Ils ont le cœur comblé
Ceux qui te chantent,
Marqués du sceau des compagnons d’Esprit. Ils mettent leurs habits de ciel.
Ils habitent déjà où tout repose en toi.