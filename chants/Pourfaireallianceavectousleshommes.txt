R.
SORS DE TA MAISON,
MARCHE VERS LES MONTS,
TU VERRAS LA TERRE PROMISE.
SORS DE TON PAYS,
MARCHE DANS LA NUIT,
TU VERRAS L’AURORE DIVINE.

1.
Pour faire alliance avec tous les hommes,
Dieu le Vivant donne sa parole,
Il appelle Abraham,
Le croyant dit sa foi;
Il appelle Abraham,
Le croyant partira.

2.
Joie d’accueillir l’Etranger qui passe,
Paix du Seigneur sur les trois visages,
Sous ta tente, Abraham,
Dieu prend place au banquet,
Sous ta tente, Abraham,
Tes espoirs sont comblés.

3.
Vois les étoiles, une foule immense,
Tu connaîtras même descendance,
De ta souche, Abraham,
Un grand peuple naîtra;
De ta souche, Abraham,
Le Messie fleurira.