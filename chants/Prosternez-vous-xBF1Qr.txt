1.
Prosternez-vous devant votre Roi,
adorez-le de tout votre cœur.
Faites monter vers sa majesté
des chants de gloire pour votre Roi des rois!

2.
Déposez-lui, toute votre vie,
accueillez-le, il est le Sauveur !
Reconnaissez son Immensité,
sa Vérité, sa Puissance et sa Gloire !