Refrain :
Dieu, mon dieu, je te cherche
Pour m’abandonner à toi.

1.
On m’a dit que ta tendresse
Est tendresse pour toujours ;
On m’a dit que ta promesse
Est promesse de l’amour.

2.
On m’a dit que tu es Père,
Que tes bras sont grands ouverts,
Que tu entends la prière
De tous ceux qui ont souffert.

3.
On m’a dit que ton visage
Est présent dans notre vie ;
Que la mort n’est qu’un passage
Et ta vie le paradis.