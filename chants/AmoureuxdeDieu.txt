R.
REVE UN MONDE MERVEILLEUX
N'OUBLIE JAMAIS D'ETRE AMOUREUX
AIME ET FAIS CE QUE TU VEUX
TU ES AIMÉ DE DIEU

1.
Si tu regardes avec ton coeur,
Sans crainte tu peux fermer les yeux
Car tu n'auras plus jamais peur,
Tu es rempli du don de Dieu

2.
Laisse-toi guider par l'Amour
Aie foi en l'homme qui te sourit
Ne lui laisse pas le coeur lourd
Ouvre ton âme, offre ta vie

3.
Tu t'en retournes avec la paix
Partage avec l'homme qui doute
Ce que tu as de beau, de vrai
Et emmène-le sur Sa route