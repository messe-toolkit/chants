1.
Sans toi je ne puis être,
Tu es la main tendue
Et nul ne peut renaître
S'il n'est pas attendu.
Le jour de ma naissance
Est au-devant de moi.
Chaque jour qui commence
Je le vivrai par toi.

ET JE TIENS LA MAIN DE JÉSUS-CHRIST.
LA MAIN TENDUE DE DIEU VERS L'HOMME.
ET JE VOIS L'AMOUR DE JÉSUS-CHRIST.
OÙ RENAÎT À LA VIE TOUT HOMME.

2.
Sans toi je ne puis vivre,
Tu es le corps offert.
Pour ma vie tu te livres,
Tu es chair de ma chair
Et les forces obscures
Où jaillit notre accord,
L'Esprit les transfigure
Au-delà de nos corps.

ET JE PRENDS LE CORPS DE JÉSUS-CHRIST,
LE CORPS OFFERT DE DIEU POUR L'HOMME.
ET JE VOIS L'AMOUR DE JÉSUS-CHRIST,
OÙ RENAÎT À LA VIE TOUT HOMME.

3.
Ta parole m'invite
Aux chemins inconnus
Et craquent les limites
Où je m'étais reclus !
En ton amour je puise
L'élan des renouveaux
Et tes pas me conduisent
Vers un monde nouveau.

ET J'ENTENDS LA VOIX DE JÉSUS-CHRIST,
LA VOIX VENUE DE DIEU VERS L'HOMME.
ET JE VOIS L'AMOUR DE JÉSUS-CHRIST,
OÙ RENAÎT À LA VIE TOUT HOMME.

4.
Sans toi je ne puis vivre,
Tu es le cœur ouvert,
Où j'apprendrai le livre
D'un nouvel univers,
J'y vois un fleuve immense
D'amour, de sang et d'eau,
Fontaine d'Espérance
Où naît l'Homme Nouveau.

ET JE VOIS LE CŒUR DE JÉSUS-CHRIST,
LE CŒUR OUVERT DE DIEU POUR L'HOMME.
ET JE VOIS L'AMOUR DE JÉSUS-CHRIST,
OÙ RENAÎT À LA VIE TOUT HOMME.