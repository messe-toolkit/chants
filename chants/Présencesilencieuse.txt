1.
Présence silencieuse,
Dieu a grandi en toi,
Sœur et compagne de Claire,
Servante du Seigneur.

2.
Tu laisses tes richesses
Pour le plus grand trésor:
Goûter le nom d'un Dieu Père
Et vivre avec lui l'alliance.

3.
Souffrances ni épreuves
Ne te font perdre cœur:
Le serviteur, le seul maître
A pris cette route étroite.

4.
Paisible et attentive,
Élance toi sans peur.
Sur le chemin quelle pierre
Pourrait empêcher ta course ?

5.
L'amour qui te possède
Affirmera tes sœurs.
Il porte au loin la lumière
D'un feu qui traverse l'ombre.

6. À l'heure du passage,
Le jour demeure en toi.
Ta paix nous parle du Père,
Servante du roi de gloire.