HEUREUX CELUI QUI VIENT VERS TOI,
LUMIÈRE AU COEUR DE TOUTE VIE,
HEUREUX CELUI-LÀ.

1.
Heureux celui que ton visage a fasciné,
HEUREUX CELUI QUE TON VISAGE
A FASCINÉ SEIGNEUR JÉSUS !
Et dont l’amour a reconnu
Partout le sceau de ton image.

2.
Heureux celui que ta présence a dépouillé,
HEUREUX CELUI QUE TA PRÉSENCE
A DÉPOUILLÉ, TU L’ENVAHIS ;
Saisie par Toi toute sa vie
Te laisse voir en transparence.

3.
Vivante icône où Ton mystère est apparu,
VIVANTE ICÔNE OÙ TON MYSTÈRE
EST APPARU SUR NOS CHEMINS,
Heureux celui qui, dans tes mains,
Passe avec Toi du monde au Père.