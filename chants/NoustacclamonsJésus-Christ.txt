LUMIÈRE ET PAIX EN NOS COEURS,
NOUS T’ACCLAMONS JÉSUS-CHRIST.
LUMIÈRE ET PAIX EN NOS COEURS,
TU RECRÉES EN NOUS LA VIE.

1.
Tu es l’aurore au point du jour
Tu viens chez moi chasser la nuit.
Tu me fais naître à ton amour
Un clair matin en moi revit.

2.
Je suis l’ouvrage de tes doigts
Je suis le souffle de l’Esprit.
Et je m’approche de la table
Tu fais de moi ton ami.

3.
Tu me recrées dans la tendresse
Et brûle en moi la charité.
Tu me façonnes à ton image
Et tu m’invites à partager.

4.
Je deviens signe d’alliance
Du sang versé de Jésus-Christ.
Et tu inscris au coeur de moi
Les sentiments de Jésus-Christ.