Humble gardien de la sainte Famille,
Époux fidèle, Père vigilant,
Dans la confiance,
Tu n’as pas craint de recevoir chez toi
Marie, l’aurore du salut.
Nous t’honorons,

Glorieux saint Joseph,
Notre soutien quotidien dans le labeur.
Tu as veillé sur Jésus comme un père,
Ô veille sur nous,
Et conduis-nous à lui.  