1.
Là où il y a la haine,
Que surgisse ton amour.
Là où il y a le doute,
Que s’élève un chant de foi !

R.
QUE TON RÈGNE VIENNE
COMME L’AUBE SUR LA NUIT !
QUE TON RÈGNE VIENNE
QU’IL ÉCLAIRE ET CHANGE NOTRE VIE !

2.
Là où règnent les ténèbres,
Que paraisse ta clarté.
Là où cesse l’espérance,
Que s’élève un chant d’espoir !

3.
Là où naissent les discordes,
Que s’installe l’unité.
Là où il y a la guerre,
Que s’élève un chant de paix !

4.
Là où il y a l’offense,
Que s’éveille le pardon.
Là où règne la tristesse,
Que s’élève un chant de joie !

5.
Là où germe le mensonge,
Fais fleurir la vérité.
Là où siège l’injustice,
Que s’élève un chant d’amour !