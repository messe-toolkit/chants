R.
Ta parole est la lumière de mes pas,
La lampe de ma route.

Puisque l’Esprit est votre vie,
Laissez-vous conduire par l’Esprit.
Tenez-vous sous la main puissante de Dieu,
Il vous rendra inébranlables.