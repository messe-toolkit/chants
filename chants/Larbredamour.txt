R.
QUAND LEUR ARBRE D’AMOUR AURA VAINCU LE TEMPS
LEURS FEUILLES SE FERONT D’OR ET D’ARGENT
ET LA SÈVE QUI COURT DANS LES YEUX DES ENFANTS
FERA NAÎTRE UN BOURGEON CHAQUE PRINTEMPS

1.
Ils se sont rencontrés
Au hasard d’une année
Ils se sont dit bonjour,
Ont parlé tendrement
De la pluie, du beau temps
Et puis naturellement
Ils se sont embrassés
Et à force d’y croire
À leur petite histoire
Ils ont pris le chemin,
Le chemin du pari
Celui qui les unit
“Toi et moi pour la vie”
Portés par tant d’espoir.

2.
Et le temps a couru
Comme fou éperdu
Dans un coin du village
Ils ont bâti maison
Ont fleuri le perron
Ils se sont dits au fond
Ils s’ront les bienvenus
Bienvenus leurs enfants,
Malicieux, pétillants
Fruits de leur bel amour,
Qu’ils soient nés de leur chair
Ou de lointaines terres,
Dans leurs yeux la lumière
Est un soleil levant.

3.
Aujourd’hui les enfants
Ont fêté leurs vingt ans
Ils sont partis un jour,
Ont quitté le foyer
Les copains du quartier
Pour s’en aller chercher
Un futur au présent
Mais quand ils s’en reviennent,
De rires les mains pleines
La maison est en fête,
Les parfums de l’enfance
La joie des connivences
Mènent aux confidences
Quand la vie vous entraîne.

4.
Et depuis qu’ils sont deux
Sont toujours amoureux
Et la vie est bien belle,
Le temps n’a pas changé
Leurs sentiments passés
S’ils devaient se quitter
Ce s’rait bien malheureux
Peut-être que demain,
Précieux dans un écrin
Sur des photos jaunies,
Retrouvera ce jour
Où la foi d’un toujours
Redit qu’un grand Amour
N’aura jamais de fin.