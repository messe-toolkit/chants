Toi, l’Infini,
Tu attends là,
Tu nous fais signe
Entre nos croix.
Comme un poème,
Tu te déploies
Entre les lignes,
Entre les lois.

Souffle l’Esprit.
Qu’il nous envoie
Soigner les vignes
De notre foi,
Là où l’on sème,
Bien au-delà,
Là où l’on s’aime
Pousse la joie.