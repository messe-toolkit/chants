R.
ES-TU CELUI QUI DOIT VENIR,
RÉPONSE DE DIEU POUR LES HOMMES?
ES-TU CELUI QUI DOIT VENIR,
VISAGE D’HOMME AUX TRAITS DE DIEU ?

1.
Es-tu celui qui doit venir éclairer nos visages ?
Tant de prophètes ont annoncé ton passage ;
Une voix a crié dans le désert :
Préparez les chemins du Seigneur…

2.
Es-tu celui qui doit venir accorder nos musiques ?
Tant de prophètes ont annoncé ta naissance ;
Dans la nuit la lumière a brillé :
Préparez les chemins du Seigneur…

3.
Es-tu celui qui doit venir rappeler l’Alliance ?
Tant de prophètes ont annoncé ta parole ;
Israël ton sauveur est nouveau né :
Préparez les chemins du Seigneur…

4.
Es-tu celui qui doit venir réveiller nos semences ?
Tant de prophètes ont annoncé ta lumière ;
L’arbre mort ce matin a refleuri :
Préparez les chemins du Seigneur…