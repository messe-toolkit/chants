DÈS LE MATIN
JE T’APPELLE CHAQUE JOUR,
DÈS LE MATIN
JE RÉCLAME TON AMOUR!

1.
Tu entends ma voix,
Je crie dans ma tête,
Tu entends ma voix,
Et je te guette…

2.
Ne fais pas le sourd
Quand tu sais ma peine,
Ne fais pas le sourd,
Puisque tu m’aimes !

3.
Tu ne m’ouvres pas,
Je frappe à ta porte,
Tu ne m’ouvres pas,
Mon coeur grelotte.

Coda:
SEIGNEUR, MON DIEU!