1.
La paix ressemble à chacun de nous
Elle est semée en chacun de nous
Dieu nous la donne, Dieu la moissonne
La paix ressemble à chacun de nous.

2.
La paix réchauffe l’humanité
Elle est le feu nous sommes clarté
Dieu nous la donne, Dieu la moissonne
La paix réchauffe l’humanité.

3.
La paix fait naître la liberté
Au grand soleil ou dans le secret
Dieu nous la donne, Dieu la moissonne
La paix fait naître la liberté.

4.
La paix donnée fait grandir la paix
Elle vient de Dieu l’homme la transmet
Dieu nous la donne, Dieu la moissonne
La paix donnée fait grandir la paix.

5.
La paix se meurt dans de vains combats
Car l’amour vrai est sa seule loi
Dieu nous la donne, Dieu la moissonne
La paix se meurt dans de vains combats.

6.
La paix est pauvre et va les pieds nus
C’est d’espérance qu’elle est vêtue
Dieu nous la donne, Dieu la moissonne
La paix est pauvre et va les pieds nus.