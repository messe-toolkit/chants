IL EST BON, SEIGNEUR, DE TE RENDRE GRÂCE !

1.
Qu’il est bon de rendre grâce au Seigneur,
de chanter pour ton nom, Dieu Très-Haut,
d’annoncer dès le matin ton amour,
ta fidélité, au long des nuits.

2.
Le juste grandira comme un palmier,
il poussera comme un cèdre du Liban ;
planté dans les parvis du Seigneur,
il grandira dans la maison de notre Dieu.

3.
Vieillissant, il fructifie encore,
il garde sa sève et sa verdeur
pour annoncer : « Le Seigneur est droit !
Pas de ruse en Dieu, mon rocher ! »