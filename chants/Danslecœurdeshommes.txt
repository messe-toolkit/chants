R.
DANS LE COEUR DES HOMMES S’EST LEVÉ
UN VENT DE PARTAGE ET D’HUMANITÉ
DANS LE COEUR DES HOMMES ON VU LE JOUR.
LES MOTS QUI CHANGENT LA VIE
DES MOTS QUI PARLENT D’AMOUR.
C’EST AU COEUR DES HOMMES QU’ELLE EST NÉE
LA PASSION D’ÉCRIRE ET DE DESSINER.
PARDESSUS LES ARMES ET LES COUTEAUX
LES MOTS DE LA LIBERTÉ
UN CHANT DE FRATERNITÉ

Après la vengeance au coeur des assassins
Lorsque la violence crache dans leurs mains
Une foule marche pour l’égalité
Une foule immense bat le pavé
Peuple debout qui chante
Peuple qui chante
Pour que la joie se réinvente
Se réinvente
Diversité, différence
C’est notre cri, notre chance
Ils ont brandi la guerre
qui l’oblige à se taire
Mais qu’entre nous
En tenailles
Ils ont perdu la bataille

Si tu apprends, si tu apprends
A ton enfant, à ton enfant
Des mots de haine à longueur de jours
Au lieu de lui donner l’envie d’un chemin d’amour
Change de cri, change de cri
Fais-le pour lui, fais-le pour lui
Et la tendresse à longueur de jours
Sera comme un levain qui fait déborder l’amour