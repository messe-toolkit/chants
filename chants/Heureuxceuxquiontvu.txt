STANCE
Heureux ceux qui ont vu le Ressuscité
Et reconnu à ses plaies la victoire.
Mais heureux sommes-nous de croire sans avoir vu,
Et, sans le voir encore d'aimer Jésus, notre Sauveur.

R.
Seigneur, tu es vivant, nous t'avons reconnu,
Au dernier jour, nous te verrons.
VERSETS POUR LE 3ème DIMANCHE ABC DU TEMPS PASCAL

1.
Pleurant près du tombeau soudain Marie se dresse
Qui donc l'appelle par son nom ?

2.
À la table d'Emmaüs les voyageurs tout interdits se lèvent.
Qui donc pour eux vient de rompre le pain ?

3.
Réfugiés dans la chambre haute les disciples frémissent
Qui donc se tient au milieu d'eux ?

4.
Approchant du rivage les pécheurs soudain s'interrogent :
Qui donc les attend près d'un feu ?

VERSETS POUR LE 2ème DIMANCHE ABC DU TEMPS PASCAL

1.
Sans voir l'accomplissement des Promesses
Vos pères les ont saluées de loin.

2.
Dans le miroir de l'espérance,
Vous contemplez l'invisible.

3.
Plus précieuse que l'or périssable
Votre foi donne louange à Dieu.