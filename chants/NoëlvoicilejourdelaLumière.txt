R.
Noël, Noël
Gloire à Dieu
Voici le jour de la lumière
Noël, Noël
Gloire à Dieu
Venez, entrez dans la lumière.

1.
Pour un seul chant de joie
D'un bout à l'autre de la Terre
Que des milliers de voix
Viennent fêter cette Lumière.

2.
Pour un seul chant de paix
D'un cœur à l'autre sur la Terre
Pour des milliers d'années
Venez cueillir cette Lumière.

3.
Pour un seul chant d'amour
Des uns aux autres sur la Terre
Pour des milliers de jours
Faites passer cette Lumière.