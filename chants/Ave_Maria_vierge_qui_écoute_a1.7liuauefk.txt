1.
Vierge qui écoute
la parole du Seigneur,
Dans la foi, dans l'Esprit :
Heureuse es-tu Marie !

R.
Ave Maria, gratia plena,
Dominus te cum benedictatu !

2.
Vierge du silence,
tu contemples le Sauveur,
Ton enfant et ton Dieu :
Heureuse es-tu Marie !

3.
Mère de l'Eglise,
à la Croix tu es debout,
Cœur livré, cœur meurtri :
Heureuse es-tu Marie !

4.
Vierge du Cénacle,
tu attends le Saint Esprit,
Tu es là et tu pries :
Heureuse es-tu Marie !

5.
Femme dans la gloire,
signe pur des cieux nouveaux,
Vraie clarté dans la nuit :
Heureuse es-tu Marie !