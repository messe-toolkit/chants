1.
Nous avons fait ce long voyage
Pour arriver au voyageur ;
Nous étions tour à tour rivage,
Le “passager” et le “passeur”…

REFRAIN (bis)
Au revoir !
Tu sais, faut pas qu’la vie s’arrête,
Au revoir !
Et puis, ce n’est qu’un “au revoir”…

2.
Libre comme un oiseau en cage,
Je rêvais tant d’un autre ailleurs,
Mais je garderai cette image
Celle qui ressemble au bonheur…

3.
Et puisque nul ne saurait être,
Sans aimer, sans “exister pour”
Voici les clés pour te permettre,
D’être chez toi, et pour toujours…