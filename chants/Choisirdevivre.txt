1.
Seul celui qui voyage connaîtra le chemin qui mène à son pays
Seul, peut-être le sage connaît la liberté parce qu’il y met le prix
Seul, qui affronte l’orage connaîtra l’arc-en-ciel et le goût de la pluie.
Seul, celui qui s’engage connaîtra la grandeur et le sens de la vie.

CHOISIR DE VIVRE ! LUTTER POUR VIVRE !
ENVIE DE RIRE ET DE CHANTER VIVE LA VIE ! (bis)

2.
Seul qui écoute la plage connaîtra les chansons qui s’adressent à lui.
Seul qui décrypte un visage se connaîtra lui-même et bien sûr l’autre aussi.
Seul qui tourne la page connaîtra le soleil au-delà de la nuit.
Seul qui est au labourage et puis au pâturage connaît le goût des fruits.

3.
Seul, qui sans maquillage avance au quotidien et connaît l’aujourd’hui
Seul, qui largue cordages connaîtra l’aventure et la soif d’infini.
Seul mais s’il fait passage peut devenir passeur et cela malgré lui.
Seul, ce serait dommage alors viens avec moi, s’il te plaît, mon ami.