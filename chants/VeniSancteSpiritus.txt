R.
Veni sancte spiritus
Désirons-le par dessus tout
Laissons-nous conduire par l’Esprit du Seigneur
Laissons-le agir en nous.

1.
L’Esprit est là, au plus profond de l’âme.
Il nous soutient dans la foi,
dans l’espérance et dans l’amour.

2.
Soyons-lui fidèle, Il est le plus grand don
car Il nous rend capable d’aimer notre Dieu,
de nous aimer les uns les autres. Soyons fidèles !

3.
Ainsi enflammés de l’Esprit,
c’est Jésus qui aimera,
qui agira en nous.