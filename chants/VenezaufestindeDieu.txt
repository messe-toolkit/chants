R.
Venez au festin de Dieu, goûtez sa paix et sa joie !

1.
Quand nous mangeons le pain, quand nous buvons la coupe,
Nous annonçons la mort du Seigneur.

2.
Le pain est le corps livré, la coupe est le sang de l'alliance,
Et ce repas nous unit dans l'amour.