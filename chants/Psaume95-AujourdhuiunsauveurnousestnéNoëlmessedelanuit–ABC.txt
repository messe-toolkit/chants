AUJOURD’HUI, UN SAUVEUR NOUS EST NÉ :
C’EST LE CHRIST, LE SEIGNEUR.

1.
Chantez au Seigneur un chant nouveau,
chantez au Seigneur, terre entière,
chantez au Seigneur et bénissez son nom !

2.
De jour en jour, proclamez son salut,
racontez à tous les peuples sa gloire,
à toutes les nations ses merveilles !

3.
Joie au ciel ! Exulte la terre !
Les masses de la mer mugissent,
la campagne tout entière est en fête.

4.
Les arbres des forêts dansent de joie
devant la face du Seigneur, car il vient,
car il vient pour juger la terre.

5.
Il jugera le monde avec justice,
et les peuples selon sa vérité !