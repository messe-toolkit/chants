Refrain :
C’est lui, c’est son pas,
Je l’attendais depuis longtemps déjà,
C’est lui, c’est sa voix,
Du moins je crois...

1.
Il faisait si froid,
J’ai rallumé pour Lui le feu de bois
Et, ça va de soi,
Fait le repas.

2.
Il me parlera
De la vie, de l’amour, de nos combats ;
Il connaît tout ça
Bien mieux que moi !..

3.
Il fredonnera
Ce que nous chantions tant et tant de fois ;
Tout s’éveillera
Comme autrefois.