R.
NOËL! NOËL ! JÉSUS EST NÉ :
UN NOUVEL APPEL À LA PAIX.
NOËL! NOËL! UN NOUVEAU JOUR,
UN NOUVEL APPEL À L’AMOUR.

1.
Noël, c’est Dieu avec nous,
Emmanuel!
Noël, c’est Jésus qui vient :
Fils du Dieu vivant.

2.
Noël, c’est Dieu qui libère,
Emmanuel!
Noël, c’est Jésus qui vient :
Berger pour les peuples.

3.
Noël, c’est Dieu qui nous sauve,
Emmanuel!
Noël, c’est Jésus qui vient :
Prince de la paix.

4.
Noël, c’est Dieu qui se donne,
Emmanuel!
Noël, c’est Jésus qui vient :
Germe de justice.