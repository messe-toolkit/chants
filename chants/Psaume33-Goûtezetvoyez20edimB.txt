GOÛTEZ ET VOYEZ
COMME EST BON LE SEIGNEUR !

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Saints du Seigneur, adorez-le :
rien ne manque à ceux qui le craignent.
Des riches ont tout perdu, ils ont faim ;
qui cherche le Seigneur ne manquera d’aucun bien.

3.
Venez, mes fils, écoutez-moi,
que je vous enseigne la crainte du Seigneur.
Qui donc aime la vie
et désire les jours où il verra le bonheur ?

4.
Garde ta langue du mal
et tes lèvres des paroles perfides.
Évite le mal, fais ce qui est bien,
poursuis la paix, recherche-la.

ALLÉLUIA. ALLÉLUIA.

Qui mange ma chair et boit mon sang
demeure en moi, et moi en lui, dit le Seigneur.