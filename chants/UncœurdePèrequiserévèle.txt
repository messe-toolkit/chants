UN COEUR DE PÈRE QUI SE RÉVÈLE,
SUR CETTE TERRE : BONNE NOUVELLE !
C'EST DIEU QUI SÈME SA PAROLE,
POUR QU'ELLE GERME CHEZ TOUS LES HOMMES !

1.
Au commencement, il créa la vie,
Son souffle planait au-dessus de l'eau,
Et ce fut le temps où le Père dit :
"Il est bon, il naît, ce monde si beau !"

2.
Dans le paradis, comblé de l'amour,
Adam fait le choix, de désobéir,
Le peuple conduit, sait qu'il vient le jour,
Où viendra le Roi, donné pour servir !

3.
Au temps du désert, conduit par l'Esprit,
Abraham entend, qu'il faut avancer,
Traversant la mer, Moïse s’enfuit,
Il offre son chant, il se sait aimé !

4.
À Jérusalem, au temple doré,
Les prophètes crient, tous ces mots de feu,
Qui attend l'Eden, se fait baptiser,
Par Jean quand surgit le Messie de Dieu.