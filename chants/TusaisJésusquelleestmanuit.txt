1.
Tu sais, Jésus, quelle est ma nuit ;
Es-tu à l’oeuvre dans ma vie
D’aveugle-né ?
Sois la réponse à mes pourquoi,
Révèle-moi quelle est ta voie
Pour l’être aux yeux fermés.

LUMIÈRE POUR MES YEUX,
TU VIENS VERS MOI.
PROPHÈTE AMI DE DIEU,
EN TOI JE CROIS.
TU SAIS CE QUE JE VEUX:
Ô SEIGNEUR, QUE JE VOIE !

2.
Toi qui me parles au fond du coeur,
Révèle-toi le Dieu sauveur
Qui peut guérir.
Accorde-moi de te parler,
Alors ma voix pourra chanter
L’amour qui fait surgir.

PAROLE DU SEIGNEUR,
EN MOI, PRENDS CORPS!
PROPHÈTE AMI DE DIEU,
DIS-MOI SES MOTS !
TU SAIS CE QUE JE VEUX:
EXULTER DE SA JOIE !