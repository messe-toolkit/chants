R.
TOUT T'APPARTIENT
LE SOLEIL ET LA MER
LA LUMIERE DU MATIN
L'OISEAU DANS LE DESERT
TOUT TE PARLE D'AMOUR
TOUT TE PARLE DE JOIE
D'UN BONHEUR ECLATANT
FAIT POUR TOI.

1.
Y' en a qui te diront
De garder le silence
Surtout de rester sage
De ne pas perdre patience
A ce prix on enchaîne
La vie dans les prisons
On étouffe l'espoir
On sauve les millions.

2.
Comme le petit enfant
Qui rêve d'avenir
Ouvre tout grand tes portes
Il est temps de sortir
Demain viendra bientôt
Si tu es endormi
Tu manqueras c'est sûr
La fête de la vie.

3.
Tu es l'enfant d'amour
Qui réchauffe la terre
Tu es l'homme nouveau
De feu et de lumière
Tu entraînes avec toi
La folie, la passion
Voici le temps de vivre
De perdre la raison.