R.
UN ASTRE SE LÈVE, SOLEIL DE JUSTICE ;
QU’IL RÈGNE À JAMAIS, LE FILS DE DAVID,
QU’IL RÈGNE À JAMAIS, JÉSUS L’EMMANUEL!

1.
Son règne sera grand
Et ses demeures toutes belles :
Jardin de paix au bord d’un fleuve,
Vallée des justes en Israël. (bis)

2.
Lui seul nous conduira
Vers le bonheur des sources vives,
Comme un berger parmi son peuple,
Seigneur et Maître en Israël. (bis)

3.
Vers lui regarderont
Petits et grands de notre terre :
Ceux qui recherchent son alliance,
Immense foule en Israël. (bis)