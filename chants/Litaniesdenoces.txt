1.
Tu me donnes ton nom,
Je t’offre mon destin,
Tu m’offres ta maison,
Je te donne ma main.

R.
Chair de ma chair,
Sang de mon sang,
Mon rire sur tes lèvres,
Tes vagues sur ma grève,
Puisque nous serons deux,
Nous ne ferons plus qu’un,
Le ciel de nos chemins
S’étoilera d’un feu qui jamais ne s’éteint.

2.
Tu me donnes ton champ,
Je t’offre mon pays,
Tu m’offres tes torrents,
Je te donne mon puits.

3.
Tu me donnes ton pain,
Je t’offre mon amour,
Tu m’offres tes matins,
Je te donne mes jours.

4.
Tu me donnes tes soirs,
Je t’offre mes réveils,
Tu m’offres ton regard,
Je te donne mon ciel.

5.
Tu me donnes ton blé,
Je t’offre mes rubis,
Tu m’offres tes étés,
Je te donne ma vie.