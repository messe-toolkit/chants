TA JOIE DANS LA GLOIRE DU CIEL,
MARIE, TU LA DOIS À TON FILS.
TOUTE GRÂCE, AIMÉE DE DIEU,
LE SEIGNEUR EST AVEC TOI,
LUI LA SOURCE DE TA JOIE.

1.
Vierge de lumière, tu nous dis ta foi,
Mère sans pareille près du Christ en croix !

2.
Femme au coeur fidèle jusqu’au dernier jour,
Trésor de tendresse sur la voie d’amour !

3.
Quand le vin des noces vient à nous manquer,
Ta demande est forte, Dieu sait t’exaucer !

4.
Témoin du Royaume sur les pas du Christ,
Tu le suis en pauvre qui a soif de vie.

5.
Couronnée d’étoiles, signe dans le ciel,
Tu dis la victoire du Seigneur de paix.