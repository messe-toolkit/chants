R.
Cari-carillonne un chant nouveau,
Qu’il enchante le Seigneur
Cari-carillonne un chant nouveau,
C’est la fête dans mon cœur !

1.
Pour te dire que je t’aime,
Avec le chant des oiseaux,
Je ferai de la musique
Toute en flûtes et en pipeaux !

2.
Pour te dire que je t’aime,
Avec les arbres et les fleurs,
J’inventerai des poèmes
Tout en rimes et en couleurs !

3.
Pour te dire que je t’aime,
Avec un bel arc-en-ciel,
J’écrirai des pas de danse,
Tout en gerbes d’étincelles !

4.
Pour te dire que je t’aime,
Avec le chant de la mer,
Je ferai jouer l’orchestre
Tout en vagues et en éclairs !