R.
BIENHEUREUSE, TOI QUI AS CRU
À L’APPEL DE TON SEIGNEUR!
TU REÇOIS LE VERBE ATTENDU,
SA LUMIÈRE ENVAHIT TON COEUR.

1.
Avec la foi d’Abraham et de Moïse
Tu as vécu parmi ton peuple en Israël.
Coeur libéré pour accueillir l’Emmanuel,
Tu étais prête à croire au Dieu de l’impossible.

2.
Avec la foi de l’épouse qui jubile,
Tu fais jaillir le chant nouveau Magnificat.
Élisabeth et son enfant vibrent de joie,
Il est venu, le temps promis de l’Évangile.

3.
Avec la foi d’une mère dans l’épreuve,
Tu es debout près de ton Fils au Golgotha.
L’Apôtre Jean devient le fils que tu reçois,
Et comme lui nous te prenons dans nos demeures.

4.
Avec ta foi en Jésus vainqueur de Pâques,
Tu oses croire à la puissance de la Vie.
L’Esprit descend sur les disciples réunis,
Tu les soutiens par ta prière et par ta flamme.