R.
Je voudrais te remercier,
Te le dire et le chanter,
Mais souvent je ne sais pas te parler de ma joie,
Je voudrais te remercier,
Te le dire et le chanter
Mais souvent je n’ose pas te parler tout bas.

1.
Merci mon Dieu, merci,
Pour la famille
Que tu m’as donnée
Pour la famille
Dont je suis aimé,
Et qui m’a appris
À te connaître.

2.
Merci mon Dieu, merci,
Pour cette terre
Remplie de beauté,
Pour cette terre
Belle à regarder,
Quand elle est miroir de ta lumière.

3.
Merci mon Dieu, merci,
Pour la confiance
Que tu mets en moi,
Pour la confiance
Qui emplit ma voix,
Quand je suis témoin de ta Parole !