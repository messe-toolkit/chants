Ô MARIE, TU NOUS AIMES TANT
NOUS QUI SOMMES TES ENFANTS.
DONNE-NOUS TOUT AU LONG DES JOURS
D’ÊTRE TÉMOINS DE TON AMOUR.

1.
À chaque seconde et à chaque pas
Je le sais, Marie, tu n’es jamais loin.
Je sens ta présence tout près de moi
J’ai confiance en toi, tu me tiens la main.

2.
Auprès des malades, auprès des mourants
Comme une maman, tu ouvres les bras.
Tu veilles en silence, le coeur souffrant
Tu portes en ton coeur le poids de nos croix.

3.
En pèlerinage, en route avec toi
Nous venons chanter, marcher dans la joie.
Entends nos prières, monter vers toi
Tu viens nous guider jusqu’à l’au-delà.