ANTIENNE
VENEZ, TOUT EST PRET POUR LE BANQUET
VERSETS

1.S

1. es invité es-tu préparé ?S

1.S

1.s-tu refuser peux-tu t'excuser ?S

1.S

1.lgré nos péchés Dieu veut nous convier.
4.
Dieu sait nous aimer Dieu sait pardonner.
5.
Allez au festin laissez vos chagrins.
6.
Jésus nous attend son corps est présent.

VERSETS

7.S

7.eu est mangé ton coeur peut chanter.S

7.
Quand Dieu est mangé ton corps peut danser.
9.
Toi seul est Seigneur la joie de nos coeurs.
10.
Par tous les chemins allons au festin.
11.
Allez par la ville vivez l'Evangile.