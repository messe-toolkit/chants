GLOIRE A DIEU
PAIX AUX HOMMES
JOIE DU CIEL SUR LA TERRE !
JOIE DU CIEL SUR LA TERRE !

1.
Pour tes merveilles, Seigneur Dieu,
Ton peuple te rend grâce :

2.
Ami des hommes sois béni,
Pour ton règne qui vient !

3.
A toi les chants de fête
Par ton Fils bien-aimé dans l'Esprit

4.
Sauveur du monde Jésus-Christ,
Écoute nos prières ;

5.
Agneau de Dieu, vainqueur du mal,
Sauve-nous du péché !

6.
Dieu Saint, splendeur du Père,
Dieu vivant, le Très-Haut, le Seigneur