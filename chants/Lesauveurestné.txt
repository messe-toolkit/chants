R.
LE SAUVEUR EST NE
PAIX SUR LES HOMMES
PAIX SUR NOTRE TEMPS
PAIX SUR AUJOURD'HUI.
LE SAUVEUR EST NE
JOIE POUR LE MONDE
JOIE POUR AUJOURD'HUI
JOIE POUR TOUS LES TEMPS.

1.
Anges du Seigneur, amis des hommes,
Dites-nous pourquoi Dieu sous notre toit ?

2.
Courent les bergers jusqu'à la crèche,
Puisqu'il les attend, quel est cet enfant ?

3.
Son étoile invite au loin les mages,
Dans la nuit sans fin, quel est ce chemin ?