1.
Sur le chemin, tous ceux qui cherchent Dieu
Sont en attente d’accueillir le Sauveur.
Il nous surprend : il se fait si petit,
Pauvre et fragile, pour naître parmi nous.

JÉSUS, JÉSUS,
SAUVEUR ATTENDU !
JÉSUS, TON NOM,
EST « EMMANUEL », « DIEU AVEC NOUS ». (bis)

2.
Source de vie, Jésus est déjà là,
Il nous attend, pour abreuver nos coeurs ;
Il nous connaît, et nous appelle à lui,
Par nos prénoms, pour être ses amis.

JÉSUS, JÉSUS,
VISAGE DU PÈRE !
JÉSUS, EN TOI,
EST LA SOURCE DE VIE ÉTERNELLE. (bis)

3.
Dès le matin, Jésus aime prier,
Dans le secret, ce Père qu’il connaît.
Il aime tant accueillir tous ces gens,
Leur révéler combien ils sont aimés !

JÉSUS, JÉSUS,
FILS AIMÉ DU PÈRE !
JÉSUS, JE CROIS !
APPRENDS-MOI À PRIER COMME TOI ! (bis)

4.
C’est aujourd’hui que le salut arrive
Dans nos maisons, dans le coeur des petits
L’amour de Dieu peut transformer nos vies,
Croire en Jésus apporte le bonheur !

JÉSUS, JÉSUS,
TENDRESSE DU PÈRE !
JÉSUS, TA JOIE,
C’EST DE VENIR HABITER EN MOI. (bis)

5.
C’est par amour que Jésus est venu,
C’est par amour qu’il a donné sa vie !
Pour nous conduire dans l’éternelle vie,
Celle de Dieu, plus forte que la nuit.

JÉSUS, JÉSUS,
VIVANT POUR LES SIÈCLES !
JÉSUS, TA CROIX,
EST SIGNE DE TON AMOUR POUR MOI. (bis)

6.
À ses disciples, pour être ses témoins,
Jésus promet la force de l’Esprit.
Comme une flamme, il est venu sur eux.
Ils sont sortis : c’est l’Église qui naît.

JÉSUS, JÉSUS,
TU ENVOIES L’ESPRIT !
JÉSUS, TA FORCE,
FAIS QU’ELLE BRÛLE EN NOUS, AUJOURD’HUI ! (bis)