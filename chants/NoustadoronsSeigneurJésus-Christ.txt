Nous t’adorons, Seigneur Jésus Christ,
ici et dans toutes les églises qui sont dans le monde entier,
et nous te bénissons, parce que, par ta sainte croix,
tu as racheté le monde.