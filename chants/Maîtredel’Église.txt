1.
Maître de L’Église
Tu parlais à ton peuple
Sur la barque de Pierre :
Appel pour Israël.
De Sion vient le salut.
La ville aux douze portes
Unira l’humanité
Dans la splendeur des noces.

2.
Toi que Paul annonce
Aux nations de ce monde,
Tu rassembles les hommes :
En toi tous ne font qu’un,
Et le proche et le lointain.
Chacun devient pour l’autre
Le témoin du Dieu plus grand :
Verbe au-delà des langues.

3.
Dans la ville immense
Où se perdent les foules,
Pierre et Paul se rejoignent,
Guidés par l’Esprit.
L’Évangile est proclamé.
S’achève ici leur course,
Où l’empire a son orgueil,
Sous les remparts de Rome.

4.
Fière Babylone,
Tu rejettes et méprises
Les témoins sans fortune
Du Christ mort sur la croix :
Vois le prix de ton salut !
Ô Rome bienheureuse,
N’oublie pas leur sang versé,
Source à jamais précieuse.