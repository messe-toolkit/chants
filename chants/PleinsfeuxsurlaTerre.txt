PLEINS FEUX SUR LA TERRE
PLEINS FEUX AUJOURD’HUI
PLEINS FEUX SANS FRONTIÈRES
PLEINS FEUX POUR LA VIE.

1.
Cherche la lumière
Dans le ciel chaque nuit
Trouve la lumière
Dans les yeux d’un ami.

2.
Veille la lumière
Elle promet l’avenir
Laisse la lumière
Doucement t’envahir.

3.
Sème la lumière
Dans ce monde si noir
Cueille la lumière
Au jardin de l’espoir.

4.
Porte la lumière
Dans tes mains d’éclaireur
Donne la lumière
Comme on offre son coeur.