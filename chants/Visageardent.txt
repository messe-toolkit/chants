1.
Visage ardent du Christ en croix,
Ta clarté resplendit sur l’Église ;
Visage ardent tourné vers toi,
Elle annonce déjà le Royaume.

2.
Vivant message à l’homme mort,
Ta Parole illumine l’Église ;
Vivant message écho du tien,
Elle apporte la Bonne Nouvelle.

3.
Passion d’un Dieu au sang versé,
Ton amour a jailli en Église ;
Passion du corps cloué au bois,
Dont l’amour fait renaître les hommes.

4.
Ô joie de Dieu pour nous pécheurs,
Ton pardon a fleuri dans l’Église ;
Ô joie de Mère en ses enfants,
Elle porte ses fruits sur la terre.

5.
De tout pays, au long du temps,
L’Esprit Saint nous convoque en Église,
En toute langue un peuple saint
Bénit Dieu pour l’alliance nouvelle.

6.
Dieu fort, vainqueur, premier témoin,
Le Vivant, le Seigneur de l’Église ;
Dieu fort, rocher de notre foi,
Sur ton Nom se bâtit le Royaume.