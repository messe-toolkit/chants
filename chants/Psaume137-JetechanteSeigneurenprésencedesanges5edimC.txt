JE TE CHANTE, SEIGNEUR,
EN PRÉSENCE DES ANGES.

1.
De tout mon coeur, Seigneur, je te rends grâce :
tu as entendu les paroles de ma bouche.
Je te chante en présence des anges,
vers ton temple sacré, je me prosterne.

2.
Je rends grâce à ton nom pour ton amour et ta vérité,
car tu élèves, au-dessus de tout, ton nom et ta parole.
Le jour où tu répondis à mon appel,
tu fis grandir en mon âme la force.

3.
Tous les rois de la terre te rendent grâce
quand ils entendent les paroles de ta bouche.
Ils chantent les chemins du Seigneur :
« Qu’elle est grande, la gloire du Seigneur ! »

4.
Ta droite me rend vainqueur.
Le Seigneur fait tout pour moi !
Seigneur, éternel est ton amour :
n’arrête pas l’oeuvre de tes mains.

ALLÉLUIA. ALLÉLUIA.

« Venez à ma suite, dit le Seigneur,
et je vous ferai pêcheurs d’hommes. »