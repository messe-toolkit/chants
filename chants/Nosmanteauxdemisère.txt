R.
LAISSE TOMBER TON MANTEAU DE MISÈRE
LÂCHE TOUT CE QUI RETIENT LA POUSSIÈRE,
VIENS, METS TA VIE EN PLEINE LUMIÈRE,
PREND L’HABIT AUX COULEURS QUI ÉCLAIRENT.

1.
Que tes pensées soient de paix,
Que tes deux mains soient d’amitié
Laisse tomber ce qui empêche d’aimer
Mets-toi en route pour changer.

2.
Que tes paroles soient de paix,
Que ton regard soit de clarté,
Laisse tomber ce qui empêche d’aimer,
Mets-toi en route pour changer.

3.
Que chaque geste soit de paix,
Que ton sourire en soit la clé,
Laisse tomber ce qui empêche d’aimer,
Mets-toi en route pour changer.

4.
Que ton chemin soit de paix,
Que tes oublis soient réveillés,
Laisse tomber ce qui empêche d’aimer,
Mets-toi en route pour changer.