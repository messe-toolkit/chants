Antienne - stance
Seul un Samaritain revint sur ses pas rendre grâce au Seigneur.

R.
Oculi nostri ad Dominum Jesum
Oculi nostri ad Dominum nostrum

Versets

Ps 20

1.
Seigneur, le roi se réjouit de ta force ;
quelle allégresse lui donne ta victoire !
Tu as répondu au désir de son coeur,
tu n'as pas rejeté le souhait de ses lèvres.

2.
Tu lui destines bénédictions et bienfaits,
tu mets sur sa tête une couronne d'or.
La vie qu'il t'a demandée, tu la lui donnes,
de longs jours, des années sans fin.

3.
Par ta victoire, grandit son éclat :
tu le revêts de splendeur et de gloire.
Tu mets en lui ta bénédiction pour toujours :
ta présence l'emplit de joie !

4.
Oui, le roi s'appuie sur le Seigneur :
la grâce du Très-Haut le rend inébranlable.
Dresse-toi, Seigneur, dans ta force :
nous fêterons, nous chanterons ta vaillance.

Ou bien

Ps 94

1.
Venez, crions de joie pour le Seigneur,
acclamons notre Rocher, notre salut !
Allons jusqu'à lui en rendant grâce,
par nos hymnes de fête acclamons-le !

2.
Oui, le grand Dieu, c'est le Seigneur,
le grand roi au-dessus de tous les dieux :
Il tient en main les profondeurs de la terre,
et les sommets des montagnes sont à lui ;

3.
À lui la mer, c'est lui qui l'a faite,
et les terres, car ses mains les ont pétries.
Entrez, inclinez-vous, prosternez-vous,
adorons le Seigneur qui nous a faits.

4.
Aujourd'hui écouterez-vous sa parole ?
" Ne fermez pas votre coeur comme au désert,
où vos pères m'ont tenté et provoqué,
et pourtant ils avaient vu mon exploit. "