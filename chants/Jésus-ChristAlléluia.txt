Refrain :
Jésus-Christ, alléluia !
Jésus-Christ, alléluia !
Jésus-Christ, alléluia !
Jésus-Christ sur nos chemins ! (bis)

1.
Comme un oiseau blessé sans défense
Tu me vois désemparé,
Toi qui as tant connu la souffrance
Tu ne peux m’abandonner !
Comme un bateau seul dans la tempête,
Cherchant en vain du secours,
Dis-moi où sont passés tes prophètes,
Dis-moi ce qu’est l’Amour !

2.
Je t’ai cherché au plus haut des montagnes
Jusqu’au fond des vallées,
Au fond des villes et des campagnes,
Je ne t’ai pas trouvé ;
J’ai questionné les savants et les sages
Sans être satisfait,
Partout je cherche encore ton visage,
Dis-moi donc, où tu es !

3.
Dans notre monde où l’argent est seul maître
Et l’amour un plaisir,
Il est temps pour moi de te connaître,
Je crois qu’il faut partir ;
Avec tous ceux qui crieront ton histoire
Par les quatre horizons,
Avec tous ceux qui commencent à y croire,
Je veux crier ton nom !