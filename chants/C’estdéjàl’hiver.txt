1.
C’est déjà l’hiver,
le jour est moins clair.
Mais bientôt c’est Noël,
la Lumière sera belle !

UN PETIT ENFANT NAÎTRA DE MARIE,
IL APPORTERA LA JOIE PLEIN LA VIE.
UN PETIT ENFANT NAÎTRA DANS LA NUIT
ET L’AMOUR ENVAHIRA NOTRE VIE !

2.
Tu viens dans mon coeur
souffler sur mes peurs.
Je t’attends, je suis prêt,
petit prince de la Paix !