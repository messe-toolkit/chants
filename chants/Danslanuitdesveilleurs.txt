DANS LA NUIT DES VEILLEURS
NOUS TENONS NOTRE LAMPE ALLUMÉE.
TU CONNAIS, TOI SEIGNEUR,
LES MILLIONS DE VIVANTS TORTURÉS.
PRÈS DE TOI NOUS PRIONS :
QUE LEURS YEUX VOIENT LE JOUR SE LEVER !

1.
Pour les victimes et leurs bourreaux
Nous formons la chaîne des priants :
Chaîne aux maillons forts des non-violents,
Voix criant l’horreur dans les cachots.

2.
Avec nos cris de révoltés,
Nous formons la chaîne des appels :
Chaîne qui bouscule terre et ciel
Pour que des tyrans soient ébranlés.

3.
De tous les points de l’horizon
Nous formons la chaîne de nos mains :
Chaîne face aux murs de l’inhumain
Pour briser le mal dans les prisons.

4.
Chrétiens debout en vrais témoins,
Nous formons la chaîne de l’espoir :
Chaîne qui rejoint tout homme en croix
Dans sa traversée vers le matin.