Refrain : LOUÉ SOIT DIEU POUR L’AUBE DE PÂQUE !

1.
Jour du seigneur,
Plus éclatant qu’un soleil,
Jour du Seigneur,
Lève-toi pour dissoudre ma nuit,
Pour clarifier mon regard ! Ref.

2.
Puissant Amour,
Victorieux de la mort,
Puissant Amour,
Brûle en moi ce qui n’est pas l’amour :
Incendie-moi tout entier ! Ref.

3.
Souffle de vie,
Force qui tiens l’univers,
Souffle de vie,
Porte-moi au-delà de mes peurs,
A la douceur de la croix ! Ref.