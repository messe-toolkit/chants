1.
Près de toi se trouve le pardon :
toute guérison et toute grâce,
Tu entends ma voix au fond de mes impasses !
Dieu qui n’oublies pas,
rien de ma plainte ne t’échappe.

2.
Viendras-tu, le soir, comme un voleur,
ou dans la splendeur d’une aube en fête ?
Rien ne m’est connu de l’heure qui s’apprête,
mais, à ta venue,
tout dans ma nuit sera lumière.

3.
Bienheureux celui qui veillera
quand tu paraîtras, nimbé de gloire,
Tu l’inviteras, lui dresseras la table,
tu le serviras :
qu’il prenne la plus belle place !