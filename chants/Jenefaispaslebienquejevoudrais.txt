R.
Je ne fais pas le bien que je voudrais
Je fais souvent le mal qui te déplaît
Mais j'ai confiance et je viens vers Toi, sans peur,
Car ton amour est plus grand que mon cœur.

1.
Mes pensées ne sont pas tes pensées.
Et mes chemins ne sont pas toujours tes chemins
Alors je viens pour écouter ta Parole, Seigneur.
C'est elle qui fait changer mon regard et mon cœur.
Toi, tu enlèves le poids de ma faute
Je suis en paix tu m'accueilles en ton amour.

2.
Mes paroles sont parfois un fusil.
Elles font si mal, elles blessent ou elles tuent des amis.
Alors je viens pour écouter tes paroles de paix.
Elles sont les mots d'amour qui invitent à aimer.
Toi, tu enlèves le poids de ma faute
Je suis en paix tu m'accueilles en ton amour.

3.
Ce que je fais, en mal ou en bien,
Tu le connais, car c'est à Toi, ô Seigneur que je le fais.
Tu es présent dans ceux qui sont à mes côtés.
Toi, tu enlèves le poids de ma faute
Je suis en paix tu m'accueilles en ton amour.

4.
Tu as dit :"Je ne viens pas juger.
Je suis venu pour vous aimer et vous sauver".
Brebis perdue, fils égaré qui a fui la maison.
Tu nous attends, les bras ouverts pour le pardon.
Toi, tu enlèves le poids de ma faute
Je suis en paix tu m'accueilles en ton amour.

5.
Nous aussi, tu veux nous envoyer
Pour annoncer à tous ton pardon et ta paix.
Notre péché est enterré dans la nuit du tombeau
Tu nous fais vivre au grand jour d'un monde nouveau.
Toi, tu enlèves le poids de ma faute
Je suis en paix tu m'accueilles en ton amour.