R.
Tu étais le témoin de notre oui
Il y a cinquante ans dans notre vie
La tendresse a pris racine dans nos cœurs
Tous les deux fidèles, ensemble à lui Seigneur
Nous offrons nos cinquante ans d'amour
Nous offrons nos cinquante ans d'amour

1.
Tu as fais la route avec nous
Pendant cinquante ans, Seigneur
Tu as fais durer entre nous
Notre cœur à cœur

2.
Et sous ton regard aujourd'hui
Après cinquante ans d'amour
Être tous les deux nous réjouit
Comme au premier jour

3.
Nous avons reçu de l'Amour
A travers cinquante années
Des enfants qui chantent à leur tour
Ceux qui leur sont nés