1.
J’ai vu l’eau vive
jaillissant du coeur du Christ, Alleluia !
Tous ceux que lave cette eau
seront sauvés et chanteront : Alleluia !

2.
J’ai vu la source
devenir un fleuve immense, Alleluia !
Les fils de Dieu rassemblés
chantaient leur joie d’être sauvés, Alleluia !

3.
J’ai vu le Temple
désormais s’ouvrir à tous, Alleluia !
Le Christ revient victorieux,
montrant la plaie de son côté, Alleluia !

4.
J’ai vu le Verbe
nous donner la paix de Dieu, Alleluia !
Tous ceux qui croient en son nom
seront sauvés et chanteront, Alleluia !