R.
ENTRE NOS MAINS
C'EST LE PAIN DU PARTAGE,
CORPS DU CHRIST RESSUSCITE,
ENTRE NOS MAINS
C'EST LE PAIN DU PARTAGE,
CORPS DE CELUI QUI NOUS DONNE SA VIE,
JESUS-CHRIST, JESUS CHRIST.

1.
Pain du partage
Tu viens sur nos chemins
Habiter notre faim sur la terre,
Viens au secours de notre faiblesse
Toi, dont le pur Amour
Invente la tendresse.

2.
Pain du partage
Tu viens nous rencontrer
Pour mieux nous révèler ta prière.
Viens au secours de notre faiblesse
Toi, dont le pur Amour
Invente la tendresse.

3.
Pain du partage
Tu viens comme un ami
Pour changer notre nuit en lumière.
Toi, dont le pur Amour
Invente la tendresse.

4.
Pain du partage
Tu nous invites aussi
A donner notre vie pour nos frères.
Viens au secours de notre faiblesse
Toi, dont le pur Amour
Invente la tendresse