1.
Nous te cherchions, Seigneur Jésus,
Nous t'avons longtemps attendu,
Nous avions soif de ton visage :
Ô seul désir pour notre foi
Qu'un long regard posé sur toi.

2.
Comme une source jaillissant
Remplirait la nuit de son chant,
Tu nous redis le nom du Père :
Révélation de cet Amour
Qui te possède au premier jour.

3.
Ce qui commence là sans bruit,
L'oblation du grain pour le fruit,
Qui parmi nous peut le comprendre ?
Voici le pain, voici le vin
Déjà remis entre nos mains.

4.
Vers quelle joie nous conduis-tu,
Au-delà du Fils apparu,
Nuit de Noël et nuit de Pâques ?
Vers l'éternelle Eucharistie
Qui chante au sein du Dieu de vie.