Alma Redemptóris Mater,
quae pérvia caéli pórta mánes,
et stélla máris,
succúrre cadénti
súrgere qui cúrat pópulo :
Tu quae genuísti, natúra miránte,
túum sánctum Genitórem :
Virgo prius ac postérius,
Gabriélis ab óre
súmens íllud Ave,
peccatórum miserére

Sainte Mère du Rédempteur
Porte du ciel, toujours ouverte,
étoile de la mer
viens au secours du peuple qui tombe
et qui cherche à se relever.

Tu as enfanté,
ô merveille !
Celui qui t'a créée,
et tu demeures toujours Vierge.

Accueille le salut
de l'ange Gabriel
et prends pitié de nous, pécheurs.