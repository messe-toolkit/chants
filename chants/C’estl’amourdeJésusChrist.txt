1.
Tout homme est lourd du même poids,
Le poids que Dieu pèse à la Croix.
Tout homme vaut le même prix :
Le sang de Dieu qui vers Dieu crie.
Tous ont ainsi partie liée
À cette vie multipliée.

R.
C’EST L’AMOUR DE JÉSUS CHRIST
QUI NOUS POUSSE PLUS AVANT.

2.
Si de ce don je prends ma part,
Je ne peux plus vivre à l’écart.
Je ne veux plus garder pour moi
Mon pain rassis mon coeur étroit.
L’homme nouveau s’est avancé,
Un temps de paix a commencé.

3.
Toute fraîcheur pour apaiser,
Toute vigueur pour redresser.
Tout germe neuf au point du jour,
Tout lingot pur au creux du four,
(Sont) Sont dans la main ressuscitée
Ouverte sur l’éternité.

4.
Cette joie vient tout déranger,
Ce renouveau fait tout bouger.
Nous devenons ambassadeurs
Du seul pouvoir contre la peur
Qui s’étendra au monde entier
À l’univers réconcilié.