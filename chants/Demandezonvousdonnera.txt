Demandez on vous donnera ;
Cherchez et vous trouverez ;
Frappez, on vous ouvrira.

Qui demande reçoit et qui cherche trouvera,
A qui frappe on ouvrira

Qui d'entre vous, quand son fils lui demande du pain lui donne un caillou ?
Qui d'entre vous, quand son fils demande un poisson lui donne un serpent ?

Lorsque vous qui êtes mauvais donnez à vos enfants de bonnes choses,
Comment votre Père du ciel ne comblerait-il pas ceux qui le prient ?