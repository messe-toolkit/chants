1.
Voici ton jour qui vient,
Servante de l’Agneau,
Le jour de ton exode
Face aux ténèbres.

2.
Sur toi passe la nuit
D’angoisse et de douleurs :
Mais nul ne peut te prendre
Ton espérance.

3.
Pourquoi es-tu livrée
Aux mains des insensés ?
Jésus n’a de réponse
Que son offrande.

4.
Il joint à sa Passion
La mort de ses martyrs,
Ainsi, dans ton épreuve,
Grandit l’Église.

5.
Tu souffres pour son Corps
Il souffre dans le tien.
Le Christ déploie sa force
En ta faiblesse.

6.
Voici ton jour qui vient,
Servante de l’Agneau,
Tu montes vers ta Pâque,
Dans la lumière.