1.
Ils fabriquent des voitures ou des avions,
Elles inventent des histoires ou des chansons.
Ils protègent les montagnes et les forêts,
Toi mon Dieu, tu les inspires en secret !

TU ENVOIES TON ESPRIT
SUR LES HOMMES ET LES FEMMES
DONT LE COEUR TE RÉCLAME,
ILS TE DISENT MERCI, MERCI !

2.
Elles dessinent des immeubles ou des jardins,
Ils découvrent des planètes ou des parfums.
Elles affrontent l’océan ou les sommets,
Toi mon Dieu, tu les inspires en secret !

3.
Ils soulagent les chagrins par le sourire,
Elles font naître des bébés pour l’Avenir.
Tous ensemble ils font des rêves et des projets,
Toi mon Dieu, tu les inspires en secret !