1.
Vers la montagne de Sion
Nous avançons en pèlerins de la lumière.
Dans la Cité du Dieu vivant
Nous recherchons la joie promise à notre terre :

R.
Jérusalem illuminée par le Seigneur,
En toi des milliers d’anges sont en fête !
Jérusalem illuminée par le Seigneur,
En toi nous célébrons le Dieu fidèle.

2.
Vers l’assemblée des premiers-nés
Nous cheminons, marqués du Souffle qui fait vivre.
Dans la Cité du Dieu vivant
Que notre nom demeure écrit dans le grand Livre !

3.
En Jésus Christ ressuscité
Nous accueillons l’alliance offerte à tous les hommes.
Dans la Cité du Dieu vivant
Un peuple immense est rassemblé par sa parole.

4.
Chacun de nous est l’invité
Pour qui le Maître a su choisir l’unique place.
Dans la Cité du Dieu vivant
Voici les pauvres et les pécheurs qui rendent grâce.