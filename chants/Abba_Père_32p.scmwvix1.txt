1.
Bien avant le chant qui créa l’univers
Bien avant l’Esprit qui planait sur la Terre
Bien avant que tu me formes de la poussière
Tu rêvais du jour ou Tu pourrais m’aimer
\n
Bien avant les premiers
Battements de mon cœur
Bien avant que je m’éveille à Ta douceur
Bien avant mes doutes,
Mes joies et mes douleurs
Tu rêvais du jour ou je pourrais T’aimer

R.
Abba Père, je suis à Toi
Abba Père, je suis à Toi
Abba Père, je suis à Toi
Abba Père, je suis à Toi

2.
Bien avant que Jésus marche sur la Terre
Bien avant le Fils qui nous montre le Père
Bien avant que les cieux
Sur moi soient ouverts
Tu rêvais du jour ou Tu pourrais m’aimer
\n
Bien avant que mon péché brise Ton cœur
Bien avant que coulent le sang et la sueur
Bien avant les clous, le froid, et la douleur
Tu rêvais du jour ou je pourrais T’aimer

P.
Abba Père, je suis émerveillé
Saisi par l’immensité de Ton amour pour moi
Abba Père, si grande est ta tendresse
Ton cœur est grand ouvert,
Et je viens plonger dans Tes bras