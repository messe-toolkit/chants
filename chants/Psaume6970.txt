Mon Dieu, viens me délivrer ;
Seigneur, viens vite à mon secours !

Qu’ils soient humiliés, déshonorés,
ceux qui s’en prennent à ma vie !
Qu’ils reculent, couverts de honte,
ceux qui cherchent mon malheur ;
que l’humiliation les écrase,
ceux qui me disent : “C’est bien fait !

Mais tu seras l’allégresse et la joie
de tous ceux qui te cherchent ;
toujours ils rediront : “Dieu est grand !
ceux qui aiment ton salut.

Je suis pauvre et malheureux,
mon Dieu, viens vite !
Tu es mon secours, mon libérateur :
Seigneur, ne tarde pas !

Doxologie
Rendons gloire au Père tout-puissant,
à son Fils Jésus Christ, le Seigneur,
à l’Esprit qui habite nos coeurs,
pour les siècles des siècles. Amen.