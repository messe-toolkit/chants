R.
Gloire à notre Dieu,
Gloire à son Saint Nom,
Gloire au Roi des rois,
Alléluia !

1.
Il est le Puissant,
Il est le Très-Haut,
Il est le Seigneur,
Il est la vie.

2.
Il est le Sauveur,
Le libérateur,
Il est la lumière,
Il est l´amour.

3.À lui la sagesse,
À lui la puissance,
À lui la victoire,
À lui la force.

4.À lui la grandeur
Et la majesté,
À lui tout honneur
Et toute gloire.