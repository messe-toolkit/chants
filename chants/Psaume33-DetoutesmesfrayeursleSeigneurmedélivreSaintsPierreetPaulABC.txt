DE TOUTES MES FRAYEURS,
LE SEIGNEUR ME DÉLIVRE.

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Magnifiez avec moi le Seigneur,
exaltons tous ensemble son nom.
Je cherche le Seigneur, il me répond :
de toutes mes frayeurs, il me délivre.

3.
Qui regarde vers lui resplendira,
sans ombre ni trouble au visage.
Un pauvre crie ; le Seigneur entend :
il le sauve de toutes ses angoisses.

4.
L’ange du Seigneur campe alentour,
pour libérer ceux qui le craignent.
Goûtez et voyez : le Seigneur est bon !
Heureux qui trouve en lui son refuge !

ALLÉLUIA. ALLÉLUIA.

Tu es Pierre,
et sur cette pierre je bâtirai mon Église ;
et la puissance de la Mort ne l’emportera pas sur elle.