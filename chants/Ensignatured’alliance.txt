1.
Comme en ce premier matin
Où la terre fut créée,
Votre amour est un jardin.
Ref.Votre amour est un jardin.

Comme en ce premier matin
Où la terre fut donnée
Goût de miel et de rosée,
Ref.Votre amour est un jardin.

Comme en ce premier matin
Votre amour est un jardin
Qu’ensemence notre Dieu.
Ref. Votre amour est le jardin de notre Dieu.

Grand Refrain.
Alleluia ! Bénissons notre Dieu !
Vous prenez place en sa mémoire !
Alleluia ! Bénissons notre Dieu !
Il vous inscrit dans son histoire
En signature d’Alliance !

2.
Comme au cours de ce repas
Lors des noces de Cana
Votre amour est un festin.
Ref. Votre amour est un festin.

Comme au cours de ce repas
Premier signe qu’Il donna
Quand il changea l’eau en vin.
Ref. Votre amour est un festin.

Comme au jour de ce repas
Votre amour est un festin
Où s’invite notre Dieu.
Ref. Votre amour est le festin de notre Dieu.

3.
Comme au dernier rendez-vous
À la table où Il se tient
Votre amour a goût de pain.
Ref.Votre amour a goût de pain.

Comme au dernier rendez-vous
Où l’Alliance par son corps
Fut enfin renouvelée
Ref.Votre amour a goût de pain.

Comme au dernier rendez-vous
Votre amour a goût de pain
Dans les mains de notre Dieu.
Ref.Votre amour a goût de pain pour notre Dieu.

Grand Refrain.

4.
Comme en ce nouveau jardin
Où la mort ne peut plus rien
Votre amour est un soleil.
Ref.Votre amour est un soleil.

Comme en ce nouveau jardin
Où jaillit l’Arbre de vie
De sa Pâque illuminé.
Ref.Votre amour est un soleil.

Comme en ce nouveau jardin
Votre amour est un soleil
Tout brûlant au coeur de Dieu.
Ref. Votre amour est un soleil au coeur de Dieu.

Grand Refrain.

5.
Comme au soir des pèlerins
S’en allant vers Emmaüs
Votre amour est un chemin.
Ref. Votre amour est un chemin.

Comme au soir des pèlerins
Éblouis par les accents
Des paroles du Passant.
Ref. Votre amour est un chemin.

Comme au soir des pèlerins
Votre amour est un chemin.
Pour les pas de notre Dieu.
Ref. Votre amour est un chemin pour notre Dieu.

Grand Refrain.
Alleluia ! Bénissons notre Dieu !
Vous prenez place en sa mémoire !
Alleluia ! Bénissons notre Dieu !
Il vous inscrit dans son histoire
En signature d’Alliance !