JÉRUSALEM! JÉRUSALEM !
COMBIEN DE FOIS
J’AI VOULU RASSEMBLER TES ENFANTS!

1 (Lun.)
Seigneur, nous avons péché devant toi,
et nous t’avons désobéi !
Nous n’avons pas écouté la voix de tes prophètes,
nous avons oublié tes chemins.

2 (Mar.)
Que ta colère se détourne de nous !
Nous ne sommes qu’un reste parmi les nations :
pour la gloire de ton nom délivre-nous, Seigneur,
et fais-nous miséricorde !

3 (Mer.)
Regarde, Seigneur, et tends l’oreille !
Dans les enfers les morts ne peuvent te louer ;
mais c’est un coeur brisé par la souffrance
qui te rend gloire et justice, Seigneur.

4 (Jeu.)
Accorde-nous, Seigneur, au coeur de notre exil,
de rentrer en nous-mêmes et connaître ton nom,
et donne-nous un coeur qui s’ouvre à ta présence,
et qui célèbre tes merveilles !

5 (Ven.)
Fais-nous revenir, Seigneur,
sur la terre que tu as donnée à nos pères ;
établis sur nous une alliance éternelle :
tu seras notre Dieu et nous serons ton peuple !

6 (Sam.)
Écoute, Seigneur, et prends pitié,
mets sur nos lèvres la louange de ton nom !
Ne te souviens pas des fautes de nos pères,
mais de ton amour et de ta tendresse pour nous !

7 (Dox.)
Ô Père des miséricordes,
Dieu de tendresse et de pardon,
convertis notre coeur par la croix de ton Fils
et par la force de ton Esprit !