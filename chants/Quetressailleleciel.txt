GLOIRE À DIEU DANS LE CIEL
ET PAIX SUR TERRE.

1.
Que tressaille le ciel
Et que chante la terre.
Un enfant nous est né,
Un Fils nous est donné.
Gloire à Dieu dans le ciel
Et paix sur terre.

2.
Un enfant nous est né
Un Fils nous est donné
C’est le Fils de Marie
Et le fruit de son "Oui".
Gloire à Dieu dans le ciel
Et paix sur terre.

3.
C’est le Fils de Marie
Et le fruit de son "Oui".
Il est Dieu, le Seigneur,
Le Messie, le Sauveur.
Gloire à Dieu dans le ciel
Et paix sur terre.

4.
Il est Dieu, le Seigneur,
Le Messie, le Sauveur.
Tous les hommes verront
Qu’ils sont aimés de Lui.
Gloire à Dieu dans le ciel
Et paix sur terre.