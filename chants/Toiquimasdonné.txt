R.
Toi qui m'as donné tout ce qu'on peut donner,
Fais lever ton Jour dans mon amour,
Fais lever ton Jour dans mon amour.

1.
J'ai vu des yeux qui pleurent,
Devant eux mon amour s'est fermé.
Viens, Seigneur, réveiller mes yeux,
Viens, Seigneur, réveiller mes yeux.

2.
J'ai peur des mains qui tremblent,
Devant elles mon amour est muet.
Viens, Seigneur, libérer mes mains,
Viens, Seigneur, libérer mes mains.

3.
J'ai mal aux coeurs qui souffrent,
Devant eux mon amour n'a rien fait.
Viens, Seigneur, ajuster mon coeur,
Viens, Seigneur, ajuster mon coeur.