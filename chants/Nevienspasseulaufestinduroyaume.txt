STANCE
Ne viens pas seul au festin du Royaume ;
sur tes chemins, crie la nouvelle :
« La promesse est un pain
donné pour le partage ;
Dieu vous presse d’entrer
dans l’unique demeure
et vous rassemble dans l’amour. »

R.
Jésus, espérance des hommes,
fais de nous les témoins de ton salut.

VERSETS

1.
Je suis venu apporter le feu sur la terre,
je veux qu’il se répande et qu’il brûle.

2.
Je suis venu pour que tous aient la vie
et la reçoivent en plénitude.

3.
Je suis venu chercher ce qui était perdu,
non pas en Juge mais en Sauveur.