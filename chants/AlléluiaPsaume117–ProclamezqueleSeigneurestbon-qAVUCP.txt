R.
Alléluia, alléluia, alléluia (bis)

1.
Proclamez que le Seigneur est bon,
éternel est son amour !
Que le dise la maison d’Israël,
éternel est son amour !

2.
Le Seigneur est ma force et mon chant,
le Seigneur est mon salut.
Je ne mourrai pas, non je vivrai,
je dirai l’œuvre de Dieu.

3.
Dans l’angoisse j’ai crié vers lui,
le Seigneur m’a exaucé.
Le Seigneur est là pour me défendre,
j’ai bravé mes ennemis.

4.
Oui, c’est toi mon Dieu, je te rends grâce,
Seigneur mon Dieu, je t’exalte.
Proclamez que le Seigneur est bon,
éternel est son amour !