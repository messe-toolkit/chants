1.
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Il veut nous mener
Au cœur du silence où il parle
Dans le désert de plénitude,
Dieu veut se révéler.
Dans le désert de plénitude,
Dieu veut se révéler.

2.
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Il veut nous mener
Vers les sources vives de la vie.
Dans le désert de plénitude
Dieu veut nous abreuver.
Dans le désert de plénitude
Dieu veut nous abreuver.

3.
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Il veut nous mener
Au lieu du festin et de la fête
Dans le désert de plénitude
Dieu nous dit son pardon.
Dans le désert de plénitude
Dieu nous dit son pardon.

4.
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Dans quel désert de plénitude
Dieu veut-il nous mener ?
Il veut nous mener
Au festin des noces éternelles.
Dans le désert de plénitude
Dieu nous donne son pain.
Dans le désert de plénitude
Dieu nous donne son pain.