ÉCOUTONS LA PAROLE
PAROLE DE DIEU
TOUTE IMPRÉGNÉE D’AMOUR
VIVANTE LA PAROLE
PAROLE QU’ON PEUT
VIVRE AU JOUR LE JOUR

1.
Le grain est tombé au bord du chemin
Les passants l’ont piétiné
Le mal survient et bloque le coeur
La Parole est refusée.

2.
Le grain est tombé au milieu des pierres
Il s’est desséché
La joie hier, l’épreuve aujourd’hui
La Parole a succombé.

3.
Le grain est tombé au milieu des ronces
Elles l’ont étouffé
La vie s’enfonce, plaisirs et soucis
La Parole est asphyxiée.

4.
Le grain est tombé dans la bonne terre
Il a porté tant de fruits
Qui persévère dans un coeur aimant
Garde la Parole en vie.

5.
Dans chaque humain il y a toujours
Du terreau et des rocailles
Chacun de nous reçoit par Amour
Du bon grain pour les semailles.