POUR TA GLOIRE ON PARLE DE TOI, VILLE DE DIEU!

1.
Elle est fondée sur les montagnes saintes.
Le Seigneur aime les portes de Sion
plus que toutes les demeures de Jacob.

2.
Pour ta gloire, on parle de toi, ville de Dieu !
Mais on appelle Sion/ « Ma mère »
car en elle, tout homme est né.
C’est lui, le Très-Haut, qui la maintient.

3.
Au registre des peuples, le Seigneur écrit :
« Chacun est né là-bas ».
Tous ensemble ils dansent et ils chantent :
« En toi, toutes nos sources » !