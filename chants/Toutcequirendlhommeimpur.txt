Antienne - stance
Tout ce qui rend l'homme impur vient du cœur de l'homme.

R.
Dominus Spiritus est,
Spiritus autem vivificat,
Spiritus autem vivificat.

Versets
Ps 18B 8-10

1.
La loi du Seigneur est parfaite,
Qui redonne vie ;
La charte du Seigneur est sûre,
Qui rend sages les simples.

2.
Les préceptes du Seigneur sont droits,
Ils réjouissent le cœur ;
Le commandement du Seigneur est limpide,
Il clarifie le regard.

3.
La crainte qu'il inspire est pure,
Elle est là pour toujours ;
Les décisions du Seigneur sont justes
Et vraiment équitables.

Ou bien

Ps 94, 1-9

1.
Venez, crions de joie pour le Seigneur,
Acclamons notre Rocher, notre salut !
Allons jusqu'à lui en rendant grâce,
Par nos hymnes de fête acclamons-le !

2.
Oui, le grand Dieu, c'est le Seigneur,
Le grand roi au-dessus de tous les dieux :
Il tient en main les profondeurs de la terre,
Et les sommets des montagnes sont à lui ;

3.
À lui la mer, c'est lui qui l'a faite,
Et les terres, car ses mains les ont pétries.
Entrez, inclinez-vous, prosternez-vous,
Adorons le Seigneur qui nous a faits.

4.
Aujourd'hui écouterez-vous sa parole ?
« Ne fermez pas votre cœur comme au désert,
Où vos pères m'ont tenté et provoqué,
Et pourtant ils avaient vu mon exploit. »