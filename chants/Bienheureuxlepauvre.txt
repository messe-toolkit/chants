R.
BIENHEUREUX, BIENHEUREUX,
BIENHEUREUX, BIENHEUREUX.

1.
Bienheureux le pauvre au seuil des festins :
Les palais de Dieu lui sont fraternels !
Bienheureux le monde où l’argent n’est rien :
Les trésors de Dieu seront éternels !

2.
Bienheureux les yeux qui cherchent le jour :
La splendeur de Dieu demain brillera !
Bienheureux le coeur assoiffé d’amour :
L’océan de Dieu pour lui jaillira !

3.
Bienheureux les cris au fond des prisons :
La Cité de Dieu résonne de joie !
Bienheureux le sang des martyrs sans nom :
Le jardin de Dieu fleurit de leur foi !

4.
Bienheureux les bras ouverts au pardon :
La bonté de Dieu reçoit leur espoir !
Bienheureux l’enfant dans son abandon :
Car la main de Dieu ne peut décevoir !

5.
Bienheureux le nom du juste opprimé :
La pitié de Dieu sera sans défaut !
Bienheureux le corps qui n’a pas compté :
Car l’amour de Dieu veille à son repos !