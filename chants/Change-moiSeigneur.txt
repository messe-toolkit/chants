CHANGE-MOI, SEIGNEUR,
OUVRE-MOI LES YEUX.
CHANGE-MOI, SEIGNEUR,
RANIME LE FEU.

1.
Que ma vie se transforme
Tout au long de ces jours
Ton Amour prendra forme
Et je verrai le jour.

2.

Que je sache reconnaître
Mon orgueil, mon péché
Tu m’appelles à renaître
Que je sois pardonné !

3.
Que j’apprenne à me taire
pour t’entendre parler
Mon silence est prière
Si je sais t’écouter.

4.
Que je marche à ta suite
Les pieds dans la poussière
Aujourd’hui tu m’invites
À partir au désert.