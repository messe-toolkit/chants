APPELÉS POUR BÂTIR LE ROYAUME,
NOUS MARCHONS GUIDÉS PAR TON ESPRIT,
INVITÉS À SERVIR TOUS LES HOMMES,
NOUS CHANTONS LA FORCE DE L’AMOUR.

1.
Chercheurs des traces du Royaume,
Vous travaillez à sa moisson,
Et son Royaume se construit.
Car son amour habite notre terre.
Chercheurs des traces du Royaume,
Vous fécondez par la bonté
Le grain qu’il a déjà semé,
Et son Royaume s’agrandit !
Car son amour travaille dans nos vies.
Chercheurs des traces du Royaume,
Dans les labeurs où vous peinez,
Chantez la joie d’être envoyés !
En vous (nous) l’Amour est le levain
Qui fait monter en un sel pain
L’humanité.
Chantons la joie d’être envoyés !

2.
Passeurs des signes du Royaume,
Vous appelez pour son repas,
Et son Royaume se construit.
Car son amour habite notre terre.
Passeurs des signes du Royaume,
La table est mise par vos mains,
Les pauvres sont rois du festin,
Et son Royaume s’agrandit !
Car son amour travaille dans nos vies.
Passeurs des signes du Royaume,
Face aux puissances de l’argent,
Chantez la joie d’être envoyés !
L’amour vous (nous) place en serviteurs
Pour que s’approche du banquet
L’humanité.
Chantons la joie d’être envoyés !

3.
Pêcheurs aux rives du Royaume,
Vous ramenez ses pleins filets,
Et son Royaume se construit.
Car son amour habite notre terre.
Pêcheurs aux rives du Royaume,
En le voyant ressuscité,
C’est dans ses pas que vous marchez,
Et son Royaume s’agrandit !
Car son amour travaille dans nos vies.
Pêcheurs aux rives du Royaume,
Dans les tempêtes de ce temps,
Chantez la joie d’être envoyés !
Et son amour vous(nous) pousse au loin,
Pour qu’en son corps soit rassemblée
L’humanité.
Chantons la joie d’être envoyés !

4.
Chanteurs aux vignes du Royaume,
Vous entonnez son chant de paix,
Et son Royaume se construit.
Car son amour habite notre terre.
Chanteurs aux vignes du Royaume,
Vous accordez d’un même élan
Tous les sarments au cep vivant,
Et son Royaume s’agrandit !
Car son amour travaille dans nos vies.
Chanteurs aux vignes du Royaume,
Aux nuits de haine et de mépris,
Chantez la joie d’être envoyés !
Et son amour vient réjouir
Du chant béni des bienheureux
L’humanité.
Chantons la joie d’être envoyés !

5.
Veilleurs aux portes du Royaume,
Vous attisez son feu nouveau,
Et son Royaume se construit.
Car son amour habite notre terre.
Veilleurs aux portes du Royaume,
Repose en vous l’Esprit d’en Haut
Pour vous guider dans les travaux,
Et son Royaume s’agrandit !
Car son amour travaille dans nos vies.
Veilleurs aux portes du Royaume,
Malgré les jours d’obscurité,
Chantez la joie d’être envoyés !
Et son amour chasse vos(nos) peurs
Pour qu’en son feu soit embrasée
L’humanité.
Chantons la joie d’être envoyés !