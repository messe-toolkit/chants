R.
Oui, le Seigneur est notre joie, alegria !
Il nous partage son amour, alegria !
Oui, le Seigneur est notre joie, alegria !
Il est fidèle pour toujours, alegria !

1.
Il est notre joie, alegria !
Il est notre paix, alegria !
Il est notre frère, alegria !
Il est la lumière, alegria !

2.
Il est le Sauveur, alegria !
Il est le pardon, alegria !
Il est le Seigneur, alegria !
Il est notre Dieu, alegria !