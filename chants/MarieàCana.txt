R.
FAITES TOUT CE QU’IL VOUS DIRA
NOUS A DIT MARIE À CANA.

1.
Il nous dira qu’il avait faim
Qu’on avait brisé son enfance
Qu’il ne demandait pas du pain
Mais qu’on lui fasse un peu confiance.

2.
Il nous dira qu’il avait froid
Qu’il frappe encore à notre porte,
Qu’il a le coeur en désarroi
Mais on lui répond qu’il s’en sorte.

3.
Il nous dira qu’il était nu
Comme une terre après la guerre
Que l’espoir n’est jamais venu
Et qu’aujourd’hui n’en a plus guère.

4.
Il nous dira qu’il était vieux
Oublié au fond d’un hospice
Que le monde est bien silencieux
Et qu’il attend que ça finisse.

5.
Il nous dira que c’était lui
A l’hôpital ou en naufrage
En prison hier ou aujourd’hui
Car l’autre est toujours son visage.