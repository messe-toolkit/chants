JE VIENS DE COMMUNIER,
TU ES AU FOND DE MOI,
CE MOMENT TOUT ENTIER.
JE VEUX TE LE DONNER.

1.
Je t’offre un silence
Aussi simplement,
Je t’offre un silence
Tu es là présent.

2.
Reçois un merci
Aussi simplement,
Reçois un merci
Tu es là présent.

3.
Je veux tout t’offrir
Aussi simplement,
Je veux tout t’offrir
Tu es là présent.

4.
Et puis Te parler
Aussi simplement,
Et puis Te parler
Tu es là présent.