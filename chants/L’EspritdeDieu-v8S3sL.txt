R.
L’Esprit de Dieu repose sur moi,
L’Esprit de Dieu m’a consacré,
L’Esprit de Dieu m’a envoyé proclamer la paix, la joie.

1.
L’Esprit de Dieu m’a choisi
Pour étendre le Règne du Christ parmi les nations,
Pour proclamer la Bonne Nouvelle à ses pauvres.
J’exulte de joie en Dieu, mon Sauveur !

2.
L’Esprit de Dieu m’a choisi
Pour étendre le Règne du Christ parmi les nations,
Pour consoler les coeurs accablés de souffrance.
J’exulte de joie en Dieu, mon Sauveur !

3.
L’Esprit de Dieu m’a choisi
Pour étendre le Règne du Christ parmi les nations,
Pour accueillir le Pauvre qui pleure et qui peine.
J’exulte de joie en Dieu, mon Sauveur !

4.
L’Esprit de Dieu m’a choisi
Pour étendre le Règne du Christ parmi les nations,
Pour annoncer la grâce de la délivrance.
J’exulte de joie en Dieu, mon Sauveur !