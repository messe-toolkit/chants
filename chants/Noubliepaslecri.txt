R.
N'oublie pas Seigneur, n'oublie pas le cri des malheureux!
N'oublie pas Seigneur, n'oublie pas le cri des malheureux!

1.
Je t'adresse ma prière,
Seigneur réponds-moi,
Par ta force et ta puissance,
Tu peux me sauver.

2.
Ton amour est là sans cesse,
Tu viens me sauver.
Ne me cache pas ta face,
Je compte sur toi.

3.
Me voici, pêcheur et faible,
Ta main me soutient.
Je t'exalte et te rends grâce,
Pour tous tes bienfaits.