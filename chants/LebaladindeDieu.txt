1.
Toi, le baladin de Dieu
Tu es venu ce soir
Marcher sur notre route...
Mille éclairs au fond des yeux,
Du soleil plein ton coeur,
Tu veux chasser nos doutes ?
Et la route est si longue...
On a du mal à s'engager...
Et pourtant dans le monde
Il y a d'l'amour à partager...
Il faut donner du pain
A ceux qui n'ont de quoi manger,
Partager un sourire
Quand le bonheur est oublié

2.
Toi, dans ta marche en avant,
Tu nous as dit comment
On rencontre nos frères
Et les phrases de tes chants,
Les visages d'enfants,
sont comme une prière
Et le monde est si grand...
On a du mal à regarder
Tous ces gens qui ont peur
Dans ces pays, juste à côté...
Il faut donner son coeur
A ceux qui sont désespérés,
Et parler de Jésus
Quand ils ont soif de liberté

3.
Toi, le copain de Jésus,
Merci d'être venu
Eclairer notre marche
Tu laisses au fond de nos coeurs
Comme un très grand bonheur
Qu'il faut chanter encore !
Et le chant est si beau...
On a du mal à commencer...
Mais quand on est parti,
On ne peut jamais s'arrêter...
Il faut donner sa voix
Pour ceux qui n'osent plus parler,
Et chanter son amour
Pour ceux qui sont abandonnés

4.
Toi, le baladin de Dieu,
Tu t'en iras demain
Marcher sur d'autres routes...
Tu emportes au fond des yeux
La joie de notre coeur
Et de l'amour, sans doute…

CODA
Toi, le baladin de Dieu,
Dis-toi que sur ta route
Tu ne chantes plus seul !