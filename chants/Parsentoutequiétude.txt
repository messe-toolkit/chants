PARS EN TOUTE QUIÉTUDE
PARS EN TOUTE SÉRÉNITÉ
CAR TU AS LA CERTITUDE
D’AVOIR TON SEIGNEUR À TES COTÉS.

1.
Mais c’est pourtant vrai qu’il en coûte
De tout laisser, de tout quitter
Aussi vrai que prendre la route
Est la seule façon de pouvoir se croiser.

2.
Où donc est ta terre promise ?
Où sont tes terres désertées ?
Comment s’appelle ton Moïse ?
Celui qui va t’aider à faire la traversée.

3.
La route est le pays de l’autre
Nous suivons le même chemin
Si je suis l’un, tu deviens l’autre
Tu me regardes enfin et je suis ton prochain.

4.
Sur les chemins de l’évangile
Nous ne voulons rien posséder
Marcher nous semble plus facile
Frères de pauvreté, sans attaches à nos pieds.