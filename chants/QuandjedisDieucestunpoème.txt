R.
QUAND JE TE DIS DIEU C'EST UN POEME,
C'EST UNE ETOILE DANS MA VIE,
UN FEU QUI BRULE DANS MES VEINES,
UN GRAND SOLEIL POUR AUJOURD'HUI !

1.
Mon Dieu est source de lumière, Amour unique et partagé ;
tous ses chemins sont vérité, mon coeur a soif de les connaître.

2.
Mon Dieu est loin des évidences malgré le chant de l'univers.
Combien de nuits, combien d'hivers avant de naître à l'espérance ?

3.
Mon Dieu fait route avec les hommes, il vient s'asseoir dans nos maisons
Par lui s'éclaire l'horizon, ses pas nous mènent vers l'aurore.

4.
Christ est plus fort que nos barrières avec son coeur d'humanité ;
quand il nous dit le mot de paix nous entendons "mort à la guerre".