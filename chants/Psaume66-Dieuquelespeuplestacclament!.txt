DIEU, QUE LES PEUPLES T’ACCLAMENT!
QU’ILS T’ACCLAMENT, TOUS ENSEMBLE!

1.
Que ton visage s’illumine pour nous ;
et ton chemin sera connu sur la terre,
ton salut, parmi toutes les nations.

2.
Que les nations chantent leur joie,
car tu gouvernes le monde avec justice ;
sur la terre, tu conduis les nations.

3.
Dieu, notre Dieu nous bénit.
Que Dieu nous bénisse,
et que la terre tout entière l’adore !