R.
Père, je te bénis :
Le mystère caché aux sages et aux savants tu l'as révélé aux petits.

1.
Laissez venir à moi les petits enfants :
Le Royaume des cieux est à eux et à ceux qui leur ressemblent.

2.
Nul, s'il ne renaît de l'eau et de l'Esprit,
N'entrera dans le Royaume des cieux.

3.
Gloire au Père et au Fils et au Saint-Esprit.