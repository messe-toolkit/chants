D’UN MÊME COEUR, LOUONS LE SEIGNEUR,
GLOIRE À DIEU, NOTRE PÈRE TOUT-PUISSANT
PAR JÉSUS, LE FILS BIEN-AIMÉ,
DANS L’ESPRIT SAINT QUI NOUS ANIME !

1.
Dieu nous a aimés, il s’est donné lui-même
En son Fils, Jésus, le Sauveur promis.
Mère du Seigneur, Marie comblée de grâce,
Que ton oui d’amour nous habite à jamais !

2.
Fils du Dieu vivant, Jésus, tu nous révèles
L’éternel amour qui nous donne vie.
De ton coeur brûlant, s’élève la louange
Vers le Créateur, notre Père des cieux.

3.
Toi, le Pain de vie, Jésus, tu nous partages
Le grand feu d’amour de ton coeur brûlant.
Donne-nous l’Esprit, et nous ferons mémoire
De ton Corps livré, de ton Sang répandu.

4.
Toi, le Bon Berger, Jésus, tu nous rassembles
Dans l’immense amour de ton coeur brûlant.
Garde en nous la foi, suscite en ton Église
Un élan nouveau, des chemins d’unité.

5.
Toi, notre Chemin, Jésus, tu nous enseignes
Les secrets d’amour de ton coeur brûlant.
Viens revivre en nous les pages d’Évangile
Où tu nous apprends le pardon et la paix.

6.
Toi, notre Salut, Jésus, tu nous abreuves
Au torrent d’amour de ton coeur brûlant.
Prends notre aujourd’hui, nos joies et nos souffrances ;
Tout portera fruit dans la Vigne de Dieu.

7.
Fils du Dieu vivant, Jésus, tu nous entraînes,
Dans le chant d’amour de ton coeur brûlant.
Fais que notre vie proclame les merveilles
Du Dieu trois fois saint qui nous aime à jamais.