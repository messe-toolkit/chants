Seigneur Jésus, envoyé par le Père
Pour guérir et sauver tous les hommes,
Prends pitié de nous.
Prends pitié de nous.

Ô Christ, venu dans le monde
Appeler tous les pécheurs,
Prends pitié de nous.
Prends pitié de nous.

Seigneur, élevé dans la gloire du Père
Où tu intercèdes pour nous,
Prends pitié de nous.
Prends pitié de nous.