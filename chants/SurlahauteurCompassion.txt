1.
Sur la hauteur, près de la ville,
Lorsque s’éloigne la rumeur,
Trois croix déchirent le ciel vide,
Sourd aux prières des maudits.

2.
Quand le soleil déjà s’incline
Devant son Dieu défiguré,
La Vierge ploie sous la souffrance,
Avec l’enfant qu’elle a porté.

3.
Vers l’orient l’ombre s’allonge,
Jean et Marie lèvent les yeux :
Un soldat ouvre de sa lance
Le coeur offert du Fils de Dieu…

4.
…Sur la hauteur, près de la ville,
Sous l’arbre où dort l’Homme nouveau,
Dieu a créé la nouvelle Ève,
L’Église née de son côté.