1.
Voici que resplendit
La clarté de la Pâque :
Tout jubile, tout éclate d’allégresse ;
La terre se joint au ciel
Pour célébrer le Sauveur en sa victoire.

2.
La mort n’est plus la mort,
Notre Dieu ressuscite,
Les enfers n’ont pu tenir en sa présence,
Les portes du paradis,
Jadis fermées, en ce jour nous sont ouvertes.

3.
Ô jour de sainte joie,
Radieux de lumière,
Don du Verbe, vie de Dieu pour tous les hommes,
Ô Pâque d’éternité,
Tu nous convies à la table du Royaume !

4.
Que nul ne reste seul,
Accablé de tristesse,
Que résonne jusqu’au soir l’hymne nouvelle,
Qu’exulte le monde entier :
Christ est vivant, son amour a fait merveille !

5.
Jésus, nous te chantons,
Toi qui vis pour les siècles,
Toi qui rends la vraie louange à notre terre.
L’Église, renouvelée,
Dans l’Esprit Saint, glorifie le nom du Père.