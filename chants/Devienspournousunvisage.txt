R.
CESSE ENFIN D'ETRE UN MOT
DEVIENS POUR NOUS UN VISAGE.

1.
Combien de temps, Seigneur, vas-tu m'oublier ?
Combien de temps, me cacher ton visage ?
Combien de temps aurais-je l'âme en peine ?
Et chaque jour le coeur attristé ?

2.
Regarde, réponds-moi, Seigneur mon Dieu !
Laisse à mes yeux la lumière.
Garde moi du sommeil de la mort,
Que mes rivaux ne jouissent pas de ma chute.

3.
Moi, je suis sûr de ton amour,
Mon coeur est dans la joie car tu me sauves
Je veux chanter au Seigneur
Tout le bien qu'il m'a fait