JE CROIS QU’IL SE TIENT À MA PORTE
SERAIT-CE LUI ? JE L’ATTENDRAI
JE CROIS SAVOIR CE QU’IL M’APPORTE
OUI, C’EST BIEN LUI, IL M’ATTENDAIT.

1.
Je Lui confierai tous mes rêves
Toutes les clefs de ma maison
Il m’apprendra ce qui élève
Je lui chanterai mes chansons.

2.
Je sais bien, qu’il n’y a pas d’heure
Il peut venir quand il voudra
Et je ferai de ma demeure
Un lieu où Il habitera.

3.
Avec Lui je prendrai la route.
Et je tiendrai si fort sa main,
J’arriverai quoiqu’il en coûte
Jusqu’au sommet, jusqu’au matin.