Gloria in excélsis Deo !
Et in terra pax hominibus bonae voluntatis.
Laudamus te, Benedicimus te, Adoramus te.
Glorificamus te. Gratias agimus tibi propter magnam gloriam tuam.
Domine Deus, Rex cælestis, Deus Pater omnipotens.
Domine Fili unigenite, Jesu Christe.
Domine Deus, Agnus Dei, Filius Patris.
Qui tollis peccata mundi, miserere nobis.
Qui tollis peccata mundi, suscipe depracationem nostram.
Qui sedes ad dexteram Patris, miserere nobis.
Quoniam tu solus Sanctus. Tu solus Dominus.
Tu solus Altissimus, Jesu Christe.
Cum Sancto Spiritu, in gloria Dei Patris. Amen.