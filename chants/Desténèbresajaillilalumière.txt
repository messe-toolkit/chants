1.
Des ténèbres a jailli la lumière :
Tu créais nos nuits et nos jours.
Du plus noir de l’abîme pascal
A surgi le vainqueur de la mort.

Béni sois-tu
Pour le premier des jours,
Car tout cela est bon.

2.
Dans l’espace, tu lances la terre
Où ruissellent les pluies et les sources.
Dans les eaux, foisonne la vie :
D’un baptême naît l’homme nouveau.

Béni sois-tu
Pour le deuxième jour,
Car tout cela est bon.

3.
Les collines se couvrent de blé,
Les coteaux se parent de vignes ;
Ils annoncent le Pain de la Vie
Et le vin d’une Joie immortelle.

Béni sois-tu
Pour le troisième jour,
Car tout cela est bon.

4.
Dans le ciel, le soleil et la lune
Marqueront vos années et vos jours.
Dans nos vies, Ta naissance et Ta mort
Ont signé notre siècle à venir.

Béni sois-tu
Pour le quatrième jour,
Car tout cela est bon.

5.
Ils animent, ils enchantent, ils colorent
Nos forêts, nos rivières et nos champs.
Emplissez nos cités de vos hymnes,
Nouveau-nés dans la foi de l’Église !

Béni sois-tu
Pour le cinquième jour,
Car tout cela est bon.

6.
Tu as dit : Faisons l’homme et la femme !
A l’image de Dieu, tu les fis.
Tu as dit : l’Époux est parmi vous !
Pare-toi, fiancée, pour tes noces !

Béni sois-tu
Pour le sixième jour,
Car tout cela est bon.