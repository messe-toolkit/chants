1.
Soyons dans la joie, c’est la fête des lumières.
Marie présente au Temple Jésus son enfant.
Jésus nous dira souvent : “Je suis la lumière du monde”.

SOYONS DANS LA JOIE,
SOYONS DANS LA JOIE,
SOYONS DANS LA JOIE,
SOYONS DANS LA JOIE.

2.
Rendons gloire à Dieu ! Syméon est exaucé,
Il attendait depuis longtemps la consolation d’Israël.
Syméon prit l’enfant dans ses bras et rendit gloire à Dieu !

RENDONS GLOIRE À DIEU,
RENDONS GLOIRE À DIEU,
RENDONS GLOIRE À DIEU,
RENDONS GLOIRE À DIEU!

3.
C’est la fête des lumières pour tous les baptisés.
Livrons-nous sans réserve à Jésus qui nous sauve,
Envoyé par le Père, il est venu nous donner la vie.

SOYONS DANS LA JOIE ET RENDONS GLOIRE À DIEU !
SOYONS DANS LA JOIE ET RENDONS GLOIRE À DIEU !
SOYONS DANS LA JOIE ET RENDONS GLOIRE À DIEU !
SOYONS DANS LA JOIE ET RENDONS GLOIRE À DIEU !