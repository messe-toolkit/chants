Refrain :
Ô Marie, souviens-toi de nous,
Puisque tu es dans son Royaume !
Ô Marie, souviens-toi de nous,
Puisque tu es au coeur des hommes !

1.
De tous ceux qui n'ont plus de vin,
Ni plus de pain, ni plus personne ;
De tous ceux qui cherchent sans fin,
Quand le destin les emprisonne.

2.
De tous ceux qui ne croient en rien,
Trop accablés par la malchance ;
Ceux pour qui tu es bien trop loin,
Brebis perdues dans l'ignorance.

3.
De tous ceux qui luttent en vain
Pour chercher encore un passage,
Ceux pour qui la lampe s'éteint,
Qui n'entendront plus ton message.

4.
De tous ceux pour qui c'est la fin,
Sarments séchés, figuiers stériles,
Ceux qui cherchent le vrai chemin,
Prodigues perdus dans la ville.