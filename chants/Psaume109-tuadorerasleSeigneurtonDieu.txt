Il régnera sur la maison de David et son règne n'aura pas de fin.

Oracle du Seigneur à mon seigneur :
« Siège à ma droite,
Et je ferai de tes ennemis
Le marchepied de ton trône »

De Sion, le Seigneur te présente
Le sceptre de ta force :
« Domine jusqu'au cœur
De l'ennemi. »

Le jour où paraît ta puissance,
Tu es prince, éblouissant de sainteté :
« Comme la rosée qui naît de l'aurore,
Je t'ai engendré. »

Le Seigneur l'a juré
Dans un serment irrévocable
« Tu es prêtre à jamais
Selon l'ordre du roi Melkisédek. »

À ta droite se tient le Seigneur :
Il brise les rois au jour de sa colère.
Au torrent il s'abreuve en chemin,
C'est pourquoi il redresse la tête.

Rendons gloire au Père tout puissant,
À son fils Jésus-Christ le Seigneur,
À l'Esprit qui habite en nos cœurs,
Pour les siècles des siècles. Amen