Gestes d’offrande

BÉNI SOIT DIEU, MAINTENANT ET TOUJOURS!

Doxologie

Par lui, avec lui et en lui,
AMEN!

À toi, Dieu le Père tout-puissant, dans l’unité du Saint-Esprit,
AMEN!

Tout honneur et toute gloire,
pour les siècles des siècles. AMEN!