R.
Éternellement heureux !
Éternellement heureux !
Dans son Royaume !

1.
Ils sont nombreux les bienheureux
Qui n'ont jamais fait parler d'eux
Et qui n'ont pas laissé d'image
Tous ceux qui ont, depuis des âges,
Aimé sans cesse et de leur mieux
Autant leurs frères que leur Dieu

2.
Ceux dont on ne dit pas un mot,
Ces bienheureux de l'humble classe,
Ceux qui n'ont pas fait de miracle
Ceux qui n'ont jamais eu d´extase
Et qui n'ont laissé d´autre trace
Qu'un coin de terre ou un berceau

3.
Ils sont nombreux, ces gens de rien,
Ces bienheureux du quotidien
Qui n'entreront pas dans l'histoire
Ceux qui ont travaillé sans gloire
Et qui se sont usé les mains
A pétrir, à gagner le pain

4.
Ils ont leurs noms sur tant de pierres,
Et quelquefois dans nos prières
Mais ils sont dans le cœur de Dieu !
Et quand l'un d'eux quitte la terre
Pour gagner la maison du Père,
Une étoile naît dans les cieux...