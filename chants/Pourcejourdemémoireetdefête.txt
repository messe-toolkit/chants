GLORIA, GLORIA, GLORIA TIBI DOMINE!
GLORIA, GLORIA, GLORIA TIBI DOMINE!

1.
Pour ce jour de mémoire et de fête
Pour la joie d’annoncer tes merveilles
Béni sois-tu Seigneur !
Pour l’Esprit appelant les croyants
À marcher sur les traces des saints
Béni sois-tu !

2.
Pour l’Église au-delà des frontières
Pour la route à nos pas familière
Béni sois-tu Seigneur !
Pour nos voix ne formant qu’un seul chant
Et les mots que les saints nous ont dits
Béni sois-tu !

3.
Pour la foi triomphant de l’épreuve,
La semence où la vie est à l’oeuvre
Béni sois-tu Seigneur !
Pour les saints qui proclament “Dieu seul”
Et leur coeur attentif à chacun.
Béni sois-tu !

4.
Pour Jésus qui se joint aux disciples
Son appel à porter l’évangile
Béni sois-tu Seigneur !
Pour les saints qui témoignent de lui
Et repoussent le doute et la peur
Béni sois-tu !

5.
Pour celui que ta grâce relève
Le pécheur découvrant ta tendresse
Béni sois-tu Seigneur !
Pour les saints qui connaissent ton coeur
Et reflètent sur nous ta bonté
Béni sois-tu !