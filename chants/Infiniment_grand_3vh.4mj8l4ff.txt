1.
Quand je sonde l'univers,
Les astres que tu as créés,
Les océans, les mers,
Tout me révèle ta majesté.

R.
Alors mon âme veut s'écrier,
De tout mon cœur je veux chanter.
Alors mon âme veut s'écrier,
De tout mon cœur je veux chanter.
\n
Ô Dieu tu es infiniment grand,
Éternel puissant, Roi majestueux.
Ô Dieu tu es infiniment grand,
Éternel puissant, Roi majestueux.

2.
Quand je songe que tu m'as fait,
Que tu t'es révélé à moi,
Que pour moi tu as tout créé,
Tout me révèle ta bonté.
