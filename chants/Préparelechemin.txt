R.
Prépare le chemin du seigneur,
Entends celui qui vient au désert de ton coeur.
Prépare le chemin, entends celui qui vient,
Pour libérer ton coeur.

1.
Il vient baptiser
Au fleuve de sa vie;
Les eaux de vérité
Jaillissent dans ta nuit.
Chercheur de Dieu, avance toi
Pour découvrir une autre voie !

2.
Il vient pour te greffer
Au peuple des croyants;
L’Esprit fera monter
La sève du Vivant.
Rameau de Dieu, tu grandiras,
Sur toi justice mûrira.

3.
Il vient pour entraîner
Ta marche vers le soir,
Lui seul pourra combler
Tes creux de désespoir.
Ami de Dieu, choisis ses pas
Et ton désert refleurira.