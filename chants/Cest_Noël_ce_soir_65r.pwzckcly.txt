1.
C'est Noël, c'est fête sur la terre
Car c'est Dieu qui naît dans nos misères
Un enfant apporte la lumière
Fils de Dieu, prince de l'univers

R.
C'est Noël ce soir,
Dieu vient dans notre histoire
C'est Noël ce soir, Jésus notre espoir
C'est Noël ce soir,
Dieu vient dans notre histoire
C'est Noël ce soir, Jésus notre espoir

2.
C'est Noël, entends les milliers d'anges
Entonner leurs hymnes de louange
Un enfant, un Sauveur nous est né
Car un fils nous a été donné