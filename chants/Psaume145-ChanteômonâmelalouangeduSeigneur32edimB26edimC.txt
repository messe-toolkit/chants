CHANTE, Ô MON ÂME,
LA LOUANGE DU SEIGNEUR !

1.
Le Seigneur garde à jamais sa fidélité,
il fait justice aux opprimés,
aux affamés, il donne le pain ;
le Seigneur délie les enchaînés.

2.
Le Seigneur ouvre les yeux des aveugles,
le Seigneur redresse les accablés,
le Seigneur aime les justes,
le Seigneur protège l’étranger.

3.
Il soutient la veuve et l’orphelin,
il égare les pas du méchant.
D’âge en âge, le Seigneur régnera :
ton Dieu, ô Sion, pour toujours !