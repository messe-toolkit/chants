R.
EN MARCHE VERS QUELS LENDEMAINS?
SE DEMANDER À QUI TENIR LA MAIN
POUR AVANCER ET TROUVER SON CHEMIN.

1.
Route de ténèbres, le regard baissé.
Route de Lumière, les yeux relevés.

2.
Route de ténèbres, les mains refermées.
Route de Lumière, tout à partager.

3.
Route de ténèbres, le coeur enchaîné.
Route de Lumière, capable d’aimer.

4.
Route de ténèbres, des mots pour blesser.
Route de Lumière, Parole de Paix.

5.
Route de ténèbres, l’esprit tourmenté.
Route de Lumière, vivre en vérité.

6.
Route de ténèbres, prêt à condamner.
Route de Lumière, l’autre à pardonner.