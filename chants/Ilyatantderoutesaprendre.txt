R.
IL Y'A TANT DE ROUTES A PRENDRE,
D'UN BOUT A L'AUTRE DU JOUR.
IL Y'A TANT DE ROUTES A PRENDRE,
VERS LA GUERRE OU VERS L'AMOUR.

1.
Chemins de bagarre,
Vers la mare des chagrins
Où nos pas s'égarent.
Chemins de tendresse,
Vers la fête des copains
Et nos mains s'empressent !

2.
Chemins de tristesse,
Où l'on s'est rendu tout seul,
Sur des mots qui blessent…
Chemins de silence,
Pour entrebailler le coeur
Vers d'autres étoiles.

3.
Chemins du sourire,
Quand la vie répand la joie
De sa tirelire.
Chemins du sourire,
Quand la vie répand la joie
De sa tirelire.