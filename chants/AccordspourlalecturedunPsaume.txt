1.
Dans la faiblesse de Dieu,
Les martyrs ont puisé la force ;
Leurs pas dans les pas du Sauveur,
Ils affrontent l'Adversaire.

2.
Enveloppés de tourments,
Ils redisent le Nom du Père ;
Au long du combat pour la foi,
Se consomme leur baptême.

3.
Près de la Croix de Jésus,
Ils rejoignent Marie sa Mère ;
Témoins d'un Seigneur humilié,
Ils pénètrent son mystère.

4.
Leur défenseur est vivant,
La lumière espérée se lève ;
La mort a perdu son pouvoir,
Et la paix les transfigure.