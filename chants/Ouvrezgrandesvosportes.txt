R.
OUVREZ GRANDES VOS PORTES
OUVREZ VOS COEURS EN GRAND
QUE LA VIE VOUS EMPORTE
PLUS LOIN DEVANT

1.
Gardez dans vos mémoires
Qu’un monde est à bâtir
Des sillons de l’histoire
Jaillira l’avenir

2.
L’espoir fait le voyage
Vous en êtes témoins
N’arrachez pas la page
Où s’écrira demain

3.
Allez, quittez votre île
Car la route est tracée
Un vaisseau : l’Évangile
Un ciel de liberté

4.
Transformez l’héritage
En trésor partagé
Votre part de nuages
En monde ensoleillé

5.
Il est un Dieu unique
Qui nous dit l’essentiel
Que vivre oecuménique
C’est vivre fraternel