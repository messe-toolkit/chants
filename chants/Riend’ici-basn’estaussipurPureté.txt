1.
Rien d’ici-bas n’est aussi pur
Que l’âme de Marie :
Un océan de cristal
Sous le soleil de midi.
La lumière du Très-Haut s’y reflète,
S’y repose et s’y noie :
Noce du feu et de l’eau
Où la Vie se déploie.

2.
La toile vierge au Créateur
Se livre sans repli.
C’est le Chef-d’oeuvre de Dieu :
Le Coeur très pur de Marie.
Un passage comme une ombre l’effleure
Et s’y marque d’un trait :
Grâce infinie de l’amour
Qui le blesse à jamais.

3.
Ne pas savoir et consentir
À l’oeuvre de l’Esprit ;
N’avoir rien d’autre à donner
Que le trésor de son oui.
Tous les âges te diront bienheureuse,
Entre toutes bénie,
Toi qui portas le Sauveur,
Sainte Vierge Marie.