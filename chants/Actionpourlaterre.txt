WO… O, O, O ! WO… O, O, O !
YÉ… É, É, É ! YÉ… É, É, É !
ATTENTION… ATTENTION AU DÉPART
TROIS… DEUX… UN
ACTION POUR LA TERRE.

1.
Agissons pour que la terre soit belle
Que le printemps soit aux hirondelles
Donnons à la vie tout son naturel
Ouvrons nos coeurs à l’essentiel.

2.
Agissons pour laisser la nature
Évoluer au fur et à mesure
Ne la brusquons pas, pensons au futur
Respectons-la pour que ça dure.

3.
Agissons en osant partager
C’est l’enjeu pour notre humanité
Donnons à l’Amour la priorité
Et que personne ne soit lésé.