R.
RESSUSCITE! ALLELUIA!
IL EST VIVANT, IL EST VIVANT
IL REVIENDRA... ALLELUIA!

1.
Que fleurisse la paix sur tous les toits du monde
Roule la pierre de nos tombeaux
La mort est renversée
Il vient nous sauver.

2.
Que tremblent les puissants du sommet de leurs trônes
La loi du plus fort inversée
Il faut réinventer
Pour la liberté.

3.
Que dansent les boiteux les affligés sans nombre
Au soleil de sa compassion
La joie est retrouvée
Souffrances appaisées.

4.
Que tous ceux qui ont faim soient conviés à la noce
Au grand partage de l'amour
Tout homme est rassasié
Il vient nous combler.

5.
Heureux tous les petits les rejetés du monde
Béatitudes annoncées
Chaque homme est relevé
Vive la dignité.