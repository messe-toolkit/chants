1.
À ce monde que tu fais chaque jour avec tendresse,
DONNE UN COEUR DE CHAIR, DONNE UN COEUR NOUVEAU!
À ce monde où tu voudrais plus de joie, moins de détresse,
DONNE UN COEUR DE CHAIR, DONNE UN COEUR NOUVEAU!
À ce monde qui renaît s’il a foi en ta promesse,
DONNE UN COEUR DE CHAIR, DONNE UN COEUR NOUVEAU!

R.
VIENNENT LES CIEUX NOUVEAUX
ET LA NOUVELLE TERRE
QUE TA BONTÉ NOUS DONNERA!
VIENNENT LES CIEUX NOUVEAUX
ET LA NOUVELLE TERRE,
OÙ LA JUSTICE HABITERA !

2.
Sur les hommes qu’il t’a plu de créer à ton image,
ENVOIE TON ESPRIT, UN ESPRIT NOUVEAU!
Sur les hommes que l’on tue pour leur peau ou leur visage,
ENVOIE TON ESPRIT, UN ESPRIT NOUVEAU!
Sur les hommes qui n’ont plus qu’à se taire sous l’outrage,
ENVOIE TON ESPRIT, UN ESPRIT NOUVEAU!

3.
À ce monde traversé par la haine et la violence,
DONNE…
À ce monde ravagé par la guerre et la souffrance,
DONNE…
À ce monde séparé de ses sources d’espérance,
DONNE…

4.
Sur les hommes de ce temps que révolte la misère,
ENVOIE…
Sur les hommes que tu prends dans le feu de la prière,
ENVOIE…
Sur les hommes que tu rends fraternels et solidaires,
ENVOIE…

5.
À l’Église que ton coeur engendra de sa blessure,
DONNE…
À l’Église toute en pleurs quand les frères se torturent,
DONNE…
À l’Église des pécheurs refusant sa déchirure,
DONNE…

6.
Sur le peuple des croyants dérouté par son histoire,
ENVOIE…
Sur le peuple des souffrants qui occupe ta mémoire,
ENVOIE…
Sur le peuple qui attend que paraisse enfin ta gloire,
ENVOIE…

7.
À l’Église pour la Paix et l’annonce de ton Règne,
DONNE…
À l’Église qui connaît les épreuves du calvaire,
DONNE…
À l’Église qui se plaît au message des Prophètes,
DONNE…

8.
Sur ton peuple qui se meurt s’il n’élève la louange,
ENVOIE…
Sur ton peuple de chanteurs convoqué avec les anges,
ENVOIE…
Sur ton peuple sans frayeur que tu tiens dans l’Alliance,
ENVOIE…

9.
À nos corps où tu as mis le désir de voir ta face,
DONNE…
À nos corps que tu conduis de leurs tombes vers ta Pâque,
DONNE…
À nos corps déjà bénis dans le Corps où tout est grâce,
DONNE…
10.
Sur les foules esseulées qui ont soif de ta Parole,
ENVOIE…
Sur la Ville des sauvés où s’achève tout exode,
ENVOIE…
Sur ta Vigne bien-aimée qui prépare ses récoltes,
ENVOIE…
11.
À ce peuple que ton pain a gardé de la famine,
DONNE…
À ce peuple qui est tien et ne veut pas d’autre guide,
DONNE…
À ce peuple de témoins passionné pour ta justice,
DONNE…
12.
Sur tous ceux que tu choisis pour répandre l’Évangile,
ENVOIE…
Sur tous ceux qui ont repris l’aventure des disciples,
ENVOIE…
Sur tous ceux qui ont appris la grandeur de ton service,
ENVOIE…