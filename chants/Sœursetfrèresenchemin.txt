1.
Un jour, Jésus raconte
Un récit étonnant
L’histoire d’une rencontre
Pour parler du prochain
Sur le bord d’une route
Qu’on dit mal fréquentée
Par des bandits sans doute
Un homme vient de tomber

SUR LA MÊME TERRE,
DES SOEURS ET DES FRÈRES,
SORELLE, FRATELLI. (bis)

SUR NOS CHEMINS AUSSI ! (bis)

2.
Une ombre qui s’approche
Mais change de trottoir
Une autre fait de même
Et ne veut pas le voir
Un étranger arrive
S’arrête et s’accroupit
Lui parle et puis le soigne
Se rend proche de lui

3.
Une histoire singulière
De solidarité
Rien de mieux sur la terre
Que la fraternité
Que cette parabole
Éclaire nos matins
Qu’elle soit la vraie boussole
Vers l’amour du prochain