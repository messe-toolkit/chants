R.
Visage de tendresse,
Dieu d'amour en vérité,
Visage de tendresse,
Tu sauras nous pardonner !

1.
Dieu fidèle à ton Alliance,
Nous avons marché loin de toi.
Fais-nous quitter nos voies d'errance,
Dieu saint, ravive notre foi !

2.
Dieu fidèle à ta parole,
Qui de nous a su t'écouter ?
La nuit, le jour ta voix résonne.
Dieu bon, tu viens nous réveiller !

3.
Dieu fidèle à ta promesse,
Tu nous as donné Jésus-Christ.
Sur nos chemins sa croix se dresse ;
Dieu fort conduis nos pas vers lui !

4.
Dieu fidèle au long des siècles,
Cherchons-nous ta face aujourd'hui ?
Toi le Pasteur, tu nous recherches.
Dieu grand, tu sauves tes brebis.