1.
Dieu, dis-moi
Combien de temps
Faut-il donc que tu m’ignores
Combien Dieu
Combien de temps
Vas-tu m’oublier encore
Vas-tu m’oublier toujours
Comme un chien au fond d’une cour ?

MON DIEU, MON DIEU
ALLUME MES YEUX!
MON DIEU, MON DIEU
ALLUME MES YEUX!

2.
Dieu, dis-moi
Combien de temps
Faut-il chercher ton visage
Combien Dieu
Combien de temps
Faudra-t-il tourner en cage
Faudra-t-il jusqu’à la mort
Traîner dans les corridors ?

3.
Dieu, dis-moi
Combien de temps
Faut-il ronger sa colère
Combien Dieu
Combien de temps
Mon coeur s’ra-t-il aux galères
Faudra-t-il que mon seul pain
Soit celui de mes chagrins ?

4.
Dieu, dis-moi
Combien de temps
Faut-il que je sois victime
Ô, mon Dieu
Combien de temps
Faudra-t-il que l’on m’opprime
Faudra-t-il que ton parti
Soit celui de l’ennemi ?