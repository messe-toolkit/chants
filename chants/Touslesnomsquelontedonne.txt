R.
TOUS LES NOMS QUE L’ON TE DONNE
SONT LES ROSES D’UN BOUQUET,
Ô MARIE QUE NOS PRIÈRES
SOIENT DES FLEURS POUR TE CHANTER.

1.
Reçois la fleur de la confiance
Et ses pétales parfumés,
Notre-Dame d’Espérance
Viens fleurir nos coeurs fanés.

2.
Reçois la fleur de nos faiblesses
Et ses épines si serrées,
Notre-Dame de Tendresse
Viens fleurir nos coeurs blessés.

3.
Reçois la fleur de nos regards
Et ses couleurs d’humanité,
Notre-Dame de la Garde
Viens fleurir nos coeurs fermés.

4.
Reçois la fleur de nos « pourquoi ? »
Et ses moissons de pauvreté,
Notre-Dame de la Joie
Viens fleurir nos coeurs usés.