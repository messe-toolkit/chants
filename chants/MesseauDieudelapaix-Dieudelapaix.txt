R.
DIEU DE LA PAIX DANS LE CIEL ET SUR LA TERRE,
GLOIRE À TOI PAR TON FILS JÉSUS CHRIST !
DIEU DE LA PAIX, TON VISAGE NOUS ÉCLAIRE,
GLOIRE À TOI POUR LE DON DE L’ESPRIT !

1.
Tu nous appelles à faire Église en ta maison,
La voix du grand Berger nous rassemble.
Sa croix détruit le mur des divisions,
Il crée les temps nouveaux du vivre ensemble.

2.
Tu nous appelles à convertir notre regard,
Pour voir de quel amour tu nous aimes.
Libère notre coeur des nuits du mal,
Nos yeux s’éveilleront à tes merveilles.

3.
Tu nous appelles à recevoir le pain des mots,
Heureux qui mangera ta Parole !
En nous se lèvera comme en écho
Le chant de qui veut croire à ton aurore.

(en envoi)

4.
Tu nous appelles à rebâtir maison de paix
Quand l’homme a fait son oeuvre de ténèbres.
Le Christ est avec nous à tout jamais,
Témoin que tu es Dieu « briseur de guerres ».

5.
Tu nous appelles à partager tes durs combats,
Le monde a faim d’amour et de justice.
Qui donc à tes côtés ne connaîtra
La joie de le nourrir de l’Évangile.