R.
QUAND IL VIENDRA,
L’ESPRIT DE VERITE
IL NOUS GUIDERA
JUSQU’À LA VERITE.

1.
Offrons-lui notre histoire,
Laissons jaillir l’Esprit,
Pour qu’il soit la mémoire
Des mots de Jésus-Christ.

2.
Au creux de nos souffrances
Laissons gémir son cri,
Pour qu’il soit la défense
Des hommes dans la nuit.

3.
Au vent de la prière
Laissons grandir son feu,
Pour qu’il soit la lumière
Qui chante dans nos yeux.