R.
ALLUME UN GRAND FEU AU SOMMET DE LA MONTAGNE,
ALLUME LE FEU DE TES YEUX.
AUTOUR DE CE FEU TOUS LES PEUPLES SE REJOIGNENT,
ALLUME LE FEU DE DIEU.

1.
Regarde les étoiles dans la nuit,
chacune porte un nom que tu connais.
Le feu comme une étoile nous redit
que notre Dieu ne nous oublie jamais.

2.
Aux portes du désert tu dois choisir
la route à prendre vers la liberté.
Le feu comme un appel qu'il faut saisir
nous montre le chemin de l'unité.

3.
Dans la lumière du matin naissant
ton coeur peut-il longtemps vivre emmuré ?
Le feu comme un soleil resplendissant
éclaire nos regards transfigurés.

4.
Relève-toi car Dieu vient aujourd'hui
pour achever ce qui était écrit.
Le feu qui se répand comme la pluie
innonde notre terre de l'Esprit.