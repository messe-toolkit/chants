R.
Marie, toi notre Mère,
Bénie du Dieu vivant,
Tendres-se pour la terre,
Soutiens tous tes enfants.
Marie des nuits obscures
Et des matins levés,
Délivre et transfigure
Nos coeurs d’enfants blessés !

1.
Mystères joyeux
Montre-nous Jésus né dans une étable,
Descendu chez nous parmi les bergers.
Vrai Fils de Dieu et Roi véritable,
Dans la nuit du monde il vient nous sauver.

2.
Mystères lumineux
Montre-nous Jésus, Messager du Père,
Ce qu’il dit de Lui peut changer les coeurs :
Prenez la voie des fils de lumière,
Choisissez de vivre et n’ayez pas peur !

3.
Mystères douloureux
Montre-nous Jésus, quand survient son heure,
Il gravit pour nous le mont Golgotha.
Pourquoi faut-il qu’il souffre et qu’il meure ?
Tu es là, debout, près du Christ en croix.

4.
Mystères glorieux
Montre-nous Jésus au matin de Pâques,
Rayonnant de paix, Fils ressuscité.
Il est vivant, des voix le proclament :
Un Soleil de joie pour l’humanité.

5.
Mystères glorieux
Montre-nous Jésus répandant son Souffle ;
Des témoins se lèvent et leurs mots vont loin.
L’Esprit de feu les guide et les pousse,
Nul n’arrêtera ce brasier divin.