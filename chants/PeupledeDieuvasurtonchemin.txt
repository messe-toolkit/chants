PEUPLE DE DIEU, VA SUR TON CHEMIN.
SUIS LES PAS DU BON SAMARITAIN.
PEUPLE DE DIEU, VA SUR TON CHEMIN.
SOIS L'ÉGLISE OUVERTE QUI PREND SOIN !

1.
Du matin jusqu'à la nuit
Dieu prend soin de toi.
Sa lumière te conduit,
Marche dans ses voies !
Dieu te choisit pour son Alliance,
Ton coeur soit plein d'une espérance.

2.
L'Évangile est un trésor,
Dieu te le confie.
Donne chair à tous ses mots,
Qu'ils soient chants de vie !
Va les redire à ceux qui passent,
Qu'ils se libèrent des impasses !

3.
Pour la paix de tes années
Dieu prend soin de toi.
Dans un monde à libérer,
Lui sera ta joie.
Église à l'oeuvre synodale,
Sois rayonnante de la Pâque !

4.
Le Seigneur nous a aimés
D'un amour sans prix.
Serviteur il s'est donné
Jusqu'à l'infini :
Aimer son Dieu, aimer ses frères,
Aimer chacun comme sois-même !

5.
Par nos gestes de pardon
Dieu est révélé ;
Le premier il nous fait don
De sa vérité :
Il est amour – miséricorde
Manifesté à tous les hommes.

6.
Pour les pauvres et les petits
Qui se lèvera ?
Entrons tous en diaconie
Même à petits pas !
La compassion envers tout homme,
Voilà l'entrée dans le Royaume.

7.
L'univers où nous vivons
Vient nous questionner ;
Des merveilles par millions
Sont à contempler !
Qui donc est l'homme en ces espaces ?
Qu'il soit louange et rende grâce !

8.
La planète survivra
Si nous le voulons.
Osons croire à des combats
Pour la communion !
L'air pur et l'eau qui nous font vivre,
Longtemps vont-ils baigner nos rives ?

9.
Nous chantons " Laudato si "
De tout notre coeur ;
Que nos gestes soient aussi
À la vraie hauteur !
En serviteurs de la nature
Nous construisons Maison commune.

10.
En disciples du Sauveur,
Soyons ses témoins !
Dieu saura toucher les coeurs
Par son Esprit-Saint.
Qu'il nous pénètre de sa force
Dans une Église qui nous porte.

11.
Nous croyons en l'avenir
Dans un au-delà ;
Il nous reste à le bâtir
Dès cet ici-bas.
Pour des moissons de joies nouvelles,
Semons des graines de sagesse !

12.
Pour grandir dans l'unité
Christ est avec nous ;
Il nous veut à ses côtés,
Forts dans son amour.
Sous l'arc-en-ciel des différences
Gardons sans crainte la confiance.