1.
Notre-Dame Lumière,
Tu abrites dans ton cœur,
Le plus grand des mystères,
Pour la joie de ton Seigneur !
Le plus grand des mystères,
Pour la joie de ton Seigneur !

2.
Parmi toutes les femmes,
Dieu te cherche et te choisit,
Tu l’accueilles en ton âme
Pour nous donner le Messie !
Tu l’accueilles en ton âme
Pour nous donner le Messie !

3.
Ton enfant vient au monde,
Mais ton cœur connaît déjà
La blessure profonde
Sous le digne de la croix !
La blessure profonde
Sous le digne de la croix !