R.
Jésus est né,
Chante, chante Noël !
Jésus est né,
Chante, chante Noël !

1.
Un tout petit enfant est né
Dans une étable de Judée
C’est Noël !
Ouvrons nos yeux, ouvrons nos mains
C’est lui Jésus, c’est lui qui vient.
C’est Noël !

2.
C’est dans la nuit que les bergers
Ont vu l’étoile qui brillait
C’est Noël :
Alors bien vite, ils ont couru
Ils ont couru pour voir Jésus
C’est Noël !

3.
Près de Joseph, près de Marie,
Une lumière dans la nuit
C’est Noël !
Voici Jésus, petit enfant
Allons à lui, il nous attend
C’est Noël !

4.
C’est dans la joie que nous chantons
C’est dans la paix que nous aimons
C’est Noël !
Tous les enfants chantent Noël
Voici Jésus, Emmanuel
C’est Noël !