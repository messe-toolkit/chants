1.
Christ est ressuscité
Et l'amour a triomphé
Dans sa faiblesse il est plus fort
Que l'emprise de la mort
Alléluia ! Alléluia ! Alléluia ! Alléluia ! Alléluia !
Nous célébrons le jour nouveau
Où Jésus sort du tombeau.
Alléluia !

2.
Christ est ressuscité
Dans la joie il est entré ;
Splendeur qui perce notre nuit
Vers le Père il nous conduit
Alléluia ! Alléluia ! Alléluia ! Alléluia !
Célébrons le Père et le Fils
Dans l'Esprit qui les unit.
Alléluia !