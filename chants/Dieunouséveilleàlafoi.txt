1.
Dieu nous éveille à la foi,
Voici le jour que fit le Seigneur.
L’Agneau livré guérit les pécheurs :
Il nous libère.

R.
Jour d'allégresse, alléluia !
Jour d'allégresse, alléluia !

2.
Dieu nous convoque à la joie,
Voici le jour que fit le Seigneur.
Notre berger, le Christ est vainqueur :
Il nous rassemble.

3.
Dieu nous invite au repas,
Voici le jour que fit le Seigneur.
L’amour donné, plus fort que nos peurs,
Ouvre au partage.
