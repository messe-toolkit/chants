BIENHEUREUX CEUX QUI FONT MISÉRICORDE
CAR ILS OBTIENDRONT LE PARDON DE DIEU ;
BIENHEUREUX SUR LES PAS DU FILS DE L’HOMME
CAR ILS REVIVRONT DANS LA PAIX DE DIEU !

1.
Heureux ces bien-aimés de notre Père,
Choisis pour vivre dans l’amour :
L’amour aux fleurs de la tendresse
Nous rappelant ce que Dieu fait pour nous,
À la louange de sa gloire ! bis

2.
Heureux tous ces vivants que Jésus sauve,
« Par ses blessures ils sont guéris »
Guéris et membres du Royaume
Pour être au monde une parole enfouie,
À la louange de sa gloire ! bis

3.
Heureux ces artisans de l’Évangile,
Douceur et force des témoins :
Témoins au coeur de pacifiques,
Parmi leurs frères ils tracent des chemins,
À la louange de sa gloire ! bis

4.
Heureux ces messagers de l’espérance,
Marqués du souffle de l’Esprit :
L’Esprit semeur de résistances,
Qui les envoie porter beaucoup de fruit,
À la louange de sa gloire ! bis

5.
Heureux ces libérés de l’esclavage,
L’humanité des gens debout :
Debout, sortis de nos impasses
Pour voir la paix bénir nos rendez-vous,
À la louange de sa gloire ! bis

6.
Heureux ces pèlerins de la sagesse,
Toujours en quête de la vie :
La vie de l’ombre à la lumière,
Tournée vers Pâques et l’avenir en Christ,
À la louange de sa gloire ! bis

7.
Heureux d’être promis à l’héritage
Qui comblera tous les désirs :
Désirs d’entendre au long des âges
Un chant d’aurore où le Seigneur se dit,
À la louange de sa gloire ! bis