1.
J’ai usé plus d’un fond d’culotte
Sur les bancs des grandes écoles.
J’ai gravi tous les échelons
Toujours premier d’ma promotion.
De l’ENA ou d’polytechnique,
Des finances à la politique,
Ça m’a pris mes jeunes années,
Oui je peux dire qu’j’en ai bavé.
VANITÉ, VANITÉ,
LAISSEZ-MOI VOUS CHANTER,
VANITÉ, VANITÉ,
LE BLUES DES VANITÉS.

2.
Puis j’ai monté mon entreprise,
Multinationale, je précise !
Dans chaque pays j’avais un pied,
Je faisais la loi du marché.
J’étais le Seigneur de la bourse,
Je battais tout l’monde à la course,
J’devins très grand, très riche aussi,
Tout m’réussissait dans la vie.

3.
J’faisais tout cela dans la sagesse
Avec du coeur mais sans faiblesse.
Sans rien refuser à mes yeux
Qui réclamaient du merveilleux.
Mon coeur jouissait de ma peine,
De mon travail à perdre haleine,
Puis, j’ai r’gardé c’que j’avais fait :
Je n’y ai vu que vanité.