1.
Réjouis-toi, sainte Mère de Dieu,
Par toi s’allume la joie dans nos coeurs,
Le ciel se réjouit avec la terre,
La terre danse en choeur avec le ciel !

R.
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA !

2.
Tu es la gloire des oeuvres du Roi,
Trésor unique où l’on puise la vie,
Du jour spirituel, tu es l’aurore,
L’étoile qui annonce le soleil !

3.
Tu illumines le coeur des croyants,
C’est toi l’appui de leur foi dans le Christ,
Prélude des merveilles de la grâce,
Tu montres Jésus-Christ, notre Sauveur !

4.
Toi qui portas le Seigneur en ton sein,
L’ami des hommes et de tout l’univers,
Apporte à tes enfants une lumière
Et fais jaillir une eau de grande paix !