R.
Les temps sont accomplis,
Dieu se fait tout proche ;
Qu’il règne dans nos vies
Par toi, Jésus, Fils de l’homme ! (bis)

3e dimanche

1.
Notre monde appelle des temps nouveaux
Qui prendront naissance avec la justice.
Fais souffler, Seigneur, un esprit nouveau,
Et nous bâtirons une terre libre.

2.
Notre Église appelle un regard nouveau
Sur les Galilée où tu nous précèdes,
Fais souffler, Seigneur, un esprit nouveau,
Que nos yeux découvrent tes voies humaines !

3.
Notre marche appelle un élan nouveau
Qui nous conduira jusqu’à l’autre rive.
Fais souffler, Seigneur, un esprit nouveau,
Nous avancerons en osant te suivre.

4.
Notre siècle appelle des cieux nouveaux
Qui dissiperont toutes les ténèbres.
Fais souffler, Seigneur, un esprit nouveau,
Que nos jours s’embrasent dans ta lumière !
8e dimanche

5.
Notre vigne appelle un soleil nouveau
Qui fera mûrir des fruits de tendresse.
Fais souffler, Seigneur, un esprit nouveau,
Tu vendangeras des raisins de fête.

6.
Tes disciples appellent ton vin nouveau ;
Il fermentera dans des outres neuves.
Fais souffler, Seigneur, un esprit nouveau,
Que nos chants résonnent dans ta demeure !

7.
Notre terre appelle des coeurs nouveaux
Qui partageront leur espoir de vivre.
Fais souffler, Seigneur, un esprit nouveau,
Toute chair attend que tu la délivres.

8.
Notre corps appelle un habit nouveau
Qui resplendira au festin des Noces.
Fais souffler, Seigneur, un esprit nouveau,
Et nous prendrons place dans ton Royaume.