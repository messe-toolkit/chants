R.
Pasteur d’un peuple en marche,
Conduis-nous par tes chemins.
Berger des sources vives,
Guide-nous vers ton repos.

1.
Le Seigneur est mon berger,
Rien ne manque à mon repos,
Ni les verts pâturages ni les eaux.
Jésus, tu peuples ma vie,
Toi, le pasteur des brebis.

2.
Tu m’enseignes tes chemins,
Tu m’entraînes par tes voies
Sur les monts de justice vers ta croix.
Jésus tu donnes ta vie,
Ô vrai Pasteur pour tes brebis.

3.
Aux ténèbres de la mort,
Plus de crainte sous ta main,
Ton bâton me rassure et me soutient.
Jésus tu gardes ma vie,
Toi, le Sauveur de tes brebis.

4.
Tu me marques de ta joie,
Tu m’invites à ton festin,
Ton amour donne un signe dans le pain.
Jésus, tu passes en ma vie,
Toi, la vraie manne des brebis.

5.
Et ta grâce me poursuit
Dans l’angoisse où le bonheur.
Mais comment reconnaître le Seigneur ?
Jésus, révèle ta vie,
Toi, la lumière des brebis.

6.
Tant que durent nos déserts,
Nous marchons vers ton repos,
Vers l’unique bercail de ton troupeau.
Jésus rassemble nos vies,
Toi, le Pasteur de tes brebis.