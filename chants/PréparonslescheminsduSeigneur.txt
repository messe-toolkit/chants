R.
Préparons les chemins du Seigneur !
Rendons droits ses sentiers !
Une voix crie au désert de nos cœurs :
« Christ est là : regardez ! »

1.
Voix du Prophète annonçant le Royaume :
Dieu cherche en toi les fruits du repentir ;
Es-tu la terre où Jésus peut venir ?
Es-tu l’arbre où l’amour peut grandir ?

2.
Cri du veilleur dans la nuit de ce monde :
Dieu cherche en toi le soleil de la vie ;
Es-tu la lampe qui brûle et qui luit ?
As-tu le feu du baptême de l’esprit ?

3.
Voix du témoin face aux grands de la terre :
Dieu nous prépare une aurore de joie ;
Es-tu celui qui révèle un espoir ?
Es-tu cet homme où le sang dit la foi ?