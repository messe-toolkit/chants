1.
Jésus, Fils de Dieu,
Son égal en toutes choses,
Chez nous, parmi les hommes,
Tu renonces à ta grandeur
Et tu choisis l’abaissement du Serviteur.

2.
Jésus, bon Pasteur,
Devenu semblable aux hommes,
Ta mort sous les opprobres
Nous apprend la vraie grandeur :
Terrible croix pour le salut de tout pécheur !

3.
Jésus, le Seigneur,
Premier-né dans le Royaume,
C’est toi le Fils de l’homme
Dont le nom est « Dieu sauveur ».
Nous t’acclamons, à tout jamais tu es vainqueur.