EN CES JOURS-LÀ FLEURIRA LA JUSTICE,
GRANDE PAIX JUSQU’À LA FIN DES TEMPS.

1.
Dieu, donne au roi tes pouvoirs,
à ce fils de roi ta justice.
Qu’il gouverne ton peuple avec justice,
qu’il fasse droit aux malheureux !

2.
En ces jours-là, fleurira la justice,
grande paix jusqu’à la fin des lunes !
Qu’il domine de la mer à la mer,
et du Fleuve jusqu’au bout de la terre !

3.
Il délivrera le pauvre qui appelle
et le malheureux sans recours.
Il aura souci du faible et du pauvre,
du pauvre dont il sauve la vie.

4.
Que son nom dure toujours ;
sous le soleil, que subsiste son nom !
En lui, que soient bénies toutes les familles de la terre ;
que tous les pays le disent bienheureux !