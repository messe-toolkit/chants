R.
Voici que l’ange Gabriel, devant la Vierge est apparu.
De toi va naître un enfant Dieu,
Et tu l’appelleras Jésus.

1.
De mon Seigneur j’ai tout reçu, je l’ai servi jusqu’à ce jour,
Qu’il fasse en moi sa volonté, je m’abandonne à son amour.

2.
Et Dieu se fit petit enfant, la Vierge lui donna son corps.
Il connut tout de notre vie, nos humbles joies et notre mort !

P.
Et son nom est Emmanuel !