Refrain :
Bonjour, bonjour madame la planète,
On dit partout que vous n’allez pas bien ;
Et puis toujours, cette mine défaite,
Venez cueillir les fruits de nos jardins.

1.
Tohu-bohu de cette vie sans code,
Tout le tintouin du “ graf ” au son du rap.
Le coeur-à-corps du zapping branché-mode,
Quand, trop c’est trop, pleure pas si ça dérape !

2.
Génération “ boat-people ” et mal-être,
Info “ kleenex ” - CAC 40 et jackpot,
Destins bradés, “ déjantés-fiers-de-l’être ” ...
Cliquez ! c’est naze, normal si ça capote ! ...

3.
Dans ce jardin un peu à notre image,
Nous sèmerons nos rêves et nos choix,
Et nos chansons ouvriront un PASSAGE,
Sur un SOLEIL qui ne se couche pas...