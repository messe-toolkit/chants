PEUPLE LIBÉRÉ
PAR LE DIEU DE L'ALLIANCE,
ÉGLISE SANCTIFIÉE
PAR LE DON DE JÉSUS-CHRIST,
CHANTE DANS L'ESPRIT
TA FOI, TON ESPÉRANCE !

1.
Heureux les habitants
De ta maison, Seigneur,
Maison de fête,
Maison de paix :
Tu nous dévoiles ton amour !

2.
Heureux les habitants
De ta maison, Seigneur,
Ouverte au monde
Pour t'accueillir :
Tu nous dévoiles ta bonté !

3.
Heureux les habitants
De ta maison, Seigneur,
Lieu de partage,
Lieu d'amitié :
Tu nous dévoiles ton pardon !

4.
Heureux les habitants
De ta maison, Seigneur,
Belle demeure
Pour tes enfants :
Tu nous dévoiles ta grandeur !

5.
Heureux les habitants
De ta maison, Seigneur.
Dans ta lumière,
Tu la construis :
Tu nous dévoiles ta clarté !