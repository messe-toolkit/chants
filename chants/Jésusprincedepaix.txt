1.
Jésus prince de paix, mon bien-aimé, ma vie,
Mon rocher, mon salut.
Jésus ma forteresse, ma joie et mon soutient,
Étoile du matin.

2.
Jésus parole vive, pain brisé pour les hommes,
Force sur leurs chemins.
Jésus flamme d'amour, repos de l'humilité,
Lumière vérité.

3.
Jésus soleil levant sur les peines des hommes,
Mon berger, mon espoir.
Jésus terre promise, mon seul bien, mon trésor,
Source d'humanité.

4.
Jésus ressuscité, vainqueur de toute angoisse,
Ami, consolateur.
Jésus frère de sang, mon maître mon seigneur,
Mon roi et mon sauveur.