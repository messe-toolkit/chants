R.
Seigneur, lève-toi
Et montre ton visage,
Seigneur, lève-toi
Et viens nous rendre l'espoir.

1.
On nous a dit que pour sauver les hommes
Tu as livré ton Fils sur une croix.
Quand viendra-t-il le temps de son Royaume ?
Reviens vers nous, que sommes-nous sans toi ?

2.
On nous a dit que tu défends le pauvre
Et que vers toi on ne crie pas en vain.
Est-il donc vrai que ton amour nous sauve ?
Reviens vers nous, pourquoi es-tu si loin ?

3.
Aux quatre vents, Seigneur, tu nous disperses
Comme poussière au gré des tourbillons.
La faim nous mord et le froid nous transperce,
Reviens vers nous, c'est toi que nous cherchons.

4.
On nous a dit l'éclat de tes merveilles
Pour libérer ton peuple de la mort.
Mais aujourd'hui, Seigneur, prête l'oreille,
Reviens vers nous, pour nous conduire au port.