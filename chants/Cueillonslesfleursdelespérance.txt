Refrain :
Cueillons les fleurs de l’Espérance
Et rallumons notre bougie,
Chacun de nous est une chance,
Là où il est, là où il vit !

1.
Certains disent que Dieu
Est devenu aveugle,
Mais c’est avec nos yeux
Qu’il peut voir aujourd’hui.
Qu’il peut voir aujourd’hui.

2.
Certains disent que Dieu
Est devenu muet,
Mais c’est avec nos voix
Qu’il s’exprime aujourd’hui,
Qu’il s’exprime aujourd’hui.

3.
Certains disent que Dieu
Est devenu manchot,
Mais c’est avec nos mains
Qu’il construit aujourd’hui.
Qu’il construit aujourd’hui.

4.
Certains disent que Dieu
Est devenu absent,
C’est avec notre coeur
Qu’il redonne la vie.
Qu’il redonne la vie.