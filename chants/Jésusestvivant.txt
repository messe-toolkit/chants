R.
JESUS EST VIVANT, RESSUSCITE,
ALLELUIA, ALLELUIA.

1.
Quand vient le jour, quand vient le temps
Courez jusqu'au tombeau de pierre
Alléluia ! Dieu est vivant.
Entrez, la vie vous y attend
Passez de l'ombre la lumière ?
Alléluia ! Dieu est vivant.

2.
Quittez le deuil et vos remords
Brisez les murs de vos silences
Alléluia ! Dieu est vivant.
Ne cherchez plus parmi les morts
Soyez témoins de l'espérance
Alléluia ! Dieu est vivant.

3.
Fini la nuit, fini la peur
Réveillez-vous de vos vieux rêves
Alléluia ! Dieu est vivant.
Ouvrez les yeux, ouvrez vos coeurs
De l'arbre mort jaillit la sève
Alléluia ! Dieu est vivant.

4.
Premier matin, nouveau départ
Portez au monde le message
Alléluia ! Dieu est vivant.
N'attendez pas qu'il soit trop tard
Pour vous commence le voyage
Alléluia ! Dieu est vivant.