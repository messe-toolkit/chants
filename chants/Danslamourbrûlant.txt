1.
Dans l’amour brûlant, la charité suprême,
notre Dieu est là.
Et l’amour du Christ aujourd’hui nous rassemble :
« Veillez , priez! »
Qui pourrait dormir à quelques pas du Maître,
la nuit tombée ?
Suivons le Bien-Aimé dans sa détresse,
son dur combat.
De tout l’amour du coeur aimons nos frères,
comme Jésus jusqu’à la croix.

2.
Dans l’amour plus fort que tous les cris de haine,
notre Dieu est là.
C’est un même corps que nous formons ensemble,
restons unis.
Ne déchirons pas cette unité si belle,
Dieu la bénit !
Que cessent nos violences et nos querelles,
cherchons la paix !
Et qu’au milieu de nous le règne vienne
des temps nouveaux où l’on renaît !

3.
Dans l’Amour livré souffrant la mort humaine,
notre Dieu est là.
Puissions-nous rejoindre tes amis en fête,
qui voient ton jour,
contempler ta face irradiée de gloire,
Seigneur d’amour !
Alors nous connaîtrons bonheur immense,
la joie sans fin,
l’éternité de vie dans la lumière,
près de ton Père, Dieu très saint.

Amen.

UBI CARITAS ET AMOR.
ANTIENNE
Ubi caritas et amor, Deus ibi est.
Congregavit nos in unum Christi amor.
Exultemus et in ipso jucundemur.
Timeamus, et amemus Deum vivum.
Et ex corde diligamus nos sincero.

Ant.
Ubi caritas et amor, Deus ibi est.
Simul ergo cum in unum congregamur.
Ne nos mente dividamur caveamus.
Cessent jurgia maligna, cessent lites.
Et in medio nostri sit Christus Deus.

Ant.
Ubi caritas et amor, Deus ibi est.
Simul quoque cum beatis videamus.
Glorianter vultum tuum, Christe Deus :
Gaudium, quod est immensum, atque probum,
Saecula per infinita saeculorum.

Amen.