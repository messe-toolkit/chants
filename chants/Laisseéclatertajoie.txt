TAPE DANS TES MAINS
CHANTE LA LA LA LA… (La la la la.)
SAUTE À PIEDS JOINTS
LAISSE ÉCLATER TA JOIE… (Ta joie.)

1.
Tu verras l’Amour venir habiter notre terre
Et l’Espérance habiller l’aube chaque matin
Tu verras le ciel s’illuminer sur nos hivers
Chasser l’orage un peu plus loin.

2.
Tu verras les hommes aller-venir entre pays
Semer des champs de fleurs, de rires et de merveilles
Tu verras nos pas donner des quantités de fruits
Comme le blé sous le soleil.

3.
Tu verras nos peurs aller s’échouer sur d’autres rives
Et laisser place à la beauté des sentiments
Tu verras l’espoir et tous les efforts qui s’ensuivent
Offrir au monde un coeur d’enfant.

4.
Tu verras des signes, des promesses multicolores
De la tendresse à fleur de peau dans notre histoire
Tu verras l’Amour s’intensifier toujours encore
Et faire tomber les idées noires.