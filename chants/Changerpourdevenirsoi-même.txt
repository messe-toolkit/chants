Oh, oh, oh, oh…
CHANGER, CHANGER.
bis

1.
Changer
Pour devenir soi-même
Mais en “plus” et en “mieux”
Découvrir un : « Je t’aime »
Là au fond de tes yeux.
Changer
Se sourire à soi-même
Là devant son miroir
Et s’écrire un poème
Embellir son histoire.

2.
Changer
Laisser les anathèmes
Laisser passer les loups
Au jour des chrysanthèmes
Se remettre debout.
Changer
Se relever quand même
Tutoyer les volcans
Nul besoin de diadèmes
Pour s’apprécier vraiment.

3.
Changer
Plus haut jusqu’à l’extrême
Comme le “Goéland”
Puisque ceux qui essaiment
Restent toujours vivants.
Changer
Et devenir soi-même
Devenir “plus” et “mieux”
Applaudir ceux qui s’aiment
Inviter le Bon Dieu.