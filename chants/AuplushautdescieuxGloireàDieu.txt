R.
AU PLUS HAUT DES CIEUX
GLOIRE À DIEU QUI NOUS AIME,
AU PLUS HAUT DES CIEUX
ALLÉLUIA !
PAIX DE JÉSUS-CHRIST,
JOIE DE DIEU SUR LA TERRE,
PAIX DE JÉSUS-CHRIST,
ALLÉLUIA !

1.
Nous te louons, Dieu notre Père,
Source d’amour et Créateur.
À toi, Dieu saint, nous rendons grâce ;
Béni sois tu pour tes merveilles !
Roi du ciel, nous t’adorons,
Roi du ciel, nous t’adorons.

2.
Nous te louons, ô Fils unique,
Toi Jésus-Christ, le Premier-Né.
Agneau de Dieu, Sauveur du monde,
Reçois le cri de nos prières :
Fils de Dieu, pitié pour nous,
Fils de Dieu, pitié pour nous !

3.
Nous te louons, Fils de lumière,
Toi le vivant, le seul Seigneur.
Ton règne est grand près de ton Père,
Dans l’Esprit Saint nous rendons gloire :
Dieu Très-Haut nous t’acclamons,
Dieu Très-Haut nous t’acclamons !