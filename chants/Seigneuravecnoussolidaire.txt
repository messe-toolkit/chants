R.
Seigneur avec nous solidaire,
tu veux des semeurs dans ton champ ?
Par toi grandiront sur la terre,
Les blés de justice à tous vents ;
Seigneur avec nous solidaire,
La paix mûrira dans ton champ !

1.
La terre enfante des exclus,
Millions d'humains à la dérive.
Des sœurs et frères de Jésus,
Sont prisonniers de la famine,
Si notre amour s'est refermé,
Qui viendra les délivrer ?

2.
Toujours en quête d'un pays,
Ton peuple immense est en exode,
Où donc es-tu, toi que l'on dit
Libérateur, ami des hommes ?
Où sont les œuvres de tes mains
Révèle toi sur nos chemins

3.
Vergers de Dieu nous deviendrons,
Et nos déserts seront fertiles.
En nous tu creuses les sillons,
Où s'enracine l'Évangile.
Tu donneras soleil et pluie,
Pour que nos arbres portent fruit.

4.
Justice et Paix s'embrasseront
Si nous croyons à la tendresse.
L'amour sera libération
Quand il vaincra toute misère.
Pour ces combats qu'il faut mener
Serons-nous prêts à tout donner ?

5.
Nous découvrons le grain qui meurt
Sur le sommet de la colline ;
Il a tout pris de nos douleurs
Mais son réveil nous illumine.
La nuit n'a pas le dernier mot,
La mort est morte à son tombeau.

6.
Quel arc-en-ciel sur l'univers
N'est déchiré par nos violences ?
Un chant pascal nous est offert
Quand Jésus Christ refait l'Alliance.
Pour la colombe et l'olivier
Louange au Prince de la paix !

7.
Maison de paix nous bâtirons
Sur le terrain des droits de l'homme,
Nations et peuples y trouveront,
La place offerte pour le pauvre.
Alors justice fleurira,
L'amour sera l'unique loi.

8.
Déjà la Terre d'avenir
A la couleur que je lui donne
Et le désert peut reverdir
Si c'est l'eau vive que j'apporte.
Au puits creusé l'espoir renaît :
Heureux qui fait lever la paix !