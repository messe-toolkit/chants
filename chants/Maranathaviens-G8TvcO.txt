R.
Maranatha, viens, Seigneur Jésus,
Que ton règne vienne bientôt !

1.
Voici, l´Époux vient au creux de la nuit.
Heureux le serviteur qu´il trouve vigilant.
Veillons donc à ne pas succomber au sommeil
Pour ne pas être vaincus par la mort et exclus du Royaume.

2.
Arrachons-nous à la torpeur et dans l´adoration,
Éveillons-nous :
Saint, Saint, Saint, es-tu notre Dieu,
Père des miséricordes, Verbe devenu homme,
Esprit de sainteté !
Par les prières de la mère de Dieu, aie pitié de nous.

3.
En ces temps qui sont les derniers,
Elle passe la figure de ce monde.
Voici l´Époux qui vient et il se tient aux portes :
Les vierges saintes courent à sa rencontre
Leurs lampes allumées, rayonnantes de joie.

4.
Les vierges folles, avec leurs lampes éteintes
Heurtent en vain les portes du festin du Royaume.
Aussi veillons, soyons sobres , gardons-nous transparents ,
Tendons dans l´espérance vers le Seigneur qui vient.