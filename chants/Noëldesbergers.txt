1.
C’est aujourd’hui qu’est né d’une vierge sans tache, Emmanuel :
un petit, pauvre et nu, couché dessus la paille, c’est la Noël !
Quand soudain réveillés par mille chants des anges, joyeusement,
les bergers, dans la nuit, s’en vont vers la crèche où dort l’enfant !

2.
Vite, bergers, c’est joie, c’est joie sur terre :
voici qu’est venue la lumière !
Jésus est né dans l’étable :
vous le verrez près de sa mère.
Vite, bergers, c’est joie sur terre :
voici qu’est venue la lumière !

3.
Or dites-nous, gentils bergers,
quelle merveille contemplez,
un Dieu dans une crèche,
c’était si grande fête !

4.
Vous étiez tant émerveillés,
que vous a dit ce nouveau-né,
qu’il faut aimer ses frères,
et plaire à Dieu son Père !

5.
Chantons ce roi qui vient de naître
pour nous en ce lieu misérable !
Dieu en ce jour tient promesse.
Et ce berceau dit sa tendresse.
Chantons ce roi qui vient de naître
pour nous en ce lieu misérable !

6.
C’est aujourd’hui qu’est né d’une vierge sans tache, Emmanuel !
un petit, pauvre et nu, couché dessus la paille, c’est la Noël !
Quand soudain réveillés par mille chants des anges, joyeusement,
les bergers, dans la nuit, s’en vont vers la crèche : c’est la Noël !