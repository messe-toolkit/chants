R.
JE NE TE VOIS PAS AVEC MES YEUX,
JE NE T’ENTENDS PAS À MON OREILLE,
TOI MON PÈRE QUI ES DANS LES CIEUX,
ET POURTANT JE T’AIME!

1.
Mais je vois le soleil,
les astres dans le ciel,
et je vois l’océan,
les arbres du printemps !
Alors je peux te retrouver
dans tout ce que tu as créé.

2.
Mais j’entends les oiseaux,
le chant des grandes eaux,
les cigales en été,
la brise dans les blés.
Alors je peux te retrouver
dans tout ce que tu as créé.