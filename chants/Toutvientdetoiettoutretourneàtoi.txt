1.
Tout vient de toi
Et tout retourne à toi,
Père très bon !
Ce pain, que tu nous donnes chaque jour,
Nous te le présentons :
Qu’il devienne pour nous
Le Corps du Christ !

Refrain :
Béni sois-tu, maintenant,
Béni sois-tu pour les siècles !

2.
Tout vient de toi
Et tout retourne à toi,
Père très bon !
Ce vin, qui réjouit le coeur de l’homme,
Nous te le présentons :
Qu’il devienne pour nous
Le Sang du Christ !

Refrain :
Béni sois-tu, maintenant,
Béni sois-tu pour les siècles !

3.
Tout vient de toi
Et tout retourne à toi,
Père très bon !
La vie, reçue de toi par le Baptême,
Nous te la remettons :
Qu’elle soit, dans tes mains,
Offrande pure !

Refrain :
Béni sois-tu, maintenant,
Béni sois-tu pour les siècles !