1.
Quand j’entends battre le cœur des hommes,
J’entends battre le cœur de Dieu.
Quand je partage la peur des hommes,
Je partage la peur de Dieu.

2.
Quand je soulage la faim des hommes,
Je soulage la faim de Dieu.
Quand je partage le pain des hommes,
Je partage le pain de Dieu.

3.
Quand je refuse la main des hommes,
Je refuse la main de Dieu.
Quand je partage la joie des hommes,
Je partage la joie de Dieu.