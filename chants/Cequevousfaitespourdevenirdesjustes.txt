CE QUE VOUS FAITES POUR DEVENIR DES JUSTES,
ÉVITEZ DE L’ACCOMPLIR DEVANT LES HOMMES
POUR VOUS FAIRE REMARQUER.

Sinon, il n’y a pas de récompense pour vous
auprès de votre Père qui est aux cieux.