1.
Dans le désert, je cherche ta Face.
Dans le désert, ton pain me nourrit.
Je ne crains pas d’avancer sur ta trace.
C’est pour ma soif que ton eau vive jaillit !

R.
Promis à Toi dans la justice,
Promis à toi dans l’amour,
Dans la fidélité,
Comme je suis connu, je connaîtrai !

2.
Dans le désert, j’entends ta Parole.
Dans le désert, loin de la rumeur.
Le souvenir de ta Loi me console.
Ô Dieu caché, tu veux parler à mon cœur !

3.
Dans le désert, j’aspire ton souffle.
Dans le désert, habite l’Esprit.
Il est la force, au matin, qui me pousse.
Il est le feu qui me précède dans la nuit !