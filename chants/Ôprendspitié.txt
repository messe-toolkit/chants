1.
Dieu, entends ma plainte, pitié, exauce ma prière,
Des terres lointaines, je t’appelle quand le coeur me manque.
Tu me conduiras jusqu’au rocher trop loin de moi
Car tu es pour moi un refuge face à l’ennemi.

Ô PRENDS PITIÉ, Ô MON SEIGNEUR !
MON DIEU, MON ROI, MON REMPART, MON SAUVEUR.
Ô PRENDS PITIÉ, Ô MON SEIGNEUR !
MON DIEU, MON REFUGE, ROC DE MON COEUR.

2.
Je n’ai d’autre pain, que mes larmes, le jour, la nuit,
Moi qui chaque jour, entends dire : « Où est-il ton Dieu ? »
Pourquoi te désoler, ô mon âme et rugir sur moi ?
Espère en ton Dieu ! De nouveau, oui, je lui rendrai grâce !

3.
Brisé, écrasé, à bout de force, mon coeur gronde.
Seigneur, mon désir est devant toi et rien ne t’échappe.
Tu es mon secours, mon Dieu, Seigneur, ne tarde pas !
Je suis pauvre et malheureux mais le Seigneur pense à moi !

Soyez forts, prenez courage, vous tous qui espérez le Seigneur.
Moi, je suis sûr de toi, je dis : « Tu es mon Dieu »
Sauve-moi par ton amour, toi qui as vu ma détresse,
Sur ton serviteur que s’illumine ta face !

JE TE LOUERAI, Ô MON SEIGNEUR !
CAR TU ENTENDS LA PLAINTE DE MON COEUR.
J’EXALTERAI, TA GRANDEUR !
TU T’ES FAIT PROCHE DE TON SERVITEUR.

Tu t’es fait proche de ton serviteur.
Tu as entendu le cri de mon coeur.