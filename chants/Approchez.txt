1.
La terre a vu se lever ce matin
La lueur d’un jour qui ne s’éteint pas.
Il balaye les ombres de nos coeurs las.
De sa victoire nous sommes témoins.

APPROCHEZ! DIEU RÉVÈLE SON NOM,
JÉSUS CHRIST PAROLE DE LA VIE.
APPROCHEZ! DIEU RÉVÈLE SON NOM,
PARTAGEONS LA PAIX DE SON ESPRIT.

2.
Les peuples, du plus proche au plus lointain,
Sont conviés au bonheur de son mystère.
Il provoque une espérance sans frontière.
De sa promesse nous sommes témoins.

3.
Le petit apaise en lui son chagrin :
Les derniers seront devant les premiers.
Il ouvre nos regards aux oubliés.
De sa justice nous sommes témoins.