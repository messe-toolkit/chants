Jésus, toi qui as promis d´envoyer l´Esprit
À ceux qui te prient,
Ô Dieu, pour porter au monde ton feu,
Voici l´offrande de nos vies.