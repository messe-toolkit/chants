R.
N’oublie pas Seigneur qu’ils ont tout quitté pour toi,
N’oublie pas Seigneur ceux qui marchent sur tes pas
N’oublie pas Seigneur qu’ils ont tout quitté pour toi,
N’oublie pas Seigneur ceux qui marchent sur tes pas !

1.
Ils sont partis au bout du monde
Guidés par ton Esprit
À chaque instant chaque seconde,
Pour vivre de ta vie !

2.
Ils ont connu toutes les guerres
Où tu les appelais,
Pour être au milieu de leurs frères
Des artisans de paix !

3.
Ils ont risqué dans la misère
Leur vie de chaque jour,
Pour faire avancer sur la terre
Les forces de l’Amour !