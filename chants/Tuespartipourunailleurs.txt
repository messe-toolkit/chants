1.
Souvent j'te cherche sans savoir
J' guette ton pas derrière la porte
Des fois j'me dis
qu' t'es en retard
Au moindre bruit
v'là qu'je sursaute
Souvent les jours de grand soleil
Mon regard va par la fenêtre
Mais rien n'est plus jamais pareil
J' voudrais tellement
t'y voir paraître

R.
MAIS TU ES PARTI POUR UN AILLEURS
PARTI AU VENT
COMME UNE FEUILLE
MAIS TU ES PARTI POUR UN AILLEURS
EN ME LAISSANT
PORTER TON DEUIL

2.
Des fois, j' t'imagine à la gare
Je me dis que tu es tout proche
Est-ce-que le train est en retard
Déjà je te fais des reproches

R.
MAIS TU ES PARTI POUR UN AILLEURS
PARTI AU VENT
COMME UNE FEUILLE
MAIS TU ES PARTI POUR UN AILLEURS
EN ME LAISSANT
PORTER TON DEUIL

3.
Souvent au long des jours de pluie
Au milieu d' tous ces gens
qui passent
A l'abri sous mon parapluie
J'attends toujours
que tu m'embrasses
J'ai du mal à dormir la nuit
te guettant d'un instant à l'autre
Très attentif au moindre bruit
Mais c'est vrai
tu n'est plus des nôtres

R.
MAIS TU ES PARTI POUR UN AILLEURS
PARTI AU VENT
COMME UNE FEUILLE
MAIS TU ES PARTI POUR UN AILLEURS
EN ME LAISSANT
PORTER TON DEUIL