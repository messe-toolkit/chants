VOICI NOTRE SAUVEUR QUI VIENT !

VOICI NOTRE SAUVEUR QUI VIENT, PRÉPARONS LE CHEMIN !
RESTEZ ÉVEILLÉS, IL VIENT SANS TARDER,
REVÊTEZ LES ARMES DE LUMIÈRE,
C'EST LE TEMPS DE LA VISITE !

1.
Heureux toi qui vigiles, tu entreras dans la joie,
La nuit va bientôt disparaître, le jour est avancé !

2.
Heureux toi qui écoutes, prêtant l'oreille à la voix
De celui qui frappe à la porte et veut nous rencontrer !

3.
Heureux toi qui rassembles toutes paroles en ton cœur,
Pour toi se lèvera bientôt l'étoile du matin !

4.
Heureux toi qui accueilles l'invitation au festin,
Tu recevras l'habit de noces, le bonheur d'être aimé !

5.
Heureux toi qui désires voir apparaître le ciel,
Au plus secret de notre Terre naîtra l'Emmanuel !