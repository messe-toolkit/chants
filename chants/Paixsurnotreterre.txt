R.
LA PAIX SOIT AVEC NOUS,
LA PAIX SUR NOTRE TERRE,
LA PAIX SOIT ENTRE NOUS,
LA PAIX POUR TOUS NOS FRÈRES.

1.
Offrir la paix,
La paix sur notre terre.
La paix du Christ,
La paix pour tous nos frères.

2.
Offrir l’amour,
L’amour sur notre terre.
L’amour du Christ,
L’amour pour tous nos frères.