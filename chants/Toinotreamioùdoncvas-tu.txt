1.
Toi notre ami, où donc vas-tu ?
Vas-tu au pays des vivants,
Des sources vives, des arcs-en-ciel ?
Toi notre ami, où donc vas-tu ?
Tiens-tu sur ton cœur le soleil ?
Bois-tu au secret des printemps ?
Toi notre ami, où donc vas-tu ?

2.
Toi notre ami, où donc vas-tu ?
Es-tu remonté de la nuit,
Hors de l’abîme, vaincue la mort ?
Toi notre ami, où donc vas-tu ?
Tes yeux s’ouvrent-ils éblouis ?
Dis-nous si ton cœur bat trop fort ?
Toi notre ami, où donc vas-tu ?

3.
Toi notre ami, où donc vas-tu ?
Vas-tu au pays des chansons,
Des farandoles, des tambourins ?
Toi notre ami, où donc vas-tu ?
Es-tu l’invité d’un festin ?
Vas-tu retrouver ta maison ?
Toi notre ami, où donc vas-tu ?

4.
Toi notre ami, où donc vas-tu ?
Sont-ils accourus te fêter ?
À ta rencontre combien d’amis ?
Toi notre ami, où donc vas-tu ?
Es-tu accueilli comme un fils ?
Ton Dieu est-il là pour t’aimer ?
Toi notre ami, où donc vas-tu ?