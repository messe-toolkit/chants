OFFERTOIRE
Tu es béni, Dieu de l'univers, toi qui nous donnes ce pain, fruit de la terre et du travail des hommes ;
Nous te le présentons, il deviendra le pain de la vie.
Tous : Béni soit Dieu, maintenant et toujours !
Tu es béni, Dieu de l'univers, toi qui nous donnes ce vin, fruit de la vigne et du travail des hommes ;
Nous te le présentons, il deviendra le vin du Royaume éternel.
Tous : Béni soit Dieu, maintenant et toujours.

PRIÈRE SUR LES OFFRANDES
Par Jésus le Christ notre Seigneur.
Tous : Amen.

PRÉFACE
Le Seigneur soit avec vous. Tous : Et avec votre esprit.
Élevons notre cœur.
Nous le tournons vers le Seigneur.
Rendons grâce au Seigneur notre Dieu. Tous : Cela est juste et bon.

Vraiment, il est juste et bon de te rendre gloire, de t'offrir notre action de grâce, toujours et en tout Lieu, à toi, Père très saint, Dieu éternel et tout-puissant, par le Christ, notre Seigneur.

ACCLAMATION
Nous rendons grâce à notre Dieu et nous le bénissons.

Dans le mystère de sa Pâque, il a fait une œuvre merveilleuse, car nous étions esclaves de la mort et Du péché, et nous sommes appelés à partager sa gloire ; (Acclamation)
Nous portons désormais ces noms glorieux : nation sainte, peuple racheté, race choisie, sacerdoce Royal ; (Acclamation)
Nous pouvons annoncer au monde les merveilles que tu as accomplies, toi qui nous as fait passer des Ténèbres à ton admirable lumière. (Acclamation)
C'est pourquoi, avec les anges et tous les saints, nous proclamons ta gloire, en chantant d'une seule Voix :

Saint ! Saint ! Saint, le Seigneur, Dieu de l'univers ! Le ciel et la terre sont remplis de ta gloire.
Hosanna au plus haut des cieux ! Béni soit celui qui vient au nom du Seigneur.
Hosanna au plus haut des cieux !

PRIÈRE EUCHARISTIQUE

Toi qui es vraiment saint, toi qui es la source de toute sainteté, Seigneur, nous te prions :
Sanctifie ces offrandes en répandant sur elles ton Esprit ; qu'elles deviennent pour nous le corps et Le sang de Jésus le Christ, notre Seigneur.

Tous : Vienne l'Esprit du Dieu très saint, sanctifier pour nous cette coupe et ce pain.

Au moment d'être livré et d'entrer librement dans sa passion, il prit le pain, il rendit grâce, il le rompit et le donna à ses disciples en disant : « Ceci est mon corps livré pour vous. »

Tous : Amen, Amen.

De même à la fin du repas, il prit la coupe ; de nouveau il rendit grâce, et la donna à ses disciples en Disant : « Prenez et buvez en tous, car ceci est la coupe de mon sang, le sang de l'Alliance nouvelle Et éternelle, qui sera versé pour vous et pour la multitude en rémission des péchés. Vous ferez cela En mémoire de moi. »

Tous : Amen, Amen.

ANAMNÈSE

Il est grand le mystère de la foi.
Tous : Nous proclamons ta mort, Seigneur Jésus ; nous célébrons ta résurrection ; nous attendons ta Venue dans la gloire.

Faisant ici mémoire de la mort et de la résurrection de ton Fils, nous t'offrons, Seigneur, le pain de La vie et la coupe du salut, et nous te rendons grâce, car tu nous as choisis pour servir en ta présence.
Humblement, nous te demandons qu'en ayant part au corps et au sang du Christ, nous, soyons Rassemblés par l'Esprit Saint en un seul corps.

Tous : Vienne l'Esprit du Dieu très saint, qu'il nous sanctifie par la coupe et le pain.

Souviens-toi, Seigneur, de ton Église répandue à travers le monde ; fais la grandir dans ta charité Avec le Pape…, notre Évêque… Et tous ceux qui ont la charge de ton peuple.

Tous : Nous t'en prions.

Souviens-toi aussi de nos frères qui se sont endormis dans l'espérance de la résurrection, et tous les Hommes qui ont quitté cette vie, reçois-les dans ta lumière auprès de toi.

Tous : Nous t'en prions.

Sur nous tous enfin, nous implorons ta bonté ; permets qu'avec la Vierge Marie, la bienheureuse Mère de Dieu, avec les apôtres et les saints de tous les temps qui ont vécu dans ton amitié, nous ayons part à la vie éternelle et que nous chantions ta louange, par Jésus-Christ, ton Fils bien-aimé. Par lui, avec lui…
Tous : Amen, Amen.