1.
D'une femme auprès d'un berceau
S'occupant de lange et de lait,
Qui pourrait lire le silence et les secrets ?
Tu n’as rien dit, Vierge Marie !
Dieu t'a choisie
Et c'est un ange qui nous dit que tout se change en Paradis.

2.
D'une femme un jour de Rameaux
Frémissant des peurs qu'elle tait,
Qui pourrait lire le silence et les secrets ?
Tu n’as rien dit, Vierge Marie !
Dieu t'a choisie
Et c'est un ange qui nous dit que tout se change en Paradis.

3.
D'une femme aux mains des bourreaux,
Déchirée quand claquent les fouets,
Qui pourrait lire le silence et les secrets ?
Tu n’as rien dit, Vierge Marie !
Dieu t'a choisie
Et c'est un ange qui nous dit que tout se change en Paradis.

4.
D'une femme au pied d'un poteau,
Regardant le Fils qu'elle avait,
Qui pourrait lire le silence et les secrets ?
Tu n’as rien dit, Vierge Marie !
Dieu t'a choisie
Et c'est un ange qui nous dit que tout se change en Paradis.

5.
D'une femme au bord d'un tombeau
Qui lui prend celui qu'elle aimait,
Qui pourrait lire le silence et les secrets ?
Tu n’as rien dit, Vierge Marie !
Dieu t'a choisie
Et c'est un ange qui nous dit que tout se change en Paradis.

6.
D'une femme au chant des oiseaux
Quand le Jour de Nuit se défait,
Qui pourrait lire le silence et les secrets ?
Tu n’as rien dit, Vierge Marie
Dieu t'a choisie
Et c'est un Ange qui nous dit que tout se change en Paradis.