R.
Rendons gloire à Dieu, notre Père,
Pour son Fils venu nous sauver.
Rendons gloire à Dieu, notre Père,
Il accomplit ce qu'il promet.

1.
Tous les lointains de la terre verront
Le salut de notre Dieu.
Tous les lointains de la terre diront
C'est Lui, le Seigneur !

2.
De son tombeau le Seigneur s'est levé,
Jésus ressuscité.
Par l'Esprit Saint il vient nous recréer
A l'image du Père.

3.
Et mon esprit exulte de joie
En Dieu mon sauveur.
Car je suis sûr de son amour
Pour moi, Alléluia.