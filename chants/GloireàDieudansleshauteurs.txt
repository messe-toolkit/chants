Gloire à Dieu dans les hauteurs du ciel !
La justice et la paix sur la terre des hommes !

1.
Le peuple qui marchait dans les ténèbres a vu se lever une grande lumière ;

2.
Tu as multiplié notre allégresse, tu as fait éclater notre joie ;

3.
Tu as brisé et réduit en poussière le joug qui meurtrissait nos épaules ;

4.
Seigneur, Fils unique, Jésus-Christ, Fils de David, Toi pour toujours.

Nous te louons, nous te glorifions pour ton immense gloire !

1.
Sur les habitants du pays de l’ombre, voici qu’une lumière a resplendi ;

2.
Tu nous as réjouis devant ta face, comme au jour de récolte et de vendange ;

3.
Tu as brûlé au feu de ta colère les armes des guerriers qui sèment la terreur ;

4.
Conseiller Merveilleux, Dieu Puissant, Père Éternel, et Prince de la paix,