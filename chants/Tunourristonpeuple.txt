Stance
Tu nourris ton peule De la fleur du froment, Dieu très bon !
Tu rassasies ton peuple du miel du rocher, Dieu très bon !
Aujourd’hui, tu le combles Du don de Jésus Christ !

R.
EN MÉMOIRE DE LA PÂQUE DE TON FILS,
NOUS TE CHANTONS, DIEU FIDÈLE !
ALLÉLUIA, ALLÉLUIA ! ALLÉLUIA, ALLÉLUIA !

1.
« Je suis la pain vivant qui es descendu du ciel » ; dit le Seigneur. (Jn 6)

2.
« Si quelqu’un mange de ce pain, il vivra éternellement. » (Jn 6)

3.
« Donnez-leur vous-mêmes à manger. » dit le Seigneur. (Jn 6)