Je te salue, Marie, pleine de grâce,
Le Seigneur est avec toi,
Tu es bénie entre toutes les femmes
Et Jésus, le fruit de tes entrailles, est béni.

Sainte Marie, Mère de Dieu,
Prie pour nous, pauvres pécheurs,
Maintenant et à l’heure de notre mort.
Amen.