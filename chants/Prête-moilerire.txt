R.
JE TE PRÊTERAI MON COEUR,
ET TU L'ENVAHIRAS.
QUAND TA JOIE DONNERA DES FLEURS,
MON COEUR GRANDIRA.

1.
Prête-moi le rire
Pour qu'il vogue dans ma voix,
Comme un navire,
Prête-moi ta joie !

2.
Prête-moi la danse
Pour qu'elle entre dans mes pas,
Et que j'avance,
Prête-moi ta joie !

3.
Prête-moi des ailes
Pour les mots qui viennent en moi,
En étincelles,
Prête-moi ta joie !

4.
Prête-moi la rime
Pour qu'elle porte jusqu'à toi
Cette comptine,
Prête-moi ta joie !

CODA
QUAND JE CHANTERAI POUR TOI,
ELLE VOLERA BIEN MIEUX,
MA COMPTINE INVENTEE POUR TOI,
COMPTINE A BON DIEU !