LE PARFUM DU LILAS,
LE BON GOÛT DU CHOCOLAT,
LA RONDE DES COULEURS
JÉSUS, QUEL BONHEUR !

1.
Tu me fais découvrir
Plein de trésors sous le soleil,
Les fleurs et les ruisseaux, les champs, les forêts.

2.
Tu me fais découvrir
Tant de bonn’ choses à partager,
Cadeaux du potager, cadeaux du verger.