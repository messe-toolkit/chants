R.
LES BLÉS RENDENT LA TERRE BLONDE
LES BLÉS RENDENT LE MONDE EN ATTENTE DE PAIN
MOISSONNEUR ENTENDS-TU LE MONDE QUI A FAIM?
MOISSONNEUR ES-TU PRÊT À MOISSONNER LE GRAIN?

1.
À l’aurore déjà, le semeur est sorti
Pour dix graines semées, il rêvait mille épis…
Les épis sont bien là, avant la nuit tombée
La terre était donc bonne et très bien préparée.

2.
Assis, en attendant que vienne un autre jour,
Le semeur est patient, comme sait l’être l’amour
Il attend d’autres mains apprêtées au labeur
Qui sauront se mouiller de perles de sueur.

3.
Toi, seras-tu de ceux qui répondront : « présent »
Debout, au jour nouveau, à travailler au champ?
Toi, seras-tu de ceux qui apportent au moulin
De quoi moudre, espérer en d’autres lendemains ?

4.
Dieu n’est pas un “faucheur”, Dieu se fait le meunier
Qui saura séparer le bon grain de l’ivraie
Il roulera la pierre, comme il roula la mort
Et le pain sera bon, et le pain rendra fort.