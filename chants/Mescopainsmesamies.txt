R.
MES COPAINS, MES AMIES
DE SOLEIL ET DE PLUIES,
JE CROIS BIEN, IL ME SEMBLE,
QUE NOUS ETIONS ENSEMBLE.
VOUS SEMBLE T'IL AUSSI ?

1.
Est-ce la pluie ou bien le vent
Qui nous tirait si loin devant ?
Est-ce la pluie ou le soleil
Qui nous tirait de nos sommeils ?
Moi, il me semble que c’est à force d’aimer
Que, tous ensemble, nous nous sommes apprivoisés.

2.
Est-ce le froid ou bien la faim
Qui nous serrait autour du pain ?
Est-ce le froid ou les moissons
Qui faisait lever nos chansons ?
Moi, il me semble que c’est à force d’aimer
Que, tous ensemble, sur la route je suis né.

3.
Est-ce la grange ou bien le foin
Qui poussait les chemins si loin ?
Est-ce la grange ou bien la pluie
Qui nous faisait si hauts les Puys ?
Moi, il me semble que c’est à force d’aimer
Que, tous ensemble, nous avons pu nous nommer.

4.
Est-ce la gentiane ou le thym
Qui nous poussait dans le matin ?
Est-ce la gentiane ou le temps
Qui nous faisait le coeur content ?
Moi, il me semble que c’est à force d’aimer
Que, tous ensemble, les conflits sont exprimés.

5.
Est-ce la crête ou le versant
Qui nous faisait rager le sang ?
Est-ce la myrtille ou les blés
Qui faisait le ciel étoilé ?
Moi, il me semble que c’est à force d’aimer
Que, tous ensemble, nous étions si affamés.

6.
Finis la montagne et le sac,
Finis la bruyère et les lacs
Finis les filles et les garçons,
Finis les refrains, les chansons.
Mais il me semble que c’est à force d’aimer
Que, tous ensemble, nous pourrons bâtir la paix.