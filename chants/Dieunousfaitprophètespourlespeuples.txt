R.
Dieu nous fait prophètes pour les peuples,
Il nous veut l’image de son Fils.
Christ est l’Envoyé parmi ses frères,
(bis) Mais son chant d’amour est-il compris ? (bis)

1.
Ne dis pas : « Je ne suis qu’un enfant ! »
Dieu sait bien que ton coeur est ardent.
Tu iras sur les pas du Vivant,
Son message est un feu dévorant.

2.
Dieu te dit : « Maintenant ne crains pas !
N’aie pas peur car je suis avec toi! »
Dans le monde aujourd’hui sois ma voix,
Par ta vie mon amour se dira.

3.
Dieu t’envoie où le mal est semé;
Cette ivraie, tu devras l’arracher.
Tu iras pour bâtir et planter ;
Fais lever la justice et la paix !