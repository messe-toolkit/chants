R.
Tu es le chemin, Seigneur, la vérité et la vie.

Psaume 24
Seigneur, enseigne-moi tes voies,
fais-moi connaître ta route.
Dirige-moi par ta vérité, enseigne-moi,
car tu es le Dieu qui me sauve.
Rappelle-toi, Seigneur, ta tendresse,
ton amour qui est de toujours.
Oublie les révoltes, les péchés de ma Jeunesse;
dans ton amour, ne m'oublie pas.
Il est droit, il est bon, le Seigneur,
lui qui montre aux pécheurs le chemin.
Sa justice dirige les humbles,
il enseigne aux humbles son chemin.