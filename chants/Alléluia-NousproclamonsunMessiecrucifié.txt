ALLÉLUIA, ALLÉLUIA.

Nous proclamons un Messie crucifié,
folie pour les hommes,
mais puissance et sagesse de Dieu.

ALLÉLUIA.