R.
Hosanna, hosanna, hosanna !
Béni soit le Seigneur le roi des rois.

1.À lui le monde et sa richesse,
La terre et son peuplement.
C´est lui qui l´a fondée sur les mers.

2.
Portes, levez vos frontons,
Levez-vous, portes éternelles.
Qu´il entre le Roi de gloire.

3.
Qui est donc ce Roi de gloire ?
Le Seigneur, le fort, le vaillant,
Le Seigneur, le vaillant des combats.

4.
Qui est donc ce Roi de gloire ?
Le Seigneur, Dieu de l´univers,
C´est lui le Roi de gloire.