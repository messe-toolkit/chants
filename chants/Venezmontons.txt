R.
Venez, montons à la montagne du Seigneur
Pour qu'il nous enseigne ses voies.

1.
Moi, le Seigneur, je t'enseigne ce qui t'est salutaire,
Je te fais cheminer sur les sentiers où tu marches.

2.
Je suis le Chemin, la Vérité et la Vie.
Qui me suit ne marche pas dans les ténèbres.