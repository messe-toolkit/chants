1.
Nous marchons sans avancer :
Pas de brèches à nos impasses.
Nous tâtonnons au hasard :
Pas d'écho à notre voix.
Tu es muré en toi-même :
Tu crois vivre, il faut renaître !
L'aveugle sait-il
Que l'amandier est en fleurs ?
Et le prisonnier, que la nuit touche à sa fin ?
Ouvre les yeux, redresse-toi :
Ne vois-tu pas le jour de Dieu ?
Ouvre les yeux, redresse-toi :
Ne vois-tu pas le jour de Dieu ?

2.
Le Seigneur habite en toi,
Escorté de sa Justice :
Il ôtera ta tristesse,
Te vêtira de sa gloire.
Non, tu n'es pas l'Abandonné :
Car sa lumière est ta lumière !
L'aveugle verra
Que l'amandier est en fleur,
Et le prisonnier, que la nuit touche à sa fin !
Ouvre les yeux, redresse-toi :
Ne vois-tu pas le jour de Dieu ?
Ouvre les yeux, redresse-toi :
Ne vois-tu pas le jour de Dieu ?