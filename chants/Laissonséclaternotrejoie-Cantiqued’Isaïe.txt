LAISSONS ÉCLATER NOTRE JOIE :
DIEU EST AU MILIEU DE NOUS.

1.
Voici le Dieu qui me sauve :
j’ai confiance, je n’ai plus de crainte.
Ma force et mon chant, c’est le Seigneur ;
il est pour moi le salut.

2.
Rendez grâce au Seigneur,
proclamez son nom,
annoncez parmi les peuples ses hauts faits !
Redites-le : « Sublime est son nom ! »

3.
Jouez pour le Seigneur,
car il a fait les prodiges que toute la terre connaît.
Jubilez, criez de joie, habitants de Sion,
car il est grand au milieu de toi, le Saint d’Israël !