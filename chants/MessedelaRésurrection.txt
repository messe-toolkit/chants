SEIGNEUR, PRENDS PITIÉ

Seigneur, prends pitié, Seigneur, prends pitié.
Seigneur, prends pitié, Seigneur, prends pitié.

Ô Christ, prends pitié, Ô Christ, prends pitié.
Ô Christ, prends pitié, Ô Christ, prends pitié.

Seigneur, prends pitié, Seigneur, prends pitié.
Seigneur, prends pitié, Seigneur, prends pitié.

- - - - - - - - - - - - - - - - - - - -

GLOIRE À DIEU

Gloire à Dieu, au plus haut des cieux,
et paix sur la terre aux hommes qu’il aime.

Nous te louons, nous te bénissons, nous t’adorons,
Nous te glorifions, nous te rendons grâce, pour ton immense gloire,
Seigneur Dieu, Roi du ciel, Dieu le Père tout-puissant.

Seigneur, Fils unique, Jésus Christ,
Seigneur Dieu, Agneau de Dieu, le Fils du Père ;
Toi qui enlèves le péché du monde, prends pitié de nous ;
Toi qui enlèves le péché du monde, reçois notre prière ;
Toi qui es assis à la droite du Père, prends pitié de nous.

Car toi seul es saint,
Toi seul es Seigneur,
Toi seul es le Très-Haut : Jésus Christ, avec le Saint-Esprit
Dans la gloire de Dieu le Père. Amen, Amen.

- - - - - - - - - - - - - - - - - - - -

ALLÉLUIA

Alléluia, Alléluia, Alléluia, Alléluia,
Alléluia, Alléluia, Alléluia, Alléluia.
Verset du 29e dimanche du temps ordinaire C ; on l’adaptera bien entendu à la liturgie du jour.
Elle est vivante, efficace, la parole de Dieu : Elle juge des intentions des pensées et des cœurs.

- - - - - - - - - - - - - - - - - - - -

SANCTUS

Saint ! Saint ! Saint, le Seigneur, Dieu de l’univers !
Saint ! Saint ! Saint, le Seigneur, Dieu de l’univers !

Le ciel et la terre sont remplis de ta gloire.
Hosanna au plus haut des cieux.
Hosanna au plus haut des cieux.

Béni soit celui qui vient au nom du Seigneur.
Hosanna au plus haut des cieux.
Hosanna au plus haut des cieux.

- - - - - - - - - - - - - - - - - - - -

ANAMNÈSE

Il est grand, le mystère de la foi :
Nous proclamons ta mort, Seigneur Jésus, nous célébrons
ta résurrection, nous attendons ta venue dans la gloire.

- - - - - - - - - - - - - - - - - - - -

DOXOLOGIE

Par lui, avec lui et en lui, à toi, Dieu le Père tout-puissant,
dans l’unité du Saint-Esprit, tout honneur et toute gloire, pour les siècles des siècles.

Amen, Amen, Amen.

- - - - - - - - - - - - - - - - - - - -

AGNEAU DE DIEU

Agneau de Dieu, qui enlèves le péché du monde, prends pitié de nous.
Agneau de Dieu, qui enlèves le péché du monde, prends pitié de nous.
Agneau de Dieu, qui enlèves le péché du monde, donne-nous la paix.