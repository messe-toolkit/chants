1.
Quarante années dans le désert ils ont marché.
Quarante années dans le désert ils ont crié :
Toi, Seigneur, tu peux nous sauver !
Toi, Seigneur, tu peux nous sauver !

2.
Quarante années dans le désert ils ont chanté.
Quarante années dans le désert ils ont dansé :
Toi, Seigneur, tu sais nous aimer !
Toi, Seigneur, tu sais nous aimer !

3.
Quarante jours dans le désert tu t’es caché.
Quarante nuits dans le désert tu as lutté.
Toi, Jésus, tu viens nous sauver.
Toi, Jésus, tu viens nous sauver.

4.
Quarante jours dans le désert tu as jeûné.
Quarante nuits dans le désert tu as prié.
Toi, Jésus, tu viens nous aimer.
Toi, Jésus, tu viens nous aimer.

5.
Dans nos déserts, quarante jours, nous te cherchons.
Dans nos déserts, quarante nuits, nous t’espérons.
Toi, Jésus, tu donnes la vie !
Toi, Jésus, tu donnes la vie !