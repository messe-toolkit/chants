NE CHERCHONS PLUS LE CHRIST À L’OMBRE DE NOS MORTS
CAR NOUS SOMMES LE CORPS DE SA RÉSURRECTION.
ALLÉLUIA, ALLÉLUIA!

Ils couraient tous les deux ensemble.
En se penchant, il voit le linceul resté là.
Et cependant, il n’entre pas.

1.
Partir vers le jardin
Enveloppé de nuit
Courir vers le tombeau
Où le linge est posé
Pour atteindre le seuil
Sans pouvoir y entrer.

Pourquoi pleures-tu ?
Qui cherches-tu ?
Dis-moi où tu l’as mis et moi,
J’irai le prendre.

2.
Rester là et pleurer
L’Amour qui s’est perdu
Immobile, guetter
Un regard inconnu
Pour espérer trouver
Celui qu’on n’attend plus.

Tandis qu’ils parlaient,
Jésus lui-même s’approcha
Et il marcha avec eux.

3.
Prendre route pour fuir
La promesse évanouie
Mais rencontrer le pas
De celui qui traduit
L’écriture d’hier
En récit d’aujourd’hui.

Reste avec nous…
Ils le reconnurent
Mais il disparut à leur regard.
Alors ils retournèrent à Jérusalem.

4.
Inviter l’inconnu
Pour retenir le temps
Puis découvrir Ton nom
Dans cet effacement
Qui ouvre vers le jour
Où marchent les vivants.