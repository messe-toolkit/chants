1.
Au soir d’Amour, parlant sans parabole,
Jésus disait : «Si quelqu’un veut m’aimer,
Toute sa vie qu’il garde ma Parole, Mon Père et moi viendrons le visiter.
Et de son cœur, faisant notre demeure,
Venant à lui, nous l’aimerons toujours,
Rempli de paix, nous voulons qu’il demeure en notre amour, en notre amour.

4.
Vivre d’Amour, c’est garder en soi-même
Un grand trésor en un vase mortel.
Mon bien-aimé, ma faiblesse est extrême.
Ah ! Je suis loin d’être un ange du ciel.
Mais si je tombe à chaque heure qui passe,
Me relevant, tu viens à mon secours.
À chaque instant, tu me donnes ta grâce : je vis d’amour, je vis d’amour.