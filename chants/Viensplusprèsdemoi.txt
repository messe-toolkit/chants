1.
Lorsque l'orgueil se dresse en moi
Plus fort et plus haut que ta voix
Viens plus près, Seigneur
Viens plus près de moi !
Lorsque sur le chemin
La nuit tombe soudain
Et que je ne peux plus voir ton visage
Lorsque tout devient lourd
Sans joie et sans amour
Et que mon cœur humain devient sauvage
Lorsque l'orgueil se dresse en moi
Plus fort et plus haut que ta voix
Viens plus près, Seigneur
Viens plus près de moi.

2.
Lorsque la crainte hurle en moi
Plus fort et plus haut que ta joie
Viens plus près, Seigneur
Viens plus près de moi !
Lorsqu'il faudrait là-haut
Monter comme un oiseau,
Et que mes pieds sont lourds sur cette terre…
Lorsqu'il faudrait crier mon chant de liberté
Et qu'au fond de mon cœur, je désespère…
Lorsque la crainte hurle en moi
Plus fort et plus haut que ta joie
Viens plus près, Seigneur
Viens plus près de moi.

3.
Lorsque la bête hurle en moi
Plus fort et plus haut que ta loi
Viens plus près, Seigneur
Viens plus près de moi !
Lorsque le jour s'enfuit,
Et que j'entends les cris
De toutes les chimères de mon âme…
Lorsqu'au fond de mon cœur, soudain, j'ai froid et peur
Et que je ne sens plus ta vive flamme…
Lorsque la bête hurle en moi
Plus fort et plus haut que ta loi
Viens plus près, Seigneur
Viens plus près de moi.