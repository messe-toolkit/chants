1.
Toi Jésus, tu es passé en faisant le bien,
Tout’ ta vie tu as consolé et soulagé
Et pourtant la foul’ en colèr’ veut que tu meures,
Chez Ponce-Pilat’ ils te conduisent, ils te condamnent.

PARDON JÉSUS, POUR TANT DE HAINE,
PARDON JÉSUS POUR LA VIOLENCE,
SUR TOI, ON SE DÉCHAÎNE,
PARDON, POUR CES SOUFFRANCES,
PARDON JÉSUS, PARDON, JÉSUS.

2.
Toi Jésus, tu as nourri ton peuple affamé,
Tout’ ta vie tu as écouté et pardonné,
Et pourtant les homm’ déchaînés vont te clouer,
Sur une croix pour que tu meures et disparaisses.

3.
Toi Jésus, tu vois combien tes enfants sont sourds,
Tout’ ta vie tu as annoncé l’amour de Dieu,
Pourtant les grands, tout-puissants, fuient ta Parole
Tu meurs en croix, tu dis « oh Père, pardonne-leur ! »