R.
Sur les routes des hommes,
Le Seigneur nous attend
Pour bâtir son Royaume de justice et de paix,
Pour bâtir son Royaume de justice et d´amour.

1.
Va sans bagages, Pauvre de tout argent.
Va sur les routes, Avec un cœur chantant
Je veux faire de toi, Un messager de paix
Je veux faire de toi, Un témoin de l'amour

2.
Va vers le large, Et jette le filet.
Va, sois sans crainte, Car je t'ai appelé
Je veux faire de toi, Un messager de paix
Je veux faire de toi, Un témoin de l'amour

3.
Va vers tes frères, Avec un cœur d'enfant
Va dire aux hommes :"Jésus Christ est vivant"
Je veux faire de toi, Un messager de paix
Je veux faire de toi, Un témoin de l'amour