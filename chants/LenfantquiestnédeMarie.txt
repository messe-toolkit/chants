1.
L’Enfant qui est né de Marie
N’est pas couché dans un berceau,
Il n’a ni maison ni patrie,
Juste un abri pour animaux.
Pourtant il est Dieu sur la terre,
Incognito parmi les siens.
Nous proclamons le grand mystère :
Dieu venu chez les humains.

2.
Premiers arrivés à l’étable,
Répondant à la voix des cieux,
Des gens assez peu fréquentables,
Des bergers, des « pauvres-de-Dieu ».
Pourtant bien avant tous les autres,
Gens d’écriture et gens de Loi,
Ils seront les premiers apôtres,
De Jésus, le Messie Roi.

3.
Voici que surviennent les Mages,
Avec l’or, la myrrhe et l’encens.
Hérode convoque les sages :
Il redoute, il craint cet Enfant !
Les Mages retrouvent l’étoile
Qui les guidait avec bonheur :
Pour eux le secret se dévoile
De Jésus, Messie Sauveur.