R.
FAIS-MOI COURIR À TA RENCONTRE,
OUVRE L’ESPACE DE TES BRAS,
OFFRE-TOI À MES YEUX BAISSÉS,
ÉTONNÉS DE TE VOIR SI PROCHE.

1.
Je ne suis plus qu’un arbre en cendres.
REDRESSE-MOI, LÈVE-MOI !

2.
Mon temps est brûlé, mes jours flambent.
REDRESSE-MOI, LÈVE-MOI !

3.
Je ne tiens que par ta parole.
REDRESSE-MOI, LÈVE-MOI !

4.
Tu es la tendresse ancienne.
REDRESSE-MOI, LÈVE-MOI !

5.
L’éternel sourire du monde.
REDRESSE-MOI, LÈVE-MOI !