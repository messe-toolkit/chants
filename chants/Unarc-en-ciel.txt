Un arc-en-ciel
Se lève dans ma voix
Un arc-en-ciel
Qui ne s'efface pas.
Un arc-en-ciel
De mots magiques
Couleur d'amour
Et de musique.
Un arc-en-ciel
Pour faire un pont
Entre ton coeur
Et ma chanson.