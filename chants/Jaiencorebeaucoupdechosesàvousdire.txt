« J’ai encore beaucoup de choses à vous dire,
Mais pour l’instant vous ne pouvez pas les porter.
Quand il viendra, lui, l’Esprit de vérité,
Il vous conduira dans la vérité tout entière. »