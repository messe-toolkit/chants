R.
C’ÉTAIT UN PETIT SANTON
QUI MARCHAIT PRÈS D’UN MOUTON
VERS L’ÉTOILE DE NOËL
C’ÉTAIT LE PETIT BERGER.
D’UNE PLANÈTE OUBLIÉE
COMME UNE ÎLE AU FOND DU CIEL.

1.
Un drôle de petit homme
Avec sur la tête
Plein d’épis de blé
Un drôle de petit homme
Avec des chaussettes
Mais pas de souliers
Qui s’en allait le nez en l’air
Sur un chemin de courant d’air.

2.
Un drôle de petit homme
Avec une écharpe
En plumes d’arc-en-ciel
Un drôle de petit homme
Avec une écharpe
En rayons de soleil
Qui n’avait rien d’une chanson
Pour l’enfant Dieu petit garçon !

3.
Un drôle de petit homme
Avec des pervenches
Tout au fond des yeux
Un drôle de petit homme
Avec une branche
D’aubépine bleue
Qui savait mieux que tous les grands
Ce qui enchante les enfants.