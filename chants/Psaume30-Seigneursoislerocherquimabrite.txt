SEIGNEUR, SOIS LE ROCHER QUI M’ABRITE.

1.
En toi, Seigneur, j’ai mon refuge ;
garde-moi d’être humilié pour toujours.
Dans ta justice, libère-moi ;
écoute, et viens me délivrer.

2.
Sois le rocher qui m’abrite,
la maison fortifiée qui me sauve.
Ma forteresse et mon roc, c’est toi :
pour l’honneur de ton nom, tu me guides et me conduis.

3.
Sur ton serviteur, que s’illumine ta face ;
sauve-moi par ton amour.
Soyez forts, prenez courage,
vous tous qui espérez le Seigneur !