Notre Père, qui es aux cieux,
Que ton Nom soit sanctifié,
Que ton Règne vienne,
Que ta volonté soit faite sur terre comme au Ciel
Donne-nous aujourd'hui notre pain de ce jour
Pardonne-nous nos offenses
Comme nous pardonnons aussi à ceux qui nous ont offensés ;
Et ne nous soumets pas à la tentation
Mais délivre-nous du Mal.
Car c'est à toi qu'appartiennent
Le Règne la puissance et la gloire
Pour les siècles des siècles. Amen