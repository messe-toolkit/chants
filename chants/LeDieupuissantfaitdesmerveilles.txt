R.
Le Dieu puissant fait des merveilles ;
Dans son amour il t'a choisie.
Le Dieu puissant fait des merveilles,
Ta vie tressaille sous l'Esprit.

1.
Fille d'un peuple en chemin,
Myriam éclose sous la grâce,
Dans notre histoire tu prends place,
Discrète voix du Dieu très saint.

2.
Toute au Seigneur d'Israël,
Tu mets ta joie dans son Alliance.
Par les sentiers de l'espérance
Tu te prépares à son appel.

3.
Coeur où l'amour a grandi,
Avec Joseph tu prends la route,
Et Dieu vous tient à son écoute
Pour inventer un avenir.

4.
Vierge audacieuse en ta foi,
Tu crois le Dieu de l'impossible :
Il t'a bénie par sa visite,
L'Emmanuel prend chair en toi.

5.
Femme affrontée aux comment
Et aux pourquoi qui sont les nôtres,
Tu nous apprends que le royaume
Est un combat de chaque instant.

6.
Mère attentive à ton fils,
Tu vois le monde à sa lumière.
Quand il te parle de son Père,
Ton coeur médite ce qu'il dit.

7.
Femme debout dans la nuit,
Ta vie connaît de longs silences
Et la douleur des jours d'errance
Quand le Calvaire te meurtrit.

8.
Toi dont les yeux sont ouverts
Sur le vivant du jour de Pâques,
Éveille-nous à son passage
Sur une terre où il se tait.

9.
Dans le cénacle où tu pries
Souffle un grand vent de Pentecôte :
L'Esprit de feu sur les Apôtres
Veut se répandre à l'infini.

10.
Joie dans le ciel, ô Marie !
Jésus t'accueille dans sa gloire.
À ton regard il se dévoile ;
Heureuse es-tu de vivre en lui !