JE TE DIS BON-JOUR,
JE TE SOUHAITE UN BON JOUR.
QUE CE JOUR SOIT BON,
COMME TOI, SEIGNEUR,
TU LE VEUX POUR NOUS.

1.
Dès le début du jour
Mille merveilles sont posées là
Mille merveilles tout près de moi.
Et tout au long du jour
Mille merveilles sont placées là,
Mille merveilles, cadeaux pour moi.

2.
Dès le début du jour
Tant de copains sont donnés là
Tant de copains auprès de moi.
Et tout au long du jour,
De l’amitié déposée là,
De l’amitié, cadeau pour moi.

3.
Dès le début du jour
J’ai des parents chemin pour moi,
J’ai des parents auprès de moi.
Et tout au long du jour,
Tout plein d’Amour qui m’est donné,
Tout plein d’Amour, cadeau pour moi.