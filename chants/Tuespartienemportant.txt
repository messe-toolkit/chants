1.
Tu es parti en emportant
Une part essentielle de moi-même,
Que vais-je faire maintenant
De nos projets de nos : « je t’aime » ?
Et tu me vois perdu, errant,
Là, au milieu des chrysanthèmes,
Tu es parti en emportant
Une part essentielle de moi-même.

IL Y AURA TOUJOURS
UNE LAMPE QUI BRILLE,
IL Y AURA TOUJOURS
UNE LAMPE ALLUMÉE. bis

2.
Tu es parti en emportant
L’essentiel de mon existence,
Que vais-je faire maintenant
De ces journées sans importance ?
Je n’ai plus rien : que seulement
Une présence dans l’absence,
Tu es parti en emportant
L’essentiel de mon existence.

3.
Tu es parti en me laissant
Bien plus qu’une immense blessure,
Que vais-je faire maintenant
De toutes ces journées si dures ?
Mais tu me pousses, je le sens,
À inventer ma vie future
Pour aller encore de l’avant,
J’ferai d’mon mieux, je te l’assure.

Coda
Sainte Marie, Salve Regina,
Mater misericordiae.
Sainte Marie, Salve Regina,
Mater misericordiae.

DONNONS-NOUS RENDEZ-VOUS,
LÀ-BAS SUR « L’AUTRE RIVE »
EN GARDANT, JUSQU’AU BOUT
CETTE LAMPE ALLUMÉE… bis