CHRIST ÉLEVÉ DANS LA GLOIRE DES CIEUX,
CHRIST AVEC NOUS JUSQU’À LA FIN DES TEMPS,
CHRIST HABITÉ PAR LE SOUFFLE DE DIEU,
TU METS TA GLOIRE À SAUVER TOUT VIVANT.

1.
Pour que l’homme soit au plus près du Très-Haut,
Jésus Christ, tu viens près de nous au plus bas :
Verbe qui assume notre chair,
Fils de l’homme élevé sur la croix,
Témoin de l’amour de ton Père.

2.
Toi qui es passé sur la rive inconnue,
Tu demeures aussi dans le champ de nos vies :
Verbe qui nous parles d’avenir,
Fils de Dieu revenu de la mort,
Témoin des matins de lumière.

3.
Avec nous tu prends le chemin d’Emmaüs,
Avec nous tu restes et partages le pain :
Verbe qui nous veux à ton festin,
Fils de Dieu notre Pâque aujourd’hui,
Témoin d’une table de fête.

4.
Avec toi, Seigneur, nous montons vers la joie,
Avec toi qui es le bonheur espéré :
Verbe descendu nous éclairer,
Fils de Dieu devenu notre paix,
Témoin de l’Alliance éternelle.

5.
Avec ton Esprit nous vivons tes combats,
Il nous est donné pour grandir dans l’amour :
Verbe qui nous mènes vers le Jour,
Fils de Dieu dans un monde à guérir,
Témoin de la Terre nouvelle.