R.
UN CHEMIN D’ÉVANGILE
AU SOLEIL DU RESSUSCITÉ,
DIEU NOUS OUVRE LE LIVRE
AU PAIN DE LA FRATERNITÉ.

1.
Des paroles de feu,
Gloire à Dieu :
Jésus est vivant !
Un peuple de prophètes,
Une fête :
Jésus est vivant !

2.
Des paroles de joie,
Notre foi :
Jésus est vivant !
Un peuple de prière,
Sa lumière :
Jésus est vivant !

3.
Des paroles de vie
Dans l’Esprit :
Jésus est vivant !
Un peuple de partage,
Son message :
Jésus est vivant !

4.
Paroles du pardon
En son nom :
Jésus est vivant !
Un peuple de courage,
Un passage :
Jésus est vivant !