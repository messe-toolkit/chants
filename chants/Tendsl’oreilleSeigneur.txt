R.
TENDS L’OREILLE, SEIGNEUR,
RÉPONDS-MOI,
ENTENDRAS-TU MA VOIX?
MA PRIÈRE
QUI N’ESPÈRE QU’EN TOI.

1.
Pour “les sans défense”,
Les “sans Espérance”,
Fatigués, déçus et inquiets ;
Pour tous ceux qui doutent,
Qui perdent la route,
Pour ce monde en quête de Paix.

2.
Chaque fois qu’un homme
Crie justice au monde,
Près de lui, Seigneur, es-tu là ?
Chaque fois qu’un homme
Part au bout du monde
Pour donner sa vie, es-tu là ?

3.
De ce mal subi,
De ce mal rendu
Et du mal que nous avons fait ;
Pour te retrouver
Et vouloir “Ta” Paix,
Donne-nous la force d’aimer.