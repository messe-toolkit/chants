ANTIENNE
"Que je marche en Espérance vers Toi le plus haut sommet, dans la gloire du Père."

1.
Au Seigneur le monde et sa richesse,
la terre et tous ses habitants !

2.
C’est lui qui l’a fondée sur les mers
et le garde inébranlable sur les flots.

3.
Qui peut gravir la montagne du Seigneur
et se tenir dans le lieu Saint ?

4.
L’homme au coeur pur, aux mains innocentes,
qui ne livre pas son âme aux idoles.

5.
Il obtient du Seigneur la bénédiction,
et de Dieu son Sauveur, la justice.

6.
Voici le peuple de ceux qui le cherchent !
Voici Jacob qui recherche ta Face!