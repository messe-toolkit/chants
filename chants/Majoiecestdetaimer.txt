R.
QUE ME FONT LA MORT OU LA VIE ?
JESUS, MA JOIE C’EST DE T’AIMER.
JESUS, MA JOIE C’EST E T’AIMER.

1.
Si Dieu est pour nous qui sera contre nous ?
Il n’a pas refusé son propre Fils,
il l’a livré pour nous tous !
Comment pourrait-il avec lui
ne pas nous donner tout ?

2.
Qui accusera ceux que Dieu a choisis ?
puisque c’est Dieu qui justifie.
Qui pourra condamner ?
puisque Jésus Christ est mort;
plus encore : il est ressuscité,
il est à la droite de Dieu,
et il intercède pour nous.

3.
Qui pourra nous séparer de l’amour du Christ ?
La détresse ? l’angoisse ? la persécution ? la faim ?
le dénuement ? le danger ? le supplice ?
Non, car en tout cela nous sommes les grands vainqueurs
grâce à celui qui nous a aimés.

4.
J’en ai la certitude : ni la mort ni la vie,
ni les esprits ni les puissances,
ni le présent ni l’avenir,
ni les astres, ni les cieux, ni les abîmes,
ni aucune autre créature,
rien ne pourra nous séparer de l’amour de Dieu
qui est en Jésus-Christ notre Seigneur