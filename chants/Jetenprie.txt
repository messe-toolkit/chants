- MERCI
- DE RIEN
- JE T'EN PRIE
- C’EST GRATUIT
« DEMANDEZ, VOUS RECEVREZ »
L’AMOUR EST INFINI
- S’IL TE PLAÎT
- JE T'EN PRIE
- C’EST COMBIEN
- C’EST GRATUIT
« DEMANDEZ, VOUS RECEVREZ »
L’AMOUR EST INFINI

1.
Ici rien n’est à prendre
Ici rien n’est à vendre
Saisissons l’occasion
Transformons la maison
Ici tout est offert
Les outils et les pierres
Louons le bâtisseur
Ouvrons notre demeure

2.
Entrons dans la demeure
Et visitons nos coeurs
Prenons place à la table
Pour un temps véritable
Ecoutons en conscience
Avançons en confiance
Méditons, contemplons
Regardons l'horizon

Coda
Dieu merci.