Antienne - stance
Ils viendront nombreux de l'Orient et de l'Occident prendre place au festin du Royaume.

R.
In manus tuas, Pater, commendo spiritum meum.

Versets

Ps 85.

1.
Prends pitié de moi, Seigneur,
toi que j'appelle chaque jour.
Seigneur, réjouis ton serviteur :
vers toi, j'élève mon âme !

2.
Toi qui es bon et qui pardonnes,
plein d'amour pour tous ceux qui t'appellent,
Écoute ma prière, Seigneur,
entends ma voix qui te supplie.

3.
Je t'appelle au jour de ma détresse,
et toi, Seigneur, tu me réponds.
Aucun parmi les dieux n'est comme toi,
et rien n'égale tes oeuvres.

4.
Toutes les nations, que tu as faites,
viendront se prosterner devant toi
car tu es grand et tu fais des merveilles,
toi, Dieu, le seul.

Ou bien

Ps 102.

1.
Bénis le Seigneur, ô mon âme,
bénis son nom très saint,  tout mon être !
Bénis le Seigneur, ô mon âme,
n'oublie aucun de ses bienfaits !

2.
Car il pardonne toutes tes offenses
et te guérit de toute maladie ;
Il réclame ta vie à la tombe
et te couronne d'amour et de tendresse ;

3.
Le Seigneur est tendresse et pitié,
lent à la colère et plein d'amour ;
il n'agit pas envers nous selon nos fautes,
ne nous rend pas selon nos offenses.

4.
Aussi loin qu'est l'orient de l'occident,
il met loin de nous nos péchés ;
comme la tendresse du père pour ses fils,
la tendresse du Seigneur pour qui le craint !