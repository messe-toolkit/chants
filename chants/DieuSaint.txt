DIEU SAINT, DIEU JUSTE ET SAINT,
DIEU LIBRE ET SAINT !
La vie de tes créatures est ta gloire !
Ciel et terre sont remplis de ton amour !
HOSANNA PAR TOUS LES UNIVERS !
Béni soit celui qui vient en ton nom !
HOSANNA SUR LA TERRE DES HOMMES!