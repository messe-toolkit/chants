Stance

J'ai désiré d'un grand désir manger cette Pâque avec vous avant de souffrir.

R.

O Christe Domine Jesu,
O Christe Domine Jesu !

Versets

Ps 26.

1.
Le Seigneur est ma lumière et mon salut ;
De qui aurais-je crainte ?
Le Seigneur est le rempart de ma vie ;
Devant qui tremblerais-je ?

2.
J'ai demandé une chose au Seigneur,
La seule que je cherche :
Habiter la maison du Seigneur
Tous les jours de ma vie.

3.
Pour admirer le Seigneur dans sa beauté
Et m'attacher à son temple.
Oui, il me réserve un lieu sûr
Au jour du malheur.

4.
Il me cache au plus secret de sa tente,
Il m'élève sur le roc.
Maintenant je relève la tête
Devant mes ennemis.