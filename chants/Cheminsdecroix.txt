Jésus,
tu veux mes mains pour passer cette journée
à aider les pauvres et les malades qui en ont besoin :
Seigneur, aujourd'hui, je te donne mes mains.

Seigneur,
tu veux mes pieds pour passer cette journée
à visiter ceux qui ont besoin d'un ami :
Seigneur, aujourd'hui, je te donne  mes pieds.

Seigneur,
tu veux ma voix pour passer cette journée
à parler à ceux qui ont besoin de paroles d'amour :
Seigneur, je te donne aujourd'hui ma voix.

Seigneur,
tu veux mon cœur pour passer cette journée
à aimer chaque homme uniquement parce que c'est un homme :
Seigneur, je te donne aujourd'hui mon cœur.