R.
POUR AIMER NOS FRÈRES
COMME TU NOUS AS AIMÉS,
NOUS VENONS PRIER LE PÈRE
COMME TU NOUS L'AS MONTRÉ.

1.
Si nous nous aimons
les uns les autres,
Dieu habite en nous
au fil des jours.
Si nous nous aimons
les uns les autres,
il nous gardera
dans son amour.

2.
Nous avons besoin
de ta lumière
pour ouvrir nos coeurs
de baptisés.
Nous avons besoin
de ta lumière
pour tendre la main
à l'étranger.

3.
Pour qu'à travers nous
ton Règne arrive
il nous faut le feu
de ton Esprit.
Pour qu'à travers nous
ton Règne arrive
souffle sur nos lèvres
et dans nos vies.