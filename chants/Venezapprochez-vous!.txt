R.
Venez, approchez-vous,
soyez bénis, soyez nourris.
Venez, l'amour est partagé
aucun n'est digne
Chacun est invité.

1.
Venez, n’attendez pas,
Il a préparé ce repas.
Osez, venez déposer
Vos nuits, vos croix,
Voyez, il nous ouvre la joie.

2.
Venez, n’attendez pas,
Il vient apaiser chaque soif.
Osez, venez déposer
Vos coeurs, vos choix,
Voyez, il nous donne la joie.

3.
Venez, n’attendez pas,
Il vient pour allumer la foi.
Osez, venez déposer
Vos peurs, vos voix,
Voyez, il devient notre joie.
