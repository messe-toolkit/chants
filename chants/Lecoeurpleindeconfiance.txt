R.
Le coeur plein de confiance,
Nous marchons vers toi.
Notre Dame de Lourdes,
Entends nos voix,
Entends nos voix.

1.
Venus de toutes parts
Toutes régions, toutes couleurs
Nous marchons vers toi
Nous portons en nos coeurs
Mille soucis, mille bonheurs
Entends nos voix.

2.
Au pas du pèlerin
Nous accueillons le temps de Dieu
Nous marchons vers toi
Révèle-nous ton Fils
Guide nos vies vers sa clarté
Entends nos voix.

3.
Printemps pour notre coeur
Matin d’espoir, chant de bonheur
Nous marchons vers toi
Au temps de l’Angelus,
Dis-nous la joie de ton Seigneur
Entends nos voix.