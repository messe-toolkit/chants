Ô SEIGNEUR, NOTRE DIEU,
QU’IL EST GRAND, TON NOM, PAR TOUTE LA TERRE !

1.
À voir ton ciel, ouvrage de tes doigts,
la lune et les étoiles que tu fixas,
qu’est-ce que l’homme pour que tu penses à lui,
le fils d’un homme, que tu en prennes souci ?

2.
Tu l’as voulu un peu moindre qu’un dieu,
le couronnant de gloire et d’honneur ;
tu l’établis sur les oeuvres de tes mains,
tu mets toute chose à ses pieds.

3.
Les troupeaux de boeufs et de brebis,
et même les bêtes sauvages,
les oiseaux du ciel et les poissons de la mer,
tout ce qui va son chemin dans les eaux.