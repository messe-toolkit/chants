1.
J’ai vu ce flacon de parfum
Sur tes pieds répandu, gaspillé,
Quand une femme agenouillée
T’embaumait de ses mains !
Toi, tu te laissais faire,
Moi, j’étais en colère
Contre toi, contre toi.
J’aurais voulu garder l’argent
Au lieu de le répandre…
Ou qu’on le donne aux indigents.

2.
Tout ce que mon coeur désirait
C’est de voir mon pays libéré,
Mais ce n’est pas ce qui comptait
Dans ton coeur, en premier !
Toi, tu m’as laissé faire,
Moi, j’étais en colère
Contre toi, contre toi.
Je t’ai vendu pour de l’argent,
J’ai vu mourir mon rêve,
Mais il n’était pas assez grand !

3.
Je me suis servi d’un baiser
Quand je t’ai désigné aux soldats.
Je suis venu tout près de toi
Et ils t’ont arrêté.
Toi, tu t’es laissé faire,
Moi, j’étais en colère
Contre moi, contre moi.
Je suis Judas, et sur mon nom
S’accrochent les racines
De la Passion… des passions.

Coda
Tu m’as déçu, je t’ai trahi :
J’avais rêvé d’un Dieu qui gagne.
Je t’ai perdu, je t’ai trahi,
Car toi tu es venu sans armes,
Et moi je t’ai vendu !