1.
Dans le temps dit eucharistie,
Tout en rendant un peu de vie
À la vie même,
À contre-temps de notre temps
Qui va sans cesse s’écoulant,
Nous t’avons rendu, Dieu vivant,
Nos règnes.

2.
Ce n’est pas qu’ils soient assurés,
Car nous voguons à tous les grés,
Si peu nous sommes,
Si peu de jours nous les perdons,
Si peu nous avons nous faisons,
Si peu nous connaissons le fond
De l’homme.

3.
Et pourtant chacun est assis
Comme s’il régissait sa vie
À sa gouverne,
Comme s’il dirigeait son temps
Sur un bateau toujours branlant,
D’autant plus qu’il va s’approchant
Du terme.

4.
Mais toi, Jésus, tu es venu,
À l’ombre du terme inconnu
Que tu foudroies,
Remplacer déjà ses méfaits,
Occuper ce qu’il a défait,
Souffler dans nos vies étouffées
La joie.