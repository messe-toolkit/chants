1.
Écoute la voix du Seigneur,
Prête l’oreille de ton cœur.
Qui que tu sois
Ton Dieu t’appelle,
qui que tu sois,
Il est ton Père.

R.
Toi qui aimes la vie,
Ô toi qui veux le bonheur,
Réponds en fidèle ouvrier
de sa très douce volonté,
Réponds en fidèle ouvrier
de l'Évangile et de sa paix.

2.
Écoute la voix du Seigneur,
Prête l’oreille de ton cœur.
Tu entendras
Que Dieu fait grâce,
Tu entendras
L'Esprit d’audace.

3.
Écoute la voix du Seigneur,
Prête l’oreille de ton cœur.
Tu entendras
Crier les pauvres,
Tu entendras
Gémir ce monde.

4.
Écoute la voix du Seigneur,
Prête l’oreille de ton cœur.
Tu entendras
Grandir l’Église,
Tu entendras
Sa paix promise.

5.
Écoute la voix du Seigneur,
Prête l’oreille de ton cœur.
Qui que tu sois,
Fais-toi violence,
Qui que tu sois,
Rejoins ton frère.