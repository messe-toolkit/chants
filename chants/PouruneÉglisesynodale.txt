POUR UNE ÉGLISE SYNODALE
MARCHONS ENSEMBLE AVEC JÉSUS !
QUE TOUT DISCIPLE ANNONCE PÂQUES
EN PARTAGEANT SA FOI VÉCUE,
EN REDONNANT LA FOI REÇUE !

1.
Formons l’Eglise porte ouverte
Où chacun dit ce qu’il ressent,
Le coeur sensible à d’autres frères
Qui disent Dieu différemment.

2.
Formons l’Eglise table offerte
Qui dit « Je t’aime » et fait le bien.
Qu’elle prodigue par ses gestes
Sa soif d’aller vers toute faim !

3.
Formons l’Eglise solidaire
Sur les terrains d’humanité.
Notre avenir est une terre
Où l’espérance s’est levée.

4.
Formons l’Eglise sans frontières
Où les vivants sont libérés.
Brisons les murs et les barrières
Et bâtissons maison de paix !

5.
Formons l’Eglise ardente braise
Où prend le feu des renouveaux ;
L’Esprit suscite des prophètes
Qui nous entraînent vers le haut.

6.
Formons l’Eglise missionnaire
Où la grandeur est de servir,
Comme l’a fait Jésus lui-même,
Lui l’Envoyé donnant sa vie.

7.
Formons l’Eglise trinitaire,
Peuple de Dieu et Corps du Christ,
Temple où l’Esprit nous mène au Père
Pour le bonheur du ciel promis.