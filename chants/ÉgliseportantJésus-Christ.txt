ÉGLISE PORTANT JÉSUS-CHRIST,
TU REÇOIS SON ESPRIT DANS TON COEUR.
VIENS REDIRE AVEC MARIE :
« JE SUIS LA SERVANTE DU SEIGNEUR » !

1.
Avec les yeux de notre Mère,
Nous saurons découvrir la Lumière :
D'âge en âge, Dieu répand son amour
Sur ceux qui attendent son Jour.

2.
Avec le coeur de notre Mère,
Nous pouvons rencontrer tous nos frères :
Dans ce monde aux affamés sans chemins,
Dieu vient pour combler de ses biens.

3.
Avec la foi de notre Mère,
Nous voulons devenir missionnaires :
Notre Dieu compte sur nous pour semer
Le grain du Royaume annoncé.

4.
Avec l'amour de notre Mère,
Accueillons la splendeur du Mystère.
Dieu nous dit que les derniers sont premiers :
Voici sa justice et sa paix !

5.
Avec la joie de notre Mère,
Exultons, glorifions notre Père !
Le Seigneur reste fidèle et présent :
Il montre la route en ce temps.

6.
Avec le « oui » de notre Mère,
Demeurons rassemblés en prière
Pour chanter notre espérance aujourd'hui
En Dieu qui nous donne la Vie !