Répons/
Le Christ s'est fait pour nous obéissant
jusqu'à la mort et la mort de la croix.

1.
Si le grain de blé tombé en terre ne meurt pas,
il reste seul. R/

2.
Lui, le fils, il a appris l'obéissance
par les souffrances de sa passion. R/

3.
C'est pourquoi Dieu l'a exalté,
et lui a donné le nom qui est dessus de tout nom. R/