Moi, je suis le pain vivant,
qui est descendu du ciel :
si quelqu’un mange de ce pain,
il vivra éternellement.