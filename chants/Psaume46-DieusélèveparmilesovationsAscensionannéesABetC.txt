DIEU S’ÉLÈVE PARMI LES OVATIONS,
LE SEIGNEUR, AUX ÉCLATS DU COR.

1.
Tous les peuples, battez des mains,
acclamez Dieu par vos cris de joie !
Car le Seigneur est le Très-Haut, le redoutable,
le grand roi sur toute la terre.

2.
Dieu s’élève parmi les ovations,
le Seigneur, aux éclats du cor.
Sonnez pour notre Dieu, sonnez,
sonnez pour notre roi, sonnez !

3.
Car Dieu est le roi de la terre,
que vos musiques l’annoncent !
Il règne, Dieu, sur les païens,
Dieu est assis sur son trône sacré.