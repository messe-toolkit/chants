1.
Fils d’un peuple en qui l’Alliance porte fruit,
Saül de Tarse, ton coeur brûle pour la Loi ;
Tu condamnes à la prison tous ceux qui croient
Que Jésus est l’Envoyé du Dieu de vie.
Pourquoi donc les arracher à leur maison ?
Seraient-ils blasphémateurs du Dieu très bon ?

R.
PAUL DE TARSE,
PAUL, APÔTRE DE JÉSUS !

2.
Plein de zèle, tu fais route vers Damas,
Et voici qu’une lumière t’éblouit ;
Une voix à tes oreilles retentit,
Que nul autre ne saisit auprès de toi :
« Pourquoi donc à mon égard tant de furie ?
Et pourquoi persécuter tous mes amis ? »

3.
Aveuglé par la clarté de ce plein jour
Tu entends cet Inconnu que tu poursuis,
Et sa voix te dit aussi qu’il te poursuit :
D’un seul mot il te relève par amour.
Pourquoi donc est-il venu sur ton chemin ?
Pourquoi donc va-t-il te prendre par la main ?

4.
Dans la ville un frère est là pour t’accueillir,
Un disciple qui saura guider tes pas.
Qui peut dire où l’Esprit-Saint te mènera,
En ce jour où tes yeux s’ouvrent à Jésus Christ ?
Pourquoi donc à son service es-tu choisi ?
De tout coeur tu lui rends grâce et le bénis.