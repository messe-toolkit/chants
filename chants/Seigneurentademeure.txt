R.
Seigneur, en ta demeure, toute paix, toute joie!

Psaume 83
Mon âme s'épuise â désirer
les parvis du Seigneur;
mon coeur et ma chair sont un cri vers le Dieu vivant!

L'oiseau lui-même s'est trouvé une maison,
et l'hirondelle, un nid:
tes autels, Seigneur de l'univers,
mon Roi et mon Dieu !

Heureux les habitants de ta maison:
ils pourront te chanter encore!
Heureux les hommes dont tu es la force:
des chemins s'ouvrent dans leur coeur !

Seigneur, Dieu de l'univers, entends ma prière;
écoute, Dieu de Jacob.
Dieu, vois notre bouclier,
regarde le visage de ton messie.