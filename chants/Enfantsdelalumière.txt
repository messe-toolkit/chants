Stance
Enfants de la lumière, enfants du jour,
Dieu nous a sauvés de la nuit ;
enfants de la lumière, enfants du jour,
ne demeurons pas endormis !
Tenons en éveil notre lampe de l’amour,
Jésus nous fait vivre aujourd’hui !
Tenons en éveil notre lampe de l’amour,
Il viendra nous prendre avec lui.

SUR LA TERRE DES VIVANTS
NOUS VERRONS LE DIEU DE BONTÉ.
COMME UN PÈRE IL NOUS ATTEND,
SA MAISON DEMEURE ÉCLAIRÉE.

Ps 26.

1.
Le Seigneur est ma lumière et mon salut ;
de qui aurais-je crainte ?
Le Seigneur est le rempart de ma vie ;
devant qui tremblerais-je ?

2.
J’ai demandé une chose au Seigneur,
la seule que je cherche ;
habiter la maison du Seigneur
tous les jours de ma vie.