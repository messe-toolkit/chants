PAR LE PAIN DU PARTAGE,
SEIGNEUR, TU DONNES VIE;
VERS LE JOUR DE LA PÂQUE
TON SOUFFLE NOUS CONDUIT.

1.
Tu rejoins notre marche,
Jésus ressuscité,
Quand notre espoir s’éteint
Devant la croix dressée.
Ta voix nous accompagne
Et tu nous prends la main.

2.
Quel bonheur, ta Parole,
Jésus ressuscité,
Quand nous la recevons
Comme un soleil d’été !
Dans le soir qui approche
Déjà nous revivons.

3.
Viens chez nous prendre place,
Jésus ressuscité,
Que le pain soit rompu
Par toi notre Invité !
Voici qu’à notre table
Ta gloire est reconnue.

4.
Envoie-nous vers nos frères,
Jésus ressuscité,
Nos coeurs sont tout brûlants,
Nos yeux pleins de clarté.
Révèle ta lumière
Aux hommes de ce temps.