R.
Dieu monte parmi l'acclamation,
Le Seigneur, aux éclats du cor.

1.
Tous les peuples, battez des mains,
Acclamez Dieu par vos cris de joie !

2.
Car le Seigneur est le Très-Haut, le redoutable,
Le grand roi sur toute la terre.

3.
Dieu s'élève parmi les ovations,
Le Seigneur, aux éclats du cor.

4.
Sonnez pour notre Dieu, sonnez,
Sonnez pour notre roi, sonnez !

5.
Car Dieu est le roi de la terre :
Que vos musiques l'annoncent !

6.
Il règne, Dieu, sur les païens,
Dieu est assis sur son trône sacré.