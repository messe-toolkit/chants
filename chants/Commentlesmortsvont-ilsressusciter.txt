R.
Par ton Esprit, Seigneur, fais germer en nous
Le bon grain de l’espérance !
Par ton Esprit, Seigneur, fais briller sur nous
Le matin des renaissances !

STROPHES
Comment les morts vont-ils ressusciter ?
Quel sera leur corps au réveil dans la clarté?
A
Sans mourir d’abord
le grain semé ne peut donner la vie.

B
Tu ne sèmes pas le corps de la plante en devenir,
elle est toute nue la graine que tu sèmes;

C
Il en sera de même
quand les morts pourront surgir. (au Refrain)

A
Ce qui est semé dans la terre est périssable,
ce qui ressuscite est impérissable.

B
Ce qui est semé n’a plus rien d’une richesse,
ce qui ressuscite est lumière et gloire .

C
Ce qui est semé est marqué par la faiblesse,
ce qui ressuscite est puissance et force. (au Refrain)

A
Ce qui est semé est un corps humain,
Ce qui ressuscite est un corps spirituel.

B
Puisqu’il existe un corps humain,
Il existe aussi un corps spirituel. (au Refrain)

A
« Le premier Adam était un être humain
qui avait reçu la vie » (Gn 2,7);

B
Le dernier Adam, Jésus Christ,
c’est lui le Spirituel qui donne la vie.

C
D’abord est apparu l’être humain,
Ensuite seulement le spirituel. (au Refrain)

A
Pétri de terre, le premier homme vient de la terre ;
Le deuxième homme vient du ciel.

B
Pétris de la terre,
nous sommes tous à l’image d’Adam.

C
Puisque le Christ est venu du ciel,
nous serons tous à son image,

C
Fils du ciel et de la terre
À la gloire du Vivant. (au Refrain)