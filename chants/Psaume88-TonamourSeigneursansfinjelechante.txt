TON AMOUR, SEIGNEUR,
SANS FIN JE LE CHANTE !

1.
L’amour du Seigneur, sans fin je le chante ;
ta fidélité, je l’annonce d’âge en âge.
Je le dis : C’est un amour bâti pour toujours ;
ta fidélité est plus stable que les cieux.

2.
Heureux le peuple qui connaît l’ovation !
Seigneur, il marche à la lumière de ta face ;
tout le jour, à ton nom il danse de joie,
fier de ton juste pouvoir.

3.
Tu es sa force éclatante ;
ta grâce accroît notre vigueur.
Oui, notre roi est au Seigneur ;
notre bouclier, au Dieu saint d’Israël.