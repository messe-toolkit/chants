R.
DE PASSAGE, DE PASSAGE
EN PASSAGER OU EN PASSEUR.
DE RIVAGE, EN RIVAGE
SITOT ICI DEJA AILLEURS

1.
Pour trimbaler l'inaccessible,
Sur de vieux refrains généreux.
Pour croire encore à l'impossible,
La tête au spleen, le coeur aux cieux.

2.
Toujours branché sur notre époque,
Des mots qui rencontrent les gens.
Tant pis pour tous ceux qui s'en moquent,
Passe, passeur, va de l'avant…

3.
Cours, mais cours donc camarade,
Ce vieux monde est loin derrière toi,
Malgré ton blues et ta panade,
"CHACUN POUR TOUS", nous on y croit.

4.
Et si j'avais eu tort peut-être
D'avoir raison un peu trop tôt
Force serait de reconnaître
La vérité est dans ces mots.