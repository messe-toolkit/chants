R.
NE CRAINS PAS, N’AIE PAS PEUR,
LAISSE TES DOUTES
AU BORD DE LA ROUTE.
NE CRAINS PAS, N’AIE PAS PEUR,
MARCHE AVEC MOI,
GARDE CONFIANCE.

1.
Tu as vu l’espérance
D’un homme à genoux,
Une grande confiance
Qui va jusqu’au bout.
Pour que son enfant vive,
Pour qu’elle soit sauvée.

2.
Tu as vu la souffrance
D’un père meurtri.
Tu es sa seule chance
Au milieu des cris.
Son enfant n’est pas morte,
Pourquoi tant de pleurs ?

3.
Tu as pris la main de l’enfant
Devant disciples et parents
Jeune fille, lève-toi !
Jeune fille, lève-toi !

4.
Tu connais nos blessures,
Tu es avec nous,
Et ta main nous rassure
Pour vivre debout.
Quand nous perdons courage
Réveille notre foi.