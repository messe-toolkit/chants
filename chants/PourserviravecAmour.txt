R.
Dans le pauvre et le petit
Donne-nous de te servir !
Ton Eglise en est témoin :
Dieu se fait parole et pain.

1.
Pour servir avec amour,
Seigneur, tu nous choisis.
Ton appel nous met debout,
Nous chantons notre merci.
Sur ton chemin de Serviteur
Nous connaîtrons la vraie grandeur.

2.
Bien-Aimé de l'Éternel
Venu porter du fruit,
Trente années à Nazareth
Humblement tu as servi.
Dans la maison du charpentier
Dieu t'a nourri d'humanité.

3.
Messager du Dieu de paix,
Tu parles par ta vie.
Grande joie en Galilée,
Les malades sont guéris !
Tu sais trouver les mots du coeur
Et nous ouvrir à ton bonheur.

4.
Comment suivre sans trembler
Ta marche vers la croix ?
Que l'Esprit de vérité
Nous arrache au désarroi !
Quand notre foi connaît la nuit,
Révèle-nous le jour qui luit.

5.
Dans ce monde où tu nous veux,
Jésus ressuscité,
Nous serons de ces veilleurs
Attentifs à ta clarté.
Lumière et force des vivants,
Béni sois-tu par tout croyant !