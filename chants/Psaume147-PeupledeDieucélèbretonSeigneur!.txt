PEUPLE DE DIEU,
CÉLÈBRE TON SEIGNEUR !

1.
Glorifie le Seigneur, Jérusalem !
Célèbre ton Dieu, ô Sion !
Il a consolidé les barres de tes portes,
dans tes murs il a béni tes enfants.

2.
Il fait régner la paix à tes frontières,
et d’un pain de froment te rassasie.
Il envoie sa parole sur la terre :
rapide, son verbe la parcourt.

3.
Il révèle sa parole à Jacob,
ses volontés et ses lois à Israël.
Pas un peuple qu’il ait ainsi traité ;
nul autre n’a connu ses volontés.