R.
Puisque Dieu nous a aimés
Jusqu’à nous donner son Fils
Ni la mort, ni le péché
Ne sauraient nous arracher
À l’amour qui vient de Lui.

1.
Depuis l’heure où le péché
S’empara du genre humain
Dieu rêvait de dépêcher
En ami sur nos chemins
Le Seigneur Jésus son Fils.

2.
Puisque Dieu s’est fait petit,
Inconnu parmi les siens,
Tournons-nous vers ses amis
Qui dans l’ombre et sans témoins
Ont au coeur le monde entier.

3.
À l’étable de Noël
Les bergers ont vu l’Enfant
Et les anges dans le ciel
Leur ont dit ce nouveau chant
De la paix pour l’univers.

4.
Mais la paix de notre
Dieu N’est ni calme ni repos,
Et Jésus proclame heureux
Qui partage ses travaux
Et prend part à ses douleurs.

5.
Puisque Dieu nous a comblés
Au-delà de tout désir,
Il nous reste à partager
Jusqu’aux mondes à venir
Le présent de cet amour.

6.
Que Dieu garde vigilants
Ceux qui chantent le Seigneur,
Qu’ils ne soient en même temps
Les complices du malheur
Où leurs frères sont tenus.

7.
Au grand arbre de la Croix
L’amertume n’a pas cours.
L’innocent qui souffre là
Nous révèle pour toujours
Que les pauvres sont aimés.

8.
Et quand l’homme des douleurs
Sort vivant de son tombeau,
Nous pouvons reprendre coeur
Et renaître dans une eau
Qui nous lave pour toujours.

9.
Puisque Dieu nous a choisis
Comme peuple de sa paix,
Comment voir un ennemi
Dans quelque homme désormais
Pour lequel Jésus est mort ?