1.
Peuple dans la nuit,
Réveille-toi, voici l’aurore !
Peuple dans la nuit,
Voici le jour qui resplendit !
Vers les ombres de la mort,
Nous avons marché longtemps ;
Qui pourrait nous rendre encore
La lumière des vivants ?
Peuple dans la nuit,
Réveille-toi, voici l’aurore !
Peuple dans la nuit,
Voici le jour qui resplendit !

2.
Dieu nous aime tant
Qu’il a donné son Fils unique ;
Dieu nous aime tant
Qu’il donne au monde son enfant.
Il nous dit son dernier mot
Pour que vive l’univers ;
Pour nous rendre un coeur nouveau,
Sa parole se fait chair.
Dieu nous aime tant
Qu’il a donné son Fils unique ;
Dieu nous aime tant
Qu’il donne au monde son enfant.

3.
Le Sauveur est né,
Tout homme en lui découvre un frère ;
Le Sauveur est né,
Tout homme en lui se reconnaît.
Dieu que nul n’a jamais vu
Prend visage en cette nuit.
Son regard est apparu
Dans les yeux de Jésus Christ.
Le Sauveur est né,
Tout homme en lui découvre un frère ;
Le Sauveur est né,
Tout homme en lui se reconnaît.