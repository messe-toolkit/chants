1.
J’ai vu l’eau vive jaillissant du cœur du Christ,
Tous ceux que lave cette eau seront sauvés
Tous ceux que lave cette eau seront sauvés et chanteront : Alléluia !
Alléluia! Alléluia! Alléluia!

2.
 J’ai vu la source devenir un fleuve immense,
Les fils de Dieu chantaient leur joie d’être sauvés,
Les fils de Dieu rassemblés chantaient leur joie d’être sauvés, Alléluia !
Alléluia! Alléluia! Alléluia!

3.
 J’ai vu le temple désormais s’ouvrir à tous
Le Christ revient montrant la plaie de son côté,
Le Christ revient victorieux montrant la plaie de son coté, Alléluia !
Alléluia! Alléluia! Alléluia!

4.
 J’ai vu le verbe nous donner la paix de Dieu,
Tous ceux qui croient en son nom seront sauvés,
Tous ceux qui croient en son nom seront sauvés et chanteront : Alléluia !
Alléluia! Alléluia! Alléluia!