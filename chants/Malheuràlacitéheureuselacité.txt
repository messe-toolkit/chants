REF.
HEUREUSE LA CITE
QUI A CHOISI L’AGNEAU DE DIEU
POUR ETRE SON BERGER :
IL LA CONDUIRA AUX SOURCES DES EAUX VIVES !

1.
Malheur à la cité
Vêtue de lin, de pourpre et d’or,
Où la justice meurt de faim.
Vaine est sa richesse.
Il suffira d’une heure
Pour qu’elle croule et meure.

2.
Malheur à la cité
Emprisonnée dans ses remparts,
Où la parole est bâillonnée.
Vaine est sa puissance.
Il suffira d’une heure
Pour qu’elle croule et meure.

3.
Malheur à la cité
Qui tue le juste et l’innocent
Et se confie en ses armées.
Vaine est sa violence.
Il suffira d’une heure
Pour qu’elle croule et meure.