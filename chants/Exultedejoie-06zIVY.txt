R.
Exulte de joie, peuple de la terre,
Le Christ notre Dieu est ressuscité,
Pour nous délivrer de tous nos péchés
Et nous faire entrer dans l'éternité.

1.
Dansez, criez de joie
A la venue du Seigneur,
Ouvrez tout grand vos cœurs
Au Christ notre Sauveur .

2.
Nous sommes réconciliés
Avec Dieu notre Père.
Jésus, vrai Dieu et vrai homme,
Epouse l'humanité.

3.
Que brille cette lumière
Sur toute notre terre
Et nous virons tous en frères,
Enfants d'un même père.

4.
Alléluia, alléluia,
Que chante en moi la joie,
La joie d'être sauvé
La joie d'être pardonné.