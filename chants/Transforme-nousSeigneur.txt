1.
Comme le bois qui se consume, ne reste pas… de bois
Et comme l’eau mêlée au vin devient du vin…
Et le vin… en ton sang versé.

R.
TRANSFORME-NOUS, SEIGNEUR
TRANSFORME-NOUS EN TOI
TRANSFORME-NOUS, SEIGNEUR
TRANSFORME-NOUS EN TOI.

TRANSFORME-NOUS, SEIGNEUR
TRANSFORME-NOUS EN TOI
TRANSFORME-NOUS, SEIGNEUR
TRANSFORME-NOUS EN TOI.

2.
Comme le grain broyé parmi d’autres milliers… de grains
Comme le blé du boulanger devient le pain…
Et le pain… en ton corps livré.

3.
Que ton amour nourrisse en nous l’amour de nos… prochains
Que ton regard soit en nos yeux comme un reflet
L’autre n’est plus un étranger.

4.
Transforme-nous, que le vieil homme puisse mourir… en nous
Transforme-nous en Hommes nouveaux et vivants
Ainsi que tu nous as créés.