1.
Que soit béni le nom de Dieu
Qu’il soit béni toujours et à jamais
Que soit béni le nom de Dieu
Pour sa bonté et sa fidélité
Pour que résonne sur la terre
Une espérance, un chant, une prière
Exultez de tout votre coeur
Pour le seigneur.

CHANTE, CHANTE, ALLÉLUIA, OH OH OH
CHANTE POUR DIEU, CHANTE ALLÉLUIA
CHANTE, CHANTE, METS DU CIEL DANS TA VOIX
OH OH OH, ALLÉLUIA
CHANTE, CHANTE, ET LÈVE-TOI, OH, OH, OH
DANSE POUR DIEU, DANSE ALLÉLUIA, ALLÉLUIA !

2.
Que soit béni le nom de Dieu
Qu’il soit béni toujours et en tout lieu
Que soit béni le nom de Dieu
Pour son amour miséricordieux
Pour que se lève sur la terre
Une éternelle aurore, une lumière
Offrez à Dieu un chant nouveau
Toujours plus beau.

Coda
Chante de joie….
Danse de joie…