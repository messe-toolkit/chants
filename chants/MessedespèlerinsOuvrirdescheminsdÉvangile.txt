R.
OUVRIR DES CHEMINS D’ÉVANGILE,
PRÉPARER LES CHEMINS DU SEIGNEUR,
OUVRIR DES ROUTES POUR NOS FRÈRES,
PARTIR OÙ L’ESPRIT NOUS ENVOIE,
PARTIR OÙ L’ESPRIT NOUS CONDUIT!

1.
Partir sur des routes nouvelles…
Dieu nous appelle à partager le pain,
Dieu nous attend pour bâtir son Royaume!
Prêtons nos mains pour être ses témoins.

2.
Partir, l’Esprit nous renouvelle…
Dieu nous appelle à montrer le chemin,
Dieu nous attend pour bâtir son Royaume!
Offrons nos vies pour être ses témoins.

3.
Partir vers la terre nouvelle…
Dieu nous appelle au temps de la moisson,
Dieu nous attend pour bâtir son Royaume!
Ouvrons nos coeurs pour être ses témoins.

4.
Partir à la saison nouvelle…
Dieu nous appelle ! « Sais-tu le don de Dieu ? »
Dieu nous attend pour bâtir son Royaume!
Donnons nos vies pour être ses témoins.