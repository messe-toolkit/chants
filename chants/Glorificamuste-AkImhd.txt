1.
Viens, Esprit de Dieu et nous serons humbles et pauvres.
Viens nous apprêter à hériter de ton Royaume.
Viens nous fortifier dans la douleur et dans l’épreuve.
Viens nous rassasier de ton eau vive.

R.
Veni Sancte Spiritus,
Veni Sancte Spiritus,
Veni Sancte Spiritus,
Glorificamus te !

2.
Viens, Esprit de Dieu, mettre ta paix dans la discorde.
Viens nous serons doux, nous obtiendrons miséricorde.
Viens et nous serons des artisans de paix sur terre.
Viens donner la joie, qui vient du Père.

3.
Viens, Esprit de Dieu et sanctifie nos sacrifices.
Viens nous soutenir dans nos combats pour la justice.
Viens rends nos cœurs purs et nous verrons l’éclat du Père.
Viens, éclaire-nous de sa lumière.

F.
Deo Patri sit Gloria,
Et Filio, qui a mortuis,
Surrexit ac Paraclito
In saeculorum saecula.
Amen (x4)