Dans notre cœur la vigilance,
Lampe allumée par le Seigneur,
Se renouvelle dans sa flamme
Au chant commun de notre joie.

Que veille en nous l'action de grâce,
Comme la fleur de l'amandier,
Qui la première au loin regarde
L'été venir et sa moisson.

Que notre amour et sa louange
Soient les deux ailes du matin
Qui se déploient dans la prière
Et nous emportent loin de nous.

Voici l'Époux qui nous appelle,
Courons aux noces de l'Agneau.
Mais que la route paraît longue :
Quand poindras-tu, dernier matin ?