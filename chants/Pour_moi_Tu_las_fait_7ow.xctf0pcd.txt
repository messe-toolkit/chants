1.
Même si pour moi Tu devais t'abaisser
Pour porter ma vie et tout réconcilier
Me montrer le Père qui m'a tant aimé
Je sais que pour moi tu l'as fait (x2)
\n
Même si Tu devais quitter toute gloire
Venir T’incarner, épouser notre Histoire
Au cœur de chaque homme, déposer l’espoir
Je sais que pour moi tu l'as fait (x2)

R.
Car mon Dieu tient toujours ses promesses
Me couvrant de tendresse 
Voici quel est mon Dieu
Car Il fait toutes choses nouvelles 
Sa grâce est éternelle
Il a ouvert les Cieux 
Voici quel est mon Dieu

2.
Même si la croix devenait ton fardeau
Le poids de ce monde, l'oubli dans le tombeau
Pour essuyer toutes larmes et tous sanglots
Je sais que pour moi tu l'as fait (x2)

3.
Même si Ta vie devait être oubliée
En prenant la place du serviteur caché
Pour que je revive en Ta fidélité
Je sais que pour moi tu l'as fait (x2)