TOUS LES JOURS DE NOTRE VIE,
NOUS VOULONS TE PRIER,
NOTRE DAME, NOTRE DAME.
TOUS LES JOURS DE NOTRE VIE
NOUS VOULONS T’IMPLORER,
NOTRE DAME.

1.
Au pied de la croix
Marie, tu étais là,
En plein désarroi,
Pourquoi faut-il, pourquoi ?

2.
Tu as vu souffrir
Jésus, agonisant ;
Tu l’as vu mourir
Son corps couvert de sang :

3.
Ton fils, ton enfant,
Venu pour nous sauver
Est mort, innocent,
Pour notre humanité :

4.
Un jour nous serons
Avec toi, dans les cieux.
Nos vies trouveront
La joie au coeur de Dieu :