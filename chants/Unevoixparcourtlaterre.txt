1.
Une voix parcourt la terre,
Dieu s'approche dans la nuit :
La semence de lumière
Donne enfin son fruit.

2.
Voici l'heure du Royaume,
L'arbre mort a refleuri ;
Mais devant le fils de l'homme,
Qui pourra tenir ?

3.
À l'Orient son Jour se lève,
Nul n'échappe à sa venue.
Sa Parole comme un glaive
Met les cœurs à nu.

4.
Seul le pauvre trouve grâce,
Seul le pauvre sait aimer :
Dieu l'invite à prendre place
Près du Fils aîné.

5.
Et l'Agneau des sources vives,
Dieu fait chair en notre temps.
Chaque jour sous d'humbles signes
Vient à nos devants.

6.
Offre-lui tes mains ouvertes.
Prends son corps livré pour toi ;
Son amour sera ta fête,
Donne-lui ta foi.

7.
Marche encore vers la Ville
Où tes yeux verront l'Agneau ;
Cherche en lui la route à suivre,
Viens au jour nouveau.