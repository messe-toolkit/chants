1.
L’aventure touchant le coeur pur élève très haut
Libéré du poids du passé, chassé dans le dos
Dieu vivant, prévenant, chacun est son enfant
En ami, il nous dit : « Aime » à l’infini.

PARS CONFIANTE, AIMANTE IL T’APPELLE
NE SERAS TU PAS CELLE
QUI SAURA FAIRE LE BON CHOIX ?
POUR LA VOIE QU’IL TRACE POUR TOI !

2.
Pauvre dame, priant pour les âmes, jamais tu condamnes
Claire d’Assise buvant le calice et portant le lys
Frère François, en émoi devant le roi des rois
Il te gagne pour le coeur du libérateur.

3.
L’Évangile tu voudrais le suivre, l’annoncer, le vivre
T’approcher de tous les lépreux, les soigner au mieux
Ton étoile sous ton voile, en Marie te sourit
Elle te montre le chemin où rompre le pain.