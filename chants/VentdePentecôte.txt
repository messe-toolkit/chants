LE VENT SOUFFLE OÙ IL VEUT
OU, OU, OU, OU, OU.
ET TU NE SAIS PAS D’OÙ IL VIENT
ET TU NE SAIS PAS OÙ IL VA.
MAIS TU ENTENDS SA VOIX.

1.
Il apporte la pluie,
Il redonne la vie.
Il porte l’eau venue des mers
Qui fait refleurir les déserts.

2.
Il nous fait avancer,
Nos vies sont bouleversées,
Le vent qui pousse les voiliers,
Le vent qui peut déraciner.

3.
Il souffle sur les plaies,
La douleur est calmée,
Le vent qui vient tout rafraîchir
Quand la chaleur nous fait souffrir.

4.
Il est comme l’Esprit
Qui nous donne la vie.
Il vient, il pénètre partout
Et jusqu’au plus profond de nous.

5.
Il fait venir la joie,
Le courage et la foi.
Le coeur des Apôtres est changé
Quand le vent du ciel a soufflé.