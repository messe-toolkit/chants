1.
La cheminée quand elle s’endort
Le feu ne montre plus sa joie
Mais sous la cendre il reste encore
Quelques tisons, quelques éclats
Il suffira d’un peu de vent
D’un peu d’espoir d’un peu de bois
Pour donner à ce feu mourant
La flamme qui le relèvera.
Et les accents de nos guitares
Se font plus riches et plus vivants
Et c’est dans la chaleur du soir
Que naissent des rêves d’enfants.

2.
De plus en plus majestueux
Le feu se dresse et devient fou
Il est si fier et ambitieux
Que le bois craque sous son joug
L’affrontement est inégal
C’est le loup contre la brebis
Le résultat sera fatal
C’est le malheur des plus petits.
Et les accents de nos guitares
Se font plus forts et plus ardents
Pour dénoncer le désespoir
Et la misère trop souvent.

3.
Le feu s’attise tous les jours
Et laisse place à la lumière
La vie est faite pour l’Amour
Mais la nuit colle à notre terre
Il nous faudra beaucoup de temps
Pour élaguer nos vieilles branches
Il y aura mille printemps
Pour que fleurissent les pervenches.
Et les accents de nos guitares
Résonneront jusqu’au matin
Ils sont l’écho de nos espoirs
Pour continuer sur ce chemin.