1.
En remplissant ta cruche, femme d’Israël,
Tu laissas de ta main l’eau s’enfuir en chantant.
Gabriel annonça cette heureuse nouvelle :
« Du Très-Haut, dans ton sein, germera un enfant ! »
Ton coeur alors s’emplit de joie.

MARIE, SUR LE CHEMIN,
RADIEUSE, TU AVANCES!
QUAND TARDE LE MATIN,
SOUTIENS NOTRE ESPÉRANCE. (bis)

2.
Tu te hâtas vers la maison d’Élisabeth.
Te voyant, ta cousine, animée par l’Esprit,
Ne se put contenir : « Oui, le ciel est en fête !
Et Jésus, ton garçon, parmi tous, est béni ! »
Ton coeur alors s’emplit de joie.

3.
Tu es partie avec Joseph à Bethléem.
Tu fis naître Jésus, fils de Dieu, le Très Saint,
« Sur la terre, la paix ! Gloire à Dieu qui nous aime ! »
Les bergers et les anges entonnaient ce refrain.
Ton coeur alors s’emplit de joie.