R.
JESUS TU NOUS INVITES
A PARTAGER LE PAIN
JESUS TU NOUS INVITES
A PARTAGER LE VIN
JESUS TU NOUS CONVIES
A PARTAGER TA VIE

1.
Je suis très heureux, Jésus mon ami
Je suis très heureux quand je communie
Mais apprends-moi aussi
A partager ma vie
A vivre chaque jour
Avec beaucoup d'amour

2.
Tu as pris du pain, tu as prié Dieu
Tu as pris du pain et remercié Dieu
Tu nous l'as partagé
Tu nous l'as donné
C'est mon corps, mes amis
Donné pour votre vie

3.
Tu viens dans mon coeur quand je communie
Tu viens dans mon coeur, je te dis merci
Je deviens ton ami
Et tu nous réunis
En devenant ton Corps
Nous devenons plus fort