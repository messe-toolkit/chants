R.
Revenez au Seigneur votre Dieu : il vous rendra la vie.

1.
Déchirez votre cœur et non vos vêtements. Repentez-vous :
Dieu ne veut pas la mort du pécheur, mais sa vie.

2.
Cherchez sa face et vivez devant lui :
Il brillera sur vous comme l'aurore ; comme une ondée,
Comme une pluie de printemps sur la terre, il descendra en vos cœurs.