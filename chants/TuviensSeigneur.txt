R.
Tu viens, Seigneur, de plus loin que la mer.
Tu viens, Seigneur, c'est Noël sur la terre.

1.
Noël autrefois, un soir sans espoir,
L'enfant de Marie est né dans la nuit.

2.
Noël aujourd'hui, le ciel dans la vie,
Un Dieu, un enfant qu'on tient dans ses mains.

3.
Noël une histoire, un feu dans le noir,
Le temps de rêver aux feux de l'été.

4.
Noël chaque jour, la joie dans le cœur.
Quand Dieu se fait chair c'est Dieu pour de vrai.