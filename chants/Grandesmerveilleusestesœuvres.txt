R.
Grandes, merveilleuses, tes œuvres, Seigneur, Dieu de l'univers.

1.
Ils sont justes, ils sont vrais tes chemins, Roi des nations.

2.
Qui ne te craindrait, Seigneur ? À ton nom, qui ne rendrait gloire ?

3.
Oui, toi seul es saint ! Oui, toutes les nations viendront et se prosterneront devant toi ;
Oui, ils sont manifestés, tes jugements.