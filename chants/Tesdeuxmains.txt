QUAND TA MAIN DROITE SE POSE SUR TON COEUR
QUAND TA MAIN GAUCHE REJOINT SA PETITE SOEUR
AU BOUT DU MONDE ELLES PEUVENT S’ENVOLER
COMME UN OISEAU, FOU DE LIBERTÉ.

1.
Tes deux mains pour semer des fleurs à nos frontières
Et creuser au désert le lit d’une rivière
Tes deux mains pour bâtir l’espoir à l’infini
Et sauver un oiseau qui tombe de son nid.

2.
Tes deux mains pour agir sans te décourager
Et pétrir chaque jour le pain à partager
Tes deux mains pour porter l’oiseau jusqu’à chez toi
Et donner de toi-même à ceux qui cherchent un toit.

3.
Tes deux mains pour tisser la paix au quotidien
Et nouer patiemment les fils de ton destin
Tes deux mains pour soigner l’oiseau comme un ami
Et l’aider à ouvrir ses ailes endormies.

4.
Tes deux mains pour saisir la vie du bon côté
Et choisir pour chemin la générosité
Tes deux mains pour briser les murs et les barrières
Et laisser s’envoler l’oiseau vers la lumière.