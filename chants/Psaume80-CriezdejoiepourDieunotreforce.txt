CRIEZ DE JOIE POUR DIEU, NOTRE FORCE.

1.
Jouez, musiques, frappez le tambourin,
la harpe et la cithare mélodieuse.
Sonnez du cor pour le mois nouveau,
quand revient le jour de notre fête.

2.
C’est là, pour Israël, une règle,
une ordonnance du Dieu de Jacob ;
il en fit, pour Joseph, une loi
quand il marcha contre la terre d’Égypte.

3.
J’entends des mots qui m’étaient inconnus :
« J’ai ôté le poids qui chargeait ses épaules ;
ses mains ont déposé le fardeau.
Quand tu étais sous l’oppression, je t’ai sauvé.

4.
« Tu n’auras pas chez toi d’autres dieux,
tu ne serviras aucun dieu étranger.
C’est moi, le Seigneur ton Dieu,
qui t’ai fait monter de la terre d’Égypte ! »