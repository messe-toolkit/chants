R.
Visage d’Église,
Vierge Marie,
Sois près de nous celle qui rassemble ;
Sois près de Dieu celle qui nous aime.

1.
Toute faible,
Vierge Marie,
Tu es servante du Seigneur,
Signe de Dieu pour les hommes : (bis)

2.
Toute pure,
Fille choisie,
Tu es la Mère du Sauveur,
Mère de Dieu et des hommes : (bis)

3.
Toute pauvre,
Femme bénie,
Tu es porteuse des douleurs,
Temple de Dieu et des hommes : (bis)

4.
Toute simple,
Vierge Marie,
Tu es sagesse pour nos cœurs,
Souffle de Dieu pour les hommes : (bis)