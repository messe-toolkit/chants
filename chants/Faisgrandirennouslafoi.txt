RASSEMBLÉS DANS LA MÊME FOI,
RÉVEILLÉS PAR LE MÊME ESPRIT,
NOUS FORMONS UN MÊME CORPS,
TON ÉGLISE, Ô JÉSUS CHRIST !

1er dim. - Lc 4, 1-13 - le désert

1.
Fais grandir en nous la foi, (bis)
Toi Jésus qui nous recrées
Par des mots brûlants de vie.
Guide notre marche (bis)
Par-delà tous nos déserts
Jusqu’à l’aube de la Pâque.

Les 5 dimanches

2.
Fais grandir en nous la foi, (bis)
Ton Mystère est célébré
Dans le Souffle de l’Esprit.
Source d’espérance (bis)
Qui jaillis dans nos déserts,
Fais-nous boire à tes eaux vives !

Les 5 dimanches - Envoi

3.
Fais grandir en nous la foi, (bis)
Chaque jour elle est vécue
En réponse à tes appels.
Toi qui nous fais signe (bis)
Au milieu de nos déserts,
Donne-nous d’oser te suivre.

2e dim. C - Lc 9, 28-36 - La Transfiguration

4.
Fais grandir en nous la foi, (bis)
Tu nous mènes à des montées
Pour la fête inattendue.
Joie dans ta lumière ! (bis)
Elle apporte à nos déserts
Des instants de paix divine !

3e dim. C - Lc 13, 1-9 - Le figuier

5.
Fais grandir en nous la foi, (bis)
Toi qui nommes Dieu « JE SUIS »,
Et nous dis qu’il est Sauveur.
Grande est ta patience, (bis)
Tu fécondes nos déserts
Pour que nos figuiers mûrissent.

4e Dim C - Lc 15, 1-32 - Le Prodigue

6.
Fais grandir en nous la foi, (bis)
Tu révèles un Dieu d’amour
Qui pardonne au fils pécheur.
Gloire à notre Père, (bis)
Qui nous voit dans nos déserts
Et nous ouvre à sa tendresse !

5e dim. C - Jn 8, 1-11 - F. adultère

7.
Fais grandir en nous la foi, (bis)
Toi qui sauves l’affligée
Et lui donnes sa grandeur !
Nul ne la condamne, (bis)
Tu l’arraches à ses déserts
Pour l’entrée dans ton Royaume.