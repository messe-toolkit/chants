R.
Je te cherche, Dieu, tu es mon Dieu,
Et je t’appelle,
Je te cherche, Dieu, entends la voix,
De ma prière.

1.
Comme une biche vient au torrent,
Je te cherche et te désire.
Sois la source qui ne tarit pas,
Fleuve d’eau vive.