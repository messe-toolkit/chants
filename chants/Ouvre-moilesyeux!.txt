1.
S’il te plaît, Seigneur,
Ouvre-moi les yeux
Devant tes merveilles :
L’automne et ses couleurs,
La symphonie des fleurs !
S’il te plaît, Seigneur,
Donne-moi des yeux soleil,
Des yeux qui s’émerveillent !

R.
J’ai besoin de toi pour éveiller mes yeux,
J’ai besoin de toi, et si tu m’aides un peu,
Tout, en moi, s’éveillera !

2.
S’il te plaît, Seigneur,
Fais battre mon cœur
Devant tes merveilles :
L’amour de mes parents,
La venue d’un enfant.
S’il te plaît, Seigneur,
Donne-moi un cœur-soleil,
Un cœur qui s’émerveille !

3.
S’il te plaît, Seigneur,
Ouvre-moi les mains
Devant tes merveilles :
L’eau vive et le raisin,
Le blé avant le pain !
S’il te plaît, Seigneur,
Donne-moi des mains-soleil,
Des mains qui s’émerveillent !