R.
Partons tout aussitôt
Pour un pays nouveau
Regagnons la montagne
Chantons le grand hallel
C’est lui qui nous appelle
“Nous voici Seigneur !” 

1.
Puisqu’il nous a choisis, et puisqu’on a dit : “Oui” ne faisons pas attendre
Nous n’emporterons rien, sans peur du lendemain, c’est Lui qui nous conduit
Debout et déjà prêt, les sandales aux pieds le peuple peut comprendre
Le sens de son destin et le pain sans levain, puisqu’il nous a choisis.

2.
Puisqu’il est avec nous et puisqu’habite en nous le feu de sa présence
La flamme est au buisson pour révéler son Nom, il a besoin de nous
Et sans se consumer, ce pacte d’amitié fait le Peuple d’Alliance
C’est lui qui nous envoie, il est le feu de joie, puisqu’il est avec nous.