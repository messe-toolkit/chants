R.
OUVRE NOS YEUX A TA LUMIERE
FILS DU TRES HAUT, JESUS SEIGNEUR.
VIENS DIRE AU MONDE "PAIX SUR TERRE".
JOIE POUR LES HOMMES ET GLOIRE A DIEU !
JOIE POUR LES HOMMES ET GLOIRE A DIEU !

1.
Dans notre nuit quand l'aube s'est levée,
Le monde avait fermé ses fenêtres.
Quel homme attendait de voir naître
Un Dieu qui prend la voie de l'ombre.

2.
A Bethléem, les pauvres sont venus
Ils ont trouvé l'enfant et sa mère ;
Comment découvrir le mystère
D'un Dieu qui prend la voie d'enfance ?

3.
Tes messagers proclament tout joyeux :
L'étoile est apparue, Dieu nous sauve !
Partons annoncer le Royaume
D'un Dieu qui prend nos voies humaines.

4.
Sans dire un mot, le Verbe se fait chair
Il établit chez nous sa demeure.
Noël nous prépare à cette heure,
Où Dieu prendra la voie de Pâques.