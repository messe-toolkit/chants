CONNAISSEZ-VOUS L’HISTOIRE, CETTE HISTOIRE ROMANTIQUE
D’UN POMMIER ÉTONNANT, D’UN POMMIER MYSTÉRIEUX.
IL DONN’ DES TONN’S DE POMMES, DES POMMES FANTASTIQUES.
C’EST L’ARBRE ÉVIDEMMENT, C’EST L’ARBRE GÉNÉREUX.

1.
Un vieil homme m’a raconté que quand il était petit,
Il jouait tout’ la journée avec l’arbre son ami.
Une cord’, une balançoir’… pour toi…
Des ficell’s, une caban’… pour toi…
Quelques feuill’s, une couronn’… pour toi…
Et reviens quand tu veux,
Je suis le plus heureux
Des arbres généreux.

Pont
Mais un jour, devinez quoi ?
Le pommier est tout en joie
Devant lui se tient l’enfant
Disparu depuis longtemps.

2.
Quand l’enfant est revenu, il était adolescent.
On ne sait pour quell’ raison il avait besoin d’argent.
Tout’s mes pomm’s, roug’s ou vert’s… pour toi…
Reinett’s ou pomm’s d’api… pour toi…
Cent paniers pleins de fruits… pour toi…
Et reviens quand tu veux,
Je suis le plus heureux
Des arbres généreux.

3.
Maintenant je suis papa, dit à l’arbre le garçon,
Donne-moi toutes tes branch’s pour fabriquer ma maison.
Des murs, une charpent’… pour toi…
Une porte et des fenêtr’s… pour toi…
Une table et quelques chais’s… pour toi…
Et reviens quand tu veux,
Je suis le plus heureux
Des arbres généreux.

4.
Bonjour l’arbre mon ami, tu as vu mes cheveux blancs ?
Je voudrais partir au loin, traverser les océans.
Une coque avec mon tronc… pour toi…
Un pont et trois grand-mâts… pour toi…
De la proue à la poup’… pour toi…
Et reviens quand tu veux,
Je suis le plus heureux
Des arbres généreux.

5.
Un vieillard au dos courbé, fatigué, trainait les pieds.
Viens t’asseoir mon vieil ami lui murmure le pommier.
Une souch’ comme tabouret… pour toi…
Que dis-je, un canapé… pour toi…
Encore un peu de moi… pour toi…
Et reviens quand tu veux,
Je suis le plus heureux
Des arbres généreux.