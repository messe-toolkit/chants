1.
Comme l’astre du matin
Fils de Dieu, tu paraîtras ;
Ta lumière sans déclin
À nos yeux se lèvera,
Et chacun reconnaîtra
S’il est bien de tes amis :
Les talents que Dieu sema
Auront-ils porté du fruit ?

2.
Ta clarté rayonnera
Au plus noir de notre nuit,
Et ce jour paraît déjà
Pour qui reste à la vigie.
Bienheureux de te servir,
De tout coeur il entendra :
La demeure de ma vie,
Qu’elle s’ouvre aussi pour toi !

3.
Dernier soir, il finira
L’ancien monde avec ses riens,
Et par toi, Seigneur, naîtra
Une terre au jour sans fin.
Nous croyons au lendemain
Où ton peuple exultera :
Gloire à Dieu pour le festin !
Toute chair connaît ta joie.