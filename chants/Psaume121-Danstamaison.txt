1.
C'est grande joie, Seigneur, et je le dis sans cesse :
un jour je parviendrai dans ta maison.
Mes yeux se fermeront, je cesserai ma course.
Un jour je parviendrai dans ta maison.

2.
Nos pierres s'uniront pour bâtir ta cité,
la paix resplendira dans ta maison.
Les peuples chanteront le chant de l'unité,
la paix resplendira dans ta maison.

3.
Marchons, frères humains, vers la Ville nouvelle,
le bonheur nous attend dans sa maison.
Les aveugles verront, les boiteux marcheront,
le bonheur nous attend dans sa maison.

4.
Dansons sur tes parvis, chantons dans tes palais,
oui, Seigneur, notre Dieu, dans ta maison.
Nous sommes dans ton coeur
et ton coeur nous emplit,
oui, Seigneur, notre Dieu, dans ta maison.