1.
Si tu ouvres un chemin de liberté
Pour ton frère écrasé par l'esclavage,
Si ton coeur a des mots de vérité,
Loin des gestes qui blessent et qui menacent,
Alors en pleine nuit ta lumière brillera,
Alors en plein désert le Seigneur te comblera,
Tu seras le bâtisseur de la cité ;
Toute brèche par toi se fermera.

2.
Si ton pain met debout les affamés
Dans un monde où les pauvres sont sans nombre,
Si ton coeur sait comprendre l'opprimé
Quand il veut le soleil d'une rencontre,
Alors en pleine nuit ta lumière brillera,
Alors en plein désert le Seigneur te comblera,
Tu seras comme un jardin bien irrigué ;
De ta source une eau vive jaillira.

3.
Si le Jour du Seigneur devient ta joie,
Jour de fête à la gloire de l'Unique,
Si ton coeur improvise un chant de foi
Pour Celui dont l'amour est sans limite,
Alors en pleine nuit ta lumière brillera,
Alors en plein désert le Seigneur te comblera,
Ton Berger te mènera sur les hauteurs ;
Près de Dieu longue vie tu connaîtras.