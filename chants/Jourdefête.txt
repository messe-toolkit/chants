JOUR DE FÊTE, JOUR DE JOIE,
LE SEIGNEUR NOUS INVITE.
JOUR DE FÊTE, JOUR DE JOIE,
ALLÉLUIA !

1.
Tout l’monde est arrivé
Nous voici rassemblés
Avant de commencer
On va se présenter.
Moi, je te dis : “Bonjour !”
Toi, tu me dis : “Bonjour !”
Jésus nous réunit
Pour qu’on soit tous amis.

2.
Une maman nous lit
Ce que Jésus a dit.
Puis elle expliquera
Ce qu’on ne comprend pas.
On chante, on applaudit,
On dit : “Jésus, merci !
Nous voulons, nous aussi,
Devenir tes amis.”

3.
Chacun vient apporter
Ce qu’il a préparé :
Une fleur, un cadeau
Ou un dessin très beau.
Voici, pour Toi, Seigneur,
Je l’ai fait, de bon coeur
Je vais le partager
Pour apprendre à aimer.