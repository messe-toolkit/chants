■ 1.
a) Gloire à Dieu au plus haut des cieux,
b) Paix sur la terre aux hommes qu’il aime.
c) Gloire à Dieu au plus haut des cieux !
■ 2.
a) Nous te louons, nous te bénissons ;
b) Nous t’adorons, nous te glorifions.
c) Nous te rendons grâce pour ton immense gloire !
■ 3.
a) Seigneur Dieu, Roi du ciel, Dieu le Père tout puissant.
b) Seigneur, Fils unique, Jésus Christ,
c) Seigneur Dieu, Agneau de Dieu, le Fils du Père !
■ 4.
a) Toi qui enlèves le péché du monde, prends pitié de nous !
b) Toi qui enlèves le péché du monde, reçois notre prière !
c) Toi qui es assis à la droite du Père, prends pitié de nous !
■ 5.
a) Car toi seul es saint, toi seul es Seigneur ;
b) Toi seul es le Très-Haut, Jésus Christ, avec le Saint-Esprit,
c) Dans la gloire de Dieu le Père. Amen!