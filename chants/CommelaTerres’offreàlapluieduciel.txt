COMME LA TERRE S'OFFRE À LA PLUIE DU CIEL

COMME LA TERRE S'OFFRE À LA PLUIE DU CIEL
POUR PORTER FRUIT,
VOICI TON PEUPLE, SEIGNEUR, QUI S'ÉLANCE VERS TOI
EN UN SEUL COEUR :
"EMMANUEL ! O MON SEIGNEUR, VIENS !"

1.
Fils de Dieu et de la femme
Tu viens nous révéler l'amour du Père
Au secret de nos vies.

2.
Vrai Soleil, sauveur du monde
Tu viens percer la nuit profonde
Montre-nous ta clarté !

3."Avancez en ma présence,
Sans vous lasser, invoquez mon nom,
Vous serez des témoins !"

4."Écoutez, suivez mes chemins,
Vous connaîtrez la joie du royaume
Promis aux tout-petits !"

5." Demeurez dans ma parole
Jusqu'à ce que se lève en vos cœurs
L'étoile du matin !"

6."Tenez bon dans la prière
Vous recevrez mon souffle de vainqueur,
Vous serez des vivants !"