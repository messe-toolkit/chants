« Élevé de terre, Jésus attire à lui tous les hommes. »
L’Heure est venue :
mystère d’ombre et de clarté,
traversée de mort et de vie en plénitude.
« Celui qui aime sa vie, la perd. »
Le Fils de l’Homme,
fidèle au rendez-vous de l’obéissance,
tombe en terre pascale.
Le grain va mourir : il donnera son fruit !
« Là où je suis, là aussi sera mon serviteur. »
Maintenant, dans les coeurs renouvelés,
l’Alliance de communion est à jamais gravée.
« Rends-moi la joie d’être sauvé ! »
Maintenant, dans le rejet du prince de ce monde,
le Serviteur attire à lui tous les hommes.
« Rends-moi la joie d’être sauvé ! »
Maintenant, dans l’Amour à sa perfection,
le Nom du Père est sanctifié.
« Père, glorifie ton Nom ! »
L’Heure est venue, et c’est maintenant :
le signe de l’exode vers le Père devient pour le Fils
instant d’éternelle victoire.
« Je l’ai glorifié et le glorifierai encore ! »