ON RECHERCHE UN HOMME APPELÉ JÉSUS
NOS LOIS, NOS COUTUMES, NOS IDÉES REÇUES,
IL NOUS LES BOUSCULE: JUSQU’OÙ VA-T-IL ALLER ?
ON CHERCHE CET HOMME POUR L’EXÉCUTER.

1.
SABBAT
Il se dit “Maître du Sabbat”
Pour qui se prend-il celui-là ?
Il guérit des handicapés,
Arrache des épis de blé.
Et précisément ce jour-là,
Où l’on ne fait rien
C’est la Loi !
Pas même pour aider ou soigner.
C’est ainsi qu’on a toujours fait.

2.
EXCLUS
Il ne cesse de fréquenter
Tous ceux que l’on doit écarter :
Les pauvres, les femmes, les enfants,
Les pécheurs, les non-pratiquants.
Il mange avec des publicains,
Dit du bien des Samaritains.
Ceux qui sont malades, il nous dit
Que Dieu ne les a pas punis.

3.
TEMPLE
“Je suis le vrai Temple, dit-il,
Le vôtre est un figuier stérile.”
Vous savez qu’il vient de chasser
De l’esplanade, à coups de fouets,
Les vendeurs et leurs animaux.
Puis il a osé dire, bien haut :
“Ce Temple est maison de prière,
Pour tous les hommes de la terre.”

4.
FRONTIÈRES
Il nous dit que l’on doit aimer
Tous les hommes, sans distinguer,
L’étranger et même l’ennemi,
Les païens (“que Dieu aime aussi”).
Avec des idées comme celles-là,
Il nous renverse notre foi.
Dieu est pour nous et rien qu’à nous.
On est son Peuple, un point c’est tout.

5.
ET NOUS?
Il passait, en faisant le bien.
Il disait d’aimer son prochain
Il demandait de pardonner
Comme lui-même nous l’a montré.
Après tout, on ne comprend pas
Ce qui est si mal dans tout ça.
Pourquoi voulait-on l’arrêter ?
Le faire taire et l’exécuter ?

ON RECHERCHE UN HOMME APPELÉ JÉSUS
NOS LOIS, NOS COUTUMES, NOS IDÉES REÇUES,
IL NOUS LES BOUSCULE POUR NOUS LIBÉRER.
ON CHERCHE CET HOMME CAR ON VEUT L’AIMER.