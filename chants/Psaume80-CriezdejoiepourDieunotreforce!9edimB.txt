CRIEZ DE JOIE POUR DIEU, NOTRE FORCE !

1.
Jouez, musiques, frappez le tambourin,
la harpe et la cithare mélodieuse.
Sonnez du cor pour le mois nouveau,
quand revient le jour de notre fête.

2.
C’est là, pour Israël, une règle,
une ordonnance du Dieu de Jacob ;
Il en fit, pour Joseph, une loi
quand il marcha contre la terre d’Égypte.

3.
J’entends des mots qui m’étaient inconnus :
« J’ai ôté le poids qui chargeait ses épaules ;
ses mains ont déposé le fardeau.
Quand tu criais sous l’oppression, je t’ai sauvé.

ALLÉLUIA. ALLÉLUIA.

Ta parole, Seigneur, est vérité ;
dans cette vérité, sanctifie-nous.