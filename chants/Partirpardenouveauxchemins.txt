1.
Avec Toi, rien ne sera pareil
C’est devant, que nous attend le ciel
Tous ensemble, nous devenons chemin
Ton Amour a besoin de nos mains.

PARTIR, PAR DE NOUVEAUX CHEMINS,
ENSEMBLE ON IRA BIEN PLUS LOIN,
SEIGNEUR, SOLEIL DE NOS MATINS,
C’EST TOI QUI OUVRES LE CHEMIN.

2.
Avec Toi, les pas ne font plus peur
C’est devant, que s’allume le ciel
Tous ensemble, allumons ce grand feu
Ton Amour a besoin de nos yeux.

3.
Avec Toi, qu’importe le passé
C’est devant, qu’on regarde le ciel
Tous ensemble, habitons le présent
Ton Amour a besoin de vivants.