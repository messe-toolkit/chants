PETITS PAS, PETITS PAS
OÙ TE MÈNERONT-ILS
QUI DONC SUIT PAS À PAS
CHACUN DE CES PAS-LÀ
PETITS PAS, PETITS PAS
OÙ TE MÈNERONT-ILS
PETITS PAS

1.
Premier pas de la vie
Quel effort fabuleux
On oublie bien trop vite
Combien c’est périlleux
Quelques pas hésitants
La maison en sommeil
Et ranger, déranger
Tant de rêves au réveil

2.
Quelques pas enjambés
Le chemin te sourit
Se lancer, s’élancer
Et que danse la vie
Quelques pas essoufflés
Le soleil perd le Nord
Alors vite chanter
En changeant de décor

3.
Quelques pas paresseux
Quelques pas météores
Et croiser, décroiser
La vie, l’amour, le sort
Et laisser, délaisser
Le passé qui s’enfuit
Se laisser emporter
Par l’élan de la vie