LE PLUS BEAU DES ENFANTS DES HOMMES
EST DEVENU POUR NOUS SAUVER.
LE DERNIER DES DERNIERS DES HOMMES
POUR NOUS, IL A TOUT SUPPORTÉ.

1.
On l’a compté pour rien
Fouetté sur tout le corps
On l’a traité de chien
Et conduit à la mort.

2.
L’angoisse au fond du coeur
Il vit son agonie
Au-delà de la peur
Entendez-le qui prie.

3.
Il va tout supporter
Sans crier au secours
Pour nous réconcilier
Il invente l’amour.

4.
Achevé sur la croix
Agneau dans l’abattoir
Il nous pose un pourquoi
Il invente l’espoir.

5.
Dans le noir du tombeau
Dans la mort, dans la nuit
Éclate un jour nouveau
Il invente la vie.