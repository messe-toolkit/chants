1.
Pâque de Jésus-Christ Sauveur !
Il règne en sa victoire,
Triomphe de l'amour :
Ses bras ouverts en croix Dressent vers Dieu
L'angoisse de la vie
Et portent vers lui nos cœurs.

2.
Vivre ressuscités pour Dieu
Dans la lumière neuve,
Aurore de la joie :
Les hommes surgiront
Au grand espoir
Qui monte dans leur corps
À l'aube de ce matin.

3.
Peuple de baptisés, marqués
Du Sceau de la promesse,
Témoins de Jésus-Christ,
Venez manger la chair,
Boire le sang
Du Fils aimé de Dieu
Pour vivre de son Esprit.

4.
L'heure paraît déjà : veillez !
Car Dieu dresse la table
Aux Noces de l'Agneau :
Le Maître et le Seigneur Vient nous servir
Le vin de son retour :
Victoire de charité.