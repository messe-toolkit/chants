URGENCE DE LA BONNE NOUVELLE !
URGENCE ! LA PAROLE EST SI BELLE !
VENUE DU CIEL COMME LA PLUIE,
SOURCE DE PAIX POUR AUJOURD’HUI.

1.
Jamais homme n’a parlé, parlé comme Jésus :
Recevez l’Evangile de Paix.
Jamais homme n’a aimé, aimé comme Jésus :
Proclamez sa tendresse à jamais.

2.
Jamais homme n’a marché, marché comme Jésus :
Approchez, il vous trace un chemin.
Jamais homme n’a béni, béni comme Jésus :
Recueillez tout bienfait de ses mains.

3.
Jamais homme n’a veillé, veillé comme Jésus :
Rallumez vos regards endormis.
Jamais homme n’a prié, prié comme Jésus :
Respirez le grand vent de l’Esprit.