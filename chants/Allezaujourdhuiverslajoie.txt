Allez aujourd'hui vers la joie qui s'avance :
Christ est ressuscité !
Et l'homme découvre, s'il renaît en lui,
L'éternelle enfance.

Elle est consommée
L'oeuvre des sept jours
Dans l'éveil du dimanche !
Le temps peut reprendre son cours
Mais tout est transformé.
Voici de nouveaux signes :
Le Pain, semence
Des moissons de Dieu,
Et le vin, sève de sa vigne.

Vivez aujourd'hui la mémoire pascale :
Christ est ressuscité !
Et l'homme découvre, s'il fait route en lui,
Sa patrie natale.

Elle est accomplie
L'oeuvre du Vivant
Qu'annonçait la promesse !
À nous de puiser maintenant
Nos vivres dans sa vie.
L'amour tient table ouverte,
Sa voix nous presse
Au festin de Dieu :
Jour de joie, jour de Pâque offerte !

Chantez aujourd'hui l'indicible merveille :
Christ est ressuscité !
Et l'homme découvre, s'il se perd en lui
Une vie nouvelle.