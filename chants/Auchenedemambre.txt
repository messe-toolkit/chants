R.
Ne passe pas sans l'arrêter, mais dis nous qui tu es.

1.
Au chêne de Mambré,
Abraham est assis
à l'heure de midi.
Ayant levé les yeux,
il voit trois inconnus
debout pas loin de lui.

2.
Il se lève aussitôt,
il court au-devant d’eux,
s’incline avec respect :
« Monsieur, je vous en prie,
arrêtez-vous ici ;
vous vous reposerez ».

3.
Il vient trouver Sara,
lui dit : « Pétris du pain ;
fais cuire des gâteaux ».
Il court à son troupeau,
prend le veau le plus beau,
du fromage et du lait.

4.
Durant tout le repas
Abraham est debout :
il sert ses invités.
Alors ils lui demandent :
« Où est Sara, ta femme?
- Elle est ici, sous la tente ».

5.
« Attends encore un an
et quand je reviendrai
ta femme aura un fils ».
Sara qui écoutait
riait et se disait :
« Je suis bien trop âgée ! »

6.
Mais Abraham a cru
Il eut confiance en Dieu
Pour Dieu rien n'est trop merveilleux