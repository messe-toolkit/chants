R.
Tu nous parles aujourd'hui par ton Fils,
Gloire à toi, Seigneur de l'Alliance :
Une alliance donnée pour la vie,
Une route éclairée d'espérance !

1.
Tes pensées ne sont pas nos pensées,
Elles changent le coeur des humains.
Tes chemins ne sont pas nos chemins,
Vers la Pâque ils nous font avancer.

2.
Tes pensées ne sont pas nos pensées,
Mieux que nous tu connais notre bien.
Tes chemins ne sont pas nos chemins,
Pour gravir les sommets de la paix.

3.
Tes pensées ne sont pas nos pensées,
Tu nous veux libérés de tout lien.
Tes chemins ne sont pas nos chemins,
Mais qui donc les prendra sans trembler ?

4.
Tes pensées ne sont pas nos pensées,
Ton Esprit les révèle à chacun.
Jésus Christ est l'unique Chemin,
Pour aller vers la Terre où tu es.