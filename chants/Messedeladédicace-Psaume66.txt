R.
Dieu, que les peuples t'acclament,
Qu'ils t'acclament tous ensemble.

1.
Que ton visage s'illumine pour nous
Et ton chemin sera connu sur la terre,
Ton salut parmi toutes les nations.

2.
Que les nations se réjouissent,
Qu'elles chantent,
Car tu gouvernes le monde avec justice,
Et tu conduis les nations sur la terre.

3.
Dieu, notre Dieu, nous bénit ;
Qu'il nous bénisse, et qu'il soit adoré
Sur toute l'étendue de la terre.