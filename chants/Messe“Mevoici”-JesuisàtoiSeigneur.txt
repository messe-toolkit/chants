JE SUIS À TOI, SEIGNEUR.
JE SUIS À TOI
POUR TOI JE VIS, SEIGNEUR.
POUR TOI JE VIS.

1.
Que veux-tu de moi ?
Toi qui m’a créé(e)
Je suis tout à toi
Qui m’as appelé(e) !
Que veux-tu de moi ?
Tu m’as attendu(e)
Je le sais, sans toi
Je serai perdu(e) !

2.
Que veux-tu de moi ?
Je dis « me voici »
Je suis tout à toi
Je t’offre ma vie
Que veux-tu de moi ?
Je te dirai « oui »
Je suis tout à toi
Guide mon esprit.

3.
Que veux-tu pour moi
L’ombre ou le soleil ?
Que veux-tu pour moi
L’épreuve ou le ciel ?
Je suis tout à toi
Pour toi agissant
Je suis tout à toi
Dis-moi où ? Comment ?