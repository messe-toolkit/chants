R.
Un Sauveur nous est né, c'est le Christ, le Seigneur.

Psaume 95
Chantez au Seigneur un chant nouveau,
chantez au Seigneur, terre entière,
chantez au Seigneur et bénissez son nom !

De jour en jour, proclamez son salut,
racontez à tous les peuples sa gloire,
à toutes les nations ses merveilles !

Joie au ciel ! Exulte la terre !
Les masses de la mer mugissent,
la campagne tout entière est en fête.

Les arbres des forêts dansent de joie
devant la face du Seigneur, car il vient,
pour gouverner le monde avec justice.