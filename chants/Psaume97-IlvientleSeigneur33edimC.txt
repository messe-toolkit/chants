IL VIENT, LE SEIGNEUR,
GOUVERNER LES PEUPLES AVEC DROITURE.

1.
Jouez pour le Seigneur sur la cithare,
sur la cithare et tous les instruments ;
au son de la trompette et du cor,
acclamez votre roi, le Seigneur !

2.
Que résonnent la mer et sa richesse,
le monde et tous ses habitants ;
que les fleuves battent des mains,
que les montagnes chantent leur joie.

3.
Acclamez le Seigneur, car il vient
pour gouverner la terre,
pour gouverner le monde avec justice
et les peuples avec droiture !