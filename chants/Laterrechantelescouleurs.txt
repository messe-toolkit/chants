LA TERRE CHANTE LES COULEURS
QUE DIEU A MISES DANS NOS MAINS.

1.
Crayon bleu, crayon bleu,
Dieu dessine le ciel,
Crayon noir, crayon noir,
Dieu dessine la nuit.

2.
Crayon gris, crayon gris,
Dieu dessine la pluie,
Crayon d’or, crayon d’or,
Dieu dessine un soleil.

3.
Crayon brun, crayon brun,
Dieu dessine la terre,
Crayon roux, crayon roux,
Dieu dessine le feu.

4.
Crayon vert, crayon vert,
Dieu dessine la mer,
Crayon nid, crayon nid,
Dieu dessine un oiseau.

5.
Crayon blanc, crayon blanc,
Dieu dessine un enfant,
Crayon Dieu, crayon Dieu,
Dieu dessine l’amour.