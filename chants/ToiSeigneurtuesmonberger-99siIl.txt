R.
Toi, Seigneur, tu es mon berger,
Avec toi, je ne manque de rien.
Toi, Seigneur, tu me conduiras,
Avec toi, je ne crains rien.

1.
Vers les sentiers de justice
Et vers les eaux du repos
Tu me guides et me rassures
Et tu y refais mon âme à cause de ton nom.

2.
Même si je passe un jour
Dans un ravin de ténèbres,
Je sais que je ne crains rien,
Car tu es tout près de moi, tu es là qui me console.

3.
Oui je sais que toi, Seigneur,
Tu combats mes ennemis,
Tu me prépares une table,
Tu me parfumes la tête, je déborde de joie.

4.
Que la grâce et le bonheur
M’accompagnent chaque jour.
Et je ferai ma demeure
Dans la maison du Seigneur, tous les jours de ma vie.