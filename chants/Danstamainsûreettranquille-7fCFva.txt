Dans ta main sûre et tranquille,
Nous reposons comme un oiseau au creux du rocher,
Car tes pensées  sont au delà de nos pensées.
Ta miséricorde nous conduit,
Tant que dure pour nous ton aujourd’hui.

Dans ta main sûre et tranquille
Nous reposons