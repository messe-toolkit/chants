« C’est maintenant le temps favorable. »
La terre a reçu la promesse.
Les sillons crient leur soif de justice et de liberté.
« Ne me chasse pas loin de ta face. »
Le peuple de Dieu marche vers les humbles labours
qui déchirent les coeurs.
Il revient au désir de sa jeunesse.
Il attend la fertile moisson.
« Crée en moi un coeur pur, ô mon Dieu. »
L’appel au secret trois fois a retenti :
Souvenir perdu de ce qui fut donné,
écrit à jamais sur les mains du Père.
« Rends-moi la joie d’être sauvé. »
Porte fermée sur l’ineffable intimité,
ouverte à jamais par l’amour du Père.
« Rends-moi la joie d’être sauvé. »
Parfum répandu sur la radieuse austérité,
à jamais consacré dans la grâce du Père.
« Rends-moi la joie d’être sauvé. »
Au nom du Christ,
les réconciliés avec Dieu
reçoivent dans la joie de l’Esprit
le vêtement de tendresse et d’amour.
« C’est maintenant le jour du salut. »