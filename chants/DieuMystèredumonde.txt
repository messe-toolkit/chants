1.
Dieu,
Mystère du monde,
L’homme étonné contemple ton oeuvre:
Où est le sage, le savant,
Le poète ou l’ignorant
Qui dira
Ce que fut la genèse
De la vie,
De l’univers immense
De l’amour !

2.
Mystère du monde,
Feu rayonnant, ton jour se dévoile:
Un peuple pauvre, sans renom,
Méprisé par les nations,
S’est levé
Dans la nuit lumineuse,
Cheminant
Au pas de ton alliance
Vers l’amour.

3.
Mystère du monde,
Roi crucifié, ta pâque est obscure:
Nul autre signe que du pain
Pour te suivre en ce chemin,
Tu es seul
Pour créer le passage,
Attirer
Tout homme en sa souffrance
Dans l’amour.

4.
Mystère du monde,
Vie de l’Esprit, ta joie est féconde:
Comme une source bien cachée,
Comme un vent de liberté,
Tu surgis
Dans le coeur de l’Église,
Pour donner
Aux hommes l’espérance
Et l’amour.