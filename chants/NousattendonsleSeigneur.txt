R.
Nous attendons le Seigneur
Dans la joie des jours meilleurs
Le Seigneur !

1.
Il est venu sur notre terre
Et nous a sauvés pour toujours
Dénonçant le mal et la guerre
Il ne nous parle que d’amour !

2.
Il est venu sur notre monde
Nous annoncer des jours nouveaux
Si aujourd’hui la guerre gronde
Nos lendemains seront plus beaux !

3.
Il est venu sur notre terre
Nous partager son pain d’amour
Et nous donner sa vie entière
Jusqu’au dernier jour de ses jours.