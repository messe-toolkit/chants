R.
LE SEIGNEUR A FAIT POUR TOI DES MERVEILLES,
QUE SON NOM SOIT BENI !
LE SEIGNEUR A FAIT POUR TOI DES MERVEILLES,
BIENHEUREUSE MARIE !

1.
Marie s'est endormie
pour quitter la terre,
Marie s'en est allée
vers l'autre vie !

2.
Marie veille sur nous
à côté du Père,
elle est dans sa maison
auprès de Lui.