Il est grand, le mystère de la foi :
Nous proclamons ta mort, Seigneur Jésus,
nous célébrons ta résurrection,
nous attendons ta venue dans la gloire.