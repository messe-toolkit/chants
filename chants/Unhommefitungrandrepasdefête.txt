Un homme fit un grand repas de fête,
et quand vint l’heure,
il envoya son serviteur chercher les invités :
« Venez, tout est prêt, venez à la fête ».
Venez manger mon pain
venez boire le vin
que j’ai préparé pour vous.
« Venez, tout est prêt, venez à la fête ».
Gloire au Père, le Maître du festin,
Gloire à Jésus-Christ, son Serviteur,
Gloire à l’Esprit qui réjouit notre coeur !
« Venez, tout est prêt ! »