Christ est ressuscité, alléluia, alléluia (bis)
En lui nous sommes tous sauvés, alléluia (bis)
Alléluia, alléluia, alléluia, alléluia.

Les femmes courent au tombeau, alléluia, alléluia (bis)
Afin d'y embaumer Jésus, alléluia (bis)
Alléluia, alléluia, alléluia, alléluia.

Mais l'ange du Seigneur dit : alléluia, alléluia (bis)
Allez et soyez dans la joie, alléluia (bis)
Alléluia, alléluia, alléluia, alléluia.

Partageons tous la même joie, alléluia, alléluia (bis)
Et bénissons tous le Seigneur, alléluia (bis)
Alléluia, alléluia, alléluia, alléluia.