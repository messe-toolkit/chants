NOUS AVONS VU L’ÉTOILE
ET NOUS VENONS VERS TOI
NOUS AVONS VU L’ÉTOILE,
ELLE A GUIDÉ NOS PAS

1.
Comme les mages en route
Pour rechercher un Roi
Je viens pour t’apporter
Tous ces cadeaux remplis de joie

2.
Comme les mages arrivent,
S’inclinent devant Toi
Je viens pour déposer
Tous ces cadeaux remplis de foi

3.
Comme les mages repartent
Pour annoncer la joie
Je viens de recevoir
Un vrai trésor au fond de moi