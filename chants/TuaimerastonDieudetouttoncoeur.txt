Antienne - stance
Tu aimeras ton Dieu de tout ton coeur ; tu aimeras ton frère comme toi-même.

R.
Oculi nostri ad Dominum Jesum
Oculi nostri ad Dominum nostrum

Versets

Ps 32

1.
Criez de joie pour le Seigneur, hommes justes !
hommes droits, à vous la louange !
Rendez grâce au Seigneur sur la cithare,
jouez pour lui sur la harpe à dix cordes.

2.
Oui, elle est droite, la parole du Seigneur ;
il est fidèle en tout ce qu'il fait.
Il aime le bon droit et la justice ;
la terre est remplie de son amour.

3.
Le Seigneur a fait les cieux par sa parole,
l'univers, par le souffle de sa bouche.
Il amasse, il retient l'eau des mers ;
les océans, il les garde en réserve.

4.
Heureux le peuple dont le Seigneur est le Dieu,
heureuse la nation qu'il s'est choisie pour domaine !
Que ton amour, Seigneur, soit sur nous
comme notre espoir est en toi !

Ou bien

Ps 62

1.
Dieu, tu es mon Dieu, je te cherche dès l'aube :
mon âme a soif de toi ;
après toi languit ma chair,
terre aride, altérée, sans eau.

2.
Je t'ai contemplé au sanctuaire,
j'ai vu ta force et ta gloire.
Ton amour vaut mieux que la vie :
tu seras la louange de mes lèvres !

3.
Toute ma vie je vais te bénir,
lever les mains en invoquant ton nom.
Comme par un festin je serai rassasié ;
la joie sur les lèvres, je dirai ta louange.

4.
Dans la nuit, je me souviens de toi
et je reste des heures à te parler.
Oui, tu es venu à mon secours :
je crie de joie à l'ombre de tes ailes.