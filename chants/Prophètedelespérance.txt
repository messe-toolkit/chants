R.
PROPHÈTE DE L’ESPÉRANCE,
LÈVE-TOI PARMI TES FRÈRES !
PÈLERIN DE LA CONFIANCE,
PRENDS LA ROUTE DES APÔTRES.
PROPHÈTE DE L’ESPÉRANCE,
LÈVE-TOI PARMI TES FRÈRES !
SOIS TÉMOIN DE L’ÉVANGILE
SUR LES ROUTES DE LA TERRE.

1.
En chemin, une rencontre,
Un regard, une parole ;
Laisse là tous tes filets :
Dieu t’appelle à le suivre !

2.
N’aie pas peur d’aller au large
En témoin de l’évangile ;
Dieu t’appelle aujourd’hui
À passer sur l’autre rive !

3.
Appelé par ton baptême,
Envoyé de par le monde,
Dieu t’appelle à aimer,
À aimer comme lui-même !