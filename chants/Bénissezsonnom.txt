BÉNISSEZ SON NOM,
BÉNISSEZ L’ESPRIT, LE FILS, LE PÈRE.
BÉNISSEZ SON NOM,
BÉNISSEZ LA VIE DANS LA LUMIÈRE.
TOUTE LA CRÉATION ET TOUTES LES NATIONS
BÉNISSEZ, BÉNISSEZ SON NOM.

1.
Vous, soleil et lune
BÉNISSEZ SON NOM;
La clarté, la brume
BÉNISSEZ SON NOM.
Célébrez le nom du Seigneur,
Dieu créateur, Dieu sauveur.

2.
Toutes les musiques
BÉNISSEZ SON NOM;
Percussions rythmiques
BÉNISSEZ SON NOM.
Célébrez le nom du Seigneur,
Dieu créateur, Dieu sauveur.

3.
Vous, garçons et filles
BÉNISSEZ SON NOM;
Toutes les familles
BÉNISSEZ SON NOM.
Célébrez le nom du Seigneur,
Dieu créateur, Dieu sauveur.