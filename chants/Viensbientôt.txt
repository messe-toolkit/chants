1.
Viens bientôt, Sauveur du monde,
Lève-toi, clarté d'en-haut,
Vrai soleil du jour nouveau,
Viens percer la nuit profonde.

2.
Vois le mal et la souffrance,
Et tant d'hommes chancelants
Dans l'immense enchaînement
Du mépris et des violences.

3.
Ta naissance dans l'histoire
Transfigure nos tourments
En douleurs d'enfantement
Où déjà surgit ta gloire.