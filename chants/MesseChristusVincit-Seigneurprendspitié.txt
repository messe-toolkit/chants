SEIGNEUR PRENDS PITIÉ DE NOUS !
SEIGNEUR PRENDS PITIÉ DE NOUS !

1.
Seigneur Jésus, toi l’Envoyé pour nous guérir et nous sauver.
Nous te prions Dieu de bonté, écoute-nous et prends pitié !

2.
Ô Christ Jésus tu es venu chercher ce qui était perdu.
Nous te prions Dieu de bonté, écoute-nous et prends pitié !

3.
Seigneur élevé près de Dieu tu es venu ouvrir les cieux.
Nous te prions Dieu de bonté, écoute-nous et prends pitié !

KYRIE ELEISON ! KYRIE ELEISON !

GLORIA

GLOIRE À DIEU DANS LES CIEUX !
PAIX AUX HOMMES QU’IL AIME !

1.
Nous te louons, nous t’acclamons.
Ta Gloire nous la proclamons.
Seigneur du ciel nous t’adorons.
Père très Saint nous te chantons.

2.
Sauveur du monde, Jésus Christ,
Agneau de Dieu, le Fils béni.
Toi qui enlèves le péché,
Écoute-nous et prends pitié.

3.
Toi le seul Saint, le seul Seigneur,
Toi le Très haut, Jésus vainqueur,
Avec le Père et l’Esprit Saint
Dieu glorieux loué sans fin.

GLORIA DEO !
IN TERRA PAX HOMINIBUS !

ALLÉLUIA

ALLÉLUIA ! ALLÉLUIA ! ALLÉLUIA ! ALLÉLUIA !

1.
La Bonne Nouvelle est annoncée aux pauvres.
ALLÉLUIA !  ALLÉLUIA !

2.
Tu as les Paroles de la Vie Éternelle.
ALLÉLUIA ! ALLÉLUIA !

3.
Tu es le Chemin, la Vérité, la Vie.
ALLÉLUIA ! ALLÉLUIA !
ou : GLOIRE À TOI, SEIGNEUR JÉSUS CHRIST !

SANCTUS

Le ciel et la terre sont remplis de ta gloire.
Hosanna au plus haut des cieux.
Béni soit celui qui vient au nom du Seigneur.
Hosanna au plus haut des cieux.

SAINT ! SAINT ! SAINT, LE SEIGNEUR,
LE SEIGNEUR DIEU DE L’UNIVERS !

1.
Le ciel et la terre sont remplis de ta gloire.
HOSANNA AU PLUS HAUT DES CIEUX.
SANCTUS, SANCTUS, SANCTUS DOMINUS,
DOMINUS DEUS SABAOTH !

Béni soit celui qui vient au nom du Seigneur.
HOSANNA AU PLUS HAUT DES CIEUX.
SANCTUS, SANCTUS, SANCTUS DOMINUS,
DOMINUS DEUS SABAOTH !

ANAMNÈSE

GLOIRE À TOI QUI ÉTAIS MORT !
GLOIRE À TOI QUI EST VIVANT !
NOTRE SAUVEUR ET NOTRE DIEU !
VIENS, SEIGNEUR JÉSUS !

DOXOLOGIE

Par lui, avec lui et en lui,
à toi le Père tout-puissant dans l’unité du Saint-Esprit.
Honneur et gloire au Dieu vivant !
AMEN ! AMEN ! AMEN ! ALLÉLUIA !

AGNUS

AGNEAU DE DIEU, LE VRAI BERGER,
SAUVE-NOUS, PRENDS PITIÉ DE NOUS !

1 et 2.
Toi qui enlèves le péché du monde,
SAUVE-NOUS, PRENDS PITIÉ DE NOUS !
AGNUS DEI, AGNUS DEI, MISERERE NOBIS !

3.
Toi qui enlèves le péché du monde,
SAUVE-NOUS, DONNE-NOUS LA PAIX !
AGNUS DEI, AGNUS DEI, MISERERE NOBIS !