R.
ECOUTEZ MES AMIS !
ECOUTEZ DANS LA NUIT !
GRANDS ET PETITS ENFANTS
VENEZ ! PRENONS LE TEMPS !

1.
Installez vous au chaud près de la cheminée !
Marie prends ce coussin et viens près de Vianney !
Charles, reste avec moi sur le grand fauteuil vert !
Dehors la neige tombe dans le ciel noir d'hiver.

2.
C'est un soir de Noël, il y a très longtemps
Je ne sais plus très bien, j'étais encore enfant
Grand-père m'a raconté pour la première fois
Cette étonnante histoire venue des pays froids

3.
Il était une fois, loin dans une vallée
Au coeur d'une montagne, trois arbres qui rêvaient
Ils étaient bien petits avec leurs trois printemps
Mais que deviendraient-ils quand ils seraient plus grands ?