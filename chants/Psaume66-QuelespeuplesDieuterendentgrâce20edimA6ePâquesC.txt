QUE LES PEUPLES, DIEU, TE RENDENT GRÂCE ;
QU’ILS TE RENDENT GRÂCE TOUS ENSEMBLE !

ou : ALLÉLUIA !

1.
Que Dieu nous prenne en grâce et nous bénisse,
que son visage s’illumine pour nous ;
et ton chemin sera connu sur la terre,
ton salut, parmi toutes les nations.

2.
Que les nations chantent leur joie,
car tu gouvernes le monde avec justice ;
tu gouvernes les peuples avec droiture,
sur la terre, tu conduis les nations.

3.
La terre a donné son fruit ;
Dieu, notre Dieu, nous bénit.
Que Dieu nous bénisse,
et que la terre tout entière l’adore !