CANON
DIS SEIGNEUR COMBIEN DE JOURS,
COMBIEN DE NUITS,
COMBIEN DE FAIMS, COMBIEN DE PHOTOS,
COMBIEN DE VIES, COMBIEN DE COUPS,
COMBIEN DE PLEURS, COMBIEN DE VOIX.

1.
Dis Seigneur combien de jours
Combien de nuits, combien de soleils à l'horizon
Pour que ta paix éclate enfin sur nos moissons
Et combien de faims au creux du ventre à rassasier
Avant que ton vent de liberté vienne à souffler
Combien de photos reportages
Combien de premières pages
Où des visages d'enfants
Ressemblent à des morts vivants.

2.
Dis Seigneur combien de morts
Combien de vies, combien de rêves
au fond des prisons
Pour que ta paix jaillisse enfin sur nos maisons
Et combien de peurs sur nos visages à effacer
Avant que ton vent de liberté vienne à souffle
Combien de musées de la guerre
Combien de cimetières
Combien de flaques de sang
Et combien de cris d'innocents.

3.
Dis Seigneur combien de coups
Combien de pleurs combien de poètes assassinés
Pour que ta paix apporte enfin le temps d'aimer
Et combien de corps sous la torture déchiquetés
ant que ton vent de liberté vienne à souffler
Combien de vies que l'on arrache
Combien de coups de hache
Seigneur encore combien de fois
Te voir ainsi cloué en croix.