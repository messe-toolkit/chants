1.
Dans le livre de ton histoire
Tu viens relire pour y croire
Des mots qui ouvrent l’horizon
Des mots écrits avec passion
Ils chantent la fraternité
Ils sont en gras et soulignés.

R.
PARS EN AFRIQUE OU AILLEURS
PARS UN NOUVEAU JOUR SE LÈVE
PARS EN DIRECTION DU COEUR.
NE LAISSE PAS TOMBER TES RÊVES

2.
Tu dois sauter, faire le pas
Laisser la peur en contrebas
L’oiseau qui veut sa liberté
Abandonne son nid douillet
C’est l’exigence de la vie
Mais c’est du bonheur à l’appui.

3.
Le vent n’est pas toujours porteur
Et la météo de ton coeur
Annonce parfois la tempête
Un ciel pesant où tout s’arrête
Mais le soleil revient déjà
Ton désir est toujours là-bas.