AUJOURD'HUI, LE VERBE S'EST FAIT CHAIR, ALLELUIA.
VENEZ, ADORONS-LE, ALLELUIA.

Que Dieu nous prenne en grâce et nous bénisse,
que son visage s'illumine pour nous ;
et ton chemin sera connu sur la terre,
ton salut, parmi toutes les nations.

Que les nations chantent leur joie,
car tu gouvernes le monde avec justice ;
tu gouvernes les peuples avec droiture,
sur la terre, tu conduis les nations.

La terre a donné son fruit ;
Dieu, notre Dieu, nous bénit.
Que Dieu nous bénisse,
et que la terre tout entière l'adore !

Rendons gloire au Père tout-puissant,
À son Fils Jésus-Christ, le Seigneur,
À l'Esprit qui habite en nos coeurs,
Pour les siècles des siècles. Amen.