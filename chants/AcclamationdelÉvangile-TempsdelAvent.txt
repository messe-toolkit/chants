Alleluia, Alleluia, Alleluia.

1. (A, B, C)
Fais-nous voir, Seigneur, ton amour,
[ ]
et donne-nous ton salut.

2. (A, B, C)
Préparez le chemin du Seigneur,
rendez droits ses sentiers :
tout être vivant verra le salut de Dieu.

3. (A, B, C)
L'Esprit du Seigneur est sur moi :
il m'a envoyé
porter la Bonne Nouvelle aux pauvres.

4. (A)
Voici que la Vierge concevra :
elle enfantera un fils,
on l'appellera Emmanuel, "Dieu avec nous"

(B, C)
Voici la servante du Seigneur :
[ ]
que tout m'advienne selon ta parole.