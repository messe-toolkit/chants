ET LE TEMPS DES CHANSONS EST VENU, MA TOUTE BELLE,
LE DOUX CHANT DES TOURTERELLES ÉGAIE NOTRE TERRE.
LE FIGUIER A FLEURI, LÈVE-TOI, MA TOUTE BELLE,
AU PARFUM DE LA VIGNE, LÈVE-TOI ET VIENS !

1.
Le voici qui vient, mon bien-aimé,
J'entends sa voix, il bondit sur les montagnes,
Oui, c'est lui qui vient comme la gazelle,
Sur les collines, il me parle mon bien-aimé !

2.
Il me dit : "Ô viens, ma toute belle,
Viens, lève-toi, car l'hiver s'en est allé,
Les pluies ont cessé, se sont enfuies,
Et sur la terre, apparaissent tant de fleurs !"

3.
Tu accours vers moi, j'entends ta voix,
Dans les rochers, sur les routes escarpées :
"Tu es mon bonheur, ma toute belle,
Toi ma colombe, ton visage est séduisant !"