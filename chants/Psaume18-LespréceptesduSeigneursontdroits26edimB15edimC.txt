LES PRÉCEPTES DU SEIGNEUR SONT DROITS,
ILS RÉJOUISSENT LE COEUR.

1.
La loi du Seigneur est parfaite,
qui redonne vie ;
la charte du Seigneur est sûre,
qui rend sages les simples.

2.
La crainte qu’il inspire est pure,
elle est là pour toujours ;
les décisions du Seigneur sont justes
et vraiment équitables.

3.
Aussi ton serviteur en est illuminé ;
à les garder, il trouve son profit.
Qui peut discerner ses erreurs ?
Purifie-moi de celles qui m’échappent.

4.
Préserve aussi ton serviteur de l’orgueil :
qu’il n’ait sur moi aucune emprise.
Alors je serai sans reproche,
pur d’un grand péché.