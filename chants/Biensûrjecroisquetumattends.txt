1.
Depuis que tu t'es envolé
En me laissant trop loin derrière
Le monde a fermé ses volets
Le monde a fermé ses frontières
Ce jour là j'ai serré les poings
Je suis rentré dans mes silences
J'ai vu la mer partir au loin
J'ai vu s'éloigner les distances

R.
MON DIEU
JE CROIS QUE TU M'ATTENDS
MAIS TU ES LOIN DANS LES NUAGES
MON DIEU
QU'IL EST BIEN LONG LE TEMPS
LE TEMPS DE REVOIR TON VISAGE

2.
Tous les matins au petit jour
Je vais te guetter sur la route
J'ai beau espérer ton retour
Je me sens prisonnier du doute

R.
MON DIEU
JE CROIS QUE TU M'ATTENDS
MAIS TU ES LOIN DANS LES NUAGES
MON DIEU
QU'IL EST BIEN LONG LE TEMPS
LE TEMPS DE REVOIR TON VISAGE

3.
Retrouverai-je un jour tes yeux,
Ta voix qui était ma lumière
Même l'espoir est douloureux
Dis-moi ! entends-tu ma prière?

R.
MON DIEU
JE CROIS QUE TU M'ATTENDS
MAIS TU ES LOIN DANS LES NUAGES
MON DIEU
QU'IL EST BIEN LONG LE TEMPS
LE TEMPS DE REVOIR TON VISAGE