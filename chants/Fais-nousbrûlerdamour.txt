FAIS-NOUS BRÛLER D’AMOUR,
EN TA DOUCE PRÉSENCE
DIEU DE TOUTE BONTÉ,
GRÂCE ET FIDÉLITÉ.

1.
Le secret du Seigneur
Est pour ceux qui le craignent,
Heureux celui qui te cherche,
Son âme habitera le bonheur.
Heureux ceux dont tu es la force,
Des chemins s’ouvrent dans leurs coeurs.
Seigneur, que ta grâce nous porte :
Nous irons de hauteur en hauteur.

2.
Rassasie-nous de ton amour,
Nous vivrons dans la joie et les chants.
Apprends-nous la vraie mesure de nos jours,
Nous courrons sur la voie de tes commandements.
Mon coeur a brûlé de désir,
Je garderai tes exigences.
Je trouve en ta loi mon plaisir,
Toi seul… ma récompense.

3.
Oh…