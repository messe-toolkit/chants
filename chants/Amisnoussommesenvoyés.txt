AMIS, NOUS SOMMES ENVOYÉS
ANNONCER LE JOUR DE L’ESPÉRANCE,
CAR DIEU TOUJOURS ESSUIE LES LARMES,
CAR DIEU TOUJOURS EST LÀ OÙ VIVENT SES ENFANTS.
AMIS, NOUS SOMMES ENVOYÉS
DERRIÈRE LE MUR DES SOLITUDES,
CAR DIEU JAMAIS NE CONDAMNE,
CAR DIEU JAMAIS N’OUBLIE SES BIEN-AIMÉS. (bis)

1.
Ensemble nous sommes sa présence,
Nous sommes son fidèle visage.
Ensemble nous sommes son audace,
Nous sommes sa réelle tendresse.

2.
C’est là-bas dans les pays de l’absence,
C’est là-bas que nous le rencontrons.
C’est là-bas dans les pays de détresse,
C’est là-bas que nous le célébrons.

3.
En son Nom portons la Nouvelle :
« Votre Dieu vient vous relever ! »
En son Nom chantons l’Évangile :
« Votre Dieu vient pour le pardon ! »

4.
Comment pourrait-il nous délaisser
Alors qu’il a donné sa vie ?
Comment pourrait-il se détourner
Alors qu’il a passé la mort ?

5.
Amis, prenons sa Parole,
Accueillons-la, c’est un cadeau !
Amis, venons à sa table,
Recevons le pain de pleine vie !