UN AMI À DROITE,
UN AMI À GAUCHE,
DES AMIS DANS MON COEUR.
UN AMI À DROITE,
UN AMI À GAUCHE,
PARTAGEONS NOS COULEURS.

1.
Si tu as un ami
Donne-lui de ton temps ;
C’est bon de s’amuser.
Si tu as un ami
Prends du temps avec lui,
Ça construit l’amitié !

2.
Si tu as un ami,
Surtout écoute-le ;
C’est bon de partager.
Si tu as un ami
Prends du temps avec lui,
Ça construit l’amitié !

3.
Si tu as un ami
Parle-lui de ta vie ;
C’est bon de s’entraider.
Si tu as un ami
Prends du temps avec lui,
Ça construit l’amitié !

4.
Si tu as un ami
Fais fleurir des projets ;
C’est si bon d’inventer.
Si tu as un ami
Prends du temps avec lui,
Ça construit l’amitié !