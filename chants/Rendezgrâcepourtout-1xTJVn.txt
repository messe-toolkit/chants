R.
Rendez grâce pour tout,
Rendez grâce toujours
À Dieu le Père tout-puissant.
Rendez grâce pour tout,
Rendez grâce toujours
Au Nom de Jésus le Seigneur.

1.
Cherchez en l´Esprit Saint ce qui vous comblera,
Récitez entre vous des psaumes et des hymnes.
Notre Seigneur, chantez-le de tout cœur !

2.
Tenez-vous donc debout avec, pour le combat,
Le casque du salut, le glaive de l´Esprit,
Armez-vous de la Parole de Dieu !

3.
Vivez dans la prière et les supplications,
Priez à tout moment dans la joie de l´Esprit.
Que le Seigneur vous accorde la paix !