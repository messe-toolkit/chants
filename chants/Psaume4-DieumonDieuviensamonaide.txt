Refrain :
Dieu, mon dieu, viens à mon aide,
Ô seigneur à mon secours,
Dieu, écoute nos prières
Souviens-toi de tes enfants.

1.
Quand je crie, Dieu, ma justice
Réponds-moi, libère-moi
De l’insulte et du mensonge
Sois la raison de ma vie.

2.
Au secret de tes silences
De notre désir de toi
Que s’éclaire ton visage
Soit le Rocher de ma vie.

3.
Garde en nos coeurs, ta présence,
Pour ne reposer qu’en toi
Pour habiter ta confiance
Sois le bonheur de ma vie.