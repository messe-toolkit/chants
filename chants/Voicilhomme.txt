R.
JESUS-CHRIST, ROI BLESSE,
DIEU COURONNE DE NOS EPINES,
O SEIGNEUR, PRENDS PITIE,
QUE TON PARDON NOUS ILLUMINE !

1.
L'homme,
Voici l'homme,
Jamais homme n'a parlé comme cet homme.
Roi de silence,
Roi qui se tait devant l'offense,
Roi de patience et de bonté.

2.
L'homme,
Voici l'homme,
Jamais homme ne fut vrai comme cet homme.
Roi de lumière,
Roi humilié dans la poussière,
Roi de prière et de clarté.

3.
L'homme,
Voici l'homme,
Jamais homme n'a aimé comme cet homme.
Roi de largesse,
Roi qui console nos détresses,
Roi de tendresse en nos duretés.