R.
TOUT POUR JÉSUS, TOUT POUR JÉSUS,
TOUT PAR AMOUR DE DIEU!
TOUT POUR JÉSUS, TOUT POUR JÉSUS,
TOUT PAR AMOUR DE DIEU!

1.
Maria Assunta,
Dès ton enfance tu appris
À travailler le jour, la nuit,
D’un coeur joyeux et résolu
Pour l’amour de Jésus.

2.
Maria Assunta,
Tu désiras te consacrer
À Jésus-Christ, ton bien-aimé.
Et sur les pas de saint François,
Tu fis de lui ton Roi !

3.
Maria Assunta,
Humble servante du Seigneur,
Tu l’as aimé avec ardeur,
Tu fis toujours “comme il lui plaît”
Selon sa volonté.

4.
Maria Assunta,
Tu as voulu partir au loin
Pour être en Chine le témoin
Près des enfants et des lépreux
De l’amour de ton Dieu.

5.
Maria Assunta,
Quand arriva le dernier jour,
C’est dans la paix et dans l’amour
Que tu partis vers le Soleil
À nul autre pareil !

6.
Maria Assunta,
Après ta mort, tu répandis
Un doux parfum de paradis :
Toutes tes soeurs, remplies de joie,
Chantèrent : “Alléluia !”