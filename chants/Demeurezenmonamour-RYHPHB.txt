R.
Demeurez en mon amour,
Car hors de moi
Vous ne pouvez rien faire.

1.
Je suis la vigne véritable
Et mon Père est le vigneron,
Tout sarment qui donne du fruit, il l´émonde
Afin qu´il en donne davantage.

2.
De même que le sarment ne peut donner de fruit
S´il ne demeure pas sur la vigne,
Ainsi vous ne donnerez pas de fruit
Si vous ne demeurez pas en moi.

3.
Si vous demeurez en moi +
Et que mes paroles demeurent en vous,
Demandez ce que vous voudrez et vous l´obtiendrez.
C´est la gloire de mon Père que vous donniez beaucoup de fruit
Et deveniez mes disciples.

4.
Comme le Père m´a aimé, +
Moi aussi je vous ai aimés :
Demeurez en mon amour.
Si vous gardez mes commandements,
Vous demeurerez en mon amour.

5.
Voici quel est mon commandement : +
Vous aimer les uns les autres
Comme je vous ai aimés.
Nul n´a de plus grand amour que celui-ci :
Donner sa vie pour ses amis.

6.
Je ne vous appelle plus serviteurs mais amis.
Tout ce que j´ai entendu de mon Père, je vous l´ai fait connaître ,
C´est moi qui vous ai choisis, afin que vous donniez du fruit,
Et que votre fruit demeure.