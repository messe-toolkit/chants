1.
Quand le Seigneur se montrera,
Trouvera-t-il au monde
Un peuple ferme dans la foi
Dont l´amour lui réponde ?

2.
Quand nous verrons du fond des cieux
Venir le Fils de l´Homme,
Les hommes tourneront les yeux
Vers ce Dieu qui les nomme.

3.
Nous entendrons du fond des temps
Le nom que Dieu nous donne :
Quand l´heure vient du jugement,
Nous croyons qu´il pardonne.

4.
Dieu nous attend depuis toujours,
Il nous promet sa gloire :
C´est le témoin de son amour
Qui conduit notre histoire.

5.
Nos lendemains seront bâtis
Avec nos pleurs, nos rires :
Notre destin, c´est aujourd´hui
Qu´il nous faut le construire.

6.
Si Dieu opère en nos combats,
Nous en chassons la haine
Et par sa force en notre bras
S´ouvrira notre chaîne.

7.
Le dernier jour est commencé :
Dieu parmi nous s´avance ;
Son Jour de gloire est arrivé,
Nous gardons l´espérance.