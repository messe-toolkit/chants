ALLÉLUIA, ALLÉLUIA.

Viens, Esprit Saint !
Pénètre le coeur de tes fidèles !
Qu’ils soient brûlés au feu de ton amour !

ALLÉLUIA.