R.
Joie dans le ciel, paix sur la terre !
Bonne nouvelle à tous les hommes :
Jésus nous conduit jusqu’au Père.

1.
Nous avons marché
Au pays de la nuit,
Guettant une clarté :
Quelqu’un s’est approché
Jésus,
Le Verbe de lumière.

2.
Nous avons frappé
A la porte de Dieu,
Cherchant la vérité :
Quelqu’un s’est révélé,
Jésus,
Sur lui l’Esprit repose.

3.
Nous avons gardé
L’espérance en nos coeurs
Quand tout s’en est allé :
Quelqu’un nous est donné,
Jésus,
L’Amour en plénitude.