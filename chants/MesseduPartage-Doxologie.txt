Par lui, avec lui et en lui, Amen.
À toi, Dieu le Père tout-puissant,
Dans l’unité du Saint-Esprit, Amen.
Tout honneur et toute gloire
Pour les siècles des siècles. Amen.