Pour nous, le Christ s'est fait obéissant,
Jusqu'à la mort, et la mort sur une croix.

Voilà pourquoi
Dieu l'a élevé souverainement
Et lui a donné le Nom,
Qui est au-dessus de tout nom.