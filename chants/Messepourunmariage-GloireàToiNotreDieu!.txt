R.
Gloire à toi, notre Dieu, ton amour nous unit !
Gloire à toi, notre Dieu, pour le don de la vie !

1.
Ta Parole est vérité, ton Alliance nous appelle.
Garde-nous dans la clarté, notre oui sera fidèle.

2.
Ton Esprit nous conduira sur des routes qui surprennent.
"Nul ne sait jusqu'où il va", mais sa force nous entraîne.

3.
Tu choisis notre maison, viens t'asseoir à notre table.
Le vrai pain que nous cherchons, Toi Seigneur, tu le partages.