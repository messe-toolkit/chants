1.
Source en attente, secret du monde,
Amour caché au creux du monde :
Christ au-dedans du mal de l'homme,
Au cœur des pauvres Christ vivant,
Au cœur des pauvres tu attends.

2.
Source nouvelle, vivant baptême
Où notre mal aspire au Père
Pour que détruit, il soit lumière :
Sainte Parole, Jésus-Christ.
Notre espérance n'est qu'un cri.

3.
Source éternelle, Esprit d'enfance,
Apporte au monde sa naissance :
En ton Amour tout est Présence,
Par Toi le monde voit son Dieu.
En Toi, le monde devient Dieu.

4.
Source vivante, ô Christ Lumière,
Tu es en nous la joie du Père :
Chante l'Esprit en tous nos frères !
Source de gloire, Trinité,
Ta joie aux hommes est partagée.