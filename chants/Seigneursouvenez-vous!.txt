1.
Seigneur, souvenez-vous de nos frères,
De ceux que nous aimions sur la terre ;
Accordez-leur enfin la lumière
Et la vivante paix de votre amour.

2.
Ayez pitié des pauvres qui meurent,
Sans un ami près d'eux qui les pleure ;
Qu'ils soient admis dans cette demeure
Où vous rassemblerez tous vos enfants.

3.
Pour nous, quand prendra fin le voyage,
Puissions-nous voir alors le Visage
De votre Fils béni dont l'image,
Malgré tant de péchés, poursuit nos cœurs