DIEU NOUS AIME, PETITS OU GRANDS,
À CHACUN IL DONNE SA FORCE.
DIEU NOUS AIME, PETITS OU GRANDS,
NOUS SOMMES SES ENFANTS.

1.
Un éléphant la trompe en avant,
ça, c’est fort ! Ça, c’est fort !
Une p’tite fourmi sous un gros tas de riz,
ça, c’est vraiment plus fort.

2.
Des éclairs un jour de tonnerre,
ça, c’est fort ! Ça, c’est fort !
Mais le doux murmure du vent
ça, c’est vraiment plus fort.

3.
Une colère à se rouler par terre,
ça, c’est fort ! Ça, c’est fort !
Des mots doux glissés dans le cou
ça, c’est vraiment plus fort.