Pleine de grâce, réjouis-toi !
L'Emmanuel a trouvé place
Dans ta demeure illuminée
Par toi, la gloire a rayonné
Pour le salut de notre race.

Arche d'Alliance, réjouis-toi !
Sur toi repose la présence
Du Dieu caché dans la nuée.
Par toi la route est éclairée
Dans le désert où l'homme avance.

Vierge fidèle, réjouis-toi !
Dans la ténèbre où Dieu t'appelle,
Tu fais briller si haut ta foi
Que tu reflètes sur nos croix
La paix du Christ et sa lumière.

Reine des anges, réjouis-toi !
Déjà l'Église en toi contemple
La création transfigurée :
Fais-nous la joie de partager
L'exultation de ta louange.