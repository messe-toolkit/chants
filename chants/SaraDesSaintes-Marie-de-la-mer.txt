Refrain :
Sara, Sara notre amie,
Pour te confier nos vies,
On est tous réunis !
Sara, Sara notre amie,
Pour te confier nos vies,
Aux Saintes-Marie !

1.
Ils viennent “de partout”, tous ces gens du voyage,
Leur pays c’est “là-bas”, “nulle part” ou “ailleurs”,
Mais ils gardent toujours, au fond de leur bagage,
Ce cantique à SARA, qu’ils reprennent en choeur:

2.
Écoutez leurs chansons, à ces gens du voyage,
Elles parlent d’espoirs et de grands “Rendez-vous”,
Des droits de l’être humain, d’amitié, de courage,
D’un monde en liberté qui appartient à tous.

3.
Mais il y a parfois, pour les gens du voyage,
Les sentiers escarpés de la peur, de la faim,
Ils rencontrent aussi la bourrasque et l’orage,
Mais SARA, notre amie, veille sur le chemin.

4.
Tu les aimes vraiment tous ces gens du voyage,
Il y a dans leurs yeux, la route et le soleil,
Guide toujours leurs pas, jusqu’à ton seul rivage
Qui s’appelle l’amour, qui s’appelle le ciel.