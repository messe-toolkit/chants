Antienne - stance
Jésus saisit la main de l'enfant et lui dit : Lève-toi !

R.
Confitemini Domino, quoniam bonus.
Confitemlni Domino, alleluia.

Versets

Ps 102, 1-4 ; 6-7.

1.
Bénis le Seigneur, ô mon âme,
Bénis son nom très saint, tout mon être !
Bénis le Seigneur, ô mon âme,
N'oublie aucun de ses bienfaits !

2.
Car il pardonne toutes tes offenses
Et te guérit de toute maladie ;
Il réclame ta vie à la tombe
Et te couronne d'amour et de tendresse ;

3.
Le Seigneur fait œuvre de justice,
Il défend le droit des opprimés.
Il révèle ses desseins à Moïse,
Aux enfants d'Israël ses hauts faits.