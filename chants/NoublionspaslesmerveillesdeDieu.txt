Stance
N’oublions pas les merveilles de Dieu,
que tous les âges à venir le connaissent.

R.
N’OUBLIONS PAS LES MERVEILLES DE DIEU,
QUE TOUS LES ÂGES À VENIR LE CONNAISSENT.

Psalmodie (Ps. 33, 2-9)

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Magnifiez avec moi le Seigneur,
exaltons tous ensemble son nom.
Je cherche le Seigneur, il me répond :
de toutes mes frayeurs, il me délivre.

3.
Qui regarde vers lui resplendira,
sans ombre ni trouble au visage.
Un pauvre crie ; le Seigneur entend :
il le sauve de toutes ses angoisses.

4.
L’ange du Seigneur campe à l’entour
pour libérer ceux qui le craignent.
Goûtez et voyez : le Seigneur est bon !
Heureux qui trouve en lui son refuge !