1.
Tu parais sur les bords du Jourdain,
Toi, Jésus, qui rejoins les pécheurs.
Avec eux tu te fais pèlerin,
Ils verront quel sera leur Sauveur !

2.
Tu descends dans les eaux du Jourdain,
Quel baptême oses-tu demander !
Jean-Baptiste pressent d’où tu viens,
Il t’accueille d’un coeur purifié.

3.
Baptisé dans les eaux du Jourdain
Tu remontes du fleuve pascal.
Jean-Baptiste est lui-même témoin :
Tu seras le vainqueur de tout mal.

4.
Grande joie sur les bords du Jourdain !
Dieu nous parle et les cieux sont ouverts.
Sur les eaux plane encor l’Esprit Saint,
Te voici Messager de sa paix.

5.
Une voix sur les bords du Jourdain
Te proclame « le Fils bien-aimé ».
Aujourd’hui cette voix nous parvient,
Tout l’amour de ton Père est donné.