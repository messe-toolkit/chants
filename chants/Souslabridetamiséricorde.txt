Sous l'abri de ta miséricorde,
Nous nous réfugions, ô Mère de Dieu ;
Ne méprise pas nos prières,
Mais de tout danger délivre-nous,
Ô toujours vierge, seule glorieuse et bénie.

1.
Vierge en qui nous mettons notre confiance
Conduis-nous, au long de ce jour.

2.
Reste avec nous, Marie,
Quand nous cherchons le visage de Dieu.

3.
Humble servante du Seigneur,
Découvre-nous ses chemins.