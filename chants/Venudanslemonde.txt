VENU DANS LE MONDE
POUR GUÉRIR ET SAUVER LES HOMMES
JÉSUS,
OUVRE NOS COEURS À TON ROYAUME.

1.
Vos pensées ne sont pas mes pensées
Dit le Seigneur
Mais Toi Jésus
Dieu avec nous l’Emmanuel
Donne-nous part à sa sagesse.

2.
Nous ne parvenons pas à prier
Comme il le faut
Mais Toi Jésus
Dieu avec nous l’Emmanuel
Mets sur nos lèvres la louange.

3.
Pardonner nous ne le pouvons pas
Pauvres humains
Mais Toi Jésus
Dieu avec nous l’Emmanuel
Rends nos coeurs purs de toute haine.

4.
Les petits et les humbles sont là
Chante Marie
Ô Toi Jésus
Dieu avec nous l’Emmanuel
Fais-nous chanter Magnificat.