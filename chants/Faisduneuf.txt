R.
FAIS DU NEUF AUJOURD’HUI
C’EST L’APPEL DE LA VIE
DANS TES MAINS A SOUFFLE
UN VENT DE LIBERTÉ
FAIS DU NEUF DANS TA VIE

1.
Tu voudrais aujourd’hui
Du nouveau dans tes yeux
Regarder le soleil
Allumer un grand feu
Laisse-toi approcher
Pour risquer l’amitié.

2.
Tu voudrais aujourd’hui
Des couleurs pour la vie
Et changer l’horizon
De nos jours noirs et gris
Viens lutter avec nous
Pour les droits et la vie.

3.
Tu voudrais aujourd’hui
Un nouvel avenir
Et tracer les chemins
D’une vie réussie
N’aie pas peur d’inventer
Tu es fait pour créer.

4.
Tu voudrais aujourd’hui
Du nouveau sous tes pas
Pour aller au devant
Des petits, des sans-voix
Et changer le destin
De tous ceux qui n’ont rien.