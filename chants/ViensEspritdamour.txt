Viens, Esprit d‘amour

Paroles et musique : Astrid BroedersNo. 23-12
Arrangement : J. Witt
Librement d’après le Veni, SancteSpiritus

R.
Viens, Esprit d‘amour,
Ô Esprit très saint.
Force des pauvres,
Souffle divin. (bis)

1.
Dans nos souffrances,
Ô consolateur,
Viens à notre aide,
Emplis-nous de paix.
Dans nos détresses,
De tous nos combats,
Force des humbles,
Viens nous délivrer !

2.
Dans les ténèbres,
Telle une nuée,
Douce lumière,
Illumine-nous.
Lave nos âmes,
Purifie nos vies,
Que nos blessures
Soient transfigurées.

3.
Joie de nos âmes,
Ô Esprit d’amour,
Notre allégresse,
Viens nous sanctifier.
Rends nos cœurs simples,
Unifie nos vies.
Prends tout notre être,
Nous sommes à toi.