BÉNIE SOIS- TU, MARIE, NOTRE LUMIÈRE,
MONTE VERS TOI LE CHANT DE NOS PRIÈRES.

1.
Tu nous apprends la confiance
Discrètement, fidèlement,
De toi surgit l’Espérance,
Magnificat pour ton enfant.

2.
En recevant la promesse,
Tu as dit oui malgré la peur.
Avec amour et tendresse,
Tu as dit oui du fond du coeur.

3.
Sur nos chemins en silence,
Tu accompagnes tous nos pas
Tu sais nos joies, nos souffrances,
Tu les accueilles et portes en toi.