R.
Pierres vivantes rassemblées par Jésus Christ,
Nous te chantons, Seigneur de gloire.
Pierres vivantes façonnées par ton Esprit,
Nous sommes ta maison.

1.
Heureuse Eglise avec Jésus pour Sanctuaire,
Le Saint des saints que tu choisis.
Son corps détruit fut relevé dans la lumière,
Nous rendons grâce pour sa Vie.

2.
Heureuse Eglise qui grandit à ta louange,
Au long des jours tu la construis.
L'amour unit nos coeurs de frères et les cimente,
Sur nous, Seigneur, ton Nom s'écrit.

3.
Heureuse Eglise où tes enfants refont leurs forces,
Chez nous ton pain est partagé.
Voici la table où tu prodigues ta Parole,
Nous rendons grâce à ta bonté.