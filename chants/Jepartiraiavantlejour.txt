1.
Je partirai avant le jour
Pour la sainte montagne,
Sur la Parole du Seigneur…
Je marcherai en sa présence
Au pas à pas de sa lumière.

2.
Je goûterai dans le désert
Le silence où résonne
Chaque Parole du Seigneur…
Je veux offrir à son attente
Le fruit secret de la confiance.

3.
Je puiserai en plein midi
Mon courage et ma force
Dans la Parole du Seigneur…
Je garderai l’écoute ardente
Jusqu’à la source des eaux vives.

4.
Quarante jours, quarante nuits
Avant l’aube de Pâques
Où la Parole s’accomplit !
Déjà s’élève dans l’Eglise
L’action de grâce pour l’Alliance…