R.
Mets ta joie dans le Seigneur,
Compte sur Lui et tu verras,
Il agira et t’accordera,
Plus que les désirs de ton coeur. (bis)

1.
Remets ta vie, dans les mains du Seigneur
Compte sur lui il agira.
Grâce à son amour, ta foi apparaîtra
Comme un soleil en plein jour.

2.
Reste en silence devant le Seigneur
Oui, attends le avec patience
Grâce à son amour, ton pas est assuré,
Et ton chemin lui plaît.

3.
Dieu connaît les jours de tous les hommes droits,
Il leur promet la vraie vie.
Grâce à son amour, ils observent sa voie,
Ils mettent leur espoir en lui.