Antienne
LE SEIGNEUR A FAIT MERVEILLE :
NOUS VOICI DANS LA JOIE.

1.
Quand le Seigneur ramena les captifs à Sion,
nous étions comme en rêve !
Alors notre bouche était pleine de rires,
nous poussions des cris de joie.

2.
Alors on disait parmi les nations :
“Quelles merveilles fait pour eux le Seigneur !”
Quelles merveilles le Seigneur fit pour nous :
nous étions en grande fête !

3.
Ramène, Seigneur, nos captifs,
comme les torrents au désert.
Qui sème dans les larmes
moissonne dans la joie.

4.
Il s’en va, il s’en va en pleurant,
il jette la semence ;
il s’en vient, il s’en vient dans la joie,
il rapporte les gerbes.