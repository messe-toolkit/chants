Miroir très pur du cœur de Dieu,
Reine de tous les saints,
Tu brilles comme un signe d'espérance.
Vierge Marie, obtiens-nous toute grâce et toute paix
En Jésus-Christ ton enfant. Amen. Alléluia !