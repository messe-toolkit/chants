R.
FAIS SILENCE DANS TA VIE,
FAIS SILENCE DANS TON COEUR :
TU RENCONTRERAS LE SEIGNEUR.
ET TU CONNAÎTRAS LE VRAI BONHEUR.

1.
Celui qui est, qui était et qui vient
Veut nous dire son amour de Père.

2.
Celui qui est, qui était et qui vient
A donné son Fils unique au monde.

3.
Celui qui est, qui était et qui vient
Nous envoie l’Esprit de Sainteté.