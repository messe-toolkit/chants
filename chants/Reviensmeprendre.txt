1.
Ciel de décembre,
Soleil de cendre,
Ma chanson s’est usée pour rien.
L’homme est à prendre,
La femme à vendre,
Même l’amour n’a goût de rien.

REVIENS ME PRENDRE, REVIENS ME PRENDRE,
REVIENS ME PRENDRE PAR LA MAIN !
REVIENS ME PRENDRE, REVIENS ME PRENDRE,
EMMÈNE-MOI LÀ D’OÙ JE VIENS !

2.
J’ai mal d’attendre,
Mal de me tendre,
Moi de m’arracher les deux mains.
Peux-tu entendre ?
Veux-tu descendre ?
Laisseras-tu crier en vain ?

3.
Croiront entendre,
Croiront comprendre,
Croiront surprendre, mes copains,
Croiront entendre,
Croiront surprendre
Un mal d’amour dans mon refrain.

4.
Mais comment m’y prendre
Pour me défendre
De vouloir gâcher leur festin ?
J’ai faim prendre,
J’ai soif de prendre
Un autre pain, un autre vin.

REVIENS ME PRENDRE, REVIENS ME PRENDRE,
REVIENS ME PRENDRE PAR LA MAIN !
REVIENS ME PRENDRE, REVIENS ME PRENDRE,
ÇA VAUDRAIT MIEUX, TU LE SAIS BIEN !