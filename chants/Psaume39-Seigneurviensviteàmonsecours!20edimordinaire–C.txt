SEIGNEUR, VIENS VITE À MON SECOURS !

1.
D’un grand espoir,
j’espérais le Seigneur :
il s’est penché vers moi
pour entendre mon cri.

2.
Il m’a tiré de l’horreur du gouffre,
de la vase et de la boue ;
il m’a fait reprendre pied sur le roc,
il a raffermi mes pas.

3.
Dans ma bouche il a mis un chant nouveau,
une louange à notre Dieu.
Beaucoup d’hommes verront, ils craindront,
ils auront foi dans le Seigneur.

4.
Je suis pauvre et malheureux,
mais le Seigneur pense à moi.
Tu es mon secours, mon libérateur :
mon Dieu, ne tarde pas !