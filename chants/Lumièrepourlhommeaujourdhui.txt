1.
Lumière pour l'homme aujourd'hui
Qui vient depuis que sur la terre
Il est un pauvre qui t'espère,
Atteins jusqu'à l'aveugle en moi :
Touche mes yeux afin qu'ils voient
De quel amour tu me poursuis.
Comment savoir d'où vient le jour
Si je ne reconnais ma nuit ?

2.
Parole de Dieu dans ma chair
Qui dis le monde et son histoire
Afin que l'homme puisse croire,
Suscite une réponse en moi :
Ouvre ma bouche à cette voix
Qui retentit dans le désert,
Comment savoir quel mot tu dis
Si je ne tiens mon cœur ouvert ?

3.
Semence éternelle en mon corps
Vivante en moi plus que moi-même
Depuis le temps de mon baptême,
Féconde mes terrains nouveaux :
Germe dans l'ombre de mes os
Car je ne suis que cendres encor,
Comment savoir quelle est ta vie
Si je n'accepte pas ma mort ?