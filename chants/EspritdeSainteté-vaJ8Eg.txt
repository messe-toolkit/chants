Esprit de Sainteté,
viens combler nos cœurs,
Tout au fond de nos vies
réveille ta puissance,
Esprit de Sainteté,
viens combler nos cœurs,

Chaque jour, fais de nous
des témoins du Seigneur.
Tu es la lumière qui vient
nous éclairer, le libérateur
qui vient nous délivrer,
Le consolateur, Esprit de vérité,
en toi l’espérance et la fidélité.