R.
Celui qui aime est né de Dieu,
Celui qui aime est fils de Dieu,
Car Dieu est amour et l'amour est Dieu (bis)

1.
Celui qui aime, jamais ne meurt
Il sait que Dieu habite en lui.
Celui qui espère, plus jamais n'a peur
Il sait que l'aurore détruira la nuit.

2.
Tout est possible à ceux qui croient,
Déjà ils voient le jour de Dieu.
Finies les douleurs, l'univers est là
Où Dieu et les hommes se tiendront la main.