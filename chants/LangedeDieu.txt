1.
L’Ange de Dieu vient annoncer
la bonne nouvelle aux bergers :
« Dans la nuit froide de Judée
vous trouverez un nouveau-né. »
Une lumière a resplendi,
l’Ange de Dieu nous réjouit.

2.
« L’Enfant que Dieu vous a donné,
dans une mangeoire est couché. »
Ils ont suivi le messager,
vers une étable ils ont marché.
Une lumière a resplendi,
le Fils de Dieu nous réjouit.

3.
L’Amour de Dieu a rayonné
sur leurs visages étonnés.
Reflet de grâces, illuminée,
Marie révèle sa bonté.
Une lumière a resplendi,
l’Amour de Dieu nous réjouit.