1.
De tout mon coeur et mon esprit
Comment t’aimer, Seigneur de vie,
À la mesure de toi-même ?
Pour prononcer un vrai « je t’aime »
Je laisserai Jésus ton Fils
Le dire en moi comme il l’a dit.

2.
De tout mon coeur et mon esprit
Je répondrai tel que je suis,
Fragile au vent de l’éphémère.
Ton coeur écoute ma prière
Et par le souffle de l’Esprit
Je peux vibrer à l’infini.

3.
De tout mon coeur et mon esprit
Je chercherai ce que tu dis
Par les Apôtres et les prophètes.
Dans l’Écriture ils nous révèlent
Ce que tu fais dès aujourd’hui
Pour sauver l’homme de sa nuit.

4.
De tout mon coeur et mon esprit
Je serai prêt à te servir
Dans le plus humble de mes frères.
De tous les points de notre terre
Leurs voix se lèvent et te supplient,
Fais résonner en moi leurs cris !

5.
De tout mon coeur et mon esprit
Je chanterai notre merci
Pour la venue de ta lumière.
Elle est à l’oeuvre en nos ténèbres,
Jésus nous ouvre un avenir,
Sa main nous aide à le bâtir.