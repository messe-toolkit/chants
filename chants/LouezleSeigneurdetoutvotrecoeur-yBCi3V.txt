R.
Louez le Seigneur de tout votre cœur,
Louez le Seigneur, peuples de la terre.

1.
Louez-le en son sanctuaire,
Louez-le au firmament de sa puissance,
Louez-le en ses œuvres de vaillance,
Louez-le en toute sa grandeur.

2.
Louez-le par l´éclat du cor,
Louez-le par la harpe et la cithare,
Louez-le par la danse et le tambour,
Louez-le par les cordes et les flûtes.

3.
Louez-le par les cymbales sonores,
Louez-le par les cymbales triomphantes,
Que tout ce qui respireLoue le Seigneur !