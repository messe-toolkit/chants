1.
Dieu, toi, mon Dieu,
Mon âme a soif de toi,
De toi, le Dieu de vie,
La Source de l’amour.
Christ est venu, Parole et voix du Père,
Lui seul nous dit ton Nom,
Dieu fort et Dieu très bon.

2.
Dieu, toi, mon Dieu,
Comment venir à toi,
A toi le Dieu très saint,
Que nul ne peut saisir ?
Christ est venu, visage de lumière ;
En lui nous te voyons,
Dieu fort et Dieu très bon.

3.
Dieu, toi, mon Dieu,
Fais-moi revivre en toi,
En toi le Dieu Sauveur,
Qui m’ouvres ta maison.
Christ est venu changer nos coeurs de pierre ;
En lui nous renaissons,
Dieu fort et Dieu très bon.