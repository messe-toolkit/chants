1.
Cela faisait si longtemps
Dans la chaleur et le vent
Qu’ils travaillaient en souffrant
Ces esclaves, pauvres gens.

IL EST TEMPS DE PARTIR,
DE MARCHER, DE SORTIR,
DE CHANTER, DE SOURIRE,
IL EST TEMPS DE VIVRE !

2.
Cela faisait si longtemps
Qu’on leur avait dit pourtant
Qu’un jour au soleil levant
Ils s’enfuiraient, en chantant.

3.
Cela faisait si longtemps
Que ces femmes et ces enfants
Croyaient qu’il viendrait le temps
De liberté, à tous vents !

4.
Cela faisait si longtemps
Dans la confiance en priant
Qu’ils attendaient le moment
De liberté, en marchant.