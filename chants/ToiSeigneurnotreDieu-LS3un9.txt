R.
Toi Seigneur notre Dieu,
Tu nous as rachetés.
Roi puissant et glorieux,
Nous voulons t´acclamer !

1.
Notre Dieu révèle son amour,
Exultez et soyez dans la joie,
En son Fils unique Jésus-Christ,
Il nous donne à jamais le salut.

2.
Jésus a livré sa vie pour nous,
Dans sa mort nous sommes baptisés,
Mais il est vraiment ressuscité,
Et en lui nous recevons la vie.

3.
Par l´Esprit le Verbe s´est fait chair
Sa lumière éclaire notre nuit,
Par sa mort et sa résurrection
Du péché nous sommes libérés.

4.
Bénissons notre Dieu trois fois Saint,
Son amour est pour chacun de nous
Et il nous appelle à vivre en lui,
À jamais louons notre Seigneur.