R.
RIEN QU'UN VERRE D'EAU FRAICHE
A L'UN DE CES PETITS
JE LE SAIS, CE N'EST RIEN.

1.
Tous les enfants utilisés
Comme des soldats de plomb
Pour marcher au pas des grands
Au prix du sang.

2.
Tous les enfants embrigadés
Poussés au feu du front
Fusillés tout comme les grands …
Tout ça pour du vent.

3.
Tous les enfants conditionnés
Entraînés par le fond
Dans de sales histoires de grands…
En paravent.

4.
Même si ce n'est que goutte d'eau
Dans l'océan du monde
C'est le plus beau des cadeaux
Qu'un geste d'amour.