QUELLE EST LA MESURE DE DIEU ? ELLE EST SANS MESURE !
ON NE PEUT PAS MESURER L’AMOUR DE DIEU : IL EST SANS MESURE !

Grâce à Jésus et à son Esprit,
notre vie aussi devient du « pain rompu » pour nos frères.
Et en vivant ainsi, nous découvrons la véritable joie !