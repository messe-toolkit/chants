R.
Mille jeunes, mille copains,
Mille frères, mille témoins,
Pierres vivantes de l'Esprit,
Vois ton église, Jésus-Christ,
Pierres vivantes de l'Esprit,
Vois ton église, Jésus-Christ !

1.
Vivre au milieu des hommes
Pour partager leurs joies ;
Croire que dans les pauvres
Brille un reflet de toi !

2.
Vivre au milieu du monde,
Planter la liberté ;
Mille cris de colombes
Feront un chant de paix !

3.
Vivre avec ceux qui doutent
Cherchant la vérité ;
Leur indiquer la route
Qui mène à la clarté !

4.
Vivre comme des frères
Autour du même feu ;
Détruire les barrières
Pour laisser passer Dieu !

5.
Vivre pour l'Évangile
Et l'annoncer partout ;
Prendre en nos mains fragiles
Ton corps livré pour nous !

6.
Vivre dans l'espérance
D'un avenir meilleur ;
Saisir chacun sa chance
Inventer le bonheur !

7.
Vivre pour le service
Et l'amour du prochain ;
Combattre l'injustice
Nourrir ceux qui ont faim !

8.
Vivre dans la prière
Pour nous unir à toi ;
Rayonner de lumière
Pour que le monde croie !