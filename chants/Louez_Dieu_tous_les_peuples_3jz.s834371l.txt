1.
Louez Dieu, tous les peuples
Louez Dieu, tous les peuples
Chantez sa grande gloire
Chantez sa grande gloire
Oui, notre Dieu nous aime
Son amour est fidèle

R.
Alléluia, Alléluia
Alléluia, Alléluia
Alléluia, Alléluia
Alléluia, Alléluia

2.
Chantez le Seigneur par des hymnes
Car il a fait des merveilles
Chantez le Seigneur, terre entière

3.
Que toute la terre l’acclame
Qu'elle proclame sa gloire
Que tout l’univers soit en fête