JUSTICE AU COEUR DU MONDE!
JUSTICE AU COEUR DE L’HOMME!
CENT MILLE VOIX POUR LE REDIRE,
CENT MILLE COEURS POUR LE CONSTRUIRE,
CENT MILLE MAINS POUR FAIRE AUTOUR DU MONDE
UNE CHAÎNE D’AMOUR.

1.
Chaque enfant qui vient à la vie
A le droit d’être aimé…
Et tout homme existant sur terre
A le droit d’être respecté
Et d’agir en liberté…

2.
Chaque enfant qui devient un homme
A le droit d’être instruit…
Et tout homme vivant sur terre
A le droit d’avoir un foyer
Et de pouvoir se loger…

3.
Chaque enfant resté seul au monde
A le droit d’être aidé…
Et tout homme souffrant sur terre
A le droit d’être soulagé
Défendu ou libéré !