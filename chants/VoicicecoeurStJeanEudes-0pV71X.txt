R.
Voici ce cœur
Qui a tant aimé les hommes.
Voici ce cœur qui s’est livré pour le monde.

1.
Jésus, Dieu de ma vie, que ton amour
Est admirable pour moi.
Tu m’aimes, tu me désires me cherches
Comme si j’étais nécessaire.
Tu me désires comme un véritable trésor.

2.
Tu cherches mon amitié
Pour en faire ton bonheur.
Seigneur, pourrais-tu en faire
Davantage pour moi ?
Ô bonté, je me perds dans l’infini de ton amour.

3.
Que répondre à ces voix qui m’invitent à t’aimer ? +
Je veux que toutes mes pensées, mes paroles et actions,
Tous les instants de ma vie
Et même tous mes péchés se transforment
En autant de voix qui disent éternellement :
« Mon Seigneur Jésus, je t’aime ».

4.
Seigneur Jésus, mon Sauveur, je te bénis
Et t’adore de toute mon âme.
Je choisis aujourd’hui ton divin cœur
Et celui de ta sainte Mère
Comme roi et refuge de mon cœur.

5.
Je t’offre mon amour.
Je désire que toutes mes pensées,
Mes paroles et mes actions
Soient en tout des actes de louange
Et d’adoration pour ton cœur.

6.
Reçois-moi, Seigneur très bon,
Au nombre de tes serviteurs,
Des enfants de ton cœur,
De celui de ta mère
Qui n’est qu’un avec le tien.

7.
Dispose de moi et de tout ce qui m’appartient
Selon ton bon plaisir.
De mon côté, ô Jésus, je prends la résolution de t’aimer
Et d’honorer ton sacré Cœur en imitant son humilité
Et son ardent amour pour le prochain.

8.
Père des miséricordes,
Imprime toi-même ces vertus
Dans le cœur de ton enfant,
Pour t’aimer et te glorifier
Pour l’éternité.