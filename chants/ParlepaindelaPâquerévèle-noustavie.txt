1.
Compagnon de nos routes,
Jésus ressuscité,
Nous n'avons rien compris
À tes obscurs sentiers ;
Dissipe en nous le doute,
Éclaire notre nuit.

R.
Par le pain de la Pâque,
Révèle-nous ta vie. (bis)

2.
Pour entrer dans la gloire,
Jésus ressuscité,
Nous faudra-t-il souffrir
La mort du crucifié ?
Nos coeurs sont lents à croire,
Explique-nous ta nuit.

3.
Quand le soir est tout proche,
Jésus ressuscité,
Ami, reste avec nous,
La lampe est allumée.
Nous attendrons l'aurore
Pour vivre de ton Jour.

4.
Tu prends place à la table,
Jésus ressuscité,
Partage-nous ton pain,
Bonheur des invités.
Nous reprendrons la marche,
Creusés d'une autre faim.

5.
Pour parler à nos frères,
Jésus ressuscité,
Nos coeurs sont tout brûlants,
Nos yeux pleins de clarté.
Par nous que ta lumière
Se donne à tout vivant !