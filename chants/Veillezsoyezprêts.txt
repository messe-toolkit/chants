1.
Murmure dans nos vies,
Signe annoncé aux hommes,
Lueur en notre nuit,
Dieu va sauver son peuple !

VOICI LE TEMPS DE SA PROMESSE
PRÉPAREZ DANS VOS COEURS UN CHEMIN
LA JOIE DU CIEL CHANTE SUR TERRE
VEILLEZ, SOYEZ PRÊTS : IL VIENT ! (bis)

2.
Appel qui retentit,
Présage du Baptiste,
Lever d’un jour nouveau,
Dieu réveille son peuple !

3.
Fête pour les nations,
Promesse qui rassemble,
Cadeau à partager,
Dieu invite son peuple !