R.
Seigneur, Dieu, reviens ! Fais-nous revenir près de Toi !
Éclaire Ton visage et nous serons sauvés !

1.
Berger d'Israël, écoute, toi qui conduis Joseph,
Ton troupeau : resplendis au-dessus des Kéroubim,
Devant Ephraïm, Benjamin, Manassé !
Réveille ta vaillance et viens nous sauver.

2.
Seigneur, Dieu de l'univers, vas-tu longtemps encore
Opposer ta colère aux prières de ton peuple,
Le nourrir du pain de ses larmes, l'abreuver de larmes sans mesure ?
Tu fais de nous la cible des voisins : nos ennemis ont vraiment de quoi rire !

3.
La vigne que tu as prise à l'Égypte,
Tu la replantes en chassant des nations.
Tu déblaies le sol devant elle,
Tu l'enracines pour qu'elle emplisse le pays.

4.
Son ombre couvrait les montagnes,
Et son feuillage les cèdres géants ;
Elle étendait ses sarments jusqu'à la mer,
Et ses rejets jusqu'au Fleuve.

5.
Pourquoi as-tu percé sa clôture ?
Tous les passants y gaspillent en chemin ;
Le sanglier des forêts la ravage
Et les bêtes des champs la broutent.

6.
Du haut des cieux, regarde et vois : visite cette vigne, protège-la,
Celle qu'a plantée ta main puissante,
Le rejeton qui te doit sa force.
La voici détruite incendiée ; que ton visage les menace, ils périront !

7.
Que ta main soutienne ton protégé,
Le fils de l'homme qui te doit sa force,
Jamais plus nous n'irons loin de toi :
Fais-nous vivre et invoquer ton nom !