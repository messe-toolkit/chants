R.
TERRE D'AVENIR,
PLANETE AUX CENT VISAGES,
TERRE D'AVENIR,
VAISSEAU POUR TOUS LES AGES,
DIEU TE PLACE DANS NOS MAINS
POUR SAUVER TES LENDEMAINS.

1.
Sauver le bleu des océans,
chasser le soufre des nuages,
sauver le vert des continents,
chasser les ombres qui nous gagnent,
sauver la braise des couchants
et l'arc-en-ciel après l'orage.
Pour que la terre voie le jour
brisons les forces de la nuit,
pour que la terre voie le jour
donnons sa chance à toute vie !

2.
Sauver les arbres et les forêts,
Chasser le feu des pluies acides,
sauver la neige des sommets,
chasser le bruit troublant les cimes,
sauver les fleuves massacrés
par les eaux rouges des usines.
Pour que la terre voie le jour
brisons les forces de la nuit,
pour que la terre voie le jour
donnons sa chance à toute vie.

3.
Sauver le cri des éléphants,
chasser l'argent qui les décime,
sauver les phoques au manteau blanc,
chasser les tueurs aux mains cupides,
sauver le vol des goélands
loin des marées qui assassinent.
Pour que la terre voie le jour,
brisons les forces de la nuit,
pour que la terre voie le jour
donnons sa chance à toute vie.

4.
Sauver le rire des enfants,
chasser le blues des cités grises,
sauver les jeunes et leurs vingt ans,
chasser la drogue et la déprime,
sauver tous ceux qui sont partants
sur des bateaux à la dérive.
Pour que la terre voie le jour
brisons les forces de la nuit,
pour que la terre voie le jour
donnons sa chance à toute vie.

5.
Sauver les fleurs d'humanité,
chasser le spectre de la guerre,
sauver l'espoir de moissonner,
chasser la faim et la misère,
sauver l'amour qui peut trouver
des sources riches de lumière.
Pour que la terre voie le jour
brisons les forces de la nuit,
pour que la terre voie le jour
donnons sa chance à toute vie.