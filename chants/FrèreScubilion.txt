Créole

FRÈRE SCUBILION,
DANS L’ÎLE BOURBON,
POU BANN’ PLI P’TI,
TOUJOURS, OU LA DI OUI.

1.
Quand bon Dié la di a ou
Pou allé dann l’îl’ Bourbon,
Ou te conné pa l’était où,
Mais ou la pa gaign’ di non.

2.
Quand bon Dié la di a ou :
« Casse la chaîn’ l’indifférenc’ »
Boug pou attend l’était pas ou
Pou rouv’ live la connaissanc’.

3.
Quand bon Dié la di a ou :
« Si ou mi compte mou noir »
Ou jet’ à ou dann’ tout’ combat,
Ka mê domoun’ té nouar.

4.
Pou noute Frère Scubilion,
Zor di bon Dié, grand merci,
La a mont’ a nou out’ l’Amour,
Pou bann pli faibl’s, bann pli p’tits !

Français

FRÈRE SCUBILION,
DANS L’ÎLE BOURBON,
POUR LES PLUS P’TITS,
TOUJOURS, TU AS DIT OUI.

1.
Quand Jésus t’a dit un jour
De partir à l’île Bourbon,
Tu ne savais pas où c’était,
Mais tu n’as pas su dire non.

2.
Quand Jésus t’a dit un jour :
« Brise les chaînes d’indifférence »
Tu n’étais pas homme à attendre
Pour livrer la connaissance.

3.
Quand Jésus t’a dit un jour :
« Sur toi je compte, mon gars »
Tu t’es jeté dans les combats,
Même si les hommes étaient noirs.

4.
Pour notre Frère Scubilion,
Aujourd’hui, Seigneur, merci,
Il nous a montré ton Amour,
Pour les plus faibles les plus p’tits !