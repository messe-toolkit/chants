1.
Un homme, en ce temps-là, en tout point respectable,
Invita à sa table Jésus passant par-là ! (bis)
Tout se passait très bien chez ce bon pharisien.
C’est alors que survint une femme de rien,
Dont la réputation mit le monde en émoi ! (bis)

JÉSUS, TOI QUI SAIS TOUT, TU CONNAIS MON PÉCHÉ!
MAIS EN TON GRAND AMOUR, FAIS NAÎTRE EN MOI LA PAIX !
JÉSUS, TOI QUI SAIS TOUT, TU CONNAIS MON PÉCHÉ!
MAIS EN TON GRAND AMOUR, FAIS-MOI SIGNE DE PAIX !

2.
La femme, se jetant en larmes à ses pieds,
Commence à les laver et à les essuyer ! (bis)
Alors les invités furent scandalisés :
« Comment se peut-il donc que notre jeune rabbi
Se laisse ainsi toucher par celle qui est péché ? » (bis)

3.
Jésus, les regardant et les dévisageant,
S’adressa à Simon, lui donnant la leçon ! (bis)
« J’étais ton invité ! M’as-tu bien respecté ?
Mais elle, de ses larmes, m’a arrosé les pieds,
Et de ses longs cheveux, me les a essuyés » (bis)

4.
« Tu sais, mon cher ami, l’amour n’a pas de prix !
Regarde cette femme : elle aime sans compter ! (bis)
Elle a beaucoup aimé malgré son grand péché !
Il lui est pardonné
Elle est réconciliée !
Elle a trouvé la Paix, car sa foi l’a sauvée ! » (bis)