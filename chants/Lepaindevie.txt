1.
Le pain de vie multiplié
Le vin des noces aux invités
Table ouverte à tous les hommes !
Corps et Sang livrés pour nous :
Chantons la Vie, chantons Jésus Christ !

2.
Christ en nos mains vient reposer
Trésor de vie à partager
Grâce donnée pour tous les hommes !
Communion d’amour pour nous :
Chantons la Vie, chantons Jésus Christ !

3.
Voici la paix à proclamer
Voici l’amour à inventer
Joie offerte à tous les hommes !
Signe de croix tracé sur nous :
Chantons la Vie, chantons Jésus Christ !

4.
Gloire promise, à contempler
Notre espérance, pour témoigner
Fête éternelle pour les hommes !
Esprit de Dieu donné pour nous :
Chantons la Vie, chantons Jésus Christ !