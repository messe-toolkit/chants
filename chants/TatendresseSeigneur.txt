ANTIENNE
TA TENDRESSE, SEIGNEUR,
VEUT LA JUSTICE POUR LE MONDE
TON AMOUR, Ô MON DIEU,
MET LE PARDON AU COEUR DE L’HOMME

1.
Pitié pour moi, Dieu, en ta bonté
En ta grande tendresse efface mon péché
Lave-moi de toute malice
Et de ma faute purifie-moi

2.
Car mon péché, moi, je le connais
Ma faute est devant moi sans relâche
Contre-toi, toi seul, j’ai péché
Ce qui est coupable à tes yeux je l’ai fait

3.
Pour que tu montres ta justice quand tu parles
Et que paraisse ta victoire quand tu juges
Vois : mauvais je suis né
Pécheur, ma mère m’a conçu

4.
Rends-moi le son de la joie et de la fête
Et qu’ils dansent les os que tu broyas
Détourne ta face de mes fautes
Et tout mon mal efface-le

5.
Mais tu aimes la vérité au fond de l’être
Dans le secret tu m’enseignes la sagesse
Ôte mes tâches avec l’hysope, je serai pur
Lave-moi, je serai blanc plus que neige

6.
Dieu, crée pour moi un coeur pur
Restaure en ma poitrine un esprit ferme
Ne me repousse pas loin de ta face
Ne m’enlève pas ton esprit de sainteté

7.
Rends-moi la joie de ton salut
Assure en moi un esprit magnanime
Aux pécheurs j’enseignerai tes voies
À toi se rendront les égarés