Elle est venue au coin d’un soir
Aux insomnies qui broient du noir
Elle a ouvert ses grands yeux tristes
La solitude, ça existe.

R.
MON DIEU, MON DIEU À QUI LA FAUTE?
C’EST LE DÉBUT D’UN LONG HIVER.
ET ÇA N’ARRIVE PAS QU’AUX AUTRES
LA SOLITUDE EST UN CANCER.

1.
Les autres ont dit : “Que je n’ai qu’à…
Qu’il faudrait que je réagisse
Que je devrais ceci, cela…”
D’abord il faudrait que je puisse
La solitude est sur mon dos
Et partout je traîne l’absence
La solitude est dans ma peau
Mais mon Dieu ! Où est ta présence ?
La solitude, ça existe.

2.
Autrefois j’avais peur des gens
Mais maintenant c’est moi qu’ils craignent
Je leur fais peur à mes dépens
Bien sûr, ils sont là qui me plaignent
Mais je crois que je les fais fuir
Je ne suis plus convié aux fêtes
Ils sont lassés de mes soupirs
Et me laissent tout seul dans ma tête.
La solitude, ça existe.

3.
Ma solitude a pris la mer
Pour aller au bout de moi-même
J’ai traversé le grand désert
Qui a simplifié mes problèmes
J’ai découvert la paix du coeur
J’apprends à parler le silence
Le solitaire est un vainqueur
Et ma solitude est présence.
Et ma solitude est présence.