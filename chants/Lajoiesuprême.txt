LA JOIE SUPRÊME
C’EST DE L’AIMER QUAND MÊME
CETTE FOULE EN COLÈRE
QUI NOUS JETA LA PIERRE
LA JOIE PARFAITE
C’EST UN INSTANT DE FÊTE
AU FOND D’UNE PRISON
APRÈS LA TRAHISON

1.
Le pauvre qui a froid, il lui donne un habit
Il embrasse un lépreux et son âme guérit
Son père l’a renié, il comprend les maudits
Humilié ou blessé, il pardonne et sourit

2.
Il caresse le loup qui se change en agneau
Il supplie les chasseurs d’écouter les oiseaux
Quand les fidèles tuent, il fait honte aux bourreaux
Et chante l’avenir aux portes d’un tombeau

3.
Quand sa demeure brûle, une larme suffit
Pour qu’un ciel étoilé devienne un paradis
Un matin, il s’envole, à la grâce de Dieu
Le corps ensanglanté, le voilà bienheureux