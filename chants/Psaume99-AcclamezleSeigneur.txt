ANTIENNE 1
Allez vers le Seigneur parmi les chants d’allégresse !
ANTIENNE 2
Loire à Toi, Seigneur !
ANTIENNE 3
Alléluia !
ANTIENNE 5
Le Seigneur paraîtra dans la gloire, et toute chair verra le salut de Dieu !

1.
Acclamez le Seigneur, terre entière, servez le Seigneur dans l’allégresse,
Allez à lui avec des chants de joie.

2.
Sachez que lui, le Seigneur, est Dieu, il nous a faits et nous sommes à lui,
Son peuple et le troupeau de son bercail.

3.
Allez à ses portiques en rendant grâces, entrez dans ses parvis avec des hymnes,
Rendez-lui grâce et bénissez son nom.

4.
Oui le Seigneur est bon, oui éternel est son amour,
Sa fidélité demeure d’âge en âge.

5.
Rendons gloire au Père tout puissant, à son Fils, Jésus-Christ, le Seigneur,
À l’Esprit qui habite en nos cœurs.