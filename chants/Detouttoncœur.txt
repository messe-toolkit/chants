DE TOUT TON COEUR, OUVRE TES MAINS
DE TOUT TON COEUR, PARLE BIEN FORT
DE TOUT TON COEUR, MARCHE PLUS LOIN,
ET FAIS LE BIEN, DE TOUT TON COEUR !

1.
Tu verras sur le chemin, le blessé qui n’a plus rien,
Tu verras comme il a faim, et te dire : « c’est un vaurien ! »
Tu verras comme un copain, qu’avec un tout petit rien,
Tu verras sur le chemin, un ami qui le vaut bien !

2.
Tu verras dans le désert, l’assoiffé qui n’en peut plus,
Tu verras dans le grand air, le regard des gens déçus,
Tu verras comme un compère, l’homme qui a enfin bu,
Tu verras dans le désert, un frère à perte de vue !

3.
Tu verras tout près de toi, l’immigré qui cherche un toit,
Tu verras à chaque fois, qu’il est bon d’entendre une voix,
Tu verras grandir ta joie, même si tu t’sens maladroit,
Tu verras tout près de toi, un humain que tu reçois !