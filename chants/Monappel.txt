1.
Entends-tu cet appel
Entends-tu cette voix
Qui sonne et qui résonne
Au fond de toi ?

Entends-tu ô mon âme
Le son de cette voix
L’écho d’un feu d’amour
Au fond de toi ? (bis)

QUEL QUE SOIT LE COMBAT
LES MISÈRES ET LES CROIX
MON APPEL EST BIEN LÀ
C’EST MARCHER DANS TES VOIES

2.
Ô mon âme es-tu prête
À recevoir la foi
Qu’il pose et qu’il dépose
Au fond de toi ?

Car le Seigneur t’appelle
Car le Seigneur t’envoie
Celui qui a donné
Sa vie pour toi (bis)

Pont
Je te suivrai, Seigneur
Ton nom gravé au coeur
Je te suivrai, Seigneur
Alléluia, alléluia, alléluia (x4)