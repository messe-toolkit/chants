1.
Tu es la vraie lumière
jaillie dans notre nuit.
Printemps de notre terre,
c’est toi qui nous conduis.
\n
Tu es le beau visage,
clarté dans le matin.
D’un radieux message,
nous sommes les témoins.

2.
Victoire qui délivre
des marques du péché,
Ta Pâques nous fait vivre
en vrais ressuscités.
\n
Parole vivifiante,
tu viens pour notre faim.
Dans notre longue attente,
ton corps est notre pain.

3.
Tu fais de nous des frères
rassemblés par ta croix.
Enfants d’un même Père,
nous partageons ta joie.
\n
Merveille de ta grâce,
tu viens nous libérer.
Qu’en ton amour se fasse,
Seigneur, notre unité.
