R.
PRIEZ LE MAITRE DE LA MOISSON
D'ENVOYER DES OUVRIERS
POUR SA MOISSON !
PRIEZ LE MAITRE DE LA MOISSON
D'ENVOYER DES OUVRIERS
POUR SA MOISSON !

1.
Dans ce monde déchiré
Par la guerre et la violence
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde qui a faim
Et qui connait la misère
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.

2.
Dans ce monde sacrifié
A l'argent et l'égoïsme
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde où règne encore
Le profit et l'injustice
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.

3.
Dans ce monde à libérer
Des prisons, des esclavages
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde à transformer
En demeure fraternelle
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.

4.
Dans ce monde qui attend
Des messagers d'Evangile
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde qui voudrait
Des prophètes d'Espérance
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.

5.
Dans ce monde où Dieu voudrait
Des serviteurs au coeur libre
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde où Dieu voudrait
Des témoins de sa tendresse
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.

6.
Dans ce monde où seraient Rois
Tous les petits de la terre
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde où le plus grand
Est serviteur de ses frères
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.

7.
Dans ce monde qui renaît
Si l'Amour est sa richesse
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.
Dans ce monde où se lève
Un nouveau matin de Pâques
LA MOISSON EST ABONDANTE,
LES OUVRIERS PEU NOMBREUX.