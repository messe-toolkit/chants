R.
Marie de tous les horizons,
Le monde entier fredonne
Des mots d’amour,
Autour de ton prénom.
Marie de tous les horizons,
Le monde carillonne
Ton chant d’amour sur tous les tons.

1.
On t’a donné tell’ment de noms,
Marie, pleine de grâce,
Dans nos prières et nos chansons,
Marie du temps qui passe…
Depuis la Galilée
Jusqu’à l’Immaculée,
Tes noms de Reine et de Douleurs,
Marie, je les connais par cœur !

2.
On t’a donné des noms jolis,
Marie de la Sagesse,
Je t’en invente moi aussi,
Marie de ma tendresse.
Avec des mots sans voix,
Qui brûlent ou qui ont froid,
Mais qui résonnent à l’intérieur,
Marie, tu les connais par cœur !

3.
On t’a donné des noms tout bleus,
Marie de l’Espérance,
Les miens sont un peu nuageux,
Marie de mes silences.
Quand ma vie se découd,
Quand je reviens de tout,
Tu prends le nom de la douceur,
Marie, tu me connais par cœur !