R.
Gloire à Dieu qui donne vie !
Nous sommes dans sa main.
Gloire à Dieu par Jésus Christ,
Seigneur des lendemains !

1.
Regardez les lys des champs,
Leur parure est sans égale ;
Salomon couvert de gloire
N’eut jamais pareil manteau !
Plus que les fleurs et les oiseaux,
Dieu votre Père vous connaît.

2.
Regardez l’oiseau du ciel,
Il ne sèm(e) ni ne moissonne.
Les épis que Dieu lui donne
Sont chantés comme un cadeau.
Plus que les fleurs et les oiseaux,
Dieu votre Père vous connaît.

3.
Au royaume de l’amour,
Cherchez donc ce qui fait vivre.
Grandissez dans la justice,
Dieu regarde ce trésor !
Plus que les fleurs et les oiseaux,
Dieu votre Père vous connaît.