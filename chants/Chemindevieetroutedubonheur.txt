R.
CHEMIN DE VIE ET ROUTE DU BONHEUR,
JÉSUS CHRIST, BONNE NOUVELLE.
VRAI PAIN DE VIE DE L’HUMBLE SERVITEUR,
JÉSUS CHRIST TU NOUS APPELLES.

1.
Si tu me suis, si tu m’ouvres ta vie,
je viendrai chez toi y faire ma demeure.
Je te conduirai au pas de mes amis :
Avance joyeux, confiance, n’aie pas peur.

2.
Si tu as faim du pain de mon amour,
Je viendrai chez toi y faire ma demeure.
Je te comblerai de ma vie chaque jour :
Nourris-toi de moi, j’habiterai ton coeur.

3.
Si tu entends mon appel à aimer,
Je viendrai chez toi y faire ma demeure.
Lève-toi confiant, apprends à te donner :
Car en se donnant on devient serviteur.

4.
Si tu rejoins mes frères assemblés,
Je serai chez vous, vous serez ma demeure.
Je vous enverrai avec mission d’aimer :
Le monde verra le secret du bonheur.