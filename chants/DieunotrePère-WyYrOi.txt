R.
Dieu notre Père,
Christ et Seigneur,
Ô Esprit Saint,
Ton amour nous renouvelle.
Dieu qui se donne,
Vie en nos âmes,
Nous te louons,
Gloire à toi dans tous les siècles.

1.
Béni sois-tu, Seigneur,
Dieu de tendresse, Dieu de pitié,
Dieu riche en grâce et fidélité,
Dont la colère ne dure pas.
Toi qui voulus créer
À ton image, l´homme comblé,
Malgré sa chute, tu l´as aimé,
De la poussière l´as relevé.

2.
Peuple choisi de Dieu,
Ouvre ton cœur, entends aujourd´hui
Dieu qui t´appelle, ne tarde pas,
Viens vers l´eau vive étancher ta soif.
Marche jusqu´au désert,
Viens, n´aie pas peur mais suis ton Seigneur,
Lui dont la grâce ne peut manquer,
Il veillera sur tous tes chemins.

3.
Peuple du Dieu vivant,
Sèche tes larmes, oublie ton péché,
Car ta misère ton Dieu l´a vue,
Dans son amour il t´a pardonné.
Peuple garde la foi,
Dans le Seigneur ta force et ta paix
Lève les yeux car viennent des jours
Où tu verras le désert fleurir.