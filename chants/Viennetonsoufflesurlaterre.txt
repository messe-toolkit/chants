VIENNE TON SOUFFLE SUR LA TERRE,
QUE TOUTE VIE SOIT RÉVEILLÉE !
VIENNE TON FEU ET SA LUMIÈRE,
NOUS GRANDIRONS DANS TA CLARTÉ !

1.
Sur tes disciples rassemblés,
Envoie, Seigneur, l’Esprit du Père !
Lui seul pourra nous révéler
Les mots jaillis de ta prière.
Par sa venue tu es présent,
Nous sommes fils du Dieu vivant.

2.
L’Esprit murmure au fond des coeurs
le chant nouveau des délivrances.
Voici l’Eau vive du Seigneur,
la joie qui vient des renaissances.
Dans ton Église tu fais grandir
l’amour qui peut nous rajeunir.

3.
Toi qui façonnes l’univers,
tu crées la vie comme une fête.
Par ton Esprit sur toute chair
que tout ton peuple soit prophète !
Finie la peur dans nos maisons,
Ressuscité, nous t’annonçons.