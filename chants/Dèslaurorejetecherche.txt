DÈS L’AURORE JE TE CHERCHE,
TOI, LE DIEU QUI NOUS ATTENDS;
DEPUIS L’AUBE TU ME CHERCHES
POUR DONNER L’ESPRIT VIVANT.

1.
Cerf altéré cherche l’eau vive,
Terre sans eau crie vers la source.
Que tout mon être soit désir :
Désir de Toi, le Dieu vivant !

2.
Il n’est de jour sans pain de larmes,
Il n’est de nuit sans cri d’angoisse ;
La nuit, le jour, on vient me dire :
Où est ton Dieu, le Dieu vivant ?

3.
Devrais-je encor trembler de crainte
Quand je te dis ma sauvegarde ?
M’oublierais-tu si je t’oublie,
Source d’amour, Ô Dieu vivant ?

4.
Passe vers moi, Dieu de l’abîme,
Passe vers moi quand je t’appelle !
Viens sauver l’homme que je suis,
Fais-moi revivre, Dieu vivant !

5.
Reviendra-t-il ce temps de fête
Où j’exultais dans ta louange ?
Tout mon espoir jusqu’au matin :
Paraître face au Dieu vivant.