À LA FRACTION DU PAIN
L’AMOUR A PRIS VISAGE
L’AMOUR RÉPAND LA PAIX.
AU CREUX DE NOS DEUX MAINS
L’AMOUR SE FAIT PARTAGE
L’AMOUR NE MEURT JAMAIS. (BIS)

1.
Amis d’un peu partout
Venez rassemblons-nous
Le temps est à la fête. (bis)
Seigneur, par ton alliance
Unis nos espérances
Le temps est aux prophètes. (bis)

2.
Les blés sont moissonnés
La vigne vendangée
Le temps est à la fête. (bis)
Seigneur, tu viens t’asseoir
Au coeur de notre histoire
Le temps est aux prophètes. (bis)

3.
La table d’aujourd’hui
Se dresse à l’infini
Le temps est à la fête. (bis)
Seigneur au quotidien
Ton pain nourrit nos faims
Le temps est aux prophètes. (bis)

4.
Par le plus grand mystère
Le ciel marie la terre
Le temps est à la fête. (bis)
Seigneur, de nos destins
Sois l’éternel témoin
Le temps est aux prophètes. (bis)

5.
Chantez les amoureux
Dansez autour du feu
Le temps est à la fête. (bis)
Seigneur que ton Esprit
Enflamme notre vie
Le temps est aux prophètes. (bis)