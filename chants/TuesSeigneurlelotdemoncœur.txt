R.
Tu es Seigneur, le lot de mon cœur,
Tu es mon héritage ;
En toi, Seigneur, j'ai mis mon bonheur,
Toi mon seul partage !

1.
Je pense à toi, le jour, la nuit, Ô Seigneur !
Et c'est ta main qui me conduit, Ô Seigneur !

2.
Préserve-moi de tout faux pas…
Mon cœur ne veut servir que Toi…

3.
Sur Toi les yeux toujours fixés…
Je chante et marche en sûreté…

4.
Que peut la mort pour m'engloutir…
Car du tombeau je dois surgir…

5.
Devant ta Face il n'est que joie…
Joie débordante auprès de Toi…