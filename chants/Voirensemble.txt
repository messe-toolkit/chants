VOIR ENSEMBLE
DANS LA NUIT DE NOS YEUX VOILÉS,
VOIR ENSEMBLE
DES CHEMINS DE LIBERTÉ.

1.
Sur nos sentiers d’aveugles-nés
Tant de regards se sont posés
Cherchant à dire un peu de joie !
Quand le Sauveur vient à passer
Avec ses mots libérateurs,
Alors nos coeurs sont éclairés.

2.
Nous entonnons un chant de vie
Car nos visages sont remplis
D’une clarté venue d’ailleurs.
Le «Voir ensemble» est comme un jour
Qui nous entraîne à son bonheur,
L’écho d’un Dieu qui est amour.

3.
Comme l’aveugle à Jéricho
Laissons tomber le vieux manteau
Qui nous empêche de bondir !
Jésus nous parle d’avancer
En le suivant sans perdre coeur
Vers l’inconnue de ses montées.

4.
Que les aveugles et malvoyants
Soient écoutés par les voyants
Dans le grand peuple des marcheurs !
Nous avons tous à rénover
Nos yeux fermés par mille peurs,
Pour voir un ciel d’humanité.