R.
Seigneur Tu nous appelles, Seigneur Tu nous appelles,
à vivre chaque jour, l’annonce de ton amour !
Seigneur Tu nous appelles, Seigneur Tu nous appelles,
à vivre chaque jour, la communion fraternelle !

1.
Forts de ton Évangile, de ta confiance en nous,
nous osons  parler de toi jusqu’aux périphéries.
Leur dire que tu es là chaque jour près de tous,
dans les peines et les joies qui… jalonnent la vie.

2.
Notre cœur grand ouvert à l’action de l’Esprit,
qu’au sein de nos familles unies par la prière,
nos enfants et nos jeunes apprennent ton amour,
qu’ils sont nés de toi et que Tu… leur tends les bras.

3.
Accueillir l’Évangile, c’est accueillir la vie,
témoigner que Tu es Jésus ressuscité.
Nous faire tout à tous, vivre la charité,
pour marcher vers Dieu tous ensemble…dans la fraternité .

 

 