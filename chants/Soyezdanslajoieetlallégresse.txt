« SOYEZ DANS LA JOIE ET L’ALLÉGRESSE » (MT 5, 12),
DIT JÉSUS À CEUX QUI SONT PERSÉCUTÉS À CAUSE DE LUI.
« SOYEZ DANS LA JOIE ET L’ALLÉGRESSE » (MT 5, 12),
DIT JÉSUS À CEUX QUI SONT HUMILIÉS À CAUSE DE LUI.

1.
Le Seigneur demande tout ;
et ce qu’il offre est la vraie vie.

2.
Le Seigneur a élu chacun d’entre nous pour
que nous soyons « saints et immaculés en sa
présence, dans l’amour » (Ep I, 4).