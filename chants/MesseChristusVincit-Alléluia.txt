ANTIENNE
Alléluia ! Alléluia ! Alléluia ! Alléluia !

1.
La Bonne Nouvelle est annoncée aux pauvres.
Alléluia ! Alléluia !

2.
Tu as les Paroles de la Vie Éternelle.
Alléluia ! Alléluia !

3.
Tu es le Chemin, la Vérité, la Vie.
Alléluia ! Alléluia !
Ou : Gloire à Toi, Seigneur Jésus-Christ !