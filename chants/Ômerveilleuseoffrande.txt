1.
Ô merveilleuse offrande.
Où la loi s’accomplit !
Aujourd’hui Joseph et Marie
Offrent à Dieu
Leur Premier-né, son Fils unique.

2.
Il a tenu parole
le Dieu saint d’Israël !
Aujourd’hui Anne et Syméon
Voient de leurs yeux
L’Enfant promis à tous les peuples.

3.
Déjà Marie découvre
Que la croix est dressée :
Aujourd’hui lui est annoncée
L’heure où le Fils
Remet sa vie aux mains du Père.

4.
À toi, Seigneur, la gloire
Pour ton Fils bien-aimé !
Tu lui donnes de nous sauver
En l’accueillant
Dans l’aujourd’hui de son offrande.