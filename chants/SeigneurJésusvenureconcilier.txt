Seigneur Jésus-Christ venu réconcilier tous les hommes
Avec ton Père et notre Père. Prends pitié de nous.

Toi le serviteur fidèle, devenu péché en ce monde
Pour que nous soyons justifiés. Prends pitié de nous.

Toi, qui vis auprès du Père et nous attires vers lui
Dans l’unité de l’Esprit-Saint. Prends pitié de nous.