1.
Pour que l’homme soit un fils à son image
Dieu l’a travaillé au souffle de l’esprit
Lorsque nous n’avions ni forme ni visage,
Son amour nous voyait libres comme lui.

2.
Nous tenions de Dieu la grâce de la vie,
Nous l’avons tenue captive du péché :
Haine et mort se sont liguées pour l’injustice
Et la loi de tout amour fut délaissée.

3.
Quand ce fut le jour et l’heure favorable,
Dieu nous a donné Jésus le bien-aimé
L’arbre de la Croix indique le passage
Vers un monde où toute chose est consacrée.

4.
Qui prendra la route vers ces grands espaces ?
Qui prendra Jésus pour Maître et pour Ami
L’humble serviteur a la plus belle place
Servir Dieu rend l’homme libre comme lui.