R.
Laissons la présence du Dieu amour,
Devenir le soleil irradiant notre vie.

1.
Si quelqu’un m’aime, il gardera ma parole, mon Père l’aimera,
Nous ferons en lui notre demeure.

2.
Que ma vie soit une oraison continuelle, un long acte d’amour.
Que rien, que rien ne puisse me distraire de Toi.

3.
Je t’offre la cellule de mon cœur.
Oh viens t’y reposer, je t’aime, je t’aime tant.