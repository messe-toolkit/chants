R.
OÙ TE CACHES-TU, MON DIEU,
OÙ TE CACHES-TU LE MIEUX ?

1.
Est-ce dans l'église, ta maison,
Dans les livres où l'on écrit ton nom ?

2.
Est-ce dans le bleu de l'océan,
Parmi les étoiles au firmament ?

3.
Est-ce dans les fleurs et dans le vent,
Dans les arbres en fête du printemps ?

4.
Est-ce dans nos coeurs et dans nos yeux,
Nos éclats de rire ou dans nos jeux ?

CODA
OÙ TE CACHES-TU, MON DIEU,
OÙ TE CACHES-TU LE MIEUX ?
TU NOUS VOIS, TU NOUS ENTENDS,
TU NOUS RESPIRES…
TU ES LÀ DEPUIS LONGTEMPS
DANS NOS SOURIRES !