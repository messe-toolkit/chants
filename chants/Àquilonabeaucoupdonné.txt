« À qui l’on a beaucoup donné,
On demandera beaucoup ;
À qui l’on a beaucoup confié,
On réclamera davantage. »