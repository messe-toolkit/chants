Voici la troisième heure.
Voici l’heure où Jésus comparaît devant Pilate.
Voici l’heure où est mis en jugement
Celui qui viendra juger les vivants.
Voici la troisième heure.
Voici l’heure où est frappé notre Sauveur.
De ce roi, nul ne peut contempler la beauté.
Vêtu de pourpre et couronné d’épines, il porte nos péchés.
Voici la troisième heure.
Adorons Jésus-Christ, le Seigneur.