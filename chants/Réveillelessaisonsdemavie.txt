RÉVEILLE LES SAISONS DE MA VIE ;
MON COEUR ATTEND QUE TU VIENNES.
RÉVEILLE-MOI, JÉSUS, MON AMI;
IL Y A LONGTEMPS QUE JE T’AIME !

1.
Je suis veilleur et je te guette
quand ma lampe est allumée.
C’est dans la joie que je m’apprête
au retour du nouveau-né !

2.
Depuis toujours, tu me fais signe
de te suivre pas à pas.
Mais moi je ne me sens pas digne
de marcher auprès de toi.