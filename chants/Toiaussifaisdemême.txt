R.
Regarde Jésus,
Il a brisé les murs,
Toi aussi, fais de même,
Regarde Jésus,
Il a donné sa vie,
Toi aussi, fais de même.

1.
Partage ton pain (bis)
Avec l'homme qui a faim,
Ne te dérobe pas à sa misère,
Partage ton pain et ta prière,
Et ta prière.

2.
Partage ton toit (bis)
Avec l'homme qui a froid,
Ne te dérobe pas, il est ton frère,
Partage ton toit et ta lumière,
Et ta lumière.

3.
Partage tes cris (bis)
Avec l'homme qui supplie,
Ne te dérobe pas à ses souffrances,
Partage tes cris et tes silences,
Et tes silences.

4.
Partage ton cœur (bis)
Avec l'homme qui se meurt,
Ne te dérobe pas à sa détresse,
Partage ton cœur et ta tendresse,
Et ta tendresse.