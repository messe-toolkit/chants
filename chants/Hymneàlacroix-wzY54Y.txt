R.
Nous te louons, nous t´adorons !
De ta croix a jailli la joie dans le monde !

Soliste :
1. Ô Christ, notre frère, venu nous sauver,
tous : De ta croix a jailli la joie dans le monde !
Sagesse éternelle et Verbe de Dieu,
tous : De ta croix a jailli la joie dans le monde !

2. Étoile du matin, qui annonce le jour, …
tous : De ta croix a jailli la joie dans le monde !
Flambeau de la Nouvelle Jérusalem, …
tous : De ta croix a jailli la joie dans le monde !

3.
Splendeur de la Lumière éternelle du Père, …
tous : De ta croix a jailli la joie dans le monde !
Jésus, tu éclaires nos ténèbres et nos nuits, …
tous : De ta croix a jailli la joie dans le monde !

4.
Agneau immolé pour tous nos péchés,
tous : De ta croix a jailli la joie dans le monde !
Tu es notre paix par le sang de ta croix,
tous : De ta croix a jailli la joie dans le monde !

5. Ô Christ notre Roi, doux et humble de cœur, …
tous : De ta croix a jailli la joie dans le monde !
Berger qui nous mène au Royaume des Cieux, …
tous : De ta croix a jailli la joie dans le monde !

6.
Fontaine jaillissante de grâce et de vie, …
tous : De ta croix a jailli la joie dans le monde !
Ô Source d´eau vive pour notre soif, …
tous : De ta croix a jailli la joie dans le monde !

7.
Vraie manne qui nous donne la vie éternelle, …
tous : De ta croix a jailli la joie dans le monde !
Tu es le Pain vivant, le vrai Pain du ciel, …
tous : De ta croix a jailli la joie dans le monde !

Acclamations :
1.
Voici la croix du Christ, arbre de la vie !
Voici la croix du Christ, signe de la paix !

2.
Gloire à toi Seigneur, Roi de l´Univers !
Gloire à toi Seigneur, prince de la Paix !

3.
Que la paix du Christ règne dans nos cœurs !
Que la paix du Christ transforme notre monde !

Soliste :
1.
Toi qui es venu dans le monde,
tous : Prends pitié de nous !
Toi qui fus pendu à la croix,
tous : Prends pitié de nous !
Toi qui, pour nous, as accepté la mort en croix,
tous : Jésus-Christ, source de la vie !

2.
Toi qui reposas au tombeau,
tous : Prends pitié de nous !
Toi qui ressuscitas de la mort,
tous : Prends pitié de nous !
Toi qui montas au ciel, toi qui envoyas l´Esprit,
tous : Jésus-Christ, source de la vie !