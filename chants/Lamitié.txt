R.
L’amitié, c’est comme une flamme
Qui éclaire et réchauffe ta vie ;
La maison où tu la trouves
C’est le cœur de ton ami
La maison où tu la trouves
C’est le cœur de ton ami.

1.
Quand tu te sens tout seul sur cette terre
Petit Prince au désert, aviateur naufragé
Tout près de toi, un être solitaire
Te cherche pour se faire apprivoiser.

2.
Quand tu es dans la nuit, il voit l’aurore
C’est grâce à son regard que tu peux t’en sortir.
Quand, pour toi, c’est l’hiver, il voit éclore
Les fleurs que tu pourras bientôt cueillir.

3.
Il est tout différent, d’une autre race,
Il vient d’un monde à lui, cet étrange étranger.
Mais saisis donc cette chance qui passe
D’aller plus loin que ton jardin fermé.

4.
Amis, vous êtes l’arbre et la fontaine
Qui peuvent résister, même au cœur du désert
Lorsque vous partagez, aux jours de peine
Ce que chacun possède de plus cher.