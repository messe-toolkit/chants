R.
REVELE-NOUS SEIGNEUR LES VOIES DE L'UNITE
RASSEMBLE-NOUS SEIGNEUR DANS TA MAISON DE PAIX !

1.
Dieu d'Israël et des Païens,
Nous sommes tous des hommes loin
de ton alliance.
Quand Jésus vient nous rapprocher
Qui d'entre nous veut oublier
ses discordances ?

2.
Malgré la chair du Crucifié,
Ils ne sont pas encor tombés
nos murs de haine.
Toi le Seigneur d'immense paix,
Inspire-nous des gestes vrais
envers nos frères.

3.
Tes longs appels ont rencontré
Nos sables morts et nos rochers
de certitudes.
Viens nous apprendre à découvrir
La communion qui est ta vie
en plénitude.

4.
Ceux qui te cherchent sont nombreux,
Trouveront-ils un jour ton Feu
dans nos Eglises ?
Toi le Pasteur d'un seul troupeau,
Arrache-nous aux vieux enclos
qui nous divisent.

5.
Combien te nomment sans te voir
Et combien vivent de l'espoir
de tes disciples !
La vérité qui nous unit
En un seul corps, un seul esprit,
c'est l'Evangile.