R.
UN AVENIR, UNE ESPÉRANCE,
TOI, NOTRE DIEU, TU LES PROMETS.
UN AVENIR, UNE ESPÉRANCE,
TU VAS, SEIGNEUR, NOUS LES DONNER.

1.
Forgeons des socs et des faucilles
Avec les lances et les épées !
Forgeons la paix dans la justice,
Brisons le mal et ses armées !

2.
Nations, venez vous mettre en marche,
Dieu vous dira tous ses chemins.
Prenez la voie de sa montagne,
Ouvrez les routes pour demain.

3.
Venez, familles de la terre,
Quittez l’impasse de la nuit.
Marchez sans peur dans la lumière,
Le Fils de l’homme vous conduit.

4.
De tous pays, de toutes races,
De l’Orient, de l’Occident,
Dieu nous appelle à prendre place,
Formons la table des vivants !

5.
Avec Jésus dans nos demeures
Le monde ancien s’en est allé.
Par son Esprit toujours à l’oeuvre
Un monde neuf est déjà né.