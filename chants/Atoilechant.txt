R.
À TOI LE CHANT DE NOTRE VIE !
À TOI L’ÉLAN DE NOTRE COEUR,
À TOI, SEIGNEUR!

1.
Pour le fardeau que Tu enlèves,
Pour le pardon qui nous relève
Et pour la main que Tu nous tends, Ô Jésus !

2.
Pour les paroles de la vie,
Pour cette table où Tu convies
Et pour ce peuple rassemblé, Ô Jésus !

3.
Pour la lumière de ce jour,
Pour ce royaume de l’amour
Qui s’inaugure sur ta croix, Ô Jésus !