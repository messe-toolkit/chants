Resplendis, resplendis, nouvelle Jérusalem !
Sur toi la gloire du Seigneur s'est levée.
Réjouis-toi, exulte, ô Sion,
et toi, Mère de Dieu très pure, réjouis-toi,
car ton Fils est ressuscité !