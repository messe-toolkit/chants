Ô Abîme de la sagesse et de la science de Dieu,
Insondables ses décrets, incompréhensibles ses voies.

1.
Mystère de Dieu, Mystère du Christ
Où se trouvent cachés tous les trésors de la sagesse et de la connaissance.

2.
Dieu a voulu nous faire connaître la gloire de ce mystère
Au milieu des nations : Le Christ parmi nous, l'espérance de la gloire.