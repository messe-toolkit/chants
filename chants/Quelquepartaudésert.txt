QUELQUE PART AU DÉSERT UNE SOURCE,
QUELQUE PART LE DÉSIR CREUSE UN PUITS.
IL EST VENU S’ASSEOIR AU SOLEIL DE MIDI
ET J’AI PUISÉ DE L’EAU POUR APAISER SA FIÈVRE.
IL EST VENU S’ASSEOIR AU SOLEIL DE MA VIE
ET J’AI BU SON EAU VIVE.

1.
Savoir que le désert aride
Abrite un ruisseau de fraîcheur.
Savoir que le sillon fertile
Cache en son cœur le blé qui meurt.

2.
Creuser les cailloux jusqu’aux larmes
Avant d’accueillir le bonheur.
Creuser sans orgueil et sans arme
Pour ne voir bien qu’avec le cœur.

3.
Chercher une terre nouvelle
Enfouie sous la guerre et l’argent.
Chercher la fontaine éternelle
Où viendront boire les pauvres gens.

4.
Donner ses deux mains en offrande,
Au creux recevoir l’eau du ciel.
Donner à celui qui demande
Le grand cadeau providentiel.

5.
Ouvrir son histoire et sa porte,
Risquer d’être mal estimé.
Ouvrir son cœur grand comme un porche
Devant celui qui sait aimer.

6.
Courir annoncer le message,
Partir vers parents et amis.
Courir et crier aux nuages :
« Il est la source, il est le puits ! »