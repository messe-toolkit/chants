R.
Tout vient de toi, ô Père très bon :
Nous t'offrons les merveilles de ton amour.

1.
Voici, Seigneur, ton peuple assemblé
Joyeux de te célébrer.

2.
Voici le fruit de tous nos travaux,
L´offrande d´un cœur nouveau.

3.
Voici la joie de notre amitié,
L´amour nous a rassemblés.

4.
Voici l´effort des hommes de paix
Qui œuvrent dans l´univers.

5.
Voici la peine du monde entier
Qui cherche son unité.

6.
Par toi, ces dons deviennent le pain
Qui fait de nous des témoins.

7.
Voici, Seigneur, le pain de nos vies
Changées en la vie du Christ.

8.
Voici, Seigneur, le pain partagé,
Le signe de l´unité.

9.
Seigneur Jésus, ce pain est ton corps :
Nous sommes corps du Seigneur.

10.
L´Esprit d´amour consacre nos dons,
Par lui nous te bénissons.

11.
Merci, Seigneur, ta vie est en nous,
Merci pour tout ton amour.

12.
Louange à toi, ô Père très bon,
A toi, toute adoration.