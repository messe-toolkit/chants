SEIGNEUR, TU TE DONNES EN PARTAGE,
TU OFFRES TON CORPS ET TON SANG,
ACCUEILLE EN RETOUR NOTRE OFFRANDE,
LE PAIN ET LE VIN DE NOS VIES.

1.
Accepte, Seigneur, en échange,
Nos chants qui proclament ton Nom ;
Que règne en nos vies ta Présence,
Envoie ton Esprit sur ces dons.

2.
Regarde, Seigneur, notre monde,
Jeté en pâture aux puissants ;
Fais naître un soleil d’espérance,
Que brillent les yeux des enfants !

3.
Apaise, Seigneur, en nos frères,
L’angoisse des nuits sans matin ;
Que vienne une paix sans frontières,
Espoir de nouveaux lendemains.

4.
Rénove, Seigneur, ton Eglise,
Nourris ses enfants de ton Pain ;
Qu’ils aillent annoncer l’Évangile,
Joyeux de la joie de ton Vin.