1.
Par le petit garçon qui meurt près de sa mère
Tandis que les enfants s’amusent au parterre ;
Et par l’oiseau blessé qui ne sait pas comment
Son aile tout à coup s’ensanglante et descend
Par la soif et la faim et le délire ardent :
Je vous salue Marie.

2.
Par les gosses battus par l’ivrogne qui rentre,
Par l’âne qui reçoit des coups de pieds au ventre
Par l’humiliation de l’innocent châtié,
Par la vierge vendue qu’on a déshabillée,
Par le fils dont la mère a été insultée :
Je vous salue Marie.

3.
Par la vieille qui, trébuchant sous trop de poids,
S’écrie : « mon Dieu ! » Par le malheureux dont les bras
Ne purent s’appuyer sur une amour humaine
Comme la croix du fils sur Simon de Cyrène ;
Par le cheval tombé sous le chariot qu’il traîne :
Je vous salue Marie.

4.
Par les quatre horizons qui crucifient le monde,
Par tous ceux dont la chair se déchire ou succombe,
Par ceux qui sont sans pieds, par ceux qui sont sans mains,
Par le malade qui opère et qui geint
Et par le juste mis au rang des assassins :
Je vous salue Marie.

5.
Par la mère apprenant que son fils est guéri,
Par l’oiseau rappelant l’oiseau tombé du nid,
Par l’herbe qui a soif et recueille l’ondée,
Par le baiser perdu par l’amour redonné,
Et par le mendiant retrouvant sa monnaie :
Je vous salue Marie.