TU SERAS MA LOUANGE, SEIGNEUR,
DANS LA GRANDE ASSEMBLÉE.

1.
Devant ceux qui te craignent, je tiendrai mes promesses.
Les pauvres mangeront : ils seront rassasiés ;
ils loueront le Seigneur, ceux qui le cherchent :
« À vous, toujours, la vie et la joie ! »

2.
La terre entière se souviendra et reviendra vers le Seigneur,
chaque famille de nations se prosternera devant lui :
« Oui, au Seigneur la royauté,
le pouvoir sur les nations ! »

3.
Et moi, je vis pour lui : ma descendance le servira ;
on annoncera le Seigneur aux générations à venir.
On proclamera sa justice au peuple qui va naître :
Voilà son oeuvre !