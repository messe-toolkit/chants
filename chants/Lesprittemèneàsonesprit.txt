1.
L’Esprit te mène à son désert,
Toi qu’il baptise de son feu.
Deviens par lui ce fils de Dieu
Vainqueur du mal à découvert.

2.
L’Esprit anime ton désert,
Tu le découvres peu à peu.
Laisse-toi prendre par son jeu,
Ne pleure pas ce que tu perds.

3.
L’Esprit te parle en son désert,
Entends le Maître et le Seigneur.
Le chant qui monte dans ton coeur
Est joie plus vaste que la mer.

4.
L’Esprit travaille ton désert,
Son puits d’eau vive est généreux ;
Il peut tirer du sol pierreux
L’espoir nouveau d’un arbre vert.

5.
L’Esprit chemine en ton désert,
Il te conduit vers d’autres cieux :
Terrible exode où même Dieu
A vu trembler son corps de chair.

6.
L’Esprit franchit tous les déserts
Et notre Pâque est son seul voeu.
Allons sans crainte à ce qu’il veut,
L’amour est tout son univers.