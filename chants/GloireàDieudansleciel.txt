GLOIRE A DIEU DANS LE CIEL !
GRANDE PAIX SUR LA TERRE !
GLOIRE A DIEU DANS LE CIEL !
GRANDE PAIX SUR LA TERRE !

1.
Nous te louons, nous te bénissons, nous t’adorons,
Nous te glorifions, nous te rendons grâce pour ton immense gloire !

2.
Seigneur Dieu, le Roi du ciel, le Père tout-puissant !
Seigneur Dieu, Agneau de Dieu, le Fils du Père !

3.
Le seul Saint, le seul Seigneur, le seul Très Haut !
Jésus-Chrsit, avec l'esprit, dans la gloire du Père !