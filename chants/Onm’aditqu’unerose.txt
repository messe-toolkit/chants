1.
On m’a dit que les roses
Vivent ce que vivent les roses l’espace d’un matin.
On m’a dit que les choses
Ne sont que peu de chose : dérisoire destin.
On m’a dit : « Tu proposes »,
Mais c’est Dieu qui dispose quelque soit le chemin.
On m’a dit : « pas de pause ! »
Une terre s’arrose pour devenir jardin.

QUAND ON AIME ON GRANDIT
TON: “JE T’AIME”, ME L’A DIT.
QUAND ON AIME ON FLEURIT
TA FONTAINE: C’EST MA VIE !

2.
On m’a dit qu’une rose
A besoin de l’osmose, de ton temps, de tes liens.
On m’a dit : « Pour ta cause,
Avance lutte et ose mais desserre les poings. »
On m’a dit mille choses
Je suis comme en “surdose” le silence m’étreint.
Et tout devient grandiose
Quand ton regard se pose, quand tu me tends la main.

3.
On m’a dit qu’une rose
Fleurit comme en symbiose au fond de mon jardin
J’ai donc pris “fait et cause”
J’ai préparé ma prose et mes bouts de refrains.
Si fragile est ma rose
Si unique elle s’expose à inventer nos liens.
Je serai virtuose
Pour protéger ma rose au fond de mon jardin.