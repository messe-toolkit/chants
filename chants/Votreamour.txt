1.
Votre amour a tenu, votre amour a duré
Contre toute vertu, contre vents et marées,
Votre amour a fleuri, votre amour a germé
Entre soleil et pluie tout au long des années.

MON PÈRE, MA MÈRE,
JE VOUDRAIS VOUS REMERCIER
PAPA, MAMAN, DE VOUS AIMER SIMPLEMENT.

2.
Votre amour a conduit son navire en plein vent
Pour voguer sans abri entre neige et printemps,
Votre amour a choisi de porter des enfants
Comme on risque ses fruits sur la branche du temps.

3.
Votre amour a construit lentement sa maison
Des amis sont venus de tous les horizons,
Votre amour a forgé des soleils à foison
Pour nous donner le coeur d’inventer nos moissons.