NOUS SOMMES SUR LA TERRE
MESSAGERS DE LA JOIE
DE FOI ET DE LUMIÈRE,
MESSAGERS DE LA JOIE
AVEC DES SOEURS, AVEC DES FRÈRES
MESSAGERS DE LA JOIE
ENSEMBLE ET AVEC TOI
MESSAGERS DE LA JOIE.

1.
Ne craignons pas les différences
Sans elles tout serait tout gris
Chaque couleur est une chance
Chaque sourire peint la vie.

2.
Nous chantons que la vie est belle
Mêm’ si tout n’est pas toujours beau
Les moqueries et les querelles
Ne couleront pas le bateau.

3.
Notre vie est pèlerinage
Y’a tant de maux à y soigner
Soignons les mots qui les soulagent
Dans l’élan du Ressuscité.