Notre Père qui es aux cieux,
Que ton nom soit sanctifié, que ton règne vienne,
Que ta volonté soit faite sur la terre comme au ciel.
Donne-nous aujourd’hui notre pain de ce jour,
Pardonne-nous nos offenses
Comme nous pardonnons aussi à ceux qui nous ont offensés
Et ne nous soumets pas à la tentation,
Mais délivre-nous du mal.

Car c’est à Toi qu’appartiennent
Le règne, la puissance et la gloire
Pour les siècles des siècles.

Nouvelle traduction
(entrée en vigueur le premier dimanche de l’Avent 2017 soit le 3 décembre 2017)

Notre Père qui es aux cieux,
que ton nom soit sanctifié, que ton règne vienne,
que ta volonté soit faite sur la terre comme au ciel.
Donne-nous aujourd'hui notre pain de ce jour.
Pardonne-nous nos offenses,
comme nous pardonnons aussi à ceux qui nous ont offensés.
Et ne nous laisse pas entrer en tentation,
mais délivre-nous du Mal.

Car c'est à toi qu'appartiennent
le règne, la puissance et la gloire,
pour les siècles des siècles !