" Aujourd’hui, écoutez la voix du Seigneur. "
Témoin rebelle des exploits du Seigneur,
le peuple assoiffé met au défi la patience de Dieu.
L’eau, jaillie du rocher de l’Horeb,
chante la présence de Dieu
au milieu des enfants d’Israël.
" Acclamons notre rocher, notre salut ! "
Témoin fidèle de la tradition de ses pères,
la femme altérée met au défi la puissance de Jésus.
L’eau, tirée du puits de Jacob,
annonce le don de Dieu
à ceux qui attendent le Christ Sauveur.
" Moi qui te parle, je suis le Messie. "
Un amour ardent embrase le coeur du Fils,
eau vive de véritable adoration :
l’heure viendra, selon les Ecritures, pour le Messie annoncé ;
l’heure est venue, selon sa Parole, pour la présence révélée.
" L’amour de Dieu est répandu dans nos coeurs par l’Esprit. "
Nourri de la volonté du Père
et envoyé parfaire l’oeuvre de Dieu,
Jésus exulte de joie :
la terre a reçu la semence.
" Au temps fixé par Dieu, le Christ est mort pour nous. "
Nourris de la Parole du Maître
et envoyés recueillir les prémices du salut,
les apôtres lèvent les yeux et regardent :
les champs sont prêts pour la moisson.
" C’est vraiment lui, le Sauveur du monde ! "