LA COUPE DE BÉNÉDICTION
EST COMMUNION AU SANG DU CHRIST.

1.
Comment rendrai-je au Seigneur
tout le bien qu’il m’a fait ?
J’élèverai la coupe du salut,
j’invoquerai le nom du Seigneur.

2.
Il en coûte au Seigneur
de voir mourir les siens !
Ne suis-je pas, Seigneur, ton serviteur,
moi, dont tu brisas les chaînes ?

3.
Je t’offrirai le sacrifice d’action de grâce,
j’invoquerai le nom du Seigneur.
Je tiendrai mes promesses au Seigneur,
oui, devant tout son peuple.