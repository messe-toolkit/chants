Seigneur, ouvre mes lèvres, et ma bouche publiera ta louange.

Soliste/ Assemblée

R.
Le Seigneur est vraiment ressuscité. Alléluia !
Schola

R.
Le Seigneur est vraiment ressuscité. Alléluia !

Ps 116.
Louez le Seigneur tous les peuples, fêtez-le tous les pays.

Son amour envers nous s'est montré le plus fort
Éternelle est la fidélité du Seigneur.

Bénissons le Seigneur, Nous rendons grâce à Dieu.
Que le Seigneur nous bénisse, qu'il nous garde de tout mal
Et nous conduise à la vie éternelle. Amen !