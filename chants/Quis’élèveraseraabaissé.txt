R.
QUI S’ÉLÈVERA SERA ABAISSÉ,
QUI S’ABAISSERA SERA ÉLEVÉ.

1.
Vous n’avez qu’un seul Père, celui qui est aux cieux.

2.
Vous n’avez qu’un seul maître, le Christ.