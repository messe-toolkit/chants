R.
Dieu est grand, Dieu est présent,
Dieu Miséricorde !
Dieu est grand, Dieu est présent,
À son amour il nous accorde.

1.
Comme une lyre il nous accorde
Et nous vibrons sous son Esprit.
Des sons de fête en nous résonnent,
Montés du coeur de Jésus Christ :
Amour suprême qui se donne
Et transfigure toute vie.

2.
Mais pour vibrer avec justesse
Il faut, Seigneur, beaucoup de temps !
Le chant nouveau de la tendresse
Est un combat de chaque instant ;
Que notre monde le connaisse,
Il en a soif depuis longtemps !

3.
Toi, l’Harmonie de toutes choses,
Éveille-nous à tes accents !
Nous entrerons dans le Royaume
Où se rassemblent tes enfants ;
Et que ta paix sur nous repose,
Dieu notre Père, amour puissant !