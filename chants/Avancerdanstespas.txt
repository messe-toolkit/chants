Loué sois-tu, mon Seigneur, avec toutes tes créatures,
spécialement messire frère Soleil, qui fait le jour :
par lui tu nous illumines.
Il est beau, rayonnant d'une grande splendeur :
de toi, Très-Haut, il est le symbole.
Loué sois-tu, mon Seigneur, pour sœur Lune et les étoiles :
dans le ciel tu les as formées, claires, précieuses et belles.
Loué sois-tu, mon Seigneur, pour frère Vent
et pour l'air et le nuage, pour le ciel serein et tous les temps :
par eux, tu soutiens tes créatures.
Loué sois-tu, mon Seigneur, pour sœur Eau,
qui est très utile et humble, précieuse et chaste.
Loué sois-tu, mon Seigneur, pour frère Feu,
par qui tu illumines la nuit :
il est beau et joyeux, robuste et fort.
Loué sois-tu, mon Seigneur, pour sœur notre mère la Terre,
elle nous porte et nous nourrit,
elle produit la diversité des fruits,
avec les fleurs aux mille couleurs et l'herbe.