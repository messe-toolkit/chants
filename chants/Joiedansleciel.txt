JOIE DANS LE CIEL,
JOIE POUR NOTRE PÈRE,
UN PÉCHEUR EST DE RETOUR!
JOIE DANS LE CIEL,
JOIE SUR NOTRE TERRE,
DIEU PRODIGUE SON AMOUR!

1.
On le disait l’enfant perdu,
L’amant de toutes les folies.
À l’horizon aucune issue,
La fièvre au corps brûlait sa vie.
Mais un beau jour la faim l’emporte,
Sa chair implore un peu de pain.
Son père attend devant la porte,
La place est prête à son festin.

2.
On la croyait brebis perdue,
L’enfant terrible du troupeau.
Partie sans peur vers l’inconnu,
Fuyant la corde et les enclos.
Mais sans répit Quelqu’un la cherche,
Son coeur découvre le berger.
Avec la main de la tendresse
La fugitive est ramenée.

3.
On l’appelait Samaritain
Cet hérétique en Israël.
Était-il bien sur le chemin
De ceux qui servent l’Éternel ?
Mais en passant ses yeux regardent,
Il fait un pas vers le blessé.
Le voilà prêt pour le partage,
En lui l’amour est vérité.

4.
On le nommait le publicain,
L’argent de Rome était sa loi.
Il ne voyait jamais quelqu’un
Pour une fête sous son toit.
Mais de tout près Jésus lui parle,
Son coeur entend le mot “Zachée”
Sa vie bascule et prend le large,
Ses mains sont libres pour donner.

5.
On la trouvait au fond des rues,
C’était la nuit son univers.
À pleine coupe elle avait bu
Les désespoirs les plus amers.
Mais chez Simon c’est la rencontre,
Jésus lui dit qu’elle est aimée.
Dans le parfum de sa réponse
La vie nouvelle est annoncée.