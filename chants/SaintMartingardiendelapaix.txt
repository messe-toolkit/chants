SAINT MARTIN, SAINT MARTIN
POUR SOULEVER LES MURS
LES NUITS OÙ C’EST TROP DUR
ET TROP LONG NOS CHEMINS
TU ES LE PATRON : S’IL TE PLAÎT
GARDE-NOUS GARDIENS DE LA PAIX.

1.
Dehors la nuit se noie dans l’ombre
Il fait de plus en plus épais
Au coin des rues dans la pénombre
Je suis là toujours aux aguets.

2.
Le crime est là qui cherche à mordre
Sur le trottoir des trafiquants
Je suis là pour maintenir l’ordre
N’importe où et n’importe quand.

3.
Ma vocation c’est la justice
Je dois lui rapporter les faits
Ma profession c’est la police
Mon nom c’est Gardien de la Paix.

4.
La prière est ma meilleure arme
Contre mes violences et mes peurs
En pleine action dans les vacarmes
Elle est la paix gardant le coeur.