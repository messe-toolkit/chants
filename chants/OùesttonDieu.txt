1.
Que puis-je annoncer à mes frères
Quand tout le jour j´entends dire :
Où est ton Dieu ?
Où est ton Dieu ?
Je dirai des mots de tendresse
En révélant ton nom au cœur de tout amour.
Je dirai des mots de tendresse
En révélant ton nom au cœur de tout amour.

2.
Que puis-je donner à mes frères
Quand tout le jour j´entends dire :
Où est ton Dieu ?
Où est ton Dieu ?
Je prendrai le blé de la terre
Et j´en ferai le pain de ton amour pour nous.
Je prendrai le blé de la terre
Et j´en ferai le pain de ton amour pour nous.

3.
Que puis-je annoncer à mes frères
Quand tout le jour j´entends dire :
Où est ton Dieu ?
Où est ton Dieu ?
Je boirai le vin de tes noces
En proclamant partout que l´homme est invité.
Je boirai le vin de tes noces
En proclamant partout que l´homme est invité.

4.
Que puis-je entonner pour mes frères
Quand tout le jour j´entends dire :
Où est ton Dieu ?
Où est ton Dieu ?
J´attendrai le feu de l´aurore
En murmurant le chant du nouveau jour qui vient.
J´attendrai le feu de l´aurore
En murmurant le chant du nouveau jour qui vient.