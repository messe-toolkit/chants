R.
Bravo ! Alléluia ! Chantons le Livre
Bravo ! Alléluia ! Ouvrons le Livre
Et Dieu nous parlera.

1.
Écoutons l’histoire
De Dieu parmi nous
Écoutons l’histoire
Mettons-nous debout.

2.
C’est Jésus qui parle
Ouvrons notre cœur
C’est Jésus qui parle
Il est le Seigneur.

3.
C’est une parole
Message de Dieu
C’est une parole
Qui nous rend heureux.