AU SEIGNEUR NOTRE DIEU,
TOUT HONNEUR ET TOUTE GLOIRE!

1.
Chantez au Seigneur un chant nouveau,
racontez à tous les peuples sa gloire,
à toutes les nations ses merveilles !

2.
Il est grand, le Seigneur, hautement loué,
redoutable au-dessus de tous les dieux :
lui, le Seigneur, a fait les cieux.

3.
Rendez au Seigneur, familles des peuples,
rendez au Seigneur la gloire et la puissance,
rendez au Seigneur la gloire de son nom.

4.
Adorez le Seigneur, éblouissant de sainteté :
Allez dire aux nations : « Le Seigneur est roi ! »
Il gouverne les peuples avec droiture.