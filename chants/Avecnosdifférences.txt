AVEC NOS DIFFÉRENCES,
MALGRÉ NOS DIVISIONS,
SEIGNEUR JÉSUS-CHRIST,
PRENDS-NOUS DANS TON ALLIANCE !
ASSEMBLE TON ÉGLISE !

1.
Ton amour est sans mesure.
Tu désarmes nos combats.
Dans les sillons de nos blessures
Ta semence germera.

2.
Tu inspires les prophètes
Qui réchauffent notre coeur.
Tu nous envoies des adversaires
Qui éclairent nos erreurs.

3.
En tout lieu où tu demeures,
À travers tous les pays,
Tu nous relie(s) en un seul peuple
Animé du même Esprit.

4.
Dieu est Père : il nous adopte.
Plus de frères ennemis !
Dans le Royaume qu’il nous offre
L’esclavage est aboli.

5.
Tu nous aimes sans mesure.
Ta moisson est annoncée.
Ni les combats ni les blessures
Ne pourront nous séparer.