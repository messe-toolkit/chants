AU BORD DE L’EAU,
NOTRE MONDE EST SI BEAU,
NOTRE MONDE EST SI BEAU,
SI BEAU !

1.
Pour être heureux,
Découvrir des merveilles,
Pour être heureux,
Chanter, croire au soleil.
Ouvrir nos mains,
Retrouver les copains,
Ouvrir nos mains,
Partager nos refrains.

2.
Petits et grands,
Formez une famille,
Petits et grands,
Restez toujours unis.
De coeur en coeur,
La joie chasse la peur,
Gloire au Seigneur,
C’est lui le vrai bonheur.