1.
Hey, la vie, pour ne pas te formater,
Recopier la télé,
Accroche-toi aux nuages ! - La vie !
Salut, fais bon voyage ! - Salut !
Tape ! Tape dans tes mains !

2.
Hey, la vie, va falloir embrayer,
Jocker et s’envoler,
Ouvrir tout grand la cage ! - La vie !
Choisir d’autres rivages ! - Choisir !
Tape ! Tape dans tes mains !

3.
Hey, la vie, si tu n’veux pas zapper,
Exploser, imploser,
Écoute le message ! - La vie !
Au fond d’un coquillage ! - Au fond !
Tape ! Tape dans tes mains !

4.
Hey, la vie, va falloir y penser,
Tant de gens à aimer,
Tant et tant à se dire ! - La vie !
À chanter, à écrire ! - Chanter !
Tape ! Tape dans tes mains !