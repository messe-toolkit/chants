ANTIENNE
Prière de jour, prière de nuit, emporte-nous au sein du Dieu béni.

3.
Qu’il soit béni, le Dieu et Père
de notre Seigneur, Jésus, le Christ!
Il nous a bénis et comblés
des bénédictions de l’Esprit,
au ciel dans le Christ.

4.
Il nous a choisis, dans le Christ,
avant que le monde fût créé,
pour être saints et sans péchés devant
sa face, grâce à son amour.5 Il nous a prédestinés
à être pour lui des fils adoptifs
par Jésus, le Christ.
Ainsi l’a voulu sa bonté,

6.
à la louange de gloire de sa grâce,
la grâce qu’il nous a faite
dans le Fils bien-aimé.

7.
En lui, par son sang, nous avons le rachat,
le pardon des péchés

8.
C’est la richesse de sa grâce
dont il déborde jusqu’à nous
en toute intelligence et sagesse.

9.
Il nous dévoile ainsi
le mystère de sa volonté
selon que sa bonté l’avait prévu
dans le Christ:

10.
pour mener les temps à leur plénitude,
récapituler toutes choses dans le Christ
celles du ciel et celles de la terre.