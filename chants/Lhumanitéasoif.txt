1.
DU PREMIER JOUR.
AU TEMPS QUI SE PRÉPARE
L’HUMANITÉ A SOIF
BELLE JEUNESSE
VOUS ÉCRIREZ L’HISTOIRE
L’HUMANITÉ A SOIF

Dans ses puits de cailloux
Dans sa terre à genoux
Dans ses peuples sans eau
Dans le flanc des taureaux
Dans le rêve immobile
D’un poète en exil

2.
DU PREMIER JOUR.
AU TEMPS QUI SE PRÉPARE
L’HUMANITÉ A SOIF
BELLE JEUNESSE
POUR SA PLUS GRANDE GLOIRE
L’HUMANITÉ A SOIF

Dans ses coeurs en cagoule
Dans le credo des foules
Implorant le pardon
D’un Dieu de procession
Dans ses rues de misère
Ses ghettos de colère

3.
DU PREMIER JOUR.
AU TEMPS QUI SE PRÉPARE
L’HUMANITÉ A SOIF
BELLE JEUNESSE
QUAND LA PEUR NOUS ÉGARE
L’HUMANITÉ A SOIF

3.
Mais elle ouvre son ciel
Tant de passé appelle
De nouvelles rivières
Un vin qui désaltère
Peut-être que ce soir
Chacun pourra le boire

4.
DU PREMIER JOUR.
AU TEMPS QUI SE PRÉPARE
L’HUMANITÉ A SOIF
LEQUEL DE NOUS
EN GARDE LA MÉMOIRE
L’HUMANITÉ A SOIF

P.
BELLE JEUNESSE
VOUS ÉCRIVEZ L’HISTOIRE
L’HUMANITÉ A SOIF