Dieu saint, Dieu saint et fort,
Dieu saint et immortel.
Kyrie eleison, Kyrie eleison.
Kyrie eleison.
Toi qui as souffert pour nous sur la Croix,
Christe eleison, Christe eleison.
Christe eleison.
Souviens-toi de nous quand tu viendras dans ton Royaume.
Kyrie eleison, Kyrie eleison.
Kyrie eleison.

Variantes du 2ème Verset pour les divers temps liturgiques.

Avent : Toi qui viens nous sauver.
Noël : Toi qui es né de la Vierge Marie.
Carême : Toi qui t'es manifesté dans notre chair.
Passion : Toi qui as souffert pour nous sur la Croix.
Pâques : Toi qui es ressuscité d'entre les morts.
Ascension : Toi qui es monté dans les cieux.
Pentecôte : Toi qui nous envoies l'Esprit Saint.