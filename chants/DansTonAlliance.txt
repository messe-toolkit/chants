R.
Aujourd’hui Dieu d’espérance,
Tu les reçois dans ton Alliance,
Béni sois-tu pour tant de joie.
Aujourd’hui, Dieu d’espérance,
Tu les reçois dans ton Alliance,
Béni sois-tu, alleluia !

1.
Un jour, leurs yeux se sont levés.
Ce jour-là, leurs regards ont brillé.

2.
Un jour, leurs pas se sont croisés,
Ce jour-là, leur chemin s’est tracé.

3.
Un jour, leurs voix se sont trouvées,
Ce jour-là, une source a chanté.

4.
Un jour, leurs mains se sont touchées,
Ce jour-là, leur silence a parlé.

5.
Les jours de grâce et les jours gris,
Tous ces jours où bâtir une vie.