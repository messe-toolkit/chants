1.
Ô seul Maître des temps,
Jésus tu nous conduis,
Nous suivons tes chemins,
Nous cherchons ton visage.

2.
Étrangers, pèlerins,
Toujours prêts à partir,
Nous portons nos regards
Vers le Jour et vers l'Heure.

3.
Nous marchons sur tes pas,
Tu viens à nos devants :
Dans le jeu de la foi
Nous guettons l'Invisible.