Gloire et louange à Toi, Seigneur Jésus !

1.
Christ manifesté dans la chair,
Gloire et louange à Toi.

2.
Christ justifié dans l'Esprit.
Gloire et louange à Toi.

3.
Christ contemplé par les Anges.
Gloire et louange à Toi, Seigneur Jésus !

4.
Christ proclamé chez les païens.
Gloire et louange à Toi.

5.
Christ qui est cru dans le monde.
Gloire et louange à Toi.

6.
Christ exalté dans la gloire.
Gloire et louange à Toi, Seigneur Jésus.