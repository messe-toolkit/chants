1.
Pour son peuple en fête.
La table est dressée.
Le Seigneur appelle
Tous ses invités
A venir aux noces
Du Fils Bien-Aimé
Qui fait alliance,
Avec l’humanité.

2.
Voici venue l’heure
Du plus grand amour
Où le Fils de l’homme
Offre à ses amis
Le vin de la grappe
Pressée sur la Croix
Qu’il vous donne à boire,
Enfants du même sang.

3.
Les anges l’adorent.
Allélouïa.
L’univers exulte.
Allélouïa.
L’Eglise rend grâce
Allélouïa
Au Seigneur des siècles,
Allélouïa !