TOUTE CHOSE PASSE
MAIS AU SOIR DE LA VIE,
SEUL DEMEURE L’AMOUR

1.
Alors, dans ton grenier,
Engrange beaucoup de grain,
Alors, moissons de paix
Déborderont demain.

2.
Alors, dans ton pressoir,
Recueille beaucoup de fruit,
Alors, au dernier soir,
Tu goûteras à la vie.

3.
Alors, que dans ton cœur,
Ton Dieu soit ton seul trésor,
Alors, en grand vainqueur,
L’amour sera plus fort.