CAP SUR L’ÉVANGILE EMBARQUE AVEC NOUS
ENSEMBLE AU SOUFFLE DE L’ESPRIT
CAP SUR L’ÉVANGILE EMBARQUE AVEC NOUS
DEMAIN SE PRÉPARE AUJOURD’HUI.

1.
Si tu n’as que tes mains à donner aujourd’hui
Tes rêves et tes projets pour faire changer la vie
Si tu n’as que ton coeur, tes espoirs, tes amis
Demain te réclame aujourd’hui.

2.
Viens choisir aujourd’hui les couleurs de demain
Projeter l’avenir aux reflets d’un matin
Le printemps d’une Pâques où la vie a jailli
Demain se dessine aujourd’hui.

3.
Pour bâtir aujourd’hui l’Église de demain
En suivant les apôtres, suivons Jésus-Christ
Il vient, il l’a promis, partager nos chemins
Demain se révèle aujourd’hui.