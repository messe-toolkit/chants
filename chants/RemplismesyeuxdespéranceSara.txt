R.
REMPLIS MES YEUX D'ESPERANCE
REMPLIS MON RIRE DE CONFIANCE
ET REMPLIS MA VIE DE TA VIE.

1.
Si je peux contempler les étoiles,
Je ne peux mesurer ton Amour.
Chaque jour le présent te dévoile,
La confiance n'a que l'âge de l'Amour.

2.
Si j'ai peur que le temps m'emprisonne,
Je ne peux mesurer ton Amour.
Chaque jour le présent nous étonne,
L'Espérance n'a que l'âge de l'Amour.

3.
Si je peux enfanter dans mon âge,
Je ne peux mesurer ton Amour.
Chaque jour le présent nous engage,
Ta puissance n'a que l'âge de l'Amour.

4.
Si je peux si longtemps oser croire,
Je ne peux mesurer ton Amour.
Chaque jour le présent est victoire ;
La patience n'a que l'âge de l'Amour.

5.
Si je peux accueillir la promesse,
Je ne peux mesurer ton Amour.
Chaque jour le présent est jeunesse,
Ton Alliance n'est que l'oeuvre de l'Amour.