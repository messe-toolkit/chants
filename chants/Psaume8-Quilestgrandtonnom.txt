1.
(Refrain du Psaume 8)
O Seigneur, notre Dieu,
Qu’il est grand ton nom
Par toute la terre !
Alleluia !

2.
(Acclamation de l’Évangile)
Alleluia !
Alleluia !

3.
(Refrain de prière universelle)
O Seigneur, notre Dieu,
Nous crions vers toi,
Entends nos prières !