R.
Entre tes mains mon dieu
Entre tes mains je remets mon Esprit
Entre tes mains mon dieu
Entre tes mains ma vie.

1.
Je te promets de reprendre la route
Et de trouver la force au fond de moi
Je m’en remets, avec mes peurs et mes doutes
Je m’en remets entièrement à TOI.

2.
Je te promets de te rester fidèle
Jusqu’au matin « Que serais-je sans toi ! »
Je m’en remets, c’est pour ça que j’appelle
Pour exister entièrement à TOI.

3.
Je te promets de chercher à comprendre
Sans avoir peur, ni des autres ni de moi.
Je te promets la braise sous la cendre,
Puisque le FEU nous vient d’abord de TOI !