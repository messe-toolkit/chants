R.
APPELÉS À LA MÊME ESPÉRANCE,
BAPTISÉS DANS LE MÊME ESPRIT SAINT,
NOUS FORMONS TON ÉGLISE, SEIGNEUR,
UN SEUL CORPS À LA GLOIRE DU PÈRE.

1.
La même foi
Nous conduit dans ta demeure,
Nous proclamons
Que tu es le seul Sauveur.
Louange à toi
Qui nous aimes et nous rassembles !
Béni sois-tu
De répondre à notre attente.

2.
Un même cri
Nous arrache à nos ténèbres,
Nous accueillons
Ton message comme un feu.
Louange à toi
Qui nous donnes ta parole,
Béni sois-tu
Pour les signes du Royaume!

3.
Un même pain
Est offert à cette table,
Nous avançons
Vers la joie de ton festin.
Louange à toi
Qui conduis notre prière,
Béni sois-tu
D’habiter nos chants de fête !

4.
Un même espoir
Aujourd’hui nous met en marche,
Nous recherchons
La lumière sur tes pas.
Louange à toi
Fils de Dieu qui nous précèdes !
Béni sois-tu
D’être à l’oeuvre sur la terre !