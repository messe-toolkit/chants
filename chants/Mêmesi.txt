1.
Même si la mer est démontée,
Même si l’orage a éclaté,
Garde confiance et continue,
Toute tempête sera vaincue.
Même si tes terres sont brûlées,
Si le soleil a tout séché,
Il faut semer, un jour la pluie
Fera germer les grains enfouis.
Même si parfois tu n’en peux plus
D’entendre dire on t’a trop vu,
Reste debout et sois confiant,
Ton heure viendra, quelqu’un t’attend.

2.
Même si tes ailes sont coupées
Si tu te sens abandonné
Espère encore, il est des gens
Qui ont vraiment un coeur géant.
Même si ton ciel est sans étoile
Et que ta vie semble banale
Puisse le feu d’autres regards
Brûler en toi comme un espoir.
Même si tu es devant un mur
Que tu ne peux franchir, c’est sûr,
Rappelle-toi comme un refrain,
C’est à plusieurs qu’on y parvient.

3.
Même si le brouillard t’a perdu,
Même si tu es blessé, fourbu,
Je suis certain qu’un bon berger
Saura chercher pour te trouver.
Même si tu as gâché ta vie,
Même si tu n’en sais plus le prix,
Relève-toi, crois au pardon
Qui t’est offert sans condition.
Même si tes jours sont pleins de croix
Je t’en supplie, ne faiblis pas,
Regarde bien sur ton chemin
Ton Dieu t’attend, tends-lui la main.