1.
Transmets la Parole que tu reçois,
Dieu te l’a donnée, tu lui dis « je crois ! »
Les mots qui ont pris chair en Jésus Christ,
Qu’ils soient parole neuve dans ta vie !

R.
Parole qui murmure un « Lève-Toi! »,
Verbe de Dieu, trésor de vie,
Parole qui réveille et me dit « Va ! »,
Que je t'annonce, Ô Jésus Christ!
Ô Jésus Christ ! (bis)

2.
Transmets la Parole qui t’a sauvé,
Tu révéleras le Seigneur de paix.
Par toi, que l’espérance allume un feu !
La flamme du Seigneur emplit tes yeux.

3.
Transmets la Parole qui met debout :
Le Sauveur l’a fait, en témoin d’amour.
Il a guéri l’aveugle et le boiteux,
Il a redonné force aux dix lépreux.

4.
Transmets la Parole où naîtra la joie,
Il y a quelqu’un qui l’attend de toi.
Heureux qui prend la route avec Jésus,
Heureux qui ose croire à sa venue !

5.
Transmets la Parole que tu reçois,
Dieu la donne au monde, et tu es sa voix.
Le grain qui tombera dans le sillon,
Un jour il germera pour la moisson.