R.
Seigneur, Ô Maître souverain,
Sauve-nous quand nous veillons,
Et garde-nous quand nous dormons.
Nous veillerons avec le Christ
Et reposerons en paix,
Garde-nous dans ta paix.

1.
Maintenant Seigneur,
tu peux laisser ton serviteur,
Aller en paix selon ta parole.
Mes yeux ont vu le salut que tu prépares
À la face d´Israël ton peuple.

2.
Lumière pour éclairer les nations,
Et gloire d´Israël ton peuple.
Gloire au Père, gloire au Fils, au Saint-Esprit
Maintenant et dans tous les siècles.