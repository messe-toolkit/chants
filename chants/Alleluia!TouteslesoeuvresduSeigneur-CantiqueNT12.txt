a1
Le salut, la puissance, la gloire de notre Dieu. Alleluia 2.
a2
Ils sont justes, ils sont vrais, ses jugements. Alleluia 2.

b
Célébrez notre Dieu, serviteurs du Seigneur.
Alleluia 3.
b Vous tous qui le craignez, les petits et les grands. Alleluia 3.

c
Il règne le Seigneur notre Dieu Tout-puissant. Alleluia 2.
c
Exultons, crions de joie et rendons-lui la gloire. Alleluia 2.

d1
Car elles sont venues les noces de l’agneau. Alleluia 3.
d2
Et pour lui son épouse a revêtu sa parure. Alleluia 1.