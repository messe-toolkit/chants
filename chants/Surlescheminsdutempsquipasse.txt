1.
Sur les chemins du temps qui passe
Comment chanter le temps qui dure ?
Ce temps de l’homme après la mort
N’est-il que silence et froidure ?
POUR TOI, SEIGNEUR, IL N’EST D’AUTOMNE
SANS CRAQUEMENTS NI FEUILLES MORTES.
DIS-NOUS LA MORT QUI FUT LA TIENNE
ET L’AU-DELÀ DE MORT HUMAINE.

2.
As-tu rejoint nouvelle terre
Où le soleil est Dieu lui-même ?
Vois-tu renaître aux cieux nouveaux
Le souffle animant ta demeure ?

3.
Raconte-nous l’autre versant,
L’éveil secret au Dieu vivant,
La vie qui monte et se répand,
Jeunesse au plein coeur des sarments.

4.
Toi qui saisis le temps qui dure,
Laisse-nous croire au temps qui passe.
Il est au creux de nos hivers
Un feu qui déjà transfigure.

5.
L’Esprit murmure sa chanson,
Printemps d’amour à déraison ;
L’espoir fleurit nos horizons
Quand Pâque envahit nos maisons.

6.
Viendra la nuit d’errance folle
Et la vendange de nos heures.
Qui pourra boire aux derniers temps
Le vin des nouvelles aurores ?

7.
Les arbres morts, les vieux sarments
Ressurgiront aux quatre vents ;
Dieu nous appelle à son levant
Pour vivre la joie de son temps.