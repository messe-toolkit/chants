1.
J’ai vu l’eau vive jaillissant du coeur du Christ,
Alléluia ! Alléluia !
Tous ceux que lave cette eau seront sauvés et chanteront :
Alléluia, Alléluia, Alléluia !

2.
J’ai vu la source devenir un fleuve immense,
Alléluia ! Alléluia !
Les fils de Dieu rassemblés chantaient leur joie d’être sauvés,
Alléluia, Alléluia, Alléluia !

3.
J’ai vu le Temple désormais s’ouvrir à tous,
Alléluia ! Alléluia !
Le Christ revient victorieux montrant la plaie de son côté,
Alléluia, Alléluia, Alléluia !

4.
J’ai vu le Verbe nous donner la paix de Dieu,
Alléluia ! Alléluia !
Tous ceux qui croient en son nom seront sauvés et chanteront :
Alléluia, Alléluia, Alléluia !