1.
Il était une foi… la mienne
Entre mes peurs et mes “je t’aime”
Entre le ciel et l’océan
Entre homme et femme,
Entre maintenant et longtemps

R.
IL ÉTAIT UNE FOI… “JE CROIS”.
IL ÉTAIT UNE FOI… “JE CROIS”.

2.
Il était une foi… l’histoire
Marquée au fer de nos mémoires
Une femme au pied de la croix
Ta mère pleure,
Le fallait-il vraiment ? Pourquoi ?

3.
Il était une foi… la nôtre
Que l’on soit damné ou apôtre
Entre courage et lâcheté
Nos petitesses
Et nos grandeurs d’humanité

4.
Il était une foi… l’Église
Petits îlots de Terre Promise
Morceaux épars d’un même puzzle
Le pain qui lève,
Du grain éclaté sous la meule

5.
Il était une foi… quand même
Moins de “j’ai peur” que de “je t’aime”
Car mon Dieu tu portes le poids
De mes silences,
Schisme de feu au creux de moi

6.
Il était une foi… ensemble
La vie qui gagne la mort qui tremble
Sur nos chemins tu nous rejoins
On se rassemble
Pour la Parole et pour le Pain

7.
Il était une foi… sereine
Coeur en avant, corps en Carême
Tu sais, je pense à toi souvent…
Ma route humaine
Cherche l’auberge où tu m’attends