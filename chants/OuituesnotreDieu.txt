OUI, TU ES NOTRE DIEU,
ET NOUS LE PEUPLE
QUE TU GUIDES PAR TA MAIN.

1.
Criez de joie pour le Seigneur,
Hommes droits, à vous la louange.
Rendez grâce au Seigneur sur la cithare,
Jouez pour lui sur la harpe à dix cordes.

2.
Oui, elle est droite, la parole du Seigneur,
Il est fidèle en tout ce qu’il fait.
Il aime le bon droit et la justice :
La terre est remplie de son amour.

3.
Heureux le peuple dont le Seigneur est le Dieu,
Heureuse la nation qu’il s’est choisie pour domaine !
Du haut des cieux, le Seigneur regarde :
Il voit la race des hommes.

4.
Du lieu qu’il habite, il observe
Tous les habitants de la terre,
Lui qui forme le coeur de chacun,
Qui pénètre toutes leurs actions.

5.
Nous attendons notre vie du Seigneur :
Il est pour nous un appui, un bouclier.
La joie de notre coeur vient de lui,
Notre confiance est dans son nom très saint.