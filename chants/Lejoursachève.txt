1.
Le jour s'achève, mais la gloire du christ illumine le soir.
Le pain rompu, le vin nouveau portent leur fruit de louange :
Béni sois-tu, ô notre Père, en Jésus, le Vivant !

2.
L'Esprit nous garde sous l'alliance du Christ et le signe pascal.
La vie reçue, la vie donnée rythment le temps de l'Eglise :
Nous sommes tiens, ô notre Père, en Jésus, le Vivant !

3.
Le monde marche vers le règne du Christ, et sa nuit prendra fin.
Nos coeurs l'ont su, nos yeux verront : l'oeuvre de Dieu est lumière.
Tu nous l'as dit, ô notre Père, en Jésus, le Vivant !

4.
Que l'on découvre le visage du Christ à la joie des sauvés !
Il est venu, Il vient encor', Dieu tient toujours ses promesses :
Tu nous bénis, ô notre Père, en Jésus, ton enfant !