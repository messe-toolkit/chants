1.
Que la lumière éclate au firmament du ciel
que la lumière brille sur la terre et sur l’eau
pour faire chanter le monde.

Il y eut un soir, il y eut un matin
et Dieu vit que cela était bon.
Il y eut un soir, il y eut un matin
et Dieu vit que cela était bon.

2.
Qu’il soit des luminaires au firmament du ciel
soleil pour brûler étoiles et lune d’or
pour faire chanter la nuit.

3.
Que les oiseaux du ciel volent au firmament
et que les poissons glissent dans la profonde mer,
que tout se multiplie.

4.
Alors Dieu créa l’homme, à son image
le créa homme et femme
il le fit, pour qu’ensemble au long des jours,
ils gardent le monde dans l’amour.