Stance
Grande fête aujourd’hui
dans la cité du ciel !
Aujourd’hui l’Esprit saint
vient reposer sur les humbles,
saisis par sa parole.

AUJOURD’HUI TOUT EXULTE ET CHANTE !
AUJOURD’HUI TOUT EXULTE ET CHANTE !

1.
Tu mets dans nos coeurs plus de joie
que toutes les vendanges et les moissons.

2.
Le vin de la vraie vigne,
renouvelle nos coeurs.

3.
Les anges avec les hommes
chantent même louange.