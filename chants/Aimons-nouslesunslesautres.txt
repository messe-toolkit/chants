R.
Aimons-nous les uns les autres comme Dieu nous aime.

Psaume 145.
Heureux qui s'appuie sur le Dieu de Jacob,
Il garde à jamais sa fidélité,
Il fait justice aux opprimés;
aux affamés, il donne le pain.

Le Seigneur délie les enchaînés,
le Seigneur ouvre les yeux des aveugles.
Le Seigneur redresse les accablés,
le Seigneur aime les justes.

Le Seigneur protège l'étranger,
il soutient la veuve et l'orphelin.
Il égare les pas du méchant,
d'âge en âge, le Seigneur régnera.