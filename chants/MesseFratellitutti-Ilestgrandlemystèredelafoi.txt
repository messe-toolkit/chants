Il est grand, le mystère de la foi :
Nous annonçons ta mort, Seigneur Jésus,
nous proclamons ta résurrection,
nous attendons ta venue dans la gloire.