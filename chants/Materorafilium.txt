1.
Mater ora filium,
ut post hoc exilium
nobis donet gaudium,
beatorum omnium.
Connaissez-vous la nouvelle
qu’un berger s’en vient chantant ?
Douce Marie toute belle
a mis au monde un enfant.

2.
C’est le Fils de Dieu lui-même
qui repose en ce lieu.
Connaissez-vous la nouvelle ?
Il est né le Roi des cieux !
Mater ora filium,
ut post hoc exilium
nobis donet gaudium,
beatorum omnium.

3.
Trois rois vers lui s’avancent
portant or, myrrhe et encens.
Ils s’inclinent en silence
et déposent leurs présents.
Mater ora filium,
ut post hoc exilium
nobis donet gaudium,
beatorum omnium.

4.
Amis, rendons hommage
à ce Roi nouveau-né,
que la Vierge douce et sage
cette nuit nous a donné.
Mater ora filium,
ut post hoc exilium
nobis donet gaudium,
beatorum omnium.