QUE LA LUMIÈRE CHANTE
PARTOUT ET EN TOUT LIEU
QUE CHAQUE FLAMME DANSE
VIVE L’AMOUR DE DIEU

1.
C’est dès le premier jour
Qu’elle a été créée
Depuis, ce premier jour
Sans fin elle est donnée
De nuit dans un berceau
Dieu nous l'a déposée
C’est le plus grand cadeau
Venu nous éclairer

2.
Si tu sais chaque jour
La donner sans compter
Ces moments chaque jour
Vont pouvoir s'enflammer
Rien n’est jamais plus beau
Que de les partager
Et le plus beau cadeau
En est sa gratuité

Refrain final
QUE LA LUMIÈRE CHANTE
PARTOUT ET EN TOUT LIEU
QUE CHAQUE FLAMME DANSE
FEU DE L'AMOUR DE DIEU