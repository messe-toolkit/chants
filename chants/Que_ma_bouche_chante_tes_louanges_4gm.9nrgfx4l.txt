R.
Que ma bouche chante tes louanges,
Que ma bouche chante ton nom !
En présence des anges,
Que toute la terre t'acclame,
Jésus Ô Roi !

1.
Tu as vaincu la mort à jamais!
Oui que tout ton peuple chante tes bienfaits!
Victoire à l'Agneau ! Victoire, alléluia!
Victoire à l'Agneau ! Victoire, alléluia!

2.
Tu sièges en majesté, Roi des rois!
Quelle immensité ton amour pour moi!
Victoire à l'Agneau ! Victoire, alléluia!
Victoire à l'Agneau ! Victoire, alléluia!