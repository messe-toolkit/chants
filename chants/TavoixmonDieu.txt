1.
Ta voix, mon Dieu, a dit mon nom.
Ta joie m’attend dans ta maison.
Ta parole, mon Seigneur est fidèle, Éternelle.

2.
Ta mort, Jésus m’a délivré.
Tu vis en moi, ressuscité.
Ta parole, mon Seigneur est fidèle, Éternelle.

3.
Esprit d’amour, redis ce mot
Qui crée en moi un coeur nouveau.
Ta parole, mon Seigneur est fidèle, Éternelle.