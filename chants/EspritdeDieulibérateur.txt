1.
Esprit de Dieu libérateur,
Tu nous arraches à la torpeur
De l'esclavage.
Par toi nous sommes délivrés,
Un monde neuf est enfanté
Sur ton passage.

2.
Les fils de Dieu viendront au jour,
Tenant de toi les mots d'amour :
« Dieu notre Père ! »
Esprit d'en-haut, tu nous conduis
Sur le chemin de Jésus-Christ
Vers la lumière.

3.
Louange à toi qui es Seigneur !
Tu viens en nous chasser la peur
Par ton audace,
Et tu nous mènes à tout donner
De nos instants d'humanité :
Qu'ils rendent grâce !