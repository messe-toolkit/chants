1.
Il est invité,
pour le déjeuner
c’est un nouvel ami,
mais pour le moment,
on l’attend !

R.
DANS MA TETE IL FAIT SOLEIL
ON ATTEND LA FÊTE
DANS MA TETE IL FAIT SOLEIL
ET LE TEMPS S’ARRÊTE

2.
Pour l’anniversaire
de mon petit frère
on a tout préparé…
il est impatient,
il attend !

3.
Juste avant Noël,
nos coeurs ont des ailes,
Jésus revient chez nous,
car Jésus va venir
Ce petit enfant,
on l’attend !