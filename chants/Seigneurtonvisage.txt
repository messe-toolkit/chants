SEIGNEUR TON VISAGE, NOUS L’AVONS DESSINÉ
SEIGNEUR TON VISAGE, C’EST CELUI DE TOUS LES CONDAMNÉS

1.
Cet homme qu’on arrête c’est Toi à Gethsémani
Ce prisonnier qu’on frappe c’est Toi aux mains des soldats
Celui que l’on torture pour le faire parler
Et que, sans un murmure on laisse supplicier
C’est son calvaire à lui, c’est ta souffrance à Toi.

2.
Ces hommes qu’on supprime c’est Toi qu’on a rejeté
L’innocent qu’on maltraite c’est Toi qu’on a flagellé
Tous ces gens qui demeurent au fond de leur prison
Et tous ceux que l’on chasse bien loin de leur maison
Dans leur regard perdu c’est ton regard à Toi.

3.
Cet homme qu’on va pendre sans même l’avoir entendu
C’est Toi que l’on va prendre sans même t’avoir défendu
Celui qu’on humilie dont on raille la foi
C’est Toi qu’on crucifie couronné comme un roi
Sur son visage à lui, les larmes sont de Toi.