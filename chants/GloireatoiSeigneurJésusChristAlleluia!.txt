1.
Gloire à Toi, Seigneur Jésus Christ, ALLÉLUIA !
Ta parole éclaire nos vies, ALLÉLUIA !

2.
Fils de Dieu présent parmi nous, ALLÉLUIA !
Tu nous dis la voie de l’amour, ALLÉLUIA !

3.
Bienheureux qui prend ton chemin, ALLÉLUIA !
Tu le mènes à l’aube sans fin, ALLÉLUIA !

4.
Tes combats nous ont relevés, ALLÉLUIA !
Par ta croix tu viens nous sauver, ALLÉLUIA !

5.
Premier-Né de tous les vivants, ALLÉLUIA !
Tu nous donnes un coeur de croyant, ALLÉLUIA !