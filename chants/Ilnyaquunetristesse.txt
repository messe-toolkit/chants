« IL N’Y A QU’UNE TRISTESSE, C’EST DE NE PAS ÊTRE DES SAINTS ».
« IL N’Y A QU’UNE TRISTESSE, C’EST DE NE PAS ÊTRE DES SAINTS ». (Léon Bloy)

1.
N’aie pas peur de la sainteté,
n’aie pas peur de viser haut.

2.
N’aie pas peur de te laisser aimer
et purifier par Dieu.

3.
N’aie pas peur de te laisser guider
par l’Esprit Saint.