19e dimanche du temps ordinaire – B

GOÛTEZ ET VOYEZ
COMME EST BON LE SEIGNEUR !

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Magnifiez avec moi le Seigneur,
exaltons tous ensemble son nom.
Je cherche le Seigneur, il me répond :
de toutes mes frayeurs, il me délivre.

3.
Qui regarde vers lui resplendira,
sans ombre ni trouble au visage.
Un pauvre crie ; le Seigneur entend :
il le sauve de toutes ses angoisses.

4.
L’ange du Seigneur campe alentour
pour libérer ceux qui le craignent.
Goûtez et voyez : le Seigneur est bon !
Heureux qui trouve en lui son refuge !

20e dimanche du temps ordinaire – B

GOÛTEZ ET VOYEZ
COMME EST BON LE SEIGNEUR !

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Saints du Seigneur, adorez-le :
rien ne manque à ceux qui le craignent.
Des riches ont tout perdu, ils ont faim ;
qui cherche le Seigneur ne manquera d’aucun bien.

3.
Venez, mes fils, écoutez-moi,
que je vous enseigne la crainte du Seigneur.
Qui donc aime la vie
et désire les jours où il verra le bonheur ?

4.
Garde ta langue du mal
et tes lèvres des paroles perfides.
Évite le mal, fais ce qui est bien,
poursuis la paix, recherche-la.

21e dimanche du temps ordinaire – B

GOÛTEZ ET VOYEZ
COMME EST BON LE SEIGNEUR !

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Le Seigneur regarde les justes,
il écoute, attentif à leurs cris.
Le Seigneur affronte les méchants
pour effacer de la terre leur mémoire.

3.
Malheur sur malheur pour le juste,
mais le Seigneur chaque fois le délivre.
Il veille sur chacun de ses os :
pas un ne sera brisé.

4.
Le mal tuera les méchants ;
ils seront châtiés d’avoir haï le juste.
Le Seigneur rachètera ses serviteurs :
pas de châtiment pour qui trouve en lui son refuge.

4e dimanche de Carême – C

GOÛTEZ ET VOYEZ
COMME EST BON LE SEIGNEUR !

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Magnifiez avec moi le Seigneur,
exaltons tous ensemble son nom.
Je cherche le Seigneur, il me répond :
de toutes mes frayeurs, il me délivre.

3.
Qui regarde vers lui resplendira,
sans ombre ni trouble au visage.
Un pauvre crie ; le Seigneur entend :
il le sauve de toutes ses angoisses.