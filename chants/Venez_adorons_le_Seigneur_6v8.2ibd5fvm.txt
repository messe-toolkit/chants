R.
Venez, adorons le Seigneur,
Venez et chantons notre joie,
Pour le Dieu de notre salut,
Pour le Dieu de notre salut.

1.
Avançons devant lui
Avec des chants de louanges,
Faisons retentir pour lui l’acclamation,
\s
Que jaillissent en nos cœurs
Des hymnes d'allégresse. (2x)

2.
C'est lui qui a formé
Les profondeurs de la terre,
A lui les sommets et tous les océans,
\s
Tout nous vient de sa main
Comme un don de sa gloire. (2x)

3.
Venez, prosternons-nous
Humblement devant sa face,
A genoux devant le Dieu qui nous a faits,
\s
Car c’est lui notre Dieu
Et nous sommes son peuple. (2x)

4.
Ecoutez, aujourd’hui
La voix de Dieu nous appelle,
Ne détournons pas nos cœurs de son amour,
\s
Entrons dans son repos
Et goûtons sa présence. (2x)