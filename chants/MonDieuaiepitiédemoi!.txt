1.
Je suis partie à la guerre…
pourquoi ?
Sens-tu la violence tout au fond de moi ?
Moi qui brûlais d’être ta voix
Je suis forcée de me taire
Ils ne savent pas qui je suis
Leurs yeux me dévisagent et me salissent
Ils ont crié : « Voilà l’ennemi ! »
Je suis parfaite pour un sacrifice
Ils m’ont dit : « Tu es étrangère
Tu es sur nos terres ! »

ILS M’ONT FAIT TOMBER GENOUX À TERRE
ILS M’ONT COUVERT LE VISAGE DE LEUR.
POUSSIÈRE
J’AI SOIF, ILS S’ENIVRENT
J’AI FROID, ILS S’ENLACENT
J’AI PEUR, ILS ME CHASSENT
MON DIEU, AIE PITIÉ DE MOI !

2.
Je suis ici prisonnière…
et toi ?
Pourquoi ce silence ? Montre-moi le ciel !
Regarde, ils ont brisé mes ailes
Je ne vois plus la lumière
Ils ne cessent de chanter ton nom
Tandis qu’ils me bousculent et me maudissent
J’ai si mal, je perds la raison
Ils me conduisent vers un précipice
Je suis folle, je suis étrangère
Exilée, sans terre.

3.
J’ai mis un pied en enfer…
pourquoi ?
Privée de regards, de souffle et d’envie
Usée, je me suis endormie
Aux pieds de mes adversaires
Que font-ils de leur propre vie ?
Drogués, nourris de peur, couverts d’épines
Ils s’enterrent sans le moindre cri
Obstinément, ils me piétinent
Je suis vide, entourée de pierres
Condamnée aux fers.