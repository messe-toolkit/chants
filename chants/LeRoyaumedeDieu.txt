R.
Cherchez d'abord le Royaume de Dieu,
Et votre vie sera réussie.
Cherchez d'abord le Royaume de Dieu,
Vous trouverez le bonheur.

1.
(Il vient)
Le Royaume ressemble à la graine
Qui grandit, dans le sol, doucement.
Elle pousse, elle se donne de la peine,
Pour devenir un arbre très grand.
Ainsi, mes amis, un jour vous verrez
Les gens de partout se rassembler ;
Comme des oiseaux, dans cet arbre si beau
Qui sera le monde nouveau.

2.
(Il est déjà là)
Regardez bien. Il est déjà là.
Le Seigneur nous invite à entrer.
« Tout est prêt. Venez à mon repas. »
Mais beaucoup cherchent à s'excuser.
Alors, ce sont les pauvres et les malheureux
Qu'il invite et qu'il envoie chercher
Pour dire : « Il est là, le Royaume de Dieu,
Quand l'amour vient vous rassembler. »

3.
(Il est devant)
Le Royaume, on l'espère, on l'attend,
En gardant notre lampe allumée.
Il se construit avec nos talents,
Qui grandissent en étant partagés.
Espérons le jour où Jésus viendra
Pour nous dire : « Entrez dans ma joie.
Vous qui, dans les pauvres, m'avez rencontré,
Le Royaume vous est donné. »