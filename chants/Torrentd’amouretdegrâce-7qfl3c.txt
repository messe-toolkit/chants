1.
Torrents d’amour et de grâce,
Amour du Sauveur en croix
A ce grand fleuve qui passe,
Je m’abandonne et je crois.

R.
Je crois à ton sacrifice,
O Jésus, Agneau de Dieu,
Et couvert par ta justice,
J’entrerai dans le saint lieu.

2.
  Ah! Que partout se répande
Ce fleuve à la grande voix;
Que tout l’univers entende
L’appel qui vient de la croix.

3.
Que toute âme condamnée
Pour qui tu versas ton sang,
Soit au Père ramenée
Par ton amour tout-puissant.

 