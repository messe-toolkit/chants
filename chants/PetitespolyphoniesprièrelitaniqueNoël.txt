TEMPS DE NOËL
Joie au ciel, paix sur la terre !

SAINTE FAMILLE ET CARÊME
Par ton obéissance, Jésus, sanctifie-nous.

FÊTES ET CARÊME
Servez le Seigneur dans l'allégresse !

ÉPIPHANIE ET PRIÈRE DU MATIN
Lumière de lumière, illumine ce jour !
BAPTÊME DU CHRIST ET CARÊME
Ô Christ, mets sur nous ton Esprit !