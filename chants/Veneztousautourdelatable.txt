VENEZ TOUS AUTOUR DE LA TABLE!
JÉSUS RASSEMBLE SES AMIS
VOICI LE PAIN QUE L’ON PARTAGE
VOICI LE PAIN QUI DONNE VIE.

1.
Le Pain de Dieu, c’est Toi, Jésus
Je me souviens de tes paroles :
« Prenez et mangez, ceci est mon corps »
Entre nos mains, tu es le pain.

2.
Le Pain de Dieu, c’est Toi, Jésus
Je me souviens de tes paroles :
« Laissez les enfants venir près de moi »
Alors je viens c’est mon chemin.

3.
Le Pain de Dieu, c’est Toi, Jésus
Je me souviens de tes paroles :
« Heureux les coeurs purs car ils verront Dieu »
Ouvre mes yeux, rends-moi heureux.

4.
Le Pain de Dieu, c’est Toi, Jésus
Je me souviens de tes paroles :
« Je suis avec vous toujours et partout »
Reste avec moi, tu es ma joie.

5.
Le Pain de Dieu, c’est Toi, Jésus
Je me souviens de tes paroles :
« Ils sont pardonnés tes nombreux péchés »
Voici mon coeur, je n’ai plus peur.