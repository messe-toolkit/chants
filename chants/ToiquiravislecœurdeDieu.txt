Toi qui ravis le cœur de Dieu
Et qui l’inclines vers la terre,
Marie, tu fais monter vers lui
Ta réponse en offrande.

Toi qui reçois l’appel de Dieu
Comme une terre la semence,
Tu laisses prendre corps en toi
L’espérance nouvelle.

L’homme a perdu la joie de Dieu
En refusant la ressemblance ;
Par toi le Fils nous est donné,
Qui nous rend à son Père.

Vierge bénie qui porte Dieu,
Promesse et gage de l’alliance,
L’amour en toi rejoint nos vies
Et les prend dans la sienne.