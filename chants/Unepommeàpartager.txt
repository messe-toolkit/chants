1.
Une pomme
Rouge comme
Les bonnes joues de mon voisin.
Une pomme
Rouge et bonne
Je la prends dans ma main.

2.
Une pomme
Rouge et bonne
Je la mets dans ta main.
Cette pomme
Je te la donne
Car tu es mon copain.

3.
Tu la goûtes
Me la redonnes ;
On la croque à pleines dents.
Qu’elle est bonne
Cette pomme !
Quand on partage on est content.