CHAQUE SILENCE EST COMME UNE FLEUR.
QUI NAÎT DOUCEMENT AU FOND DE MON COEUR,
L’EAU, LE SOLEIL, C’EST TOI SEIGNEUR.
TU FAIS TOUT GRANDIR POUR MON BONHEUR.

1.
Des gouttes de Joie, toutes en éclat
Se déposent au fond de moi
Quand je me tais, je peux écouter
Ce que Toi Seigneur tu veux me donner.

2.
Des gouttes de Paix, si colorées
Se déposent au fond de moi
Quand je me tais, je peux écouter
Ce que Toi Seigneur tu veux me donner.

3.
Des gouttes d’Amour, à volonté
Se déposent au fond de moi
Quand je me tais, je peux écouter
Ce que Toi Seigneur tu veux me donner.