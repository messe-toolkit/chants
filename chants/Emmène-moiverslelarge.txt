1.
Il vient un jour
Où l’on se dit :
« Moi qui cherche l’amour,
Où va ma vie ? »
J’ai tant besoin d’une flamme
Qui me guide en pleine nuit
Tu connais si bien mon âme
Seigneur, fais-moi découvrir qui je suis.

EMMÈNE-MOI VERS LE LARGE
RISQUER MA VIE AVEC TOI
JE METTRAI LE CAP SUR D’AUTRES RIVAGES
MALGRÉ LA TEMPÊTE OU LE CALME PLAT
JE METS MA CONFIANCE EN TOI.

2.
Tant de chemins
Mènent à la Vie
Il me faudra demain
Avoir choisi
Je porte en moi tant de doutes
Comment faire le premier pas
J’ai peur de prendre la route
Seigneur, délivre mon coeur de ce poids.

3.
J’ouvre les yeux
Sur mon parcours
Pour y chercher le feu
De ton amour
Lorsque les vagues sont fortes
Tu tiens la barre avec moi
Les jours où le vent me porte
Seigneur, tu souffles au plus profond de moi.

4.
J’entends ta voix
Qui m’interpelle
Tu me promets l’éclat
D’une vie nouvelle
Je t’offre ma soif de vivre
Mes désirs seront les tiens
Je fais le choix de te suivre
Seigneur, aujourd’hui je crois en demain.

5.
Sous ton soleil
Ma vie s’éclaire
Je grandis, je m’éveille
Tu me libères
Comme un père, tu prends patience
Lorsque parfois je m’égare
Et malgré mon inconstance
Seigneur, tu es au coeur de mon histoire.