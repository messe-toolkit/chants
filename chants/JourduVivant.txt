1.
Jour du Vivant pour notre terre !
Le fruit que Dieu bénit
mûrit en lumière :
Soleil déchirant la nuit !

2.
Jour du Vivant sur notre histoire !
Le corps, hier meurtri,
rayonne sa gloire :
L’amour a brisé la mort !

3.
Jour du Vivant sur tout exode !
De l’eau et de l’Esprit
renaissent les hommes :
Chacun porte un nom nouveau !

4.
Jour du Vivant, si loin, si proche !
Le vin nous est servi,
prémices des noces :
La joie du Royaume vient !

5.
Jour du Vivant offert au Souffle !
Le feu soudain a pris,
créant mille sources :
Le monde rend grâce à Dieu !