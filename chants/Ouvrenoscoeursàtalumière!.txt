1.
O Toi, le Dieu de Jésus-Christ,
Ouvre nos coeurs à ta lumière !
Toi, notre Père dans sa gloire,
Ouvre nos coeurs à ta lumière !
Abreuve-nous de ta sagesse,
Ouvre nos coeurs à ta lumière !
Donne vigueur et donne vie,
Ouvre nos coeurs à ta lumière !

TOI, NOTRE DIEU, TOI NOTRE PÈRE,
ENVOIE SUR NOUS TON ESPRIT.
OUVRE NOS COEURS À TA LUMIÈRE,
SUR LES SENTIERS DE JÉSUS-CHRIST.

2.
Dieu tout-puissant, regarde-nous,
Ouvre nos coeurs à ta lumière !
Donne ta force à notre monde,
Ouvre nos coeurs à ta lumière !
Pour partager ton héritage,
Ouvre nos coeurs à ta lumière !
Pour mieux comprendre ton appel,
Ouvre nos coeurs à ta lumière !

3.
Envoie sur nous ton Esprit-Saint,
Ouvre nos coeurs à ta umière !
Dans ton amour qui nous libère,
Ouvre nos coeurs à ta lumière !
Avec Jésus qui nous rend frères,
Ouvre nos coeurs à ta lumière !
Loué sois-tu, Seigneur de vie,
Ouvre nos coeurs à ta lumière !