1.
Peuples de la terre, acclamez votre Dieu,
fêtez-le, tous les pays,
alléluia, alléluia, alléluia, alléluia !

R.
DIEU D'AMOUR EN JESUS CHRIST,
ALLE, ALLELUIA !
DIEU FIDELE POUR LA VIE,
ALLE, ALLELUIA, ALLE, ALLELUIA !

2.
Peuples sans frontières, exultez pour son nom,
Dieu de paix vous garde unis,
alléluia, alléluia, alléluia, alléluia !

3.
Peuples de sauvés, rendez gloire au Dieu fort,
louez-le, chantez pour lui,
alléluia, alléluia, alléluia, alléluia !

4.
Toutes les nations, bénissez le Seigneur,
son amour est infini,
alléluia, alléluia, alléluia, alléluia !