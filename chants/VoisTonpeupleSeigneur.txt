Stance
Vois ton peuple, Seigneur,
Vois ton peuple en attente,
Montre-nous le visage de ton Christ !
Voici qu’il vient,
Plus puissant que Jean Baptiste ;
Il nous baptisera dans l’Esprit et le feu.
Tournés vers lui, nous le prions.

Refrain
Baptisé dans les eaux du Jourdain,
(bis) Fils de Dieu, donne-nous ton Esprit ! (bis)

Versets

1.
Baptisé au milieu de ton peuple,
Jésus Christ, tu te tiens en prière.
Du ciel ouvert, l’Esprit Saint descend sur toi.

2.
Il descend, tel un vol de colombe,
Et du ciel une voix retentit :
« C’est toi mon Fils : aujourd’hui je t’ai engendré. »

R.

Stance et refrain