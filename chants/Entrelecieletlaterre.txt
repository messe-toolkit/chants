« Les temps sont accomplis. »
Entre le ciel et la terre
le Créateur signe l’acte d’Alliance :
mémoire d’amour et de tendresse.
pour toutes les générations.
« Seigneur, tu es le Dieu qui nous sauve ! »
Le sourire de Dieu, tout armé de patience,
montre aux humbles le chemin
entre la terre et le ciel.
« Seigneur, enseigne-moi tes voies ! »
Le Règne est tout proche,
arche nouvelle en ses commencements.
Toutes voiles dehors au souffle de l’Esprit,
la traversée pascale dans les eaux du baptême,
aborde la quarantaine d’épreuve et de délivrance.
« Seigneur, fais-moi connaître ta route ! »
Jésus est Seigneur,
nouvel Adam accordé au ciel comme à la terre.
Dans sa chair, le Juste est mort pour les péchés.
Dans l’Esprit il est rendu à la vie.
« Seigneur, tu es le Dieu qui nous sauve ! »
Ses frères,
jadis prisonniers de la mort,
aujourd’hui sont vainqueurs avec lui dans sa résurrection.
« Convertissez-vous ! Croyez à la Bonne Nouvelle ! »