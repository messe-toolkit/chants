COMME L’AVEUGLE, JE VIENS VERS TOI, SEIGNEUR,
REPENTANT ET CRIANT DANS UNE JOIE SANS FIN :
“GLOIRE À TOI, LUMIÈRE QUI ILLUMINE NOTRE NUIT !”

1.
(I V.)
Dieu qui a dit : “Que du sein de la ténèbre brille la lumière !”
Voici qu’il a fait briller sa lumière en nos coeurs !
Je suis la lumière du monde !
Celui qui fait la vérité vient à la lumière.

2.
Le Seigneur t’a pris par la main,
il t’a désigné comme alliance du peuple et lumière des nations
pour ouvrir les yeux des aveugles
et faire sortir de prison ceux qui habitent les ténèbres.

3.
(Ld.)
Celui qu’on appelle Jésus a fait de la boue,
il m’a enduit les yeux et m’a dit :
“Va te laver à Siloë, à la piscine de l’Envoyé !”
Alors j’y suis allé, je m’y suis lavé, et j’y vois !

4.
Dieu est lumière, en lui il n’est point de ténèbres ;
éveille-toi, ô toi qui dors, et le Christ t’illuminera !
Car c’est toi, Seigneur, mon flambeau !
Ô mon Dieu, illumine ma ténèbre !

5.
(II V)
“Crois-tu au Fils de l’homme ?
Tu le vois, c’est lui qui te parle.”
Alors, il dit : “Je crois, Seigneur !”
Et se prosternant, il l’adora.

6.
Je ferai marcher les aveugles sur mes sentiers
et je changerai devant eux les ténèbres en lumière,
par l’amour du coeur de notre Dieu,
soleil levant qui est venu nous visiter !

7.
(Dox.)
Gloire au Père des lumières de qui vient tout don parfait,
gloire au Fils de l’homme, lumière née de la lumière,
gloire à l’Esprit qui ouvre les yeux de notre coeur
à l’oeuvre du salut, quand Jésus nous est envoyé !

8.
(Mag.)
Maintenant, tu peux laisser s’en aller ton serviteur
en paix, selon ta parole ;
car mes yeux ont vu ton salut,
lumière des nations et gloire d’Israël !