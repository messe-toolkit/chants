ÉCOUTE MA FILLE (psaume 44)

PLEINE DE GRÂCE, RÉJOUIS-TOI)
TU ES MÈRE DES VIVANTS)   bis

1.Écoute ma fille, regarde, et tends l’oreille
Oublie ton peuple et la maison de ton Père,
Le roi sera séduit par ta beauté !

2.
Fille de rois, elle est là dans sa gloire,
Vêtue d’étoffes d’or,
On la conduit toute parée vers le roi !

3.À la place de tes Pères, se lèveront des fils
Sur toute la Terre, tu feras d'eux des pinces,
Je ferai vivre ton nom pour les âges des âges !