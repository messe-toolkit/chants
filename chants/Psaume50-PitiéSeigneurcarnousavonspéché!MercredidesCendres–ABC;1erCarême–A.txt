PITIÉ, SEIGNEUR, CAR NOUS AVONS PÉCHÉ !

1.
Pitié pour moi, mon Dieu, dans ton amour,
selon ta grande miséricorde, efface mon péché.
Lave-moi tout entier de ma faute,
purifie-moi de mon offense.

2.
Oui, je connais mon péché,
ma faute est toujours devant moi.
Contre toi, et toi seul, j’ai péché,
ce qui est mal à tes yeux, je l’ai fait.

3.
Crée en moi un coeur pur, ô mon Dieu,
renouvelle et raffermis au fond de moi mon esprit.
Ne me chasse pas loin de ta face,
ne me reprends pas ton esprit saint.

4.
Rends-moi la joie d’être sauvé ;
que l’esprit généreux me soutienne.
Seigneur, ouvre mes lèvres,
et ma bouche annoncera ta louange.