1.
Dis-moi, Marie, la route à prendre pour trouver la Vie.
Dis-moi, Marie, le chemin à suivre pour vivre dans la vérité.

CONTEMPLE ET VOIS, ÉCOUTE ET SUIS-MOI.

2.
Toi, Marie, qui envahie par l’Esprit
Répondis “Oui” au Père.

CONTEMPLE ET VOIS, ÉCOUTE ET SUIS-MOI.

3.
Dis-moi, comment Jésus te fut donné,
Dis-moi comment tu vis avec lui.

CONTEMPLE ET VOIS, ÉCOUTE ET SUIS-MOI.

4.
Le Père, personne ne l’a jamais vu
Sinon le Fils qui nous l’a fait connaître.

CONTEMPLE ET VOIS, ÉCOUTE ET SUIS-MOI.

5.
Laisse-toi prendre par l’Esprit,
Son silence et sa paix
Et tu verras dans la plénitude d’amour
Jésus tout transparent à son Père et notre Père.

CONTEMPLE ET VOIS, ÉCOUTE ET SUIS-MOI.