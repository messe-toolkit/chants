SEMEURS D'ESPERANCE
POUR L'AVENIR
SEMEURS D'ESPERANCE
POUR MIEUX BATIR
SEMEURS D'ESPERANCE
POUR PARTAGER
SEMEURS D'ESPERANCE
POUR LE TEMPS D'AIMER.

1.
Demain matin dès l'aube,
Aux premières lueurs
Nous nous mettrons en marche
Pour un monde meilleur
Au-delà des frontières,
Sur tous les continents
Pour dire à tous nos frères,
Les petits et les grands
Que nous venons pour annoncer
Qu'il est venu le temps d'aimer.

2.
Annoncer la parole
Qui vient nous libérer
L'amour qui nous rassemble
Et qui nous fait chanter
Sur les terres en friche,
Les terres désertées
L'espoir qui nous fait vivre,
Jésus ressuscité
Pour que les enfants d'aujourd'hui
Puissent donner un sens à leur vie.

3.
Pour faire une récolte
Il faut d'abord semer
Et puis savoir attendre
Avec humilité
Nous les enfants du monde
On a la volonté
De vivre le partage,
De parler, d'écouter
Pour que chacun se sente bien
Et puisse trouver son chemin.