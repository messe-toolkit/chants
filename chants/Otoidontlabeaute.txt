1.
Ô toi dont la beauté
Rayonne de clarté
À l’ombre de l’Esprit,
Ève nouvelle,
Laisse-nous découvrir
Le mystère de grâce
Où le monde renaît.

2.
Ta foi nous a donné
Celui qu’ont annoncé
Les hommes de l’espoir.
Joie des prophètes,
Dieu façonne en ton corps
Son image éternelle
Et se fait l’un de nous.

3.
Le prix de ton amour
Demeure pour toujours
Caché dans nos moissons.
Mère des hommes,
Tu prépares en secret
Le ferment du Royaume
Et le pain de nos vies.