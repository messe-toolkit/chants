1.
Six jours avant la Pâque
Jésus était à Béthanie
Suivi des ses apôtres
Le lendemain il en partit
Il va sur la route :
Il monte vers Jérusalem
N'ayez plus de doute
Vous verrez le roi d'Israël

2.
Il envoie deux disciples :
« Allez au Mont des Oliviers,
dans un petit village
que l’on appelle Betphagé.
Vous verrez deux bêtes,
l’ânesse et son petit ânon.
Dites à leur maître:
« Notre Seigneur en a besoin ».

3.
Jésus s’assoit sur l’âne,
comme les pauvres et les petits.
Et les enfants l’acclament :
« Hosanna au Fils de David! »
Pour lui faire fête,
ils coupent aux arbres des rameaux
et, de leurs tuniques,
lui font un grand tapis d’honneur.

4.
Il entre dans la ville
et voit le Temple devant lui.
La foule qui l’entoure
joyeusement lui chante et crie :
Béni soit cet homme
qui vient chez nous au nom de Dieu !
Béni soit son règne!
Hosanna au plus haut des cieux !