Refrain :
S’il est vrai qu’un printemps nous ramène les roses
Il y faut malgré tout les soins du jardinier ;
Et qu’importe le temps et qu’importent les choses,
Reconnaître ton pas me permet d’exister.

1.
Car à quoi sert la vie, si la vie est en friche,
Tant de mortes saisons, tant d’épines au coeur ;
Un geste, un souffle, une ombre ou un objet fétiche,
Pour jamais plus bêcher à nos mêmes erreurs.

2.
Cette façon à toi d’apprendre à connaître,
Un bourgeon, une fleur, une feuille jaunie,
De partager ensemble, en silence peut-être,
D’effacer le mot “ fin ” au bas du manuscrit.

3.
Chaque fleur a son nom, sa magie, son mystère ;
Elle attend, tu le sais, surtout, que tu sois là.
Pour faire éclore ainsi du plus humble parterre,
Des choses qui ressemblent à l’estime de soi.