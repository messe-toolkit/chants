ANTIENNE
Allez sur les routes du monde : annoncez le Royaume de Dieu. (Mt 9, 6-7)

R.
Laudate omnes gentes, llaudate Dominum.
Laudate omnes gentes, llaudate Dominum.

Ps 66, 2-3 ; 5 ; 7-8.

Que Dieu nous prenne en grâce et nous bénisse,
Que son visage s'illumine pour nous ;
Et ton chemin sera connu sur la terre,
Ton salut, parmi toutes les nations.

Que les nations chantent leur joie,
Car tu gouvernes le monde avec justice
Tu gouvernes les peuples avec droiture,
Sur la terre tu conduis les nations.

La terre a donné son fruit ;
Dieu, notre Dieu, nous bénit.
Que Dieu nous bénisse,
Et que la terre tout entière l'adore !

Ps 144, 1-2 ; 8-14.

Je t'exalterai, mon Dieu, mon Roi,
Je bénirai ton nom toujours et à jamais !
Chaque jour je te bénirai,
Je louerai ton nom toujours et à jamais.

Le Seigneur est tendresse et pitié,
Lent à la colère et plein d'amour ;
La bonté du Seigneur est pour tous,
Sa tendresse, pour toutes ses œuvres.

Que tes œuvres, Seigneur, te rendent grâce
Et que tes fidèles te bénissent !
Ils diront la gloire de ton règne,
Ils parleront de tes exploits,

Annonçant aux hommes tes exploits,
La gloire et l'éclat de ton règne :
Ton règne, un règne éternel,
Ton empire, pour les âges des âges.

Le Seigneur est vrai en tout ce qu'il dit,
Fidèle en tout ce qu'il fait.
Le Seigneur soutient tous ceux qui tombent,
Il redresse tous les accablés.