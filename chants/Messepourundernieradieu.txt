SAINT LE SEIGNEUR.
DIEU DES VIVANTS
HOSANNA AU PLUS HAUT DES CIEUX!

1.
Sur la terre comme au ciel il donne la vie,
HOSANNA AU PLUS HAUT DES CIEUX!

2.
Béni soit celui qui vient nous prendre avec lui,
HOSANNA AU PLUS HAUT DES CIEUX!
SAINT LE SEIGNEUR…

SAINT LE SEIGNEUR, DIEU DE L’UNIVERS
HOSANNA AU PLUS HAUT DES CIEUX!

1.
Sur la terre comme au ciel éclate sa gloire
HOSANNA AU PLUS HAUT DES CIEUX!

2.
Béni soit celui qui vient au nom du Seigneur
HOSANNA AU PLUS HAUT DES CIEUX!
SAINT LE SEIGNEUR…

1.
Agneau de Dieu, agneau vainqueur,
Toi que la mort n’a pu retenir,
DONNE-LEUR LA VIE ÉTERNELLE!

2.
Agneau de Dieu ressuscité,
Toi qui nous ouvres les portes du ciel,
DONNE-LEUR LA JOIE ÉTERNELLE!