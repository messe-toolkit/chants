1.
Nuit joyeuse de Noël,
Ô nuit de belle fête !
Les bergers de Bethléem
Annoncent la nouvelle :
Ils ont vu l'Emmanuel
Couché dans une crèche.

2.
Par la grâce d'un enfant
Qui vient sourire aux hommes,
Nous voyons fleurir le temps
De la Miséricorde ;
Plus de crainte maintenant :
Voici l'Ami des pauvres !

3.
Vierge, fille d'Abraham
L'Esprit te rend féconde :
C'est par lui que naît de toi
La main qui tient le monde ;
C'est ton Fils qui sèmera
La vie sur notre tombe.

4.
Quelle étoile en cette nuit
Fait luire sa lumière ?
C'est l'étoile du Messie
Chanté par les prophètes !
C'est déjà l'Épiphanie
Du Fils de Dieu le Père !