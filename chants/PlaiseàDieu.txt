Refrain :
Plaise à Dieu Ce moment de silence,
Un p’tit peu Comme un secret d’amour.
Plaise à Dieu, Ce moment si intense
Quand nos yeux Perçoivent enfin le jour.

1.
Comme un feu
Qui renaît et qui danse,
Un ciel bleu
Ou un lever du jour.
Un “adieu”
Qui mène à l’Espérance,
Bienheureux
Tous ces moments trop courts.

2.
Pour tous ceux
Qui “subissent” l’errance,
Avec ceux
Qui appellent au secours,
Malheureux,
De malchance en malchance,
Malgré ce
“Le” recherchent toujours.

3.
Pauvre gueux,
Ou nomade en partance,
Moi, je veux
Sans trompette et tambour ;
Moi, je veux
Sur mes chemins d’errance,
Faire voeux
D’être à LUI sans détour.