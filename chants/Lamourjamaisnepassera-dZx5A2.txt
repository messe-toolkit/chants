R.
L´Amour jamais ne passera,
L´Amour demeurera,
L´Amour, l´amour seul,
La charité jamais ne passera,
Car Dieu est Amour.

1.
Quand j´aurais le don de la science,
Et connaîtrais tous les mystères,
Parlerais-je les langues des anges,
Sans amour, je ne suis rien.

2.
Si je donnais mes biens en aumône,
Si je livrais mon corps aux flammes.
Si ma foi déplaçait les montagnes,
Sans amour, je ne suis rien.

3.
La charité est toujours longanime,
Ne tient pas compte du mal.
La charité se donne sans cesse,
Ne cherche pas son avantage.

4.
La charité ne jalouse personne,
La charité jamais ne s´irrite.
En tout temps elle excuse et espère,
La charité supporte tout.

5.
Un jour, les langues vont se taire,
Les prophéties disparaîtront,
Devant Dieu le Seigneur notre maître,
Seul l´Amour restera.