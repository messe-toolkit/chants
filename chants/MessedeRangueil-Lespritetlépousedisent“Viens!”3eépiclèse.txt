Tu es l’Alpha et l’Oméga, le commencement et la fin.
Toi Esprit éternel, tu donnes la vie à toute chose.
Par ton souffle créateur, tu as fait briller du fond de l’abîme
la beauté de l’univers en son commencement.
C’est pourquoi nous te chantons, nous te bénissons,
nous te rendons grâce, ô Dieu, Esprit-Saint !

L’Esprit et l’Epouse disent « Viens ! »
Que vienne ta grâce, que ce monde passe !
Et tu seras tout en tous !

Viens, et creuse en nous la soif de la vie éternelle !
Viens, et révèle-nous les joies du Royaume qui vient !
Viens, et donne-nous le vêtement de l’immortalité !
Viens, et remplis-nous de l’éclat de ta divinité !
Viens, et conduis-nous vers les noces de l’Agneau !
Viens Esprit consolateur et demeure en nous !