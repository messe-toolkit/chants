R.
SOYONS DANS LA JOIE ET RENDONS GLOIRE À DIEU,
LA MORT N’A PLUS AUCUN POUVOIR.
VOICI LE JOUR QUE FIT LE SEIGNEUR,
JOUR DE RÉSURRECTION,
JOUR DE VIE, JOUR DE JOIE.

1.
Jésus le condamné, Jésus le crucifié est vivant.
C’est bien vrai.

2.
Les apôtres l’ont vu. Ils ont partagé son repas.
C’est bien Lui.

3.
Jésus proclame qu’en Lui toute l’Écriture s’accomplit
et que l’humanité est sauvée.
C’est bien Lui, le Messie.

4.
Jésus annonce la venue de l’Esprit Saint
qui nous revêt de la force d’en haut.
C’est bien Lui l’Esprit d’Amour.

5.
Assemblé en Église, témoins de l’Amour du Père,
du Fils et de l’Esprit Saint.
Car c’est Lui l’Alpha et l’Oméga, le principe et la fin.