REFRAIN 1.
Je crois, Seigneur, tu es source de vie.
Je crois, Seigneur, tu es source de vie.
(Parlé) Je crois en Dieu le Père.
Il a créé le monde et tout ce qui vit sur terre et dans le ciel.
Au sommet de l'Univers, il a fait l'homme à son image.
REFRAIN 2.
Je crois, Seigneur, tu remplis l'Univers.
(Parlé) Je crois au Christ, le fils de Dieu.
Il a partagé notre condition humaine.
Il nous a aimé à en mourir.
Mais son amour a vaincu la mort.
Il est ressuscité et vivant.
REFRAIN 3.
Je crois, Seigneur, ton amour est présent.
(Parlé) Je crois en l'Esprit Saint qui nous donne son Amour.
Je crois en l'Église, une, sainte, catholique et apostolique.
J'attends la résurrection des morts.
REFRAIN 4.
Je crois, Seigneur, tu nous donnes la vie.
Amen ! Amen ! Amen !