R.
Criez la joyeuse nouvelle :
Le Christ est vivant pour toujours.
Dansez, jouez toutes vos musiques,
Le Christ est plus grand que la mort.

1.
Inventez une chanson,
Une chanson pleine de rires,
La vie est délivrée,
La mort est en prison.

2.
Prenez un tambourin,
Un tambourin qui sonne
La mort n´a pas gagné,
Elle n´a plus rien à dire.

3.
Réveillez vos amis,
Vos amis étonnés.
Quelqu'un est parmi vous,
Il vous tend ses deux mains.