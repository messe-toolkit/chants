1.
Elle était ronde, elle était pure
Elle n’y peut rien c’est sa nature
Avec sa longue robe bleutée
Elle était belle à en crever
Elle en a détourné des yeux
Et on ne compte plus les envieux
Ces nostalgiques, qui rêveraient
De retrouver sa beauté passée

2.
Elle avait eu tout plein d’enfants
Épanouis, tous différents
Qu’elle entourait de sa tendresse
Qu’elle nourrissait de sa sagesse
Mais l’un d’entre eux a tout brisé
Car il se croyait plus malin
La croissance de l’humanité
La pousse doucement vers sa fin

LÈVE TON VERRE
CITOYEN DU MONDE
À LA SANTÉ DE TA VIEILLE MÈRE
ELLE ÉTAIT BELLE
ELLE ÉTAIT RONDE
MAIS C’EST DEMAIN
QUE TU L’ENTERRES

3.
Cet enfant-là, oui cet ingrat
N’a d’yeux que pour le profit
Et déborde d’énergie
Pour porter sa mère au plus bas
Il la prive de toutes ses ressources
Et os’rait même s’il le pouvait
Essayer d’la coter en Bourse
Pour se faire encore plus de blé

4.
Elle était ronde, elle était pure
Elle n’y peut rien c’est sa nature
Avec sa longue robe bleutée
Elle était belle à en crever
Mais la beauté ne vaut plus rien
Quand c’est l’argent qui fait la loi
Et quand l’Homme se salit les mains
Il ne se fait que peu de tracas
Ici la conn’rie va bon train
Et pendant que sa mère déraille
Comme son chagrin n’est pas le sien
L’Homme la laisse mourir sur la paille

ELLE APPELLE À L’AIR.
CAR ELLE EST EN DÉTRESSE
ELLE LÈVE LES YEUX AU CIEL
QUI PERD DE SON ÉCLAT…
FUKUSHIMA… EST PASSÉ PAR LÀ…

LÈVE TON VERRE
CITOYEN DU MONDE
À LA SANTÉ DE TA VIEILLE MÈRE
ELLE ÉTAIT BELLE
ELLE ÉTAIT RONDE
MAIS C’EST DEMAIN
QUE TU L’ENTERRES