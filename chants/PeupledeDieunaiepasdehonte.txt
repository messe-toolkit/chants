1.
Peuple de Dieu, n’aie pas de honte,
Montre ton signe à ce temps-ci !
En traversant l’âge du monde,
Cherche ton souffle dans l’Esprit ;
Lève ton hymne à sa puissance,
Tourne à sa grâce ton penchant :
Pour qu’il habite tes louanges
Et soit visible en ses enfants.

2.
Tiens son amour, tiens son épreuve ;
C’est dans la joie qu’il te confie
Toute la charge de son oeuvre
Pour qu’elle chante par ta voix :
Ne te replie pas sur toi-même
Comme si Dieu faisait ainsi !
C’est quand tu aimes que Dieu t’aime,
Ouvre ton coeur, fais comme lui.

3.
Va, puise dans ton héritage
Et sans compter, partage-le ;
Gagne l’épreuve de cet âge,
Porte partout le nom de Dieu !
Qu’il te rudoie, qu’il te réveille :
Tu es son corps, dans son Esprit !
Peuple d’un Dieu qui fait merveille,
Sois la merveille d’aujourd’hui.