1.
Je suis venu pour tous les temps,
Le jour d’hier et l’aujourd’hui,
Le temps présent, le temps passé,
Venez à moi, vous gens pressés,
Venez à moi, vous qui peinez.

QUEL EST TON NOM? QUI DONC ES-TU?
NOUS NE T’AVONS JAMAIS CONNU !

2.
Je suis Jésus, humble de coeur,
Venez à moi, n’ayez pas peur,
Je prends sur moi tous vos fardeaux,
Vous trouverez le vrai repos.

Dans notre coeur, c’est un trou noir,
Que du chagrin, du désespoir,
Dans notre nuit pas de sommeil,
Pas de repos ni de réveil.

Venez à moi, je suis soleil,
Je suis le jour et la clarté,
Je suis chemin de vérité,
Venez à moi, je suis la paix.

QUEL EST TON NOM? QUI DONC ES-TU?
NOUS NE T’AVONS JAMAIS CONNU !

3.
Je suis Jésus, humble de coeur,
Venez à moi, n’ayez pas peur,
Je prends sur moi tous vos fardeaux,
Vous trouverez le vrai repos.

Tout est si lourd sur notre dos,
Et notre coeur vole en morceaux,
Et notre temps part en lambeaux,
Tout dans nos vies est décousu.

Ne craignez pas, je suis Jésus,

NOUS NE T’AVONS JAMAIS CONNU !

4.
Je vous connais, je vous ai vus,
Je suis l’alpha et l’oméga,
Je suis premier, je suis dernier,
Point de départ et d’arrivée.

QUEL EST TON NOM? QUI DONC ES-TU?
NOUS NE T’AVONS JAMAIS CONNU !

5.
Je suis Jésus !

Mais nous roulons à cent à l’heure,
Et notre vie comme une auto,
Comme un été qu’on a ôté
Sur l’autoroute du malheur,
Et notre vie comme un étau
Est resserrée au coin du coeur.

Ralentissez, n’ayez pas peur,
Je suis la Vie, prenez le temps,
Arrêtez-vous, même un instant,
Car c’est pour vous que je m’en viens :
Les bien-portants n’ont pas besoin
De chirurgiens ou de docteurs,
Mais les malades et les pécheurs.
Je suis venu pour le bonheur.

QUEL EST TON NOM? QUI DONC ES-TU?
NOUS NE T’AVONS JAMAIS CONNU !

6.
Je suis Jésus !
Je suis le jour et la clarté,
Je suis chemin de vérité,
Venez à moi, je suis la paix !

Dépêche-toi, rends-nous la joie,
Nos vies s’enfoncent dans la boue,
Tends-nous la main, mets-nous debout,
Reviens guérir nos pieds blessés,
Redresse-nous sur nos sentiers.