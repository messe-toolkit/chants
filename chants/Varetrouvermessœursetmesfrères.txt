R.
VA RETROUVER MES SOEURS ET MES FRÈRES
DANS LE MONDE D’AUJOURD’HUI
VA RETROUVER MES SOEURS ET MES FRÈRES
AU QUOTIDIEN DE TA VIE.

1.
Il y a toujours, toujours, même au bout d’une arrière-cour
À rencontrer quelqu’un qui nous fera grandir
Il n’y a jamais, jamais, pour nos solidarités
De sens unique. Sachons recevoir pour servir.

2.
Il y a toujours, toujours dans l’urgence de l’amour
Pour notre foi à s’enrichir de nos combats
Il n’y a jamais, jamais dans l’aujourd’hui de la paix
À oublier que croire c’est vouloir se donner.

3.
Il y a toujours, toujours une parole de feu
Vivant foyer, qui réchauffe le coeur souffrant
Il n’y a jamais, jamais pour la parole de Dieu
D’homme perdu, Jésus est le frère des vivants.

4.
Il y a toujours, toujours la couleur d’un nouveau jour
Qui transparaît aux vitraux d’une vie brisée
Il n’y a jamais, jamais d’humaine fragilité
Qui ne devienne un ferment de fraternité.

5.
Il y a toujours, toujours des petits moments d’amour
À partager dans les recoins du temps présents
Il n’y a jamais, jamais d’espoir affichant complet
L’humanité a besoin de tous ses enfants.

6.
Il y a toujours, toujours, à grandir avec amour
À côtoyer, nos soeurs et frères fragilisés
Il n’y a jamais, jamais de dons sans altérité
Nos différences nourrissent notre humanité.

7.
Il y a toujours, toujours de nouveaux allers-retours
Parmi nos frères, nous sommes heureux d’être envoyés
Il n’y a jamais, jamais pour la justice et la paix
De vaines causes, pour un monde réconcilié.

8.
Il y a toujours, toujours un chemin pour l’Évangile
Dans notre écoute, notre présence et notre action
Il n’y a jamais, jamais d’épis de blé trop fragiles
Notre Seigneur est le maître de la moisson.

9.
Il y a toujours, toujours à donner un peu de soi
Tous nos projets avancent au pas d’un coeur qui bat
Il n’y a jamais, jamais de vrai don sans écouter
Frères et soeurs sont partenaires de nos combats.