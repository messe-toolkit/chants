1.
Nous avançons sur des terres incertaines
Dans le doute et la peur du temps qui vient
Nous traversons trop d’épreuves inhumaines
Dans la nuit, les brouillards et les chagrins :

COMME UNE ÉTOILE
ÉCLAIRE NOS YEUX,
GUIDE NOS PAS, MARIE.

2.
Pourquoi le mal, la terreur et les violences
Qui oppriment des millions d’innocents ?
L’humanité semble en perte d’espérance
Quel soleil brillera sur nos printemps ?

3.
Qu’à chaque instant jusqu’à l’autre bout du monde
S’accomplissent des gestes fraternels.
Que les enfants de partout forment une ronde
Une ronde aux couleurs de l’arc-en-ciel.