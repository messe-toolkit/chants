1.
Les collecteurs d’impôts et de nombreux pécheurs
S’approchaient de Jésus, l’écoutaient d’tout leur coeur.
Les pharisiens, les scribes critiquaient, murmuraient :
“Il accueille les pécheurs et, chez eux, va manger.”

2.
“Lequel d’entre vous tous, s’il possède cent brebis
Et qu’une seule d’entre elles, un beau jour, s’est enfuie
Ne laisse dans le désert celles qui n’ont pas bougé,
Pour chercher l’égarée, jusqu’à ce qu’il l’ait trouvée ?”

Refrain 1 :
QUI ES-TU ? QUI ES-TU ? QUI ES-TU JÉSUS ?
POURQUOI TANT DE CHEMIN POUR UNE BREBIS PERDUE ?
QUI ES-TU ? QUI ES-TU ? QUI ES-TU JÉSUS ?
TU NOUS CHERCHES TOUJOURS, JOUR APRÈS JOUR.

3.
“Quand il l’a retrouvée, tout joyeux, il la met
Autour de ses épaules, pour mieux la ramener.
Rentré à la maison, il appelle ses amis :
“Venez vous réjouir, j’ai retrouvé ma brebis !”

4.
“Ainsi, je vous le dis : il y a plus de joie
À la maison du Père, dans mon Royaume à moi,
Le jour où un pécheur se lève et qu’il revient
Que pour ceux qui restent là, mais n’ont besoin de rien.”

5.
“Écoutez ! Si une femme, qui a dix pièces d’argent
Vient à en perdre une seule, en faisant du rangement,
N’va-t-elle pas allumer, chercher et balayer,
Jusqu’à ce qu’elle ait trouvé sa précieuse monnaie ?”

6.
Elle invite ses voisines, quand elle l’a retrouvée,
Pour qu’elles se réjouissent de ce qui s’est passé.
Ainsi, y’a grande joie au ciel, je vous le dis,
Le jour où un pécheur, un seul, se convertit.

Refrain 2 :
QUI ES-TU ? QUI ES-TU ? QUI ES-TU JÉSUS ?
POURQUOI TANT DE TRACAS POUR UNE PIÈCE PERDUE ?
QUI ES-TU ? QUI ES-TU ? QUI ES-TU JÉSUS ?
TU NOUS CHERCHES TOUJOURS, JOUR APRÈS JOUR.