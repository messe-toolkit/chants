« Je veux te connaître, toi qui me connais,
te connaître comme je suis connu.
Force, pénètre-moi.
Fais de moi ce que tu veux. »