1.
Je peux faire cent fois le tour de l’univers
Et apprendre l’hébreu à l’endroit, à l’envers,
Je peux parler de tout, tenir la vérité
Dévoiler des mystères, tout savoir du passé

S’IL ME MANQUE L’AMOUR, L’AMOUR ET SA CONFIANCE,
S’IL ME MANQUE L’AMOUR, L’AMOUR QUI PREND PATIENCE,
S’IL ME MANQUE L’AMOUR, JE NE SUIS RIEN,
S’IL ME MANQUE L’AMOUR, JE NE SUIS RIEN.

2.
Je peux suivre la loi et ses enseignements
À la lettre écouter tous les commandements
Je peux crier ma foi aux cailloux du désert
Ou planter des forêts au milieu de la mer

S’IL ME MANQUE L’AMOUR, L’AMOUR ET SES BLESSURES,
S’IL ME MANQUE L’AMOUR, L’AMOUR QUI TRANSFIGURE,
S’IL ME MANQUE L’AMOUR, JE NE SUIS RIEN,
S’IL ME MANQUE L’AMOUR, JE NE SUIS RIEN.

3.
Je peux tout sacrifier, donner jusqu’à ma vie
Partager tous mes biens aux mendiants, aux petits
Je peux croire en demain, aux moissons du bonheur
Et me battre aujourd’hui pour un monde meilleur,

S’IL ME MANQUE L’AMOUR, L’AMOUR ET SA JUSTICE,
S’IL ME MANQUE L’AMOUR, L’AMOUR QUI REND SERVICE,
S’IL ME MANQUE L’AMOUR, JE NE SUIS RIEN,
S’IL ME MANQUE L’AMOUR, JE NE SUIS RIEN.