Seigneur, nous voici rassemblés
Autour de cette table,
Pour partager le pain que nous donne ta main.
Nous implorons sur nous
Ta bénédiction,
Au nom du Père, au nom du Fils,
Au nom du Saint Esprit.
Amen.