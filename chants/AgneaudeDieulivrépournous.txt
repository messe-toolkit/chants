1.
Agneau de Dieu, livré pour nous, tu viens relever le monde.
Miserere, miserere, miserere, nobis !

2.
Agneau de Dieu, vainqueur du mal, tu viens libérer le monde.
Miserere, miserere, miserere, nobis !

3.
Agneau de Dieu, ressuscité, tu viens transformer le monde.
Dona nobis, dona nobis, dona nobis pacem !

1.
Du, Gottes Lamm, du starbst für uns und nimmst auf dich alle Schuld der Welt.
Miserere, miserere, miserere, nobis !

2.
Du, Gottes Lamm, bezwingst das Leid, zu befrei'n die Welt vom Bösen.
Miserere, miserere, miserere, nobis !

3.
Du, Gottes Lamm, besiegst den Tod, gibst uns Hoffnung, daß wir auferstehn.
Dona nobis, dona nobis, dona nobis pacem !