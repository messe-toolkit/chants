1.
Le jour où tu verras ton corps se briser
Pour que vive ton frère,
Tu pourras reconnaître ton Seigneur
Au partage du pain ;
Tu pourras regarder l´Agneau de Dieu
Qui enlève le péché du monde.

2.
Le jour où tu auras le coeur transpercé
Par les cris de ton frère,
Tu pourras reconnaître ton Seigneur
Au partage du pain
Tu pourras supplier l´Agneau de Dieu
Qui enlève le péché du monde.

3.
Le jour où coulera le sang de ta vie
Pour que chante la terre,
Tu pourras reconnaître ton Seigneur
Au partage du vin
Tu pourras recevoir l´Agneau de Dieu
Qui enlève le péché du monde.