R.
Seigneur mon secours, en toi seul mon bonheur,
Ma vie repose entre tes mains. (bis)

1.
J’élève les yeux au loin, d’où me vient le secours.
Le secours me vient de Dieu, de Dieu seul.

2.
Ton pied ne chancellera, il veille sur tes pas.
Il ne dort ni ne sommeille, ton gardien.

3.
Le soleil ne t’atteindra, ni la lune en la nuit.
Le Seigneur est ton gardien, ton abri.

4.
Au départ et au retour, il gardera ton âme.
A jamais le Seigneur veille sur toi.