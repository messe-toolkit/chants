R.
Les cieux proclament la Gloire de Dieu,
Le firmament raconte l’ouvrage de ses mains.

1.
Le jour au jour en livre le récit,
et la nuit à la nuit en donne connaissance.
Pas de paroles dans ce récit,
pas de voix qui s’entende.
Mais sur toute la terre en paraît le message,
et la nouvelle aux limites du monde.

2.
Devant le ciel et tout ce que tu fis,
les étoiles et la lune et tous les luminaires,
qu’est-ce que l’homme, Seigneur mon Dieu,
pour que tu t’en avises ?
Tu l’appelles et l’invites à régner sur la terre,
tu le couronnes de tendresse et de gloire.

3.
Chantons sans fin l’amour de notre Dieu,
louons-le par nos voix et dans toutes nos oeuvres.
Chante ô mon âme, acclame Dieu,
glorifie sa Parole !
Bénissons le Seigneur sur la harpe à dix cordes,
adorons-le, Lui, dont l’amour est fidèle !

4.
De jour en jour proclamez son salut,
racontez ses merveilles et l’oeuvre de sa grâce
Joie pour les hommes, paix aux nations,
aux familles des peuples !
Et sur toute la terre annoncez le message,
et l’Évangile aux limites du monde !