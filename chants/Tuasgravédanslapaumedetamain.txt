1.
Tu as gravé dans la paume de ta main,
Mon nom, ma vie et les lignes de mon destin,
Tu me conduis sous un ciel d’amour sans fin,
Je m’accomplis, avec toi je ne crains rien.

CHACUN DE NOUS EST UN PROJET DE DIEU
PORTANT L’ESPOIR D’UN MONDE HEUREUX.
CHACUN DE NOUS EST AIMÉ DE DIEU
UNIQUE ENFANT NÉ DE SES VOEUX.

2.
Tu as gravé dans ta tendresse infinie
Ce que je suis dans mes peurs et mes folies.
J’entends ta voix dans le silence de mes nuits,
Tu es bien là et jamais tu ne m’oublies.

3.
Tu as gravé dans l’espace et dans le temps
Près de mon nom, un grand projet qui m’attend.
Je veux pour toi que prospèrent mes talents,
Qui que je sois, je resterai ton enfant.

4.
Tu as gravé dans les couleurs de la vie,
Tous ces éclats qui scintillent et se marient,
Ces milles feux dont je suis l’infime partie,
Source de joie, au soleil de Jésus Christ.