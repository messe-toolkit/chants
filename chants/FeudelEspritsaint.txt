1.
Feu de l’Esprit Saint consolateur,
Ô charité brûlante plus que flamme,
C’est toi seul qui guéris nos plaies,
De ta douceur tu les pénètres.

2.
Souffle tout-puissant, partout présent,
Tu as scruté l’abîme insaisissable ;
Voie royale, chemin des cieux,
Tu maintiens tout dans ta lumière.

3.
Source pure et claire de l’amour,
Auprès de toi les hommes se rassemblent ;
Tu abreuves les assoiffés,
Les réjouis par ta sagesse.

4.
Maître, tu enseignes tout savoir,
Sur tes élus, tu veilles sans relâche ;
Tu ramènes les égarés,
De l’Ennemi tu nous protèges.

5.
L’hymne de louange te bénit,
Et c’est de toi que naissent les louanges,
Espérance, puissante joie,
Vie des fidèles qui te chantent !