R.
Change ton regard,
Ton regard sur les autres
Change ton regard et le monde changera.

1.
Il y'a des jours où sans savoir pourquoi
Tu penses que les autres ne sont pas marrants
Tu fais la tête tu les regardes avec méfiance,
Tu dis qu’ils sont bêtes et méchants.
Mais si tu pouvais changer de lunettes,
Tu remarquerais toutes leurs qualités
Et tu oublierais toutes leurs étiquettes
Tu verrais chacun vraiment tel qu’il est.

2.
Il y a des jours où tu crois que personne ne t’aime,
Que les autres ne te comprennent pas.
Tu t’sens tout seul, enfermé dans tes p’tits problèmes,
Leur amitié, tu n’en veux pas.
Mais si tu pouvais changer de lunettes,
Tu verrais leurs mains tendues pour t’aider.
Et tu trouverais que la vie est chouette,
Qu’il y a plein de gens prêts à t’écouter.

3.
Parfois tes yeux ne voient, dans un bouquet de roses,
Que les épines qui t’ont piqué.
Comme avec ces jumelles qui déforment les choses,
Quand on voit du mauvais côté.
Mais si tu pouvais tourner tes lorgnettes,
Tu verrais d’abord la beauté des fleurs ;
Et si tu voulais changer de lunettes,
Tu saurais tout voir du regard du cœur.

4.
Certains ne savent voir, chez ceux qui les entourent,
Que ce qui peut leur rapporter.
Les autres sont, pour eux, le chewing-gum qu’on savoure
Et puis qu’on laisse de côté.
S’ils pouvaient quitter leurs grosses lunettes,
Ils arriveraient à changer un jour,
Leur gros cœur de riche sec et malhonnête
En un cœur de pauvre rempli d’amour.

5.
Il y a, tout près de nous, comme au bout de la terre,
Des gens que l’on ne sait pas voir.
Nos yeux sont aveuglés par nos murs, nos frontières,
Qui font comm' des lunettes noires.
Il faudrait casser toutes ces lunettes,
Ca nous ferait voir, d’un regard nouveau,
Qu’il y a partout des gens qui se mettent
À faire, avec nous, un monde plus beau.