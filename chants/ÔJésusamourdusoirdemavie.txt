Ô Jésus,
Amour du soir de ma vie,
Réjouissez-moi de votre vue
À l’heure de mon départ !
Ô Jésus,
Mon amour,
Soyez pour moi un soir si beau
Que mon âme ne trouve qu’allégresse
À quitter mon corps
Et s’envoler au Paradis.