UN SEUL PAIN PARTAGÉ
POUR FORMER UN SEUL CORPS;
À LA MÊME COUPE NOUS BUVONS,
PAR LE MÊME ESPRIT NOUS REVIVONS.

1.
Ce pain qui nous unit
Comme des blés moulus ensemble,
Nous le mangeons en rendant grâce :
Dieu soit béni pour la joie des invités !
Dieu soit béni pour la table préparée !

2.
Ce vin qui nous unit
Comme des grains pressés ensemble,
Nous le buvons le coeur en fête :
Dieu soit béni pour sa Vigne bien-aimée !
Dieu soit béni pour nos fruits ensoleillés !

3.
L’Agneau nous est donné,
Voici la chair du Fils de l’homme,
Corps immolé pour notre Pâque :
Dieu soit béni de nous prendre à son banquet !
Dieu soit béni pour nos vies renouvelées !

4.
Jésus, vrai Pain du ciel,
Fera de nous l’Église sainte,
Peuple témoin de l’Évangile.
Dieu soit béni de nourrir ses envoyés !
Dieu soit béni pour l’amour manifesté !