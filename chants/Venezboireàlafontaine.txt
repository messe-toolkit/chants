VENEZ BOIRE A LA FONTAINE,
AU PUITS DE LA SAMARITAINE.
VENEZ BOIRE AUX SOURCES VIVES,
DES FLEUVES JAILLIRONT DE VOUS.

1.
Si vous saviez le don de Dieu,
Qui est Celui qui vous demande à boire,
Vous lui demanderiez de l’eau,
Et il vous donnerait l’Eau Vive.

2.
Si vous saviez le don de Dieu,
Qui est Celui qui vous parle du Père,
Vous viendriez pour l’écouter,
Sa Parole en vous serait vie.

3.
Si vous saviez le don de Dieu,
Qui est Celui qui vous aime en vous-même,
Vous lui ouvririez votre cœur,
La vie renaîtrait de vos cendres.

4.
Si vous saviez le don de Dieu,
Qui est Celui qui vous donne sa vie,
Vous viendriez puiser en lui,
La vie dont vous rêvez pour vivre.