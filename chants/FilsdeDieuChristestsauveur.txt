Fils de Dieu, Christ et Sauveur,
Mort en croix pour nous,
Fils de Dieu, ressuscité,
Gloire à toi !
Gloire à toi qui paraîtras
Quand viendra ton Jour,
O Jésus, Maître et Seigneur de paix.