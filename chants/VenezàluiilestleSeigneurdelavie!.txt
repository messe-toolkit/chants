VENEZ À LUI, IL EST LE SEIGNEUR DE LA VIE !

VENEZ À LUI, VOUS QUI PEINEZ,
VENEZ À LUI, IL VOUS SOULAGERA,
IL EST LE SEIGNEUR DE LA VIE !

1.
Vous qui marchez avec les hommes
sur leurs chemins enténébrés,
Il veut se donner à vous, celui qui est la lumière du monde !

2.
Vous qui travaillez sans relâche
à faire cesser toute injustice,
Il vous comblera de biens celui qui veut rassasier toute faim !

3.
Vous qui partagez la détresse
de l'étranger que l'on rejette
Il veut vous réconforter celui qui va jusqu'au bout de l'amour !

4.
Vous qui cherchez à délivrer
la vie captive au cœur de l'homme,
Il veut vous donner sa paix celui qui est le vainqueur de la mort.