Stance
Dans la nuit qui le cernait,
Jésus, à l’abri du jardin,
a prié pour ses disciples.
Dans la gloire qui l’environne,
auprès du Père à jamais,
il prie pour tous ses frères :

PÈRE, QU’ILS SOIENT VRAIMENT
UN DANS L’AMOUR!

1.
Pour que mes disciples témoignent
que tu es en moi, que je suis en toi,

2.
Pour que mes frères contemplent un jour
la gloire que tu m’as donnée,

3.
Pour que mon Église rayonne de l’amour
dont tu l’as glorifiée,

4.
Pour que le monde reconnaisse en moi
le Messie que tu lui as envoyé,