ALLELUIA, JESUS SEIGNEUR,
ALLE, ALLELUIA, LOUANGE A TOI !
ALLELUIA, TU PARLES AU COEUR,
ALLE, ALLELUIA, HEUREUX QUI CROIT !
Lumière du Royaume,
Jésus nous t'acclamons.
Parole de sagesse,
Jésus, nous t'écoutons.