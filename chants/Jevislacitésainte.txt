R.
Je vis la cité sainte,
la Jérusalem nouvelle
qui descendait du ciel,
d’auprès de Dieu
toute prête comme une fiancée
parée pour son époux.

1.
La Demeure du Seigneur est avec nous :
Notre Dieu vient habiter parmi les hommes.

2.
Nous serons le peuple aimé de notre Dieu ;
Il sera l’Emmanuel, « Dieu-avec-nous ».

3.
Plus de larmes, ni de peines, ni de mort :
« L’ancien monde est aboli », dit le Seigneur.