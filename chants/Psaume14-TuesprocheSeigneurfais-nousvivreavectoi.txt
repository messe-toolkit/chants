TU ES PROCHE, SEIGNEUR ;
FAIS-NOUS VIVRE AVEC TOI.

1.
Seigneur, qui séjournera sous ta tente ?
Celui qui se conduit parfaitement,
qui agit avec justice
et dit la vérité selon son coeur.

2.
Il ne fait pas de tort à son frère
et n’outrage pas son prochain.
À ses yeux, le réprouvé est méprisable
mais il honore les fidèles du Seigneur.

3.
Il prête son argent sans intérêt,
n’accepte rien qui nuise à l’innocent.
L’homme qui fait ainsi
demeure inébranlable.