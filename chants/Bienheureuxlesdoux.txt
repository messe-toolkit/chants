1.
Bienheureux les doux, bienheureux les pacifiques !
Bienheureux les simples, bienheureux les coeurs purs.
Bienheureux les pauvres des empires humains,
Car ils seront puissants au Royaume de Dieu,
Au Royaume de Dieu.

2.
Bienheureux les doux, qui supportent l’injustice,
Bienheureux qui souffre sur la croix des humains.
Si l’on vous condamne, bienheureux serez-vous,
C’est vous qui jugerez au Royaume de Dieu,
Au Royaume de Dieu.