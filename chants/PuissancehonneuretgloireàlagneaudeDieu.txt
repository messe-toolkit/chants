Antienne :
Puissance, honneur et gloire à l'agneau de Dieu !

1.
Tu es digne, ô Seigneur notre Dieu, de recevoir l'honneur, la gloire et la puissance.
C'est toi qui créas l'univers. C'est par te volonté qu'il reçut l'existence et fut créé.

2.
Tu es digne Seigneur, de recevoir le livre et d'en ouvrir les pages scellées,
Car tu fus immolé, nous rachetant pour Dieu par ton sang,
De toutes tribus, langues, peuples et nations.

3.
Tu as fait de nous pour notre Dieu un royaume de prêtres,
Et nous régnerons sur la terre.
Il est digne, l'Agneau immolé, de recevoir puissance et richesse,
Sagesse et force, honneur, gloire et louange.

4.
Rendons gloire au Père tout-puissant, à son Fils Jésus-Christ le Seigneur,
À l'Esprit qui habite en nos cœurs, pour les siècles des siècles, Amen !