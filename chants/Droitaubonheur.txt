TOUS LES ENFANTS DE LA TERRE
ONT DROIT AU MÊME BONHEUR.
MOINS DE SOUFFRANCE, MOINS DE MISÈRE
POUR UN MONDE MEILLEUR.

1.
Pensons aux enfants des villages
De l’Afrique ou de l’Asie
Ils ont la joie sur le visage
Mais des trous dans leurs habits.

2.
Pour les plus pauvres pas d’école
Ils doivent aider leurs parents
Pas de loisir, d’aventure folle
Du travail pour peu d’argent.

3.
Pour l’avenir c’est difficile
Mais des rêves il y’en a plein
On s’agglutine dans les villes
On ne survit de presque rien.

4.
Soyons acteurs pour la justice
Mettons l’Amour en avant
Et que personne ne s’enrichisse
En profitant des enfants.