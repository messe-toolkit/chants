R.
Nous sommes ton peuple,
Nous sommes ton peuple,
Pitié, nous crions vers toi !
Pitié, nous crions vers toi !

1.
Je suis solitaire,
Je suis solitaire,
Toi seul es l´ami qui connais mon pas,
Toi seul es l´ami qui connais mon pas.
Mes yeux sont aveugles,
Mes yeux sont aveugles,
Tu mets la lumière dans ma maison,
Tu mets la lumière dans ma maison.

2.
Mon cœur n'est que cendres, (bis)
Ton cœur est le feu du buisson ardent. (bis)
Mon corps n'est que lèpre, (bis)
Tes mains sont la source qui me guérit. (bis)

3.
Je vais dans le doute, (bis)
Tu viens me rejoindre sur mes chemins. (bis)
Tu vois ma tristesse, (bis)
L´Esprit me console en parlant de toi. (bis)

4.
La nuit vient me prendre, (bis)
Ma nuit devient jour quand tu prends le pain. (bis)
La mort veut me perdre, (bis)
Ta mort me fait vivre à ta vie de Dieu. (bis)

5.
Mes mains sont trop riches, (bis)
Ta croix me fait signe de tout donner. (bis)
Je suis plein de haine, (bis)
Ta voix me redit que tu es l´Amour. (bis)

6.
Mes puits se dessèchent. (bis)
Ton sang me réjouit comme un vin nouveau. (bis)
La faim me torture, (bis)
Ton corps est la chair de l´Agneau Pascal. (bis)