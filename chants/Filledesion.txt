R.
FILLE DE SION, REJOUIS-TOI ! CAR LE SEIGNEUR EST EN TOI,
EN VAILLANT SAUVEUR !

1.
Debout! Rayonne ! car voici la lumière
Et sur toi se lève la gloire de Yahvé,
Tandis que les ténèbres s'étendent sur la terre
Et l'obscurité sur le peuple.

2.
Lève les yeux aux alentours et regarde :
Tous se rassemblent ils viennent à toi ;
Tes enfants arrivent de loin
Et tes filles sont portées sur les bras.

3.
Tu le verras et seras radieuse
Ton coeur tressaillera et se dilatera ;
Car les richesses de la mer afflueront vers toi
Et les trésors des nations viendront chez toi.

4.
Qui sont ceux-là qui volent comme un nuage
Comme des colombes vers leur colombiers ?
Oui, les navires s'assemblent pour toi,
Les vaisseaux de Ta rsis en tête.

5.
Alors que tu étais délaissée,
Haïe et abandonnée,
Je ferai de toi un objet d'éternelle fierté,
Un motif de joie d'âge en âge.

6.
Tu n'auras plus le soleil comme lumière le jour,
La clarté de la Lune ne t'illuminera plus ;
Mais Yahvé sera ta lumière éternelle ,
Et ton Dieu sera ta beauté.

7.
Ton soleil ne se couchera plus
Et ta lune ne disparaîtra plus ;
Mais Yahvé sera ta lumière éternelle ,
Et les jours de ton deuil seront accomplis.