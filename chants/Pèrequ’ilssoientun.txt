PÈRE, QU'ILS SOIENT UN !     (d’après Jean 14 à 17)

PÈRE, QU'ILS SOIENT UN)
COMME TOI ET MOI NOUS SOMMES UN !) Bis

1.
Veillez et priez en gardant ma parole,
L'Esprit vous conduira dans la vérité.

2.
Moi je suis la vigne, vous êtes les sarments
Demeurez en moi, comme moi en vous.

3.
Voici que je vous donne un commandement nouveau :
Vous aimer les uns les autres comme je vous ai aimés.

4.
Encore un peu de temps, le monde ne me verra plus
Mais vous vous me verrez, car je vis et vous vivrez !

5.
Celui qui croit en moi fera les œuvres que je fais
Car il recevra la vie en abondance !