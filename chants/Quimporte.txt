1.
Qu’importe de partir, s’enfuir au loin
Qu’importent les sirènes et leurs refrains
Qu’importent les vents du destin
Les coups de poings.

R.
Puisqu’aimer nous suffit bien
Comme un choix, comme un refrain
Et puisqu’aimer nous rend serein
Vivant enfin !
Enfin vivant !

2.
Qu’importent les brûlures sur le chemin
Les nuits, ou le brouillard dans le matin
Qu’importent les loups et les chiens
Ou le dédain.

3.
Qu’importe de jouer au plus malin
Quand le hasard nous prend pour un pantin
Pourquoi accumuler des biens
C’est trop mesquin !