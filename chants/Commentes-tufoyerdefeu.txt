1.
Comment es-tu foyer de feu
et fraîcheur de la fontaine,
une brûlure, une douceur
qui rend saines nos souillures ?

4.
Comment n'es-tu qu'un avec nous,
nous rends-tu fils de Dieu même ?
Comment nous brûles-tu d'amour
et nous blesses-tu sans glaive ?

2.
Comment fais-tu de l'homme un dieu,
de la nuit une lumière,
et des abîmes de la mort
tires-tu la vie nouvelle ?

5.
Comment peux-tu nous supporter
rester lent à la colère,
et de l'ailleurs où tu te tiens
voir ici nos moindres gestes ?

3.
Comment la nuit vient-elle au jour ?
Peux-tu vaincre les ténèbres,
porter ta flamme jusqu'au coeur
et changer le fond de l'être ?

6.
Comment si haut et de si loin
ton regard suit-il nos actes ?
Ton serviteur attend la paix,
le courage dans les larmes !