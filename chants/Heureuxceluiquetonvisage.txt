1.
Heureux celui que ton visage a fasciné, Seigneur Jésus,
Et dont l'amour a reconnu partout le sceau de ton image.

2.
Heureux celui que ta présence a dépouillé : tu l'envahis ;
Saisie par toi, toute sa vie te laisse voir en transparence.

3.
Vivante icône où ton mystère est apparu sur nos chemins,
Heureux celui qui dans tes mains passe avec toi du monde au Père.