ANAMNESE
Proclamons le mystère de la foi :
Gloire à toi qui étais mort,
gloire à toi qui es vivant,
notre Sauveur et notre Dieu :
Viens, Seigneur Jésus !