1.
Un refrain, rien qu’un refrain,
Pas même chansonnette.
Un refrain, rien qu’un refrain,
Petit refrain de rien.
Tout juste assez pour faire…

2.
Un amour, rien qu’un amour,
Pas même une tendresse.
Un amour, rien qu’un amour,
Petit amour tout court.
Tout juste assez pour faire…

3.
Un chagrin, rien qu’un chagrin,
Pas même une tristesse.
Un chagrin, rien qu’un chagrin,
Petit chagrin de rien.
Tout juste assez pour faire…
Un refrain…