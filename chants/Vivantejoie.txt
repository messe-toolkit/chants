1.
Vivante joie des hommes libres,
Don de l'amour à ses amis,
Nous te chantons !
En saint Bernard tu resplendis,
Esprit de feu qui nous attires
Au jour du Christ !

2.
Quand il célèbre la merveille
D'un Dieu fait chair pour tout sauver,
Visite-nous.
Avec Bernard fais-nous trouver,
Dormant à l'ombre de la Vierge,
L’Enfant d'un jour.

3.
Et que sa voix dans les ténèbres,
Près de la croix du Bien-Aimé
S'élève encor :
L'Amour a pris notre péché.
Son beau visage de lumière
S'est obscurci.

4.
Aux mots humains
Donne ta flamme
Pour que Bernard tienne en éveil
Ceux qui dormaient.
Viendra Jésus comme un soleil,
Comme un soleil au fond de l'âme
Éblouissant !