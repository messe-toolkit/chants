1.
En toi Seigneur, mon espérance
Sans ton appui, je suis perdu
Mais rendu fort par ta puissance,
Je ne serai jamais déçu.

2.
Sois mon rempart et ma retraite,
Mon bouclier, mon protecteur
Sois mon rocher dans la tempête
Sois mon refuge et mon sauveur.

3.
Lorsque du poids de ma misère
Ta main voudra me délivrer
Sur une route de lumière
D'un cœur joyeux je marcherai.

4.
De tout danger garde mon âme,
Je la remets entre tes mains,
De l'ennemi qui me réclame
Protège-moi, je suis ton bien.