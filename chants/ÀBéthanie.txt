1.
Un deuil de plus à Béthanie
L’homme n’est qu’herbe vaine
Jésus gardien de tes amis
Viens partager leur peine.

2.
Marthe ne peut se résigner
Ni pleinement te croire
Donne à nos yeux mal dessillés
De contempler ta gloire.

3.
Marie vêtue de sa douleur
Te suit dans le silence
Où ton amour verse les pleurs
Qui lavent la souffrance.

4.
Toi qui attires et qui fais peur
Montre-nous ta justice
Sur le chemin de ta douceur
Que Pâques s’accomplisse.

5.
La mort pourra faucher encor
Tu caches le fragile
L’inépuisable et pur trésor
Dans nos vases d’argile.