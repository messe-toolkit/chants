Refrain :
Gloire au Christ,
Parole éternelle du Dieu vivant
Gloire à toi, Seigneur

Versets :

1.
Parle, Seigneur,
tes serviteurs écoutent,
tu as les paroles de la vie éternelle.

2.
Tu nous donnes un commandement nouveau,
tu es nourriture de vie éternelle.

3.
Tout ce que vous avez fait
à l’un de ces petits qui sont mes frères,
c’est à moi que vous l’avez fait.