POUR SA PAROLE ET POUR SON PAIN, JE CHANTERAI LE SEIGNEUR (Bis)

1.
Pour la table prête
Quand je suis revenu,
Pour l'habit de fête
Qu'un Père m'a rendu,
Je chanterai le Seigneur.

2.
Pour sa voix qui perce
La brume de ma foi,
Pour mes yeux qui cherchent
Ouverts à ce repas,
Je chanterai le Seigneur.

3.
Pour ces mots qui peuvent
Ressusciter l'amour,
Pour la route neuve
Qu'ils m'ouvrent chaque jour,
Je chanterai le Seigneur.

4.
Pour la main des frères
Qui m'ont donné la paix,
Pour la terre entière
Tendue dans l'unité,
Je chanterai le Seigneur.