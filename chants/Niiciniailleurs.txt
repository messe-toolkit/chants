Refrain :
Ni ici - ni ailleurs,
Mais faut faire autrement
Ni ici - ni ailleurs,
Pensez à nos enfants !

1.
“Un projet de Labo” … et nous on n’ se doutait pas.
“Un stockage de déchets” … mais faut pas vous en faire…
C’est pas “HIROSHIMA…” Et pourtant, c’est comme ça,
Que le village est devenu “poubelle nucléaire !”
Ils ont subventionné les jardins et son lac,
La salle polyvalente et sa piste de danse,
Même les clubs de foot, de canoës-kayaks,
Ils ont tout acheté - y compris les CONSCIENCES.

2.
Un canton vieillissant, et tellement inactif…
Des promesses d’emplois… alors “on fait avec”.
Mais le RADIOACTIF - reste RADIOACTIF.
Même avec des promesses et un carnet de chèques,
Je dis non aux DÉCHETS, mon pays est si beau.
À tous les fossoyeurs du GARD ou de la MEUSE,
J’leur dis que j’n’suis pas BIO, ni ÉCOLO,
Mais qu’ma TERRE est si BELLE et tellement PROMETTEUSE !