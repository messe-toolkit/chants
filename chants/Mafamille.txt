1.
Rendez-vous le matin
Au petit-déjeuner,
Il y’en a quelques-uns
Pas très bien réveillés.
On s’embrasse et on se quitte,
La journée passera vite !

C’EST UN CADEAU DU PÈRE
MA FAMILLE RÉUNIE,
C’EST UN CADEAU DU PÈRE
UNE CHANCE DANS MA VIE.
C’EST UN CADEAU DU PÈRE
MA FAMILLE POUR TOUJOURS,
CE PETIT COIN D’AMOUR.

2.
Le midi ou le soir,
À nouveau rassemblés,
On partage une histoire,
Un fou rire, une idée.
Quelquefois c’est la pagaille,
Certains jours on se chamaille !

3.
On reçoit des cousins,
Un grand père, un ami,
Pour chacune ou chacun
C’est le coeur qui grandit.
Au pays de la tendresse,
Ma famille a une adresse.