MARIE, MARIE
MAMAN DE JESUS
LE MONDE EST BEAU CONTRE TON COEUR.
MARIE, MARIE
MAMAN DE JESUS
ENTRE TES BRAS, JE N'AI PLUS PEUR.

1.
Vers ton visage de lumière
Mes pensées deviennent prières
Quand j'ai peur au fond de mon lit
Tu me rassures dans la nuit

2.
Vers ton visage de tendresse
Mes pensées deviennent caresses
Je sens le bonheur d'être aimé
Dans tes yeux qui disent la Paix

3.
Vers ton visage d'espérance
Mes pensées deviennent confiance
Toi, tu m'écoutes à chaque instant
Je me sens vraiment ton enfant