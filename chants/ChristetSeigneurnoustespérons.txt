R.
CHRIST ET SEIGNEUR, NOUS T’ESPÉRONS,
DÉJÀ TON SOUFFLE NOUS HABITE!
VIENNE TON RÈGNE ET NOUS VERRONS
UN TEMPS D’AMOUR ET DE JUSTICE!

1.
Il viendra le Jour promis !
Fils de Dieu, tu paraîtras,
Toute chair te connaîtra.
Aujourd’hui garde-nous éveillés !
Nous croyons que tu viens
Donner au monde ta jeunesse.

2.
Il viendra le Jour promis !
Nos ravins seront comblés,
Nos montagnes nivelées.
Aujourd’hui préparons ton chemin !
Nous croyons que tu viens
Ouvrir nos coeurs à ta Sagesse.

3.
Il viendra le Jour promis !
Sur nos lèvres un chant naîtra
Que l’Esprit murmurera.
Aujourd’hui exultons de ta joie !
Nous croyons que tu viens
Chasser la nuit de nos tristesses.

4.
Il viendra le Jour promis !
Par l’Esprit qui t’a formé
Grandira le Corps entier,
Aujourd’hui regardons vers Marie !
Nous croyons que tu viens
Nous faire vivre des merveilles.

5. (Noël - Épiphanie)
Il viendra le Jour promis !
Ta lumière, Emmanuel,
Brillera sur Bethléem.
Aujourd’hui, accueillons cette paix !
Nous croyons que tu viens
Offrir aux hommes ta tendresse.

6.
Il viendra le Jour promis !
Tu seras manifesté
Aux nations du monde entier.
Aujourd’hui, levons-nous pleins d’espoir !
Nous croyons que tu viens
Guider nos pas vers ta lumière.