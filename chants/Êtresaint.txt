ÊTRE SAINT, ÊTRE SAINT !
C’EST RESSEMBLER À JÉSUS !
LE PRIER DANS SON COEUR
ET L’AIMER EN VÉRITÉ.
ÊTRE SAINT, ÊTRE SAINT !
C’EST RESSEMBLER À JÉSUS !
LE PRIER DANS SON COEUR
ET AIMER SES FRÈRES ET SOEURS.

1.
Il y a des grands saints
Qui ont fait de très grandes choses,
Ont beaucoup écrit, ou ont guéri.
Moi je suis petit,
Mais Jésus m’appelle aussi
À vivre en lui et être saint...

2.
Il y a d’autres saints
Qui n’ont pas fait de grandes choses,
Mais qui ont vécu avec Jésus,
Ont beaucoup aimé,
Pardonné, encouragé...
Moi, comme eux, je veux être saint...

3.
Il y a des enfants,
À la maison ou à l’école,
Qui portent Jésus, dans le secret.
J’aimerais aussi
Être ami, en vérité,
Pour vivre en lui et être saint...

Guimmick :
Ta la pam pam
Ta la pam pam
Ta la pam pam
Ta la pam pam pam