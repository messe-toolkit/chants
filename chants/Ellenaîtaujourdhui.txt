1.
Elle naît aujourd’hui
Celle qui n’a jamais
Connu l’oeuvre de la nuit.
Tandis que le péché
Enténèbre le monde,
Là-bas sur la Judée
L’aurore monte.

2.
Elle naît aujourd’hui
Celle en qui vient la paix
Et rayonne la vie.
La grâce est sa beauté,
Dieu seul est sa lumière.
L’Esprit a sanctifié
La bonne terre.

3.
Elle naît aujourd’hui
Celle en qui Dieu se plaît,
Celle qu’il a choisie.
La tige de Jessé
Fleurit dans le silence.
Le fruit nouveau formé
Rend l’espérance.

4.
Elle naît aujourd’hui
Celle qui va porter
La joie du jour promis.
Marie l’Immaculée
Est la Vierge fidèle.
Tout son être est livré
Quand Dieu l’appelle.