R.
Où sont amour et charité,
Dieu est présent.

1.
Qui donc pourrait nous séparer du Dieu vivant ?
Aurions-nous peine à témoigner de son amour ?

2.
Qui n’aime pas son propre frère qu’il peut voir,
ne peut prétendre aimer son Dieu qu’il ne voit pas.

3.
Que tous nos actes soient empreints de charité,
car nous serons jugés par Dieu sur notre amour.

4.
La charité est vérité, patience et paix,
simplicité, service et joie de chaque instant.

5.
Quand j’avais faim, quand j’avais soif, nous dit Jésus,
avez-vous su me reconnaître dans vos frères ?

6.
Quand j’étais nu, quand j’étais pauvre devant vous,
a
ez-vous su me reconnaître dans vos frères ?

7.
Dans la mesure où nous aimons notre prochain,
c’est Jésus Christ que nous aimons réellement.

8.
Et notre joie sera parfaite au dernier jour
en contemplant le Dieu d’amour qui vit en nous.