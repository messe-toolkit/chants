Voici l'époux,
Sortez à sa rencontre !
Voici l'époux :
Il vous donne pour l'attendre
Et l'huile de vos lampes
Et la flamme.

R.
Vers toi, Seigneur, notre regard !

Vous étiez autrefois ténèbres,
À présent lumière dans le Seigneur :
Marchez en enfants de lumière.

Que resplendisse en vous
La connaissance de la gloire de Dieu :
Jésus a brillé dans vos cœurs.

Rendez grâce au Père :
Il vous appelle à partager
L’héritage des saints dans la lumière.

Tenez-vous prêts, sur vos gardes :
Vous ne savez ni le jour ni l'heure.