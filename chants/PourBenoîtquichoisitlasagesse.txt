1.
Pour Benoît qui choisit la Sagesse,
Pour Benoît qui te cherche sans cesse,
Béni sois-tu, Seigneur !
Pour l'amour embrassant la patience,
Le désert où le cœur fait silence
Béni sois-tu, ô notre Père,
Une eau vive murmure ton nom !

2.
Pour Benoît que tu mets à l'épreuve,
Sa victoire où l'Esprit est à l'œuvre,
Béni sois-tu, Seigneur !
Pour le faible exaltant ta puissance
Le pécheur qui te garde confiance
Béni sois-tu, ô notre Père,
Dans la nuit tu combats avec nous !

3.
Pour Benoît que ta paix environne,
La douceur habitée par ta force,
Béni sois-tu, Seigneur !
Pour la voix qui redit ton message,
Le service où rayonne ta grâce
Béni sois-tu, ô notre Père,
L'évangile prend corps dans tes saints !

4.
Pour Benoît que ta gloire fascine,
Son regard qui discerne tes signes,
Béni sois-tu, Seigneur !
Pour le pain partagé avec l'hôte,
Béni sois-tu, ô notre Père,
Notre cœur est en fête pour toi !

5.
Pour Benoît qui rassemble des frères,
Et leurs vies façonnées de prière,
Béni sois-tu, Seigneur !
Pour l'appel à chanter ta louange,
À chanter en présence des anges,
Béni sois-tu, ô notre Père,
Toi qui viens habiter notre chant !