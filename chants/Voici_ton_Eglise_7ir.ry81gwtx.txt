1.
Dieu notre Père, nous venons devant toi.
Fils bien-aimé, tu es notre Roi.
Esprit de Dieu viens et embrase-nous ;
Ô Dieu Saint, nous t'adorons.
Ô Dieu Saint, nous te prions !

R.
Seigneur, mets en nous ton feu,
Viens, éclaire nos cœurs,
Rassemble-nous, unis ton Église !
Nous proclamerons ton Nom !
Nous chanterons pour toi,
Tu es notre Dieu, voici ton Église !

2.
À ton image, tu nous as façonnés.
Dans ta bonté, tu t'es révélé.
En Jésus Christ, tu nous donnes ta vie.
Ô Dieu Saint, nous t'adorons.
Ô Dieu Saint, nous te prions !

Esprit de Dieu, viens en nous. (x4)