R.
Croix glorieuse,
Sainte Croix de Jésus Christ,
Croix qui nous sauve,
Clé du ciel pour notre vie,
Vers toi nous regardons :
Fils de Dieu, nous te glorifions !

1.
Croix d’ignominie, lieu de mort dans la souffrance,
Croix qui scandalise et défie notre espérance,
Comment te regarder,
Croix du Juste condamné,
Comment te regarder
Sans douter d’un Dieu tendresse?

2.
Croix du Serviteur sans un mot sous la torture,
Croix qui fait trembler à la vue de ses blessures,
Comment te regarder,
Croix du Frère premier-né,
Comment te regarder
Sans changer nos coeurs de pierre ?

3.
Croix marquée des clous dans les mains du Fils de l’homme,
Croix rougie du sang répandu sous les opprobres,
Comment te regarder,
Croix du Pauvre dépouillé,
Comment te regarder
Sans frémir devant sa peine?

4.
Croix du Rédempteur qui fut élevé de terre,
Croix qui nous attire en montrant la vraie Lumière,
Comment te regarder,
Croix du Maître bien-aimé,
Comment te regarder
Sans s’ouvrir à ta sagesse ?

5.
Croix du Verbe unique expirant sur la colline,
Croix du Bon Pasteur dont la Pâque nous délivre,
Comment te regarder,
Croix du Fils abandonné,
Comment te regarder
Sans aimer jusqu’à l’extrême ?