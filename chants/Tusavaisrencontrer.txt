R.
TU SAVAIS RENCONTRER LES GENS DE TON PAYS,
L'ETRANGER DEVENAIT TON AMI.

1.
Dans le oui de Bartimée
fleurissait tant d'espérance,
que tu as guéri ses yeux d'aveugle-né.
A l'appel du centurion
tu as partagé sa peine,
et sauvé son serviteur dans sa maison.

2.
Voyant Simon et André
tu en as fait tes apôtres,
Jacques et Jean et tous les autres appelés.
Le jeune homme riche et bon
qui aurait voulu te suivre,
s'en est retourné bien triste en sa maison.

3.
Tu disais que les enfants
ont les clefs de ton royaume,
avec ceux qui leur ressemblent au fil des temps.
Tu avais de bons amis,
Marthe, Marie et Lazare,
tu aimais les retrouver à Béthanie.