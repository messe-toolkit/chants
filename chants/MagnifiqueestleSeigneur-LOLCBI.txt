R.
Magnifique est le Seigneur
Tout mon cœur pour chanter Dieu
Magnifique est le Seigneur.

1.
Magnifique est le Seigneur
Tout mon cœur pour chanter le Dieu de mon salut !
Son regard s' est posé sur son humble servante :
Toutes les générations découvriront ma joie !

2.
Sa puissance fait pour moi des merveilles :
Que son nom est grand !
Sa tendresse va de génération en génération
A ceux qui le reconnaissent.

3.
Il déploie la force de son bras
Pour la déroute des orgueilleux ;
il détrône les puissants et relève les humbles.

4.
Il rassasie les affamés
et renvoie les riches les mains vides
Il prend soin de son peuple comme d' un fils
Dans la fidélité de son amour.

5.
Il tient la parole donnée autrefois
En faveur d' Abraham
et de sa lignée dans les siècles.