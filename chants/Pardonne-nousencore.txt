R.
Pardonne-nous, Seigneur,
Nous enfantons la mort,
Pardonne-nous, Seigneur, pardonne-nous encore.

1.
Pourquoi froisser le blé dont l´épi a mûri ?
Pourquoi souffler le feu où le pain avait cuit ?
Pourquoi se dérober quand la fête est servie ?

2.
Pourquoi refuser l´eau cueillie au fond du puits ?
Pourquoi fermer les yeux au soleil de midi ?
Pourquoi tourner le dos à l´enfant qui sourit ?

3.
Pourquoi couper le bois, la branche avait fleuri ?
Pourquoi presser le fruit si la coupe est remplie ?
Pourquoi clouer en croix celui qui donne vie ?