COMME TON DIEU CROIT EN TOI,
N’AIE PAS PEUR DE VIVRE TA FOI
ET TU MARCHERAS AVEC LUI, VERS D’AUTRES VOIES.
ET SI TU CROIS EN TOI,
SURTOUT SACHE QU’IL NE T’OUBLIE PAS
ET TU MARCHERAS AVEC LUI, VERS D’AUTRES VOIES.

1.
Appelés à la vie, frères en humanité,
Chacun se construit en fraternité.
Appelés à aimer, donner sens à la vie.
Notre société t’invite à agir.
Oser s’oublier, savoir se donner,
Avoir foi en l’autre, devenir apôtre.

2.
Baptisés en son nom, témoins de sa parole,
Dans un monde qui parfois te désole.
Ce monde en crise de foi t’invite à dire « je crois ».
Avance en confiance, porteur d’ Espérance.
Oser s’engager, savoir partager,
Avoir foi en lui, guidés par l’Esprit.

3.
Occupés, fatigués de toujours courir,
Oubliant de penser, de réfléchir.
Prends le temps de relire, pour construire l’avenir.
Libre de tes choix, fais grandir ta foi.
Oser s’arrêter, savoir discerner
S’accomplir en lui pour porter du fruit.