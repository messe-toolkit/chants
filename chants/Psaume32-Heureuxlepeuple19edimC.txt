HEUREUX LE PEUPLE
DONT LE SEIGNEUR EST LE DIEU.

1.
Criez de joie pour le Seigneur, hommes justes !
Hommes droits, à vous la louange !
Heureux le peuple dont le Seigneur est le Dieu,
heureuse la nation qu’il s’est choisie pour domaine !

2.
Dieu veille sur ceux qui le craignent,
qui mettent leur espoir en son amour,
pour les délivrer de la mort,
les garder en vie aux jours de famine.

3.
Nous attendons notre vie du Seigneur :
il est pour nous un appui, un bouclier.
Que ton amour, Seigneur, soit sur nous
comme notre espoir est en toi !