R.
Je mets ma main dans Ta main,
Je vais sur le chemin qui me conduit vers Toi.
Je mets ma main dans Ta main,
Je vais sur le chemin, je marche dans la joie.

1.
Toi qui es venu m'appeler,
Toi qui es venu me chercher,
Toi qui es venu me sauver,
Je marche avec Toi.

2.
Tout le long des chemins brûlants,
Sous le soleil et dans le vent,
Moi je m'en vais le coeur chantant,
Je marche avec Toi.