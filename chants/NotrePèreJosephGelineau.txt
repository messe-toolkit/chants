Notre Père,
qui es aux cieux, que ton Nom soit sanctifié,
que ton règne vienne, que ta volonté soit faite
sur la terre comme au ciel.
Donne-nous aujourd'hui notre pain de ce jour,
pardonne-nous nos offenses
comme nous pardonnons aussi à ceux qui nous ont offensés,
et ne nous soumets pas à la tentation,
mais délivre-nous du Mal.
Car c'est à toi appartiennent
Le règne, la puissance et la gloire,
Pour les siècles des siècles. Amen