Pitié, Seigneur, j'ai péché contre toi,
Lave-moi plus blanc que neige !

1.
Pitié pour moi, mon Dieu dans ton amour,
Par ta grande tendresse, efface tous mes torts.
Lave-moi tout entier de ma faute,
Purifie-moi de mon péché.

2.
Oui, je reconnais tous mes torts,
Ma faute est toujours devant moi.
Contre toi, toi seul, j´ai péché,
Ce qui est mal à tes yeux, je l´ai fait.

3.
Rends-moi la joie par des chants de fête,
Et les os que tu avais broyés, danseront.
Ne regarde plus mes péchés,
Enlève toutes mes fautes.
4.
Refais-moi un cœur tout neuf,
Raffermis au fond de moi mon esprit.
Ne me chasse pas loin de ta face.
Ne retire pas de moi ton Esprit Saint.