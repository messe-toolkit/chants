KYRIE ELEISON, CHRISTE ELEISON, KYRIE ELEISON

1.
Seigneur, tu as dit à Zachée: « Il me faut demeurer chez toi! »
Jésus, nous t’attendons, et viendra le salut pour notre maison.

2.
Seigneur, tu as promis de l’eau à la Samaritaine.
Jésus, donne-nous de cette eau, et nous n’aurons plus jamais soif.

3.
Seigneur, tu as sorti Lazare de la tombe.
Jésus, défais les liens de nos peurs, nous renaîtrons à ta Vie.

4.
Seigneur, tu as dit à Simon: « J’ai prié pour toi »
Jésus, quand notre foi défaille, intercède pour nous.

5.
Seigneur, tu as rendu la vue à l’aveugle.
Jésus, donne-nous ton regard, et notre coeur s’ouvrira à ta grâce.

6.
Seigneur, tu t’assieds à la table des pécheurs.
Jésus, viens partager avec nous le pain de nos vies

7.
Seigneur, tu accueilles les larmes et le parfum.
Jésus, nous venons à tes pieds briser le flacon de nos vies.

8.
Seigneur, tu t’arrêtes à l’auberge d’Emmaüs.
Jésus, quand la nuit tombe, reste encore avec nous.

9.
Seigneur, tu as guidé la pêche des apôtres,
Jésus, viens remplir les filets de ceux qui marchent à ta suite.

10.
Seigneur, tu sais que la moisson est abondante.
Jésus, prie le Père pour nous d’envoyer des ouvriers.