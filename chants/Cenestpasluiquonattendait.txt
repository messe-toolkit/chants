R.
CE N’EST PAS LUI QU’ON ATTENDAIT
C’ÉTAIT UN AUTRE,
PAS CET ENFANT SANS MAJESTÉ,
MAIS UN GRAND HOMME,
UN ROI PUISSANT ET COURONNÉ,
MAIS PAS UN GOSSE,
CE N’EST PAS LUI QU’ON ATTENDAIT,
C’ÉTAIT UN AUTRE.

1.
Quand ils ont entendu les anges,
Les bergers se sont demandé :
« Pour qui ces chants et ces louanges ?
D’où vient-il donc, ce nouveau-né ? »
Ils ne comprirent pas qu’il était Fils de Dieu
Mais quand ils l’ont quitté,
leur coeur était heureux !

2.
Quand ils ont aperçu l’étoile,
Les mages ont dit : « Un roi est né !
Le ciel à nos yeux se dévoile,
Partons ensemble l’adorer ! »
Ils ne comprirent pas qu’il était Fils de Dieu
Mais quand ils l’ont quitté,
leur coeur était heureux !

3.
Quand Marie a donné naissance
À un garçon, vers la minuit,
Elle adora, dans le silence,
Ce fils promis, né de l’Esprit.
Elle avait bien compris qu’il était Fils de Dieu
Et quand elle l’embrassait,
son coeur était heureux !

C’ÉTAIT BIEN LUI QU’ELLE ATTENDAIT,
ET PAS UN AUTRE,
IL DONNERAIT AUX REJETÉS
UN VRAI ROYAUME,
RÈGNE D’AMOUR, RÈGNE DE PAIX,
JOIE POUR LES HOMMES,
C’ÉTAIT BIEN LUI QU’ELLE ATTENDAIT
ET PAS UN AUTRE.