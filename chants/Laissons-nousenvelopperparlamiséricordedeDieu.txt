Laissons-nous envelopper par la miséricorde de Dieu ;
comptons sur sa patience qui nous donne toujours du temps ;
ayons le courage de retourner dans sa maison,
de demeurer dans les blessures de son amour, en nous laissant aimer par lui.