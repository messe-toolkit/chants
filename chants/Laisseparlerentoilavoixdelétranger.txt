1.
Laisse parler en toi
La voix de l'étranger.
Ouvre-lui ton chemin.
Quand tu seras toi-même égaré
Il t'apprendra son pas.

2.
Laisse jaillir en toi
La joie de l'étranger,
Ouvre-lui ta chanson.
Quand tu seras toi-même livré,
Il t'apprendra sa paix.

3.
Laisse grandir en toi
La faim de l'étranger.
Ouvre-lui ton repas.
Quand tu seras toi-même brisé,
Il t'apprendra son pain.

4.
Laisse brûler en toi
Le coeur de l'étranger.
Ouvre-lui ta maison.
Puisque tu es toi-même étranger,
Tu connaîtras sa croix.