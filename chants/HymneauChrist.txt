R.
Christ aujourd'hui
Comme hier et demain,
Christ avec nous,
Jusqu'à la fin du temps,
Toi qui es, qui étais et qui viens,
Béni sois-tu éternellement !

1.
Fils de Dieu,
Égal à Dieu depuis toujours,
Tu as pris la condition de serviteur.
Louange à toi !
Fils de Dieu,
Égal à Dieu depuis toujours,
Tu t'es fait semblable aux hommes.
Louange et gloire à toi !
Fils de Dieu,
Égal à Dieu depuis toujours,
Tu obéis jusqu'à mourir en croix
Louange, honneur et gloire à toi !

2.
Fils de l'homme,
Exalté par ton Seigneur et ton Dieu
Tu as reçu le nom au-dessus de tout nom.
Louange à toi !
À ton nom béni,
Tout ce qui vit fléchit le genou
Au ciel, sur terre et aux enfers.
Louange et gloire à toi !
Ton nom très saint, ton nom béni
Est acclamé dans toutes langues
À la gloire de Dieu ton Père.
Louange, honneur et gloire à toi !

3.
Image du Dieu invisible,
Premier-né de toute créature,
En toi tout fut créé au ciel et sur terre.
Louange à toi !
Tu es avant toute chose créée,
Toute chose est créée par toi et pour toi,
Et toute créature subsiste en toi.
Louange et gloire à toi !
Fils Bien-aimé du Père,
L'humanité est rachetée,
Ton sang lui ouvre le Royaume.
Louange, honneur et gloire à toi !

4.
Tête et corps de ton Église,
Tu es le commencement de tout,
Le premier-né d'entre les morts.
Louange à toi !
Tu as, en tout, la primauté
Et toute plénitude habite en toi,
En toi tout est réconcilié avec Dieu.
Louange et gloire à toi !
C'est toi qui as fait la paix,
Par le sang de ta croix,
Pour tous les êtres sur la terre et dans le ciel !

5.
En toi, Christ et Seigneur,
Le Père nous a bénis et comblés
Des bénédictions de l'Esprit.
Louange à toi !
En toi, Christ et Seigneur,
Le Père nous a choisis avant toute création
Pour être saints en sa présence.
Louange et gloire à toi !
En toi, Christ et Seigneur,
Le Père a récapitulé toutes choses
Et mené les temps à leur plénitude.
Louange, honneur et gloire à toi !