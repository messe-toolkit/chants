1.
J’écoute les bruits autour de moi
Ceux qui sont là, ceux qui sont loin,
J’écoute la vie autour de moi.

J’ÉCOUTE EN SILENCE
MON CŒUR QUI BAT,
J’ÉCOUTE UN SILENCE
AU FOND DE MOI.

2.
J’écoute les bruits plus loin, là-bas
Ceux qui sont forts, ceux qui sont doux,
J’écoute la vie plus loin, là-bas.

3.
J’écoute la vie au creux de moi
Tout doucement, comme un printemps,
J’écoute la vie au creux de moi.

4.
J’écoute la vie au fond de moi
J’y reconnais, Jésus, ta voix
J’écoute ta Vie au fond de moi.