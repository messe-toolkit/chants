R.
Ta parole est une fête,
Ton amour est comme un pain.
Que ton nom éclate en fête
Dans nos coeurs et dans nos mains,
Que ton nom éclate en fête
Dans nos coeurs et dans nos mains.

1.
Voici le temps de la rencontre,
Chacun de nous est invité.
Heureux le coeur où la joie monte,
C´est la saison du grain semé.

2.
Voici le temps de la Parole.
Tout près d´un puits il vient parler.
Heureux le coeur qui s´en étonne,
C´est la saison du champ de blé.

3.
Voici le temps d´ouvrir les mains,
Il a bon goût de pain donné.
Heureux le coeur brûlant de faim,
C´est la saison de récolter.

4.
Voici le temps d´être en chemin.
Va partager, cours annoncer.
Heureux le coeur qui est témoin,
C´est la saison de tout donner.