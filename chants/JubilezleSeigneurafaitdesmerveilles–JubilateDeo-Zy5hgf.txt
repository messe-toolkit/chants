R.
Jubilate Deo, Jubilate Deo (bis)

1.
Voici le Dieu qui me sauve : j’ai confiance, je n’ai plus de crainte.
Ma force et mon chant, c’est le Seigneur :
Il est pour moi le salut.

2.
Rendez grâce au Seigneur, proclamez son Nom,
Annoncez à tous les peuples ses hauts faits !
Redîtes-le : « Sublime est son Nom ! »

3.
Car il a fait des prodiges que toute la terre reconnaît.
Jubilez, criez de joie, car Dieu est grand
Au milieu de vous !