COMME DAVID, MON SEIGNEUR ET MON ROI,
JE VEUX TE CHANTER !
JE VEUX DANSER POUR MON DIEU L’ÉTERNEL,
ALLELUIA, ALLELUIA !

1.
Je te dis merci, du fond de mon coeur,
Pour tous mes amis, pour tant de belles fleurs,
Et pour ma famille, merci vraiment de tout coeur,
De tout coeur !

2.
Je te dis pardon, du fond de mon coeur,
Avec affection, tu parles à toute heure,
Et pour mes ronchons, pardon vraiment de tout coeur,
De tout coeur !

3.
Je te dis bravo, tu es créateur,
Dieu tu es si beau, tu es mon seul bonheur,
Et pour ces cadeaux, bravo vraiment de tout coeur,
De tout coeur !