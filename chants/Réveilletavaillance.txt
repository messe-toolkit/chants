« Réveille ta vaillance et viens nous sauver. »
Berger d’Israël, écoute :
l’ouvrage de tes mains,
vase fragile
où sommeille la mémoire de ton Nom,
vase d’argile
où tu déposes les richesses de ta Parole,
l’ouvrage de tes mains, Seigneur,
attend le signe du Jour.
« Déchire les cieux et viens à notre rencontre ! »
La nuit est avancée, le jour est proche.
Dieu fidèle, Dieu patient,
tu viens déchirer le voile d’oubli
tombé sur la vigne endormie,
et tu l’établis en sûreté
pour le combat de la lumière.
« L’heure est venue de sortir du sommeil ! »
Nos coeurs, façonnés par ta main puissante,
veillent jusqu’à ton retour.
Grâce et paix
les drapent d’une vêture de joie
brodée d’espérance.
« Dieu de l’univers, reviens, reviens ! »
Dans l’insondable mesure du temps,
comme dans les signes des astres,
une lueur d’espérance s’éveille à l’horizon :
elle annonce le Jour
de Notre Seigneur Jésus-Christ.
« Relevez la tête, car votre salut est proche ! »