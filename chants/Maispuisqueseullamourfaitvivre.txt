1.
On peut briller ou être éteint
Ou ne fonctionner qu’à l’instinct,
Dealer sa vie et son destin.
On peut parler de tout de rien,
Vivre chez soi en clandestin,
Tricher parfois, faire le malin.

R.
MAIS PUISQUE SEUL L’AMOUR FAIT VIVRE,
MA VIE N’A DE SENS QU’AVEC TOI.
JE VEUX, MON DIEU, TOUJOURS TE SUIVRE
ET NOUS IRONS OÙ TU VOUDRAS. (bis)

2.
On peut vouloir choisir le “Bien”
Et pourtant n’être qu’un “vaut rien”,
Rechercher la Lumière en vain.
On peut dévorer des bouquins
Et pourtant rester sur sa faim,
N’être plus sûr de son chemin.

3.
On peut zoomer, brasser du vent,
Perdre sa vie en la gagnant,
Tricher au jeu impunément.
On peut se dire non-violent
Et décréter inconsciemment
L’état de guerre permanent.

4.
On peut être accro à l’argent,
Toujours vouloir passer devant,
Jouer des coudes effrontément.
On peut, interminablement,
Terminer un mauvais roman
Et commencer son meilleur chant.