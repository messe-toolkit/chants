1.
Avant que mes yeux ne s’ouvrent à l’étal de tes merveilles
Avant que mes yeux, habitués à la nuit, ne découvrent le jour
Avant que mes yeux ne perdent leurs écailles
Comme un bourgeon qui craquerait au printemps de l’amour

R.
TU ÉTAIS LÀ, MON DIEU, MENDIANT DE MOI
TU ÉTAIS LÀ, DISCRET, TU M’ATTENDAIS
TU ÉTAIS LÀ, ET TU M’AIMAIS

2.
Avant que mon coeur ne puise vie à l’eau de ta fontaine
Avant que mon coeur, abreuvé mais tari, ne s’épure à ton jour
Avant que mon coeur ne s’ouvre et se libère
Comme un torrent qui jaillirait au printemps de l’amour

3.
Avant que mes mains n’éclosent au soleil de la justice
Avant que mes mains, si blanches mais flétries, ne s’offrent à ton jour
Avant que mes mains rayonnent ta tendresse
Comme un rameau qui reverdit au printemps de l’amour