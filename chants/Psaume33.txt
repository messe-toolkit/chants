QUAND UN PAUVRE APPELLE LE SEIGNEUR ENTEND.
bis

1.
Je bénirai le Seigneur en tout temps,
Sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
Que les pauvres m’entendent et soient en fête !

2.
Qui regarde vers lui resplendira,
Sans ombre ni trouble au visage.
Un pauvre crie ; le Seigneur entend :
Il le sauve de toutes ses angoisses.

3.
Le Seigneur regarde les justes,
Il écoute, attentif à leurs cris.
Le Seigneur entend ceux qui l’appellent :
De toutes leurs angoisses, il les délivre.

4.
Il est proche du coeur brisé,
Il sauve l’esprit abattu.
Malheur sur malheur pour le juste,
Mais le Seigneur chaque fois le délivre.