R.
PAR DELA MA DETRESSE, SEIGNEUR,
FAIS-MOI, VIVRE SEIGNEUR!

1.
Tous ceux qui me voient me bafouent,
ils ricanent et hochent la tête ;
"Il comptait sur le Seigneur : qu'il le délivre !
Qu'il le sauve puisqu'il est son ami !"

2.
Oui, des chiens me cernent,
une bande de vauriens m'entourent ;
ils me percent les mains et les pieds,
Je peux compter tous mes os.

3.
Ils partagent entre eux mes habits
et tirent au sort mon vêtement.
Mais toi, Seigneur, ne sois pas loin :
ô ma force, viens vite à mon aide !

4.
Mais tu m'as répondu !
Et je proclame ton nom devant mes frères,
Je te loue en pleine assemblée.
Vous qui le craignez, louez le Seigneur.