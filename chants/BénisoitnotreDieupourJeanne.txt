STANCE 1.
Béni soit notre Dieu pour Jeanne de Lorraine,
Bergère qu’il appelle à mener des combats !
La force du Très-Haut transcende sa jeunesse,
Un peuple libéré peut la suivre avec joie.

R.
Béni soit notre Dieu pour Jeanne au coeur fidèle,
Sa flamme dans nos vies jamais ne s’éteindra.

STROPHES

1.
Fille d’une terre où la foi demeure ardente ,
L’enfant s’éveille à Dieu sous le ciel de Domrémy.
L’eau vive de l’Esprit la nourrit d’une espérance,
Et le Berger divin lui prépare un avenir.

2.
Grande est la détresse au royaume de ses pères,
La guerre et le pillage ont partout semé l’horreur.
Mais Jeanne entend des voix qui sont riches de promesses,
Voici le temps venu des matins libérateurs.

3.
A la cour du roi sa parole est de sagesse,
Mais nul ne veut cueillir le bonheur de ses propos :
Bouter les ennemis au-delà de nos frontières,
Par Dieu il faut y croire et surtout chasser leurs maux.

4.
Forte dans sa foi, elle avance la première,
Portant son étendard dans les villes délivrées.
Les pauvres et les petits se confient à sa gouverne,
Ils voient enfin monter un espoir de longue paix.

5.
Comme le Sauveur dans la nuit de sa souffrance,
Voici que des Judas l’ont livrée à prix d’argent.
Les juges et les grands prêtres prononcent la sentence :
Relapse doit mourir à la face des vivants.

6.
Femme aux mains liées sur le bois du sacrifice,
Elle a les yeux fixés sur la croix du Bien-Aimé.
Il est allé lui-même au sommet de l’indicible,
Et Jeanne dit « Jésus » quand son corps est consumé.

7.
Peuple de témoins, nous voyons dans son martyre
Le signe d’une vie qui l’emporte sur le mal.
Nous restent des pourquoi dans un monde qui doit vivre
En reprenant sans cesse la voie du Golgotha.

8.
Sainte Jeanne d’Arc avec Dieu dans la victoire
Nous montre le chemin où la Pâque s’accomplit.
Patronne de la France, elle éclaire notre histoire ;
Heureux qui connaîtra même soif de l’infini !

STANCE 2.
Béni soit notre Dieu pour Jeanne de Lorraine !
Elle a donné sa vie au service du Roi!
Le peuple qu’elle aima se souvient de sa geste,
L’Église n’oublie pas la grandeur de sa foi.

REFRAIN FINAL
Béni soit notre Dieu pour Jeanne au coeur fidèle,
Sa flamme dans nos vies jamais ne s’éteindra !
Béni soit notre Dieu pour Jeanne au coeur fidèle,
Lumière du Seigneur aujourd'hui sur nos pas !