1.
Mon chemin, c’est toi, Seigneur, oui c’est toi !
Mon chemin, c’est toi, Seigneur, oui c’est toi !
Pour sortir de la nuit, pour fêter le matin,
Pour sortir de la nuit, quel serait mon chemin ?

2.
Mon pays, c’est toi, Seigneur, oui c’est toi !
Mon pays, c’est toi, Seigneur, oui c’est toi !
Pour sourire au soleil, au soleil de midi,
Pour sourire au soleil, où serait mon pays ?

3.
Mon amour, c’est toi, Seigneur, oui c’est toi !
Mon amour, c’est toi, Seigneur, oui c’est toi !
Pour brûler sans mourir, sans mourir en plein jour,
Pour brûler sans mourir, qui serait mon amour ?

4.
Mon refrain, c’est toi, Seigneur, oui c’est toi !
Mon refrain, c’est toi, Seigneur, oui c’est toi !
Pour chanter mon pays, pour fêter mon chemin,
Pour danser mon amour, quel serait mon refrain ?