Refrain :
Surtout
Quand il reviendra,
Surtout prévenez-moi
Je voudrais être là
Pour l’accueillir chez moi.

1.
Depuis,
Qu’on parle de lui,
Moi je l’attends aussi,
Viendra-t-il dans ma vie ?
Viendra-t-il aujourd’hui ?

2.
Pourquoi,
Dites-moi, pourquoi,
Tous les gens comme moi,
Ont tant de mal parfois,
À entendre sa voix.

3.
Je sais,
Je doute, je sais.
Et pourtant, je voudrais,
Qu’il soit là pour de vrai,
Comme IL l’a déjà fait.

4.
Je crois,
Je doute et je crois,
Mais, je suis sûr parfois
Qu’il est peut être là,
Et qu’IL veille sur moi.