Alléluia (Acclamation de l'Évangile)
Alléluia, Jésus Seigneur,
Alléluia, nous t'acclamons !
Alléluia, tu parles au cœur,
Alléluia, nous te croyons.
Fils de Dieu, montre-nous ton visage,
Guide-nous au chemin de la vie.

Sanctus
Saint le Seigneur de l'univers,
Saint le Seigneur, Alléluia !
Dieu Créateur, nous te chantons,
Dieu, le Très-haut, nous t'adorons !
Terre et Ciel sont remplis de ta gloire.
Hosanna, béni soit Jésus-Christ !

Anamnèse
Mort sur la Croix, Jésus Sauveur,
Alléluia ! Nous t'acclamons !
Ressuscité, tu es vainqueur,
Alléluia ! Nous t'attendons !
Tu viendras nous ouvrir ton royaume.
Fais lever ta lumière en nos cœurs.