Parce que tu m’as vu, tu crois.
Heureux ceux qui croient sans avoir vu.