R.
Qu'il soit béni, celui qui vient
Nous révéler le nom de Dieu.
Qu'il soit béni, celui qui vient
Réconcilier l´homme avec Dieu.

1.
Qu'il soit béni au nom du Seigneur,
Celui qui vient
Nous faire connaître et nous faire aimer
Le Dieu trois fois Saint.

2.
Gloire et louange à toi, Jésus-Christ,
Le don du Père,
Hosanna au Fils de David,
Au Fils de Dieu.

3.
Tu ne voulais ni holocauste,
Ni sacrifice,
Alors j'ai dit : "Voici je viens faire
Ta volonté."

4.
Jésus victime s'offre lui-même
Pour nos péchés.
Puissance et gloire à l'Agneau de Dieu
Qui nous a sauvés.

1985, Éditions de l´Emmanuel, 89 boulevard Blanqui , 75013 Paris