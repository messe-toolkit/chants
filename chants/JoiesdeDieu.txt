R.
C'EST LA JOIE DE DIEU
AU DEVANT DE TOI
C'EST LA JOIE DE DIEU
QUI VIENT
DEMEURER CHEZ TOI

1.
Quand un sourire éclaire ton visage
Quand la joie du pardon tourne la page
Quand le chant des oiseaux se mêle à ta vie
Quand le soleil paraît après l'orage
Quand la brebie retrouve ses alpages
Quand un arc-en-ciel chasse la pluie

2.
Quand ensemble on construit un nouveau monde
Quand tes gestes se font terre féconde
Quand l'église est en marche grands et petits
Quand les fruits de l'amour au coeur abondent
Quand l'enfant est en toi et toi dans la ronde
Quand tu fais pour aimer le don de ta vie

3.
Quand tu te désaltères à l'évangile
Quand ton humble prière devient fertile
Quand ta pierre angulaire est le Dieu de vie
Quand tu te reconnais pauvre et fragile
Quand il est le potier et toi l'argile
Quand plus rien ne s'accroche en vert-de-gris