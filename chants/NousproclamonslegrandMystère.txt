NOUS PROCLAMONS LE GRAND MYSTÈRE
À TOUS LES PEUPLES RÉVÉLÉ :
AU TEMPS MARQUÉ, DIEU NOTRE PÈRE
EN JÉSUS CHRIST NOUS A PARLÉ.

1.
Il est venu parmi les hommes chercher ce qui était perdu,
leur apporter en sa personne la source vive du Salut.

2.
En souvenir de ses souffrances et de sa gloire près de Dieu,
nous partageons, dans l’espérance la source vive du Salut.

3.
Ce pain vivant, c’est Dieu lui-même qui pour les hommes s’est livré ;
et nous devons, comme il nous aime, les uns les autres nous aimer.