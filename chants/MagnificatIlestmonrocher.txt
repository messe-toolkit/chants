IL EST MON ROCHER, MA CITADELLE,
MON REMPART C’EST LE SEIGNEUR.

1.
Mon âme exalte le Seigneur,
exulte mon esprit en Dieu, mon Sauveur !
MON REMPART C’EST LE SEIGNEUR.

2.
Il s’est penché sur son humble servante ;
désormais, tous les âges me diront bienheureuse.
MON REMPART C’EST LE SEIGNEUR.

3.
Le Puissant fit pour moi des merveilles ;
Saint est son nom !
MON REMPART C’EST LE SEIGNEUR.

4.
Son amour s’étend d’âge en âge
sur ceux qui le craignent.
MON REMPART C’EST LE SEIGNEUR.

5.
Déployant la force de son bras,
il disperse les superbes.
MON REMPART C’EST LE SEIGNEUR.

6.
Il renverse les puissants de leurs trônes,
il élève les humbles.
MON REMPART C’EST LE SEIGNEUR.

7.
Il comble de biens les affamés,
renvoie les riches les mains vides.
MON REMPART C’EST LE SEIGNEUR.

8.
Il relève Israël, son serviteur,
Il se souvient de son amour,
MON REMPART C’EST LE SEIGNEUR.

9.
de la promesse faite à nos pères,
en faveur d’Abraham et de sa race, à jamais.
MON REMPART C’EST LE SEIGNEUR.

10.
Gloire au Père, et au Fils, et au Saint-Esprit,
pour les siècles des siècles
Amen.