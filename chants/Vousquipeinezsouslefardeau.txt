ANTIENNE
Vous qui peinez sous le fardeau
Voyez entre les mains de Jésus-Christ
Passant de ce monde à son Père,
Tout l'univers, les hommes, leurs travaux,
Qu'il veut transfigurer.

R.
Par le mystère de son corps livré,
Il annonce un monde nouveau.
VERSET
Nous attendons une terre nouvelle
Où la justice habitera.