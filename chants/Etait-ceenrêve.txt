1.
Était-ce en rêve,
Était-ce dans l'Esprit ?
Bernard, enfant a vu Jésus.
Il est entré dans le mystère.

2.
Dieu le visite,
Lumière d'un instant,
Bernard s'éveille au don reçu
En cette nuit qui s'illumine.

3.
Goût de la grâce,
Saveur et joie d'en-haut,
Bernard s'enivre du salut,
S'ouvre à la paix que rien n'efface.

4.
Verbe de gloire,
Seigneur Emmanuel,
Tu viens à l'heure inattendue
Attirer l'homme en ton histoire.