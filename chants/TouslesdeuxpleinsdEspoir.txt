R.
TOUS LES DEUX, PLEINS D'ESPOIR
DEVANT TOI, JÉSUS-CHRIST.
TOUS LES DEUX, PLEINS D'ESPOIR,
POUR VIVRE DE TA VIE.

1.
À la croisée de nos deux routes,
nous entendons le même appel :
marcher ensemble à ton écoute,
chercher les pas de l'Éternel.

2.
L’amour nous prend dans sa lumière,
il se nourrit à ton festin.
Fais le grandir sur notre terre,
si longs que soient nos lendemains.

3.
Par ton Esprit dans nos demeures
nos yeux verront la joie fleurir.
Chez nous, Seigneur, tu es à l’oeuvre
et tu prépares l’avenir.