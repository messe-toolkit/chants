Noël, Noël !
Noël, Noël !
Grande est cette fête,
grande est notre joie !
Noël, Noël !
Noël, Noël !
Elle est apparue la bonté de Dieu !
Elle est apparue la douceur du Verbe.

PLEIN DE GRÂCE ET DE VÉRITÉ,
JÉSUS VIENT NOUS SAUVER.

1.
J’écoute ce que dit le Seigneur.
Ce que dit le Seigneur, c’est la paix

2.
Amour et vérité se rencontrent
justice et paix s’embrassent.

3.
La justice marchera devant lui
et ses pas traceront le chemin.