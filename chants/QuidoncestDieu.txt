R.
QUI DONC EST DIEU
POUR NOUS AIMER AINSI ?

1.
Qui donc est Dieu pour nous aimer ainsi
Fils de la terre ?
Qui donc est Dieu si démuni, si grand,
Si vulnérable ?

2.
Qui donc est Dieu pour se lier d’amour
À part égale ?
Qui donc est Dieu, s’il faut pour le trouver,
Un coeur de pauvre ?

3.
Qui donc est Dieu, s’il vient à nos côtés
Prendre nos routes ?
Qui donc est Dieu, qui vient sans perdre coeur
À notre table ?

4.
Qui donc est Dieu, que nul ne peut aimer
S’il n’aime l’homme ?
Qui donc est Dieu, qu’on peut si fort blesser
En blessant l’homme ?

5.
Qui donc est Dieu pour se livrer perdant
Aux mains de l’homme ?
Qui donc est Dieu, qui pleure notre mal
Comme une mère ?

6.
Qui donc est Dieu, qui tire de sa mort
Notre naissance ?
Qui donc est Dieu pour nous ouvrir sa joie
Et son royaume?

7.
Qui donc est Dieu pour nous donner son Fils,
Né de la femme ?
Qui donc est Dieu qui veut, à tous ses fils
Donner sa Mère ?

8.
Qui donc est Dieu pour être notre pain
À chaque Cène ?
Qui donc est Dieu pour appeler nos corps
Jusqu’en sa gloire ?