R.
ECOUTE LE SEIGNEUR,
SA PAROLE GERMERA.
ECOUTE LE SEIGNEUR,
SUR LA TERRE TU VIVRAS.

1.
Dieu te parle de sa Vie,
viens puiser à sa fontaine.
Par amour il te conduit
vers les eaux qui font renaître.

2.
Dieu te parle de grandir
comme un arbre en pleine terre.
Laisse en toi l'amour fleurir,
porte fruit dans sa lumière.

3.
Dieu te parle de l'aimer,
dans tes frères il se révèle.
Ne crains pas de tout risquer,
tu verras qu'il est fidèle.