SEIGNEUR, TON AMOUR SOIT SUR NOUS,
COMME NOTRE ESPOIR EST EN TOI !

1.
Criez de joie pour le Seigneur, hommes justes !
Hommes droits, à vous la louange !
Jouez pour lui sur la harpe à dix cordes.
Chantez-lui le cantique nouveau.

2.
Oui, elle est droite, la parole du Seigneur ;
il est fidèle en tout ce qu’il fait.
Il aime le bon droit et la justice ;
la terre est remplie de son amour.

3.
Dieu veille sur ceux qui le craignent,
qui mettent leur espoir en son amour,
pour les délivrer de la mort,
les garder en vie aux jours de famine.