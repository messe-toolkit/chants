Célébrons la naissance de la Mère du
Sauveur : de sa chair surgira le rameau de
Jessé. Béni soit le fruit de ses entrailles !
Béni soit le Sauveur !

Psaume 64, 1-2, 5-6, 9-14

C’est toi, Seigneur qu’il est bon de chanter,
sur la montagne de Sion.

Que nous accomplissions nos voeux devant
toi, à Jérusalem en ta présence, toi qui
écoutes la prière.

Bienheureux celui que tu choisis et fais vivre
près de toi dans ta demeure !

Tu nous rassasies de bonheur en ta maison,
en ton temple, lieu de ta sainteté.

Célébrons la naissance de la Mère du Sauveur...

La porte du matin et la porte du soir
retentissent de cris de joie.

Tu viens visiter la terre et tu la fais danser
de joie.

Tu multiplies ses richesses : le fleuve du
Seigneur voit déborder ses flots, tu fais
germer le froment pour les hommes.

Voici que tu prépares la terre : tu abreuves
les sillons puis tu les aplanis.

Célébrons la naissance de la Mère du Sauveur...

Sous le flot de tes pluies, tu fécondes la
terre et tu bénis ses semences.

Tu couronnes l’année de tes bontés, sur ton
passage, la sève ruisselle.

Les pâturages du désert ruissellent, les
collines rayonnent d’allégresse.

Les prairies se revêtent de troupeaux, les
vallées se drapent de froment ; tout chante
et crie de joie !

Célébrons la naissance de la Mère du Sauveur...