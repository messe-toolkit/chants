PRENDS PITIÉ DE NOUS

Antienne
PRENDS PITIÉ DE NOUS, SEIGNEUR !
PRENDS PITIÉ DE NOUS, PÉCHEURS !
PRENDS PITIÉ DE NOUS !

1.
Seigneur Jésus envoyé par le Père
pour guérir et sauver les hommes,
Prends pitié de nous.

2.
Ô Christ venu dans le monde
appeler tous les pécheurs,
Prends pitié de nous.

3.
Seigneur élevé dans la gloire du Père
où tu intercèdes pour nous,
Prends pitié de nous.

ACCLAMATION DE L'ÉVANGILE

R.
ALLÉLUIA! BONNE NOUVELLE!
ALLÉLUIA ! ALLÉLUIA !
ALLÉLUIA! BONNE NOUVELLE!
ALLÉLUIA ! ALLÉLUIA !

1.
Tu es le chemin, la vérité et la Vie !
ALLÉLUIA ! ALLÉLUIA !
Tu es la Parole de la Vie éternelle !
ALLÉLUIA ! ALLÉLUIA !

2.
Tu es la Lumière qui éclaire nos routes,
ALLÉLUIA ! ALLÉLUIA !
Ta Parole en nous est la semence de Vie,
ALLÉLUIA ! ALLÉLUIA !

ACCLAMATION POUR L'EUCHARISTIE

Saint le Seigneur
SAINT, LE SEIGNEUR ! SAINT, LE SEIGNEUR !
SAINT, LE SEIGNEUR, DIEU DE L’UNIVERS !
SAINT, LE SEIGNEUR ! SAINT, LE SEIGNEUR !
SAINT, LE SEIGNEUR, DIEU DE L’UNIVERS !

Le ciel et la terre sont remplis de ta gloire.
Hosanna au plus haut des cieux.

Béni soit celui qui vient au nom du Seigneur.
Hosanna au plus haut des cieux.

Anamnèse
TU ÉTAIS MORT ! TU ES VIVANT!
Ô VIENS JÉSUS, DANS LA GLOIRE !

Doxologie
Par Jésus Christ, dans l’Esprit Saint,
Honneur à toi, notre Père !
AMEN! AMEN! AMEN! ALLÉLUIA !

Épiclèse sur les offrandes
ESPRIT DE DIEU, ESPRIT DE VIE,
VIENS SANCTIFIER NOS OFFRANDES!

Épiclèse sur l’assemblée
ESPRIT DE DIEU, ESPRIT D’AMOUR,
FAIS L’UNITÉ DE TON PEUPLE!

CHANT DE PAIX

R.
“JE VOUS LAISSE LA PAIX,
JE VOUS DONNE MA PAIX.
JE NE VOUS LA DONNE PAS
COMME LE MONDE VOUS LA DONNE.”

1.
Seigneur Jésus, tu connais le poids de nos misères,
Donne-nous ta paix !

2.
Seigneur Jésus, tu connais la foi de ton église,
Donne-lui ta paix !

3.
Seigneur Jésus, tu viens apporter la paix sur terre,
Donne-lui ta paix !

CHANT DE LA FRACTION DU PAIN

R.
IL Y A UN SEUL PAIN;
NOUS FORMONS UN SEUL CORPS;
CAR NOUS ALLONS PARTAGER.
LE MÊME PAIN QUE DIEU NOUS DONNE.

1.
Agneau de Dieu, qui enlèves le péché du monde,
prends pitié de nous.

2.
Agneau de Dieu, qui enlèves le péché du monde,
prends pitié de nous.

3.
Agneau de Dieu, qui enlèves le péché du monde,
donne-nous la paix.