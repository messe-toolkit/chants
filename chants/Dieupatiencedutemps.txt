DIEU, PATIENCE DU TEMPS,
QUE TON REGARD LÈVE L’AURORE
DE CETTE NUIT QUI NE FINIT.
OUVRE TON CIEL À MES IMPASSES.

1.
Au loin s’en est allé le jour et sa lumière
Dans la solitude qui enferme ma voix.
Comment dire ton nom et l’offrir au silence ?
Devant ces murs fermés de portes sans visages
Je suis seul aujourd’hui et mon désert grandit.
Entendras-tu le cri que je porte vers toi ?

2.
Au loin s’en est allé le jour et sa lumière
Avec l’injustice de ce mal incompris.
Comment dire ton nom et l’offrir au silence ?
Je sens s’ouvrir en moi la faille de mes doutes,
Les couleurs du soleil n’y portent plus leurs feux.
Entendras-tu le cri que je porte vers toi ?

3.
Au loin s’en est allé le jour et sa lumière
Lorsque descend l’ombre de l’amour oublié.
Comment dire ton nom et l’offrir au silence ?
Je sais les jours perdus, l’audace des prières
Appelant au pardon celui qui donne tout.
Entendras-tu le cri que je porte vers toi ?

4.
Au loin s’en est allé le jour et sa lumière
Quand vient la souffrance, la douleur me conduit.
Comment dire ton nom et l’offrir au silence ?
Mon corps oublie l’Esprit, la force de ton souffle,
Je m’épuise sans fin à soulever ta croix.
Entendras-tu le cri que je porte vers toi ?