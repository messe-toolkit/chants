R.
Jouez pour le Seigneur, chantez pour Lui,
Car Il a fait des merveilles,
Car Il a fait des merveilles.

1.
Il a choisi le pauvre pour habiter chez lui,
Le plus petit des hommes, il en fait son ami.

2.
Il donne sa Parole et ne la reprend pas
Pour qu'elle nous transforme et trace notre voie.

3.
Il nous invite à table, il aime ses amis,
Sa porte est grande ouverte, on est si bien chez lui.

4.
Lumière sur la route, sa force nous conduit,
Il entre dans sa gloire, nous sommes près de lui.

5.
Il met dans notre terre le souffle de l´Esprit,
En lui tout est lumière et peut reprendre vie.