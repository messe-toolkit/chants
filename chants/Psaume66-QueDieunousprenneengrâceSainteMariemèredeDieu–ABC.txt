QUE DIEU NOUS PRENNE EN GRÂCE
ET QU’IL NOUS BÉNISSE !

1.
Que son visage s’illumine pour nous ;
et ton chemin sera connu sur la terre,
ton salut, parmi toutes les nations.

2.
Que les nations chantent leur joie,
car tu gouvernes le monde avec justice ;
sur la terre, tu conduis les nations.

3.
La terre a donné son fruit ;
Dieu, notre Dieu, nous bénit.
Que la terre tout entière l’adore !