Notre Père qui es aux cieux
que ton nom soit sanctifié,
que Ton règne vienne
que ta volonté soit faite sur la terre comme au ciel,
donne-nous aujourd’hui notre pain de ce jour,
pardonne-nous nos offenses
comme nous pardonnons aussi
à ceux qui nous ont offensés,
et ne nous laisse pas entrer en tentation,
mais délivre nous du mal, Amen.