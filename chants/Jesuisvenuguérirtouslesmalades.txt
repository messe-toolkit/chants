1.
Je suis venu guérir tous les malades,
Tous les blessés du coeur,
Pour leur bonheur.

2.
C’est la miséricorde que je désire,
Est-il meilleur présent
Pour maintenant ?

3.
Je n’ai pas appelé rien que les justes,
Mais j’offrirai mon coeur
A tout pécheur.