R.
Jésus, porteur d’espérance,
Christ au milieu de nous,
Par toi nous sommes des coeurs nouveaux,
Bâtisseurs de paix.
Jésus, Soleil de justice,
Christ au milieu de nous,
Par toi se lèvent des temps nouveaux :
Paix du Dieu d’amour.
En entrée ou chant d’évangile

1er dimanche de l’Avent

1.
Voici le temps de revenir à notre Dieu :
À son image de Vivant il nous a faits,
Et nous croyons qu’il est toujours à nos côtés
Pour édifier nouvelle terre et nouveaux cieux.
Que nos épées deviennent socs pour des charrues !
Chassons les armes qui détruisent et qui nous tuent !
(Is. 2, 1-5)

2.
L’heure est venue de nous sortir de nos sommeils,
La nuit prend fin, le jour annonce un avenir.
Revêtons-nous pour les combats qu’il faut tenir !
Entendrons-nous monter les cris et les appels ?
Le Fils de l’homme nous choisit pour ses veilleurs ;
Tenons-nous prêts, voici qu’il vient sur les hauteurs !
(Rm 13, 11-14)

2ème dimanche de l’Avent
(strophes 1 et 3)

3.
Jean le Baptiste a proclamé dans le désert :
Préparez-vous, tracez la voie pour le Seigneur !
Changez de vie, retournez-vous du fond du coeur
Car le Royaume du Dieu saint vous est offert !
En revivant par le baptême dans l’Esprit
Vous serez l’arbre qui produit beaucoup de fruits.
(Is 40, 1-5 et Mc 1, 1-8 ; Mt 3, 1-12)
3ème dimanche de l’Avent
strophes 1 et 4.

4.
Aimés de Dieu, crions de joie pour le Sauveur !
Il est Celui qui doit venir nous libérer.
Dans nos prisons que toute peur soit dissipée,
Car il relève les aveugles et les boiteux !
Bonne nouvelle est annoncée aux plus petits ;
Heureux celui qui garde foi dans le Messie !
(Soph. 3, 14-18 ; Mt 11, 2-11)
4ème dimanche de l’Avent
(strophes 4 et 5)

5.
Avec Marie disant son oui au Dieu Très-Haut
Nous accueillons Celui qui vient dans notre chair :
Verbe éternel au nom béni, l’Emmanuel,
Fruit de l’Esprit, Dieu parmi nous incognito.
Par cet enfant devenu grand à tout jamais
Nous bâtissons jour après jour Cité de paix.
(Lc 1, 26-38)
En envoi, chaque dimanche

6.
Bâtir la paix comme on bâtit un grand vaisseau,
Avec la foi des pèlerins vers l’infini :
Porter le poids des durs labeurs, des longues nuits,
Garder l’espoir qu’il voguera longtemps sur l’eau.
Bâtir la paix, briser le mur des divisions,
Ouvrir des voies dans les Églises et les Nations.