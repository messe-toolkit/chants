La voie est ouverte, alléluia,
C’est la Pâque de Jésus !
L’appel du Seigneur nous a saisis
À l’heure où l’espoir s’était enfui :
Christ a surgi vainqueur du tombeau !
Alléluia !

La vie se rebelle, alléluia,
C’est la Pâque de Jésus !
Déjà nos prisons sont ébranlées,
L’étau de la mort s’est desserré :
Christ a fendu les eaux de la nuit !
Alléluia !

Le chemin est libre, alléluia,
C’est la Pâque de Jésus !
Quittant notre exil il faut partir,
Pour nous le désert va refleurir :
Christ a franchi le seuil du jardin !
Alléluia !

L’amour se révèle, alléluia,
C’est la Pâque de Jésus !
Le temps est venu de témoigner,
Le Verbe jamais n’est enchaîné :
Christ au milieu de nous est vivant !
Alléluia ! Alléluia ! Alléluia !