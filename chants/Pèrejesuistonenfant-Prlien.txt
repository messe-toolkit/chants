R.
Père, je suis ton enfant,
Ton enfant bien-aimé,
Et je viens vers toi me jeter dans tes bras.

1.
Comme un enfant dans les bras de sa mère,
Je m’abandonne, je m’abandonne à toi !

2.
Dieu de pardon, d’amour et de tendresse.
Tu es lumière, lumière dans ma vie !

3.
C’est en Jésus que tu t’es fait connaître.
Qui le regarde contemple ton visage.