A - L’ÉGLISE, LA BASILIQUE

1.
Voici la demeure de Dieu parmi les hommes,
sur les flots de l’Histoire, le Très-Haut la maintient,
sa patience la façonne,
son amour la soutient.

2.
Venez dans sa maison lui rendre grâce,
la maison de prière dont nos pères ont fait voeu :
dans le coeur de notre Dieu chacun a sa place,
accueillez son amour et vous serez heureux !

3.
Dans le coeur immaculé de l’Église,
Jésus rayonne en son Eucharistie.
Venez à lui, vous les pauvres, les petits,
En Lui, exultez dans le Saint Esprit !

B – LE SALUT, MYSTÈRE PASCAL

4.
Ses bras en croix sur la cité
attirent à lui le monde entier.
Ils tracent sur l’humanité
le signe béni qui nous donne la paix

5.
L’abîme appelant l’abîme,
au plus profond de nos misères
jusqu’aux sommets de vie divine,
sa voix nous tire des enfers.

6.
Ô vous qui cherchez son visage,
adorez le Seigneur, éblouissant de sainteté,
sa miséricorde s’étend d’âge en âge,
Il fait de vous ses bien-aimés !

C – LA JOIE DE LA MISÉRICORDE

7.
Vous qui veillez dans le silence
en sa présence au long des nuits,
vous puisez dans la confiance
l’Eau et le Sang qui nous donnent sa vie.

8.
Plus que la voix des eaux profondes,
superbe est le Seigneur dans les hauteurs !
Mais sa tendresse nous inonde,
embrassant le tréfond de nos coeurs.

9.
Entrez dans la joie du bon Pasteur
Lui qui vous porte sur son Coeur
à la table du Père, notre demeure,
Jésus invite les pécheurs.

D – LA MISSION

10.
Au Coeur Sacré, offre un coeur consacré :
baptisé dans l’Esprit, sanctifié dans le feu,
l’Onction de son amour en lui t’a confirmé
pour porter l’Evangile du Royaume de Dieu.

11.
Portez dans ce monde en flammes,
le feu qui brûle sans consumer,
qui embrase et console les âmes,
et transfigure la nuit du péché.

12.
Acclamez le Seigneur, terre entière !
allez à lui avec des chants de joie !
Gloire à toi, ô Christ, Roi de l’univers,
Pour les siècles des siècles, amen, alléluia !
(en Carême : Hosa-a-na !)