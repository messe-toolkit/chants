1.
Comme le jour perce la nuit
pour un renouveau d’espérance,
puissions-nous raviver nos esprits indolents
qui voudraient contempler le Soleil levant.

2.
Comme nos voix cherchent l’accord
pour faire éclater la louange,
puissions-nous percevoir la Parole de Dieu
à travers le brouillard des temps et des lieux.

3.
Et comme l’eau se mêle au vin
pou le sacrement de l’alliance,
puissions-nous être unis à la divinité
de celui qui a pris notre humanité.