1er nocturne
Psaume 18 : Il vint du ciel un bruit soudain comme un vent violent. Alléluia !
Psaume 28 : Seigneur, ton souffle en nous est bon et vivifiant. Alléluia !
Psaume 32 : Le vent souffle où il veut, tu entends le bruit qu’il fait
mais tu ne sais pas d’où il vient ni où il va. Alléluia !
Répons :
refrain : L’Esprit témoigne dans nos coeurs que nous sommes enfants de Dieu
verset 1 : C’est l’Esprit du Fils qui s’écrie : Abba, notre Père. Alléluia, Alléluia !
verset 2 : Dieu ne donne pas un esprit de crainte, mais l’esprit de force et d’amour.