R.
NOUS TE CHANTONS, SEIGNEUR, POUR FRANÇOIS NOTRE FRÈRE,
IL A SUIVI LES PAS DE TON FILS, JÉSUS CHRIST.
DANS SON COEUR A BRÛLÉ LE FEU DU SAINT-ESPRIT,
TU L’ACCUEILLES EN TA JOIE, Ô ROI DE L’UNIVERS.

1.
Pour répandre le grain de la bonne nouvelle,
Sans trêve il sillonna les chemins de l’Ombrie.
Puis l’ont rejoint des frères animés comme lui,
En grande humilité, de la joie fraternelle.

2.
Lui qui riche était né choisit la pauvreté,
Frère et ami de tous, les brigands, les lépreux.
En tout être créé, François le bienheureux,
De Dieu, le Créateur, reconnut la beauté.

3.
Il voulut à Greccio voir de ses yeux de chair
L’enfant de Bethléem en son humanité ;
À l’Alverne, François, par l’amour transporté,
Des plaies du crucifié fut marqué dans sa chair.

4.
Dieu qui es le seul Saint, tu es notre espérance,
Tu es trois, tu es un, Seigneur, en même temps.
Avec ton Fils béni, et l’Esprit bienfaisant,
Nous te rendons tout bien par nos chants et nos danses.