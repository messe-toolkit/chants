Refrain pour le temps ordinaire :
Acclame le Seigneur ô mon âme, magnifie son amour au matin. (bis)

Refrain pour le temps de l'Avent :
Voici la voix qui crie dans le désert : "préparez les chemins du Seigneur". (bis)

Béni soit le Seigneur, le Dieu d'Israël,
Qui visite et racheté son peuple.
Il a fait surgir la force qui nous sauve,
Dans la maison de David, son serviteur.
Comme il l'avait dit par la bouche des saints,
Par ses prophètes, depuis les temps anciens :
Salut qui nous arrache à l'ennemi,
A la main de tous nos oppresseurs,
Amour qu'il montre envers nos pères,
mémoire de son alliance sainte,
Serment juré à notre père Abraham
De nous rendre sans crainte,
Afin que, délivrés de la main des ennemis, +.
Nous le servions dans la justice et la sainteté,
En sa présence, tout au long de nos jours.
Et toi, petit enfant, tu seras appelé
prophète du Très-Haut : *
Tu marcheras devant, à la face du Seigneur,
et tu prépareras ses chemins
Pour donner à son peuple de connaître le salut
Par la rémission de ses péchés,
Grâce à la tendresse, à l'Amour de notre Dieu,
Quand nous visite l'astre d'en haut,
Pour illuminer ceux qui habitent les ténèbres
et l'ombre de la mort, *
Pour conduire nos pas au chemin de la paix.