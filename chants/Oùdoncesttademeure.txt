1.
Où donc est ta demeure,
Agneau de Dieu qui nous invites ?
Est-il enfin la dixième heure
pour le disciple à ta recherche ?
Car nul ne sait le jour ni l'heure
où tu viendras nous dire :
Venez et voyez !

2.
La joie de ta rencontre
est la clarté qui transfigure ;
Est-elle vive au coeur du monde
depuis ta Pâque de lumière ?
Révèle-toi plus fort que l'ombre,
toi dont l'Esprit murmure :
Jésus est Seigneur !

3.
Remplis de ta présence,
Dieu qui habite nos aurores,
nous annonçons ta joie brûlante
à tout vivant qui te recherche.
Toi seul peux dire où prend naissance
le cri de tes apôtres :
Heureux ceux qui croient !

4.
Seigneur, comment te suivre,
avec la foi des pécheurs d'hommes ?
Pendant la nuit des barques vides,
nous voulons croire à tes mains pleines.
Passe à nouveau sur notre rive
et clame nous encore :
Jetez les filets !

5.
Au seuil de ta demeure,
ta croix, Jésus, nous fera signe
car tout apôtre aura son heure
comme toi-même as eu la tienne.
Reste avec nous, Dieu notre Maître
pour dire en tes disciples :
Salut, Croix de vie !