1.
Silence de Dieu, au jardin d’agonie
Silence de Dieu, qui rend la nuit plus noire
Silence de Dieu quand la coupe est à boire
Tu es l’enfantement d’une autre vie
Où la mort est changée, un matin, en victoire.
Dieu, berger du silence.

2.
Silence de Dieu, quand l’arbre meurt en croix
Silence de Dieu, tu deviens cette sève
Silence de Dieu qui vit et qui relève
Tu donnes le fruit mûr du Golgotha
Qui germe en son tombeau, se redresse et se lève.
Dieu, berger du silence.

3.
Silence de Dieu qui alourdit nos croix
Silence de Dieu, au temps de nos souffrances
Silence de Dieu qui ressemble à l’absence
Tu es saison d’hiver et de vents froids
Où germe en notre sol, lentement, ta semence.
Dieu, berger du silence.

4.
Silence de Dieu, dans la vie de Marie
Silence de Dieu, bonheur caché en elle
Silence de Dieu, dur aux pleurs maternelles
Tu as choisi le secret de la nuit
À l’heure d’enfanter la Parole Éternelle.
Dieu, berger du silence.