1.
Quand se lèvera-t-il
Le soleil de justice ?
Au cœur des opprimés
Au cœur des mal-aimés ?
Lorsque hurlent les loups
Se serrent les troupeaux,
Lorsque claque le fouet
Il faut tendre le dos.
Quand se lèvera-t-il
Le soleil de justice ?
Quand se lèvera-t-il
Le soleil de Dieu ?

2.
Quand se lèvera-t-il
Le soleil de l'été ?
Se réchauffera-t-il
Notre monde gelé ?
Dans les yeux des enfants
La joie ne brille plus.
Dans les rues des cités
Les chansons se sont tues.
Quand se lèvera-t-il
Le soleil de l'été ?
Quand se lèvera-t-il
Le soleil de Dieu ?

3.
Quand se lèvera-t-il
Le soleil de l'espoir ?
Sur le rêve ancestral,
Sur les cris dans le noir ?
Aux appels à la paix
Répondent les tambours,
À l'envol des colombes
Jaillissent les vautours.
Quand se lèvera-t-il
Le soleil de l'espoir.
Quand se lèvera-t-il
Le soleil de Dieu ?

4.
Quand se lèvera-t-il
Le vent de liberté
Sur la foule sans nom
Des esclaves enchaînés ?
Au signal du marteau
Se ferment les prisons.
Sur la voix qui surgit
Se pose le bâillon.
Quand se lèvera-t-il
Le vent de liberté ?
Quand se lèvera-t-il
Le soleil de Dieu ?