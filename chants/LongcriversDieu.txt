STANCE
Long cri vers Dieu dans la détresse,
Mains levées, cœur ouvert,
Remise totale au Père,
Force de la faiblesse,
Jésus, notre prière,
Ouvre-nous le chemin vers le Père.

R.
Ouvre-nous le chemin vers le Père.

1.
Toi qui as dit : « Demandez et vous recevrez »,

2.
Toi qui as dit : « Celui qui cherche trouve »,

3.
Toi qui as dit : « A celui qui frappe, on ouvrira »,

4.
Toi qui as dit : « Demandez l'Esprit, il vous sera donné ».