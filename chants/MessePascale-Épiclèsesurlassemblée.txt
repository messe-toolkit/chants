Envoie, Seigneur, ton Esprit de sainteté sur ton peuple ici rassemblé.

PÈRE TRÈS SAINT, ENVOIE TON ESPRIT
POUR QUE TON PEUPLE ICI RASSEMBLÉ
SOIT UN SEUL CORPS DANS LE MÊME ESPRIT
POUR TA GLOIRE.