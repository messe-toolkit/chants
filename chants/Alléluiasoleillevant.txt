R.
ALLELUIA, ALLELUIA, ALLELUIA
ALLELUIA, ALLELUIA, ALLELUIA

1.
Veilleur du jour, Dieu dans la brise, Parole promise
Soleil levant, Verbe de feu, Parole de Dieu
Un premier-né, l'étoile brille, Parole fragile
Soleil levant, Verbe de feu, Parole de Dieu

2.
Faim de silence, vie du désert, Parole de chair
Soleil levant, Verbe de feu, Parole de Dieu
Joie du pardon renouvelé, Parole donnée
Soleil levant, Verbe de feu, Parole de Dieu

3.
Heureux les pauvres, les coeurs meurtris, Parole de vie
Soleil levant, Verbe de feu, Parole de Dieu
Ressuscité, matin du monde, Parole féconde
Soleil levant, Verbe de feu, Parole de Dieu