PAR TOI DANS NOTRE CHAIR NOUS VIVONS DU MÊME ESPRIT;
POUR ÊTRE UN MÊME CORPS TON AMOUR NOUS RÉUNIT.

1.
Corps de Jésus Christ,
Pain de Dieu à notre table,
Nous t’avons reçu dans nos mains.
Corps de Jésus Christ,
Pain donné pour le partage,
Fais grandir en nous la vraie faim.

2.
Sang de Jésus Christ,
Vin des Noces du Royaume,
Tu remplis nos coeurs de ta joie.
Sang de Jésus Christ,
Vin précieux qui réconforte,
Tu mets du soleil dans nos voix.

3.
Paix de Jésus Christ,
Don qui marque les apôtres,
Fais passer ta brise en nos vies.
Paix de Jésus Christ,
Don du Père à tous les hommes,
Souffle sur nos terres aujourd’hui !