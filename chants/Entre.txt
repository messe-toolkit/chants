1.
Entre les cris, les larmes
Entre ces paroles que je n’comprends pas
Entre les guerres, les drames
Entre tout ça…
Entre les joies, les fêtes
Entre mon coeur qui n’arrête pas
De chanter à tue-tête
Entre tout ça…

R.
DANS LE SECRET DE MON COEUR.
ENTRE LE PIRE ET LE MEILLEUR.
JE TE CONFIE, Ô SEIGNEUR,
MA VIE ENTRE TES MAINS. (bis)

2.
Entre les peurs, les doutes
Entre l’avenir qui est loin de moi
Entre les choix, les routes
Entre tout ça…
Entre désirs et rêves
Entre la vie que je porte en moi
Ces envies sur mes lèvres
Entre tout ça…

3.
Entre l’enfant, l’adulte
Entre l’image que l’on a de moi
Tout ce qui me bouscule
Entre tout ça…
Entre mes espérances
Entre l’amour que je garde en moi
Ces sourires et ces danses
Entre tout ça…

Coda
« Entre dans mes mains
Entre dans mes yeux. »
« Vois, je t’ai gravé sur la paume de mes mains
Vois, je t’aime…
Tu as du prix à mes yeux. »