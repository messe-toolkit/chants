R.
Qu'importe le pays d'où tu viens.
Et le temps qu'il a fallu pour trouver ton chemin.
C'est le cri de ton cœur qui compte pour Dieu.
Ce n'est pas la couleur de ta peau, ni de tes yeux !

1.
Il n'était pas du bon côté,
le Centurion de l'Évangile
dans ce pays, dans cette ville
qu'il avait mission d'occuper ;
mais il savait se faire aimer,
même à sa place d'officier.
Et pour sauver son serviteur,
il s'est donné de tout son cœur.

2.
Ainsi les dieux du centurion
n'abritaient pas la même histoire.
Mais il a changé de mémoire
en découvrant l'autre horizon,
car il a su dire à Jésus,
alors que tout semblait perdu :
« si tu le veux, dès aujourd'hui,
mon serviteur sera guéri ! »

3.
On se souvient du centurion
qui disait : « je ne suis pas digne
d'oser enfin lui faire un signe
et l'accueillir en ma maison »…
Mais d'autres viendront comme lui,
de tous côtés, de tous pays,
car c'est aux gens du monde entier
que la Nouvelle est annoncée !