R.
NOTRE DAME DU BON PORT,
SAUVE-NOUS DE TOUTE MORT,
NOTRE DAME DE LA MER,
PAIX DU CIEL EN NOS ENFERS !
NOTRE DAME DE LA MER,
PAIX DU CIEL EN NOS ENFERS !

1.
Combien de capitaines et combien de marins
Se sont tournés vers toi au milieu des tempêtes !
Tu les as écoutés, Étoile du matin !
Et tu changeas leurs cris en joyeux chants de fête !

2.
Combien de malheureux et combien d’oubliés,
Deviennent des rebuts, rejetés comme épaves !
Sors-les de cet enfer, Espoir des égarés !
Porte-les sur ton coeur et détruis leurs entraves !

3.
Combien ont décidé de franchir l’océan,
Tout seuls et loin de Dieu, sans amour ni repères !
Tu leur as évité la mort et le néant,
Tu leur as révélé tout l’amour d’une Mère !

4.
Combien d’enfants perdus et combien de mendiants
N’ont plus aucun espoir d’échapper au déluge !
Tu les as dirigés vers le Soleil levant,
De ton grand manteau bleu, ils ont fait leur refuge.

5.
Combien prennent la mer sans savoir où ils vont ?
Combien de grands projets sont la proie d’un naufrage !
Marie, tiens-toi debout, là-bas, à l’horizon,
Qu’éclate la beauté qui remplit ton Visage !

6.
Combien sont revenus, libérés du danger,
Plus forts et plus confiants, et le coeur plein de rêves !
Combien sont revenus pour vivre et pour aimer !
Étoile de la mer, dans la joie, tout s’achève !