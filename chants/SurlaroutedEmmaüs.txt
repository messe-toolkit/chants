1.
Sur la route d'Emmaüs
Marchent deux disciples
Depuis la mort de Jésus
Les voilà, tout tristes.
Ils en parlent tous les deux
quand quelqu'un s'approche :
C'est Jésus qui est près d'eux
Mais sans qu'ils le sachent

2.
« De quoi parlez-vous tous deux,
avec l’air si triste?
- De Jésus de Nazareth,
un très grand prophète.
Mais nos chefs l’ont crucifié
sur une colline.
Tout le bien qu’il avait fait,
ainsi se termine. »

3.
« Vous n’avez donc rien compris
à ce qui se passe:
Il fallait que le Messie
porte nos souffrances.
Il devait, après ce temps,
entrer dans sa gloire.
Maintenant il est vivant !
Pourrez-vous le croire? »

4.
Ils arrivent en parlant
en vue du village.
L’inconnu a fait semblant
d’aller en voyage.
Ils s’écrient : « Reste avec nous,
car le soir approche. »
Et Jésus entre chez eux ;
il mange à leur table.

5.
Alors Jésus prend du pain
et leur partage.
Leurs yeux s’ouvrent à cet instant :
ils le reconnaissent.
Leur coeur était tout brûlant
d’avoir vu leur Maître.
Ils repartent en courant
dire la nouvelle.