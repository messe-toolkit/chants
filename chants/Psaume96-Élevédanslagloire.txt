ÉLEVÉ DANS LA GLOIRE,
CHRIST EST SEIGNEUR !

1.
Le Seigneur est roi ! Exulte la terre !
Joie pour les îles sans nombre !
Justice et droit sont l’appui de son trône.

2.
Les cieux ont proclamé sa justice,
et tous les peuples ont vu sa gloire.
À genoux devant lui, tous les dieux !

3.
Tu es, Seigneur, le Très-Haut
sur toute la terre :
tu domines de haut tous les dieux.