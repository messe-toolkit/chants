1.
Jésus, fils du Dieu vivant,
Tu nais dans une étable.
Joyeux, courons vers l'Enfant
De qui viendra la Pâque.

2.
Jésus, l'Envoyé de Dieu,
Le Verbe de lumière,
Ton jour fait briller le feu
Qui chasse les ténèbres.

3.
Jésus, Frère des bergers,
Tu es la joie des humbles.
Toi seul peux nous amener
À vivre en Dieu sans crainte.

4.
Jésus, Fruit des temps nouveaux,
Marie te donne au monde.
La paix, signe du Très-Haut,
Fleurit nos terres d'ombre.

R.
Gloria in excelsis Deo !