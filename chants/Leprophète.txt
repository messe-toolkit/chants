Le soleil se lèvera-t-il sur un monde endormi ?
Quand l’amour se révélera, y aura-t-il encore des hommes debout ?
Je lève les yeux et ne vois que chaos, l’injustice et la violence.
Le monde se meurt et le monde en sourit.
L’horloge est à l’insouciance.
Mais voilà, je ne peux plus me taire.
Ces paroles m’ont traversé.
Ouvrez vos cœurs et regardez.
Le lys des champs, l’oiseau qui vole.
Croyez que l’amour est la clé d’une nouvelle humanité.