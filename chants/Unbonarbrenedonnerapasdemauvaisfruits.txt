Antienne - stance
Un bon arbre ne donnera pas de mauvais fruits ; un mauvais arbre ne donnera pas de bons fruits.

R.
Sit nomen Domini benedictum.
Nunc et in saecula.

Versets

Ps 32

1.
Criez de joie pour le Seigneur, hommes justes !
Hommes droits, à vous la louange !
Rendez grâce au Seigneur sur la cithare,
jouez pour lui sur la harpe à dix cordes.

2.
Oui, elle est droite, la parole du Seigneur ;
il est fidèle en tout ce qu'il fait.
Il aime le bon droit et la justice ;
la terre est remplie de son amour.

3.
Heureux le peuple dont le Seigneur est le Dieu,
heureuse la nation qu'il s'est choisie pour domaine !
Du haut des cieux, le Seigneur regarde :
il voit la race des hommes.

4.
La joie de notre coeur vient de lui,
notre confiance est dans son nom très saint.
Que ton amour, Seigneur, soit sur nous
comme notre espoir est en toi !

Ou bien

Ps 102

1.
Bénis le Seigneur, ô mon âme,
bénis son nom très saint, tout mon être !
Bénis le Seigneur, ô mon âme,
n'oublie aucun de ses bienfaits !

2.
Car il pardonne toutes tes offenses
et te guérit de toute maladie ;
il réclame ta vie à la tombe
et te couronne d'amour et de tendresse ;

3.
Le Seigneur est tendresse et pitié
lent à la colère et plein d'amour ;
il n'agit pas envers nous selon nos fautes,
ne nous rend pas selon nos offenses.

4.
Aussi loin qu'est l'orient de l'occident,
il met loin de nous nos péchés ;
comme la tendresse du père pour ses fils,
la tendresse du Seigneur pour qui le craint !