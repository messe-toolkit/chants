Ô Mère de Dieu, toute belle et sans défaut,
demeure digne du Seigneur, en toi fut
façonné le Fils bien-aimé, le prince de la vie.

Buisson qui brûlait, contenant « Celui qui
est », mais sans en être consumé ; Il a pris
chair en toi, le Verbe de Dieu, né avant tous
les temps.

Astre du matin, ta lumière a resplendi avant
le lever du soleil ; Il s’est manifesté, « gloire
d’Israël », « lumière des nations ».

Paradis nouveau, ton sein pur et virginal
porte le fruit qui donne vie. De l’ arbre de la
croix a jailli pour nous la source qui guérit.

Il est né de toi, l’artisan de l’univers, la
Résurrection et la Vie. Du tombeau, il surgit
le nouvel Adam, ressuscité des morts.

Ô Vierge Marie, le Seigneur t’a couronnée
de tous les dons de l’Esprit Saint. Nous
chantons ta beauté, fille d’Israël, épouse
immaculée.

Gloire au Dieu d’amour, Père saint qui
donne vie ; au Fils venu dans notre chair ;
et gloire au Saint Esprit qui fit de Marie le
« Temple du Très-Haut ! »
Amen !