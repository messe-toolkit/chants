R.
JE VAIS OUVRIR VOS TOMBES
CRI DE FEU ET DE VENT
JE VAIS OUVRIR VOS TOMBES
ET VOUS SEREZ VIVANTS

1.
Ossements des mensonges
Vieux os du désespoir
Vieux rêves et vieux songes
Rassis comme un pain noir.

2.
Vieux os des habitudes
Ossements desséchés
Couples des lassitudes
Vos amours déhanchés.

3.
Vieux os de la mémoire
Vieux ossements jaunis
Ce que l'on croyait croire
Qui s'est tout racorni.

4.
J'entrerai dans vos rêves
Les habillant de sang
Et de peau et de fièvre
Esprit des quatre vents