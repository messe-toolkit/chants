R.
Nous écrirons en lettres de lumière
L´histoire de notre liberté
Et nous verrons briller sur notre terre
L´espoir pour tous les opprimés.

1.
Un peuple s´est levé
Pour crier justice,
Un peuple s´est dressé
Pour vivre debout !

2.
Nous sommes des milliers
A mener la lutte
Pour notre dignité,
Notre liberté.

3.
"Ensemble" est notre cri
Pour briser nos chaînes
"Ensemble" est aujourd'hui
Notre mot d´amour.

4.
Ne baissons pas les bras
Car le jour se lève,
Poursuivons le combat
Du monde nouveau.

5.
Nous préférons l´amour
A tout l´or du monde,
Nous voulons chaque jour
Notre part de pain.

6.
La solidarité
Rythme notre marche,
La solidarité
Fais battre nos cœurs.

7.
Au cœur de notre vie
Jésus nous appelle
Pour bâtir avec lui
La terre de Dieu.

8.
Le monde nous attend
L´Esprit nous devance,
Préparons les printemps
De nos lendemains.