STANCE
La grâce de Dieu s'est manifestée, la vie nous est donnée.

R.
Recevons Jésus verbe fait chair.
Recevons Jésus verbe fait chair.

1.
Dieu a tant aimé le monde qu'il a donné son Fils unique
Afin que quiconque croit en Lui ait la vie éternelle.

2.
Dieu notre Sauveur a manifesté sa bonté et sa tendresse pour les hommes
Par le bain du baptême, il nous a fait renaître en son Fils bien-aimé.

3.
Dieu nous a arrachés au pouvoir des ténèbres,
Il nous a fait entrer dans le royaume de son Fils bien-aimé.

4.
Dieu est lumière ! À tous ceux qui l'ont reçu et qui croient en son nom,
Il leur a donné pouvoir de devenir enfants de Dieu.

5.
Dieu, nul ne l'a jamais vu, le Fils unique qui est tourné vers le sein du Père,
Lui l'a fait connaître.