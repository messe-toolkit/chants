R.
Talitha Koum, le Seigneur vient chez toi,
Talitha Koum, c’est bien lui, c’est sa voix,
Talitha Koum, il apporte la joie,
Talitha Koum “Petite fille lève-toi” !..

1.
Jésus venait de traverser la mer,
Déjà la foule était tout près de lui,
Alors survient Jaïre, un dignitaire,
“Seigneur, Seigneur, ma fille va mourir !
\n
Il faut venir lui imposer les mains,
Car je suis sûr que tu peux la guérir...”
La foule suivait toujours sur le chemin
Tous ces voyeurs voulaient le contredire.

2.
Mais c’est alors qu’arrive un serviteur
“Tu sais, Jaïre, ta fille vient de mourir !”
“Non, dit Jésus, surtout n’ayez pas peur,
Et puis croyez que rien ne peut finir !”
\n
Et Jésus entre alors dans la maison :
“Mais pourquoi donc pleurent
et crient tous ces gens ?
Puisqu’elle dort, vos pleurs sont sans raison
Alors les gens sortaient en se moquant.

3.
Car Jacques et Jean et Pierre en sont témoins
C’est quand Jésus fait entendre sa voix,
Quand il lui dit, en lui prenant la main :
“Talitha Koum, petite fille lève-toi”
\n
Elle fut debout. Elle n’avait que douze ans,
Et tous, alors, étaient dans la stupeur ;
“Mais donnez donc à manger à cet enfant.
Et puis surtout, n’en dites rien ailleurs !”