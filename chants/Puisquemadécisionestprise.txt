R.
PUISQUE MA DÉCISION EST PRISE
QUE JE REPARS POUR UN AILLEURS
IL FAUT MON DIEU QUE JE TE DISE
DEUX OU TROIS CHOSES AU COIN DU COEUR. (bis)

1.
Je dirais que la vie est belle
Bien que j’aie eu mon pain rassis
J’ai mis mes rêves à la poubelle
J’ai eu quelques problèmes aussi
Mais la vie vaut d’être vécue
Malgré tout ce qu’on peut écrire
Mais la vie vaut d’être vécue
C’est le merci que je veux dire.

2.
Je dirais que la vie me chante
Sous le toit bleu de son grand ciel
Avec la mer qui s’impatiente
Et tous ces visages au pluriel
Oui la vie vaut d’être vécue
Ses yeux m’ont donné Dieu à lire
Oui la vie vaut d’être vécue
C’est le merci que je veux dire.

3.
Je dirais que la vie me cause
Bien que j’aie eu des jours de froid
Bien sûr j’ai vu faner les roses
J’ai mes “comment” et mes “pourquoi”
Mais la vie vaut d’être vécue
En chaque être humain Dieu respire
Oui la vie vaut d’être vécue
C’est le merci que je veux dire.

4.
Je dirais que la vie se danse
C’est comme un quatorze juillet
Avec bien sûr trop de souffrance
Avec du sang sur les oeillets
Mais la vie vaut d’être vécue
Malgré tout ce qu’on peut écrire
Oui la vie vaut d’être vécue
C’est le merci que je veux dire.