Refrain :
Si tu regardes bien le ciel,
Il te donnera ses étoiles ;
Si tu recherches le soleil,
Il viendra briller dans tes voiles.

1.
Plus tu observes ton jardin,
Et plus les fleurs deviennent belles ;
D’un regard neuf jaillit soudain,
Une lumière, une étincelle.

2.
De vrais scrupules en faux-fuyants,
N’avance plus en bateau ivre ;
On ne transmet à ses enfants,
Que lorsqu’on est soi-même libre.

3.
“ Stone ” et déjà claquemuré,
Pour panser tes plaies affectives,
Et tes flash-back et tes regrets,
Et tes promesses possessives.

4.
Si tu pardonnes à l’ennemi,
Fini, le stress et les dédales,
Pas de vengeance, je t’en prie,
Mets plein “ d’humour ” dans ta grand’ voile !

5.
Scrute au-delà de l’horizon,
Des mers, des monts et des collines ;
Là-bas, je sais que nous attend,
Ce grand soleil qui illumine.