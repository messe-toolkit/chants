1.
Gloire à toi, Seigneur des anges,
Pour leur beauté où se déploient
La splendeur de ta présence
Et l’écho de ta voix.
Dans l’éclosion de leur louange
Ils s’élancent près de toi.

2.
Frémissant devant ta face,
À pleine joie ils crient ton nom.
Que l’un d’eux descende et passe
Nous toucher d’un tison :
Alors nos lèvres rendront grâce,
Purifiées par le pardon.

3.
Ils chantaient avant l’aurore
Avant que lève notre jour,
L’univers dormait encore
Au couvert de ta nuit.
Ils adoraient le Fils de Dieu
Dont le jour allait venir.

4.
Quelle paix, sinon la sienne,
Tes messagers annoncent-ils
Dans leurs chants qui nous reprennent
À la nuit de l’exil ?
La porte s’ouvre, ils nous entraînent
Jusqu’à l’arbre de la vie.