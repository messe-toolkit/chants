1.
Dans la nuit noire et sombre,
Marie et Joseph avancent.
Sur les chemins du monde,
Ils prient et font confiance.

2.
Vers Bethléem ensemble,
Ils se déplacent à dos d’âne.
Tant de gens se rassemblent
Et cherchent une demeure.

3.
Quand Marie se relève
Et prend l’enfant tout contre elle,
Joseph regarde le nouveau-né,
Il est heureux et bénit Dieu.

4.
Dans une pauvre étable,
Jésus vient donner au monde
La joie pour ceux qui attendaient
Le Messie envoyé par Dieu.

5.
C’est Noël sur la terre,
L’amour de Dieu se révèle.
Les bergers chantent « Gloire à Dieu
Et grande paix aux hommes ! »