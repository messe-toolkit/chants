1.
Tu nous as laissé un commandement nouveau
Aimez-vous les uns les autres
Comme je vous ai aimés
Tu nous as montré pour un monde plus beau
Par les actes et par les mots
La force d’une vie donnée

R.
VIENS, MAÎTRE DE L’AMOUR.
APPRENDS-NOUS À AIMER AUJOURD’HUI
VIENS, MAÎTRE DE L’AMOUR.
METTRE DE L’AMOUR DANS NOS VIES

2.
Tu t’es fait pour nous l’ami sur nos chemins
Frère des autres et des uns
Maître au rang du serviteur
Tu nous as appris à aimer le prochain
Les pauvres d’amour ou de pain
Restent les premiers dans ton cœur

3.
Tu nous as parlé avec des mots d’enfant
Avec émerveillement
De ton Père, notre Dieu
Ce qui était caché aux sages et aux puissants
Les doux, les petits de tous temps
Par toi le contemplent de leurs yeux

4.
Tu nous as aimés jusqu’au bout de ta nuit
Et ta croix comme une folie
Nous a rendus libres pour te suivre
Ta mort Jésus est devenue la vie
De ton cœur transpercé jaillit
L’Esprit d’amour qui nous fait vivre

5.
On voudrait vivre ensemble à cœur ouvert
Devenir fils et filles de Dieu
Devenir enfants de lumière
On veut s’aimer comme entre sœurs et frères
Semer l’Évangile en tout lieu
En toute terre