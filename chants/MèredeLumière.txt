R.
Mère de lumière,
Veille sur nous !
Mère de tendresse,
Prie pour nous !

Massabielle

1.
Au pied de Massabielle,
Nous goûtons ta présence.
Mère de tout amour,
Guide-nous vers ton Fils.

2.
Au souffle de ta grâce,
La prière s'apaise.
Nous portons avec nous
Tant de vies, tant d'espoirs.

Le rocher

3.
Le coeur plein de confiance,
En ce lieu de silence,
Nous fondons sur le roc
Notre vie, notre foi.

Pénitence

4.
Nous gardons la blessure
Du péché qui divise.
Marie, en ta bonté,
Tu appelles au pardon.

L'eau

5.
Nous cueillons à la source
La fraîcheur de l'enfance.
Peuple de pèlerins,
Nous guettons le matin.

La lumière

6.
Quand vient la nuit profonde,
Une douce lumière
Guide le pèlerin
Jusqu'aux rives du ciel.

7.
Marie de l'espérance,
Tu dissous les ténèbres.
En marchant dans la nuit,
Nous chantons ta splendeur.