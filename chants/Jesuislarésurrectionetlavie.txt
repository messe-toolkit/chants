Stance

Je suis la résurrection et la vie ;
qui croit en moi, fût-il mort, vivra.

R.

O Christe Domine Jesu,
O Christe Domine Jesu !

Versets

Ps 64.

1.
Il est beau de te louer, Dieu, dans Sion,
De tenir ses promesses envers toi qui écoutes la prière.

2.
Jusqu'à toi vient toute chair avec son poids de péché ;
Nos fautes ont dominé sur nous : toi, tu les pardonnes.

3.
Heureux ton invité, ton élu : il habite ta demeure !
Les biens de ta maison nous rassasient, les dons sacrés de ton temple !

4.
Ta justice nous répond par des prodiges, Dieu notre sauveur,
Espoir des horizons de la terre et des rives lointaines.