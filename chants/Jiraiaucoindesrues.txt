1.
II y a des chandelles et des rideaux tout blancs,
Un bouquet sur la nappe et un vieux Madiran,
Ça sent bon le confit et les cèpes du pré,
Je t’attends…
II y a des chansons rêvées en t’espérant,
Des milliers de poèmes et d’étoiles d’argent,
Tu vas venir, bien sûr, tu n’as pas oublié,
Je t’attends…

2.
II y a des instants qui paraissent des ans,
Bruits de pas dans la rue qui font le coeur battant,
Souvenir d’un parfum, amour ou amitié,
Je t’attends…
II n’y a plus de flamme et les fleurs ont cent ans,
Le dernier train du soir est mort depuis longtemps,
Tu dois courir, bien sûr, vers d’autres allumés,
Je comprends…

J’IRAI AU COIN DES RUES, AUX CARREFOURS DU MONDE.
VENEZ, TOUS LES EXCLUS, LES CLOCHARDS À LA RONDE.
VENEZ, VENEZ, VENEZ, TOUS LES ESPOIRS DÉÇUS,
POUR LE FESTIN QUE J’AI PRÉVU.

3.
À ma porte, il y a, sous les ponts de plein vent,
Des mégots, des seringues et de vieux chats errants,
Ça sent fort le gasoil et la ville rouillée,
Qui attend…
II y a des silences et des cris inquiétants,
Des milliers d’enfants gris et de pleurs innocents,
Les jamais invités, les toujours oubliés,
Que j’attends…

4.
Bien plus loin, sous des cieux de famine et de sang,
Bruits de chars dans les rues et charters de migrants,
La torture du fils et la maison brûlée,
On attend…

JE SUIS AU COIN DES RUES…

5.
Et puis, jailli de l’ombre, un peuple aux habits blancs,
Étoiles des regards, chansons de coeurs battants,
Convives fraternels, pain et vin partagés,
Moi, j’attends…

JE SUIS AU COIN DES RUES, AUX CARREFOURS DU MONDE.
VENEZ, TOUS LES EXCLUS, MES AMIS À LA RONDE,
VENEZ, VENEZ, VENEZ, TOUS LES AMOURS DÉÇUS,
C’EST DIEU QUI PAYE, ET BIENVENUE !