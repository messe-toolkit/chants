1.
Notre ami(e) s’en est allé(e)
Près de toi, Seigneur.
Notre coeur en est brisé
Devant toi, Seigneur.
Mais nous avons tenu bon
Grâce à toi, Seigneur.
Et jamais nous n’oublierons
Ton amour, Seigneur.

2.
Nous qui l’avons tant aimé(e),
Nous savons, Seigneur,
Que notre amour compte peu
Près du tien, Seigneur.
Donne-lui l’éternité,
Dans ta joie, Seigneur,
Nous pourrons alors chanter :
Gloire à toi, Seigneur !

3.
Et quand notre tour viendra,
Notre tour, Seigneur,
De paraître devant toi,
Devant toi, Seigneur,
Notre ami(e) nous conduira
Près de toi, Seigneur,
Notre coeur se réjouira
Pour toujours, Seigneur.