R.
UNE PETITE FILLE,
UNE ENFANT DE DOUZE ANS
EST MORTE CE MATIN DANS UN CHAMP DE TABAC,
ET LA PETITE FILLE AVAIT DES CHEVEUX BLANCS
A FORCE DE MOURIR A TEGUCIGALPA,
A FORCE DE MOURIR A TEGUCIGALPA.

1.
Tous les gens ont la vie dure
au village d'Olancho,
et pour toute nourriture
du maïs avec de l'eau.

2.
Tous les vents de la montagne
Vous diront qu'elle a gémi
Sur le sol de sa cabane
Douze jours et douze nuits.