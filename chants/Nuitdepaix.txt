R.
NOËL, SOYONS DANS LA JOIE,
NOËL, CHANTONS NOTRE ROI !
JÉSUS NOUS EST DONNÉ, BERGER D’HUMANITÉ,
IL EST NÉ DE MARIE AU MILIEU DE LA NUIT.

1.
Le prince de la paix
Vient pour nous rassembler,
Alors pour aujourd’hui
Rangeons tous nos fusils.

2.
Jésus Christ est venu
Pour ceux qui sont perdus,
Son amour infini
Nous fait naître à la vie.

3.
Sans même aller plus loin
Parlons à nos voisins,
Ouvrons grand notre coeur
Pour donner le meilleur.

4.
Le temps a commencé
Quand Jésus nous est né,
Un monde se bâtit
Acclamons le Messie.