PAR NOS BOUCHES, PAR NOS VOIX,
NOUS CHANTONS LE DIEU DE LA JOIE,
PAR NOS BOUCHES, PAR NOS VOIX,
NOUS CHANTONS: ALLÉLUIA !

1.
Nos mains sont ouvertes pour accueillir tous nos amis
Nos mains sont ouvertes en ce jour de fête
Nos mains sont ouvertes pour accueillir Dieu notre Ami
Nos mains sont ouvertes aujourd’hui.

2.
Nos mains sont offertes pour apporter tous nos soucis
Nos mains sont offertes en ce jour de fête
Nos mains sont offertes pour apporter tout ce qui vit
Nos mains sont offertes aujourd’hui.

3.
Notre table est prête pour partager les fleurs, les fruits
Notre table est prête en ce jour de fête
Notre table est prête pour partager le Pain de Vie
Notre table est prête aujourd’hui.