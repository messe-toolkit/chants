1.
Enfantés dans la lumière
Nous avons grandi
Depuis l’âge de la pierre
Jusqu’à l’homme d’aujourd’hui.
Enfantés dans les tempêtes
Nous portons la peur
Des enfants de la planète
Devant un soleil qui meurt !

R.
NOUS, LES FILS DE LA TERRE,
HABITÉS DU MÊME CHANT,
DU MÊME CRI.
NOUS, LES FILS DE LA TERRE,
ENVOYÉS DU FOND DES TEMPS
DU FOND DES NUITS
POUR DONNER L’AMOUR À L’INFINI ! (bis)

2.
Arrachés à l’esclavage
Nous avons trouvé
Le secret d’un nouvel âge
Aux parfums de liberté.
Arrachés à l’ignorance
Nous pouvons choisir
De briser tous les silences
Qui nous fermaient l’avenir !

3.
Façonnés dans l’impatience
Nous avons brûlé
Les remparts d’indifférence
Qui nous tenaient prisonniers.
Façonnés dans la tendresse
Nous forçons la vie
À nous tenir ses promesses
Dans un monde en agonie !