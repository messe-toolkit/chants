1.
Une jeune fill’ de Nazareth qui aimait Joseph le charpentier,
Reçoit une visite un jour dans sa maison.
C’est l’ange Gabriel qui vient lui annoncer
« Tu seras la mère de Jésus ».
NOËL, NOËL, C’EST UNE ATTENTE, NOËL, NOËL, C’EST UNE ATTENTE,
NOËL, NOËL, C’EST UNE ATTENTE, NOËL, NOËL, NOËL, NOËL !

2.
Alors dans la joie Marie s’en va, à Elisabeth ell’ rend visite
Pour voir sa cousine là-haut dans la montagne, qui elle aussi attend
Un tout petit enfant.
«Magnificat » chante Marie.
NOËL, NOËL, C’EST UNE RENCONTRE, NOËL, NOËL, C’EST UNE RENCONTRE,
NOËL, NOËL, C’EST UNE RENCONTRE, NOËL, NOËL, NOËL, NOËL !

3.
C’est à Bethléem que Jésus naît, là dans une étable dans la nuit.
Pauvre petit enfant il n’y a plus de place, et c’est dans une crèche
Qu’il va dormir Jésus.
Lui, le descendant du roi David.
NOËL, NOËL, C’EST UNE NAISSANCE, NOËL, NOËL, C’EST UNE NAISSANCE,
NOËL, NOËL, C’EST UNE NAISSANCE, NOËL, NOËL, NOËL, NOËL !

4.
Guidés par l’étoile quelle route, ils ont tant marché pour voir Jésus,
Et comme auprès d’un roi ils se sont prosternés, chacun lui a donné
Un trésor, un cadeau.
Ils sont entrés chez eux dans la paix.
NOËL, NOËL, C’EST UN CADEAU, NOËL, NOËL, UN CADEAU,
NOËL, NOËL, UN CADEAU, NOËL, NOËL, NOËL, NOËL !