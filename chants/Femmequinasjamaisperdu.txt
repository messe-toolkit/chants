Stance
Femme qui n’a jamais perdu la ressemblance du premier jour
en ton coeur limpide respire la Parole.
Comme les eaux jaillissantes façonnent la glaise vierge,
elle creuse en toi l’espace où reposera la Gloire du Très-Haut.

Refrain
SAINTE VIERGE MARIE PRIE POUR NOUS TON FILS,
TRÈS SAINT ET BIEN-AIMÉ.

Versets

1.
De ton regard transparent, Servante du Seigneur, les anges s’émerveillent.

2.
Dans ton regard innocent, Épouse de l’Esprit, l’Unique se dévoile.

3.
Sous ton regard bienveillant, Ô Mère de Jésus, les faibles se relèvent.

4.
Femme du regard lumineux, qui vois le coeur ouvert, tu veilles sur le monde.