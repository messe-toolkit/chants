AIMÉS DU PÈRE, AIMÉS DU FILS
BAPTISÉS DANS UN SEUL ESPRIT,
VOICI L’HEURE DE LA MISSION
VOICI L’HEURE DE LA MISSION
C’EST LE TEMPS DE FRANCHIR NOS BARRIÈRES
ACTE D’AMOUR POUR LA TERRE !

1.
Sur le chemin des aveuglés,
Jésus marche, (Jésus marche)
Il écoute la plainte qu’ils ont fait taire
Il nous procure sa lumière
Soyons tous des lampes qui éclairent !

2.
Sur le chemin des gens perdus,
Jésus marche, (Jésus marche)
Il est le bon pasteur, il offre sa vie
Il nous revêt de son salut
Soyons les bergers de l’imprévu !

3.
Sur le chemin des coeurs blessés,
Jésus marche, (Jésus marche)
Il relève la femme chargée de péchés
Nos pierres, il nous les fait lâcher
Soyons avec lui des coeurs de chair !

4.
Sur le chemin des vies brisées,
Jésus marche, (Jésus marche)
Il chasse toute peur en ouvrant son coeur
Il nous éduque à sa douceur
Vivons en heureux consolateurs !

5.
Sur le chemin des gens repus,
Jésus marche, (Jésus marche)
Il est le Dieu fait pauvre pour mieux nous aimer
Il nous octroie la vie du Père
Soyons des témoins de l’Essentiel !

6.
Sur le chemin de nos tombeaux,
Jésus marche, (Jésus marche)
Il se montre à Marie, pour toujours, vivant !
Comme elle, il nous envoie devant
Allons annoncer qu’Il est présent.

7.
Sur le chemin de nos impasses,
Jésus marche, (Jésus marche)
À sa parole s’ouvrent d’autres espaces :
Repas béni du face-à-face,
Nous sommes vraiment tous de sa race !