NOUS VOULONS TE PARLER, JÉSUS,
NOUS VOULONS TE PARLER.
NOUS VOULONS TE PRIER, JÉSUS,
NOUS VOULONS TE PRIER.

1.
Pour tous les amis du quartier,
Les copains de l’école,
Pour Thomas, Camille et Vianney,
Jean-Baptiste et Nicole.

2.
Pour ceux qui vivent à la maison,
Jean-Baptiste et Colette,
Ceux qui sont loin de la maison,
Oncle Pierre et Lisette.

3.
Pour ceux qui sont très malheureux
Et ceux qui sont malades,
Qui ont des larmes plein les yeux
Dans leurs maisons si froides.

4.
Pour ceux qui n’ont rien à manger,
Pour tous les enfants pauvres
Qu’on voit parfois à la télé
Et pour ceux qui les sauvent.