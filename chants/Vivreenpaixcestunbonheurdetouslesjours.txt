R.
Vivre en paix, c’est un bonheur de tous les jours,
Vivre ensemble ouvre des portes à l’avenir.
A ton image fais-nous vivre, Dieu d’amour,
Viens donner un nouveau souffle pour grandir !

1.
Tu as donné son assise à la terre,
Qu’elle reste inébranlable au cours des temps :
Terre agitée par de multiples tremblements,
Terre habitée par des milliards de vies humaines...
Sur la planète où chacun veut bâtir,
L’oeuvre de nos mains, tu l’attends jour et nuit!

2.
Tu as donné une ASSISE à la terre,
Qu’elle apprenne le Cantique du soleil:
Ville animée par des prophètes en éveil,
Ville épousée par Saint François et sainte Claire...
Sur la planète où la joie peut fleurir,
L’homme au coeur nouveau dit la paix de l’Esprit.

3.
Nous donnerons une ASSISE à la terre,
Que le ciel de nos rencontres soit au bleu :
Dire un « je crois » où reste ouvert le mot de Dieu,
Dire un espoir qui rejoindra celui d’un frère...
Sur la planète où l’humain doit mûrir,
Vivre dans l’amour portera tout son fruit.