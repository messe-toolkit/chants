1.
De tout mon coeur je crie vers Dieu !
Mon cri semble tomber à rien.
De tout mon coeur, le Seigneur crie vers Dieu :
Dieu l’entend, et il vient.
Le Seigneur est amour, nous faisons coeur à deux,
Dieu fait coeur avec lui,
Car le Seigneur est Dieu :
Voilà ce que fait un ami !

2.
Le Seigneur a tout l’homme en amour,
Tout homme ne le sait pas.
Le Seigneur veut tout homme d’amour,
Il donne tout, pour qu’il le soit.
Il s’est fait cors au corps humain,
Mon sang peut passer par son coeur ;
Il lui donne sa source et sa fin :
Voilà ce que fait le Seigneur !

3.
Dans sa nuit de confiance, il l’envoie
Porter à tout le corps son amour ;
Il le rappelle de sa voix
Pour le recharger en amour.
Tendrement il se fait des sujets,
Patiemment, il leur forme des yeux
Pour qu’ils témoignent de la vérité :
Voilà ce que fait notre Dieu !