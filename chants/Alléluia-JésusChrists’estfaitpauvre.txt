ALLÉLUIA, ALLÉLUIA.

Jésus Christ s’est fait pauvre,
lui qui était riche,
pour qu’en sa pauvreté vous trouviez la richesse.

ALLÉLUIA.