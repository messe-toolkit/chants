R.
GLOIRE À DIEU, LE SEUL SAGE,
PAR JÉSUS CHRIST
ET POUR LES SIÈCLES DES SIÈCLES.
AMEN.

1.
Gloire à Dieu,
qui a le pouvoir de vous rendre forts
conformément à l’Évangile que je proclame
en annonçant Jésus Christ.

2.
Oui, voilà le mystère qui est maintenant révélé :
il était resté dans le silence depuis toujours,
mais aujourd’hui il est manifesté.

3.
Par ordre du Dieu éternel,
et grâce aux écrits des prophètes,
ce mystère est porté à la connaissance
de toutes les nations
pour les amener à l’obéissance de la foi.