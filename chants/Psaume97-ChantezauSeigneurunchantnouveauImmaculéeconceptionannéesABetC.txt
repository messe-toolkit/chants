CHANTEZ AU SEIGNEUR UN CHANT NOUVEAU,
CAR IL A FAIT DES MERVEILLES.

1.
Chantez au Seigneur un chant nouveau,
car il a fait des merveilles ;
par son bras très saint, par sa main puissante,
il s’est assuré la victoire.

2.
Le Seigneur a fait connaître sa victoire
et révélé sa justice aux nations ;
il s’est rappelé sa fidélité, son amour,
en faveur de la maison d’Israël.

3.
La terre tout entière a vu
la victoire de notre Dieu.
Acclamez le Seigneur, terre entière.
Acclamez votre roi, le Seigneur !

ALLÉLUIA. ALLÉLUIA.

Je te salue, Marie, Comblée?de-grâce :
le Seigneur est avec toi,
tu es bénie entre les femmes.