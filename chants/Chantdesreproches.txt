Choeur
Ô mon peuple, que t’ai-je fait ?
En quoi t’ai-je contristé ? Réponds-moi.

Réponse
Ô Dieu saint,
Ô Dieu fort,
Ô Dieu immortel, prends pitié de nous.

Choeur I
Peuple égaré par l’amertume,
peuple au coeur fermé, souviens-toi !
Le Maître t’a libéré.
Tant d’amour serait-il sans réponse,
tant d’amour d’un Dieu crucifié ?

Soliste 1.

1.
Moi, depuis l’aurore des mondes,
j’ai préparé ton aujourd’hui ;
toi, tu rejettes la vraie Vie
qui peut donner la joie sans ombre,
ô mon peuple, réponds-moi ! R/. Ô Dieu saint...

Soliste 2.

2.
Moi, j’ai brisé tes liens d’esclave,
j’ai fait sombrer tes ennemis ;
toi, tu me livres à l’ennemi,
tu me prépares une autre Pâque,
ô mon peuple, réponds-moi ! R/.

Soliste 1.

3.
Moi, j’ai pris part à ton exode,
par la nuée je t’ai conduit ;
toi, tu m’enfermes dans ta nuit,
tu ne sais plus où va ma gloire,
ô mon peuple, réponds-moi ! R/.

Soliste 2.

4.
Moi, j’ai envoyé mes prophètes,
ils ont crié dans ton exil ;
toi, tu ne veux pas revenir,
tu deviens sourd quand je t’appelle,
ô mon peuple, réponds-moi ! R/.

Soliste 1.

5.
Moi, j’ai voulu, vivante Sève,
jeter l’espoir de fruits nouveaux ;
toi, tu te coupes de mes eaux
mais pour aller vers quelle sève ?
Ô mon peuple, réponds-moi ! R/.

Choeur II
Vigne aux raisins d’amertume,
vigne aux sarments desséchés, souviens-toi !
La Grappe fut vendangée ;
ce Fruit mûr serait-il sans partage,
ce Fruit mûr que Dieu a pressé ?

Soliste 1.

6.
Moi, j’ai porté le poids des chaînes,
j’ai courbé le dos sous les fouets ;
toi, tu me blesses en l’opprimé,
l’innocent tombé sous la haine,
ô mon frère, réponds-moi ! R/.

Soliste 2.

7.
Moi, j’ai porté sceptre et couronne
et manteau royal empourpré ;
toi, tu rougis de confesser le Fils de Dieu
parmi les hommes,
ô mon frère, réponds-moi ! R/.

Soliste 1.

8.
Moi, j’ai marché vers le calvaire
où mes deux bras furent cloués ;
toi, tu refuses la montée quand meurt en croix
l’un de mes frères,
ô mon frère, réponds-moi ! R/.

Soliste 2.

9.
Moi, je revis depuis l’Aurore
où le Vivant m’a réveillé ;
toi, le témoin de ma clarté,
es-tu vivant parmi les hommes ?
Ô mon frère, réponds-moi ! R/.

Choeur III
Frère sevré d’amertume,
frère au coeur desséché, souviens-toi !
Ton frère t’a relevé, Jésus Christ, le Verbe et la Réponse,
Jésus Christ, l’amour révélé.