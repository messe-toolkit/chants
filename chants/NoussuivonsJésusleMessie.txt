1.
Le Verbe crie naissance,
Voici : Dieu se fait homme.
Hosanna ! Hosanna ! Béni soit notre Dieu.
Lumière qui dissipe
La nuit et l’ignorance.
Hosanna ! Hosanna ! Béni soit notre Dieu.

R.
NOUS SUIVONS JÉSUS LE MESSIE,
L’ENVOYÉ, FILS DU DIEU BÉNI.
NOUS SUIVONS JÉSUS LE SEIGNEUR,
SERVITEURS DE SON ÉVANGILE
ET MINISTRES DE SON SALUT,
SERVITEURS DE SON ÉVANGILE
ET MINISTRES DE SON SALUT.

2.
Un homme en Palestine,
Des foules sur sa route.
Hosanna ! Hosanna ! Béni soit notre Dieu.
Berger parmi les pauvres,
Sagesse pour le monde.
Hosanna ! Hosanna ! Béni soit notre Dieu.

3.
Un homme pour les autres
Qui pardonne et qui sauve.
Hosanna ! Hosanna ! Béni soit notre Dieu.
Visage de tendresse
Qui porte nos souffrances.
Hosanna ! Hosanna ! Béni soit notre Dieu.

4.
Un Dieu au rang d’esclave
Que l’on mène au Calvaire.
Hosanna ! Hosanna ! Béni soit notre Dieu.
Messie qu’on abandonne,
Crucifié par la haine.
Hosanna ! Hosanna ! Béni soit notre Dieu.

5.
Seigneur qui ressuscite,
Vainqueur de la ténèbre.
Hosanna ! Hosanna ! Béni soit notre Dieu.
Selon les Écritures,
Dieu a tenu promesse.
Hosanna ! Hosanna ! Béni soit notre Dieu.

6.
Vrai Roi des rois et Maître,
La création t’adore.
Hosanna ! Hosanna ! Béni soit notre Dieu.
Agneau, souverain juge
Qui bâtit le Royaume.
Hosanna ! Hosanna ! Béni soit notre Dieu.