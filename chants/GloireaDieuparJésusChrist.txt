R.
Gloire à Dieu par Jésus-Christ
En lui les hommes se rassemblent
Par le lien de son Esprit.

1.
Il nous guide et nous relève
Il nous sauve et nous libère
Il fait le don de sa vie pour nous.

2.
Premier né d'un nouveau monde
Où la mort n'a plus de pouvoir
Il donne le pain d'éternité.

3.
Pain des forts et vin de l'Alliance,
Pain de vie, vin du Royaume
Qui nous délivre du péché.