Toutes les oeuvres du Seigneur, bénissez le Seigneur :
A LUI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!
Vous les anges du Seigneur, bénissez le Seigneur :
A LUI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!

1.
Vous, les cieux,
bénissez le Seigneur,
et vous, les eaux par-dessus le ciel,
bénissez le Seigneur,
et toutes les puissances du Seigneur,
bénissez le Seigneur,
À lui haute gloire…

2.
Et vous, le soleil et la lune,
et vous, les astres du ciel,
vous toutes, pluies et rosées,

3.
Vous tous, souffle et vents,
et vous, le feu et la chaleur,
et vous, la fraîcheur et le froid,

4.
Et vous, le givre et la rosée
et vous, le gel et le froid
et vous, la glace et la neige,

5.
Et vous, les nuits et les jours,
et vous, la lumière et les ténèbres,

et vous, les éclairs, les nuées,
A LUI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!

Que la terre bénisse le Seigneur :
A LUI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!

6.
Et vous, montagnes et collines,
et vous, les plantes de la terre,
et vous, sources et fontaines,

7.
Et vous, océans et rivières,
vous tous, les oiseaux dans le ciel,
vous tous, fauves et troupeaux,

Et vous, les enfants des hommes, bénissez le Seigneur !
A LUI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!

8.
Vous, enfants d’Israël,
Et vous, les prêtres,
vous, ses serviteurs,

9.
Les esprits et les âmes des justes,
les saints et les humbles de coeur,
Ananias, Azarias et Misaël,

Bénissons le Père, le Fils et l’Esprit saint :
A LUI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!

Béni sois-tu, Seigneur, au firmament du ciel :
A TOI, HAUTE GLOIRE, LOUANGE ÉTERNELLE!