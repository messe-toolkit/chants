NE PLEUREZ PAS,
NE PLEUREZ PAS SUR MOI
MAIS PLEUREZ SUR VOUS-MÊMES
PLEUREZ SUR VOS ENFANTS.

1.
Le monde se perd dans la nuit
La guerre et la haine font rage
Des innocents tombent aujourd’hui
Vous me voyez dans leur visage :

2.
Les cris de tous ceux qui ont faim
La peur et l’enfer du chômage
Et tous ces gens qui n’ont plus rien
Vous me voyez dans leur visage :

3.
Que vienne sur terre la paix
Des gestes d’amour, de partage
Tant parmi vous sont opprimés
Vous me voyez dans leur visage :