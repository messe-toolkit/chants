1.
Cloué sur deux morceaux de bois
Du haut de cette croix,
Tu as trouvé la force de parler
À ceux qui se sentaient abandonnés
Par l’Espérance ;
Les femmes qui t’avaient suivi,
Ta mère à côté de l’Ami,
Ta préférence !

À CEUX QUI T’ONT VU MOURIR,
À CEUX DU MONDE À VENIR,
TU AS DIT : RESTEZ ENSEMBLE !
À TOUS CEUX QUI T’ONT SUIVI,
AUX DISCIPLES D’AUJOURD’HUI,
TU AS DIT : RESTEZ ENSEMBLE !

2.
Après que tout soit achevé,
Avant de t’en aller,
Toi tu révèles encore à tes amis
Que par-delà les portes de la Vie
L’Amour existe…
Tes derniers mots de condamné
Redonnent aux désespérés
La joie de Vivre !

3.
La mort au ventre des tombeaux
N’a pas le dernier mot.
Marie de Magdala cherchait ton corps,
Mais toi tu l’attendais déjà dehors,
Dans la Lumière…
Et tu redis au monde entier
Que toute peine partagée
Est plus légère !