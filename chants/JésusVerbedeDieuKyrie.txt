KYRIE, CHRISTE,
KYRIE ELEISON!

1.
Jésus, Verbe de Dieu,
Verbe fait chair par amour pour les pécheurs,

2.
Jésus, Maître et Seigneur,
Gloire humiliée par amour pour les pécheurs,

3.
Jésus, homme au coeur pur,
Homme enchaîné par amour pour les pécheurs.

4.
Jésus, Prince de Paix,
Roi bafoué par amour pour les pécheurs,

5.
Jésus, Fils Premier-Né,
Dieu crucifié par amour pour les pécheurs,

6.
Jésus, Source de vie,
Corps assoiffé par amour pour les pécheurs.

7.
Jésus, humble de coeur,
Coeur transpercé par amour pour les pécheurs,

8.
Jésus, notre grand Dieu,
Mis au tombeau par amour pour les pécheurs,

9.
Jésus, ressuscité,
Prêtre éternel par amour pour les pécheurs.