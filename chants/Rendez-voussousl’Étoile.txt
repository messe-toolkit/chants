T'AS RENDEZ-VOUS SOUS L'ÉTOILE !
LAISSE-TOI PORTER PAR LE VENT.
T'AS RENDEZ-VOUS SOUS L'ÉTOILE !
AUX CÔTÉS DE L'ENFANT.

1.
J'ai pris mon bâton et mes chaussures,
j'ai suivi la lumière.
J'ai choisi un rythme à ma mesure
sans regarder en arrière.
J'ai pris la route avec le coeur rempli de doutes,
mais j'entendais tout bas
une voix qui disait : « Eh toi ! »

2.
J'ai mis dans mon sac toutes mes blessures,
mes peurs et mes combats.
Je lui présenterai car j'en suis sûr(e),
son amour transformera
le gris, le noir, en un éclat couleur d'espoir.
Et cette voix me dit :
« N'aie pas peur de marcher la nuit ! »

3.
Je n'ai pas grand-chose à lui offrir,
ni discours, ni cadeau.
J'apporterai mes projets d'avenir,
mes souvenirs les plus beaux,
ce que je suis, ce que je crois, toute ma vie.
Et ces offrandes-là,
aucun trésor ne les vaudra.

4.
Je suis parti(e) sans savoir pourquoi,
mais sans hésitation
j'ai suivi l'étoile car son éclat
a bousculé ma raison.
Le coeur battant, j'ai tout laissé pour voir l'Enfant.
J'entendais cet appel
qui résonne à travers le ciel.

5.
L'amour se révèle tout simplement
sous la nuit étoilée,
dans la faiblesse d'un petit enfant,
la douceur d'un nouveau-né.
Le Dieu vivant, maître des cieux, des océans,
a pris notre visage
pour nous délivrer son message.

6.
Me voilà grandi(e), plus fort(e) qu'hier,
nourri(e) de tant d'amour.
Dieu s'est fait humain sur cette terre,
j'irai le dire au grand jour.
Voici ma foi ! Même si c'est fou, oh oui je crois !
Car notre monde a faim
d'une espérance et de témoins.