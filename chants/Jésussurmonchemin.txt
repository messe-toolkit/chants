1.
Jésus pour te CHERCHER.
Mets sur mes pas cette clarté,
Un peu de force pour marcher
Aller vers Toi?!
Jésus pour te TROUVER.
Mets dans mes yeux cette gaieté,
Ce goût du vrai pour discerner
Que c’est bien Toi?!

R.
Jésus sur mon chemin
Mets dans mes mains
Ce bout de pain
Qui est le tien
Pour un matin
Trouver la joie.

2.
Jésus pour te CHANTER.
Mets dans ma voix des mots de Paix,
Ces mots d’Espoir et d’Amitié
Ces mots de Toi?!
Jésus pour te SERVIR.
Mets dans mes mains ce grand désir,
De tout donner jusqu’à mourir
Pour vivre en Toi?!

3.
Jésus pour te PRIER.
Mets dans mon cœur l’amour, le vrai,
Pour tenir ma lampe allumée
Tout près de Toi?!
Jésus pour T’ANNONCER.
Mets dans ma vie cette bonté,
La tolérance et le respect
Qui vient de Toi?!