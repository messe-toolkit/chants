R.
VIENNE AUJOURD’HUI DANS NOTRE COEUR
L’ESPRIT D’AMOUR ET DE LUMIÈRE !
VIENNE SA FORCE QUI LIBÈRE,
VIENNE LE SOUFFLE DU SEIGNEUR!

1.
Ravive en toi le don de Dieu,
L’Esprit qui mène à des audaces !
Choisis d’aller jusqu’où il veut,
Tu connaîtras des joies pascales.
Ravive en toi la pleine Vie,
L’Esprit reçu à ton baptême !
Fais-lui porter beaucoup de fruit
Comme Jésus l’a fait lui-même.

2.
Ravive en toi le feu nouveau,
L’Esprit qui brûle en ta demeure !
Tu seras signe du Très-Haut,
Prophète et prêtre pour son peuple.
Ravive en toi des flots d’espoir,
L’Esprit t’abreuve à ses eaux vives !
Ta foi vivante chantera
Que le Dieu saint nous fait revivre.

3.
Ravive en toi la soif de Dieu,
L’Esprit te souffle sa prière !
Les yeux se tournent vers les cieux
Pour des instants qui font renaître.
Ravive en toi douceur et paix,
L’Esprit triomphe des violences !
Le monde lance un cri d’appel,
Réponds en mots de délivrance.

4.
Ravive en toi la communion,
L’Esprit t’invite à des rencontres :
Ouvrir des voies, bâtir des ponts,
En serviteurs de la Colombe.
Ravive en toi le don de Dieu,
L’Esprit qui mène à des audaces !
Choisis d’aller jusqu’où il veut,
Tu connaîtras des joies pascales.