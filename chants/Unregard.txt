1.
Les murs étaient encore plus lépreux que les hommes
Dans cette léproserie
Où l’on tournait en rond comme des bêtes de somme
Traînant la nuit
Dans nos esprits
On n’attendait plus rien au bout de notre route
Que la venue de la mort
Qui laissait, chaque jour, ses marques goutte à goutte
Un peu plus fort
Sur notre corps.

2.
Parmi ces naufragés partant à la dérive
J’étais le seul à lutter
Quel est donc le secret de sa rage de vivre,
Se demandaient
Les infirmiers
Mais, un jour, ils t’ont vue par-dessus la muraille
Toi, ma femme qui souriais
Alors ils ont compris que ma rude bataille
Tu m’entraînais
À la gagner.

3.
Derrière beaucoup de murs, sur la terre des hommes
Tant de gens sont enfermés
Pourquoi se contenter d’envoyer une aumône ?
Pour éviter de voir de près
Si l’on savait franchir nos murs d’indifférence
On pourrait bien découvrir
Que là, tout près de nous, on espère en silence
Rien qu’un sourire
Pour n’pas mourir.