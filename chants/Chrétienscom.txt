R.
“CHRÉTIENS POINT COM”
OUVREZ LE PASSAGE,
LAISSEZ LE MESSAGE
SUR L’ÉCRAN DE NOS VIES.
“CHRÉTIENS POINT COM”
OUVREZ LE PASSAGE,
LAISSEZ LE MESSAGE
DE JÉSUS-CHRIST…
AUJOURD’HUI !

1.
Devenir témoins sur nos e-mails
Pour cliquer sur un autre lien,
Sur l’essentiel.

2.
Croyez en l’Amour, changez vos coeurs,
Annoncez à toutes nations
Résurrection.

3.
Maisons et châteaux, cours d’HLM,
Dieu s’invite au fond des ghettos
De nos modems.