R.
APPELÉS À TÉMOIGNER DE TA VIE EN NOS VIES
EN TON NOM, SEIGNEUR, NOUS VOICI RASSEMBLÉS
PAR DELÀ TOUS LES MOTS QUE NOUS AVONS APPRIS,
OUVRE NOS COEURS POUR MIEUX T’AIMER.

1.
Dans le désert au feu brûlant
Témoins de tant et tant d’errances
Guide nos pas aux sources du printemps
Où l’eau jaillit en abondance.

2.
Quand des hivers de nos cités
La vie se rend en pages blanches
C'est en nos mains offertes à l'amitié
Que le printemps tient sa revanche.

3.
Tu nous veux là où nous vivons
Simples héritiers de ta Promesse
Porte nos voix plus loin que nos maisons
Pour témoigner de ta tendresse.

4.
Vers les enfants tu nous envoies
Eux, qui sont à ta ressemblance
Puissions-nous donc découvrir dans la joie
Que te connaître est une chance.