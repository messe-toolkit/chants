R.
À l'amour que vous aurez les uns pour les autres,
Tous vous reconnaîtront pour mes disciples.

1.
En obéissant à la vérité,
Vous avez sanctifié vos âmes
Pour vous aimer comme des frères.

2.
D'un cœur pur, aimez-vous les uns les autres
Engendrés de nouveau par la Parole de Dieu.

3.
Pareils à des enfants nouveau-nés,
Désirez la Parole comme un lait pur
Pour croître en vue du salut.

4.
À l'image du Dieu saint qui vous a appelés,
Vous aussi soyez saints car moi je suis saint.