STANCE
Il est grand celui qui donne la vie, plus grand celui qui pardonne ;
Il est bon celui qui aime ses amis, meilleur celui qui donne sa vie
Pour l'homme qui le blesse. Dieu blessé, Jésus, tu nous pardonnes.
Il n'y a pas de plus grand amour que de donner sa vie pour ceux qu'on aime.

1.
Vous serez mes amis si vous faites ma volonté.

2.
Moi, le Maître et Seigneur, je vous ai donné l'exemple.

3.
Heureux serez-vous, sachant cela, si vous donnez votre vie.