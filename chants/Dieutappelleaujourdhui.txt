STANCE
Peuple de baptisés,
Dieu t’appelle depuis toujours,
Dieu t’appelle en ce temps-ci.
Il t’appelle pour que tu vives.
Marche au souffle de l’Esprit,
Devant toi marche le Christ,
En le suivant (servant) tu seras libre !

R.
Dieu t’appelle aujourd’hui,
Dieu te veut libre : viens à lui !

1.
Libre celui qui a fait ciel et terre,
Libre celui qui te donne la vie,
Libre l’appel du Dieu vivant
A ses enfants.
Dieu te veut libre comme lui.

2.
Libre celui qui t’arrache aux ténèbres,
Libre Jésus qui t’invite à le suivre,
Libre le Fils du Dieu vivant
Dans son offrande.
Dieu te veut libre en Jésus Christ.

3.
Libre celui qui te dit : «Viens au Père !»
Libre le Souffle en qui tout reprend vie,
Libre l’Esprit du Dieu vivant,
Qui est louange !
Dieu te veut libre dans l’Esprit.

4.
Libre le cœur qui écoute l’appel,
Libre l’amour qui répond : «Me voici !»
Libre la foi au Dieu vivant,
Et la confiance !
Dieu te veut libre : viens à lui !