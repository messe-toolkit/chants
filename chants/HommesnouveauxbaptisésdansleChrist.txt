STANCE
Hommes nouveaux baptisés dans le Christ, alléluia,
vous avez revêtu le Christ, alleluia
héritiers avec lui d'un Royaume de lumière,
vous possédez la liberté des fils de Dieu
pour annoncer au monde :

R.
NOUS SOMMES AU CHRIST
ET LE CHRIST EST A DIEU,
ALLELUIA, ALLELUIA !

VERSETS

1.
Venez et voyez
Les hauts faits de Dieu, ses exploits redoutables
pour les fils des hommes.

2.
Venez et voyez
Il changea la mer en terre ferme :
on passait le fleuve à pied sec.

3.
Venez et voyez
Tous ceux que conduit l'Esprit de Dieu,
ceux-là sont fils de Dieu.