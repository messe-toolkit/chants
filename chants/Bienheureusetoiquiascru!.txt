BIENHEUREUSE TOI QUI AS CRU !

BIENHEUREUSE TOI QUI AS CRU QUE S’ACCOMPLIRAIENT UN JOUR.
LES PAROLES DE TON DIEU
POUR TOUS LES HOMMES QU’IL AIME !

1.
Celui que tu portes en toi est un feu qui transfigure,
Et tu veux nous donner sa joie !

2.
Celui que tu portes en toi est la source de la vie,
Ton chant nous dit le vrai bonheur !

3.
Le fils que tu donnes au monde vient nous sauver de la mort,
Ton élan nous dit sa victoire !

4.
L’enfant qui est né de toi vient ouvrir l’éternité,
Par toi, nous connaîtrons sa gloire.

5.
Marie, tu viens visiter chaque instant de notre histoire,
Nous imprégner de ta fraîcheur.