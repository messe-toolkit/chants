LE SEIGNEUR, C’EST LUI NOTRE DIEU ;
IL S’EST TOUJOURS SOUVENU DE SON ALLIANCE.

1.
Rendez grâce au Seigneur, proclamez son nom,
annoncez parmi les peuples ses hauts faits ;
chantez et jouez pour lui,
redites sans fin ses merveilles.

2.
Glorifiez-vous de son nom très saint :
joie pour les coeurs qui cherchent Dieu !
Cherchez le Seigneur et sa puissance,
recherchez sans trêve sa face.

3.
Souvenez-vous des merveilles qu’il a faites,
de ses prodiges, des jugements qu’il prononça,
vous, la race d’Abraham son serviteur,
les fils de Jacob, qu’il a choisis.

4.
Il s’est toujours souvenu de son alliance,
parole édictée pour mille générations :
promesse faite à Abraham,
garantie par serment à Isaac.