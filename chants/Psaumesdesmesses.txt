PÂQUES
Voici le jour que le Seigneur a fait : Jour d'allégresse et Jour de joie. (Ps117)

ASCENSION
Dieu monte parmi l'acclamation, le Seigneur aux éclats du cor. (Ps 46)

PENTECÔTE
Ô Seigneur, envoie ton Esprit qui renouvelle la face de la terre. (Ps 103)