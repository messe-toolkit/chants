R.
NOUS VOULONS VIVRE AUJOURD'HUI
NOUS VOULONS ACCUEILLIR DEMAIN
L'AVENIR NOUS LANCE UN DEFI
OUVRONS NOS COEURS, OUVRONS NOS MAINS !

1.
Aux portes de l'an deux mille
Comme les premiers chrétiens
Crions le rêve fragile
D'un tombeau vide au matin

2.
Dans un monde qui se cherche
Dans un monde qui a peur
Saurons nous ouvrir des brêches
Faire un espace au bonheur

3.
Ouvrons portes et fenêtres
Notre Dieu est dans le vent
Il nous invite à la fête
Au grand banquet des vivants

4.
Perdus dans la multitude
Nous vivons en cheminant
Avec pour seule certitude
Que Toi, Seigneur, Tu es devant.