Stance :
La terre desséchée tressaille de joie :
Une source jaillit, transparence nouvelle
Où notre humanité retrouve son visage :

Source pure, Vierge Marie,
Avec toi l'espérance renaît !

1.
Ton chant d'humilité annonce le Serviteur.

2.
Ta fraîcheur nous laisse pressentir les fleuves d'eau vive.

3.
Ta course nous entraîne vers l'océan de vie.