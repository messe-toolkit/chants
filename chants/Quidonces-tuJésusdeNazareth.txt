1.
Qui donc es-tu, Jésus de Nazareth?
Qui donc es-tu? (bis)
Envoyé d’auprès du Père,
Sur nos sentiers tu nous rejoins ;
Qui donc es-tu pour nous ?
Qui donc es-tu?
Tu te révèles à Simon Pierre,
L’Esprit lui dit ton Nom divin :
Tu es le Messie, le Fils du Dieu vivant !

2.
Qui donc es-tu, Jésus de Nazareth?
Qui donc es-tu? (bis)
Tu proclames le Royaume
Et ta parole est du Dieu saint.
Qui donc es-tu pour nous ?
Qui donc es-tu?
Tu mets ta gloire, ô Fils de l’homme,
A nous défaire de nos liens.
Tu es le Messie, le Fils du Dieu vivant !

3.
Qui donc es-tu, Jésus de Nazareth?
Qui donc es-tu? (bis)
Toi qui fondes ta demeure
Sur le rocher d’un être humain,
Qui donc es-tu pour nous ?
Qui donc es-tu?
Tu nous rassembles dans un peuple
Sur qui la mort ne peut plus rien :
Tu es le Messie, le Fils du Dieu vivant !