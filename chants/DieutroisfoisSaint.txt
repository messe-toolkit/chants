Dieu Saint !
Dieu très Saint !
Dieu trois fois Saint !
Dieu de l'univers !
Le ciel et la terre
Sont remplis de ta gloire :
Hosanna au plus haut des cieux !
Béni soit celui qui vient
Au nom du Seigneur :
Hosanna au plus haut des cieux !