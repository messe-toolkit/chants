1.
Église du Seigneur, exulte et chante,
Voici l’Époux !
Dans sa tendresse, il vient vers toi,
Lui qui change l’eau en vin,
En jubilation la plainte de ton deuil.

2.
Entends le Bien-Aimé sur les collines
Et sur les monts !
Les temps nouveaux sont advenus,
Il demeure chez les siens,
Comme aux premiers jours, un peuple lui répond.

3.
L’Amour te parle au coeur et se révèle,
Tes yeux le voient !
Scellant l’Alliance dans sa chair,
Il te donne son pardon,
Il étend sur toi le pan de son manteau.

4.
Entends le chant de joie, le chant des noces,
Voici l’Agneau !
Son sang versé te sauvera,
Et les peuples rassemblés,
Palmes à la main, sans fin l’acclameront.