1.
Ne craignez pas, amis éveillez-vous,
une lumière s’est levée sur nous !
Voici l’Étoile du premier matin,
l’Ange de Dieu nous montre le chemin:
à Bethléem, un fils nous est donné !
Paix sur la terre aux bonnes volontés !

2.
Voyez l’étable où Dieu tremble de froid ;
nous l’attendions dans le palais d’un roi !
Enfant si pauvre, à notre étonnement,
près des bergers s’endort le Tout-Puissant !
Ouvrons nos coeurs à l’Envoyé de Dieu !
Paix sur la terre et gloire dans les cieux !

3.
Peuple de Dieu, acclame le Seigneur :
pour ton salut il se fait Serviteur !
Il vient, l’Agneau qui porte nos péchés,
victime offerte pour nous racheter.
Jérusalem, chante et danse de joie :
la Paix de Dieu est descendue sur toi !