ALLEZ PORTER LA PAIX SUR LES CHEMINS DU MONDE
ALLEZ PORTER LA PAIX AMIS DE JÉSUS CHRIST
ALLEZ PORTER LA PAIX SUR LES CHEMINS DU MONDE
ALLEZ PORTER LA PAIX LA PAROLE DE VIE

1.
C’est Jésus qui appelle
Amis au coeur brûlant
C’est la bonne nouvelle
Notre Dieu est vivant ! VIVANT !

2.
Voyez le jour se lève
Amis au coeur brûlant
C'est plus loin que nos rêves
Notre Dieu est vivant ! VIVANT !

3.
Voyez la route est sûre
Amis au coeur brûlant
Partons pour l'aventure
Notre Dieu est vivant ! VIVANT !

4.
C'est Jésus la lumière
Amis au coeur brûlant
Tu peux dire à nos frères
Notre Dieu est vivant ! VIVANT !