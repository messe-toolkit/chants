VOGUE, VOGUE AU LEVANT LE NAVIRE.
BRÛLE, BRÛLE TON COEUR QUI SOUPIRE.
LE VENT POUSSE OÙ IL VEUT T’EMMENER.
L’ESPRIT SOUFFLE OÙ IL VEUT T’ENTRAÎNER !

1.
Musulmans et Chrétiens se haïssent,
les croyants sans pitié se maudissent.
Quand l’amour est trahi sur la terre,
toi tu veux rencontrer l’adversaire !

2.
Les deux camps à la guerre s’entraînent.
La colère insensée se déchaîne.
« Ne vas pas t’exposer aux supplices !
L’ennemi n’est que mal et que vice ».

3.
Malgré tout, tu t’en vas sans défense,
et tu veux éviter toute offense.
Le sultan te reçoit mais hésite,
puis respecte ta foi et t’invite.

4.
Tu tenais à subir le martyre,
le seigneur ne veut pas t’y conduire.
Tu voulais convertir l’infidèle,
tu entends une voix fraternelle.

5.
Saint François, nous chantons la mémoire
du Dieu Saint, le Seigneur de l’histoire.
Dans la joie avec toi et les anges,
que s’élève sans fin la louange.