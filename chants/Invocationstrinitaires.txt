R.
O Dieu Saint,
O Dieu fort.
Dieu immortel.
Prends pitié.

1.
Père Créateur,
Père tout-puissant.
Père de Jésus-Christ,
Prends pitié.

2.
Jésus-Christ, Seigneur,
Jésus-Christ, Sauveur,
Jésus-Christ le Fils de Dieu,
Prends pitié.

3.
O Esprit de Dieu,
O Esprit de Feu,
O Esprit Consolateur,
Prends pitié.

4.
Trinité de Dieu.
Trinité Amour,
Trinité dans l'Unité,
Prends pitié.