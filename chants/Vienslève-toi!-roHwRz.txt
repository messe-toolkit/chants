R.
Viens, lève-toi, viens à Jésus, ton Sauveur,
Car son amour est grand pour toi !
Viens, lève-toi, viens à Jésus, ton Sauveur,
Il est vivant, alléluia !

1.
Tourne ton regard vers Jésus, le Fils de Dieu,
Tu n’auras plus peur, car il marche auprès de toi.
Tu es son ami : il t’appelle et te choisit,
Et pour toi, il donne sa vie !

2.
Il est le chemin, lui, la vérité, la vie !
Si tu viens à lui, il te donne son amour.
Auprès de son Père il te mène et te conduit,
Et pour toi, il donne sa vie !