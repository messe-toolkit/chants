1.
Je cherche des mots “soleil”
Des mots qui n’ont plus peur
Mots en forme de coeur
Aux couleurs arc-en-ciel
Je cherche des mots “sourire”
Qui refusent de mourir
Loin des mots prisonniers
Des feuilles de papier.

DONNE-MOI DES MOTS
BRILLANT COMME DES ÉTOILES
DONNE-MOI DES MOTS
PORTÉS PAR TOUS LES VENTS
DONNE-MOI DES MOTS
LÉGERS COMME DES VOILES
DONNE-MOI DES MOTS
POUR CRIER QUE TU ES VIVANT.

2.
Je cherche des mots “ruisseau”
Des mots qui s’raient pas fiers
Piquants comme l’hiver
Puissants comme des chevaux
Je cherche des mots “frisson”
Sucre d’orge ou glaçon
Des mots si transparents
Qu’on y lirait le vent.

3.
Je cherche des mots “enfant”
Des mots qui n’ont pas froid
Des mots comme-ci comme-ça
En matin de printemps
Je cherche des mots étonnés
Qui flotteraient sur les blés
Écarquillant les yeux
Dans ton ciel tout en bleu.