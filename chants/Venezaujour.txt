1.
Venez au jour !
Le Christ prépare son retour !
Le Christ prévient l’ère nuptiale !
Passent les temps !
Passe la chair !
L’Esprit de Dieu souffle au désert,
Annonçant l’aurore pascale !

2.
Dépouillez-vous !
Quand vous mourrez, vous perdrez tout,
Suivez votre exode à l’avance !
Tombe la mort !
Tombe le soir !
N’attendez pas qu’il soit trop tard
Pour que Dieu vous donne naissance.

3.
Ne craignez pas de vous défaire,
Il recréera ce que vous cédez de vous-mêmes ;
Fermez les yeux !
Baissez vos fronts !
Venez mendier sa création
Au fond des ténèbres humaines.

4.
Ne glissez plus sur votre pente à l’inconnu,
Car ici commence un autre âge ;
Retournez-vous !
Apprenez Dieu !
Il a promis son règne
À ceux qui emprunteront ses passages !

5.
Le jour viendra où le désert refleurira
Et l’ombre rendra la lumière !
Traversez-les dès maintenant,
Allez chercher au testament
Ce qui n’est pas né de la terre !