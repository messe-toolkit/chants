Antienne
RENDONS GRÂCE AU SEIGNEUR QUI SEUL FAIT DES MERVEILLES.
RENDONS GRÂCE AU SEIGNEUR QUI SEUL FAIT DES MERVEILLES.

1.
Qu’ils rendent au Seigneur de son amour,
qu’ils offrent des sacrifices de louange,
ceux qui ont vu les oeuvres du Seigneur
et ses merveilles parmi les océans.

2.
Il parle et provoque la tempête,
un vent qui soulève les vagues :
portés jusqu’au ciel, retombant aux abîmes,
ils étaient malades à rendre l’âme.

3.
Dans leur angoisse, ils ont crié vers le Seigneur,
et lui les a tirés de la détresse,
réduisant la tempête au silence,
faisant taire les vagues.

4.
Ils se réjouissent de les voir s’apaiser,
d’être conduits au port qu’ils désiraient.
Qu’ils rendent grâce au Seigneur de son amour,
de ses merveilles pour les hommes.