1.
Dieu qui bâtis déjà
La ville d’espérance,
Où notre monde ira
Tenir sa délivrance,
Mets en nos mains tendues
Les pierres du Royaume,
Pour que nous soit rendue
Cent fois notre oeuvre d’homme.

2.
Pour habiter demain
Les terres d’espérance
Où mènent les chemins
Que trace ton Alliance,
Guide nos avancées
Aux routes fraternelles
Qui font tout devancer
De la cité nouvelle.

3.
Vers l’assemblée des cieux,
Un peuple d’espérance,
Ton peuple, Seigneur Dieu,
Marche avec assurance.
Fais de lui maintenant
Un peuple de prophètes
Qui annonce à ce temps
Ton éternelle fête.