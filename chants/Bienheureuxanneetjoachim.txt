STANCE
Bienheureux Anne et Joachim,
Parents de la Vierge Marie,
Bénis soyez-vous d'avoir cru
Au jour qu'Abraham entrevit.
Voici qu'il resplendit enfin :
Le Sauveur attendu nous est donné.

R.
Le Dieu de nos pères a tenu la promesse,
Il réjouit l'espérance des pauvres

1.
Glorifiez-vous de son nom très saint :
Joie pour les cœurs qui cherchent Dieu.

2.
Cherchez le Seigneur et sa puissance,
Recherchez sans trêve sa face.

3.
Souvenez-vous des merveilles qu'il a faites,
De ses prodiges, des jugements qu'il prononça.