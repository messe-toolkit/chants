1.
Depuis l’aube des âges
Il cherche notre visage ;
Il a tant désiré
La coupe du partage,
Le pain de pauvreté,
Qu’il vient à notre image.

2.
Les mains nues, sans défense,
Il vient tenir l’alliance ;
Il saura désormais
Le prix de l’espérance,
L’angoisse quand l’ivraie
Étouffe la semence.

3.
Il vient rompre nos chaînes,
Nous prendre aux terres lointaines ;
N’est-il pas tout-puissant,
Celui que l’amour mène ?
Il sauve en se donnant,
Sa pâque nous entraîne.