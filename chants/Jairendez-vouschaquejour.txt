1.
J’ai rendez-vous chaque jour chaque nuit
Pour repeindre le monde et décorer la vie
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où

ICI OU LÀ… LÀ, LÀ, LÀ, LÀ
PEU IMPORTE OÙ… OÙ, OÙ, OÙ, OÙ
VOUS ÊTES LÀ… LÀ, LÀ, LÀ, LÀ
CAR AVANT TOUT, DÈS AUJOURD’HUI,
J’AI RENDEZ-VOUS AVEC VOUS MES AMIS

2.
J’ai rendez-vous sur le quai d’une gare
Pour aller vers demain et revenir plus tard
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où

3.
J’ai rendez-vous au fin fond du désert
Pour écouter mon coeur chanter avec les pierres
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où

4.
J’ai rendez-vous avec les océans
Qui dansent main dans la main avec les continents
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où

5.
J’ai rendez-vous au-delà des nuages
Avec ma bonne étoile sans arme ni bagage
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où

6.
J’ai rendez-vous aux frontières de l’espoir
Quand tout reste possible malgré le froid du soir
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où

7.
J’ai rendez-vous en toute liberté
Avec l’égalité et la fraternité
Mais avant tout, oui, avant tout…
J’ai rendez-vous… Où, où, où, où