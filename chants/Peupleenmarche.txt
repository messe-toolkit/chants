R.
PEUPLE EN MARCHE AVEC JÉSUS DE NAZARETH,
ANIMÉS PAR LA JEUNESSE DE L’ESPRIT,
NOUS CHANTONS NOTRE ESPÉRANCE DANS LA VIE :
DIEU SAUVE LE MONDE! DIEU SAUVE LE MONDE!

1.
Sur la terre où des semailles nous attendent,
La foi nous tient debout,
Fidèles au rendez-vous ;
Le Seigneur est Celui qui nous rassemble,
Notre Père entend nos demandes.

2.
Sur la terre où l’Écriture nous éclaire,
Parlons d’un Dieu présent
Qui crée l’événement.
Son appel fait grandir dans la lumière,
Nous formons un peuple de frères !

3.
Sur la terre avec ses troubles et ses tempêtes
Nous traversons des nuits,
Le doute nous saisit.
Mais le Christ a des mots qui nous réveillent,
Dans nos coeurs jaillit sa Nouvelle !

4.
Sur la terre et dans l’Église en plein exode,
Le pain nous est donné
Pour vivre une avancée:
Pain formé des blés mûrs que l’on moissonne,
Corps du Christ offert à tout homme !

5.
Sur la terre avec le fruit de nos vendanges
La table est préparée,
Le vin sera versé :
Vin de fête aux saveurs qui nous enchantent,
Sang du Christ, en signe d’Alliance.

6.
Sur la terre où des frontières se dissipent,
Le monde vient au jour,
Il naît de notre amour.
Dieu bâtit avec nous la terre libre,
Maintenant semons l’Évangile !

7.
Sur la terre où les blessés sont à nos portes,
Tombés sur le chemin,
Quel bon Samaritain
Choisira de se faire le plus proche?
Dieu nous dit : c’est là le Royaume!