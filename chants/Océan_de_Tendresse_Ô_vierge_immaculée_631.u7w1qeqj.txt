1.
Ô cœur immaculé,
Océan de tendresse,
Nous venons Te supplier
Verse en nous Tes largesses.

R.
Chantons nos louanges
Unis aux archanges
Ave Maria. Ave Maria.
Elle est notre mère
Encore sur la terre
Ave Maria. Ave Maria.

2.
Je me consacre à Toi
Ô ma Mère et ma Reine
Je me donne dans la joie
Avec un cœur sincère.

3.
Ô cœur plein de bonté
Dont la grâce déborde
Regarde l’humanité
Obtiens miséricorde.