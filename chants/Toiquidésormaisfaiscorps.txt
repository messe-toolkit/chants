1.
Toi qui désormais fais corps avec nous,
Béni sois-tu pour cette délivrance
Du grand abîme au profond de la chair !
Car tu es là, et ton corps est en nous,
Et nous pouvons découvrir sa présence
Avec ta grâce et n’y voir grand ouvert
Qu’un seul abîme de l’Amour vivant !

2.
Toi qui désormais fais vie avec nous,
Béni sois-tu d’avoir chassé la cendre
Que nous sentions à marcher vers la mort !
Car tu es là, et ta vie est en nous,
Et nous pouvons maintenant y descendre
Avec ta grâce et trouver souffle encor
Au seul air libre de l’Amour vivant !

3.
Toi qui désormais fais coeur avec nous,
Béni sois-tu d’avoir comblé le vide
Béant derrière et devant nos amours !
Car tu es là, et ton coeur est en nous,
Et nous pouvons quitter tout autre guide
Avec ta grâce et ne battre toujours
Qu’au seul mystère de l’Amour vivant.