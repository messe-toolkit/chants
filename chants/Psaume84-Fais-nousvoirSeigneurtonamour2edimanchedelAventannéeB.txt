FAIS-NOUS VOIR, SEIGNEUR, TON AMOUR,
ET DONNE-NOUS TON SALUT.

1.
J’écoute : que dira le Seigneur Dieu ?
Ce qu’il dit, c’est la paix pour son peuple et ses fidèles.
Son salut est proche de ceux qui le craignent,
et la gloire habitera notre terre.

2.
Amour et vérité se rencontrent,
justice et paix s’embrassent ;
la vérité germera de la terre
et du ciel se penchera la justice.

3.
Le Seigneur donnera ses bienfaits,
et notre terre donnera son fruit.
La justice marchera devant lui,
et ses pas traceront le chemin.

ALLÉLUIA. ALLÉLUIA.

Préparez le chemin du Seigneur,
rendez droits ses sentiers :
tout être vivant verra le salut de Dieu.