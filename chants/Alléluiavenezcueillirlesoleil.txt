VENEZ CUEILLIR LE SOLEIL,
ALLÉLUIA, ALLÉLUIA !
VENEZ FÊTER LA LUMIÈRE,
GLOIRE À DIEU, ALLÉLUIA !
ALLÉLUIA, ALLÉLUIA !
GLOIRE À DIEU, ALLÉLUIA !

1.
Savez-vous le secret
De Jésus Ressuscité ?
La mort n’a pas gagné,
La vie a bourgeonné !

2.
Savez-vous le secret
De Jésus Ressuscité ?
La peur n’a pas gagné,
La paix est à semer !

3.
Savez-vous le secret
De Jésus Ressuscité ?
Les pleurs n’ont pas gagné,
La joie peut se donner !

4.
Savez-vous le secret
De Jésus Ressuscité ?
La nuit n’a pas gagné,
Le jour a triomphé !