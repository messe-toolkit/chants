1.
J’allais mendiant de porte en porte
Vivant d’aumône, de charité
J’étais le vent en quelque sorte
N’ayant nulle part où demeurer.

J’appris qu’un chariot rempli d’or
Conduit par un roi tout-puissant
Devait passer, coquin de sort
Tout près d’ici dans quelque temps.

Je m’enquis du jour et de l’heure
Et j’attendis en cet endroit
Quand par miracle et par bonheur
Le lourd chariot s’arrêta là.

Et je vis descendre le roi
Plein d’élégance et de droiture
Son regard se posa sur moi
Tout plein d’amour je vous l’assure.

2.
Alors je lui tendis la main
Sûr de sa générosité
Mais il me dit d’un air malin :
« Et toi qu’as-tu à me donner ? »

Je fus alors tellement surpris
Que dans ma poche je fouillai
Et je lui donnai sans un bruit
Un tout, tout petit grain de blé.

Mais quand le soir fut arrivé
Je fus rempli d’un grand remords
Parmi les autres grains de blé
Je trouvai un petit grain d’or.

Alors je compris la leçon
Et je pleurai abondamment
Que n’ai-je eu le coeur assez bon ?
Je serais riche maintenant.