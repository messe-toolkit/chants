R.
Roi du monde, Jésus vivant,
Tu rassembles ton Eglise.
Roi du monde, Jésus vivant,
Gloire à toi qui nous invites !

1.
Berger qui nous mènes vers la vie,
Pour nous tu feras jaillir les eaux.
Ta main nous protège et nous conduit,
En toi nous trouvons le vrai repos.

2.
Berger qui connais le Dieu d'amour,
Ta voix nous apprend quel est son Nom :
Un Dieu qui espère nos retours,
Un Dieu de tendresse et de pardon.

3.
Berger, ton bercail est large ouvert,
Chez toi, tous les peuples chanteront.
Tu es le Sauveur de l'univers,
Celui que nos yeux découvriront.