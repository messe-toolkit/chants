J’EN SUIS SÛR, JE VERRAI LES BONTÉS DU SEIGNEUR
SUR LA TERRE DES VIVANTS.

1.
Le Seigneur est ma lumière et mon salut ;
de qui aurais-je crainte ?
Le Seigneur est le rempart de ma vie ;
devant qui tremblerais-je ?

2.
J’ai demandé une chose au Seigneur,
la seule que je cherche :
habiter la maison du Seigneur
tous les jours de ma vie,
pour admirer le Seigneur dans sa beauté
et m’attacher à son temple.

3.
Écoute, Seigneur, je t’appelle !
Pitié ! Réponds-moi !
Mon coeur m’a redit ta parole :
« Cherchez ma face. »