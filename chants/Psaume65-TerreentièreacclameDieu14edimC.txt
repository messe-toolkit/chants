TERRE ENTIÈRE, ACCLAME DIEU,
CHANTE LE SEIGNEUR !

1.
Acclamez Dieu, toute la terre ;
fêtez la gloire de son nom,
glorifiez-le en célébrant sa louange.
Dites à Dieu : « Que tes actions sont redoutables ! »

2.
Toute la terre se prosterne devant toi,
elle chante pour toi, elle chante pour ton nom.
Venez et voyez les hauts faits de Dieu,
ses exploits redoutables pour les fils des hommes.

3.
Il changea la mer en terre ferme :
ils passèrent le fleuve à pied sec.
De là, cette joie qu’il nous donne.
Il règne à jamais par sa puissance.

4.
Venez, écoutez, vous tous qui craignez Dieu :
je vous dirai ce qu’il a fait pour mon âme.
Béni soit Dieu qui n’a pas écarté ma prière,
ni détourné de moi son amour !

ALLÉLUIA. ALLÉLUIA.

Que dans vos coeurs, règne la paix du Christ ;
que la parole du Christ habite en vous
dans toute sa richesse.