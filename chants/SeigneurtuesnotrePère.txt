R.
SEIGNEUR, TU ES NOTRE PÈRE,
NOUS SOMMES L’ARGILE ET TU ES LE POTIER.
SEIGNEUR, TU ES NOTRE PÈRE,
NOUS SOMMES L’OUVRAGE DE TES MAINS.

1.
En tes mains, Seigneur, je remets ma vie.
Fais de moi, mon Dieu, ce qui te plaira,
Car entre tes mains, me voici.

2.
En tes mains, Seigneur, je remets ma vie.
Pour ce que tu fais, je te remercie,
Car entre tes mains, me voici.

3.
En tes mains, Seigneur, je remets ma vie.
Je suis prêt à tout et j’accepte tout,
Car entre tes mains, me voici.

4.
En tes mains, Seigneur, je remets ma vie.
Je te donne tout pour ta volonté,
Car entre tes mains, me voici.

5.
En tes mains, Seigneur, je remets ma vie.
Je me donne à toi de tout mon amour,
Car entre tes mains, me voici.