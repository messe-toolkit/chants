R.
Seigneur mon Dieu, je crie le jour,
Je gémis la nuit devant toi ;
Sors du silence et réponds-moi !

1.
Israël, comment te délaisser ?
Mon coeur en moi se retourne,
Toutes mes entrailles frémissent
Car je suis Dieu, non pas un homme.

2.
Trésor inestimable à mes yeux,
A grand prix je t’ai racheté.
Ne crains pas car je suis ton ami,
Par ton nom je t’appelle et tu es mien.

3.
La mort n’est pas l’oeuvre de mes mains ;
Point de joie dans la perte des vivants !
Une femme oublierait-elle son enfant ?
Impérissable est en moi ton souvenir.

4.
Proche de ceux qui m’invoquent,
Je fais route avec toi, mon ami.
Si tu passes un ravin de ténèbres,
Je suis ta lumière dans la nuit.

5.
Immuable aux cieux ma parole :
Je vous ferai surgir de vos tombeaux.
C’est le vivant, c’est le vivant qui me rend grâce,
Je ne suis pas le Dieu des morts mais des vivants.