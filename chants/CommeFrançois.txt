COMME FRANÇOIS PART DANS LES BOIS POUR DÉCOUVRIR QUELLE EST SA VOIE
SUR DIEU MON ROC, JE PRENDS APPUI, EN LUI JE FONDE MON AVENIR.
COMM’AUSSI CLAIRE CHERCHANT SON FRÈRE, POUR QU’IL TÉMOIGNE DE SA FOI
AU MONDE JE SUIS ENVOYÉ PORTER JÉSUS RESSUSCITÉ.

1.
Comment pourvoir discerner immergé dans trop d’activités ?
Garderai-je une place au Seigneur ?
Garderai-je une place au Seigneur ?
Courir pour perdre son âme
Ou stopper à l’écoute de son coeur ?
Ou stopper à l’écoute de son coeur ?

2.
Ou trouver le vrai repos ? « Lâcher prise » des soucis plein le dos !
Changerai-je ce fardeau des douleurs ?
Changerai-je ce fardeau des douleurs ?
S’épuiser par trop résister
Ou tomber dans les bras du Sauveur ?
Ou tomber dans les bras du Sauveur ?

3.
Dégager un horizon, découvrir quelle est sa vocation ?
Risquerai-je cette quête sans peurs ?
Risquerai-je cette quête sans peurs ?
Se fermer pour s’illusionner
Ou répondre à l’appel des profondeurs ?
Ou répondre à l’appel des profondeurs ?