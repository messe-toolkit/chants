R.
Gloire à toi, ô Dieu, notre Père,
Gloire à toi Jésus-Christ venu nous sauver.
Gloire à toi, Esprit de lumière,
Trinité Bienheureuse, honneur et gloire à toi !

1.
Père des Cieux, Père infiniment bon,
Tu combles tes enfants de tes dons.
Tu nous as faits, et nous t’offrons nos cœurs,
Nous te bénissons, nous croyons en toi Seigneur !

2.
Jésus Sauveur, et Fils du Dieu vivant,
Que s’élève vers toi notre chant.
Ton cœur ouvert nous donne à contempler
L’amour infini dont le Père nous a aimés.

3.
Esprit de Dieu, Esprit de sainteté,
Tu nous conduis à la vérité.
Descends sur nous éclairer nos chemins,
Sois le maître en nous, et fais de nous des témoins.