R.
Dieu saint, notre Père,
Ton Souffle passe en moi ;
Dieu saint qui nous aimes,
Louange et gloire à toi !

1.
Tu donnes sans compter,
Dieu très bon, ta main nous comble.
Pour toi je veux jouer,
Ta musique est joie du monde.

2.
Mon coeur bénit ton Nom
Par la voix des grandes orgues.
Seigneur, fais-moi le don
De garder la juste note.

3.
Ma force au long des jours,
C'est le pain de notre Pâque.
Nourri de ton amour,
Par Jésus je rendrai grâce.

4.
Sublime est l'harmonie
Qui jaillit des mains du Verbe.
Que l'hymne de ma vie
Soi baignée dans sa lumière !