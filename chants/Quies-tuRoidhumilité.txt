RERAIN
Qui es-tu, Roi d´humilité,
Roi sans palais, Roi sans armée ?
Nous sommes venus t´adorer,
Des bouts du monde,
Des bouts du monde.

2.
Nous ne savons pas bien comment
Un signe vu en Orient
A conduit nos pas au levant
De ta lumière,
De ta lumière.

3.
Que feras-tu de cet argent,
De ces bijoux, de notre encens ?
Nous les avions pris en pensant
A nos manières,
A nos manières.

4.
Regarde donc autour de toi
Dans les richesses qui sont là,
Les nations qui ne savent pas
Que tu les aimes,
Que tu les aimes.

5.
Marie pourra te raconter
Qu´avec nous, après les bergers
Tout l´univers s´est rassemblé
Sous ton étoile,
Sous ton étoile.

6.
Petit roi juif et roi du ciel,
Notre grand roi, l´Emmanuel,
Nous traversons ton Israël
Pour en renaître,
Pour en renaître !