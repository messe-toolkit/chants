MARIE, MÈRE DE DIEU ET NOTRE MÈRE

MARIE, MÈRE DE DIEU ET NOTRE MÈRE
SI PROCHE DE CHACUN, TU ES L'ÉTOILE DU MATIN.
MARIE, MÈRE DE DIEU ET NOTRE MÈRE,
SI PROCHE DE CHACUN, TU MARCHES SUR TOUS NOS CHEMINS.

1.
Gardienne de l'enfant qui a pris chair en ta demeure,
Il est l'amour dans nos impuissances
Et tu nous donnes de l'accueillir en nous.

2.
Incroyable tendresse, frémissement d'adoration,
Ta douceur indomptable rayonne au cœur du monde,
Porteuse de la grâce, tu visites les hommes.

3.
Tu crois en l'impossible, tu dis à Dieu : "Mon tout petit",
Ce que tu fais pour lui continue aujourd'hui,
Tu façonnes en nos traits son visage de gloire.

4.
Compagne de Joseph qui a vu ton profond mystère
Avec lui, tu travailles dans les jours ordinaires
Pour nous apprendre à vivre "ciel en Terre".