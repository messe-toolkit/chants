1.
Près du Fils qui meurt en croix,
l’humble Mère au coeur transpercé,
tient ses yeux sur le Supplicié :
elle entend sa douce voix.
O crux ave, spes unica !

2.
Quand il dit : "Voici ton Fils",
la Très-Pure, en larmes, reçoit
les enfants, tous nés de sa foi,
frères de Jean dans le Christ.
O crux ave, spes unica !

3.
Ô douleurs d’enfantement!
Vierge en pleurs près du seul Sauveur !
Dieu fait homme voici qu’il meurt
pour que l’homme soit vivant !
O crux ave, spes unica !

4.
À Jérusalem, voilà
que tout homme au vrai jour est né
de l’Amour, pour nous, crucifié
dans la nuit du Golgotha.
O crux ave, spes unica !