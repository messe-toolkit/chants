Stance

Que ta famille, Seigneur Jésus, nous soit une lumière et conduise nos pas au chemin de la paix.

R.

Gloria in excelsis Deo !
Gloria, gloria Deo !

Versets

Ps 95

1.
Chantez au Seigneur un chant nouveau,
Chantez au Seigneur, terre entière,
Chantez au Seigneur et bénissez son nom !

2.
De jour en jour, proclamez son salut,
Racontez à tous les peuples sa gloire,
À toutes les nations ses merveilles !

3.
Joie au ciel ! Exulte la terre !
Les masses de la mer mugissent,
La campagne tout entière est en fête.

4.
Les arbres des forêts dansent de joie
Devant la face du Seigneur, car il vient,
Car il vient pour juger la terre.

Ou bien

Ps 97

1.
Chantez au Seigneur un chant nouveau,
Car il a fait des merveilles ;
Par son bras très saint, par sa main puissante,
Il s'est assuré la victoire.

2.
Le Seigneur a fait connaître sa victoire
Et révélé sa justice aux nations ;
Il s'est rappelé sa fidélité, son amour,
En faveur de la maison d'Israël.

3.
La terre tout entière a vu
La victoire de notre Dieu.
Acclamez le Seigneur, terre entière,
Sonnez, chantez, jouez.

4.
Jouez pour le Seigneur sur la cithare,
Sur la cithare et tous les instruments ;
Au son de la trompette et du cor,
Acclamez votre roi, le Seigneur !