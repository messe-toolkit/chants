Antienne I
TU ÉCOUTES, SEIGNEUR, QUAND JE CRIE,
TU ÉCOUTES QUAND JE CRIE VERS TOI.

Antienne II
NOUS SERONS LES MESSAGERS DE TON NOM,
NOUS SERONS LES MESSAGERS DE TA PAROLE.

1.
De tout mon coeur, Seigneur, je te rends grâce :
tu as entendu les paroles de ma bouche.
SAINT EST LE SEIGNEUR NOTRE DIEU !
Je te chante en présence des anges,
vers ton temple sacré, je me prosterne.
SAINT EST LE SEIGNEUR NOTRE DIEU !

2.
Je rends grâce à ton nom pour ton amour et ta vérité,
car tu élèves, au-dessus de tout, ton nom et ta parole.
SAINT EST LE SEIGNEUR NOTRE DIEU !
Le jour où tu répondis à mon appel,
tu fis grandir en mon âme la force.
SAINT EST LE SEIGNEUR NOTRE DIEU !

3.
Tous les rois de la terre te rendent grâce
quand ils entendent les paroles de ta bouche.
SAINT EST LE SEIGNEUR NOTRE DIEU !
Ils chantent les chemins du Seigneur :
“Qu’elle est grande, la gloire du Seigneur !”
SAINT EST LE SEIGNEUR NOTRE DIEU !

4.
Si haut que soit le Seigneur, il voit le plus humble ;
de loin, il reconnaît l’orgueilleux.
SAINT EST LE SEIGNEUR NOTRE DIEU !
Si je marche au milieu des angoisses, tu me fais vivre,
ta main s’abat sur mes ennemis en colère.
SAINT EST LE SEIGNEUR NOTRE DIEU !

5.
Ta droite me rend vainqueur.
Le Seigneur fait tout pour moi !
SAINT EST LE SEIGNEUR NOTRE DIEU !
Seigneur, éternel est ton amour :
n’arrête pas l’oeuvre de tes mains.
SAINT EST LE SEIGNEUR NOTRE DIEU !