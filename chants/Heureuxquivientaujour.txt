1.
Heureux qui vient au jour
Par la nuit de Jésus,
Heureux qui se reçoit
De la main de son Dieu,
Heureux qui prend sa vie
Dans la mort de Jésus :
Des chemins s’ouvrent dans son coeur,
Il ne craint rien de la seconde mort.
Au paradis de Dieu,
Vainqueur, Il mangera de l’Arbre de la Vie.

2.
Heureux qui naît d’en-haut
Et du sang de Jésus,
Heureux qui est greffé
Sur le Corps de son Dieu,
Heureux qui est la proie
De l’Esprit de Jésus :
Des chemins s’ouvrent dans son coeur,
Il goûtera de la Manne cachée.
Au paradis de Dieu,
Vainqueur, Il recevra la Pierre au Nom béni.

3.
Heureux qui tient son nom
Du seul Nom de Jésus,
Heureux qui est heureux
Du bonheur de son Dieu,
Heureux qui met son pas
Dans les pas de Jésus :
Des chemins s’ouvrent dans son coeur,
Il détiendra Pouvoir sur les nations.
Au paradis de Dieu,
Vainqueur, Il recevra l’Étoile du matin.

4.
Heureux qui va joyeux
Sous le joug de Jésus,
Heureux qui s’en remet
Au désir de son Dieu,
Heureux qui rend l’esprit
Par la voix de Jésus :
Des chemins s’ouvrent dans son coeur,
Il siégera sur le trône du Fils.
Au paradis de Dieu,
Vainqueur, Il recevra les vêtements de joie.

5.
Heureux qui perd sa vie
Sur la croix de Jésus,
Heureux qui reçoit tout
De la main de son Dieu,
Heureux qui meurt d’aimer
Dans l’amour de Jésus :
Des chemins s’ouvrent dans son coeur,
Il ira boire à la source du ciel.
Au paradis de Dieu,
Vainqueur, Il recevra l’Héritage promis.