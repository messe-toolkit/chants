IL NOUS A FAITS, ET NOUS SOMMES À LUI,
NOUS, SON PEUPLE, SON TROUPEAU.

1.
Acclamez le Seigneur, terre entière,
servez le Seigneur dans l’allégresse,
venez à lui avec des chants de joie !

2.
Reconnaissez que le Seigneur est Dieu :
il nous a faits, et nous sommes à lui,
nous, son peuple, son troupeau.

3.
Oui, le Seigneur est bon,
éternel est son amour,
sa fidélité demeure d’âge en âge.