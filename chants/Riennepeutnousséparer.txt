RIEN NE PEUT NOUS SÉPARER
DE L’AMOUR DU SEIGNEUR
RIEN NE PEUT NOUS ÉLOIGNER
DE SON COEUR.

1.
Son amour est si grand
À travers tous les temps
Il nous porte en avant
Fait de nous des vivants.

2.
Son amour nous soutient
Quand la peur nous retient
Il se met à genoux
Quand il souffre avec nous.

3.
Son amour est si fort
Qu’au-delà de la mort
Il nous donne la vie
Qui jamais ne finit.

4.
Son amour comme un feu
Nous envoie deux par deux
Il éclaire nos pas
Au chemin de la foi.