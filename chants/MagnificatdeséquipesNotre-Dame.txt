R.
Magnificat, magnificat anima mea Dominum.

VERSET
Mon âme exalte le Seigneur, exulte mon esprit en Dieu, mon Sauveur !
Il s'est penché sur son humble servante ; désormais, tous les âges me diront bienheureuse.
Le Puissant fit pour moi des merveilles ; Saint est son nom !
Son amour s'étend d'âge en âge sur ceux qui le craignent.
Déployant la force de son bras, il disperse les superbes.
Il renverse les puissants de leurs trônes, il élève les humbles.
Il comble de biens les affamés, renvoie les riches les mains vides.
Il relève Israël, son serviteur, il se souvient de son amour,
De la promesse faite à nos pères, en faveur d'Abraham et de sa race, à jamais.