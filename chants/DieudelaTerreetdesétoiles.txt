1.
Dieu de la terre et des étoiles,
Soleil et rythme de nos temps,
La nuit le jour te rendent gloire,
Tu es le Maître des vivants.

BÉNIS SOIS-TU DE SIÈCLE EN SIÈCLE,
BÉNIS SOIS-TU, SEIGNEUR !
BÉNIS SOIS-TU DE SIÈCLE EN SIÈCLE,
BÉNIS SOIS-TU, SEIGNEUR !

2.
Dieu des nations qui nous dominent,
Tu les mesures à ta grandeur.
Avant le soir elles déclinent,
Leurs jours se fanent comme une fleur.

3.
Dieu de sagesse et Dieu de force,
Qui donc dira : « Je te connais » ?
Tu vois si loin dans mes ténèbres,
Ouvre mon coeur à ton secret.

4.
Dieu de mon peuple et de mes pères,
C’est à toi seul que je recours.
Quand de partout les lions me cernent
Ton bras me sauve et je te loue.

5.
Dieu l’avenir de tes fidèles,
Tu es déjà le vrai bonheur.
Le Fils de l’homme se révèle,
Voici qu’il vient sur les hauteurs.