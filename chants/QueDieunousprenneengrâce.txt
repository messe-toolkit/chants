R.
Que Dieu nous prenne en grâce et qu'il nous bénisse!

Psaume 66
Que son visage s'illumjne pour nous;
et ton chem in sera conng sur la terre,
ton salut, parmi togtes les nations.
Que les nations chgntent leur joie,
car tu gouvernes le mQnde avec justice ;
sur la terre, tu condujs les nations.
La terre a donng son fruit;
Dieu, notre Dieg, nous bénit.
Que la terre tout en tigre l'adore 1