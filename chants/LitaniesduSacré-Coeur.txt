R.
Fais-nous revenir à toi, Seigneur !

1.
Cœur de Jésus, Fils du Père éternel,
Cœur de Jésus, né de la Vierge Marie,
Cœur de Jésus, uni au Verbe de Dieu.

2.
Cœur de Jésus, d'une infinie majesté,
Cœur de Jésus, temple très saint de Dieu,
Cœur de Jésus, demeure du Très-Haut.

3.
Cœur de Jésus, maison de Dieu et porte du ciel,
Cœur de Jésus, fournaise ardente de charité,
Cœur de Jésus, source de justice et d'amour.

4.
Cœur de Jésus, roi et centre de tous les cœurs,
Cœur de Jésus, trésor inépuisable de sagesse et de science,
Cœur de Jésus, en qui le Père met tout son amour.

5.
Cœur de Jésus, plein de patience et de miséricorde,
Cœur de Jésus, riche pour tous ceux qui t'invoquent,
Cœur de Jésus, attente de la terre.

6.
Cœur de Jésus, réparation de nos fautes,
Cœur de Jésus, saturé de mépris,
Cœur de Jésus, broyé pour nos péchés.

7.
Cœur de Jésus, obéissant jusqu'à la mort,
Cœur de Jésus, percé par la lance,
Cœur de Jésus, victime des pécheurs.

8.
Cœur de Jésus, notre vie et notre résurrection,
Cœur de Jésus, notre paix et notre réconciliation,
Cœur de Jésus, source de toute consolation.

9.
Cœur de Jésus, salut de qui espère en toi,
Cœur de Jésus, espoir de ceux qui meurent en toi,
Cœur de Jésus, bonheur de tous les saints.