Refrain :
S’accrocher au soleil,
Et puis savoir comprendre,
Ce qu’il y a d’essentiel,
Quand la fleur devient fruit...
S’accrocher au soleil,
Et puis vouloir comprendre,
Ce qu’il y a d’essentiel,
Dans ce qui nous unit...

1.
Juste un mot pour te dire,
Credo prémonitoire,
Croire au besoin de croire,
C’est forcément fleurir ! ...

2.
Juste un mot pour te dire,
Ce destin météore,
Puisque déjà éclore,
C’est forcément mûrir ! ...

3.
Juste un mot pour te dire,
La clé du “ vivre ensemble ”,
La chercher, il me semble,
C’est forcément grandir ! ...