LE CHEMIN DU BONHEUR EST POUR TOI,
DIEU T’ÉVEILLE AU SECRET DE SA JOIE.

(Strophes lues sur fond musical)

1.
Heureux es-tu
si l’argent, le confort et tous les biens
ne sont pas ton unique souci
ni les seules richesses de ta vie.
Alors s’ouvrira la porte de ton coeur
aux trésors d’humanité qu’il te reste à découvrir
et à faire fructifier pour le bonheur de tous.

2.
Heureux es-tu
si tu fermes la route à la violence
et à l’instinct de dominer tes semblables.
Alors s’ouvrira la porte de ton coeur
à la force de la douceur et de la maîtrise de soi,
et la terre deviendra ta part d’héritage.

3.
Heureux es-tu
si tu ne crains pas de vibrer avec ton prochain
affronté à la douleur, la solitude ou la misère.
Alors s’ouvrira la porte de ton coeur
aux larmes de la compassion et au geste qui relève,
et toi aussi, tu seras consolé.

4.
Heureux es-tu
si la faim et la soif de la justice gardent leur brûlure au fond de toi
et font monter ta révolte devant les injustices.
Alors s’ouvrira la porte de ton coeur,
et tu combattras les esclavages de tes frères et de tes soeurs ;
c’est là que tu trouveras le pain qui rassasie.

5.
Heureux es-tu
si tu résistes au réflexe de condamner quiconque
en raison de ses faiblesses, de ses erreurs ou de ses fautes.
Alors s’ouvrira la porte de ton coeur,
et tu connaîtras la joie de pardonner
et d’être toi-même pardonné.

6.
Heureux es-tu
si le regard que tu portes sur autrui
est pur de tout désir de possession.
Alors s’ouvrira la porte de ton coeur
à Celui qui nous aime d’amour gratuit,
et tu découvriras le divin qui éclaire ta vie.

7.
Heureux es-tu
si tu te compromets pour la paix
et si tu t’engages à la bâtir durablement.
Alors s’ouvrira la porte de ton coeur,
et tu seras appelé du beau nom de fils de Dieu.

8.
Heureux es-tu
si tu acceptes de risquer ta vie
pour la justice et la vérité, à l’image de Jésus de Nazareth.
Alors s’ouvrira la porte de ton coeur,
et tu entendras dans le secret, montant de l’infini,
le chant nouveau du Royaume qui vient.