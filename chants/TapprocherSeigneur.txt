1.
T'approcher, Seigneur, je n'en suis pas digne,
Mais que ta Parole conduise mes pas
Et je serai guéri.

2.
Te parler, Seigneur, je n'en suis pas digne,
Mais que ta Parole demeure ma joie
Et je serai guéri.

3.
T'inviter, Seigneur, je n'en suis pas digne,
Mais que ta Parole habite mon toit
Et je serai guéri.

4.
Te servir, Seigneur, je n'en suis pas digne ;
Mais que ta Parole nourrisse ma foi,
Et je serai guéri.

5.
Te chanter, Seigneur, je n'en suis pas digne,
Mais que ta Parole traverse ma voix,
Et je serai guéri.