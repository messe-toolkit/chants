1.
Tout avait commencé dans un climat de fête,
Les foules assoiffées buvaient à sa Parole.
Les enfants s’amusaient, dansant la farandole,
Mais voici que la faim nous monta à la tête !

Refrain 1.
JÉSUS, VIENS FAIRE LA FÊTE
AU COEUR DE NOTRE VIE !
L’HUMANITÉ EN QUÊTE
VEUT GOÛTER À LA VIE !

2.
L’endroit était désert, loin des choses du monde !
Comment allait-on faire pour nourrir tant de monde ?
Un enfant avait bien cinq pains et deux poissons,
Mais était-ce suffisant pour nourrir tout ce monde ?

3.
Jésus s’en rendit compte et fit asseoir sur l’herbe
Au moins cinq mille personnes sans compter les enfants !
Prenant alors les pains, et rendant grâce au Père :
« Donnez très largement à tous ces braves gens ! »

Refrain 2.
JÉSUS, PAIN DE LA FÊTE,
JÉSUS, PAIN DE LA VIE !
AU COEUR DE NOTRE PÈRE,
C’EST TOI QUI NOUS CONDUIS!

4.
Ce fut un Pain de fête, jamais vu jusque-là !
Ce fut un vrai Repas, ce fut une folie !
Lors, Jésus leur parla du vrai Pain de la Vie,
Leur annonçant déjà qu’il était ce Pain-là !

5.
Beaucoup furent offusqués devant de tels propos !
Et la foule stupéfaite commençait à partir.
C’est alors que Jésus s’adressa aux apôtres :
« Vous qui m’avez suivi, me quitterez-vous aussi ? »

6.
Pierre prenant la parole, sans trop bien y comprendre,
Trouva cette réponse admirable à entendre :
« À qui irions-nous donc, Jésus de Nazareth ?
En Toi sont les paroles de la Vie Éternelle ! »