R.
N'hésite plus !  Aventure toi !
Le plus dur est souvent d'essayer.
Mais si tu es bien entouré,
Alors tu peux aller au bout de tes projets.
Aventure toi ! Aventure toi !

1.
Allez, termine ton sac,               
Car aujourd'hui c'est le grand jour.
Pas le temps d'avoir le trac,
Tu peux prendre la route à ton tour.

2.
Tu as tout préparé,
Des mois que tu attendais ça.
Soudée et motivée,
L'équipe est prête pour le départ.

3.
N'aies pas trop de questions,
En apportant tes compétences,
Ensemble à l'unisson
Les problèmes n'ont plus d'importance.

4.
Surtout ne t'arrête pas,
Construit tes rêves, laisse toi guider.
C'est possible d'y arriver.