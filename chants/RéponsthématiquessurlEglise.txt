7.
Refrain : Vous êtes le temple de Dieu :
* l’Esprit de Dieu habite en vous.
verset 1 : Tout vous appartient, la vie et la mort,
le présent et l’avenir. *
verset 2 : Vous êtes au Christ et le Christ est à Dieu.
8.
Refrain : Heureux les habitants de ta maison, Seigneur !
verset 1 : Des chemins s’ouvrent dans leur coeur.
verset 2 : Ils vont de hauteur en hauteur
vers les demeures du Très-Haut.
9.
Refrain : Sur tes remparts, Jérusalem,
j’ai placé des veilleurs.
verset 1 : Ni de jour, ni de nuit, ils ne doivent se taire.
verset 2 : Tenez en éveil la mémoire du Seigneur.
doxologie : Gloire au Père et au Fils et au Saint Esprit.