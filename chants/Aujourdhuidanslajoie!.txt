Stance
Aujourd’hui, dans la joie, l’univers fête son Roi.
L’Église célèbre le Seigneur, Alpha et Oméga.
Sa Sainteté emplit toute sa maison.

R.
JÉSUS CHRIST, PREMIER NÉ D’ENTRE LES MORTS,
SOIS POUR NOUS LE ROI DE NOS COEURS!

Versets

1.
Tu nous as délivrés de nos péchés par ton sang.
Tu as fait de nous le royaume et les prêtres de Dieu, ton Père.

2.
Devant Pilate, tu as rendu un beau témoignage.
Ta vérité est la fidélité de ton amour.

3.
Dans les cieux, tous les hommes te verront.
Ils regarderont Celui qu’ils ont transpercé.

4.
Ta royauté n’est pas de ce monde.
C’est ta passion glorifiée par le Père de toute éternité.