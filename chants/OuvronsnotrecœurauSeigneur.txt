R.
OUVRONS NOTRE COEUR AU SEIGNEUR,
IL VIENDRA Y FAIRE SA DEMEURE,
LE LIEU DE SON REPOS,
MYSTÈRE DE PRÉSENCE ET D’AMOUR.

1.
Soyons attentifs à sa Parole,
elle nous révèle Dieu notre Père,
et illumine notre coeur.

2.
N’ayons pas peur de sa Parole,
elle nous dit la vérité
et ordonne notre vie à la Bonne Nouvelle.

3.
Laissons-nous saisir par sa Parole,
elle nous dit le dessein d’Amour de Dieu
et nous le fait proclamer.

4.
Laissons-nous guider par sa Parole,
elle nous conduit à la Vie par Jésus se livrant
par amour pour nous jusqu’à la croix.

5.
Laissons-nous renouveler par sa Parole,
c’est le don gratuit de l’Amour
que Dieu donne à tous les hommes.