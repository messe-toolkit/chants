R.
FILS DE DIEU RESSUSCITE,
TON ESPRIT EST NOTRE PAQUE,
FILS DE DIEU RESSUSCITE ,
FAIS NOUS VIVRE TON MESSAGE.

1.
Ne cherchez pas le crucifié,
dans vos tombeaux si bien scellés,
il est vivant comme il l'a dit.
Soyez sans crainte il vous précède
et vous fait signe en Galilée :
vous le verrez dans sa lumière.

2.
Quittez le froid de vos Judée,
portez le feu des messagers,
le Vent de Dieu fait ressurgir !
Réveillez-vous, le Christ appelle !
Au rendez-vous en Galilée
vous apprendrez sa joie nouvelle.

3.
Allez au monde et révélez
le chant pascal du Premier-Né :
la porte s'ouvre à l'avenir !
Annoncez-le à tous vos frères
En vous Jésus veut demeurer.
Dieu vous baptise en sa lumière.