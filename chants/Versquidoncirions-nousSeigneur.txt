Antienne - stance
Vers qui donc irions-nous, Seigneur ?
Tu as les paroles de la vie éternelle.
Nous croyons que tu es le saint de Dieu.

R.
Jesu redemptor omnium, tu lumen et splendor Patris,
tibi sit gloria, tibi Jesu sit gloria.

Versets

Ps 88, 2-6 ; 53

1.
L'amour du Seigneur, sans fin je le chante ;
Ta fidélité, je l'annonce d'âge en âge.
Je Le dis : C'est un amour bâti pour toujours ;
Ta fidélité est plus stable que les cieux.

2.
« Avec mon élu, j'ai fait une alliance,
J'ai juré à David, mon serviteur :
J'établirai ta dynastie pour toujours,
Je te bâtis un trône pour La suite des âges. »

3.
Que les cieux rendent grâce pour ta merveille, Seigneur,
Et l'assemblée des saints, pour ta fidélité.
Béni soit le Seigneur pour toujours !
Amen ! Amen !

Ou bien

Ps 144, 1-2 ; 8-14

1.
Je t'exalterai, mon Dieu, mon Roi,
Je bénirai ton nom toujours et à jamais !
Chaque jour je te bénirai,
Je louerai ton nom toujours et à jamais.

2.
Le Seigneur est tendresse et pitié,
Lent à la colère et plein d'amour ;
La bonté du Seigneur est pour tous,
Sa tendresse, pour toutes ses œuvres.

3.
Que tes œuvres, Seigneur, te rendent grâce
Et que tes fidèles te bénissent !
Ils diront la gloire de ton règne,
Ils parleront de tes exploits,

4.
Annonçant aux hommes tes exploits,
La gloire et l'éclat de ton règne :
Ton règne, un règne éternel,
Ton empire, pour les âges des âges.

5.
Le Seigneur est vrai en tout ce qu'il dit,
Fidèle en tout ce qu'il fait.
Le Seigneur soutient tous ceux qui tombent,
Il redresse tous les accablés.