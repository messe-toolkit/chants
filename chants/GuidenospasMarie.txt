Guide nos pas, Marie,
Mère de Jésus, Mère de la Grâce,
Femme entre toutes bénie !

1.
Vierge très pure, Vierge immaculée,
Conduis-nous vers ton Fils !
Ô Marie, conçue sans péché,
Conduis-nous vers ton Fils !
Reine des cieux, source de pureté,
Conduis-nous vers ton Fils !

2.
Vierge prudente, méditant la parole,
Conduis-nous vers ton Fils !
Ô Marie, émerveillée de Dieu,
Conduis-nous vers ton Fils !
Mère de la Sagesse, montre-nous la lumière,
Conduis-nous vers ton Fils !

3.
Vierge très humble, sous le regard de Dieu,
Conduis-nous vers ton Fils !
Ô Marie, empressée de servir,
Conduis-nous vers ton Fils !
Reine des plus petits, proche de chacun,
Conduis-nous vers ton Fils !

4.
Vierge fidèle, que la foi met en marche,
Conduis-nous vers ton Fils !
Ô Marie, bienheureuse d’avoir cru,
Conduis-nous vers ton Fils !
Mère de notre foi, apprends-nous la confiance,
Conduis-nous vers ton Fils !

5.
Vierge joyeuse, exaltant le Seigneur,
Conduis-nous vers ton Fils !
Ô Marie, toute comblée de grâce,
Conduis-nous vers ton Fils !
Mère de la Beauté, prends-nous dans ta louange,
Conduis-nous vers ton Fils !

6.
Vierge attentive, à l’écoute de Dieu,
Conduis-nous vers ton Fils !
Ô Marie, servante du TrèsHaut,
Conduis-nous vers ton Fils !
Reine des disciples, première à obéir,
Conduis-nous vers ton Fils !

7.
Vierge très pauvre, sans or et sans refuge,
Conduis-nous vers ton Fils !
Ô Marie, riche d’avoir donné,
Conduis-nous vers ton Fils !
Mère du Sauveur, qui n’a rien retenu,
Conduis-nous vers ton Fils !

8.
Vierge patiente, qui attend l’Enfant-Dieu,
Conduis-nous vers ton Fils !
Ô Marie, qui Le suis sans comprendre,
Conduis-nous vers ton Fils !
Mère vigilante, debout près de la Croix,
Conduis-nous vers ton Fils !

9.
Vierge très bonne, remplie de charité,
Conduis-nous vers ton Fils !
Ô Marie, soucieuse de notre joie,
Conduis-nous vers ton Fils !
Mère de l’Amour, Mère de miséricorde,
Conduis-nous vers ton Fils !

10.
Vierge des Douleurs, à l’heure du passage,
Conduis-nous vers ton Fils !
Ô Marie, prie pour nous pécheurs,
Conduis-nous vers ton Fils !
Mère de l’Église, Mère de tous les hommes,
Conduis-nous vers ton Fils !