JEAN-PAUL ET JEAN, DIEU VOUS BÉNIT,
L’ÉGLISE EN MARCHE VOUS CÉLÈBRE.
VERS LE BONHEUR DE PLEINE VIE
GUIDEZ NOS PAS, BERGERS FIDÈLES !
PAR JÉSUS CHRIST LE SERVITEUR.
NOUS CONNAÎTRONS LA VRAIE GRANDEUR.

1.
Grandeur de l’homme baptisé
Qui peut renaître avec ses frères !
Amis de Dieu dès la jeunesse,
Vous allez droit sur ses sentiers.

2.
Grandeur de l’homme révolté
Par tant de cris montés du monde !
Semeurs d’espoir dans vos réponses,
Vous témoignez : Dieu vient sauver.

3.
Grandeur de l’homme au coeur ouvert
À toute paix qui se dessine !
Elle est un fruit de la justice,
Le chant nouveau que vous lancez.

4.
Grandeur de l’homme traversé
Par les souffrances des plus pauvres !
Face aux puissants qui déraisonnent
Vous clamez haut leur dignité.

5.
Grandeur de l’homme émerveillé
Par l’océan des oeuvres belles !
En louant Dieu pour ses merveilles
Vous maintenez nos yeux levés.

6.
Grandeur de l’homme questionné
Par l’aujourd’hui de nos Églises !
Vous donnez souffle au grand Concile
Qui renouvelle nos années.

7.
Grandeur de l’homme consacré
à la moisson de l’Évangile !
Heureux service qui fait vivre,
Et pour lequel vous appelez.

8.
Grandeur de l’homme glorifié
Dans l’au-delà de notre terre.
Jean-Paul et Jean, saintes lumières,
Dieu vous a pris dans sa clarté.