R.
Chantez, priez, célébrez le Seigneur,
Dieu nous accueille, peuple du monde.
Chantez, priez, célébrez son Nom,
Dieu nous accueille dans sa maison.

1.
Il a fait le ciel et la terre,
Éternel est son amour,
Façonné l’homme à son image,
Éternel est son amour.

2.
Il sauva Noé du déluge,
Éternel est son amour,
L’arc-en-ciel en signe d’alliance,
Éternel est son amour.

3.
D’Abraham, il fit un grand peuple,
Éternel est son amour,
Par milliers fut sa descendance,
Éternel est son amour.

4.
Il perçut le cri de son peuple,
Éternel est son amour,
Le guida en terre promise,
Éternel est son amour.

5.
Aux exilés de Babylone,
Éternel est son amour,
Il donna la foi qui libère,
Éternel est son amour.

6.
Il a parlé par les prophètes,
Éternel est son amour,
Sa parole est une promesse,
Éternel est son amour.

7.
Il combla Marie de sa grâce,
Éternel est son amour,
Il se fit chair parmi les hommes,
Éternel est son amour.

8.
Crucifié, c’est sa vie qu’Il donne,
Éternel est son amour,
Mais le Père le ressuscite,
Éternel est son amour.

9.
Dans l’Esprit Saint, Il nous baptise,
Éternel est son amour,
Son amour forge notre Église,
Éternel est son amour.

10.
Acclamez Dieu, ouvrez le Livre,
Éternel est son amour,
Dieu nous crée et Dieu nous délivre,
Éternel est son amour.