ALLÉLUIA, ALLÉLUIA.

Le Seigneur a envoyé Jésus, son Serviteur,
porter la Bonne Nouvelle aux pauvres,
annoncer aux prisonniers qu’ils sont libres.

ALLÉLUIA.