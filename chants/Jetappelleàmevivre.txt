R.
Je t’appelle à me suivre,
Je t’appelle à me vivre,
Tu me cherches dans ta vie,
Mais c’est moi qui te choisis,
Tu me cherches dans ta vie,
Mais c’est moi qui te choisis.

1.
J’ai besoin de toi
Pour toucher le cœur des hommes ;
Il me faut ta voix
Pour annoncer ma parole.

2.
J’ai besoin de toi
Pour semer dans la lumière ;
Il me faut tes bras
Pour ensemencer la terre.

3.
J’ai besoin de toi
Pour chanter Bonne Nouvelle ;
Il me faut ton cœur
Pour que l’amour étincelle.