R.
En Jésus Christ nous sommes
Créatures nouvelles.
Baptisés dans l’Esprit,
Nous rendons gloire à notre Père.

1.
Le monde ancien s’en est allé,
Sur nous la nuit ne règne plus.
Un monde neuf est déjà né,
Le jour se lève avec Jésus.

2.
Le monde ancien s’en est allé,
La mort n’a plus le dernier mot.
Un monde neuf est déjà né,
La vie triomphe du tombeau.

3.
Le monde ancien s’en est allé,
Fini le temps des fruits amers.
Un monde neuf est déjà né,
L’eau vive coule en nos déserts.

4.
Le monde ancien s’en est allé,
Mais son vent froid nous blesse encor.
Un monde neuf est déjà né
Où le bon grain doit être fort.

5.
Le monde ancien s’en est allé
Comme s’en vont les ouragans.
Un monde neuf est déjà né
Qu’il faut bâtir en y croyant.