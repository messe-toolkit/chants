1.
À Moriah,
Quand vint le moment où Dieu l’éprouva,
La foi d’Abraham
A sauvé son fils,
Le fils de la Promesse :
Bénie sera sa descendance !

R.
TOUT EST POSSIBLE À CELUI QUI CROIT !

2.
Au Sinaï,
Où Dieu lui parlait comme à un ami,
Voici que Moïse
A reçu la Loi,
La Loi pour tout le peuple :
Tiendra-t-il ferme dans l’Alliance ?

3.
Au Mont Carmel,
Où il éleva pour Dieu un autel,
Le prophète Élie
A défié les dieux,
Les dieux faits de mains d’homme !
Venu du ciel un feu s’élance.

4.
Sur le Thabor,
Quand l’élu de Dieu annonçait sa mort,
Pierre, Jacques et Jean,
Ont pu voir l’éclat,
L’éclat de son visage :
Le suivront-ils dans la souffrance ?

5.
Au Golgotha,
Quand le Fils de Dieu mourait sur la Croix,
La Vierge Marie
Était là, debout,
Debout dans les ténèbres !
Pâque dira son espérance.