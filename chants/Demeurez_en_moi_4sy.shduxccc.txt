R.
Comme le Père m'a aimé,
moi aussi, je vous ai aimés.
Demeurez en moi, demeurez en moi,
demeurez en moi, comme moi en vous,
demeurez en mon amour ! (bis)

1.
Je suis la vigne véritable,
et mon Père en est le vigneron.
Celui qui en moi porte du fruit,
il l'émonde,
afin qu'il donne davantage.

2.
S'il ne demeure sur la vigne,
le sarment ne peut pas porter de fruit.
Ainsi en est-il de votre vie,
en dehors de moi,
vous ne pouvez rien faire.

3.
Si mes paroles en vous demeurent,
et qu'en moi, vous demeurez aussi,
demandez tout ce que vous voudrez,
vous serez comblés de dons en abondance.

4.
Pour que ma joie en vous demeure,
une joie qui comble votre coeur,
je vous ai fait le don de ma loi,
aimez-vous,
de même que moi je vous aime.

5.
Je vous choisis et vous appelle,
en mon nom, allez, portez du fruit.
Il n'y a pas de plus grand amour,
que d'offrir sa propre vie
pour ceux qu'on aime.