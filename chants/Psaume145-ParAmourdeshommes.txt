STANCE
Par amour des hommes, Jésus, mon Sauveur,
Tu passais en faisant le bien.
Maître qui éclaires les cœurs,
Berger qui rassembles ton troupeau,
Toi qui panses toute blessure,
Si tu le veux, tu peux me guérir.

R.
Seigneur Jésus, si tu le veux, tu peux me guérir.

1.
Loue, ô mon âme, le Seigneur !
Je veux louer le Seigneur tant que je vis.

2.
Il rend justice aux opprimés,
Il donne aux affamés du pain.

3.
Il a pris sur lui nos infirmités,
Il s'est chargé de nos maladies.

Gloire au Père et au Fils et au Saint-Esprit, pour les siècles des siècles. Amen.