GARDE-MOI, MON DIEU :
J'AI FAIT DE TOI MON REFUGE.
J'AI DIT AU SEIGNEUR : « TU ES MON DIEU !
JE N'AI PAS D'AUTRE BONHEUR QUE TOI. »

1.
Toutes les idoles du pays,
ces dieux que j'aimais,
ne cessent d'étendre leurs ravages,
et l'on se rue à leur suite.
Je n'irai pas leur offrir le sang des sacrifices ;
leur nom ne viendra pas sur mes lèvres !

2.
Seigneur, mon partage et ma coupe :
de toi dépend mon sort.
La part qui me revient fait mes délices ;
j'ai même le plus bel héritage !

3.
Je bénis le Seigneur qui me conseille :
même la nuit mon coeur m'avertit.
Je garde le Seigneur devant moi sans relâche ;
il est à ma droite : je suis inébranlable.

4.
Mon coeur exulte, mon âme est en fête,
ma chair elle-même repose en confiance :
tu ne peux m'abandonner à la mort
ni laisser ton ami voir la corruption.

5.
Tu m'apprends le chemin de la vie :
devant ta face, débordement de joie !
À ta droite, éternité de délices !