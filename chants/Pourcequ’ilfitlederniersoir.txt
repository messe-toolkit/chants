1.
Pour ce qu’il fit le dernier soir,
Ce qu’il a dit à ses apôtres
D’accomplir en sa mémoire,
Nous rendons grâce.
Nous le croyons :
Il a remis entre nos mains
Le pain et le vin de l’alliance.

2.
Pour ce qu’il fait en ce temps-ci,
Tout ce qu’il dit à ses disciples
Quand l’Église ouvre le livre,
Nous rendons grâce.
Nous le croyons :
Il a remis à notre voix
Les mots de la Bonne Nouvelle.

3.
Pour ce qu’il donne en se livrant,
Son corps, son sang, devenus nôtres,
Et l’appel de sa présence,
Nous rendons grâce.
Nous le croyons :
Il fait lever le don de Dieu
Au coeur de nos vies quotidiennes.

4.
Pour ce qu’il mène jusqu’à Dieu,
Ce qu’il saisit dans le mystère
De sa Pâque silencieuse
Nous rendons grâce.
Nous le croyons :
Il fait entrer l’humanité
Au coeur de l’offrande éternelle.