R.
SEIGNEUR JESUS-CHRIST,
SEIGNEUR JESUS-CHRIST,
OU DEMEURES-TU ?
MAITRE DE LA VIE, MAITRE DE LA VIE,
OU TE CACHES-TU ?

1.
Vous qui cherchez un soleil,
Venez et voyez, (bis)
Christ est ressuscité.
Allelu, Allelu, Alleluia. (bis)
Laissez votre peur,
Aimez le Seigneur,
Recevez le feu de son cœur.

2.
Vous qui cherchez un chemin,
Venez et croyez, (bis)
Christ est ressuscité.
Allelu, Allelu, Alleluia. (bis)
Veillez aujourd’hui,
Suivez Jésus-Christ,
Empruntez les pas de sa vie.

3.
Vous qui cherchez le bonheur,
Venez et dansez, (bis)
Christ est ressuscité.
Allelu, Allelu, Alleluia. (bis)
Chantez au matin,
Goûtez le vrai pain,
Recueillez la paix dans vos mains.

4.
Vous qui cherchez le Messie,
Venez et priez, (bis)
Christ est ressuscité.
Allelu, Allelu, Alleluia. (bis)
Chassez tous les bruits,
Guettez dans la nuit,
Ecoutez la voix de l’Esprit.