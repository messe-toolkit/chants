R.
C'est Jésus qui nous rassemble ! C'est Jésus notre Sauveur !
En Église, tous ensemble, demeurons Corps du Seigneur !
C'est Jésus qui nous rassemble ! C'est Jésus notre Sauveur !

1.
Dieu immense et Dieu si proche, le Seigneur est avec nous !
Dieu fait chair en notre monde, le Seigneur est avec nous !
Fils de l'Homme, notre frère, le Seigneur est avec nous !

2.
Envoyé vers les plus pauvres, le Seigneur est avec nous !
Partageant toute misère, le Seigneur est avec nous !
Pour montrer l'amour du Père, le Seigneur est avec nous !

3.
Pain de Vie, vraie nourriture, le Seigneur est avec nous !
Pain rompu pour notre monde, le Seigneur est avec nous !
Pain de Dieu pour toute route, le Seigneur est avec nous !

4.
Dieu, lumière en nos ténèbres, le Seigneur est avec nous !
Dieu de grâce, Dieu fidèle, le Seigneur est avec nous !
Pour la joie de son Église, le Seigneur est avec nous !

5.
Par l'Esprit qui nous fait vivre, le Seigneur est avec nous !
Par son Souffle dans nos âmes, le Seigneur est avec nous !
Pour la joie de son Église, le Seigneur est avec nous !

6.
Messager de l'Espérance, le Seigneur est avec nous !
Espérance du Royaume, le Seigneur est avec nous !
Maintenant et d'âge en âge, le Seigneur est avec nous !