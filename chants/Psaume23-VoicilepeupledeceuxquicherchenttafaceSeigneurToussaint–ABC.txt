VOICI LE PEUPLE DE CEUX QUI CHERCHENT TA FACE,
SEIGNEUR.

1.
Au Seigneur, le monde et sa richesse,
la terre et tous ses habitants !
C’est lui qui l’a fondée sur les mers
et la garde inébranlable sur les flots.

2.
Qui peut gravir la montagne du Seigneur
et se tenir dans le lieu saint ?
L’homme au coeur pur, aux mains innocentes,
qui ne livre pas son âme aux idoles.

3.
Il obtient, du Seigneur, la bénédiction,
et de Dieu son Sauveur, la justice.
Voici le peuple de ceux qui le cherchent !
Voici Jacob qui recherche ta face !