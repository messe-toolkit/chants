R.
LAISSONS-NOUS RÉCONCILIER PAR LE CHRIST
AVEC DIEU NOTRE PÈRE !
LAISSONS-NOUS RÉCONCILIER PAR LE CHRIST
AVEC NOS FRÈRES!

1.
Je me lèverai et je reviendrai vers mon Père,
et je lui dirai :
j’ai vraiment péché contre toi,
Pardonne-moi ! Pardonne-moi !

2.
J’ai vu son regard : il n’est pas de ceux qui me jugent,
Mais j’ai lu en lui
tout l’amour qu’il veut me donner,
Dieu mon sauveur ! Dieu mon sauveur !

3.
J’ai vu ses deux mains qui se tendent et qui me relèvent,
Et ses bras offerts
qui me serrent contre son coeur,
Dieu de pardon ! Dieu de pardon !

4.
Non, je n’ai pas peur de ce Père qui me redresse,
Il me traite encor
comme son enfant bien-aimé,
Dieu plein d’amour ! Dieu plein d’amour !

5.
Je me suis levé, je me suis tourné vers mon frère
Et je lui ai dit :
j’ai vraiment brisé l’unité,
Accueille-moi ! Accueille-moi !

6.
Voici le Repas préparé par Dieu notre Père,
Amour partagé,
table ouverte aux coeurs libérés,
Dieu de la joie ! Dieu de la joie !

7.
Voici la mission que Dieu nous confie sur la terre,
Amour et pardon
pour un monde réconcilié,
Dieu de la paix ! Dieu de la paix !