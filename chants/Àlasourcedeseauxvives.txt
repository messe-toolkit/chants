ANNÉE A

1er dimanche de carême A

R.
A LA SOURCE DES EAUX VIVES
CONDUIS-NOUS, BERGER DIVIN ;
DIS LES MOTS QUI FONT REVIVRE,
TA PAROLE EST NOTRE PAIN.

1.
Dans le désert où tu nous mènes,
Qui peut te suivre, ô Jésus Christ ?
Es-tu Celui qui se révèle,
Quarante jours, quarante nuits ?
Pour notre marche vers la Terre,
Baptise-nous dans ton Esprit.

2.
Nous avons faim de ta parole,
C’est notre manne d’aujourd’hui.
Les mots de chair qui nous façonnent,
Tu les annonces par ta vie.
Parle à nos coeurs de ton Royaume,
Présence et paix de ton Esprit.

3.
Viens nous guérir de nos faiblesses
Et donne-nous de repartir ;
Nous connaîtrons le Dieu tendresse
Aux fruits d’amour qu’il fait mûrir.
Relève-nous, Justice appelle
Aux durs combats de ton Esprit.

2e Dimanche de carême A

R.
TU RÉVÈLES TON MYSTÈRE,
FILS DE DIEU TRANSFIGURÉ.
FAIS-NOUS VIVRE À TA LUMIÈRE,
SUR TES PAS FAIS-NOUS MARCHER.

1.
Pour tes amis sur la montagne,
Voici l’instant d’éternité :
Par la clarté de ton visage
Dieu manifeste qui tu es.
Heureux celui qui te regarde,
Il voit ton ciel sur les sommets.

2.
Moïse entend la Loi nouvelle,
L’ancienne alliance est rappelée ;
Elie contemple le Prophète
Dont la parole est vérité.
Nous écoutons la voix du Père :
« Voici mon Fils, mon Bien-Aimé. »

3.
Avec l’Apôtre en plein mystère
Il nous est bon de vivre ici ;
Auprès de toi dans la lumière
Nous resterions toute la vie !
Nuée de gloire sur la terre,
Mais quel regard peut la saisir ?

4.
Première annonce de la Pâque,
Le feu nouveau est révélé.
Heureux qui marche sur tes traces,
Jésus, Sauveur d’humanité !
Viendra ton Jour sur la montagne,
Le temps des fils transfigurés.

3e Dimanche de carême A

R.
TOI QUI FAIS JAILLIR L’EAU VIVE,
DONNE-NOUS TON ESPRIT SAINT !
MONTRE-NOUS LA ROUTE À SUIVRE,
TOI, JÉSUS, LE VRAI CHEMIN !

1.
Dans Samarie c’est la rencontre,
Tu viens t’asseoir à notre puits,
Et tu nous dis nos routes d’ombre,
Toi le Soleil du plein midi.
Eclaire-nous, Sauveur du monde,
Et fais-nous boire à la vraie Vie.

2.
Dans le secret fais-toi connaître,
Nous avons soif de vérité.
Tu es Celui qui fait renaître
Sur des chemins de liberté.
Que nos maisons te soient ouvertes,
A toi qui cherches où demeurer.

3.
Nous devenons pour toi le Temple
Où toute vie peut jubiler.
Ton Esprit souffle et nous rassemble,
Il dit les mots pour adorer.
Avec amour chacun te chante
Le chant nouveau des coeurs sauvés.

4e Dimanche de carême A

R.
FILS DE DIEU DANS LA LUMIÈRE,
SOIS VAINQUEUR DE NOTRE NUIT !
VIENS SAUVER CEUX QUI T’ESPÈRENT,
DONNE-LEUR LE JOUR PROMIS !

1.
Tu vois l’aveugle de naissance
Mendiant le pain sous le mépris.
Qui peut répondre à sa demande,
Sinon le coeur qui l’a compris ?
Pour le sortir de la tourmente,
Tu fais le geste qui guérit.

2.
Il va chercher à la fontaine
Les eaux qui vont le purifier.
Pour nous, Seigneur, tu fais de même,
Tu nous envoies vers Siloé,
La Source où chante la lumière
Dans la Maison de l’Envoyé.

3.
Comment, Seigneur, te rendre grâce
Quand l’univers emplit nos yeux ?
O Fils de l’homme qui nous parles,
Tu es vraiment le Fils de Dieu.
Accorde-nous la joie de croire
A l’Invisible dans les cieux.

5e Dimanche de carême A

R.
QUE TA FORCE NOUS ARRACHE
A LA NUIT DE NOS TOMBEAUX !
TU CONNAIS LA JOIE DE PÂQUE,
FAIS LEVER LES TEMPS NOUVEAUX !

1.
A Béthanie, près de Lazare,
Tu viens en maître de la vie,
Mais c’est d’abord l’instant des larmes
Devant le corps de ton ami.
La mort nous blesse et nous désarme,
Au plus profond le coeur frémit.

2.
Pour toi c’est vivre qui importe,
Comme pour Marthe et pour Marie.
Témoin du Dieu qui sauve l’homme,
Tu nous réveilles par ton cri.
Voici que s’ouvre enfin la porte
Sur l’horizon de l’infini.

3.
Nous voulons croire à ton Royaume
Où toute mort sera brisée ;
Finis les pleurs et les opprobres,
Il fera jour à tout jamais.
Pourquoi faut-il si dur exode
Avant l’aurore de la paix ?

Strophe d’envoi, à la fin de la messe
avec refrain du dimanche

Enfants de Dieu par le baptême,
Suivons les pas de Jésus Christ !
Vivons le temps de ce carême
Ouverts au Souffle de l’Esprit.
Louange et gloire à notre Père,
Qui nous envoie porter sa vie !

ANNÉE B

1er dimanche de carême B

R.
A LA SOURCE DES EAUX VIVES
CONDUIS-NOUS, BERGER DIVIN ;
DIS LES MOTS QUI FONT REVIVRE,
TA PAROLE EST NOTRE PAIN.

1.
Dans le désert où tu nous mènes,
Qui peut te suivre, ô Jésus Christ ?
Es-tu Celui qui se révèle,
Quarante jours, quarante nuits ?
Pour notre marche vers la Terre,
Baptise-nous dans ton Esprit.

2.
Nous avons faim de ta parole,
C’est notre manne d’aujourd’hui.
Les mots de chair qui nous façonnent,
Tu les annonces par ta vie.
Parle à nos coeurs de ton Royaume,
Présence et paix de ton Esprit.

3.
Viens nous guérir de nos faiblesses
Et donne-nous de repartir ;
Nous connaîtrons le Dieu tendresse
Aux fruits d’amour qu’il fait mûrir.
Relève-nous, Justice appelle
Aux durs combats de ton Esprit.

Strophe d’envoi, à la fin de la messe
pour les cinq dimanches

Enfants de Dieu par le baptême,
Suivons les pas de Jésus Christ !
Vivons le temps de ce carême
Ouverts au Souffle de l’Esprit.
Louange et gloire à notre Père,
Qui nous envoie porter sa vie !

2e Dimanche de carême B

R.
TU RÉVÈLES TON MYSTÈRE,
FILS DE DIEU TRANSFIGURÉ.
FAIS-NOUS VIVRE À TA LUMIÈRE,
SUR TES PAS FAIS-NOUS MARCHER.

1.
Pour tes amis sur la montagne,
Voici l’instant d’éternité :
Par la clarté de ton visage
Dieu manifeste qui tu es.
Heureux celui qui te regarde,
Il voit ton ciel sur les sommets.

2.
Moïse entend la Loi nouvelle,
L’ancienne alliance est rappelée ;
Elie contemple le Prophète
Dont la parole est vérité.
Nous écoutons la voix du Père :
« Voici mon Fils, mon Bien-Aimé. »

3.
Avec l’Apôtre en plein mystère
Il nous est bon de vivre ici ;
Auprès de toi dans la lumière
Nous resterions toute la vie !
Nuée de gloire sur la terre,
Mais quel regard peut la saisir ?

4.
Première annonce de la Pâque,
Le feu nouveau est révélé.
Heureux qui marche sur tes traces,
Jésus, Sauveur d’humanité !
Viendra ton Jour sur la montagne,
Le temps des fils transfigurés.

3e Dimanche de carême B

R.
JÉSUS CHRIST, LE TEMPLE UNIQUE,
LA DEMEURE DU TRÈS-HAUT,
C’EST CHEZ TOI QUE DIEU HABITE
ET QU’IL DONNE UN COEUR NOUVEAU !

1.
Dans le secret fais-toi connaître,
Nous avons soif de vérité.
Tu es Celui qui fait renaître
Sur des chemins de liberté.
Que nos maisons te soient ouvertes,
A toi qui cherches où demeurer.

2.
Nous devenons pour toi le Temple
Où toute vie peut jubiler.
Ton Esprit souffle et nous rassemble,
Il dit les mots pour adorer.
Avec amour chacun te chante
Le chant nouveau des coeurs sauvés.

4e Dimanche de carême B

R.
VERS LA CROIX DU FILS DE L’HOMME
NOUS TENONS LES YEUX OUVERTS ;
SUR CE BOIS JÉSUS NOUS SAUVE,
PAR AMOUR IL S’EST OFFERT.

1.
Christ élevé sur le calvaire,
Fais que tout homme vienne à toi !
Il obtiendra la Vie nouvelle,
Jamais son feu ne s’éteindra.
Pour nous sortir de nos ténèbres
C’est Dieu lui-même qui t’envoie.

2.
Tu ne viens pas juger le monde,
Toi qui nous aimes libérés !
Chacun te donne sa réponse
En mots brûlants de vérité.
Lumière soit dans ta rencontre,
Nous marcherons à ta clarté.

5e Dimanche de carême B

R.
FILS DE DIEU TOMBÉ EN TERRE,
TU DEVIENS LE GRAIN QUI MEURT,
MAIS PAR TOI L’ESPRIT LIBÈRE
LE PRINTEMPS DANS TOUS LES COEURS.

1.
Voici venus le jour et l’heure
Où tu vas être glorifié :
Terrible nuit, suprême épreuve,
Gethsémani te voit trembler.
Mais quelle est donc la voix du Père
Où tu entends sa volonté ?

2.
Tu voudrais fort que ce baptême
Soit accompli et dépassé !
Même en aimant jusqu’à l’extrême
Tu sais la peur d’un supplicié.
Dans tes instants de mort humaine
Tu cries à l’homme abandonné.

3.
Pour attirer à toi les hommes
Ton corps souffrant est élevé.
Sur ton visage, ô Fils de l’homme,
Nous découvrons l’Agneau livré.
Ta croix nous dit que Dieu nous sauve
Car il nous aime le premier.

4.
Nous voulons croire à ton Royaume
Où toute mort sera brisée ;
Finis les pleurs et les opprobres,
Il fera jour à tout jamais.
Mais pourquoi donc pareil exode
Avant l’aurore de la paix ?