ON A BESOIN DE TOI, POUR PROTÉGER LA TERRE,
L’AIMER, LA RESPECTER, L’HABILLER DE LUMIÈRE.
ON A BESOIN DE TOUS, CHACUN À SA MANIÈRE,
ON CHERCHE DU SOLEIL, POUR NOTRE AMIE LA TERRE.

1.
Comme l’argile est modelée avec le souffle d’un potier,
Venez, créons à volonté du beau, du bon et puis du vrai.

2.
Comme les fleurs ont leur secret, au fond des coeurs il est gravé
Venez, il nous faut cultiver ce grand jardin si bien semé.

3.
Comme un trésor à préserver qui dans nos mains est déposé
Venez, ensemble on va tisser un peu d’amour à partager.