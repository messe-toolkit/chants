De tout mon cœur, Seigneur, je te rends grâce :
tu as entendu les paroles de ma bouche.
Je te chante en présence des anges,
vers ton temple sacré je me prosterne.

Je rends grâce à ton nom pour ton amour et ta vérité,
car tu t'élèves, au-dessus de tout, ton nom et ta parole.
Le jour où tu répondis à mon appel,
tu fis grandir en mon âme la force.

Tous les rois de la terre te rendent grâce
quand ils entendent les paroles de ta bouche.
Ils chantent les chemins du Seigneur :
« Qu'elle est grande, la gloire du Seigneur ! »

Si haut que soit le Seigneur, il voit le plus humble ;
de loin, il reconnaît l'orgueilleux.
Si je marche au milieu des angoisses, tu me fais vivre,
ta main s'abat sur mes ennemis en colère.

Ta droite me rend vainqueur.
Le Seigneur fait tout pour moi !
Seigneur, éternel est ton amour :
n'arrête pas l'œuvre de tes mains.