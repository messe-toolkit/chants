R.
PUISQUE TOUT AMOUR PREND RACINE,
J’EN PLANTERAI DANS MON JARDIN. (bis)

1.
J’en planterai avec patience
Chaque jour, à chaque saison
Pour en offrir en abondance
À ceux qui passent en ma maison
Et j’en mettrai sur mes fenêtres
Pour faire plaisir à mes voisins
Et puis un brin dans chaque lettre
Que j’écris aux amis lointains.

2.
Sur la place de mon village
Au carrefour de chaque rue
J’offrirai les fleurs en partage
Aux amis comme aux inconnus
S’il y a des gens qui me haïssent
Si j’me suis fait des ennemis
Je saurai signer l’armistice
En offrant des fleurs et des fruits.

3.
J’en planterai sur les frontières
Sur les murs de séparation
Les champs dévastés par la guerre
Entre les barreaux des prisons
Dans les salles de conférence
Où se réunissent les grands
Pour qu’ils oublient toute méfiance
Et redeviennent des enfants.

4.
Toi, qui écoutes ma rengaine
Toi, qui accueilles ma chanson
Emporte les fruits et les graines
En revenant dans ta maison
Si tu veux, partout à la ronde,
Les partager et les semer
On verra bientôt, dans le monde,
Fleurir les roses de la paix.