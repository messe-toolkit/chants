1.
Il est venu,
Lui que beaucoup ont attendu,
Il est venu en s’abaissant,
Des bergers l’ont reconnu :
Un enfant leur était né.

2.
Son nom béni
Éclôt aux lèvres de Marie ;
Son nom béni "Jésus Sauveur",
Et des pauvres l’ont redit :
Un Sauveur nous est donné.

3.
Nous avons cru
Cette merveille qu’ils ont vue,
Le don de Dieu à l’univers ;
Sa tendresse est apparue :
Un enfant dans une crèche.