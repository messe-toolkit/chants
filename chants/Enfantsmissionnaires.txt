Qu'ils sont beaux les pieds
De ceux qui vont porter
La Bonne Nouvelle au monde
L'Evangile de paix
Qu'elles sont belles les mains
De ceux qui n'ont plus rien
Parce qu'ils ont tout offert
Parce qu'ils se sont donnés
Enfants du monde entier
Nous sommes des milliers
Nous irons vers nos frères
Apporter la lumière
Depuis notre quartier
Jusqu'au bout de la terre
Envoyés-missionnaires
Oh oh oh...
Qu'ils sont beaux les yeux
Des compagnons de Dieu
Remplis de l'Esprit-Saint
D'espérance et de feu
Qu'ils sont beaux les coeurs
De ceux qui n'ont plus peur
De chercher en chacun
Ce qu'il a de meilleur
Au nom du Père
Au nom du Fils
Au nom de l'Esprit-Saint
Nous sommes frères
Au nom du Père
Au nom du Fils
Au nom de l'Esprit -Saint
Nous sommes missionnaires