DOUCE MAMAN, DOUCE
GARDE-MOI SUR TES GENOUX.
DOUCE MAMAN, DOUCE
METS TA JOUE CONTRE MA JOUE.
JE SUIS BIEN, TELLEMENT BIEN
AU PAYS DE TES CÂLINS.
JE SUIS BIEN, TELLEMENT BIEN
AU PAYS DE TES CÂLINS.

1.
Un câlin de toi
C’est tout bleu dans le ciel,
Un câlin de toi
Dans mon coeur c’est Noël.

2.
Un câlin de toi
Ça ressemble au velours,
Un câlin de toi
C’est couleur de l’amour.

3.
Un câlin de toi
C’est si tendre et si chaud,
Un câlin de toi
C’est toujours un cadeau.