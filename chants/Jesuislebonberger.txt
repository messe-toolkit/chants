R.
Je suis le bon berger,
Je connais mes brebis,
et mes brebis me connaissent.
Je suis le bon berger,
Je leur donne ma vie,
et toute ma tendresse.

1.
Chacune de mes brebis
Reconnaît ma voix
Si je viens en pleine nuit,
Vite on m'ouvrira !

2.
Mais si le portier s'endort
Dans ma bergerie,
Les pillards viendront alors
Voler mes brebis !

3.
Mes brebis ne savent pas
Suivre un étranger,
Elles reconnaissent à la voix
Tous les faux bergers !

4.
J'ai beaucoup d'autres brebis
Loin de cet enclos,
Je les conduirai ici
Avec mon troupeau.