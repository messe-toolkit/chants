R.
Jésus Christ notre espérance,
Vraie lumière dans la nuit,
Jésus Christ notre espérance,
Chante au coeur de toute vie !

1.
Espérer ton soleil
Tout au long de l’Avent,
Espérer simplement
Comme on attend quelqu’un,
Mais surtout se savoir attendu !
Notre Père nous dit ta venue.

2.
Réveiller notre amour
Tout au long de l’Avent ;
Tu demeures en nos temps
Et tu nous tiens la main.
Nous voilà chaque jour attendus,
Montre-nous ton visage inconnu.

3.
Cheminer vers Noël
Tout au long de l’Avent,
Oser croire humblement
Au grand bonheur qui vient.
Sur la terre où tu es l’Attendu
A tout homme tu dis : bienvenue !