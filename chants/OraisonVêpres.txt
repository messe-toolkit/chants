Père,
toi qui as merveilleusement créé l'homme
et, plus merveilleusement encore,
rétabli sa dignité,
fais-nous participer à la divinité de ton Fils,
puisqu'il a voulu prendre notre humanité.

Lui qui vit et règne avec toi dans l’unité du Saint-Esprit,
Dieu, pour les siècles des siècles.