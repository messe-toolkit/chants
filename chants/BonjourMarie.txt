BONJOUR MARIE
LE SEIGNEUR EST AVEC TOI
TU PORTERAS
UN ENFANT QUI VIENT DE LUI.

1.
Tu vivais en Galilée
Une vie tranquille
Quand un ange est arrivé
Un matin d’avril.

2.
Tu allais te marier
Sans avoir un sou
À Joseph le charpentier
Qui t’aimait beaucoup.

3.
Et Joseph était si bon
Qu’il a tout compris.
Tu as quitté ta maison
Pour vivre avec lui.

4.
Et l’enfant qui est venu
Quelques mois plus tard
Tu l’as appelé Jésus,
L’enfant de l’espoir.