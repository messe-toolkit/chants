KYRIE ELEISON

Kyrie eleison.
Christe eleison.
Kyrie eleison.

GLORIA

Gloria in excelsis Deo
Et in terra pax hominibus bonae voluntatis.
Laudamus te. Benedicimus te. Adoramus te.
Glorificamus te. Gratias agimus tibi
propter magnam gloriam tuam,
Domine Deus, Rex cælestis,
Deus Pater omnipotens.
Domine Fili unigenite, Jesu Christe.
Domine Deus, Agnus Dei, Filius Patris,
qui tollis peccata mundi, miserere nobis.
qui tollis peccata mundi, suscipe deprecationem nostram ;
qui sedes ad dexteram Patris, miserere nobis.
Quoniam tu solus Sanctus,
tu solus Dominus,
tu solus Altissimus, Jesu Christe.
Cum Sancto Spiritu :
in gloria Dei Patris. Amen.

SANCTUS

Sanctus, Sanctus, Sanctus Dominus, Deus Sabaoth !
Pleni sunt cæli et terra gloria tua. Hosanna in excelsis !
Benedictus qui venit in nomine Domini.
Hosanna in excelsis !

AGNUS DEI

Agnus Dei, qui tollis peccata mundi : miserere nobis.
Agnus Dei, qui tollis peccata mundi : miserere nobis.
Agnus Dei, qui tollis peccata mundi : dona nobis pacem.