R.
Dieu d'amour, Notre Père,
Gloire à toi par Ton Fils Jésus-Christ !
Dieu d'amour, Notre Père,
Ton Esprit dans nos coeurs Te supplie.

1.
Ton Nom est saint, ton Nom de Père,
Qu'il soit chanté par tes enfants !
Béni sois-tu de nous ouvrir à ton Mystère,
Dieu l'Invisible, Dieu dans nos temps !

2.
Que parmi nous ton Règne vienne,
L'amour sera l'unique loi !
Béni sois-tu pour ton Royaume de lumière,
Dieu de justice, Dieu notre Roi !

3.
Que tes désirs se réalisent !
Tu veux sauver tous les vivants.
Béni sois-tu de nous le dire en ton Eglise,
Dieu qui rends libre, Dieu tout-puissant !

4.
Vers toi, Seigneur, nos mains se tendent,
Tu donneras le pain du jour.
Béni sois-tu pour le travail et les semences,
Dieu des promesses, Dieu parmi nous !

5.
Tu es pour nous miséricorde
Viens nous apprendre à pardonner.
Béni sois-tu, Vainqueur du mal au coeur de l'homme,
Dieu notre force, Dieu Vérité !

6.
A toi, Seigneur, nous rendons grâce
Par Jésus Christ, ton Bien-Aimé.
Béni sois-tu de nous former à ton image
Dieu de tendresse, Dieu de bonté !