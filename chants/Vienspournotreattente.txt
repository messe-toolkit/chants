R.
Viens pour notre attente, ne tarde plus.
Pour notre délivrance, viens, Seigneur Jésus !

1.
Dans notre monde de détresse,
Nous t'appelons, Seigneur Jésus.

2.
L'amour, plus fort que nos misères,
Nous réunit, Seigneur Jésus.

3.
Dans notre angoisse, nos ténèbres,
Nous te cherchons, Seigneur Jésus.

4.
Dans nos discordes qui te blessent,
Nous t'implorons, Seigneur Jésus.

5.
Tu es venu chez nous en pauvre
Pour nous sauver, Seigneur Jésus.

6.
Pour avoir fait de nous des frères,
Nous te louons Seigneur Jésus.

7.
Pour l'unité de ton Église,
Nous te prions, Seigneur Jésus.

8.
Nous voulons vivre ta parole,
Unis en toi, Seigneur Jésus.

9.
Ta croix sera notre espérance
Dans notre nuit, Seigneur Jésus.
10.
Un jour enfin naîtra l'aurore,
Nous te verrons, Seigneur Jésus.