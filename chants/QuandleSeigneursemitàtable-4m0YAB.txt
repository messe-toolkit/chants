1.
Quand le Seigneur se mit à table avec ses amis,
Quand le Seigneur se mit à table avec ses amis,
Il leur partagea le pain en leur disant :
Prenez et mangez, ceci est mon corps livré pour vous.
Prenez et mangez, ceci est mon corps livré pour vous.

2.
Quand le Seigneur se mit à table avec ses amis,
Quand le Seigneur se mit à table avec ses amis,
Il leur distribua le vin en leur disant :
Prenez et buvez, ceci est mon sang livré pour vous.
Prenez et buvez, ceci est mon sang livré pour vous.

3.
Nous célébrons ce grand mystère, ta mort sur la Croix.
Nous célébrons ce grand mystère, ta mort sur la Croix.
Nous chantons la joie de ta résurrection.
Et nous attendons le jour de ton retour glorieux.
Et nous attendons le jour de ton retour glorieux.

1979, Studio SM