1.
Marie se hâte
Par les montagnes,
Se murmurant les mots de l’ange.
Dieu dans sa tendresse
A tenu promesse :
Il visite et rachète son peuple.

2.
Arche d’alliance,
Marie s’avance,
Berçant d’amour la loi nouvelle.
Dans son sein repose
La Parole enclose :
Le Messie, le Soleil de justice !

3.
Un grand mystère !
Devenue mère,
Elisabeth chante la grâce.
En elle tressaille
Celui qui témoigne :
Jean déjà reconnaît la lumière.

4.
Tu es heureuse,
Ô bienheureuse,
Tu es bénie entre les femmes !
Ta joie d’âge en âge
Transmet ce message :
« Le Puissant fit pour moi des merveilles ! »