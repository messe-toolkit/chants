MARCHE HUMBLEMENT AVEC TON DIEU (d'après Genèse 12…18)

MARCHE HUMBLEMENT AVEC TON DIEU
PAS APRÈS PAS, IL TE CONDUIT
ENTENDS SA VOIX QUI TE MURMURE
"MOI POUR TOUJOURS, JE SUIS AVEC TOI !"

1.
Abraham a quitté son pays
Il a pris le risque d’une amitié
Force d’une vie inconnue.

2.
Il accueille au secret de son cœur
La bonté de celui qui le bénit,
Son "oui" a ouvert l’avenir.

3.
Cette vie qui fait chanter son âme
À un fils, il lui fallait la donner
C’est "l’enfant du rire" qui est né !

4.
Souveraine liberté d’un homme
Qui connaît la tendresse inépuisable
De celui qui soutient sa vie.