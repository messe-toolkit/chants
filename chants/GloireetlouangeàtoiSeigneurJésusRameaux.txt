GLOIRE ET LOUANGE À TOI, SEIGNEUR, JÉSUS.

Pour nous, le Christ s’est fait obéissant, jusqu’à la mort,
et la mort sur une croix.
Voilà pourquoi Dieu l’a élevé souverainement
et lui a donné le Nom qui est au-dessus de tout nom.