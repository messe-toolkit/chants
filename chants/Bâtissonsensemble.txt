R.
BÂTISSONS ENSEMBLE,
L’ÉGLISE DE DEMAIN.
BÂTISSONS ENSEMBLE,
L’ÉGLISE DE NOS MAINS.

1.
Nous sommes des Pierres vivantes,
Sur nous Dieu peut compter.
Beaucoup sont dans l’attente
D’un vieux monde à changer.

2.
Vers d’autres lendemains,
Il nous faut avancer.
Sur les nouveaux chemins
D’un monde à inventer.

3.
Croisons nos différences
Par l’amour qui unit.
Brisons l’indifférence
Pour vivre en harmonie.