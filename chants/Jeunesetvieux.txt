Jeunes et vieux se réjouiront ensemble ;
les jeunes filles danseront de joie !
Laï, laï, laï, laï…
Je changerai leur deuil en allégresse
et je les consolerai.
Je leur donnerai la joie au lieu du chagrin,
je leur donnerai la joie !
Je leur donnerai la joie au lieu du chagrin,
je leur donnerai la joie !