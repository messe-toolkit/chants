1.
Venons au jour,
Le jour où Dieu rompt le silence.
Il nous invite au rendez-vous,
Au rendez-vous de sa présence.
N'hésitons pas à nous lever,
A quitter nos sécurités,
Pour vivre au rythme de son coeur.
Pour vivre au rythme de son coeur.

2.
Venons au jour,
Le jour où Dieu tient sa promesse.
Il nous invite à l'accueillir,
A l'accueillir avec tendresse.
N'hésitons pas à nous lever,
A courir vers le Bien-aimé
pour vivre au rythme de son coeur. (bis)

3.
Venons au jour,
Le jour où Dieu dit son mystère.
Il nous invite à nous ouvrir,
A nous ouvrir et à nous taire.
N'hésitons pas à nous lever,
A écouter Dieu nous parler
Pour vivre au rythme de son coeur. (bis)

4.
Venons au jour,
Le jour où Dieu se donne à l'homme.
Il nous invite à nous donner,
A nous donner comme il se donne.
N'hésitons pas à nous lever,
A aimer jusqu'à tout donner
Pour vivre au rythme de son coeur. (bis)

5.
Venons au jour,
Le jour où Dieu roule la pierre.
Il nous invite à nous dresser,
A nous dresser de la poussière.
n'hésitons pas à nous lever,
A nous tenir ressuscités,
Pour vivre au rythme de son coeur. (bis)