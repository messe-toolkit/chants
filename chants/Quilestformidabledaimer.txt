R.
QU’IL EST FORMIDABLE D’AIMER,
QUV’IL EST FORMIDABLE,
QU’IL EST FORMIDABLE D’AIMER,
QU’IL EST FORMIDABLE DE TOUT
DONNER POUR AIMER !

1.
Quand on n’a que ses mains,
A tendre ou à donner,
Quand on n’a que ses yeux
Pour rire et pour pleurer;
Quand on n’a que sa voix
Pour crier et chanter,
Quand on n’a que sa vie
Et qu’on veut la donner !

2.
Quand on n’a que sa peine,
A dire ou à cacher,
Quand on n’a que ses joies,
A taire, ou à partager;
Quand on n’a que ses rêves,
A faire voyager,
Quand on n‘a que sa vie,
Et qu’on veut la donner !

3.
Quand le pain et le vin,
Ont goût de vérité,
Quand il y a sur la route,
Ce pas qu’on reconnaît;
Quand on regarde ensemble,
Vers le même sommet,
Quand on n’a que sa vie,
Et qu’on veut la donner !

4.
Quand il y a sa présence,
Pour vivre et espérer,
Quand les chemins du risque,
S’appelle Vérité;
Quand les quatre horizons,
Conduisent vers la paix,
Quand on n’a que sa vie,
Et qu’on veut la donner !