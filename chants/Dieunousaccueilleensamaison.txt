R.
Dieu nous accueille en sa maison,
Dieu nous invite à son festin :
Jour d'allégresse et jour de joie !
Alléluia !

1.
Oh, quelle joie quand on m'a dit :
approchons-nous de sa maison
Dans la cité du Dieu vivant !

2.
Jérusalem, réjouis-toi,
car le Seigneur est avec toi :
Pour ton bonheur, il t’a choisie !

3.
Criez de joie pour notre Dieu,
chantez pour lui, car il est bon,
car éternel est son amour !

4.
Avec Jésus, nous étions morts ;
avec Jésus, nous revivons,
Nous avons part à sa clarté.

5.
Approchons-nous de ce repas
où Dieu convie tous ses enfants,
Mangeons le Pain qui donne vie.

6.
« Si tu savais le Don de Dieu »,
si tu croyais en son amour,
Tu n’aurais plus de peur en toi.

7.
Que Jésus-Christ nous garde tous
dans l’unité d’un même Corps,
Nous qui mangeons le même Pain.

8.
Soyons témoins de son Esprit !
Que disparaisse toute peur !
Montrons au monde notre foi !