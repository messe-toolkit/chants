1.
Ô nuit, de quel éclat tu resplendis :
La mort n'a pu garder dans son étreinte le Fils unique :
Jésus repousse l'ombre et sort vainqueur :
Christ est ressuscité !
Mais c'est en secret et Dieu seul connaît
L'instant où triomphe la vie.

2.
Quelqu'un près de la croix n'a pas douté :
La femme jusqu'au jour a porté seule l'espoir du monde,
Sa foi devance l'heure et sait déjà :
Christ est ressuscité !
Mais c'est en secret et Dieu seul connaît
La joie dont tressaille Marie.

3.
Jésus, lumière et vie, demeure en nous :
Pourquoi chercher encore au tombeau vide un autre signe ?
L'amour jaillit et chante au fond du cœur :
Christ est ressuscité !
Mais c'est en secret et Dieu seul connaît
Le feu qui s'éveille aujourd'hui.