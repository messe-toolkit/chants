R.
Alléluia ! Alléluia !
Alléluia ! Alléluia !

1.
Mon âme chante le Seigneur, Alléluia !
Mon âme chante le Seigneur, Alléluia !
Et dans mon cœur, il n´est que joie, Alléluia !
Et dans mon cœur, il n´est que joie, Alléluia !

2.
Il a jeté les yeux sur moi, Alléluia !
Il a jeté les yeux sur moi, Alléluia !
En moi son nom sera béni, Alléluia !
En moi son nom sera béni, Alléluia !

3.
De ses merveilles il m´a comblée, Alléluia !
De ses merveilles il m´a comblée, Alléluia !
Saint est son nom dans tous les temps, Alléluia !
Saint est son nom dans tous les temps, Alléluia !

4.
Il fait tomber les orgueilleux, Alléluia !
Il fait tomber les orgueilleux, Alléluia !
Mais il relève les petits, Alléluia !
Mais il relève les petits, Alléluia !

5.
Il rassasie les affamés, Alléluia !
Il rassasie les affamés, Alléluia !
Mais il renvoie les suffisants, Alléluia !
Mais il renvoie les suffisants, Alléluia !

6.
Il vient en aide à Israël, Alléluia !
Il vient en aide à Israël, Alléluia !
Il se souvient de son amour, Alléluia !
Il se souvient de son amour, Alléluia !

7.
Envers son peuple bien aimé, Alléluia !
Envers son peuple bien aimé, Alléluia !
Il tient promesse pour toujours, Alléluia !
Il tient promesse pour toujours, Alléluia !

8.
Gloire et louange à notre Dieu, Alléluia !
Gloire et louange à notre Dieu, Alléluia !
Gloire à Jésus, au Saint-Esprit, Alléluia !
Gloire à Jésus, au Saint-Esprit, Alléluia !