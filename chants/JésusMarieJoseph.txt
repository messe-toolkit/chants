JÉSUS, MARIE, JOSEPH,
EN VOUS LA JOIE BRILLE,
RÉVEILLEZ EN NOUS LE FEU,
TRÈS SAINTE FAMILLE.

1.
Votre vie nous dit
Un si beau poème,
Que nos coeurs s’écrient :
« Voyez comme ils s’aiment ».

2.
En votre maison,
Pas la moindre haine.
Nous proclamerons :
« Voyez comme ils s’aiment ».

3.
À parler de vous,
La paix nous entraîne.
Nous dirons partout :
« Voyez comme ils s’aiment ».

4.
À vous regarder,
S’adoucit la peine.
Nous irons chanter :
« Voyez comme ils s’aiment ».