1.
Verbe Emmanuel, monté près du Père,
O Ressuscité,
Tu es glorifié !
Heureux qui prend part à ta joie !

2.
Tu es parmi nous, Seigneur de la Pâque,
Tes bras étendus
Offrent le salut !
Heureux qui accueille ta croix !

3.
Tu répands sur nous l’Esprit de l’Alliance,
L’Esprit de bonté,
Ferment d’unité !
Heureux qui se livre à ta loi !

4.
Ton Corps et ton Sang façonnent ton peuple,
Tu fais de nos vies
Une eucharistie !
Heureux qui rend grâce avec toi !

5.
Tu es le Vivant, Jésus, Fils de l’homme,
Proche est ton retour,
Advienne ton Jour !
Heureux qui s’avance vers toi !