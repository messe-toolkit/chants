Stance :
Le Verbe ouvre le livre.
Aujourd'hui s'accomplit la parole de grâce.
Parmi nous, Jésus passe.
Allez avec lui vers le large.
Tous les pays connaîtront la nouvelle du pardon.

Heureux qui suit le Seigneur !
Heureux les Fils du Très-Haut !

3e dimanche C :
Dans le livre est écrit pour moi
Ce que tu veux que je fasse !

Tu ne voulais ni offrande ni sacrifice,
Alors j'ai dit : “Voici, je viens”.

J'ai dit ton amour et ta vérité
Devant la grande assemblée.

Tu seras l'allégresse et la joie
De tous ceux qui te cherchent.

4e dimanche C :
Rendez au Seigneur, familles des peuples,
Rendez au Seigneur la gloire et la puissance,
Rendez au Seigneur la gloire de son nom.

Que les peuples, Dieu, te rendent grâce :
Qu'ils te rendent grâce tous ensemble.
Que les nations chantent leur joie !

Toutes les nations que tu as faites
Viendront se prosterner devant toi,
Rendre gloire à ton nom, Seigneur !

5e dimanche C :
Il est droit, il est bon, le Seigneur,
Il montre aux pécheurs le chemin.

Tout ce que veut le Seigneur, il le fait,
Au ciel, sur terre et dans les mers.

Pour toujours, le nom du Seigneur !
D'âge en âge, son mémorial !

Vous direz aux âges qui viendront :
“Ce Dieu est notre Dieu,
Notre guide pour les siècles.”

6e dimanche C :
Qui sème dans les larmes,
Moissonne dans la joie !

Les pauvres mangeront, ils seront rassasiés,
À vous, toujours, la joie !

Exultez, hommes justes !
Que le Seigneur soit votre joie !

Voici le jour que fit le Seigneur,
Jour de fête et de joie !

7e dimanche C :
Le Seigneur est tendresse et pitié,
Lent à la colère et plein d'amour.

Le Seigneur n'agit pas envers nous selon nos fautes,
Ne nous rend pas selon nos offenses.

La bonté du Seigneur est pour tous,
Sa tendresse pour toutes ses œuvres.

Les lèvres du juste redisent la sagesse,
La loi de son Dieu est dans son cœur.

8e dimanche C :
Ton amour, Seigneur, emplit la terre ;
Apprends-moi tes commandements.

Justice éternelle, tes exigences,
Éclaire-moi et je vivrai.

Apprends-moi à bien saisir, à bien juger :
Je me fie à tes volontés.

Je cours dans la voie de tes volontés,
Car tu mets au large mon cœur.