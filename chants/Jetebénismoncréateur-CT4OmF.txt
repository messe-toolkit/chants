R.
Je te bénis, mon Créateur,
Pour la merveille que je suis.
Tous ces trésors, au fond de moi,
Que tu as mis sans faire de bruit.

1.
Tes yeux me voient dès le matin
Et jusqu’au soir, sans me quitter.
Tu m’as tissé et façonné
Dans le silence et la patience.

2.
Tu me connais, ô mon Seigneur,
Dans mes pensées et dans mon coeur.
Sur mes épaules, tu mets ta main,
Quand je me couche et quand je dors.

3.
Où vais-je aller, loin de ta face?
De toutes parts, tu es présent.
Quand je m’assieds, quand je me lève,
Tu es fidèle à mes côtés.