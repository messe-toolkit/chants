R.
Chez toi, Seigneur, tu nous invites,
Heureux qui vient te rencontrer !
Auprès de toi nous voulons vivre,
Tu nous as dit : «Venez, voyez !»

1.
Tu as le ciel pour vraie demeure,
Toi qui nous montres l’horizon.
Jésus présent parmi ton peuple,
Agneau de Dieu, nous te suivons.

2.
Dis-nous la joie d’une parole,
Maître de vie, silence et paix.
Dans ton regard le jour se donne,
Agneau de Dieu, tu nous connais.

3.
Que verrons-nous de ton Royaume ?
Tu le révèles à tes amis.
Nous te croyons, Sauveur des hommes,
Agneau de Dieu, Berger promis.