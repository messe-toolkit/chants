R.
CHRIST EST VENU SUR NOTRE TERRE
CHRIST EST VENU NOUS VISITER.
CHRIST EST VENU CHEZ NOUS, MES FRÈRES
CHRIST EST VENU NOUS INVITER.

1.
Il nous a parlé de Lumière,
Il nous a parlé de moisson, de feu,
d’oiseaux et de poissons.
Il nous a parlé de son Père.

2.
Il nous a parlé d’une Vie,
Il nous a parlé d’un chemin
de vérité, et du prochain.
Il nous a parlé de folie.

3.
Il nous a donné son Amour.
Il nous a donné sa passion,
Sa mère et toutes les nations.
Il nous a redonné le jour.