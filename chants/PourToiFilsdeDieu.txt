1.
Pour toi, Fils de Dieu,
Le nard de grand prix,
La vie donnée sans jamais la reprendre ;
Pour toi la louange
De ta servante.

2.
Vers toi, Jésus-Christ,
L'écoute du coeur,
Ton nom crié sans briser le silence ;
Vers toi la violence
De l'espérance.

3.
Par toi, Serviteur,
La force d'aimer,
La longue marche au désert de l'absence ;
Par toi la descente
Dans la souffrance.

4.
En toi, Bien-aimé,
La paix du désir,
La joie parfaite que nul ne peut prendre :
Ta vie en offrande
Pour ta servante.