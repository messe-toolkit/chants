DANS LA JOIE, NOUS IRONS
À LA MAISON DU SEIGNEUR.

1.
Quelle joie quand on m’a dit :
« Nous irons à la maison du Seigneur ! »
Maintenant notre marche prend fin
devant tes portes, Jérusalem !

2.
Jérusalem, te voici dans tes murs :
ville où tout ensemble ne fait qu’un !
C’est là que montent les tribus, les tribus du Seigneur,
là qu’Israël doit rendre grâce au nom du Seigneur.

3.
C’est là le siège du droit,
le siège de la maison de David.
Appelez le bonheur sur Jérusalem :
« Paix à ceux qui t’aiment ! »