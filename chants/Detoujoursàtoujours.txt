1.
Avant de plisser les montagnes,
Avant de tapisser les cieux,
Avant d’étaler la campagne,
Avant de peindre l’océan de bleu,
Tu suis l’avenir à la trace,
Mille ans à tes yeux sont un jour
Mon Dieu, mon Dieu que le temps passe !
Mes jours sont de plus en plus courts.

MAIS TOI, OUI TOI
DE TOUJOURS À TOUJOURS
ET POUR TOUJOURS TU ES DIEU.

2.
Je suis comme une herbe sauvage
Qui au matin pousse et fleurit
À midi étend ses branchages
Au soir n’est plus que du foin gris
Mon temps est toujours en faillite
Et courre après le jour qui s’enfuit
Pour moi ce jour passe plus vite
Qu’un de mes rêves dans la nuit.

3.
Mais qu’est-ce que c’est donc qu’un grand âge ?
Soixante ou quatre-vingt-dix ans
Ce n’est rien qu’un tout p’tit voyage
Une goutte d’eau dans l’océan
Et toi, mon Dieu tu fais nos comptes
Tous nos secrets sont devant toi
Pourtant tu ignores notre honte
Car toi, c’est en nous que tu crois.

4.
Comment faut-il compter mon âge
Dans ma prison de souvenirs
Je traîne avec moi mes bagages
Ma vie s’en va comme un soupir
Toi, tu entres dans ta jeunesse
Toi, tu changes la peur en soleil
Au grand banquet de ta tendresse
Je n’aurai plus jamais sommeil.