NOUS SOMMES LES ENFANTS DU MEME PERE,
RIEN NE PEUT NOUS SEPARER DE CELUI QUI NOUS UNIT.
ET POUR L'ETERNITE DE NOTRE TERRE,
DIEU NOUS DONNE EN JESUS-CHRIST SON ESPRIT A L'INFINI.

1.
L'Esprit réveille la tendresse,
A chaque automne de nos vies.
Chemin tout droit ou de traverse
Vers l'essentiel il nous conduit.

2.
L'Esprit nous ouvre des fenêtres
le long des murs de nos hivers.
Et pour demain encore à naître,
il est soleil dans l'univers.

3.
L'Esprit nous force à l'espérance,
car le printemps revient toujours.
Les coeurs fermés par la souffrance
en lui fleurissent au grand jour.

4.
L'Esprit choisit de nous surprendre
comme un orage en plein été.
Il est la braise sous la cendre
qui brûle en toute liberté.