TOI QUI ES, QUI ÉTAIS ET VIENS DANS LE SILENCE,
ET NUL NE SAIT OÙ VA TA PAROLE DE FEU,
APAISE NOS TUMULTES, GUÉRIS NOS VIOLENCES,
ET FAIS TAIRE NOS CRIS EN PRÉSENCE DE DIEU.

1.
Dans la nuit où le monde attend son espérance,
Tu fais surgir soudain dans la flamme et le vent,
Nouvelle créature et nouvelle naissance :
Les enfants engendrés par ton souffle brûlant.

2.
Par delà l'univers de signes et d'apparence,
Découvre-nous la face d'un monde nouveau,
Mais qui gémit encore d'ineffable impuissance,
Dans l'attente du nom que nous donne l'Agneau.

3.
Fais jaillir de nos coeurs le cri de ta présence,
Ravive jour et nuit la prière des fils,
Et chante en nous la joie de la nouvelle enfance,
Qu'illumine déjà le visage du Christ.

4.
Jusqu'au jour où le Fils de Dieu, dans sa puissance,
Viendra enfin juger les vivants et les morts,
Fontaine jaillissante et torrent qui s'élance,
Ressuscite une source endormie en nos corps !