À toi nous rendons grâce,
Seigneur Dieu de l’univers,
toi qui es, toi qui étais.

Tu as saisi ta grande puissance
et pris possession de ton règne.
Les peuples s’étaient mis en colère,
alors ta colère est venue
et le temps du jugement pour les morts.
Le temps de récompenser tes serviteurs,
les saints, les prophètes,
ceux qui craignent ton nom,
les petits et les grands.

Maintenant, voici le salut et le règne
et la puissance de notre Dieu,
voici le pouvoir de son Christ.
L’accusateur de nos frères est rejeté,
lui qui les accusait jour et nuit
devant notre Dieu.

Ils l’ont vaincu par le sang de l’Agneau,
par la parole dont ils furent les témoins :
renonçant à l’amour d’eux-mêmes
jusqu’à mourir.
Soyez donc dans la joie,
cieux, et vous, habitants des cieux.