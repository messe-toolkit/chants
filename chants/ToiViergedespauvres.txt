1.
Toi, vierge des pauvres,
Mère à chaque instant,
Tu nous conduis vers ton fils Jésus,
Lui notre salut,
Tu nous apprends l’accueil de l’Esprit,
Marie, en nos vies.

FEMME COMBLÉE DE GRÂCE,
HUMBLE ET TRÈS PURE MARIE,
VOIS TES ENFANTS VENUS TE PRIER,
EN DIEU, PLONGER, SE LAVER DU MAL,
ÊTRE LIBÉRÉS ET VIVRE DANS L’AMITIÉ.
EN DIEU, SE TRANSFORMER,
CHASSER L’ORGUEIL, LE PÉCHÉ
ET SUIVRE TA SAINTETÉ.

2.
Jésus, je t’appelle,
Entends ma prière.
Je te remets toutes mes pauvretés
En Vérité
Élève-moi par ta charité,
Ta fidélité.

3.
Seigneur de la Paix,
Père de l’Univers,
Que vienne enfin ton règne sur Terre
Entre les Frères.
Peuple de foi, nous marchons vers toi,
Dieu notre Joie.