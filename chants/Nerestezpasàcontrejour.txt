1.
Ne restez pas à contre jour:
Déjà brille la moisson.
Le blé nouveau blanchit.
Les temps sont courts.

2.
Ne ramez plus à contre vent:
Déjà vibre à l'horizon
Le souffle de l'Esprit.
Il vous conduit.

3.
Ne marchez plus à contre temps:
Déjà passe la saison
Des feuilles et des nids.
Portez du fruit.

4.
Ne vivez plus à contre coeur:
Déjà s'ouvre la maison
Du Père pour les fils.
Allez sans peur .