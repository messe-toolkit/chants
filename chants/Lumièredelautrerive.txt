R.
Seigneur qui nous fais signe,
Révèle-nous l’amour donné.

1.
Proche est la Pâque où Dieu nous dit :
Fini le temps des sèves mortes,
Voici le pain qui donne vie.
Mais nul convive n’est rassasié.
Tant qu’il manque à ce pain donné
La lumière de l’autre rive.

2.
Quel est celui qui nous convie
Dans le printemps des herbes vives ?
Cet homme est-il vrai pain du ciel ?
Nulle autre table n’est préparée,
Tant que l’homme est cet affamé
Aux ténèbres de notre rive.

3.
L’œuvre de Dieu dans nos déserts,
Voici le pain qu’il nous faut croire,
Un pain vivant comme sa chair.
Mais nul convive n’est rassasié,
Tant qu’il manque à ce corps livré
La lumière de l’autre rive.

4.
Qui peut mendier ce pain,
Si Dieu le Père ne l’attire,
Et ne l’éveille à ce festin ?
Mais nul convive n’est rassasié,
S’il n’espère un nouveau banquet
Aux demeures de l’autre rive.

5.
Vienne la Pâque de l’Esprit,
Le seul printemps qui fait revivre
Bien au-delà des jours de chair !
Il n’est convive ressuscité,
S’il ne passe comme invité
À la table de l’autre rive.