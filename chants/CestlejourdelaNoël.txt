1.
C’est le jour de la Noël que Jésus est né (bis)
Il est né dedans un coin, dessus la paille.
Il est né dedans un coin, dessus le foin.

2.
Saint-Joseph de son chapeau lui fit un berceau (bis)
Il coucha l’enfant si doux dans sa casaque,
Il coucha l’enfant si doux sur ses genoux.

3.
Le boeuf roux et l’âne gris veillaient le petit (bis)
Ils soufflaient bien doucement sur son visage
Ils soufflaient bien doucement dessus l’enfant.

4.
Mais à l’âge de quinze ans, quand il sera grand, (bis)
Il apprendra le métier de la boutique,
Il apprendra le métier de charpentier.

5.
Et pour la première fois fera une croix (bis)
Qui un jour le conduira jusqu’au supplice,
Qui un jour le conduira jusqu’au trépas.