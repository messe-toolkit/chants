R.
Ô mon peuple ! Que t'ai-je fait ?
En quoi t'ai-je contristé ? Réponds-moi.

1.
Moi, j’ai pour toi frappé l’Égypte,
J’ai fait mourir ses premiers-nés ;
Toi, tu m’as livré, flagellé !

2.
Moi, je t’ai fait sortir d’Égypte,
J’ai englouti le Pharaon,
Je l’ai noyé dans la Mer Rouge ;
Toi, tu m’as livré aux grands prêtres !

3.
Moi, devant toi, j’ouvris la Mer,
Toi, tu m’as ouvert de ta lance !

4.
Moi, devant toi je m’avançai
Dans la colonne de nuée ;
Toi, tu m’as conduit à Pilate !

5.
Moi, j’ai veillé dans le désert
Et de la manne t’ai nourri :
Toi, tu m’as frappé, flagellé !

6.
Moi, aux eaux vives du rocher,
Je t’ai fait boire le salut :
Tu me fis boire le fiel,
Tu m’abreuvas de vinaigre !

7.
Moi, j’ai pour toi frappé les rois,
Les puissants rois de Canaan :
Toi, tu m’as frappé d’un roseau !

8.
Moi, dans ta main j’ai mis le sceptre,
Je t’ai promu peuple Royal :
Toi, tu as placé sur ma tête
La couronne d’épines !

9.
Moi, je t’ai exalté
par ma toute puissance
Toi, tu m’as pendu au gibet de la Croix !
