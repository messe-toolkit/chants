Refrain :
Partons tout aussitôt
Pour un pays nouveau,
ts la montagne !
Chantons le grand Hallel,
C'est lui qui nous appelle :
“Nous voici, Seigneur !”

1.
Puisqu'il nous a choisis, et puisqu'on a dit “oui”, ne faisons pas attendre,
Nous n'emporterons rien, sans peur du lendemain, c'est lui qui nous conduit ;
Debout et déjà prêt, les sandales aux pieds, le Peuple peut comprendre
Le sens de son destin et le pain sans levain, puisqu'il nous a choisis.

2.
Puisqu'il est avec nous et puisque habite en nous le feu de sa présence,
La flamme est au buisson pour révéler son Nom, il a besoin de nous ;
Et sans se consumer, ce pacte d'amitié fait le Peuple d'Alliance,
C'est lui qui nous envoie, il est le feu de joie, puisqu'il est avec nous.

3.
Puisqu'il est le chemin et puisqu'il ouvre enfin la brèche du passage,
La mer s'est asséchée, l'Exode est commencé, et nous sommes sauvés.
Au désert purifié, “le petit reste” est né, “le Peuple du courage”,
Il met sa liberté et sa force d'aimer, puisqu'il est le chemin.

4.
Puisqu'il nous a nommés et puisqu'il nous connaît, nous deviendrons nous-mêmes,
Sa loi nous est donnée, c'est l'appel à aimer, l'appel à le chercher,
Présent au monde entier et ferment de la paix, le peuple du baptême,
Le sera à jamais, l'avenir est gagné, puisqu'il nous a nommés.