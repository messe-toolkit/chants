R.
GLOIRE À TOI QUI NOUS APPELLES,
JÉSUS DE NAZARETH!
GLOIRE À TOI QUI TE RÉVÈLES,
JÉSUS, FILS DE DIEU !

1.
Maître sur nos chemins, où demeures-tu?
Parole du Dieu saint, où nous mènes-tu?
Guide-nous vers ton secret, Toi qui dis :
“Venez vers le Royaume, voyez le Fils de l’Homme!”

2.
Maître qui parles au coeur, où demeures-tu?
Amour du Dieu sauveur, où nous mènes-tu?
Guide-nous vers ton secret, Toi qui dis :
“Venez puiser l’Eau Vive, voyez qui vous invite !”

3.
Maître qui nous conduis, où demeures-tu?
Lumière dans la nuit, où nous mènes-tu?
Guide-nous vers ton secret, Toi qui dis :
“Venez loin des impasses, croyez au jour de Pâques !”

4.
Maître ressuscité, où demeures-tu?
Présence au plus caché, où nous mènes-tu?
Guide-nous vers ton secret, Toi qui dis :
“Venez pour faire Église, vivez de l’Évangile !”

5.
Maître qui dois venir, où demeures-tu?
Soleil de l’avenir, où nous mènes-tu?
Guide-nous vers ton secret, Toi qui dis :
“Venez sauver la terre, soyez des peuples frères !”