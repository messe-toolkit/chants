R.
Je veux chanter ton amour, Seigneur,
Chaque instant de ma vie.
Danser pour toi en chantant ma joie
Et glorifier ton Nom.

1.
Ton amour pour nous
Est plus fort que tout
Et tu veux nous donner la vie,
Nous embraser par ton Esprit.
Gloire à toi !

2.
Oui, tu es mon Dieu,
Tu es mon Seigneur.
Toi seul es mon libérateur,
Le rocher sur qui je m´appuie.
Gloire à toi !

3.
Car tu es fidèle,
Tu es toujours là,
Tout près de tous ceux qui te cherchent,
Tu réponds à ceux qui t´appellent.
Gloire à toi !

4.
Voici que tu viens
Au milieu de nous,
Demeurer au cœur de nos vies
Pour nous mener droit vers le Père.
Gloire à toi !

5.
Avec toi, Seigneur
Je n´ai peur de rien.
Tu es là sur tous mes chemins.
Tu m´apprends à vivre l´amour.
Gloire à toi !