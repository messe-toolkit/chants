R.
Nous te rendons grâce
Pour tant de tendresse,
Tu donnes l’eau vive
Par ton cœur transpercé,
Nous te bénissons
Pour tant de merveilles,
Tu donnes la vie, tu donnes l’Esprit.

1.
Dieu, c’est toi mon Dieu, c’est toi que je cherche,
Toute ma chair après toi languit.
Je veux ton amour pour guider ma vie,
Mon âme a soif, a soif de toi.

2.
Mes lèvres diront sans fin ton éloge,
Toute ma vie, je veux te bénir.
Je veux à ton Nom élever les mains,
Mon âme a soif, a soif de toi.

3.
Quand je songe à toi, quand j’espère en toi,
Quand je t’appelle toujours tu réponds.
Alors je jubile, en paix sous tes ailes,
Mon âme a soif, a soif de toi.

4.
Et quand je te cherche, tu te laisses trouver,
Rassasie-moi de ta présence.
Je suis une terre altérée, sans eau,
Mon âme a soif, a soif de toi.