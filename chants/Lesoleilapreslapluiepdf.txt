1.
Le soleil après la pluie
Nous apporte sa clarté.
Viens toi-même en notre nuit
Dissiper l’obscurité !
Viens, ESPRIT de pauvreté ! bis

2.
Quand JÉSUS, près du Jourdain,
Vit se déchirer les cieux, (Mc 1, 10)
La colombe vint soudain
Désigner le FILS de Dieu ! (Mc 1, 11)
Viens, ESPRIT de sainteté ! bis

3.
Dieu que nul n’a jamais vu,
Tant nos yeux sont limités ! (Jn.1, 18)
Fais-nous trouver en JÉSUS
Un reflet de ta beauté ! (Col.1, 15)
Viens, ESPRIT de pureté ! bis

4.
Toi, la Source qui jaillit
Du côté du CHRIST en croix, (Jn.19, 34)
Viens combler la soif de Vie
Dans le coeur de ceux qui croient !
Viens, ESPRIT des sanctifiés ! bis

5.
Comme un vent impétueux
Sur l’Église en son berceau, (Act.2, 2)
Rends ton peuple généreux
Pour bâtir les cieux nouveaux ! (2 Pi.3, 13)
Viens, ESPRIT de charité ! bis

6.
Comme un vent dont on ne sait
Où il va ni d’où il vient (Jn.3, 8)
Sous ton Souffle l’on renaît
Pour marcher sur le Chemin ! (Jn.14, 4)
Viens, ESPRIT de liberté ! bis

7.
Quel est ce langage unique
Pour nations dessous le ciel (Act.2, 5)
Sinon celui, prophétique,
D’un amour tout fraternel ? (Jean 13, 3)
Viens, ESPRIT (de) fraternité ! bis

8.
Dieu qui veux renouveler
Le vieux monde par l’ESPRIT (Rom.8, 19)
Oh ! Reviens nous rappeler
Tout ce que JÉSUS a dit ! (Jn.14, 24)
Viens, ESPRIT de vérité ! bis

9.
Père, accueille les croyants :
Façonnés par ton ESPRIT,
Qu’ils deviennent tes enfants
Dans le Corps de JÉSUS CHRIST !
Viens, ESPRIT de l’unité ! bis

10.
Gloire à toi, Dieu notre PÈRE,
Par JÉSUS, ton bien-aimé ! (Mc.1, 11)
Que l’Esprit nous rende frères
En faisant ta volonté ! (Mc.3, 35)
Viens, ESPRIT d’éternité ! bis