1.
Il est des hommes, Seigneur Dieu, que tu combles à leur naissance,
Et prépares dès l'enfance à recréer ce qui est vieux.

2.
Il est des êtres en qui soudain tu concentres ta lumière :
Ils rayonnent sur leurs frères et leur inventent le chemin.

3.
Près de Bernard on accourait, pour creuser dans le silence et la patience
Jusqu'à la source de la paix.

4.
Tout son désir fut de rester au secret de ton visage,
Dans l'écoute et le partage de la Parole méditée.

5.
Mais à ses frères trop souvent tu arraches ce prophète,
Il sera dans les tempêtes une vigie pour son temps.

6.
Toujours ardente, la passion qui pour toi brûlait cet homme,
Fait briller jusqu'au Royaume l'obscur chemin que nous suivons.