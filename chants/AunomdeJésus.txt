Au nom de Jésus,
Tout être vivant,
Fléchi le genou
Dans le ciel sur la terre et aux abîmes !
À la gloire de Dieu,
À la gloire du Père,
Toute langue proclame :
Jésus-Christ est Seigneur !

R.
Louange à Dieu !
Jésus-Christ est Seigneur !

1.
Tu es beau comme aucun des enfants de l’homme,
La grâce est répandue sur tes lèvres.

2.
Ton honneur, c’est de courir au combat,
Pour la justice, la clémence et la vérité.

3.
Ton sceptre royal est sceptre de droiture ;
Tu aimes la justice, tu réprouves le mal.