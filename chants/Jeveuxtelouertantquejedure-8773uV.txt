R.
Je veux te louer,
Tant que je dure, tant que je vis.
Chaque instant de ma vie,
Élever les mains à ton Nom.

1.
Rendez grâce au Seigneur !
Que ma joie explose en une louange.
Bénissez à jamais son Saint Nom,
Et mon chant, c´est son amour.

2.
Dans la joie et la tristesse,
Que mon cœur demeure dans ta louange.
En tout temps, en tout lieu, je dirai :
Que ton Nom est grand, Seigneur.