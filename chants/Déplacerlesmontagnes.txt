R.
JÉSUS A FAIM, IL VEUT DÉJÀ
CUEILLIR LES FRUITS DE NOS VIES
AYONS LA FOI, AYONS LE COEUR.
À DÉPLACER LES MONTAGNES

1.
Figuier desséché
C’est sans retour
La sève a quitté
L’arbre sans amour
Mais Jésus est venu
Il est ressuscité
Ceux qui ont entendu
Doivent l’annoncer.

2.
Les vendeurs du temple
Ont été chassés
Dieu, on le contemple
Sans faire de marché
Son Amour est gratuit
Son Amour est puissant
Il transforme la nuit
En soleil levant.

3.
Lorsque nous prions
Le coeur ouvert
C’est dans l’abandon
Que vient la lumière
Jésus entend nos cris
Sans faire le magicien
Il entre dans nos vies
Par l’Esprit Saint.

4.
Ceux qui croient savoir
N’ont pas de désir
C’est comme le pouvoir
Il peut affadir
Jésus est le Messie
Il vient nous libérer
Même les coeurs endurcis
Peuvent changer.