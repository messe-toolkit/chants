ANTIENNE
Seigneur, tu as vaincu la mort.
Seigneur tu as fait briller la vie,
Pour l'éternité.

1.
Le Christ est ressuscité d'entre les morts,
Prémices de ceux qui se sont endormis.
La mort est vaincue par la vie.

2.
Le Christ est ressuscité d'entre les morts !
La mort a été engloutie par la Vie !
Ô mort, où donc est ta victoire ?

3.
Le Christ est ressuscité d'entre les morts !
Au Dieu du salut rendons grâce à jamais
Par notre Seigneur Jésus-Christ.