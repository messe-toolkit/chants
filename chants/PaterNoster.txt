Pater noster, qui es in caelis :
sanctificétur nomen tuum ;
advéniat regnum tuum ;
fiat volúntas tua,
sicut in caelo, et in terra.
Panem nostrum cotidiánum da nobis hódie ;
et dimítte nobis débita nostra,
sicut et nos dimíttimus debitóribus nostris ;
et ne nos indúcas in tentatiónem ;
sed líbera nos a malo.