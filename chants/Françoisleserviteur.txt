FRANÇOIS LE SERVITEUR,
COMME TON SEIGNEUR, TU AS CHOISI LA PAUVRETÉ.
COMME LUI TU T'ES FAIT LE PLUS PETIT
POUR ÊTRE AU MILIEU DE TES FRÈRES,
FRÈRE DE TOUS ET RICHE DE LA JOIE,
DE LA JOIE DE DIEU !
FRÈRE DE TOUS ET RICHE DE LA JOIE,
DE LA JOIE DE DIEU !

1.
Tu ne pouvais souffrir la vue des lépreux,
Et pourtant un jour sur la route
Tu embrassas sans qu'il t'en coûte
L'un de ces exclus dans l'amitié de Dieu.

2.
Tu es allé à la rencontre, sans peur,
Du féroce loup solitaire
Et tu l'as appelé « mon frère » ;
Alors sont tombés les murs de la terreur.

3.
Tu espérais la conversion du sultan,
L'ennemi sans foi, l'infidèle.
Tu découvris qu'il se révèle
Un homme sincère, un être désarmant.

4.
Sans coeur, des frères avaient chassé des brigands,
Sur ton ordre ils les retrouvèrent.
Tu les tiras de leur misère,
D'abord de la faim, puis de leurs errements.

5.
Tu accueillais les bien portants, les souffrants,
Les nantis et les misérables,
Les innocents et les coupables.
À tous tu disais l'amour du Tout-Puissant.