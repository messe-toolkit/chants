LE FLEUVE, SES BRAS RÉJOUISSENT LA VILLE DE DIEU,
LA PLUS SAINTE DES DEMEURES DU TRÈS-HAUT.

1.
Dieu est pour nous refuge et force,
secours dans la détresse, toujours offert.
Nous serons sans crainte si la terre est secouée,
si les montagnes s’effondrent au creux de la mer.

2.
Le Fleuve, ses bras réjouissent la ville de Dieu,
la plus sainte des demeures du Très-Haut.
Dieu s’y tient : elle est inébranlable ;
quand renaît le matin, Dieu la secourt.

3.
Il est avec nous, le Seigneur de l’univers ;
citadelle pour nous, le Dieu de Jacob !
Venez et voyez les actes du Seigneur,
il détruit la guerre jusqu’au bout du monde.