R.
Voici votre Dieu qui a tout donné,
Voici votre Dieu, le crucifié.
Voici votre Dieu qui nous a aimés,
Voici votre Dieu qui nous a sauvés.

1.
Objet de mépris,
Abandonné des hommes,
Homme de douleur,
Familier de la souffrance.
Or ce sont nos souffrances
Sur le bois qu'il portait
Et nos douleurs
Dont il était chargé.

2.
Il a été transpercé
À cause de nos crimes,
Écrasé
À cause de nos fautes.
Le châtiment
Qui nous rend la paix est sur lui.
Et dans ses blessures
Nous trouvons la guérison.

3.
Maltraité, il s'humiliait,
Il n'ouvrait pas la bouche,
Comme l'agneau
Conduit à l'abbatoir.
Par contraite et jugement,
Il a été saisi,
Le Seigneur
A voulu l'écraser par la souffrance.

4.
S'il offre sa vie en sacrifice,
Il prolongera ses jours
Et par lui,
La volonté du Seigneur s'accomplira.
Le Juste, mon Serviteur,
Justifiera les multitudes
En s'accablant
Lui-même de leurs fautes.