1.
En ce temps-ci,
Comme ton souffle l’a transmis,
Parvient encor ton dernier cri,
Jésus misère…
Mais n’est-il pas plus assourdi,
D’autant qu’ont grandi le défi
Et l’âge de l’homme depuis
Deux millénaires ?

2.
Par compassion écoute alors
Tes amis demandant renfort
De lumière au signe de mort,
Loin dans l’histoire,
Afin que tout ce qu’ils ont cru
Ne se réduise à un corps nu
Sur un petit coteau perdu
Et dérisoire.

3.
Comme le juraient les soldats
Qui t’étoilèrent de crachats,
Et plus aujourd’hui qu’autrefois,
Comment te croire,
Tout défaillant (Dieu défaillant !),
Tout haletant (Dieu haletant !),
Suppliant le ciel (suppliant
Sans qu’il réponde !)

4.
En ce temps-ci,
Dans ta pitié pour tes amis,
Avance encor l’éclair promis
De Pentecôte,
Les confirmant comme témoins
Bien qu’ils ne verront qu’à leur fin
Le feu pascal être un matin
D’un bord à l’autre ;

5.
Redis-nous que se déchira
Le grand voile de haut en bas
Où la lumière pénétra
La nuit du germe ;
Par là nos pères ont poussé,
Mais il faut nous y rabaisser,
Car d’autres voiles sont tissés
Qui nous enferment.

6.
En ce temps-ci,
Maintiens ta grâce d’aujourd’hui
Avec les fils, comme tu fis
Avec leurs pères ;
Tiens-toi dans leur retournement,
Qu’ils te connaissent au présent
Mais dans le sens de toi venant
Comme lumière.

7.
Puisque maintenant nous savons
Que tu entras dans ta Passion
En prenant chair de création,
Tiens-nous de même
De la mort que nul ne connaît,
Sinon toi et tes nouveaux-nés,
Presse-nous jusqu’à nous former
De nos baptêmes.