R.
Lumière de lumière,
Le Christ illumine le monde, Alléluia.

1.
Le visage découvert, réfléchissons comme en miroir la gloire du Seigneur.

2.
Et nous le connaîtrons, lui et la puissance de sa Résurrection.