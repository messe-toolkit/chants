Viens, Esprit Saint, en nos coeurs
et envoie du haut du ciel
un rayon de ta lumière.

Viens en nous, père des pauvres,
viens, dispensateur des dons,
viens, lumière de nos coeurs.

Consolateur souverain,
hôte très doux de nos âmes,
adoucissante fraîcheur.

Dans le labeur, le repos ;
dans la fièvre, la fraîcheur ;
dans les pleurs, le réconfort.

O lumière bienheureuse,
viens remplir jusqu'à l'intime
le coeur de tous tes fidèles.

Sans ta puissance divine,
il n'est rien en aucun homme,
rien qui ne soit perverti.

Lave ce qui est souillé,
baigne ce qui est aride,
guéris ce qui est blessé.

Assouplis ce qui est raide,
réchauffe ce qui est froid,
rends droit ce qui est faussé.

À tous ceux qui ont la foi
et qui en toi se confient
donne les sept dons sacrés.

Donne mérite et vertu,
donne le salut final,
donne la joie éternelle.

Amen.
Amen.