SI ON AVAIT SU
SI ON L’AVAIT CRU
ON N’AURAIT JAMAIS PU
FRAPPER ET TUER JÉSUS.

1.
Arrêter, appréhender le fils de l’homme !
C’est ôter en soi la voie qui est la bonne
Humilié, bafoué le Roi des cieux
Brisé le bel Amour qui vient de Dieu
Maltraitance, ton silence est violence,
Elle s’attaque à l’enfance, à l’innocence
Des souillures en blessures qu’elle endure !
Pour tous ces êtres ! Comment renaître ?

2.
Parodie de justice, procès trop lisse !
Faites courber les échines ! Plantez l’épine !
Humilié, bafoué le Roi des cieux
Brisé le bel Amour qui vient de Dieu
Corruption, en col blanc pas salissant
T’as la douche si tu touches aux affaires louches
Relâchez ! Libérez qui peut payer !
Vagabond ! Pour toi ! C’est la prison !

3.
Sur la croix on l’a cloué avec couronne
Par le droit qui l’ordonne, qui s’en étonne ?
Humilié, crucifié le Roi des cieux
Brisé le bel Amour qui vient de Dieu
Le progrès électrique est plus pratique
Corps sans lutte qui culbutent, qu’on exécute
Endurci, l’animal est plus fatal
Conversion ! Pour les coeurs de lion.