R.
JE SUIS UN PETIT ÂNON,
VOUS POUVEZ OUBLIER MON NOM
J’AI PORTÉ JÉSUS SUR MON DOS.
C’ÉTAIT UN BIEN LÉGER FARDEAU.

1.
Les pauvres étaient aimés de lui
Et les malades étaient guéris
Il nous parlait de Dieu son père
Pour lui chaque homme était un frère
Les enfants étaient ses amis. (bis)
Un jour, il vint dans la cité
On est venu pour me chercher
J’ai vu la fierté de ma mère
Car le meilleur homme sur terre
M’était donné pour cavalier. (bis)

2.
Partout il était acclamé
Avec des rameaux de palmiers
On rendait grâce à Dieu son père
Qui l’envoyait sur notre terre
Pour prêcher la fraternité. (bis)
« Le seul trésor qui rend heureux
C’est l’amour vrai qui vient de Dieu »
Jésus l’enseignait dans le temple
Chacun le citait en exemple
On parlait de lui en tous lieux. (bis)

3.
Il avait chassé les marchands
Qui pensaient : « Dieu aime l’argent
De ceux qui viennent les mains pleines »
Jésus en avait de la peine
Il disait garde un coeur d’enfant. (bis)
Un soir, avant la longue nuit,
Il dit aux amis réunis :
« Pain de la terre, vin de la vigne
Partagez-les comme le signe,
En mémoire de votre ami ». (bis)

4.
Mais la haine et la jalousie
De ceux qui n’avaient rien compris
Sont dans le coeur comme des ronces
Et c’est bientôt que l’on dénonce
Jésus pour le pire des bandits. (bis)
Ses amis s’enfuirent en tremblant
On emprisonna l’innocent
Et pour le sauver plus personne
Voilà la justice des hommes
On le crucifia en riant. (bis)

5.
Quand s’est dispersé le troupeau
Sa mère n’était plus qu’un sanglot
Et sous le ciel noir de ténèbres
Je les vis emporter mon Maître
On le déposa au tombeau. (bis)
Mais le matin, trois jours après
Des femmes qui venaient prier
Ont annoncé un grand mystère
Jésus vivait dans la lumière
Jésus était ressuscité. (bis)

JE SUIS UN PETIT ÂNON
VOUS POUVEZ OUBLIER MON NOM
J’AI PORTÉ JÉSUS SUR MON DOS
C’ÉTAIT UN BIEN LÉGER FARDEAU.
JE SUIS UN PETIT ÂNON
VOUS POUVEZ OUBLIER MON NOM
GARDEZ MON HISTOIRE DANS VOS COEURS
VOUS RENDREZ LE MONDE MEILLEUR.