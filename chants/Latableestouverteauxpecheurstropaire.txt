Stance
La table est ouverte au pécheurs,
Jésus invite, il nous attend.
Approchons du Maître de la vie,
lui seul peut nous guérir,
nous rendre la joie d'être sauvés.

R.
Près de toi, Seigneur,
l'abondance du pardon !

Versets

1.
Je suis venu appeler non pas les justes
mais les pécheurs.

2.
Si vous entendez ma voix,
ne fermez pas votre coeur.

3.
À ceux qui en ont soif
j'offre la délivrance

4.
Aujourd'hui l'annonce du salut
résonne à vos oreilles.