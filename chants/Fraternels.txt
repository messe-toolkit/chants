R.
FRATERNELS
ENFANTS DE LA LUMIÈRE
JÉSUS NOUS APPELLE
À ÊTRE FRATERNELS
HEUREUX DE VIVRE EN FRÈRES
NÉS DU MÊME PÈRE ÉTERNEL.
Fraternels (bis)

1.
Issus de mondes bien divers
Et d’horizons qui tant différent
D’un même élan, tous rassemblés
Venir ensemble rencontrer, prier, chanter.

2.
Il n’est d’ami ni d’âme soeur
À qui pouvoir livrer nos coeurs
Sinon à toi divin Berger
Nous désirons te rencontrer, prier, chanter.

3.
Dans le vacarme de nos vies
S’élève une autre mélodie
Qui nous entraîne à T’adorer
Et à venir te rencontrer, prier, chanter.

(Fraternels) Ma soeur, mon frère
(Fraternels) en Jésus-Christ
(Fraternels) Accueillons-nous,
Comme lui nous a accueillis, réunis.