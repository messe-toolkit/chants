R.
Regardez Dieu votre Père, allez vers lui,
Vivez sous sa lumière, votre Père vous aime. (bis)

1.
Nous devons regarder Dieu car nous sommes ses enfants.
Son amour nous enveloppe et nous garde.

2.
Dieu m’a aimé, il m’a donné sa grâce.
Il m’appelle, c’est lui mon espérance.

3.
Pour trouver Dieu, il faut aller vers le Christ Jésus
Et veiller paisiblement dans la foi, veiller paisiblement dans la foi.

4.
Il me connaît et il m’aime. À mon tour, je le connais et je l’aime.
Il m’aime comme un Père et je l’aime comme un fils : c’est sa joie.