R.
Toi Jésus tu choisis de donner ta vie
Tu choisis de donner ta vie.

1.
Par amour tu veux rassembler
Nous mener vers Dieu ton Père
Toi Jésus tu viens tout donner
Pour ouvrir à la lumière.

2.
Sur la croix Dieu a tout changé
Il te fait vivre de sa vie
Tu es mort et ressuscité
Bien vivant pour tes amis.

3.
Chaque jour pour te ressembler
Comme toi on peut dire oui
Toi Jésus tu viens nous guider
En donnant toujours l’Esprit.