R.
C'EST VRAI QU'ON N'Y PENSE PAS VRAIMENT
MAIS ÇA FAIT PEUR DE TEMPS EN TEMPS
AMIS, FAITES-NOUS CE CADEAU
METTEZ UN COEUR A VOS ROBOTS.

1.
Dis-moi comment sera demain
De quelle couleur sera le pain
Pourrons-nous voir voler encore
L'oiseau et le papillon d'or
Dis-moi si au fond du jardin
On cueillera encore le thym
Et dans la voix des troubadours
Des notes pour chanter l'amour.

2.
Dis-moi comment sera demain
Des hommes auront-ils encore faim
Aurons-nous su donner enfin
Du travail à toutes les mains
Pourrons-nous respirer encore
Dans la fumée et dans le chlore
L'arbre aura-t-il encore son lierre
Et les poissons une rivière.

3.
Si nous avons peur aujourd'hui
C'est parce que nous aimons la vie
Et si demain semble un peu gris
Nos coeurs veulent battre pour lui.

4.
Dis-moi comment sera demain
De quelle couleur sera le pain
Pourrons-nous voir voler encore
L'oiseau et le papillon d'or
Dis-moi comment sera demain
Des hommes auront-ils encore faim
Dis-moi si au fond du jardin
On cueillera encore le thym.