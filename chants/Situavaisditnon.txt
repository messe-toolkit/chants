R.
Marie, si tu avais dit non
Nos jardins seraient abandonnés
Marie, si tu avais dit non
Jamais le blé n'aurait levé

1.
Dieu t'a parlé
Tu donnes ta réponse
Tu deviens la terre du Seigneur
Pour que Dieu y jette semence
Et que vienne naissance

2.
En Galilée
Tu nous montres le Maître
"Faites tout ce qu'il vous dira"
Qu'aujourd'hui la noce s'apprête
Et que s'ouvre la fête

3.
Au Golgotha
Tu deviens notre Mère
Et d'amour ton coeur est transpercé
De la croix jaillit une eau vive
Que la paix nous arrive.