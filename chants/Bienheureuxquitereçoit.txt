BIENHEUREUX QUI TE REÇOIT !
TU LE CONDUIS VERS L’EAU VIVE DE TA JOIE.
BIENHEUREUX QUI TE REÇOIT !
TU FAIS DE LUI LE SERVITEUR DE TA JOIE.

1.
Fils de l’homme à la croisée de nos chemins,
Sois béni de proclamer « Heureux les pauvres ! »
Tu le dis car Dieu te donne un coeur de pauvre,
Le Royaume des cieux t’appartient.

2.
Toi le Maître provoqué par les violents,
Tu nous montres la douceur qui est ta force :
Chant nouveau couvrant les cris de nos révoltes,
Résistance au pays des vivants.

3.
Toi qui pleures avec tes frères dans la nuit,
Tu choisis de communier à toute peine.
Par l’Esprit consolateur tu nous relèves,
L’espérance avec toi refleurit.

4.
Toi le Juste en qui rayonne le Dieu saint,
Grand Prophète avec des mots criant justice,
Creuse en nous la soif de vivre à ton service ;
Il viendra, le moment du festin.

5.
Fils d’un Dieu qui est pardon pour les pécheurs,
Tu nous dis combien sa grâce nous façonne.
Brûle-nous de son amour miséricorde,
Nous saurons pardonner de tout coeur.

6.
Bienheureux celui qui garde le coeur pur,
Il verra le Dieu d’Alliance qui nous aime.
C’est en Toi que l’Invisible se révèle,
Saurons-nous découvrir l’Inconnu ?

7.
Fils de Dieu toujours à l’oeuvre pour la paix,
Fais de nous des baptisés briseurs de guerre.
Avec nous tu veux combattre les ténèbres ;
Tiens-nous forts au soleil de ta Paix !

8.
Fils de l’homme aux bras ouverts sur une croix,
De la mort Dieu te relève et nous libère.
Comment croire à cette Pâque de lumière ?
Viens, Jésus, réveiller notre foi !

9.
Dans l’Église au long des siècles tu es là ;
En chemin vers Emmaüs ta voix nous parle.
Ton Esprit nous fera dire l’Ineffable,
Il nous mène aujourd’hui sur tes pas !

10.
Jésus Christ ressuscité, tu nous envoies.
Serons-nous des messagers « sel de la terre » ?
Dans nos vies, que resplendisse ta lumière,
Nous serons serviteurs de ta joie.