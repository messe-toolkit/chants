NOUS AVONS VU LE SEIGNEUR.
IL MARCHE SUR NOS CHEMINS
NOUS AVONS VU LE SEIGNEUR.
NOUS EN SOMMES TÉMOINS.

1.
Jean-Baptiste annonce :
« Voici l’Agneau de Dieu. »
J’ai vu, je l’atteste :
Il est Fils de Dieu. (bis)

2.
Jésus se révèle :
“Le Consacré de Dieu.”
Oui nous le croyons
L’Esprit est sur toi. (bis)

3.
Et Jésus appelle
Matthieu, l’homme au coeur droit
Tu changes mon coeur
Je marche avec toi. (bis)

4.
Marie soit sans crainte
Ton enfant est béni
Moi je suis comblé
L’Esprit est sur moi. (bis)

5.
Prenant la parole
Simon lui répondit :
Tu es le Messie
Fils du Dieu vivant. (bis)

6.
Thomas tu retardes
Et tu ne veux pas voir
Si je fais confiance
Je suis dans la joie. (bis)

7.
Jean dit aux disciples
De suivre le Seigneur
Nous marchons ensemble
Nous suivons Jésus. (bis)