1.
Si grande est la maison de Dieu,
Qu’il y aura toujours,
Une place pour moi,
Tout pécheur que je suis,
Une place pour moi dans la maison de Dieu !

2.
Si riche est la table de Dieu,
Qu’il y aura toujours
Une place pour moi
Tout pécheur que je suis,
Une place pour moi à la table de Dieu !

3.
Si vaste est le jardin de Dieu,
Qu’il y aura toujours
Une place pour moi
Tout pécheur que je suis,
Une place pour moi dans le jardin de Dieu !

4.
Si forts sont les bras de mon Dieu,
Qu’il y aura toujours
Une place pour moi
Tout pécheur que je suis,
Une place pour moi dans les bras de mon Dieu !

5.
Si tendre est le cœur de mon Dieu,
Qu’il y aura toujours
Une place pour moi
Tout pécheur que je suis,
Une place pour moi dans le cœur de mon Dieu !