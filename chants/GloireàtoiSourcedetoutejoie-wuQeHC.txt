R.
Gloire à toi, Seigneur, source de toute joie !
Gloire à ton nom, ô Dieu très Saint !
Gloire à toi, Seigneur, ô toi, le Roi des rois !
Amen, Alléluia !

1.
Le Seigneur est ma lumière et mon salut,
Le Seigneur est le rempart de ma vie.
Je vivrai dans la maison du Seigneur,
Maintenant et à jamais.

2.
Que mon cœur exulte, mon âme te loue,
Tu ne peux m'abandonner à la mort.
Tu m'apprendras le chemin de la vie,
Plénitude de la joie.

3.
Tous les peuples de la terre louez Dieu
Annoncez la Vérité aux nations.
Bienheureux qui met sa foi dans le Seigneur,
Éternel est son amour !