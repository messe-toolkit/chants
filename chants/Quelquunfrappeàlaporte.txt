Refrain :
Quelqu’un frappe à la porte
Serait-ce lui, enfin ?
Celui qui nous apporte,
L’espérance et le pain ! ...

1.
C’est bien lui, il me semble,
Qui nous tenait la main,
Quand nous allions ensemble,
Affronter l’incertain...
Il parlait d’une “ eau vive ”,
Nous étions assoiffés,
Et puis d’une autre rive,
Qu’il nous faut rechercher...

2.
Il a gardé, par chance,
Son accent étranger,
Le goût des différences,
Le sens de l’unité.
Il a tout dit, en somme,
Avec le verbe “ aimer ”,
Au nom des droits de l’homme,
Et de la dignité.

3.
Un soir, dans notre auberge,
Nous l’avons invité ;
Un matin sur la berge,
Nous l’avons rencontré.
Belle fut l’aventure,
Nous l’avions entendu ;
Oubliées, les blessures,
Puisque nous l’avions vu...