1.
Te parler, tu vois, me fait peur parfois
Te parler tout bas, tu sais, me va mieux
Je ferme les yeux, le silence en moi
Et tout doucement
Je viens te dire : je t’aime

2.
Et je pense alors à tous mes amis
Aux meilleurs moments de cette journée
Je ferme les yeux, le silence en moi
Et tout doucement
Je viens te dire : merci

3.
Mais je vois aussi ce que j’ai mal fait
Les regards colère et les mots piquants
Je ferme les yeux, le silence en moi
Et tout doucement
Je viens te dire : pardon

4.
Te parler tout bas et de coeur à coeur
C’est à chaque fois beaucoup de bonheur
En fermant les yeux, le silence en moi
Alors doucement
J’aime te dire : je t’aime