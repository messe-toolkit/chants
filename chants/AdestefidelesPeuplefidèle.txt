1.
Peuple fidèle,
Le Seigneur t'appelle :
C'est fête sur terre,
Le Christ est né.
Viens à la crèche
Voir le roi du monde.

R.
En lui viens reconnaître,
En lui viens reconnaître,
En lui viens reconnaître,
Ton Dieu, ton Sauveur.

2.
Verbe, Lumière,
Et Splendeur du Père,
Il naît d´une mère,
Petit enfant.
Dieu véritable,
Le Seigneur fait homme.

3.
Peuple, acclame,
Avec tous les anges,
Le Maître des hommes.
Qui vient chez toi.
Dieu qui se donne
A tous ceux qu'il aime !

4.
Peuple fidèle,
En ce jour de fête,
Proclame la gloire
De ton Seigneur.
Dieu se fait homme
Pour montrer qu'il t´aime.