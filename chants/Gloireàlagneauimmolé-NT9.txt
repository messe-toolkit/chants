R.
Puissance, honneur et gloire à l'agneau de Dieu.

1.
C'est toi qui créas l'univers?; tu as voulu qu'il soit?: il fut créé.

2.
Tu es digne, Christ et Seigneur, de prendre le livre et d'en ouvrir les sceaux.

3.
Car tu fus immolé, rachetant pour Dieu, au prix de ton sang, des hommes de toute tribu, langue, peuple et nation.

4.
Tu as fait de nous pour notre Dieu un royaume de prêtres, et nous régnerons sur la terre.

5.
Il est digne l'Agneau immolé, de recevoir puissance et richesse, sagesse et force, honneur, gloire et louange.