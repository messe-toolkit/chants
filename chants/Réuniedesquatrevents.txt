OUVERTURE
Réunie des quatre vents
autour de son pasteur,
notre Église fait signe !
Peuple de prêtres, peuple de rois,
revivons le mystère
de la Pâque du Crucifié,
ressuscité par la force de l'Esprit !

R.
Comme une huile d'allégresse,
comme un baume de grand prix,
Esprit Saint, pénètre au profond de nos coeurs !

1.
Pour que notre évêque soit fidèle
à la maison que le Seigneur lui a confiée,
Viens, Esprit Saint, lumière des coeurs,
Viens, comme une huile d'allégresse !

2.
Pour que les prêtres soient de bons intendants
des mystères de Dieu,
Viens, Esprit Saint, force dans l'épreuve,
Viens, comme un baume de grand prix !

3.
Pour rendre vigueur aux corps épuisés,
pour chasser toute douleur et toute maladie,
Viens, Esprit Saint, espoir de l'homme en pleurs,
Viens, comme une huile d'allégresse !

4.
Pour libérer l'esprit, l'âme et le corps
de toute souffrance physique ou morale,
Viens, Esprit Saint, paix du coeur enfiévré,
Viens, comme un bau-me de grand prix !

5.
Pour que les catéchumènes accueillent la Bonne Nouvelle
et s'engagent dans le combat de la vie chrétienne,
Viens, Esprit Saint, hôte apaisant de l'âme,
Viens, comme une huile d'allégresse !

6.
Pour ceux qui renaissent dans l'eau du baptême
entrent dans la vie divine et communion à la gloire du ciel,
Viens, Esprit Saint, toi le bonheur sans fin,
Viens, comme un baume de grand prix !

ENVOI
Envoyée aux quatre vents
porter la paix du Seigneur,
notre Église fait signe !
Peuple d'apôtres pour la mission
annonçons le mystère de la Pâque du Crucifié,
ressuscité par la force de l'Esprit !