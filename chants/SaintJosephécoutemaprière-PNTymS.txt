R.
Saint-Joseph, écoute ma prière
Guide-moi vers Lui comme un enfant
Saint-Joseph, obtiens moi sa lumière
Pour rester en Lui toujours présent.

1.
- Bienheureux l’humble époux de Marie
Protecteur et père nourricier,
Accueillant chastement dans sa vie
Un PROJET – SON PROJET.

2.
- Bienheureux qui peut permettre à l’autre
De grandir, de naître et devenir,
Le témoin silencieux et l’apôtre
D’UN DÉSIR – SON DÉSIR.

3.
- Bienheureux serviteur d’un mystère
Recensé aux livres des vivants,
Pour rester simple dépositaire
DE L’ENFANT – SON ENFANT.

4.
- Bienheureux qui vit à mains ouvertes
Et choisit la pureté du cœur,
Pour permettre en soi la découverte
DU BONHEUR – SON BONHEUR.