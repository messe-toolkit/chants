AU COEUR DU MONDE POUR.
UN MONDE DU COEUR.
AU COEUR DU MONDE POUR.
UN MONDE MEILLEUR.
POUR L’AMITIÉ QUI FAIT
GRANDIR LA PAIX
AU COEUR DU MONDE, IL EST
TEMPS D’AIMER.
ENSEMBLE SUR CETTE TERRE
ENSEMBLE, VIVONS EN FRÈRES
ENSEMBLE AU COEUR DU
MONDE
LÀ OÙ DIEU NOUS A
ENVOYÉS.

1.
De tous temps sur la Terre
Des hommes se sont levés
L’espérance en bandoulière
Épris de fraternité !

2.
Apprentis de l’amour
Artisans des p’tits bonheurs
Dans nos vies, jour après jour
Jésus enchante nos coeurs.

3.
L’Esprit Saint vient souffler
L’amour du Père sur le monde
L’Esprit, vent de liberté
Rend nos vies belles et fécondes.

AUTRES TEMPS LITURGIQUES

Rentrée

4.
Toi et moi égale nous
Mieux que les mathématiques
Une année devant nous
Pour des moments sympathiques.

5.
Tous ensemble on est bien
Viens partageons nos richesses
Celles du coeur c’est certain
Et chanter dans l’allégresse.

Toussaint

6.
De tous temps sur la Terre
Des hommes se sont levés
L’espérance en bandoulière
Épris de fraternité.

7.
Apprentis de l’amour
Artisans des p’tits bonheurs
Dans nos vies, jour après jour
Jésus enchante nos coeurs.

Avent - Noël

8.
C’est le temps de l’Avent
Une aube nouvelle se lève
Dieu s’approche doucement
Un vent d’amour nous relève.

9.
Approchons de l’Enfant
Nos coffrets pleins de tendresse
Marie, Joseph, ses parents
Les yeux remplis de sagesse.

10.
Vivre un Noël du coeur
C’est bien plus engageant
Bien plus qu’une lueur
C’est partager simplement.

Chandeleur

11.
Éclairons notre vie
De Jésus la vraie lumière
Partageons ce qu’il nous dit
Par-delà toute frontière.

12.
Sa lumière me révèle
Ma richesse intérieure
Son amour me donne des ailes
Pour chanter un monde meilleur.

Pâques

13.
Au jardin ce matin
Les femmes ont accouru
Apportant les parfums
Pour embaumer Jésus.

14.
Quand elles sont arrivées
Au tombeau, ne l’ont pas vu
Dieu l’a ressuscité
La mort a été vaincue !

Fin d’année

15.
L’Esprit Saint vient souffler
L’amour du Père sur le monde
L’Esprit, vent de liberté
Rend nos vies belles et fécondes.

16.
Nous voici envoyés
Sur les routes du monde
Allons partout proclamer
Son amour pour notre monde.