DONNEZ DE LA PAIX,
DONNEZ DE LA JOIE,
VOUS VERREZ FLEURIR LE MONDE,
L’AMOUR ÉCLATERA.

1.
Il suffirait d’un peu de terre
Pour que germe le grain de blé ;
Peut-être de pain partagé
Pour commencer de vivre en frères.

2.
Il suffirait d’une hirondelle
Pour cueillir un air de printemps ;
Peut-être d’un rire d’enfant
Pour rendre la journée plus belle.

3.
Il suffirait d’un clair de lune
Pour chasser le noir de la nuit ;
Peut-être écouter un ami
En faisant taire une rancune.

4.
Il suffirait d’un ciel d’étoiles
À celui qui cherche un chemin ;
Peut-être qu’en tendant la main,
Jésus nous invite à la table.

5.
Il suffirait d’une eau limpide
Pour que refleurissent les fleurs ;
Peut-être d’un peu de chaleur
Pour réchauffer nos maisons vides.