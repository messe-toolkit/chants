DEBOUT, À LA DROITE DU SEIGNEUR,
SE TIENT LA REINE, TOUTE PARÉE D’OR.

1.
Écoute, ma fille, regarde et tends l’oreille ;
oublie ton peuple et la maison de ton père :
le roi sera séduit par ta beauté.

2.
Il est ton Seigneur : prosterne-toi devant lui.
Alors, les plus riches du peuple,
chargés de présents, quêteront ton sourire.

3.
Fille de roi, elle est là, dans sa gloire,
vêtue d’étoffes d’or ;
on la conduit, toute parée, vers le roi.

4.
Des jeunes filles, ses compagnes, lui font cortège ;
on les conduit parmi les chants de fête :
elles entrent au palais du roi.