R.
Toi, Joseph, l’humble charpentier,
tu ouvres un chemin à suivre chaque jour,
Toi, Joseph, l’humble charpentier,
tu montres une vie sans peur de se donner.

1.
Malgré les apparences, tu avais recueilli une mère bénie,
sainte Vierge Marie, n’écoutant que ta foi, tu as su dire oui.
Joseph, par ta confiance tu nous guide en nos vies.

2.
Tu veillais sur Marie quand elle donnait la vie, image de tendresse,
d’attention et de paix. Simple et silencieux, toi l’humble charpentier,
tu es père à jamais par amour infini.

3.
Tu as aimé ce fils et tu lui as souri, il a grandi en paix au cœur de ta maison.
En père nourricier, tu accueillais ton Dieu.
Joseph, toi qui es bon, intercède en nos noms.

4.
Tu rencontres ton Dieu et tu sais discerner la parole des anges
et la voix de Yahvé. Joseph, par le silence et l’intériorité
tu donnes à ta prière un goût d’éternité.

5.
Protégeant la famille que Dieu t’avait confiée, tu n’as pas hésité
à tout abandonner. Sur les chemins du monde à fuir et à tout quitter.
Joseph que ta sagesse nous aide à tout donner.

6.
Descendant de David, tu n’as revendiqué ni gloire ni honneur,
tu t’es abandonné. Tu as fait de ta vie la volonté de Dieu.
Aide-nous à garder la foi comme un grand feu.

7.
Saint Patron de l’église, nous pouvons te confier notre foi, nos prières
et nos fragilités. Serviteur silencieux de l’appel du seigneur, Joseph,
nous te portons pour toujours en nos cœurs.

 