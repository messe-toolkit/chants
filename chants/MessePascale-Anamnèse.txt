Quand nous mangeons ce pain et buvons à cette coupe,
nous célébrons le mystère de la foi.
TU ES VRAIMENT RESSUSCITÉ,
SEIGNEUR JÉSUS VAINQUEUR DE LA MORT.
NOUS ATTENDONS TON JOUR GLORIEUX!
ALLÉLUIA !