TU M’APPRENDS, SEIGNEUR,
LE CHEMIN DE LA VIE.

1.
Garde-moi, mon Dieu : j’ai fait de toi mon refuge.
J’ai dit au Seigneur : « Tu es mon Dieu !
Seigneur, mon partage et ma coupe :
de toi dépend mon sort. »

2.
Je bénis le Seigneur qui me conseille :
même la nuit mon coeur m’avertit.
Je garde le Seigneur devant moi sans relâche ;
il est à ma droite : je suis inébranlable.

3.
Mon cœur exulte, mon âme est en fête,
ma chair elle-même repose en confiance :
tu ne peux m’abandonner à la mort
ni laisser ton ami voir la corruption.

4.
Tu m’apprends le chemin de la vie :
devant ta face, débordement de joie !
À ta droite, éternité de délices !