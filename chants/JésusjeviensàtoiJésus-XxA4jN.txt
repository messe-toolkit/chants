R.
Jésus, je viens à toi,
Jésus, je crois en toi,
Entends ma voix, Seigneur,
Je veux vivre pour toi.
Jésus, je viens à toi.

1.
Ta Parole Seigneur est lampe sur mes pas,
Je suis ton serviteur, ô Dieu éclaire-moi.

2.
Mes soucis, mes pensées, je veux te les donner,
Tu es le bon berger, et je veux t´adorer.

3.
Je suis pauvre, Seigneur, mais toi tu penses à moi.
Mon secours, mon Sauveur, mon Dieu, ne tarde pas.