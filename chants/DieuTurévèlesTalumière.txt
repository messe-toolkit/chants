1.
Dieu, tu révèles ta lumière
À ceux qui passent par la nuit ;
Béni sois-tu
Pour les yeux qui s’ouvrent aujourd’hui
Dans la terre nouvelle :
Ils te rencontrent, Dieu vivant !

2.
Tu leur dévoiles ton visage
Après l’Exode et la Nuée ;
Béni sois-tu
Pour les yeux où danse un reflet
De l’aurore pascale :
Ils te contemplent, Dieu vivant !

3.
Tu leur découvres ta présence
Et l’allégresse des sauvés ;
Béni sois-tu
Pour les morts qui trouvent la paix
Dans la joie de tes Noces :
Ils sont ta gloire, Dieu vivant !

4.
Tu les attires en ton Mystère
Avec la force de l’Esprit ;
Béni sois-tu
Pour les corps où monte la vie
Jusqu’à l’Aube éternelle :
Ils ressuscitent, Dieu vivant !

R.
GLOIRE À TOI, SEIGNEUR,
LUMIÈRE DU ROYAUME !