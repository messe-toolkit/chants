MARCHE, MARCHE, MARCHE DEVANT
L’AVENTURE T’ATTEND.
MARCHE, MARCHE, MARCHE EN SUIVANT
LA CHANSON DU VENT,
LA CHANSON DU VENT.

1.
Oser quitter sa terre
Et suivre les étoiles
Oser franchir les mers
Au gré de la grand-voile.

2.
Oser partir plus loin
Où vivent d’autres frères
Oser livrer sans fin
La guerre à la misère.

3.
Oser rêver tout haut
D’un monde solidaire
Oser crier bravo
Quand tombent les frontières.

4.
Oser parler d’amour
Et croire en la tendresse
Oser rester toujours
Fidèle à ses promesses.

5.
Oser enfin sans peur
Montrer son vrai visage
Oser avoir le coeur
Veilleur, prophète et sage.