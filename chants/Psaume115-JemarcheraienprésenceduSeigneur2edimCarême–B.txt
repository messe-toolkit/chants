JE MARCHERAI EN PRÉSENCE DU SEIGNEUR.
SUR LA TERRE DES VIVANTS.

1.
Je crois, et je parlerai,
moi qui ai beaucoup souffert.
Il en coûte au Seigneur
de voir mourir les siens !

2.
Ne suis-je pas, Seigneur, ton serviteur,
moi, dont tu brisas les chaînes ?
Je t’offrirai le sacrifice d’action de grâce,
j’invoquerai le nom du Seigneur.

3.
Je tiendrai mes promesses au Seigneur,
oui, devant tout son peuple,
à l’entrée de la maison du Seigneur,
au milieu de Jérusalem !