R.
Le Seigneur nous unit.
Son amour nous a choisis pour le monde.

1.
L'amour est signe d'Alliance :
Le Seigneur épouse et conduit son peuple,
Signe d´unité dans le monde !

2.
Que notre amour soit le signe
De l'Amour du Christ pour l'Église sainte,
Temple du Seigneur dans le monde.

3.
Dieu nous envoie dans le monde
Annoncer la joie de son Évangile.
Il a fait de nous ses prophètes.

4.
Qu'en nous l'Église grandisse !
Notre amour nous ouvre à l'amour des autres.
Qu'il nous donne une âme de pauvre !

5.
Dans la souffrance et la peine,
Le péché ne peut vaincre en nous la grâce.
En nos vies, la croix est victoire.

6.
En notre amour Dieu invite
A la joie des noces la terre entière.
Il rassemble en nous son Église.

7.
Dieu est en notre rencontre.
Que de notre amour nos cœurs s'émerveillent.
Par lui notre vie est louange.

8.
Dieu est amour dans nos vies.
Que la joie en nous soit toujours plus claire.
Notre amour rejoint son mystère.