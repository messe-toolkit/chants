CHRIST EST RESSUSCITÉ DES MORTS !

CHRIST EST RESSUSCITÉ DES MORTS, AMEN, ALLELUIA !
IL A FAIT TRIOMPHER LA VIE, AMEN, ALLELUIA !

1.
Il vient ouvrir tous nos tombeaux, AMEN, ALLELUIA,
Il fait naître un printemps nouveau, AMEN, ALLELUIA !

2.
Son amour a vaincu la nuit, AMEN, ALLELUIA,
Il nous revêt de sa lumière, AMEN, ALLELUIA !

3.
Il est venu nous relever, AMEN, ALLELUIA,
Et nous faire vivre pour toujours, AMEN, ALLELUIA !

4.
Il a fait ressurgir nos sources, AMEN, ALLELUIA,
Il ensoleille nos matins, AMEN, ALLELUIA !

5.
Il nous délivre de la peur, AMEN, ALLELUIA,
Sa liberté nous est donnée, AMEN, ALLELUIA !

6.
Il offre sa douceur divine, AMEN, ALLELUIA,
Pour rejoindre le cœur des hommes, AMEN, ALLELUIA !