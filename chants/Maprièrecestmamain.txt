R.
MA PRIÈRE C’EST MA MAIN,
MA PRIÈRE C’EST MA MAIN.

1.
Ma main de toutes les besognes
Ma main arrondie sur l’outil
Ma main qui caresse et qui cogne
Fleur à la main ou au fusil.

2.
Ma main qui va t’offrir à boire
Ma main levée à ta santé
Qu’elle soit blanche, jaune ou noire
C’est pour cueillir la liberté.

3.
Ma main où tu passes l’alliance
La main d’un tout petit enfant
Les mains jointes dans le silence
Mes mains cathédrales du temps.

4.
Le fruit ridé de la vieillesse
Deux maigres mains sur un drap blanc
Deux fleurs ouvertes de tendresse
Ce sont les mains d’un Dieu vivant.