AUJOURD’HUI, NE FERMEZ PAS VOTRE COEUR,
MAIS ÉCOUTEZ LA VOIX DU SEIGNEUR !

1.
Venez, crions de joie pour le Seigneur,
acclamons notre Rocher, notre salut !
Allons jusqu’à lui en rendant grâce,
par nos hymnes de fête acclamons-le !

2.
Entrez, inclinez-vous, prosternez-vous,
adorons le Seigneur qui nous a faits.
Oui, il est notre Dieu ;
nous sommes le peuple qu’il conduit.

3.
Aujourd’hui écouterez-vous sa parole ?
« Ne fermez pas votre coeur comme au désert,
où vos pères m’ont tenté et provoqué,
et pourtant ils avaient vu mon exploit. »

ALLÉLUIA. ALLÉLUIA.

Dans le Christ,
Dieu réconciliait le monde avec lui :
il a mis dans notre bouche la parole de la réconciliation.