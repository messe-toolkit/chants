4.
Refrain :
Dieu est amour, Dieu est lumière !
verset 1 :
Tu es, Seigneur, notre Père, notre Rédempteur.
verset 2 :
Tu as envoyé ton Fils
pour que le monde soit sauvé.

5 Refrain : Gloire au Père de Jésus,
* gloire à notre Dieu !
verset 1 :
Lui, le premier nous a aimés. *
verset 2 :
Il nous a délivrés de nos péchés. *
verset 3 :
Il nous offre l’héritage des fils.

6.
Refrain : Si quelqu’un garde ma Parole,
* mon Père l’aimera.
verset :
Nous viendrons demeurer en lui. *
doxologie :
Gloire au Père et au Fils et au Saint Esprit.