Je suis ton enfant
Je suis ton enfant
Je suis ton enfant,
je m’abandonne à toi,
Je suis ton enfant,
en toi toute ma joie
Dieu tu es mon Dieu,
Je te cherche dès l’aurore,
Mon âme a soif de toi,
mon âme s’attache à toi
J’ai vu ta force, ta gloire,
sois louange à mes lèvres,
Toute ma vie te bénir,
lever les main vers toi
La nuit, tu es là, oui,
viens à mon secours,
Je veux crier de joie,
à l’ombre de tes ailes