1.
Lumière enfouie sous le boisseau,
Le prince de l'ombre m'épuise !
Vous n'aurez plus besoin de lune ou de soleil
Agneau de Feu, je suis votre flambeau,
Moi seul peux vous conduire au jour,
Mon jour qui se lève aux cieux nouveaux
Par le jardin où j'agonise.

2.
Parole atteinte par les eaux,
L'angoisse me force au silence !
Vous n'aurez plus besoin de lune ou de soleil
Agneau vainqueur, je suis votre flambeau,
Moi seul peux parler de paix,
Ma paix qui règne aux cieux nouveaux
Puisque la croix me fait violence.

3.
Victime offerte à mes bourreaux,
Mon corps n'est plus rien que blessure !
Vous n'aurez plus besoin de lune ou de soleil
Agneau de Dieu, je suis votre flambeau,
Moi seul peux vous combler de joie,
Ma joie qui s'ouvre aux cieux nouveaux
Puisqu'au Calvaire on me torture.

4.
Semence enfouie dans le tombeau,
La mort m'a couchée sous la pierre !
Vous n'aurez plus besoin de lune ou de soleil
Agneau vivant, je suis votre flambeau,
Moi seul peux vous donner la vie,
Ma vie qui fait les cieux nouveaux
Dans la cité de notre Père.