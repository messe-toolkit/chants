STANCE
Quand tu viendras dans ta vigne préférée
Au jour de la vendange,
Tu ne trouveras pas, Seigneur, en nos mains
Le fruit que tu désires,
Et pourtant nous avons soif de vin nouveau.

R.
Verse-nous le sang du raisin
Broyé au pressoir de la croix,
Et nous élèverons vers toi
La coupe qui te rend gloire.

VERSET
Seigneur, efface nos péchés,
Aide-nous, Dieu, notre Sauveur.

R.
Et nous élèverons vers toi
La coupe qui te rend gloire.