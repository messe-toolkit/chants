R.
Porter la lumière, c’est une prière,
C’est laisser faire le premier pas
De l’espérance au creux de soi.
Porter la lumière, c’est une prière,
Cette foi qui fait reculer l’obscurité.

1.
Guérir notre coeur
Des doutes et des peurs
Qui détournent nos regards
Du jour qui se lève.
\n
Se laisser gagner
Par la vérité ;
Dieu nous aide à inventer
La fraternité.

2.
Guérir nos esprits
Des fleurs de la nuit,
Du vent qui vient assécher
La nouvelle sève.
\n
Se laisser greffer
À l’arbre dressé,
Recevoir la vie donnée
Du ressuscité.

3.
Il n’est pas trop tard
Pour choisir l’espoir
De se laisser enlacer
Par l’amour du Père.
\n
De laisser jaillir,
D’aider à grandir
Les semences de l’Esprit
Qui nous donnent vie.


4.
Ne pas laisser libre
Le morceau de givre
Viens embuer les carreaux
Nos petites lunettes,
\n
Mais laisser reprendre
Pour mieux réapprendre
Le feu qu’on veut étouffer,
La lueur cachée.

5.
Ne pas laisser faire
Les petits bouts de fer
Alourdissent nos poignets
Nos petites menottes
\n
Je sais que tu sais
Qu’on est mesquin mais
Malgré le petit de nous
Veiller jusqu’au bout.