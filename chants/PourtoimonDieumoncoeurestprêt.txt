1.
Pour toi, mon Dieu, mon coeur est prêt ;
Mon coeur est prêt à te louer.
POUR TOI, MON DIEU, MON COEUR EST PRÊT.
Je veux jouer, chanter des hymnes,
Te rendre gloire dès l’aurore.

R.
TON AMOUR EST PLUS GRAND QUE LES CIEUX,
TA VÉRITÉ, PLUS HAUTE QUE LES NUES !

2.
Avec Jésus mon coeur est prêt,
Mon coeur est prêt à te prier.
AVEC JÉSUS MON COEUR EST PRÊT.
Je veux crier que tu nous sauves,
Te supplier pour tous les hommes.

3.
Dans l’Esprit Saint mon coeur est prêt,
Mon coeur est prêt à te nommer.
DANS L’ESPRIT SAINT MON COEUR EST PRÊT.
Je veux bénir ton nom de Père,
Te rendre grâce avec ton Peuple.