TU DONNES UNE LUMIÈRE
QUI NE S’ÉTEINT JAMAIS
TU OFFRES UNE PAROLE
QUI VIENT NOUS DÉLIVRER
QUAND NOS COEURS DÉSESPÈRENT
ET BRISENT L’AMITIÉ
TON PARDON NOUS RELÈVE
ET VIENT TOUT ÉCLAIRER.

1.
Tes mots, Jésus
Nous brûlent de tendresse
Ils bousculent nos vies
Et secouent leur paresse
Tu oses dire à pleine voix
La vérité qui nous redresse
Seigneur je viens vers toi.

2.
Ton coeur, Jésus
Est pesant de tristesse
C’est à Gethsémani
Que chacun te délaisse
Mais la confiance c’est ton choix
Devant la nuit que tu traverses
Seigneur je viens vers toi.

3.
La nuit, Jésus
Fait place à la lumière
Comme le fils perdu
Éloigné de son père
Voici qu’il revient dans ses bras
Avec le poids de sa misère
Seigneur je viens vers toi.

4.
Mes mains, Jésus
Se tendent avec confiance
L’amour que j’ai rompu
Connaît bien des offenses
Je me présente devant toi
J’ai faim de ta présence
Seigneur, c’est toi la joie.