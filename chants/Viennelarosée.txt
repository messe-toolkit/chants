1.
Vienne la rosée sur la terre,
naisse l'espérance en nos cœurs ;
Brille dans la nuit la lumière,
bientôt va germer le Sauveur.
Au désert un cri s'élève :
« préparez les voies du Seigneur. »

2.
Berger d'Israël, tends l'oreille,
descends vite à notre secours,
Et nos yeux verront tes merveilles,
nos voix chanteront ton amour.
Fille de Sion, trésaille,
le Seigneur déjà vient vers toi.

3.
Réveille, ô Seigneur, ta vaillance,
établis ton règne de paix,
Que les peuples voient ta puissance,
acclament ton nom à jamais.
L'univers attend ta gloire,
et nous préparons ton retour.