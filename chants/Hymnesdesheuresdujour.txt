1.
Voici le temps, Esprit très saint,
Où, dans le cœur de tes fidèles,
Uni au Père et à son Fils,
Tu viens répandre ta lumière.
Que notre langue et notre cœur,
Que notre vie, que notre force,
S'enflamment de ta charité
Pour tous les hommes que tu aimes.

Exauce-nous, ô Tout-Puissant
Par Jésus-Christ ton Fils unique
Qui règne avec le Saint-Esprit
Depuis toujours et dans les siècles.

2.
Le jour est dans tout son éclat,
La terre est pleine de ta gloire.
Nous t'adorons, ô Dieu puissant,
Dans la splendeur de ta lumière

Éteins la flamme du péché
Et les ardeurs de la colère,
Emplis nos cœurs de ton amour,
Et que ta paix nous réunisse.

3.
Ô toi qui es sans changement,
Seigneur du temps, ô Dieu fidèle,
Le jour décline, le soir vient,
Rassemble-nous en ta garde.

Accorde-nous la vie sans fin
Et la vieillesse sans ténèbres,
Fais que, le jour de ton retour,
Ta gloire enfin nous illumine.