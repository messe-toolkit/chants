Seigneur faites de moi un instrument de la paix.

1.
Là où il y a de la haine que je mette l’amour
Là où est l’offense que je mette le pardon
Là où est la discorde que je mette l’union
Là où sont les ténèbres que je mette la lumière.

SEIGNEUR, SEIGNEUR,
DONNE-NOUS TON ESPRIT D’AMOUR.
SEIGNEUR, SEIGNEUR,
DONNE-NOUS TON ESPRIT,
DONNE-NOUS TON ESPRIT D’AMOUR.

2.
Là où est la tristesse que je mette la joie
Là où est le mensonge que je mette la vérité
Là où est le doute que je mette la foi
Là où est le désespoir que je mette l’espérance.

Seigneur faites de moi un instrument de la paix.