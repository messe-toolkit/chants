Prière pénitentielle
Seigneur, accorde-nous ton pardon.
Nous avons péché contre toi.
Montre-nous ta miséricorde.
Et nous serons sauvés.
Que Dieu tout-puissant nous fasse miséricorde ;
qu'il nous pardonne nos péchés
et nous conduise à la vie éternelle.
Amen !
Seigneur, prends pitié.
Seigneur, prends pitié.
Ô Christ, prends pitié.
Ô Christ, prends pitié.
Seigneur, prends pitié.
Seigneur, prends pitié.

Gloire à Dieu
Gloire à Dieu, au plus haut des cieux,
Et paix sur la terre aux hommes qu’il aime.
Nous te louons, nous te bénissons, nous t’adorons,
Nous te glorifions, nous te rendons grâce,
pour ton immense gloire,
Seigneur Dieu, Roi du ciel,
Dieu le Père tout-puissant.
Seigneur, Fils unique, Jésus Christ,
Seigneur Dieu, Agneau de Dieu, le Fils du Père ;
Toi qui enlèves le péché du monde, prends pitié de nous ;
Toi qui enlèves le péché du monde, reçois notre prière ;
Toi qui es assis à la droite du Père, prends pitié de nous.
Car toi seul es saint,
Toi seul es Seigneur,
Toi seul es le Très-Haut :
Jésus Christ, avec le Saint-Esprit
Dans la gloire de Dieu le Père.
Amen.

Saint ! Saint ! Saint, le Seigneur
Saint ! Saint ! Saint, le Seigneur, Dieu de l'univers !
Le ciel et la terre sont remplis de ta gloire.
Hosanna au plus haut des cieux.
Béni soit celui qui vient au nom du Seigneur.
Hosanna, hosanna au plus haut des cieux.
Hosanna au plus haut des cieux.

Anamnèse
Il est grand le mystère de la foi :
Nous proclamons ta mort, Seigneur Jésus,
Nous célébrons ta résurrection,
Nous attendons ta venue dans la gloire.

Doxologie
Par lui, avec lui et en lui, Amen,
À toi, Dieu le Père tout-puissant,
dans l’unité du Saint-Esprit, Amen,
Tout honneur et toute gloire,
pour les siècles des siècles, Amen.

Agneau de Dieu
Agneau de Dieu, qui enlèves le péché du monde,
prends pitié de nous, prends pitié de nous.
Agneau de Dieu, qui enlèves le péché du monde,
prends pitié de nous, prends pitié de nous.
Agneau de Dieu, qui enlèves le péché du monde,
donne-nous la paix, donne-nous la paix !