R.
Ô mystère ineffable !
Dieu se fait l'un de nous !

1.
Le bœuf et l'âne ont reconnu le Seigneur, couché dans leur crèche :
Un nouveau-né, enveloppé de langes.

2.
Vierge sainte, bienheureuse Marie, tu es bénie entre toutes les femmes,
Car tu as enfanté aujourd'hui Jésus, le Sauveur du monde.

3.
Rendons gloire au Père tout-puissant, à son Fils, Jésus-Christ, le Seigneur,
À l'Esprit qui habite en nos cœurs, Alléluia, Alléluia.