R.
Voici que l’ange Gabriel, devant la Vierge est apparu,
De toi va naître un enfant Dieu,
Et tu l’appelleras Jésus.

1.
De mon Seigneur j’ai tout reçu,
Je l’ai servi jusqu’à ce jour,
Qu’il fasse en moi sa volonté,
Je m’abandonne à son amour.

2.
Et Dieu se fit petit enfant,
La Vierge lui donna son corps,
Il connut toute notre vie,
Nos humbles joies et notre mort!

F.
Et son nom est Emmanuel.