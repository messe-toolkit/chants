ANTIENNE
Libre, tu t'éveilles, premier-né d'entre les morts !
De ton cœur naît l'Église à l'image de Dieu,
Car toi seul tu baptises dans l'Esprit et le feu, alléluia ! Alléluia !

68.
Béni soit le Seigneur, le Dieu d'Israël,
Qui visite et rachète son peuple.

69.
Il a fait surgir la force qui nous sauve
Dans la maison de David, son serviteur,

70.
Comme il l'avait dit par la bouche de saints,
Par ses prophètes, depuis les temps anciens :

71.
Salut qui nous arrache à l'ennemi
À la main de tous les oppresseurs,

72.
Amour qu'il montre envers nos pères,
Mémoire de son alliance sainte,

73.
Serment juré à notre père Abraham
De nous rendre sans crainte,

74.

74.n que délivrés de la main des ennemis,
75.
Nous le servions dans la justice et la sainteté,
En sa présence, tout au long de nos jours.

76.
Et toi, petit enfant, tu seras appelé
Prophète du Très-Haut :
Tu marcheras devant, à la face du Seigneur,
Et tu prépareras ses chemins.

77.
Pour donner à son peuple de connaître le salut
Par la rémission de ses péchés,

78.
Grâce à la tendresse, à l'amour de notre Dieu,
Quand nous visite l'astre d'en-haut,

79.
Pour illuminer ceux qui habitent les ténèbres
Et l'ombre de la mort,
Pour conduire nos pas
Au chemin de la paix.
Rendons gloire…