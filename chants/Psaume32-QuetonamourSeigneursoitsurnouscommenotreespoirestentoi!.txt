QUE TON AMOUR, SEIGNEUR, SOIT SUR NOUS
COMME NOTRE ESPOIR EST EN TOI !

1.
Oui, elle est droite, la parole du Seigneur ;
il est fidèle en tout ce qu’il fait.
Il aime le bon droit et la justice ;
la terre est remplie de son amour.

2.
Dieu veille sur ceux qui le craignent,
qui mettent leur espoir en son amour,
pour les délivrer de la mort,
les garder en vie aux jours de famine.

3.
Nous attendons notre vie du Seigneur :
il est pour nous un appui, un bouclier.
Que ton amour, Seigneur, soit sur nous
comme notre espoir est en toi !

ALLÉLUIA. ALLÉLUIA.

Le Fils de l’homme est venu pour servir,
et donner sa vie en rançon pour la multitude.