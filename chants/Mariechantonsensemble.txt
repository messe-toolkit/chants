1.
Marie, si disponible au nom du Père,
Fais-nous entrer dans la prière
De celui qui est dans “les cieux” !
Marie mets sur nos pas cette lumière
Pour aller rencontrer nos frères,
Pour aller au devant de Dieu,
Alléluia !

Refrain :
Marie, chantons ensemble ses merveilles,
Puisque sur nous son Esprit veille,
Puisque Jésus demeure en nous,
Magnificat ! Bénie sois-tu, Marie !

2.
Marie, si attentive à notre monde,
Fais que notre vie soit féconde
Pour avec toi, porter du fruit !
Marie, Sa bonté reste sans pareille :
Il a fait pour nous des merveilles ;
Laissons-nous guider par l’Esprit,
Alléluia !

3.
Marie, toi qui comprends notre fatigue,
Fais que nos coeurs d’enfant prodigue
Soient accueillants à son pardon !
Marie, puisque c’est Lui qui nous fait vivre,
Rends-nous capables de Le suivre
Et d’annoncer partout Son nom,
Alléluia !