1.
Pour le souffle de Dieu qui planait sur les eaux,
Dès le commencement.
Pour l'astre du matin et pour ceux de la nuit,
Fixés au firmament.
Pour le feu la lumière et aussi pour le froid,
Pour l'eau et pour le vent
Tout nous parle de toi.

R.
Laudato si', Laudato si',
Laudato si', Laudato si'.

2.
Pour tous les océans qui dessinent la Terre,
Du levant au couchant.
Pour la montagne fière et les vastes vallées,
Les forêts et les champs.
Pour la fleur en bouton le jardin qui verdoie,
Pour le mil et le blé tout nous parle de toi.

R.
Laudato si', Laudato si',
Laudato si', Laudato si'.

3.
Pour les bêtes de l'eau de la terre ou du ciel,
Ce grouillement vivant.
Pour l'homme et pour la femme que tu fis s'élever,
Et pour tous leurs enfants.
Quand ils disent l'amour quand ils tendent les bras,
Comme des frères et sœurs, ils nous parlent de toi.

R.
Laudato si', Laudato si',
Laudato si', Laudato si'.

4.
Pour la maison bâtie par les humbles de cœur,
Qui abrite chacun.
Les artisans de paix d'une planète bleue,
Où tout homme est voisin.
Dans l'espoir de ce jour qui bientôt lèvera,
Où ici et ailleurs, on te reconnaîtra.

R.
Laudato si', Laudato si',
Laudato si', Laudato si'. (bis)

Sois loué Dieu créateur
Joie du ciel, joie dans nos cœurs