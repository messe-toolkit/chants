1.
Torrent de lumière,
Viens me visiter
Toi la source de la joie,
Viens me vivifier.
C´est toi que je cherche
Pour te ressembler,
Jésus, mon Sauveur,
Mon Seigneur, et mon Dieu !

2.
Flamme purifiante,
Brûle mon péché
Et viens transformer mon cœur,
Dans l´humilité.
Mets en moi le feu
De ta charité,
Jésus, mon Sauveur,
Mon Seigneur, et mon Dieu !

3.
Amour qui pardonnes,
Baume sur mes plaies,
Apprends-moi l´offrande
De ma pauvreté.
Cœur plein de tendresse,
Donne-moi ta paix,
Jésus, mon Sauveur,
Mon Seigneur, et mon Dieu !

4.
J´aime ta parole,
Je connais ta voix,
Apprends-moi ta volonté,
Fais grandir ma foi.
Montre ta sagesse,
Eclaire mes pas,
Jésus, mon Sauveur,
Mon Seigneur, et mon Dieu !