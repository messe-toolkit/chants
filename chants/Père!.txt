PÈRE, PÈRE, ME VOICI DEVANT TOI.
PÈRE, PÈRE, ACCUEILLE-MOI.
Père, Abba, Père, me voici devant toi.
Père, Abba, Père, ô Père, accueille-moi.

1.
Daigne attirer mon âme, en ton coeur fou d’amour,
Car ma vie te réclame, et la nuit et le jour.
Tu assures mon pas, Toi l’infinie Bonté.
Je me jette en tes bras pour me laisser aimer.

2.
Ô Trésor de tendresse, ô véritable Amour,
Toi l’infinie richesse, tu te penches sur nous.
Ô Puissance éternelle, ô Douceur ineffable,
Pénètre tout mon être, prends possession de moi.

3.
Tout ce qui m’appartient, fais-le tien pour toujours.
Sois mon unique Bien, et prends-moi sans retour.
Fais en moi ta demeure, en moi viens habiter.
Viens posséder mon coeur, mon âme consacrée.

4.
En ta douce présence, j’aime me reposer,
Partager ton silence, et me laisser aimer.
Garde-moi sur ton coeur, toute l’éternité
Seigneur, mon seul bonheur, c’est toi Père adoré !