ANTIENNE
Ah ! Qu'ils sont beaux sur la montagne
Les pas de ceux qui portent le Bonne-Nouvelle
Qui annoncent le salut et la paix.

1.
Tout pouvoir m'a été donné au ciel et sur la terre.
De toutes les nations faites des disciples
Et moi je suis avec vous tous les jours jusqu'à la fin des temps.

2.
Proclamer l'Évangile du salut à tous les hommes.
Ouvrez-lui votre cœur, le Royaume est proche
Et moi je suis avec vous tous les jours jusqu'à la fin des temps.

3.
Annoncez aux captifs la liberté, la joie aux pauvres.
Vous serez mes témoins sur la terre entière.
Et moi je suis avec vous tous les jours jusqu'à la fin des temps.