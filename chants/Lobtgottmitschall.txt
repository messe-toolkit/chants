Lobt Gott mit Schall, ihr Heiden all, ihr Völker preist den Herren !
Sein Gnad und Gunst wallt über uns, er hilft von Herzen gerne.
Was er verspricht, das trüget nicht, ewig sein Wort wird bleiben ;
Mit Fröhlichem Mund, von Herzensgrund, singen wir zu allen Zeiten :
Alleluia, alleluia mit Freuden
Alles was Odem hat lobe den Herrn
Alleluia, alleluia, alleluia, amen.

Chantez le Seigneur, louez son nom, tous les hommes, fêtez-le !
C’est lui notre Dieu, il nous a faits, rendons-lui gloire à jamais,
car éternel est son amour, il est fidèle à jamais.
Criez votre joie à pleine voix, acclamez ensemble votre Dieu.
Alléluia, alléluia, louez Dieu
Vous tous les hommes, fêtez le Seigneur
Alléluia, alléluia, alléluia, amen.