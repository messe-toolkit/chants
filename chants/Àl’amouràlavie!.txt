R.
LÀ OU FOISONNE LA VIOLENCE
TOUT BRÛLER N’EST D’AUCUN SECOURS
IL RESTE UNE DERNIÈRE CHANCE :
AJOUTER L’AMOUR À L’AMOUR
CAR LES FILS DE LA PAIX
SONT HEUREUX À JAMAIS!

1.
Tous ceux qui ont écrit l’histoire
Autrement qu’avec des fusils
Font plus de bruit dans nos mémoires
Que les larmes et les cris,
Ou les chants de victoire.

À L’AMOUR, À LA VIE !
À L’AMOUR, À LA VIE !

2.
Ceux qui désarment la colère
Avant qu’elle tourne à la folie,
Déclarent la guerre à la guerre
Et la paix se construit
Lentement pierre à pierre…

CAR LES FILS DE LA PAIX
SONT HEUREUX À JAMAIS.

3.
Des hommes gardent leur sourire
Pour d’autres qui les injurient.
La loi du Talion se déchire
Avec sa tyrannie.
Il est temps de se dire…

À L’AMOUR, À LA VIE !
À L’AMOUR, À LA VIE !