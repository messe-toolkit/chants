Ô je voudrais chanter, Marie, pourquoi je t’aime
Pourquoi ton nom si doux fait tressaillir mon cœur
Et pourquoi la pensée de ta grandeur suprême
Ne saurait à mon âme inspirer de frayeur

Si je te contemplais dans ta sublime gloire
Et surpassant l’éclat de tous les bienheureux
Que je suis ton enfant je ne pourrais le croire
O Marie, devant toi, je baisserais les yeux !…

©Droits d'auteur