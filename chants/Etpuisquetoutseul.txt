Refrain :
Et puisque tout seul on est si peu,
Mais puisqu’à deux on est si nombreux...

1.
Inventons ensemble de plus beaux matins,
L’avenir ressemble à nos chemins...

2.
Qu’éclate la fête au coeur de nos vies,
Bonjour la planète, nous voici !

3.
Effaçons les armes dans nos regards bleus,
Essuyons les larmes de nos yeux !

4.
Que brille une étoile au fond de nos coeurs,
Et hissons la voile du bonheur...