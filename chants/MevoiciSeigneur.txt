R.
Me voici, Seigneur, je viens faire ta volonté.

1.
D'un grand espoir, j'espérai le Seigneur :
il s'est penché vers moi.
Dans ma bouche il a mis un chant nouveau,
une louange à notre Dieu.

2.
Tu ne voulais ni offrande ni sacrifice,
tu as ouvert mes oreilles ;
tu ne demandais ni holocauste ni victime,
alors j'ai dit : « Voici, je viens. »

3.
Dans le livre, est écrit pour moi
ce que tu veux que je fasse.
Mon Dieu, voilà ce que j'aime :
ta loi me tient aux entrailles.

4.
Vois, je ne retiens pas mes lèvres,
Seigneur, tu le sais.
j'ai dit ton amour et ta vérité
à la grande assemblée.