R.
Viens Esprit très Saint toi qui emplis tout l´univers.
Viens en nos cœurs, viens, Esprit du Seigneur.
Viens nous t´attendons.
Viens Esprit très Saint, toi qui emplis tout l´univers,
Viens et révèle-nous les joies du Royaume qui vient.

1.
Esprit de feu, souffle du Dieu Très-Haut et donateur de vie,
Par ta puissance viens saisir nos cœurs, viens nous recréer.

2.
Toi qui connais les mystères de Dieu, Esprit de vérité,
Enseigne-nous, viens et demeure en nous, viens nous éclairer.

3.
Force et douceur, amour et don de Dieu, emplis-nous de ta paix,
Père des pauvres, Esprit consolateur, viens nous relever.