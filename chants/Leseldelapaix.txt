R.
Vous, le sel de la terre, vous, rayons de lumière,
Vous portez en vous l’espoir de vivre en hommes libres
Vous le sel de la paix, le sel de la paix.

1.
Marchez en regardant devant,
Notre nuit s’illumine.
Soyez un peuple de vivants
Qui lentement chemine
Cherchez au gré des traditions
Les vrais chemins de la passion.

2.
Venez, écrivons notre loi,
Ce n’est pas difficile.
Osez, sans comment ni pourquoi,
Vivre de l’Évangile.
Aimez comme on aime vraiment,
En se donnant tout simplement.

3.
Criez de toute votre voix,
Condamnez l’injustice.
Brisez les armes et les croix,
Les chaînes des supplices.
Changez vos cœurs, tendez les mains,
La paix comme l’oiseau revient.

4.
Sortez de vos vieilles maisons,
C’est le temps de la fête.
Levez les yeux vers l’horizon,
C’est le temps des prophètes.
Laissez entrer l’homme nouveau,
Qui ce matin sort du tombeau.