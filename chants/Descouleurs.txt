Refrain :
Des couleurs pour repeindre le monde,
Mille fleurs pour habiller la vie !
Mille fleurs pour dessiner la ronde,
Des couleurs de ton coeur, mon ami !

1.
Couleurs, par milliers, différentes,
Sachant nous dire l’unité ;
Couleurs secrètes, mais présentes,
Car l’important, c’est d’exister.

2.
Couleurs qui s’unissent ensemble
Pour inventer d’autres couleurs ;
Couleurs dont les tons se ressemblent,
Qu’on ne perçoit qu’avec le coeur.

3.
Couleurs pleines de discordance,
Mais qui inventent l’harmonie ;
Couleur qui sait que sa présence
Est mise au service d’autrui.

4.
Couleurs que l’on devine à peine,
L’essentiel échappe à nos yeux ;
Couleurs plus vives que fontaine ;
Seuls les coeurs peuvent voir Dieu !