1.
Je l’ai entendu l’oiseau qui chantait
Quand je passais près de lui sur le chemin (bis)

Chantait-il pour moi
L’oiseau qui chantait ?
Chantait-il avant ? Chantera-t il après ?

Je l’ai entendu l’oiseau qui chantait
Quand je passais près de lui sur le chemin

2.
D’autres entendront-ils cet oiseau chanter
Quand ils passeront ici sur le chemin (bis)

Chantait-il pour eux
L’oiseau qui chantait ?

Chantait-il avant ? Chantera-t il après ?
D’autres entendront-ils cet oiseau chanter
Quand ils passeront ici sur le chemin ?

M’as-tu entendu ? J’ai chanté pour toi
la la la la la la la la la la la