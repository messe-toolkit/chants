1.
Comme un enfant tient la main de sa mère
Insouciant et ravi.
Comme un enfant balbutiant sa prière
Confiant, et même, hardi.

TOUT PRÈS DE VOUS MARIE
JE VOUS SALUE MARIE. (bis)

2.
Comme un enfant dans les pas de sa mère
Parce qu’elle le conduit,
Comme un enfant qui ne sait pas se taire,
Raconte ses ennuis.

3.
Comme un enfant là, tout près de sa mère
Sans scrupule, se confie.
Comme un enfant refermant les paupières
Paisible et sans-souci.