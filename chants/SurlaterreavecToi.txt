R.
SUR LA TERRE AVEC TOI
NOUS VIVONS TES COMBATS,
DIEU PUISSANT, SOIS NOTRE FORCE!
L’HORIZON S’OUVRIRA,
GRANDE PAIX FLEURIRA.

1.
Marcheurs dans la nuit
Nous sommes conduits
Sur les chemins de ton Alliance.
Heureux sur tes pas
Le peuple qui va
Vers le pays de renaissance !

2.
Veilleurs dans la nuit
Nous sommes surpris
Par les lueurs de tant d’étoiles.
Heureux sur tes pas
Celui qui dira :
L’amour éclaire notre histoire !

3.
Debout dans la nuit
Ton peuple choisit
De croire à l’aube de la Pâque.
Heureux sur tes pas
Celui qui prendra
Le pain donné pour le partage !