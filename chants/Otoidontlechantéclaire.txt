1.
O toi dont le chant éclaire
Le commencement du monde,
Jésus, Verbe de vie,
Sous le voile de la nuée,
Aujourd’hui tout resplendit
De ta lumière.
En toi l’univers s’offre au Père :
Prends-nous dans ta louange,
Maintiens-nous dans ta clarté.

2.
O toi dont les mots rejoignent
Le balbutiement des notres,
Jésus, Verbe fait chair,
La parole de vérité
S’accomplit à découvert
Sur la montagne.
Élie et Moïse témoignent :
La loi et les prophètes

3.
O toi dont les pas conduisent
Le cheminement de l’homme,
Jésus, Maître du temps,
Ton visage d’éternité
Laisse voir pour un instant
La joie promise.
Dieu parle, invitant les disciplese :
«Voici celui que j’aime,

4.
O toi dont la joie annonce
Le huitième jour du monde ,
Jésus, Christ et Seigneur,
Librement tu t’es engagé
Sur la voie du serviteur
Mourant dans l’ombre.
L’amour a donné sa réponse :
Ton corps se transfigure
Et tiens tout dans sa clarté.