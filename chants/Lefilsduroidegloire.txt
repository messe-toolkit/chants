1.
Le Fils du Roi de gloire est descendu des cieux ;
Que nos chants de victoire résonnent en ces lieux !
Il dompte les enfers, il calme nos alarmes.
Du monde il veut briser les fers
Et pour jamais lui rend la paix.
Ne versons plus de larmes.

2.
Des cieux, le Roi, le Maître, pour nous se fait petit ;
Et pauvre il vient de naître dans un obscur réduit.
Ô l’indicible amour, rendons-lui tous hommage,
Chantons sa gloire et qu’en retour
Au doux Sauveur soit notre cœur,
Sans terme et sans partage.

3.
L’amour seul l’a fait naître pour le salut de tous ;
Il fait par là connaître ce qu’il attend de nous.
Tout notre cœur aimant et notre âme ravie,
Aimons ce cher enfant vraiment
Dès aujourd’hui soyons à lui
Par toute notre vie.