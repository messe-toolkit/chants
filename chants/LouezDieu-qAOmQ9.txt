R.
Louez Dieu, louez Dieu,
Louez Dieu pour sa grandeur
Louez Dieu, louez Dieu,
Par Jésus-Christ, notre Seigneur

1.
Venez chantez pour le Seigneur notre Dieu
Chantez et bénissez son Nom.
De jour en jour, proclamez son salut,
Annoncez partout ses merveilles.

2.
Il est grand le Seigneur le Dieu tout puissant
Lui qui fit la terre et les cieux.
Tout fut par lui, splendeur et majesté.
Devant lui chacun se prosterne.

3.
Jouez pour notre Dieu sur les instruments,
Criez de joie, dansez pour lui,
De tout votre art, soutenez l´ovation,
Offrez-lui vos chants de louange.

4.
Notre âme attend le Seigneur notre secours,
En lui la joie de notre cœur.
Que ton amour, Ô Seigneur soit sur nous,
En toi notre espoir, notre force !