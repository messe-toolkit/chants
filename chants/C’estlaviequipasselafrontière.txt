1.
Nous partions rouler le poids du jour
Pour embaumer l’Esprit d’un frère,
Mais au matin sur l’aube claire
C’est le Seigneur qui fait lumière !
Nous partions rouler le poids du jour.

R.
C’EST LA VIE QUI PASSE LA FRONTIÈRE,
QUI RÉVEILLE LA TERRE.
LA MORT EST MORTE, DÉCHIRÉE
PAR LA CROIX DE NOS PÂQUES.

2.
Nous restions cloués sur notre peur,
Barricadés au fond des peines,
Quand au plus froid du soir qui tombe,
Voici venir la joie du monde !
Nous restions cloués sur notre peur.

3.
Nous parlions du vide sous nos pas,
D’un Dieu qui laisse aller la terre.
Voici cet homme qui s’étonne
Et rompt le pain comme il se donne !
Nous parlions du vide sous nos pas.

4.
Notre Dieu se vit au temps présent,
L’éternité est à écrire.
Nous n’avons plus de peine à croire,
Ce jour suffit à notre histoire !
Notre Dieu se vit au temps présent.