R.
Dans la Maison de notre Père
Jésus t'a préparé la place ;
Par son Esprit tu vas renaître
Sous le soleil du jour de Pâques.

1.
Qui peut savoir où tu t'en vas
Sinon Jésus le vrai Chemin ?
Il est cet homme pèlerin
Qui te reçoit sur le rivage.

2.
As-tu fait l'oeuvre que Dieu fait ?
Tu connaîtrais la Vérité.
Tu es toi-même l'invité
Au grand festin de la lumière.

3.
Si tu es mort avec Jésus
Tu apprendras quelle est sa Vie.
Il a passé Gethsémani
Avant la joie qui transfigure.

4.
Toi qui pars seul vers l'inconnu,
Entends la foule des amis.
Le monde est vaste au paradis,
C'est tout un peuple qui exulte.