PUISSANCE, HONNEUR ET GLOIRE
À L’AGNEAU DE DIEU !

1.
Tu es digne, Seigneur notre Dieu,
de recevoir
l’honneur, la gloire et la puissance.

2.
C’est toi qui créas l’univers ;
tu as voulu qu’il soit :
il fut créé.

3.
Tu es digne, Christ et Seigneur,
de prendre le livre
et d’en ouvrir les sceaux.

4.
Car tu fus immolé,
rachetant pour Dieu, au prix de ton sang,
des hommes de toute tribu,
langue, peuple et nation.

5.
Tu as fait de nous, pour notre Dieu,
un royaume et des prêtres,
et nous régnerons sur la terre.

6.
Il est digne, l’agneau immolé,
de recevoir puissance et richesse,
sagesse et force,
honneur, gloire et louange.