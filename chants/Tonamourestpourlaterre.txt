Ton amour est pour la terre et pour ton peuple libéré ;
Tu n’es pas Dieu de colère, tu nous sauves du péché.

1.
Vers toi, Dieu Sauveur, fais-nous revenir,
Tu n’es pas un Dieu de vengeance ;
Viens toi-même nous rendre la vie,
Viens enfanter la joie des renaissances.

2.
Vers toi, Dieu d’amour, fais-nous revenir,
Montre-toi le Dieu de tendresse ;
Viens connaître l’humain de nos vies,
Viens partager la mort qui nous agresse.

3.
Vers toi, Dieu de paix, fais-nous revenir,
Montre-toi le Dieu qui nous sauve ;
Viens nous dire la paix de ta vie,
Viens réveiller justice au cœur des hommes.

4.
Dieu vivant, fais-nous revenir,
Que ta gloire habite nos terres ;
Viens semer ton amour en nos vies,
Viens ranimer ton germe de lumière.