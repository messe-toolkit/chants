VIENS, SEIGNEUR, ET SAUVE-NOUS !

1.
Le Seigneur fait justice aux opprimés,
aux affamés, il donne le pain,
le Seigneur délie les enchaînés.

2.
Le Seigneur ouvre les yeux des aveugles,
le Seigneur redresse les accablés,
le Seigneur aime les justes.

3.
Le Seigneur protège l’étranger,
il soutient la veuve et l’orphelin.
D’âge en âge, le Seigneur régnera.