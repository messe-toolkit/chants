ALLÉLUIA, LOUEZ DIEU
DE LA TERRE JUSQU’AUX CIEUX
ALLÉLUIA, LOUEZ DIEU
BÉNISSEZ-LE, ACCLAMEZ-LE !

1.
Pour les montagnes et l’océan
Bénissez son Nom
Pour les étoiles et le soleil.
Bénissez son Nom.

2.
Pour les oiseaux et les brebis
Bénissez son Nom
Pour tous les peuples de la terre
Bénissez son Nom.