1.
Seigneur de bonté,
Amour incarné
Ecoute nos prières ;
Dissipe nos angoisses,
Que ta parole nous libère.

2.
Seigneur le seul Saint,
La source du bien,
Ecoute nos prières ;
Pardonne toute faute ;
Purifie-nous de nos misères.

3.
Seigneur, Vérité
A tous révélée,
Ecoute nos prières ;
Flambeau sur notre route,
Ton Evangile nous éclaire.

4.
Seigneur, notre foi,
Sauveur par ta croix,
Ecoute nos prières ;
Et donne-nous l’audace,
De témoigner pour Toi sur terre.