1.
Une nuit sans visage
A couvert leurs cris.
Au bout de leur voyage,
La parole est bannie.
En Toi, Seigneur, leur espoir,
En Toi qui n’as rien dit.

2.
La raison en otage
Est saisie d’effroi.
À l’heure du passage,
Seule est forte la foi.
Vers Toi, Seigneur, leur regard,
Vers Toi qui meurs en croix.

3.
Et le mal sans courage
Montre sa folie.
En l’homme on tue l’image
De ce Dieu qu’on renie.
À Toi, Seigneur, toute mort,
À Toi qui es la vie.

4.
Avec Toi leur visage
Traverse le temps.
Leur voix surgit des pages
Que retourne le vent.
Pour Toi, Seigneur, aujourd’hui,
La gloire de leur chant !