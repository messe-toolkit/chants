1.
Approchons-nous de la table
où le Christ va s’offrir parmi nous,
Donnons-lui ce que nous sommes,
car le Christ va nous transformer en lui.

2.
Voici l’admirable échange
où le Christ prend sur lui nos péchés,
Mettons-nous en sa présence,
Il nous revêt de sa dignité.

3.
Père nous te rendons grâce
pour ton Fils Jésus-Christ le Seigneur,
Par ton Esprit de puissance,
rends-nous dignes de vivre de tes dons.

4.
Voici le temps favorable,
le royaume est déjà parmi nous,
Pourquoi s’attarder en route,
car les champs sont blancs pour la moisson.