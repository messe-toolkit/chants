1.
Ô Jésus, condamné à mort :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !

2.
Ô Jésus, chargé de la croix :
Sois béni, sois béni !

3.
Ô Jésus, tu tombes sous le bois :
Sois béni, sois béni !

4.
Ô Jésus, tu rencontres ta mère :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !

5.
Ô Jésus, aidé par Simon :
Sois béni, sois béni !

6.
Ô Jésus, une femme t'essuie la Face :
Sois béni, sois béni !

7.
Ô Jésus, meurtri par le bois :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !

8.
Jésus, tu combles les filles d'Israël :
Sois béni, sois béni !

9.
Ô Jésus, tu tombes encore sous la Croix :
Sois béni, sois béni !

10.
Ô Jésus, ils t'arrachent les vêtements :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !

11.
Ô Jésus, cloué sur la Croix :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !

12.
Ô jésus, tu meurs pour nous :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !

13.
Ô Jésus, remis à ta Mère :
Sois béni, sois béni !

14.
Ô Jésus, tu reposes dans la tombe :
Sois béni, sois béni !

15.
Ô Jésus, tu as vaincu la mort :
Sois béni, adoré !
Tu as sauvé le monde par ta Croix !