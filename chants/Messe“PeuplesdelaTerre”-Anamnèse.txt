FRANÇAIS

Il est grand le mystère de la foi.

1.
Nous proclamons ta mort, Seigneur Jésus,
Amen, amen ! AMEN, AMEN !

2.
Nous célébrons ta résurrection,
Amen, amen ! AMEN, AMEN !

3.
notre Sauveur et notre Dieu, viens, Seigneur Jésus !
Amen, amen ! AMEN, AMEN !

LATIN

1.
Mortem tuam annuntiamus, Domine,
Amen, amen ! AMEN, AMEN !

2.
et tuam resurrectionem confitemur,
Amen, amen ! AMEN, AMEN !

3.
donec venias,
Amen, amen ! AMEN, AMEN !