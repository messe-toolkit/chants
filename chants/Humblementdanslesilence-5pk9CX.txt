R.
Humblement, dans le silence de mon cœur,
Je me donne à toi, mon Seigneur.

1.
Par ton amour, fais-moi demeurer
Humble et petit devant toi.

2.
Entre tes mains, je remets ma vie,
Ma volonté, tout mon être.

3.
Enseigne-moi ta sagesse, Ô Dieu,
Viens habiter mon silence.

4.
Je porte en moi ce besoin d’amour,
De me donner, de me livrer, sans retour.

5.
Vierge Marie, garde mon chemin
Dans l’abandon, la confiance de l’amour.