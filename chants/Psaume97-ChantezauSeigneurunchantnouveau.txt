R.
Chantons Noël, alléluia : gloire à l'Emmanuel, alléluia !
Chantons Noël, alléluia : gloire à l'Emmanuel, alléluia !

1.
Chantez au Seigneur un chant nouveau,
Car il a fait des merveilles ;
Par son bras très saint, par sa main puissante,
Il s'est assuré la victoire.

2.
Le Seigneur a fait connaître sa victoire
Et révélé sa justice aux nations ;
Il s'est rappelé sa fidélité, son amour,
En faveur de la maison d'Israël.

3.
La terre tout entière a vu
La victoire de notre Dieu.
Acclamez le Seigneur, terre entière,
Sonnez, chantez, jouez !

4.
Jouez pour le Seigneur sur la cithare,
Sur la cithare et tous les instruments ;
Au son de la trompette et du cor,
Acclamez votre roi, le Seigneur !