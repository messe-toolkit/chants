Seigneur Jésus, envoyé pour guérir
les coeurs qui reviennent vers toi :
Seigneur, prends pitié.
SEIGNEUR, PRENDS PITIÉ.

Ô Christ, venu appeler les pécheurs :
ô Christ, prends pitié.
Ô CHRIST, PRENDS PITIÉ.

Seigneur, qui sièges à la droite du Père
où tu intercèdes pour nous :
Seigneur, prends pitié.
SEIGNEUR, PRENDS PITIÉ.