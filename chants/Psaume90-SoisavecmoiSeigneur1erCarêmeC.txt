SOIS AVEC MOI, SEIGNEUR,
DANS MON ÉPREUVE.

1.
Quand je me tiens sous l’abri du Très-Haut
et repose à l’ombre du Puissant,
je dis au Seigneur : « Mon refuge,
mon rempart, mon Dieu, dont je suis sûr ! »

2.
Le malheur ne pourra te toucher,
ni le danger, approcher de ta demeure :
il donne mission à ses anges
de te garder sur tous tes chemins.

3.
Ils te porteront sur leurs mains
pour que ton pied ne heurte les pierres ;
tu marcheras sur la vipère et le scorpion,
tu écraseras le lion et le Dragon.

4.
« Puisqu’il s’attache à moi, je le délivre ;
je le défends, car il connaît mon nom.
Il m’appelle, et moi, je lui réponds ;
je suis avec lui dans son épreuve. »