R.
Sur nos sentiers de fils pécheurs
Viens nous chercher, toi, le Seigneur,
Fais-nous marcher vers ta maison.

24ème dimanche ordinaire C :

1.
Lequel de nous n'est ce berger qui laisse tout pour retrouver
Sa brebis jeune au loin perdue ?
Qui d'entre nous n'est pas aussi le fugitif ou la brebis
Que le Seigneur a libéré ?
Qui ne voudrait donner au ciel un jour de rire sans pareil
En allumant un repentir ?

24ème dimanche ordinaire C :

2.
Quel est celui dans sa maison qui n'a trouvé mille façons
De recouvrer ses biens perdus ?
Qui d´entre nous n'est pas aussi comme une perle de grand prix
Dans la demeure du Dieu bon ?
Qui ne voudrait connaître au ciel trésor d'amour et de soleil
Transfigurant son avenir ?

24ème dimanche ordinaire C :

3.
Qui d'entre nous n'est pas ce fils tournant le dos à son pays
Pour s"enivrer de l"inconnu ?
Qui d'entre nous n'est retourné avec un cœur déshérité
Vers la maison qu'il avait fuie ?
Qui n'a trouvé devant le seuil la joie d'un père sans pareil
Face au prodigue repenti ?

24ème dimanche ordinaire C :

4.
Qui d'entre nous n'a blasphémé avec les mots du fils aîné
Quand notre frère est revenu ?
Mais qui de nous saura mourir et festoyer dans l'autre vie
Comme Jésus le frère aîné ?
Qui portera la joie du ciel sur notre terre de réveil
Si le pardon n'est pas pour lui ?

15ème dimanche ordinaire C :

5.
Lequel de nous est le prochain de l'étranger sur le chemin
Blessé à mort par nos cœurs durs ?
Qui d'entre nous n"est pas aussi le moribond qui peut guérir
Quand Dieu se fait Samaritain ?
Qui donc vivra la loi du ciel sinon le fils de l'Éternel
En qui l'amour vaincra la nuit ?