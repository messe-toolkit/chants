Stance
A tes disciples,
tu livres le secret:
Le Fils du Dieu vivant
marche vers la mort,
le Maître de l'univers
est le serviteur de tous.

Refrain
Conduis-nous, Seigneur,
sur le chemin d'éternité !

Versets
1° Les hommes tueront le Fils de l'homme,
mais il se lèvera d'entre les morts.
2° Si quelqu'un veut être le premier,
qu'il soit le dernier de tous.
3° Celui qui accueille en mon nom un enfant,
c'est moi qu'il accueille.