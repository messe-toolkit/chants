R.
Par ton corps tu nous donnes la vie
Nous te disons merci, Seigneur.

1.
Tu es notre pain véritable
Tu es notre source de vie,
Tu nous as conviés à la Table,
Où tu fais de nous tes amis.

2.
Par ta vie, donnée pour tes frères
Tu nous as montré le chemin.
Guide sur tes voies de lumière
Ceux que tu nourris de ton pain.

3.
Ton repas Seigneur nous rassemble
Nous mangeons le pain de ta Vie.
Fais que nous parlions tous ensemble,
Fort de ton amour, Jésus-Christ.