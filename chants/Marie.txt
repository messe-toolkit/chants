1.
Marie, toute blanche vêtue tu remplis mon regard,
Marie, je voudrais rester là durant toute ma vie,
Marie, les yeux remplis d’espoir.
Marie, un sourire transparaît sur tes lèvres figées,
Marie, la douceur de ta voix est un soleil d’été,
Marie, qui engendre la joie.

MARIE, MÈRE DE DIEU,
MAMAN DE TOUS LES HOMMES
SOIS LUMIÈRE, MOI QUI SUIS LE PÉCHEUR.

2.
Marie, ils sont venus de loin pour se mettre à genoux,
Marie, la tête entre les mains on les prendrait pour fous,
Marie, éclatant de leur foi.
Marie, le malade est venu pour retrouver la vie,
Marie, autrefois il pleurait maintenant il sourit,
Marie, le miracle il est là.

3.
Marie, devant la croix dressée, tu as sûrement pleuré,
Marie, débordante d’amour tu nous as pardonné,
Marie, et nous t’en rendons grâce.
Marie, tu es notre maman à qui l’on se confie,
Marie, tu ne peux supporter ton enfant qui gémit,
Marie, la paix soit avec toi.