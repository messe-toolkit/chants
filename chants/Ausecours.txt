1.
Venise se noie, la banquise s’en va
Ah quel émoi, ah quel émoi
Les glaces sont rompues, les esquimaux fondus
Et nous et nous… et nous aussi
En couscous les agneaux, en steak les chevaux
Pauvres agneaux, pauvres chevaux
Les abeilles enfumées, les éléphants trompés
Et nous et nous… et nous aussi

AU SECOURS NOÉ, AU SECOURS
REVIENS VITE NOUS CHERCHER.
AU SECOURS NOÉ, AU SECOURS
AVEC TON ARCHE VIENS NOUS SAUVER.

2.
Le lion aliéné, tous les poissons piégés
C’est ça pêcher, c’est ça pécher
Les moutons tous tondus, les baleines perdues
Et nous et nous… et nous aussi
On a même arraché les dents de la vipère
Et de la mer et de la mer
Les forêts mises à nue, les sapins abattus,
Et nous et nous… et nous aussi

3.
La panthère à genou, le gnou se tord le cou
Quel mauvais coup, quel mauvais coup
Vois le noyer sans noix, le perroquet sans voix
Et nous et nous… et nous aussi
On a tout arraché, démoli, tout haché
Un vrai gâchis, un vrai gâchis
La terre est abîmée, l’océan va pleurer
Et nous et nous… et nous aussi