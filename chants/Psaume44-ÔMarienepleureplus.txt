O Marie, ne pleure plus : ton Fils notre
Seigneur s’endort dans la paix ; et son
Père, dans la gloire, ouvre les portes
de la vie ! O Marie, réjouis-toi : Jésus,
ressuscité, a vaincu la mort !

Quand tombe la voix de la meule, quand
s’arrête la voix de l’oiseau, tu vois mourir
ton Fils, ton Bien-Aimé, livré au pressoir
de la Croix, et de sa chair, prise au filet de
l’oiseleur, l’Esprit s’est répandu sur l’Eglise.

O Marie, ne pleure plus...

Lorsque se taisent les chansons, et qu’on a
des frayeurs en chemin, tandis que l’homme
s’en va vers sa maison d’éternité, ton fils
a crié : « Père pardonne-leur ! » Et dans
sa chair, clouée sur la croix de souffrance,
s’ouvre un chemin de Pâques pour la vie
éternelle.

O Marie, ne pleure plus...

Au jour où tremblent les gardiens de la
maison, où se courbent les hommes en
pleine force, tandis que s’enténèbrent la
lumière et le soleil, ton Fils s’en va fonder
aux cieux une demeure. Et sa chair, brisée
par le poids du péché, se relève déjà dans la
puissance de l’Esprit.

O Marie, ne pleure plus...