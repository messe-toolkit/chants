ENFANT JÉSUS, QUI T’A FAIT SI PETIT ?
C’EST L’AMOUR ?
ENFANT JÉSUS, QUI T’A FAIT SI PETIT ?
C’EST L’AMOUR.

1.
Joseph et Marie se rendent à Bethléem.
Marie met au monde son premier enfant,
enveloppé dans une couverture,
et couché dans une mangeoire.

2.
Un ange du ciel annonce la nouvelle,
grande joie !
C’est la naissance de Jésus Sauveur.
Un enfant…

3.
Les anges chantent : « Gloria, Gloire à Dieu
au plus haut des cieux !
et paix sur la terre aux hommes qu’il aime.
Gloire à Dieu ! »

4.
Les bergers se rendent à Bethléem.
Ils trouvent Marie, Joseph et l’enfant
enveloppé dans une couverture
et couché dans une mangeoire.