R.
Vers nous ô Marie tu ouvres les bras
Auprès de ton Fils tu guides nos pas.

1.
Tu es là dans nos souffrances
Tu sais nous écouter
Et ton grand cœur de mère
Est prêt à consoler.

2.
Tu es là dans nos angoisses
Tu sais nous rassurer
Ton regard nous invite
À demeurer en paix.

3.
Tu es là dans nos révoltes
Tu sais nous apaiser
Et ta douceur désarme
Les cœurs les plus fermés.