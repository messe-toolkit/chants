R.
Béni soit Dieu maintenant et toujours!

1.
Béni sois-tu, Seigneur, pour ce pain,
Fruit de la terre et du travail des hommes.
Il deviendra le Pain de la Vie.

2.
Béni sois-tu, Seigneur, pour ce vin,
Fruit de la vigne et du travail des hommes.
Il deviendra le Vin du Royaume.