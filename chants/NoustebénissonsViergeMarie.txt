Stance
Nous te bénissons, Vierge Marie, Mère très sainte,
Tu as mis au monde le Sauveur,
Le Fils bien-aimé du Père,
D’âge en âge ton chant nous appelle.

R.
DIEU SE SOUVIENT DE NOUS.
ÉTERNEL EST SON AMOUR.
DIEU SE SOUVIENT DE NOUS.
ÉTERNEL EST SON AMOUR.

Verset 1.
Voyez comme elle a trouvé grâce devant lui l’humble servante du Seigneur,
Marie de Nazareth.
Verset 2.
Voyez comme elle a gardé dans son coeur les merveilles de Dieu,
Marie, Mère de l’Emmanuel.
Verset 3.
Voyez comme elle a suivi son fils, sur le chemin du calvaire,
Marie, Mère de l’espérance.
Verset 4.
Voyez comme elle s’est tenue debout près de la croix,
Marie, Mère des douleurs.
Verset 5.
Voyez comme Jean l’a prise chez lui celle que Jésus lui donnait pour mère,
Marie, au coeur transpercé.
Verset 6.
Voyez comme au milieu des apôtres elle attendait la venue de l’Esprit,
Marie, Mère de l’Église.
Verset 7.
Voyez cette femme dans la gloire du ciel, sa prière hâte le jour du Christ,
Marie, Mère des vivants.
Verset 8.
Voyez comme elle accompagne la marche des pèlerins,
Marie de nos longs chemins.