DEUX YEUX, UNE BOUCHE, DEUX OREILLES
ET UN PETIT NEZ (bis)
LA LA LA LA LA LÈRE, LA LA LA LA LA LA. (bis)

1.
Deux yeux pour voir les merveilles de Dieu (bis)

2.
Une bouche pour chanter les merveilles de Dieu (bis)

3.
Deux oreilles pour écouter la Parole de Dieu (bis)

4.
Un petit nez pour respirer le souffle de Dieu (bis)

5.
Deux mains pour partager, un petit coeur pour aimer.