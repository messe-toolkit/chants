1.
Splendeur jaillie du sein de Dieu,
lumière née de la lumière,
Avant que naisse l'univers,
tu resplendis dans les ténèbres.

2.
Nous t'adorons Fils bien-aimé,
objet de toute complaisance.
Le père qui t'a envoyé
sur toi fait reposer sa grâce.

3.
Tu viens au fond de notre nuit,
pour tous les hommes de ce monde.
Tu es la source de la vie
et la lumière véritable.

4.
Le monde ne te connaît pas,
et les ténèbres te repoussent,
Mais donne à ceux qui t'ont reçu
de partager ta plénitude.