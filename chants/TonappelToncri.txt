« Ton appel. Ton cri.
Tu as broyé ma surdité.
Éclair.
Splendeur.
Tu as fait fuir mon aveuglement.
Parfum. Je t’ai respiré. Je t’ai inhalé.
Je t’ai goûté. Ma faim. Ma soif.
Tu m’as touché. J’ai pris feu dans ta paix. »