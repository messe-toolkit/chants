R.
Dieu plus grand que notre cœur,
Le feu de ta parole nous éclaire.
Dieu plus grand que notre cœur,
La joie de ton pardon nous libère.

1.
Nous avons quitté les chemins de la paix,
Nous revenons vers toi les mains ouvertes.
Nous avons dormi quand il fallait veiller,
Ton amour nous invite à la fête !

2.
Nous avons enfoui les talents de nos vies,
Nous revenons vers toi les mains ouvertes.
Nous fermé la porte aux plus petits,
Ton amour nous invite à la fête !

3.
Nous avons brisé les liens de l'amitié,
Nous revenons vers toi les mains ouvertes.
Nous avons perdu la force d'avancer,
Ton amour nous invite à la fête !

4.
Nous avons fermé nos oreilles et nos yeux,
Nous revenons vers toi les mains ouvertes.
Nos avons nié le cri des malheureux,
Ton amour nous invite à la fête !

5.
Nous avons connu la tristesse et la faim,
Nous revenons vers toi les mains ouvertes.
Nous avons omis de partager le pain,
Ton amour nous invite à la fête !

6.
Nous avons suivi d'autres maîtres que toi,
Nous revenons vers toi les mains ouvertes.
Nous avons péché en refusant ta loi,
Ton amour nous invite à la fête !