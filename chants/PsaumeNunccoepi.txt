Refrain :
Et maintenant, ma vie commence,
Entre tes mains, un nouveau jour.
Plus rien, tu sais, n’a d’importance,
Je m’en remets à ton amour.

1.
Trop d’émotions et trop d’images,
Sont rattrapées par mon passé.
Trop de questions sur les visages,
De quoi demain sera-t-il fait ?

2.
Actes manqués, discours fossiles,
Maisons sans âme et rue sans vie,
Le sens, le vrai, du mot “ utile ”,
Entre tes mains, c’est : “ nunc coepi... ”

3.
Tant de projets sont en jachère,
Tant de défis sont cabossés,
Combien d’amours vont en galère,
Seules tes mains, m’offrent la paix !

4.
Pour ces moments, “ morceaux d’argile ”,
Merci d’abord au vrai potier,
Pour mes bourgeons encore fragiles,
Merci surtout au jardinier !