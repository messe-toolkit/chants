1.
J’apprends le chemin des étoiles
Qui se déroule à l’infini.
J’écoute le chant des cigales,
C’est avec toi que je grandis.

AVEC TOI QUI PRENDS MA MAIN,
JÉSUS MON AMI.
L’ÉVANGILE ME CONDUIT
SUR TON CHEMIN.

2.
J’entends la forêt qui s’éveille,
L’oiseau qui chante dans la nuit.
Tout ce qui est beau m’émerveille,
C’est avec toi que je grandis.

3.
Parfois j’ai le coeur en tempête,
Je fais du mal à mes amis.
Mais quand je reviens faire la fête,
C’est avec toi que je grandis.

4.
Tout ce qui m’écorche et me blesse,
Souvent le soir, je te le dis.
Tu viens m’entourer de tendresse,
C’est avec toi que je grandis.