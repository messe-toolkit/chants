1.
Le Christ dans sa gloire s´avance,
Escorté des anges et des saints.
Vous qui l´aimez faites silence,
Dieu vient demeurer chez les siens.
Il s´abaisse jusqu´à faire
De son propre corps le Pain vivant,
Le Pain vivant.

2.
Répands en nos cœurs ta lumière,
Seigneur, toi qui règnes sans fin.
Fais-nous pénétrer ce mystère,
Dieu va se livrer en nos mains.
Ne regarde pas nos fautes,
Viens nous purifier par l´Esprit Saint,
Par l´Esprit Saint.

3.
Voici rassemblé tout ton peuple
Qui t´offre le pain et le vin.
Père, accueille nos dons très simples
Comme un encens pur, un parfum.
Introduis-nous à la fête,
Puisque tout est prêt.
Alléluia, alléluia ! (bis)