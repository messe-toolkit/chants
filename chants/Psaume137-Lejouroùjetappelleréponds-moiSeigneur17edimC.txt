LE JOUR OÙ JE T’APPELLE,
RÉPONDS-MOI, SEIGNEUR.

1.
De tout mon coeur, Seigneur, je te rends grâce :
tu as entendu les paroles de ma bouche.
Je te chante en présence des anges,
vers ton temple sacré, je me prosterne.

2.
Je rends grâce à ton nom pour ton amour et ta vérité,
car tu élèves, au-dessus de tout, ton nom et ta parole.
Le jour où tu répondis à mon appel,
tu fis grandir en mon âme la force.

3.
Si haut que soit le Seigneur, il voit le plus humble ;
de loin, il reconnaît l’orgueilleux.
Si je marche au milieu des angoisses, tu me fais vivre,
ta main s’abat sur mes ennemis en colère.

4.
Ta droite me rend vainqueur.
Le Seigneur fait tout pour moi !
Seigneur, éternel est ton amour :
n’arrête pas l’oeuvre de tes mains.

ALLÉLUIA. ALLÉLUIA.

Vous avez reçu un Esprit qui fait de vous des fils ;
c’est en lui que nous crions « Abba », Père.