Refrain :
La plus belle lumière,
Le plus grand feu de joie,
Le plus fou des mystères,
C’est ta présence en moi !

1.
Aux vents de nos tempêtes,
Aux peurs des longues nuits,
Aux soirs de nos défaites,
Quand on cherche un ami.

2.
Quand pèse un lourd silence
Aux routes de brouillard,
Quand survient une chance
Trop tôt ou bien trop tard.

3.
Aux jours où l’on retrouve
La table et deux couverts,
Quand soudain on découvre
Un puits dans nos déserts.

4.
Certain que, d’où je vienne,
Où que j’aille, où je sois,
Et que, quoiqu’il advienne,
Tu seras toujours là.