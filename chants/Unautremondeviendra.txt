R.
QUAND NOTRE COEUR AURA TROUVÉ
LE RYTHME DU PARTAGE,
QUAND TOUS LES HOMMES AURONT GAGNÉ
LA TERRE EN HÉRITAGE,
UN AUTRE MONDE VIENDRA,
TOUTE LA VIE CHANGERA!

1.
Ton soleil est fatigué, tu prends le large,
Dans l’usine abandonnée on plie bagages.
Et tu vois défiler d’un seul coup
Tous les rêves qui tombent à genoux,
Dans ta mémoire…

2.
Toi qui viens de l’autre bout de notre terre,
Toi qui viens vivre chez nous, dans la misère,
Donne-moi quelque chose de toi,
Une histoire que je ne connais pas
Et qui me manque !

3.
Nous savons toute la joie de vivre ensemble,
Le pari de l’Espérance nous rassemble,
Les coups durs, les soirées d’amitié,
Solidaires, contre vents et marées,
L’Amour nous mène !