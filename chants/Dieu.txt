1.
Dieu si bon, si grand
Dieu soleil levant
Dieu tu es vivant dans mes journées d'enfant

R.
DIEU TRES BON
DIEU TRES GRAND
DIEU TRES BON
DIEU VIVANT

2.
Dieu, toi notre ami
Dieu, l’esprit de vie
Dieu, tu es ici
Tout près des plus petits

3.
Dieu, l’Amour c’est Toi
Dieu, présent en moi
Dieu, Tu es ma joie
Tu m’ouvres grand tes bras

4.
Dieu, notre bonheur
Dieu, là dans mon coeur
Dieu, tu es Seigneur
Tu apaises mes peurs

5.
Dieu, toi notre paix
Dieu, tu es caché
Dieu, je veux t’aimer
Et c’est notre secret