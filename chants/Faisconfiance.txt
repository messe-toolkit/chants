De tout ton coeur, fais confiance au Seigneur !
Ne t’appuie pas sur ton intelligence.
Reconnais-le, où que tu ailles
Et il guidera tes pas.

Chantons le livre de la Sagesse :
Proverbes, chapitre 3.
Souvenons-nous : Proverbes 3,
Versets 5 et 6