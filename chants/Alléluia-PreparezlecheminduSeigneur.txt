ALLÉLUIA, ALLÉLUIA.

Préparez le chemin du Seigneur,
rendez droits ses sentiers :
tout être vivant verra le salut de Dieu.

ALLÉLUIA.