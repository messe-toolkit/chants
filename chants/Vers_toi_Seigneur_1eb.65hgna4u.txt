1.
Vers toi, Seigneur,
vois nos mains qui s'élèvent.
Un seul chant joyeux a jailli de nos lèvres.
Reçois tous ces jours de travail et de fête,

R.
Royaume de Dieu parmi nous (bis)

2.
Vers toi, Seigneur, nos prières s'élancent.
Transforme nos mains en un chant de louange.
En servant nos frères,
c'est toi qu'elles chantent,

3.
Voici nos mains pour que vienne ton règne.
Voici nos deux mains
pour que change la terre.
Remplis de ta force d'amour tous nos gestes,

4.
Voici le pain et le vin sur la table:
Fais de nous, Seigneur,
le ferment dans la pâte,
Le sel d'une vie fraternelle, amicale,

5.
Prends nous la main quand nos forces s’épuisent.
Père, que ta main aujourd’hui nous conduise
Là où nous verrons se lever la justice,
Royaume de Dieu parmi nous.