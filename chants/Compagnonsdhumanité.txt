Frère, donne-moi la main,
Faisons route ensemble,
À la croisée des chemins
De nos routes d’hommes,
Compagnons d’humanité,
Faisons route ensemble.

Jésus présent sur nos chemins
Au long de deux mille ans d’histoire,
Réveille en nous la joie de croire
Et le désir d’être témoins.

1.
L’espérance n’est pas un rêve
Et nous voyons germer déjà
Dans nos amours, dans nos combats,
Un monde nouveau qui se lève.

2.
Oui nous chantons ces mains tendues
En notre monde d’exclusion,
Leurs gestes d’accueil, de pardon,
Et tant de bontés inconnues.

3.
L’amour dans la fidélité,
L’amour donné, l’amour vécu,
L’accueil offert, l’accueil reçu,
Voilà nos raisons d’espérer.

4.
C’est l’accueil fait aux tout-petits
Qui nous fait vivre sa présence,
Que son esprit, par sa puissance,
Réveille ceux qui croient en lui.

5.
Toi qui fais monter dans la nuit
Ta prière pour tous tes frères,
Tu es là, veilleur avec lui,
Pour que vienne enfin sa lumière.

6.
Toi qui dis non à l’injustice,
Toi qui aimes jusqu’à souffrir,
Toi qui te bats jusqu’à mourir
Pour que tes frères réussissent.

7.
Tu donnes sens à notre vie,
Jésus, en déclarant "heureux"
Ceux qui font la paix autour d’eux,
Ceux qui aiment à la folie.

8.
Tu donnes sens à nos combats,
Jésus, assoiffé de justice,
Jésus en habit de service,
Au moment même où tu t’en vas.

9.
Dieu inattendu, Dieu vivant,
Aux croisées de nos routes d’hommes
Dieu fragile, tu nous attends,
Comme un amour vrai qui se donne.

10.
Fais-nous contempler ta lumière,
Tout visage est un reflet,
Aide-nous à te reconnaître
En chaque frère désormais.