RESTE AVEC NOUS, IL SE FAIT TARD,
LA ROUTE EST LONGUE,
LONGUE, LONGUE, DANS LE SOIR.
RESTE AVEC NOUS, IL SE FAIT TARD,
LA ROUTE EST LONGUE,
LONGUE, LONGUE SANS ESPOIR.

1.
Le temps de l’amitié est communication,
C’est l’échec du savoir, c’est une communion :
C’est un geste, un regard et même une question,
C’est se sentir aimé : une révélation.

2.
Le temps de la Parole où les coeurs sont brûlants,
Quelques mots, un silence, un geste simplement.
L’Histoire, page à page, doit s’écrire au présent,
On retrouve la trace d’un peuple en mouvement.

3.
Le temps du sacrement ou partage du Pain,
Expérience de vie, c’est un nouveau Chemin ;
Rendez-vous et rencontre : un devenir Témoin,
Présence dans l’absence à l’aube d’un matin.