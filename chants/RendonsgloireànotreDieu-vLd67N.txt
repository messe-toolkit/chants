R.
Rendons gloire à notre Dieu !
Lui qui fit des merveilles,
Il est présent au milieu de nous
Maintenant et à jamais !

1.
Louons notre Seigneur,
Car grande est sa puissance,
Lui qui nous a créés,
Nous a donné la vie.

2.
Invoquons notre Dieu,
Demandons-lui sa grâce,
Il est notre Sauveur,
Notre libérateur.

3.
Oui le Seigneur nous aime,
Il s´est livré pour nous.
Unis en son amour,
Nous exultons de joie.

4.
Dieu envoie son Esprit,
Source de toute grâce,
Il vient guider nos pas
Et fait de nous des saints.

5.
Gloire à Dieu notre Père,
À son Fils Jésus-Christ,
À l´Esprit de lumière
Pour les siècles des siècles.