R.
Voici le pain qui donne la vie,
le Pain vivant descendu du ciel !
Il a pris chair de la Vierge Marie,
de la Mère des vivants.

1.
Le Christ nouvel Adam
a livré sa vie pour nous,
Marie, Eve nouvelle a porté en son sein
le Dieu qui ressuscite,
Recevons en notre chair,
le pain de l'immortalité.

2.
Le Christ, nouvel Isaac,
offre sa vie en sacrifice,
Et Marie, par sa foi,
voit mourir le Fils unique,
Et voit jaillir l’Eau et le Sang
qui font vivre l'Église.

3.
Le Christ, nouveau Moïse,
a rassasié son Peuple de la manne.
Et Marie, a entonné le chant de sa victoire :
Le Seigneur a comblé
ceux qui ont faim de son amour.

4.
Le Christ, nouveau David,
a rassemblé son Peuple dans la joie,
Et Marie, fille de Sion, danse devant sa Face
Car elle voit tous ses fils
rassemblés au cœur de Jérusalem.