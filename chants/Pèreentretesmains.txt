Père, entre tes mains,
j'ai déposé le fardeau de mon péché ;
ton Souffle en moi
a ranimé l'esprit;
je n'attends que l'amour :
regarde-moi et je serai sauvé.

Sur nous, Seigneur, ta miséricorde !

Heureux l'homme dont la faute est enlevée,
dont le péché est couvert.
Heureux l'homme dont le Seigneur a oublié l'offense.

Le Seigneur veille sur ceux qui l'adorent,
sur ceux qui espèrent son amour,
pour les préserver de la mort.

Fais-nous revenir, Dieu notre Sauveur,
oublie ce que tu as contre nous ;
ne viendras-tu pas nous rendre la vie ?