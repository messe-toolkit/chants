R.
Ouvre mes yeux, Seigneur, fais que je voie !
Jésus Sauveur, je crois en toi !
Ouvre mes yeux, Seigneur, fais que je voie !
Ton amour me conduira.

1.
J’étais dans la nuit, dans la détresse et dans la mort.
Du fond de l’abîme j’ai crié vers toi, Seigneur.
Tu m’as délivré, tu m’as guéri, tu m’as sauvé,
Mon âme est en paix, car tu m’aimes.

2.
Aux pauvres, aux petits, Dieu garde toujours son appui.
Il guérit l’aveugle, à l’affligé il rend la joie.
Lui qui a créé tout l’univers entend ma voix,
Jamais il n’oublie ceux qu’il aime.

3.
Je veux te louer, Seigneur, Dieu de fidélité,
Chanter ta bonté, te célébrer tant que je vis.
J’ai confiance en toi, Seigneur, tu es toute ma joie,
Je veux proclamer tes merveilles !