DIEU-VIVANT, FAIS-NOUS VIVRE,
VIVRE DE TA VIE !
DIEU-VIVANT, FAIS-NOUS VIVRE,
FAIS-NOUS VIVRE DE TOI !

Esprit

1.
Esprit de Dieu animant notre glaise
Brise de vie qui plane sur les eaux
Souffle divin qui éveille et relève
Les ossements qui dormaient au tombeau.
SOUFFLE DE DIEU ! REVIENS DES QUATRE VENTS
RÉVEILLE-NOUS ET NOUS SERONS VIVANTS !

Père

2.
Fleuve du ciel qui arrose nos rives
Puits de tendresse au creux de nos rochers
Source cachée d’où jaillît cette eau vive
Que nous révèle Jésus le Bon Berger.
DIEU CRÉATEUR ET PÈRE DE LA VIE
L’HOMME VIVANT PARTOUT TE GLORIFIE.

Fils

3.
Chemin vivant, Parole qui fait vivre
Pain partagé, offert à toute faim
Geste d’amour qui guérit et délivre
Les coeurs brisés par le malheur humain
DIEU PARMI NOUS, PARTAGEANT NOTRE VIE
DIEU-AVEC-NOUS S’APPELLE JÉSUS-CHRIST.

Église

4.
Vigne plantée dans la terre des hommes
Portant la sève qui mûrit le raisin
Pour préparer le bon vin du Royaume
Que nous boirons au meilleur des festins.
VIGNE DE DIEU ! ÉGLISE DU SEIGNEUR,
PEUPLE JOYEUX, VIVANT D’UN MÊME COEUR !