R.
Maranatha, viens seigneur jésus!
Maranatha, viens, nous t’attendons!
Ne tarde plus, l’heure est avancée; Viens Seigneur, viens!.

1.
Il régnera sur son peuple à jamais, Protégera les enfants des pauvres.
Ses jugements produiront la justice. Viens Seigneur Jésus.

2.
En ces jours là, fleuriront la justice Et puis la paix jusqu’à la fin des temps.
D’une mer à l’autre, il dominera. Viens Seigneur Jésus.

3.
Devant lui se prosterneront les rois, Devant lui, se courbera la bête.
Ses ennemis lècheront la poussière. Viens Seigneur Jésus.

4.
Il sauvera le pauvre et le petit, Aura pitié de celui qui l’invoque.
Leur vie sera précieuse à ses yeux. Viens Seigneur Jésus.

5.
Son nom sera glorifié pour toujours, En lui seront bénis races et peuples.
Que la terre et le ciel soient remplis de sa gloire.Viens Seigneur Jésus.