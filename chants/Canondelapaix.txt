Écoutez, le temps viendra
Les hommes un jour sauront la vérité
Le lion s’étendra près de l’agneau
Et nous fonderons les piques pour des faux
Et les sabres pour des herses
La paix sera notre combat
Faites que ce temps vienne