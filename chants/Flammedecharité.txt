R.
Flamme de charité,
Gloire d'humilité,
Sœur de lumière,
Sainte-Claire,
Conduis-nous !

1.
Enseigne-nous le silence,
La maîtrise de l'âme et du corps,
Ni dedans, ni dehors,
Que le bruit
N'empêche en nous l'Esprit
D'écouter la Présence.

2.
Enseigne-nous la patience,
L'humble attente, l'effort de la foi.
Qu'un regard sur la croix
Dans l'amour,
Nous presse à notre tour
De pardonner l'offense.

3.
Enseigne-nous la prudence,
La sagesse au service de tous.
Fais jaillir des remous
De nos cœurs
La force du Seigneur
Lui seul a la puissance.

4.
Enseigne-nous l'espérance,
Le désir de répondre à l'appel
Qui résonne, éternel,
Dans le fond
De notre être profond.
Que toute âme s'élance !