R.
Joie parfaite au cœur de Dieu,
Paix levée comme une aurore,
Joie de vivre au cœur de Dieu,
Viens chanter en nous, viens chanter en nous !

1.
Pour que ma joie demeure en vous
Je vous ai dit cette parole :
"Aimez-vous les uns les autres,
Demeurez dans mon amour ;
Votre joie sera parfaite,
Demeurez dans mon amour."

2.
Pour que ma joie demeure en vous
Laissez mûrir cette parole :
"Je remonte vers le Père,
J'enverrai sur vous l'Esprit ;
Votre joie sera parfaite,
J'enverrai sur vous l'Esprit."

3.
Pour que ma joie demeure en vous
Gardez confiance en ma parole :
"Dieu vous aime comme un Père,
Demandez, vous recevrez ;
Votre joie sera parfaite,
Demandez, vous recevrez."

4.
Pour que ma joie demeure en vous
Devenez forts dans ma parole :
"Moi je suis vainqueur du monde,
Levez-vous, ne craignez pas ;
Votre joie sera parfaite,
Levez-vous, ne craignez pas !"