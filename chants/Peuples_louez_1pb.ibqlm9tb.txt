R.
Peuples, louez, acclamez Dieu,
Serviteurs du Seigneur,
Sonnez pour notre Roi !
Criez de joie, Dieu s’est levé
Parmi les ovations :
Le Seigneur vient !

1.
Peuples, chantez, louez, battez des mains,
Acclamez Dieu, offrez-lui votre joie.
Adorez-le, craignez-le, il est saint.
Sonnez, pour notre Roi, sonnez !

2.
Dieu, le grand roi, règne sur les nations,
Il nous a choisis, nous a faits pour lui.
Vous ses héritiers, ses fils bien-aimés,
Sonnez, pour notre Roi, sonnez !

3.
Dieu monte parmi les acclamations,
Le Seigneur s’élève aux éclats du cor.
Que vos musiques l’annoncent aux nations !
Sonnez, pour notre Roi, sonnez !

P.
Louez-le par vos chants,
Louez le Tout-Puissant !
Louez votre Seigneur,
Il est le Rédempteur !
Peuples, battez des mains,
Poussez des cris de joie !
Acclamez votre Roi, car il est saint !

4.
Dieu nous rassemble, en un peuple béni.
Son nom s’élève au-dessus des puissants ;
Le Seigneur trône dans sa majesté.
Sonnez, pour notre Roi, sonnez !