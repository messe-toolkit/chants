1.
Je regarde autour de moi, que c’est beau ce que je vois :
Les fleurs, les pierres et le bois, ça me donne tant de joie.

MOI AUSSI JE CRÉE, JE ME SERS DE MES MAINS,
MOI AUSSI JE CRÉE, ÇA ME FAIT TANT DE BIEN,
MOI AUSSI JE CRÉE, AVEC UN PETIT RIEN,
MOI AUSSI JE CRÉE, J’AIME BIEN CE REFRAIN.

2.
Je regarde le potier créer dans son atelier,
Façonner c’est son métier, et j’ai envie de crier.

3.
Je regarde le tableau et je me dis : « que c’est beau ! »
J’aimerais leur dire bravo, quand c’est parfois rigolo.

4.
Je regarde les dessins, les miens et ceux des copains,
Comm’ c’est beau quand on a peint tous ces trésors de nos mains.