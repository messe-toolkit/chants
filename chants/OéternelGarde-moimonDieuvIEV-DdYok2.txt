R.
Garde-moi, mon Dieu,
Ma force est en toi ;
Garde-moi, mon Dieu,
Mon bonheur, c'est toi. (bis)

1. Ô Éternel, de toi dépend ma vie,
Tu es mon Dieu et je viens à toi.
Je te bénis, ô Éternel,
Toi mon conseiller, tu es avec moi.

2.
Mon cœur exulte, mon âme est en fête,
Ma chair repose, j'ai confiance en toi.
Tu ne peux m'abandonner,
Tu montres le chemin, tu es toute ma joie.