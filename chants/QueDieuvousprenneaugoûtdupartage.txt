R.
Ne perdons pas ce jour,
La voie juste est celle de l’amour.

1.
Que Dieu vous prenne au goût du partage
Et de l’humble service :
Vous transmettrez sans alliage
Sa justice !

2.
Que Dieu vous prenne au goût de le vivre
Dans un humble silence :
Il saura bien vous redire
Sa présence !

3.
Que Dieu vous prenne au goût de son oeuvre
Dans une humble patience :
Il changera votre épreuve
En louange !

4.
Et qu’il vous prenne au goût de la fête
Dans les jeux de sa grâce :
Vous prouverez que vous êtes
De sa race !