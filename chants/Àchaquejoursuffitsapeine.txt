Refrain :
À chaque jour suffit sa peine,
À chaque jour, jour après jour !
À chaque jour suffit sa grâce,
Jour après jour,
À chaque jour de ton amour.

1.
Pourquoi trembler de mille peurs
Lorsque se fanent trop de fleurs :
À chaque jour suffit sa rose ;
Pourquoi renoncer au départ,
Dire trop tôt qu’il est trop tard :
À chaque jour suffit sa pause.

2.
Pourquoi la peur de l’ouragan,
De la tempête et des grands vents :
À chaque jour suffit sa voile ;
Pourquoi trembler au long des nuits,
Et penser que tout est fini :
Chaque nuit suffit son étoile.

3.
Chaque jour est entre mes mains,
Chaque pas devient mon destin
Inscrit au cadran de ta montre ;
À chaque jour de te choisir,
De t’accueillir, de te saisir,
Le plus beau jour, c’est la rencontre.