À quoi sert la lumière
Quand elle est sous le boisseau.
Et le sel de la terre
Quand il prend le goût de l'eau.

1.
Il faut un grand feu qui se voit de loin
Pour que le bateau trouve son chemin ;
Il faut qu'un flambeau prête sa lueur
Aux yeux fatigués des voyageurs !
Ainsi vous serez
Témoins de la lumière,
Jaillie de vos cœurs
Comme jaillit de terre un épi de blé.

2.
Il faut un signal qui se voit de loin
Pour l'avion qui part et celui qui vient ;
Il faut des lampions dans le creux des nuits
Pour celui qui rentre après minuit…
Ainsi vous serez
Témoins de la lumière,
Jaillie de vos cœurs
Comme jaillit de terre un épi de blé.

3.
Il faut une voix qui s'entend de loin
Pour que sa parole jaillisse enfin ;
Il faut des musiques pour bien danser
Et guider le pas des invités !
Ainsi vous serez
Témoins de la lumière,
Jaillie de vos cœurs
Comme jaillit de terre un épi de blé.