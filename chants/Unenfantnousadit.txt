1.
Un enfant nous a dit
L’âge de Dieu.
Un enfant nous est né :
Gloire de Dieu.

Dieu avec nous, Emmanuel !
Paix sur la terre et joie au ciel !
Noël, c’est Noël !

2.
Un enfant nous a dit
Le cœur de Dieu.
Un enfant nous est né :
Grâce de Dieu.

3.
Un enfant nous a dit
La paix de Dieu.
Un sauveur nous est né :
L’Homme de Dieu.

4.
Une femme a tissé
Le corps de Dieu.
Une femme ravie
Berce son Dieu.

5.
Une femme a nourri
Le Fils de Dieu.
On l’appelle Marie,
Mère de Dieu.