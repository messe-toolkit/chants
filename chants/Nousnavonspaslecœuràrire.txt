CHOEUR D'HOMMES :

1.
Nous n'avons pas le coeur à rire,
La guerre allume ses brasiers;
Nous n'avons par le coeur à rire,
Car les puissants l'on programmée.
Comment chanter "La paix doit vivre",
Si nous gardons les mains fermées ?
PAIX SUR BETHLEEM,
PAIX DANS L'UNIVERS,
PAIX SUR NOTRE TERRE !
CHOEUR DE FEMMES :

2.
Nous n'avons pas le coeur à rire
Devant les cris des torturés,
Nous n'avons pas le coeur à rire
Quand on enferme Liberté.
Comment chanter "l'amour peut vivre" ?
La voix des justes est enterrée.
PAIX SUR BETHLEEM,
PAIX DANS L'UNIVERS,
PAIX SUR NOTRE TERRE !
HOMMES ET FEMMES :

3.
Nous n'avons pas le coeur à rire
Devant les foules d'affamés,
Nous n'avons pas le coeur à rire,
Des enfants meurent par milliers.
Comment chanter l'espoir de vivre
Dans notre monde au pain brûlé ?
PAIX SUR BETHLEEM,
PAIX DANS L'UNIVERS,
PAIX SUR NOTRE TERRE !