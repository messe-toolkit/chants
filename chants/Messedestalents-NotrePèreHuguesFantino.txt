Notre Père qui es aux cieux,
que ton Nom soit sanctifié,
que ton règne vienne,
que ta volonté soit faite
sur la terre comme au ciel.
Donne nous aujourd’hui
notre pain de ce jour,
pardonne nous nos offenses
comme nous pardonnons aussi
à ceux qui nous ont offensés
et ne nous laisse pas entrer en tentation,
mais délivre nous du mal.
Car c’est à Toi qu’appartiennent le règne,
la puissance et la gloire
pour les siècles des siècles.
Amen ! Amen !