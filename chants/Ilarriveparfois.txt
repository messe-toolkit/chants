MAGNIFICAT! MAGNIFICAT!
MAGNIFICAT! MAGNIFICAT!

1.
Il arrive parfois que s’effondrent nos rêves
L’angoisse au fond du coeur et la pluie dans les yeux.
Mais alors ton Amour si présent me relève
Et vient chasser la nuit… Et soudain tout est bleu !

2.
Il arrive parfois que s’installe le doute
Que je doute de moi et des autres et de tout.
Mais déjà ton Amour me rejoint sur la route
Et mon coeur est brûlant : je me remets debout !

3.
Il arrive parfois que triomphe la haine
Ou bien que le mépris remplace le pardon,
Alors avec Amour ta main cherche la mienne
Et je choisis la Paix et je construis des ponts.

4.
Et il m’arrive aussi de goûter Ta Lumière
Et de t’ouvrir mon coeur pour que Tu puisses entrer.
Je suis sûr et certain que Tu m’aimes et m’espères
Et j’irai dire à tous que je T’ai rencontré !