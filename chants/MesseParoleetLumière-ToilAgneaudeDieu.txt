Toi, l’Agneau de Dieu,
Parole pour les vivants,
Lumière, soleil levant,
Prends pitié de nous,
Prends pitié de nous.
Toi, l’agneau de Dieu,
Parole de vérité,
Lumière d’éternité,
Prends pitié de nous,
Prends pitié de nous.
Toi, l’Agneau de Dieu,
Parole d’un vent nouveau,
Lumière venue d’en haut,
Donne-nous la paix
Donne-nous la paix.