GARDE-MOI, MON DIEU :
J’AI FAIT DE TOI MON REFUGE.

1.
Seigneur, mon partage et ma coupe :
de toi dépend mon sort.
Je garde le Seigneur devant moi sans relâche ;
il est à ma droite : je suis inébranlable.

2.
Mon coeur exulte, mon âme est en fête,
ma chair elle-même repose en confiance :
tu ne peux m’abandonner à la mort
ni laisser ton ami voir la corruption.

3.
Tu m’apprends le chemin de la vie :
devant ta face, débordement de joie !
À ta droite, éternité de délices !