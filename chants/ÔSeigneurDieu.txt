1.
Ô Seigneur Dieu, donne-moi
tout ce qui peut me conduire à Toi.

2.
Ô Seigneur Dieu,
éloigne de moi
tout ce qui peut
me détourner de Toi.

3.
Ô Seigneur Dieu,
fais aussi que je ne sois plus à moi,
mais que je sois entièrement à Toi.