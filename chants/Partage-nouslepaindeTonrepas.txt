R.
FILS DE DIEU PARTAGE-NOUS LE PAIN DE TON REPAS
FILS DE DIEU RELEVE-NOUS ET NOUS SUIVRONS TES PAS.

1.
Tu nous convies pour ton Alliance,
Toi le Maître et le Seigneur ;
fais nous comprendre ton exemple,
l'abaissement du Serviteur.

2.
Tu viens briser nos esclavages,
toi Jésus libérateur ;
comment te suivre en cette Pâque
où tu révèles ta grandeur ?

3.
Tu es pour nous la Loi nouvelle ,
toi qui sait donner ta vie ;
connaîtrons-nous la joie parfaite
que tu promets à tes amis ?

4.
Tu es le Cep et la vraie Vigne ;
nos sarments, tu les choisis.
Qu'ils soient porteurs dans ton Eglise,
de raisins mûrs comme ton Fruit !