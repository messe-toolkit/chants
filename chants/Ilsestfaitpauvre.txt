1.
Il s’est fait pauvre, le Roi du ciel,
Pour nous combler de grâce :
Le Seigneur est nu
Lorsqu’il vient en ce monde,
Il est nu, le Fils de l’homme,
Sur la croix qui nous sauve.

2.
Sur la montagne, quand il priait,
Il a montré sa gloire :
Regardez vers lui,
Contemplez son visage,
Il sera pour ceux qui l’aiment
Un miroir de lumière

3.
Rien ne témoigne de sa beauté,
Sinon la joie des pauvres,
Les rameaux coupés
Et les chants d’espérance ;
Un ânon est sa monture,
Des enfants son cortège.

4.
En son Royaume, les plus petits
Devanceront les princes.
Bienheureux celui
Qui pour Dieu se rend libre :
L’Esprit Saint, la joie parfaite,
Fait chez lui sa demeure.

5.
Il se découvre, le Roi du ciel
Dans le regard de Claire :
Elle voit déjà
La splendeur de sa gloire,
Et l’Époux, comme en échange,
La revêt de lumière.