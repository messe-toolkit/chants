1.
Heureux les pauvres de coeur,
car le royaume des Cieux est à eux.

2.
Heureux ceux qui pleurent,
car ils seront consolés.

3.
Heureux les doux,
car ils recevront la terre en héritage.

4.
Heureux ceux qui ont faim et soif de la justice,
car ils seront rassasiés.

5.
Heureux les miséricordieux,
car ils obtiendront miséricorde.

6.
Heureux les coeurs purs,
car ils verront Dieu.

7.
Heureux les artisans de paix,
car ils seront appelés fils de Dieu.

8.
Heureux ceux qui sont persécutés pour la justice,
car le royaume des Cieux est à eux.

9.
Heureux êtes-vous si l’on vous insulte,
si l’on vous persécute

10.
et si l’on dit faussement toute sorte de mal contre vous,
à cause de moi.

11.
Réjouissez-vous, soyez dans l’allégresse,

12.
car votre récompense est grande dans les cieux !