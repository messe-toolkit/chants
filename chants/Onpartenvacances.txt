R.
PUISQUE TU ES PARTOUT, MON DIEU,
QUAND ON PART EN VACANCES
ON T’EMMÈNE AVEC NOUS!

1.
On part en vacances, un matin,
remplis d’impatience,
c’est si loin !
Plein de paysages, en chemin,
changent en silence,
le temps d’un refrain !

2.
On part en voyage, un matin,
tout le monde est sage
mais soudain,
on a mal au coeur et on a faim,
nos pieds sont en cage,
vivement demain!

3.
On part en famille, un matin,
les garçons, les filles,
les cousins,
on rit aux éclats et pour un rien,
tous en espadrilles,
comme on se sent bien !