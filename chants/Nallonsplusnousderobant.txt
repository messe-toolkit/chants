1.
N'allons plus nous dérobant
À l'Esprit qui régénère :
Le Seigneur est ressuscité !
Un sang neuf coule aux artères
Du Corps entier,
La nuit du temps
Se change en lumière :
L'homme était mort, il est vivant.

2.
N'allons plus à contre-voie
De celui qui nous entraîne :
Le Seigneur est ressuscité !
Dans sa chair monte, soudaine,
L'éternité.
Il rend leur poids
Aux jours, aux semaines,
Les achemine vers la joie.

3.
N'allons plus sans feu ni lieu
Quand Jésus nous accompagne :
Le Seigneur est ressuscité !
Le voici, pain sur la table
Des baptisés.
Présent de Dieu
Offert en partage :
Christ aujourd'hui ouvre nos yeux.

4.
Nous irons, portant plus haut
Notre foi dans la victoire :
Le Seigneur est ressuscité !
L'univers chante la gloire
Des rachetés.
Le feu et l'eau
Emportent l'histoire,
Dieu nous appelle avec l'Agneau.