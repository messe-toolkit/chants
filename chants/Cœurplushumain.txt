1.
Coeur plus humain
Que fut jamais le coeur
D’un enfant de la terre,
Tu fus blessé
De nos souffrances,
Tu les pris en partage.
Ô Jésus,
Découvre-nous l’amour,
Celui seul qui vit en Dieu pour toujours !

2.
Coeur plus ouvert
Qu’un ciel à l’infini,
Une mer sans rivage,
Tu veux briser
Nos solitudes,
Rendre l’homme à ses frères.
Ô Jésus,
Viens élargir en nous
La mesure et l’horizon de l’amour.

3.
Coeur trop aimant
Qui ne reçus de nous
Qu’une croix en réponse,
Tu t’es livré
De pleine grâce
Au repas de la Pâque.
Ô Jésus,
L’Esprit, le sang et l’eau
Sont témoins de ton alliance d’amour.

4.
Coeur de Jésus
Qui fis le coeur nouveau
De l’Église nouvelle,
Tu l’as reçue
Dès l’origine
Pour épouse et compagne.
Ô Seigneur,
Accorde-lui ce don
De convier le monde entier à tes noces !