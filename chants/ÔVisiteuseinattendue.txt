1.
Ô visiteuse inattendue,
C’est dans le ciel et dans la nuit
Que tu t’en viens pour les petits
Et ton regard les a ravis.

2.
Ô visiteuse inattendue,
La longue nuit n’est pas finie,
Mais la beauté de ton sourire
Est un reflet du paradis.

3.
Ô visiteuse inattendue,
Si ton regard voilé de larmes
Nous présenta la croix du Fils,
C’est pour guérir les coeurs meurtris.

4.
Ô Belle Dame inattendue,
Nous méditons à ton école
Les grands secrets de la parole
Qui font resplendir notre vie.