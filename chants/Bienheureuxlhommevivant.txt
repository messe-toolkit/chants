R.
Bienheureux l’homme, l’homme vivant,
Joie sur la terre où Dieu est présent,
Joie sur la terre où Dieu est présent !

1.
Heureux le pauvre devant Dieu
Sa terre est prête au grand retour,
Il a mûri des blés d’amour
Et son grenier est dans les cieux.

2.
Heureux le fort qui est douceur,
Il a le monde pour ami,
C’est l’héritier de Jésus Christ,
Patience offerte à toute peur.

3.
Heureux qui sait le chant des pleurs,
Il est soutien pour l’affligé.
Un jour naîtra sous son archer
L’hymne à la joie de son Seigneur.

4.
Heureux le corps brûlé de soif
Et l’affamé du jour de Dieu ;
Il deviendra lumière et feu,
Un Pain vivant comme son Roi.

5.
Heureux le coeur qui est pardon,
il tue la mort aux fleurs du mal ;
par lui s’éveille un fruit pascal
et sa vendange est le Dieu bon.

6.
Heureux le coeur où Dieu se voit
comme au miroir du Premier-Né ;
ses yeux seront émerveillés
au face-à-face après le soir.

7.
Heureux celui qui donne paix
comme une aurore à épouser ;
Dieu le prénomme "Bien-Aimé",
le fils nouveau-né de sa chair.

8.
Heureux témoin celui qui meurt
en criant Dieu au prix du sang ;
quand vérité défie le temps,
c’est l’éternel qui est vainqueur.