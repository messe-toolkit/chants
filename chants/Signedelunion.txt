SIGNE DE L'UNION
AVEC TON DIEU,
ÉGLISE DU SEIGNEUR ;
SIGNE D'UN AMOUR
OFFERT À TOUS,
LOUANGE À TOI !
LOUANGE À TOI !

1.
Peuple libéré des eaux de la mort,
Voici les temps nouveaux
De justice et de paix.
Viens partager la Parole
Et le Pain de Vie !

2.
Peuple d'invités, pécheurs pardonnés,
Voici les bras ouvert
de l'Amour infini.
Viens partager la Parole
Et le Pain de Vie !

3.
Peuple de sauvés au Nom de Jésus,
Voici le Jour béni
D'allégresse et de joie.
Viens partager la Parole
Et le Pain de vie !

4.
Peuple rassemblé dans l'Esprit de Dieu,
Voici le Temple Saint
Où tu chantes ta foi.
Viens partager la Parole
Et le Pain de Vie !

5.
Peuple qui reçoit, en ton sein, le Fils,
Voici que tu dis "oui"
Au Projet du Très-Haut.
Viens partager la Parole
Et le Pain de Vie !

6.
Peuple d'envoyés auprès des petits,
Voici le temps d'aimer
À la suite du Christ.
Viens partager la Parole
Et le Pain de Vie !