Stance

Comme le Père m'a aimé, moi aussi, je vous ai aimés.
Demeurez sans mon amour
Alléluia.

R.

Christus resurrexit,
Christus resurrexit, Alleluia, alleluia !

Versets

Ps 66.

1.
Que Dieu nous prenne en grâce
et nous bénisse,
Que son visage s'illumine pour nous ;
Et ton chemin sera connu sur la terre,
Ton salut, parmi toutes les nations.

2.
Que les peuples, Dieu, te rendent grâce ;
Qu'ils te rendent grâce tous ensemble !
Que les nations chantent leur joie,
Car tu gouvernes le monde avec justice.

3.
Tu gouvernes les peuples avec droiture,
Sur la terre, tu conduis les nations.
Que les peuples, Dieu, te rendent grâce ;
Qu'ils te rendent grâce tous ensemble !

4.
La terre a donné son fruit ;
Dieu, notre Dieu, nous bénit.
Que Dieu nous bénisse,
Et que la terre tout entière l'adore !