Stance
De l’orient et de l’occident,
du Nord et du Midi,
le Seigneur nous invite :
“ Venez ensemble à ma table
manger le pain et boire le vin que j’ai préparé ”.

R.
UN SEUL PAIN : UNE MÊME FOI !
UN SEUL VIN : UNE MÊME ESPÉRANCE !

1.
Ils viendront du Levant et du Couchant
Et prendront place à table au festin du Royaume !

2.
Puisqu’il n’y a qu’un seul pain,
à nous tous ne formons qu’un seul corps !

3.
Notre communion, elle est avec le Père
Et avec son fils Jésus-Christ !