R.
AIE PITIÉ DE MOI,
JE VIENS VERS TOI,
OUVRE MES YEUX,
FAIS QUE JE VOIE,
RIEN QUE POUR TOI,
JE DANSERAI DE JOIE.

1.
Dans la vie j’avance seul,
Un matin sans devenir.
Mais tu ouvres mes yeux
Et j’ai faim de ta Parole !

2.
Dans ma nuit je cherche seul
Un chemin pour l’avenir.
Mais tu ouvres mes yeux
Et j’ai soif de te connaître !

3.
Dans mon cri je chante seul
Un refrain sans harmonie.
Mais tu ouvres mes yeux
Et je suis prêt à te suivre.

4.
Sans amis, je marche seul
En risquant de m’égarer,
Mais tu ouvres mes yeux
Et je vis de ta rencontre.