TU OUVRES TA MAIN, SEIGNEUR :
NOUS VOICI RASSASIÉS.

1.
Le Seigneur est tendresse et pitié,
lent à la colère et plein d’amour ;
la bonté du Seigneur est pour tous,
sa tendresse, pour toutes ses oeuvres.

2.
Les yeux sur toi, tous, ils espèrent :
tu leur donnes la nourriture au temps voulu ;
tu ouvres ta main :
tu rassasies avec bonté tout ce qui vit.

3.
Le Seigneur est juste en toutes ses voies,
fidèle en tout ce qu’il fait.
Il est proche de ceux qui l’invoquent,
de tous ceux qui l’invoquent en vérité.