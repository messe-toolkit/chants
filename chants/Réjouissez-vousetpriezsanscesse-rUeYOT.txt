1.
Réjouissez-vous (bis) et priez sans cesse, (bis)
En toute chose (bis) rendez grâce à Dieu ! (bis)
Réjouissez-vous et priez sans cesse,
Réjouissez-vous et priez sans cesse,
En toute chose, (bis) rendez grâce à Dieu.

2.
Réjouissez-vous (bis) et priez sans cesse, (bis)
En toute chose (bis) rendez grâce à Dieu ! (bis)
C’est sa volonté sur vous dans le Christ,
C’est sa volonté sur vous dans le Christ,
En toute chose, (bis) rendez grâce à Dieu.

3.
Réjouissez-vous (bis) et priez sans cesse, (bis)
En toute chose (bis) rendez grâce à Dieu ! (bis)

Titre original (EN) : This is the day (that the Lord has made)

Adaptation : G. du Boullay © 1978, Éditions de l’Emmanuel, 89 boulevard Blanqui, 75013 Paris