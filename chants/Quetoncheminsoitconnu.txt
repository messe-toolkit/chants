QUE TON CHEMIN SOIT CONNU SUR LA TERRE
ET TON SALUT PARMI TOUTES LES NATIONS!
QUE TOUS LES PEUPLES TE CONNAISSENT, NOTRE PÈRE,
ET QUE GRANDISSE UN PEUPLE NOUVEAU!

Accueillons la Parole de Dieu :

1.
Apprenez à garder les commandements
que je vous ai donnés.

R.

Accueillons la Parole de Dieu :

2.
Je suis avec vous tous les jours
jusqu'à la fin des temps.

R.

Accueillons la Parole de Dieu :

3.
Vous allez recevoir une force,
celle du Saint-Esprit.

R.

Accueillons la Parole de Dieu :

4.
Vous serez mes témoins
jusqu'aux extrémités de la terre.