R.
QUE SOIENT OUVERTES NOS DEMEURES
À TOUS NOS FRÈRES SÉPARÉS,
CAR TON AMOUR EST SANS FRONTIÈRES,
DIEU QUI NOUS AIMES LE PREMIER !

1.
Pour des frères unis en toi,
Quelle joie de vivre ensemble !
POUR DES FRÈRES UNIS EN TOI,
QUELLE JOIE DE VIVRE ENSEMBLE!
Joie de former un même coeur
Où vit l’amour qui nous rassemble.
JOIE DE FORMER UN MÊME COEUR
OÙ VIT L’AMOUR QUI NOUS RASSEMBLE.

2.
Dans le peuple des croyants,
Quelle joie de vivre ensemble !
DANS LE PEUPLE DES CROYANTS,
QUELLE JOIE DE VIVRE ENSEMBLE!
Joie d’accueillir le même Esprit
Pour témoigner d’une espérance !
JOIE D’ACCUEILLIR LE MÊME ESPRIT
POUR TÉMOIGNER D’UNE ESPÉRANCE!

3.
Sur la terre des vivants,
Quelle joie de vivre ensemble !
SUR LA TERRE DES VIVANTS,
QUELLE JOIE DE VIVRE ENSEMBLE!
Joie de bâtir sous ton regard
Un avenir à ta louange !
JOIE DE BÂTIR SOUS TON REGARD
UN AVENIR À TA LOUANGE!