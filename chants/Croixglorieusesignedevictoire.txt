Stance

Croix glorieuse, signe de victoire,
conduis-nous dans la cité de Dieu !

R.

O Christe Domine Jesu,
O Christe Domine Jesu !

Versets

Ps 24.

1.
Vers toi, Seigneur, j'élève mon âme,
Vers toi, mon Dieu.
Je m'appuie sur toi : épargne-moi la honte ;
Ne laisse pas triompher mon ennemi.

2.
Pour qui espère en toi, pas de honte,
Mais honte et déception pour qui trahit.
Seigneur, enseigne-moi tes voies,
Fais-moi connaître ta route.

3.
Dirige-moi par ta vérité, enseigne-moi,
Car tu es le Dieu qui me sauve.
C'est toi que j'espère tout le jour
En raison de ta bonté, Seigneur.

4.
Les voies du Seigneur sont amour et vérité
Pour qui veille à son alliance et à ses lois.
A cause de ton nom, Seigneur,
Pardonne ma faute : elle est grande.