Alléluia, la paix de Dieu pour les nations !
Alléluia, la paix de Dieu dans nos maisons !

1.
Voyez comme il est beau sur la montagne,
Le Messager qui proclame la paix !

2.
L’innocent se laisse condamner :
Son supplice nous redonne la paix !

3.
Le Bienveillant a compassion de nous :
Il ne brisera pas son alliance de paix !