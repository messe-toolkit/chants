1.
Les couleurs de la terre,
Tu les as inventées ;
Les trésors de la terre,
Tu nous les as donnés.
Pour la joie de découvrir
Ta création,
J’ai envie de t’applaudir
À ma façon !

R.
Notre Père qui es aux cieux,
En tout espace, en tous lieux,
Dans le noir ou la lumière,
Dans le ventre de la terre,
Notre Père qui es partout,
Ton amour veille sur nous !

2.
Comme un soleil se lève
Et chaque soir s’éteint,
Une journée s’achève
Et reviendra demain.
C’est l’histoire de nos vies
De chaque jour,
Entre douceur et tempête,
Allers-retours !

3.
Tu es en toutes choses,
Nos yeux ne te voient pas.
Tu es le virtuose
D’un monde né de toi.
Pour les hommes de partout,
De tous les temps,
Pour l’éternité d’amour
Qui les attend !