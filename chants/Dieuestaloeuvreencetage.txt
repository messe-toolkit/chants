1.
Dieu est à l'œuvre en cet âge,
Ces temps sont les derniers.
Dieu est à l'œuvre en cet âge,
Son jour va se lever !
Ne doutons pas du jour qui vient,
La nuit touche à sa fin.
Et l'Éclat du Seigneur remplira l'univers
Mieux que l'eau ne couvre les mers !

2.
Quelle est la tâche des hommes
Que Dieu vient rassembler,
Afin de bâtir le royaume
Du prince de la paix ?
Que peut-on faire pour hâter
Ce jour tant espéré
Où l'éclat du Seigneur remplira l'univers
Mieux que l'eau ne couvre les mers ?

3.
Pour que ce jour ne nous perde,
Ce jour comme un voleur,
Ne dormons pas aux ténèbres,
Veillons dans le Seigneur.
Comme l'éclair part du Levant
Et va jusqu'au Couchant,
Il viendra dans sa gloire au-dessus des nuées,
Le Seigneur qui est Dieu d'amour.

4.
Que notre marche s'éclaire
Au signe de Jésus !
Lui seul peut sauver notre terre
Où l'homme n'aime plus.
Il faut défendre l'exploité,
Ouvrir au prisonnier,
Et l'Éclat du Seigneur remplira l'univers
Mieux que l'eau ne couvre les mers !

5.
Dieu est amour pour son peuple,
Il aime pardonner.
Dieu est amour pour son peuple,
Il veut sa liberté.
Ne doutons pas du jour qui vient,
La nuit touche à sa fin.
Déchirons notre cœur, revenons au Seigneur,
Car il est le Dieu qui revient.