REFRAIN 1
Si peu de pain dans notre main,
Et ta présence emplit nos vies.

REFRAIN 2
Un peu de vin dans une coupe,
Et tu te livres sans réserve.

1.
Ton corps en nourriture
Nous est donné :
Notre faim grandit.

Refrain 1

2.
Ton sang comme une source
Nous est offert :
Plus vive est notre soif.

Refrain 2

3.
Nos mains reçoivent ton corps,
Tu les saisis,
Tu nous entraînes dans ton offrande.

Refrain 1

4.
Nous portons à nos lèvres la coupe,
Et dans nos coeurs
Ton sang murmure la louange.

Refrain 2

5.
Unique est le don pour chacun,
Unique le corps qui nous rassemble
Dans une même action de grâce.

Refrains 1 et 2