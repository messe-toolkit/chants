R.
C’est toi, Jésus, le premier
De nos compagnons de route,
Tu entraînes ceux qui doutent.
C’est toi, Jésus, le premier
Ressuscité, ressuscité !

1.
Par le baptême
On entre dans la marche
Du peuple des croyants.
Par le baptême
On entre dans la marche
D’un Dieu qui accompagne
Ses enfants !

2.
Jean le Baptiste
Vivait au bord du fleuve,
Il parlait du Seigneur.
Jean le Baptiste
Vivait au bord du fleuve
En attendant que vienne
Le Sauveur !

3.
Vincent, Odile,
Damien, Emmanuelle,
Pour nous sont des témoins.
Vincent, Odile,
Damien, Emmanuelle,
Font partie des prophètes
Et des Saints !

4.
Par ta Parole
Nous entrons dans l’Histoire
De ceux qui t’ont trouvé.
Par ta Parole
Nous entrons dans l’Histoire
De la famille humaine
Rassemblée !