1.
Ne doute plus de ce mystère,
Joseph, fils de David,
Reçois l'annonce après Marie :
Dieu vient sur terre,
Ta maison sera la sienne.

2.
Tu prendras soin du feu qui monte,
Il doit tout embraser ;
Sur cette flamme encor cachée
Veille dans l'ombre,
Sa clarté sera la tienne.

3.
Tu donneras au Fils de l'homme
Le nom de Dieu-Sauveur,
Lui qui sera le Serviteur
Vient comme un pauvre,
Mais c'est lui notre richesse.