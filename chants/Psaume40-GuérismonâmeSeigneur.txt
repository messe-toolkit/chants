GUÉRIS MON ÂME, SEIGNEUR,
CAR J’AI PÉCHÉ CONTRE TOI.

1.
Heureux qui pense au pauvre et au faible :
le Seigneur le sauve au jour du malheur !
Il le protège et le garde en vie,
heureux sur la terre.

2.
Le Seigneur le soutient sur son lit de souffrance :
si malade qu’il soit, tu le relèves.
J’avais dit : « Pitié pour moi, Seigneur,
guéris-moi, car j’ai péché contre toi ! »

3.
Dans mon innocence tu m’as soutenu
et rétabli pour toujours devant ta face.
Béni soit le Seigneur, Dieu d’Israël,
depuis toujours et pour toujours ! Amen ! Amen !