1-1
Ô Seigneur, envoie ton Esprit qui renouvelle la face de la terre.
1-2
Toute la terre, Seigneur, est remplie de ton amour.

2.
Garde-moi, Seigneur mon Dieu, toi, mon seul espoir.

3.
Chantons le Seigneur, car il a fait éclater sa gloire,
Il a jeté à l'eau cheval et cavalier.

4.
Je t'exalte, mon Dieu, toi qui me relèves.
5-1
Ivres de joie, vous puiserez les eaux aux sources du salut.
5-2
Ivres de joie, vous puiserez l'eau vive aux sources du salut.

6.
Dieu, Tu as les paroles de la vie éternelle.
7-1
Donne-nous, Seigneur, un cœur nouveau ; mets en nous, Seigneur, un esprit nouveau.
7-2-
Mon âme a soif du Dieu vivant, quand le verrai-je face à face.

8.
Alléluia, Alléluia, Alléluia.
Rendez grâce au Seigneur car il est bon :
Éternel est son amour !
La maison d'Israël peut le dire :
Éternel est son amour !
La droite du Seigneur a fait merveille,
Sa droite m'a relevé.
Non je ne mourrai pas, je vivrai
Et publierai l'œuvre de Dieu.
La pierre rejetée des bâtisseurs
Est devenue pierre d'angle.
C'est là l'œuvre de Dieu,
Une merveille à nos yeux.