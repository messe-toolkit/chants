1.
Femme tout entière habitée par l’amour,
Arche de l’Alliance, Marie de Nazareth,
TON ÂME EXALTE LE SEIGNEUR. (bis)
Femme qui choisit de servir sans retour,
Fruit de notre terre, tu chantes pour ton Dieu :
(Sainte Catherine, tu chantes pour ton Dieu :)
ALLÉLUIA, ALLÉLUIA! (bis)
Peuple aux yeux tournés vers le feu du plein jour
Jeune Église en marche, exultons pour le Seigneur :
ALLÉLUIA, ALLÉLUIA. (bis)

2.
Femme dont le oui fait germer le Sauveur,
Temple des merveilles, Marie de Nazareth,
TON ÂME EXALTE LE SEIGNEUR. (bis)
Femme aux simples mots qui jaillissent du coeur,
Fruit de notre terre, tu chantes pour ton Dieu :
Sainte Catherine, tu chantes pour ton Dieu :
ALLÉLUIA, ALLÉLUIA! (bis)
Peuple qui répond à l’appel du Semeur,
Jeune Église en marche, exultons pour le Seigneur :
ALLÉLUIA, ALLÉLUIA! (bis)

3.
Femme qui se lève et qui porte la joie,
Chant de la Parole, Marie de Nazareth,
TON ÂME EXALTE LE SEIGNEUR. (bis)
Femme au coeur du monde en témoin de la foi,
Fruit de notre terre, tu chantes pour ton Dieu :
Sainte Catherine, tu chantes pour ton Dieu :
ALLÉLUIA, ALLÉLUIA! (bis)
Peuple qui partage l’eau vive et la soif,
Jeune Église en marche, exultons pour le Seigneur :
ALLÉLUIA, ALLÉLUIA! (bis)

4.
Femme dont la force résiste aux puissants,
Paix dans les ténèbres, Marie de Nazareth,
TON ÂME EXALTE LE SEIGNEUR. (bis)
Femme qui réveille l’espoir des vivants,
Fruit de notre terre, tu chantes pour ton Dieu :
Sainte Catherine, tu chantes pour ton Dieu :
ALLÉLUIA, ALLÉLUIA! (bis)
Peuple de lumière aux couleurs du levant,
Jeune Église en marche, exultons pour le Seigneur :
ALLÉLUIA, ALLÉLUIA! (bis)