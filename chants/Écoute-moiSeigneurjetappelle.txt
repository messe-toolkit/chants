Stance
Écoute-moi, Seigneur,
je t’appelle,
toi qui es mon seul refuge :

R.
Ne me quitte pas,
ne m’abandonne pas,
Dieu mon Sauveur!

Versets

1.
désir des humbles, tu l’écoutes, Seigneur,
tu affermis leur coeur, tu tends l’oreille.

2.
Le secret de Dieu est pour ceux qui le craignent ;
à ceux-là il révèle son alliance.

3.
Que jubilent à jamais ceux que tu protèges,
et qu’exultent en toi les amants de ton nom. Stance