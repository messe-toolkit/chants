R.
Lumière aux nuits de mort,
Fils de Dieu, notre vie,
Allume un chant d’espoir,
Feu de Pâque aujourd’hui !

1.
Fini le temps du Golgotha,
Fini le cri du Fils de l’homme,
Finie la croix,
Sinon pour l’homme
Que nous sommes.
Où donc est ta victoire,
Jésus ressuscité ?

2.
Fini le temps du Serviteur,
Fini le soir du Fils de l’homme,
Finie la peur,
Sinon pour l’homme
Que nous sommes.
Où donc est ta victoire,
Jésus ressuscité ?

3.
Premier réveil au Dieu de vie,
Premier matin du Fils de l’homme,
Premier réveil
Aussi pour l’homme
Que nous sommes.
Où donc est ta victoire,
Ô mort du Premier-Né ?

4.
Encor le temps des Golgotha,
Encor la soif au coeur de l’homme,
Immense croix
Du Fils de l’Homme
Que nous sommes.
Où donc est ta victoire,
ô mort du Premier-Né ?

5.
Viendra le temps de nos soleils,
Viendra ton Jour, ô Fils de l’Homme,
En toi réveil
De tout cet homme
Que nous sommes.
Ton Souffle est notre Pâque,
Jésus ressuscité !