R.
Je te chante en présence des Anges,
Je me prosterne en ton Temple sacré.

1.
L'Ange se tint près de l'autel,
Tenant en sa main l'encensoir d'or ;
Et la fumée des parfums monta en présence du Seigneur.

2.
Je rends grâce à ton Nom, Seigneur,
Pour ton amour et ta vérité ;
Car tu as fait pour nous des merveilles.

Gloire au Père, et au Fils, et au Saint-Esprit.