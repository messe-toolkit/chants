1.
Je veux t’aider, mon Dieu,
À me trouver à toute heure
Et pour qu’en moi, mon Dieu,
Tu établisses ta demeure.

Refrain :
Et mettre ma main dans la tienne,
Et mettre mes pas dans tes traces,
À chaque jour suffit sa peine,
À chaque jour suffit sa grâce.

2.
Je veux t’aider, mon Dieu,
Toi qui attends l’enfant prodigue
Mettant si Tu le veux
Un peu d’espoir sur ta fatigue.

3.
Je veux t’aider, mon Dieu,
Car Tu as besoin de mon aide
Je ferai de mon mieux
Moi aussi j’attends que tu m’aides.

4.
Je veux t’aider, mon Dieu,
À te révéler en moi-même,
Et à m’ouvrir les yeux
Pour percevoir que QUELQU’UN m’aime.