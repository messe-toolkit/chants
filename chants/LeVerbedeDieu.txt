LE VERBE DE DIEU,
NÉ DU PÈRE AVANT TOUS LES SIÈCLES,
A PRIS CHAIR AUJOURD’HUI DE NOTRE CHAIR :
POUR NOUS, IL S’EST ANÉANTI.

1.
Le Christ Jésus, ayant la condition de Dieu,
ne retint pas jalousement le rang qui l’égalait à Dieu ;
mais il s’est anéanti, prenant la condition de serviteur.

2.
Devenu semblable aux hommes, reconnu homme à son aspect,
il s’est abaissé, devenant obéissant jusqu’à la mort,
et la mort de la croix.

3.
C’est pourquoi Dieu l’a exalté :
et lui a donné le Nom qui est au-dessus de tout nom;
afin qu’au nom de Jésus tout genou fléchisse,
au ciel, sur terre et aux enfers,
et que toute langue proclame : « Jésus Christ est Seigneur »
à la gloire de Dieu le Père.