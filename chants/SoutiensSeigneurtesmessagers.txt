1.
Soutiens, Seigneur, tes messagers,
Qu’ils aient l’audace de crier
Sur tous les toits et au grand jour
Ce que nous dit le Dieu d’amour
Au plus secret. (bis)

2.
Comment, Jésus, ne pas trembler
Quand les témoins sont menacés
Par la violence des puissants ?
Nous sommes faits de chair et sang
Et tu le sais. (bis)

3.
Tu dis pourtant : « N’ayez pas peur ! »
Ne manquez pas ce grand bonheur
De révéler à chaque humain
Qu’il est marqué du feu divin
Au fond du coeur. (bis)

4.
Ne craignez pas qui tue le corps,
Craignez celui qui met à mort
La fleur de l’âme et de l’esprit,
Elle est le tout de votre vie,
Le seul trésor. (bis)

5.
Devant les hommes et sur la croix
Tu dis quel Dieu te donne espoir :
Il est ton Père et ton Sauveur,
Par lui tu es « Jésus Seigneur »
En qui je crois. (bis)

6.
Heureux qui parle de sa foi
En affirmant : « Christ est ma joie! »
Il rendra gloire dans les cieux
De prendre place auprès de Dieu
Et près de Toi. (bis)