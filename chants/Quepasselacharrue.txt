1.
Que passe la charrue
Sur nos landes rebelles,
Sur nos terres en friche !
La Parole ira s’y planter,
Promesse pour le pauvre,
Et pauvreté offerte au riche.

2.
Au feu tout le bois mort,
Que la flamme s’étende
Aux chardons, aux épines !
Et leurs cendres pourront servir
À féconder la terre
Où la Parole prend racine.

3.
Que tombe sur nos sols
De poussière et de roche
Une pluie généreuse !
On verra les feuilles pointer
Et les bourgeons éclore
De la Parole qui nous creuse.

4.
Advienne le soleil
Et vers lui que s’élance
La poussée de la sève !
La Parole nourrit son fruit
D’amour et de justice
Dans la louange qui l’achève.