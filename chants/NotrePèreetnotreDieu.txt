NOTRE PÈRE QUI ES AUX CIEUX,
TU FAIS DE NOUS TES ENFANTS,
FRÈRES ET SOEURS EN NOUS AIMANT,
NOTRE PÈRE ET NOTRE DIEU !

1.
Que ton Nom soit sanctifié, le seul Saint, le seul vrai Dieu.
Qu'il soit partout adoré, sur la terre comme au ciel !

2.
Que ton règne vienne en nous, règne d'amour et de paix ;
Qu'il nous conduise à la joie sur la terre comme au ciel !

3.
Que soit faite ta volonté en réponse à ton amour ;
Vrai chemin de liberté sur la terre comme au ciel !

4.
Donne-nous pour aujourd'hui le pain qui nourrit nos corps,
Et celui qui donne vie, Jésus dans l'Eucharistie.

5.
Pardonne-nous nos péchés, toi qui veux nous relever ;
Apprends-nous à pardonner pour que nous puissions aimer.

6.
Ne nous laisse pas tenter, tu sais nos fragilités ;
Mais délivre-nous du Mal qui nous tient emprisonnés.