STANCE
Le Verbe de vie, tes mains l'ont touché,
Tes yeux l'ont contemplé, ton cœur l'a reconnu !
Jean ton regard d'aigle a fixé le soleil :
Fais nous entrer dans ta louange.

R.
Louange et gloire au Verbe
Manifesté dans la chair.

1.
La lumière est venue dans le monde,
Elle repousse les ténèbres.

2.
À ceux qui le reçoivent il donne sa justice,
Les voici devenus enfants de Dieu.

3.
Aimons-nous les uns les autres :
L'envoyé du Père nous a livré l'Esprit.