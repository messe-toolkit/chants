R.
Dieu! Tu as les paroles d'Alliance éternelle.

Psaume 88.
" Avec mon élu, j'ai fait une alliance,
j'ai juré à David, mon serviteur:
J'établirai ta dynastie pour toujours,
je te bâtis un trône pour la suite des âges. "

" Il me dira: Tu es mon Père,
mon Dieu, mon roc et mon salut !
Et moi, j'en ferai mon fils aîné,
le plus grand des rois de la terre! "

" Sans fin je lui garderai mon amour,
mon alliance avec lui sera fidèle;
je fonderai sa dynastie pour toujours,
son trône aussi durable que les cieux. "