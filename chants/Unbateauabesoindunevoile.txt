Refrain :
Un bateau a besoin d'une voile ;
Que serait l'oiseau sans horizon !
Un marin a besoin d'une étoile,
Que serait ma vie sans ta chanson ? (bis)

1.
Voix d'un prophète
Risquant sa peau ;
Cri d'un poète
À Santiago.
Dans la tempête
Chante un héros ;
L'espoir secrète
Des jours plus beaux.

2.
Une caresse
Change un destin ;
Une promesse
Ouvre un matin ;
Dans la tendresse
Fleurit demain ;
L'amour sans cesse
Montre un chemin.