Refrain :
Je ne sais ni le jour ni l'heure,
Mais je sais que c'est toi qui viens ;
Je t'espère en ma demeure
Jusqu'au matin, tu le sais bien. (bis)

1.
Dans les jours de solitude
Où mon pas est incertain,
Dans les jours de déchirure,
Que tu es loin, que tu es loin !

2.
Dans les moments de malchance
Où je n'y comprends plus rien,
Dans les moments de souffrance,
Que tu es loin, que tu es loin !

3.
Mais tu viens dresser la table
Et ton pain devient mon pain ;
Ton amour est véritable,
J'en suis certain, puisque tu viens.