1.
C’est un homme qui n’a rien dit
Quand la Vierge, sa fiancée
Lui annonça qu’elle attendait
Un enfant conçu par l’Esprit.

R.
JOSEPH TU ES LE MAÎTRE DU SILENCE,
TA VIE EST UN POÈME AU FIL DES JOURS.
TA PARFAITE OBÉISSANCE
N’A DE SOURCE QUE TON AMOUR,
N’A DE SOURCE QUE TON AMOUR.

2.
C’est un homme qui n’a rien dit
Quand sur ordre de l’empereur,
Il dut quitter à contrecoeur
Sa maison et son établi.

3.
C’est un homme qui n’a rien dit
Quand personne, malgré le froid,
N’ouvrit sa porte à “ces gens-là
Qui dérangent en pleine nuit !”

4.
C’est un homme qui n’a rien dit
Quand il vit naître le Sauveur
Et les bergers, d’un même coeur,
Louer Dieu les yeux éblouis !

5.
C’est un homme qui n’a rien dit
Quand il vit le vieux Siméon
Prendre en ses bras le nourrisson
Et chanter : « J’ai vu le Messie ! »

6.
C’est un homme qui n’a rien dit
Quand il dut, pour sauver l’enfant
Des mains sanglantes d’un tyran,
Se cacher, loin de son pays.

7.
C’est un homme qui n’a rien dit
Quand au temple, durant trois jours,
Il rechercha, tremblant d’amour,
Son enfant, le fils de Marie.