Ensemble au cénacle (Auprès de Marie)
Paroles et musique : Communauté de l'Emmanuel (Benoît Carraud)No. 19-06.

1.
Auprès de Marie, ensemble au Cénacle,
Nous levons les yeux vers le ciel.
Seigneur, fais jaillir ta splendeur de gloire.
Illumine-nous de ton feu.

R.
Viens ! Souffle de Dieu,
Ô viens ! Esprit du Très-Haut,
Descends sur nous flamme d’amour,
Ô viens ! Brûle en nous toujours !

2.
Esprit Créateur, éclaire nos âmes,
Viens emplir nos cœurs de ta grâce.
Donne-nous tes dons, inspire nos langues.
Nous voulons partout témoigner !

3.
Répands en nos cœurs l’amour de nos frères,
Viens, et fais de nous un seul corps.
Tu viens habiter notre humble louange,
Renouvelle-nous dans la joie !