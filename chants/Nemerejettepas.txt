R.
Rien ne pourra jamais me séparer de ton amour,
ton amour qui s’est manifesté dans le Christ !

1.
Ne me rejette pas, Dieu très bon, dans la nuit.
Quand je me réfugie au creux de tes bras :
Je sais bien que tu m’attends !

2.
Si je me suis cherché dans l’oubli de ton Nom,
Si je me suis perdu, fuyant loin de toi,
Je viens quémander ta paix !

3.
Dieu d’infini bonté, bien plus grand que mon coeur,
Tiens-moi dans ton pardon et guéris mes plaies :
Je n’ai d’autre appui que toi !