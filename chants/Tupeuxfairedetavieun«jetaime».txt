R.
Si tu as envie
Comme Jésus-Christ lui-même
Tu peux faire de ta vie un « je t’aime ».

1.
Ils sont venus des longs chemins.
La justice en eux crie de faim.
Ils ont tout perdu au supplice,
Trouvant le bonheur d’être heureux.
Bienheureux?! Bienheureux?!
Les persécutés de justice.

2.
Avec l’acier de leurs épées,
Ils font des charrues pour la paix.
Tous les canons, ils les font taire,
Semant le bonheur d’être heureux.
Bienheureux?! Bienheureux?!
Ceux qui font la guerre à la guerre?!

3.
Quand tous les cœurs sont des cailloux,
Le pardon se lève un peu fou,
Pour réconcilier nos discordes,
Heureux du bonheur d’être heureux.
Bienheureux?! Bienheureux?!
Les amours de miséricorde.

4.
Quand le monde est pourri d’argent,
Ils vont partout en pataugeant,
La misère a lâché ses fauves
Mais ils ont la joie d’être heureux.
Bienheureux?! Bienheureux?!
Les cœurs battant au cœur des pauvres.