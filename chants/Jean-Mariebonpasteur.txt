R.
JEAN-MARIE BON PASTEUR.
PRÊTRE DU SEIGNEUR.
JEAN-MARIE, BON PASTEUR,
TRANSFORME NOS COEURS!
JEAN-MARIE, BON PASTEUR.
PRÊTRE AIMÉ DE DIEU,
JEAN-MARIE, BON PASTEUR,
MONTRE-NOUS LES CIEUX!

1.
Par tes jeûnes et tes privations,
Tu maîtrises tes passions,
En souffrant, tu répares en toi
Tout le mal fait à ton Roi !

2.
Devant Dieu, le jour et la nuit,
Tu contemples l’Infini,
La prière fait naître en toi
Paix, amour, espoir et foi !

3.
Tu accueilles dans ta bonté
Les pécheurs, les égarés,
Le pardon manifeste en toi
Tout l’amour du Christ en croix !

4.
Tu affirmes monter au ciel
Quand tu célèbres à l’autel,
Rien ne vaut ces moments de joie
Avec Dieu présent en toi !

5.
Quand survient le jour du Seigneur,
Tu rayonnes de bonheur.
La lumière qui vit en toi
Transparaît en mille éclats !