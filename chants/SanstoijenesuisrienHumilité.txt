Sans Toi, je ne suis rien,
Tu es mon Créateur !

Devant Toi ce rien paisible
Ne vit que d’un regard :
Un enfant
Sans autre désir
Que d’être aimé encore.
J’ai du prix à tes yeux,
Ô mon Dieu !

Laisse-moi être la source
Qui désaltère
Sans attendre merci,
Sans trouble et sans repos.

Sans Toi, je ne puis rien,
Tu es le seul Seigneur !

En tes mains, rien d’impossible :
Le monde est sous ta loi.
Il m’est bon
D’apprendre à servir,
D’être appelée servante.
Je suis reine avec Toi,
Ô mon Roi !

Laisse-moi être la terre
Que l’on piétine
Mais qui mène vers Toi
Le pas des égarés.

Sans Toi, je ne veux rien,
Tu es tout mon bonheur !