R.
Alleluia, alleluia !
Nous proclamons à pleine voix :
Christ est Seigneur, notre lumière !
Christ est Seigneur, paix sur la terre !

1.
Noël
Verbe de Dieu parmi ton peuple,
Verbe de Dieu, tu es la Vie,
Alleluia, alleluia !
Gloire à ton Nom, Jésus Parole,
Gloire à ton Nom, Alleluia !
Alleluia, alleluia !

2.
Pâques et Ascension
Toi le Vivant du jour de Pâques,
Toi le Vivant, Ressuscité,
Alleluia, alleluia !
Fils premier-né auprès du Père,
Fils premier-né, alleluia !
Alleluia, alleluia !

3.
Pentecôte
Souffle de Dieu qui fais revivre,
Souffle de Dieu, Esprit très saint,
Alleluia, alleluia !
Flamme en nos coeurs, tu nous réveilles,
Flamme en nos coeurs, alleluia !
Alleluia, alleluia !