RECEVOIR ET DONNER, SE SENTIR EXISTER.
ÊTRE À DEUX POUR AVANCER, À COEURS DÉVOILÉS
DES ÉTOILES PLEIN LES YEUX, DU SOLEIL POUR DEUX
DU BONHEUR À PARTAGER, C'EST LA JOIE D'AIMER ! (bis)

1.
L'amour est un chemin où se révèle chacun
Chemin de bienveillance qui nous ouvre à la confiance
Il est l'heure de partir avec nos mille sourires
Il est l'heure de s'ouvrir pour inventer l'avenir

2.
L'amour est une danse où le temps est accompli
Une danse de vie qui remue notre espérance
Sentir battre le coeur de l'être tant désiré
Offrir tout le meilleur, tout ce qu'on ne peut garder

3.
L'amour est un voyage, bien au-delà des nuages
Voyage au bout d'un monde où liberté vagabonde
Marcher main dans la main pour tenir face au malheur
Grandir en vérité, en cultivant la douceur

4.
L'amour est un baiser qui nous fait tout oublier
Baiser plein de promesses qui nous remplit de tendresse
Goûter l'instant présent pour toucher les coins du ciel
Découvrir chaque jour que l'amour est éternel