1.
La maison de mon père
Est un joli palais
Bâti en belles pierres
C’est là que je suis née
La maison est assise
Près d’un joli voisin
Juste en plein coeur d’Assise
Tout près de Saint Rufin.

DANS LA MAISON DE MON PÈRE
LA SAGESSE EST JARDINIÈRE
C’EST LE COEUR DE LA MAISON
SA TENDRESSE ET SA RAISON.

2.
La maison de mon père
Est vêtue de ciel bleu
Et de cette lumière
Qui met le coeur à Dieu
Les couleurs à leur guise
Ont pavoisé l’Ombrie
Et les grands cieux d’Assise
Chaque jour me sourient.

3.
La maison de mon père
Voit des foules accourues
Se déclarant la guerre
Se battant dans les rues
Et déchirant l’Église
Le Pape et l’Empereur
Chacun réclame Assise
En semant la terreur.

4.
La maison de mon père
N’a pas volé mon coeur
Moi qui m’appelle Claire
Suivie de mes deux soeurs
Nous allons à l’Église
Cacher nos vies en Dieu
Et nous rêvons qu’Assise
Soit un morceau des cieux.