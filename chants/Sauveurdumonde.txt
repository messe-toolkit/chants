1.
Sauveur du monde, ô Maître unique,
Heureux celui qui donne tout.
Se perd lui-même et prend ton joug
Puis cherche en toi la route à suivre.

2.
Au fond du coeur, tu lui révèles
L’âpre secret du grain qui meurt,
Le sang versé, l’amour vainqueur,
Et cette croix qui nous relève.

3.
Il porte fruit dans la lumière
Et crie ton nom sur nos chemins,
Puis quand vient l’heure, dans tes mains,
Passe avec toi du monde au Père.