I.
Ecoute petite sœur, ce que je voulais te dire :
Qu’une âme unie à Jésus, est un vivant sourire
Qui le rayonne et qui le donne.

R.
Une âme unie à Jésus
est un vivant sourire
qui le donne et le rayonne ,
qui le donne et le rayonne .

1.
Rappelle-toi toujours qu’il te cherche et qu’il t’aime ;
Qu’il veut te transformer en un autre lui-même.

2.
Puisqu’Il demeure en toi, Il faut que tu le donnes,
Que partout et toujours ton âme le rayonne.

3.
Il veut te consacrer, par son cœur très aimant,
Afin qu’à travers tout, tu sois son sacrement.

F.
Voilà petite sœur, ce que je voulais te dire…