SOIS LOUÉ, MON SEIGNEUR,
LAUDATO SI ! LAUDATO SI !
JOIE DU CIEL DANS NOS COEURS,
AVEC FRANÇOIS CHANTONS MERCI !
LOUANGE AU CRÉATEUR,
NOTRE TERRE EST BELLE,
LOUANGE AU CRÉATEUR,
LAUDATO SI ! LAUDATO SI !

1.
Planète bleue, berceau des hommes,
Foyer de vie dans l’univers,
Mère accueillante et généreuse
Ouvrant la route à des humains,
Dieu des genèses te façonne.
Il te remet entre nos mains.

2.
Planète bleue, baignée d’aurores
Et sous le feu de tes couchants,
Soeur qui nous donne d’âge en âge
Tant de merveilles et de printemps,
Dieu des lumières en toi rayonne,
Tu le révèles à notre temps.

3.
Planète bleue, maison commune
Où chaque frère est un voisin,
Mère habitée d’un vivre ensemble
Qui nous rapproche de chacun,
Dieu fait craquer nos solitudes,
Nous ne ferons bientôt plus qu’un.

4.
Planète bleue, l’abri des pauvres
Et des légions d’abandonnés,
Soeur indignée par leur détresse
Et par leur faim de dignité,
Dieu par ta voix nous interroge :
Le cri des pauvres est-il premier ?

5.
Planète bleue dans les orages
Et l’ouragan des vents mauvais,
Mère affolée par les cyclones
Et les climats désorientés,
Dieu soit le Maître qui te garde,
Lui, le Seigneur qui t’a créée !

6.
Planète bleue sous la violence
Avec nos guerres et nos conflits,
Soeur assoiffée de paix durable
Plus forte encore que nos défis,
Dieu te soutient par sa puissance,
Il est l’Amour qui donne vie.

7.
Planète bleue, trésor fragile
À la merci des prédateurs,
Mère appauvrie par nos ravages
Quand nous vivons en grands seigneurs,
Dieu nous renvoie à l’Évangile :
Jésus se fait le Serviteur !

8.
Planète bleue, bonheur du monde
Quand la nature est préservée !
Soeur humiliée par nos demandes
Et nos folies d’enfants gâtées,
Dieu t’encourage à nous répondre :
Il faut savoir raison garder.

9.
Planète bleue, tu nous entraînes
Sur des chemins de conversion ;
Mère blessée par nos errances,
Inspire-nous des solutions !
Dieu nous ramène à des sagesses
Qui voient fleurir ce qui est bon.

10.
Planète bleue, poursuis ta marche
Jusqu’à la Rive ensoleillée !
Soeur éclairée par l’espérance
Que les vivants ont fait lever,
Dieu te dira quelle est ta place
Dans la maison d’éternité.