R.
Lève-toi et marche,
Dieu est ton ami,
Lève-toi et marche,
Sur la route avec lui.

1.
Toi qui as mal au fond du cœur
Dans les blessures du malheur,
Prends ta misère sur ton dos
Prends ta civière et ton fardeau.

2.
Toi que la honte a humilié
Avec la lèpre et ton péché
Prends une larme dans tes mains :
C´est ton étoile du matin.

3.
Toi qui te donnes sans compter,
Sans voir de fruits dans tes vergers,
Prends l´espérance à bout de bras,
Apprends à vivre pas à pas.

4.
Toi qui as peur des lendemains,
Nourri de doute et de chagrins,
Prends ton soleil de chaque jour,
Il te suffit d´un peu d´amour.

5.
Toi que personne n´a plongé
Au cœur des sources d´amitié,
Prends une fleur de ton jardin,
Et va fleurir ceux qui n´ont rien.

6.
Toi que font taire les puissants
A coups de fouet, à coups d´argent,
Prends ta colère à chaque poing
Défends les droits de l´être humain.

7.
Toi que la soif a desséché
Dans ton désert de pauvreté.
Prends ta souffrance dans un cri :
Quelqu´un t´écoute dans ta nuit.