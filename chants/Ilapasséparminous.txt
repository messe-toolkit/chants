STANCE
Il a passé parmi nous,
il ne possédait rien,
mais ce qu’il est,
il nous le donne
et nous voici
les fils de son Père,
et les frères de chaque homme.

R.
Notre amour et notre vie,
c’est toi, Seigneur Jésus.

VERSETS

1.
Quant à vous, aimez :
Dieu vous a aimés le premier.

2.
Si tu n’aimes pas ton frère, que tu vois
aimeras-tu Dieu, qui demeure invisible ?

3.
Pour connaître que tu aimes les enfants de Dieu,
aime Dieu, garde ses commandements.