TOI QUI NOUS ÉMERVEILLES,
ES-TU LE VRAI MESSIE ?
TOI, LE BERGER FIDÈLE,
ES-TU LE ROI QUI DOIT VENIR ?

1.
Oui, JE SUIS ROI dès l’origine,
Quand Dieu vous tire de la boue.
JE SUIS LE VERBE qui appelle :
Levez les yeux ! Redressez-vous !

2.
Oui, JE SUIS ROI dès ma naissance.
Je n’ai de trône qu’un berceau.
Je viens sans armes ni menaces.
Je vous apporte un sang nouveau.

3.
Oui, JE SUIS ROI pour la justice :
Ceux qui ont faim sont rassasiés.
Voyez comment je sers à table !
Mon pain se donne sans compter.

4.
Oui, JE SUIS ROI sous les épines.
C’est la couronne du mépris.
Je suis le grain jeté en terre.
Je porterai beaucoup de fruit.

5.
Peuple de rois, je vous précède.
Dieu notre Père vous attend.
Quittez vos terres d’esclavage !
Venez à moi ! Je suis vivant !

TOI QUI NOUS ÉMERVEILLES,
TU ES LE VRAI MESSIE.
TOI LE BERGER FIDÈLE,
TU ES LE ROI QUI NOUS CONDUIT !