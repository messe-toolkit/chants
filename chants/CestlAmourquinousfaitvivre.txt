C’est l’amour qui nous fait vivre,
Comme un grand soleil qui vient de toi.
L’olivier de ton Alliance
Porte le printemps du premier jour.
Que ta joie toujours demeure
Dans nos vies et dans nos coeurs
C’est l’amour qui nous fait vivre
Comme un grand soleil qui vient de toi.