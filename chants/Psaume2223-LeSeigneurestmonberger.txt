1.
Le Seigneur est mon berger,
je ne manquerai de rien.
Il me guide sur la bonne voie,
parce qu’il est le berger d’Israël.

Le Seigneur est mon berger,
je ne manquerai de rien.
Il me met au repos
dans des près d’herbe fraîche.

2.
Il me conduit au calme près de l’eau.
Il ranime mes forces.
Même si je passe par la vallée obscure,
je ne redoute aucun mal.

3.
Tu me conduis, tu me défends
voilà ce qui me rassure.
Tu m’accueilles en versant sur ma tête
un peu d’huile parfumée.