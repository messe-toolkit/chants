1.
D’ici aux confins de la Terre,
Va résonner notre prière.
Nous chanterons car tout est accompli
et nous vivons de l’Esprit.
\n
Qu’éclate en toutes les nations,
le réveil d’une génération.
Nous danserons car tout est accompli
et nous crions Jésus-Christ.

R.
Louez-le louez-le, qui que vous soyez
Louez-le louez-le, où que vous soyez
Louez-le louez-le, en toutes circonstances
Louez-le louez-le, voici que Dieu avance

2.
Et de l’Europe et de l’Afrique,
de l’Asie jusqu’en Amérique.
N’ayons plus peur car tout est accompli
et nous vivons de l’Esprit.
\n
Jeunes et vieux dans la louange,
que chantent l’Eglise et les anges.
À Lui l’honneur car tout est accompli
et nous crions Jésus-Christ

3.
Du Nord au Sud de l’Est à l’Ouest
au chant de notre Père céleste.
Dansons ensemble car tout est accompli
et nous vivons de l’Esprit.
\n
Qu’elles s’appellent, qu’elles jubilent
au cœur des mers toutes les îles.
Courons ensemble car tout est accompli
et nous crions Jésus-Christ.

4.
Chrétiens d’Orient et d’Occident,
unis autour du Dieu vivant.
En Jésus-Christ car tout est accompli
et nous vivons de l’Esprit.
\n
Et qu’aux quatre coins de la France,
s’élève un chant naisse une danse.
Pour le Messie car tout est accompli
et nous crions Jésus-Christ
