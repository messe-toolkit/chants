NOTRE DAME DU GRAND POUVOIR.
VOICI NOS VIES SOUS TON REGARD.
QU’EN TOI NOS COEURS TROUVENT L’ESPOIR.
POUR AVANCER JUSQU’À TE VOIR.

1.
Quand on avance à pas perdus
Que la confiance a disparu
Nous t’en prions, Marie, maman
Viens au secours de tes enfants.

2.
Entends le cri des opprimés
Des sans abris, des réfugiés.
Nous t’en prions, Marie, maman
Viens au secours de tes enfants.

3.
Dans la clameur des hôpitaux,
Tant de souffrances et tant de maux.
Nous t’en prions, Marie, maman
Viens au secours de tes enfants.

4.
Qu’entre nos mains naisse la paix
Sur cette terre en mal d’aimer.
Nous t’en prions, Marie, maman,
Viens au secours de tes enfants.