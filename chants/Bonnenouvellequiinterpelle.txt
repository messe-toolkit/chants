Bonne nouvelle
Qui interpelle,
La pauvreté
Est visitée.

Les prisonniers
Sont délivrés,
Les opprimés
Sont libérés.

Qui ne voit plus
Trouve la vue,
Dans ses promesses,
Dieu est tendresse.

C’était écrit,
C’est Isaïe,
Mais aujourd’hui
C’est accompli.