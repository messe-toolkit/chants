Nous te rendons grâce, ô notre Père !
Nous te rendons grâce, pour tes merveilles !

Jubilate Deo, cantate Domino !
Jubilate Deo, cantate Domino !

1.
Ô vous tous qui servez le Seigneur notre Dieu,
Par des chants de joie louez-le sans vous lasser.

2.
Rendez grâce au Seigneur et jouez pour son nom,
Par des chants nouveaux acclamez le Tout-Puissant !

3.
La Parole de Dieu est plus sûre qu'un roc,
Toutes ses actions sont justice et vérité.

4.
De l'amour du Seigneur est rempli l'univers,
Et par sa Parole le monde fut créé.

5.
L'eau des mers, les abîmes, la terre et le ciel :
Tout est dans sa main et demeure sous sa Loi.

6.
Que la terre s'incline et l'adore avec foi,
Car c'est Lui qui donne la vie à l'univers !

7.
Les desseins du Seigneur nous dépassent toujours :
Il conduit le monde selon sa volonté.

8.
Bienheureux d'âge en âge le Peuple de Dieu
Car il est comblé de sa grâce, chaque jour !

9.
Notre Père des cieux nous protège en tout lieu,
Il connaît le cœur de chacun de ses enfants.

10.
La puissance du monde n'est rien devant Dieu,
L'homme le plus fort ne peut rien sans le Seigneur.

11.
Le Seigneur a les yeux sur les hommes au cœur droit,
Il les fera vivre à jamais dans sa maison.

12.
De tout cœur, attendons la venue du Seigneur :
Il nous sauvera de la mort et de tout mal.

13.
Que nos cœurs soient remplis de confiance et de joie,
Car nous avons cru à l'amour du Dieu vivant !

14.
Ton amour soit sur nous, ô Seigneur, Dieu très saint,
Comme notre espoir est en toi, par-dessus tout !

15.
Gloire au Père très bon, à son Fils Jésus Christ ;
Gloire au Saint-Esprit, maintenant et pour toujours !