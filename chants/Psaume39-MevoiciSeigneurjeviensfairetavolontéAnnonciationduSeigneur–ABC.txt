ME VOICI, SEIGNEUR,
JE VIENS FAIRE TA VOLONTÉ.

1.
Tu ne voulais ni offrande ni sacrifice,
tu as ouvert mes oreilles ;
tu ne demandais ni holocauste ni victime,
alors j’ai dit : « Voici, je viens.

2.
« Dans le livre, est écrit pour moi
ce que tu veux que je fasse.
Mon Dieu, voilà ce que j’aime :
ta loi me tient aux entrailles. »

3.
J’annonce la justice
dans la grande assemblée ;
vois, je ne retiens pas mes lèvres,
Seigneur, tu le sais.

4.
Je n’ai pas enfoui ta justice au fond de mon coeur,
je n’ai pas caché ta fidélité, ton salut ;
j’ai dit ton amour et ta vérité
à la grande assemblée.