1.
Celui qui vient à moi pour m’écouter
Et appliquer ce que je dis
Est comme un homme
Qui a su bâtir sa maison sur le roc
En posant bien ses fondations

Et la pluie est tombée
Les flots sont montés
Le vent a soufflé
Contre la maison
Oui la pluie est tombée
Les flots sont montés
Mais la maison a résisté
Grâce aux fondations

La maison du sage
Est fondée sur le roc

2.
Celui qui vient à moi pour m’écouter
Sans appliquer ce que je dis
Est comme un insensé qui a construit
Sa maison sur le sable
Sans la moindre fondation

Et la pluie est tombée
Les flots sont montés
Le vent a soufflé
Tel un ouragan
Oui la pluie est tombée
Les flots sont montés
Et la maison s’est effondrée
Réduite à néant

Le fou sur le sable a bâti sa maison
Le sage sur le roc…