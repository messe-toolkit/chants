Fils bien-aimé du Père. Seigneur, prends pitié.
Sagesse éternelle et Verbe de Dieu. Seigneur, prends pitié.

Victime offerte pour nos péchés. Ô Christ, prends pitié.
Pain vivant descendu du ciel. Ô Christ, prends pitié.

Lumière éclairant nos ténèbres. Seigneur, prends pitié.
Berger qui nous conduis dans ta maison. Seigneur, prends pitié.