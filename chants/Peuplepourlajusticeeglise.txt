1.
Peuple pour la justice,
Ton Dieu t’invite à sortir de prison
Pour défendre avec lui
La liberté de tous les hommes ;
Peuple, jadis esclave,
N’entends-tu pas
La voix de l’Esprit Saint
Dans la rumeur du monde ?

R.
RIEN NE POURRA TE SÉPARER.
DE L’INVINCIBLE AMOUR DE DIEU;
DÉJÀ TU ES VICTORIEUX
EN JÉSUS-CHRIST QUI T’A SAUVÉ:
IL EST SEIGNEUR!

2.
Peuple pour le service,
Ton Dieu t’invite à passer par la mer
Pour apprendre de lui
Qu’il est le Dieu de l’impossible ;
Peuple de la Promesse,
N’entends-tu pas
La voix de l’Esprit Saint
Dans la rumeur du monde ?

3.
Peuple pour l’espérance,
Ton Dieu t’invite à marcher au désert
Pour que s’ouvre en ton coeur
Le seul chemin de l’Évangile ;
Peuple tiré des sables,
N’entends-tu pas
La voix de l’Esprit Saint
Dans la rumeur du monde ?

4.
Peuple pour le Royaume,
Ton Dieu t’invite à payer de ton sang
Pour bâtir la cité
Où témoigner de la Parole ;
Peuple de son Alliance,
N’entends-tu pas
La voix de l’Esprit Saint
Dans la rumeur du monde ?

5.
Peuple pour les plus pauvres,
Ton Dieu t’invite à l’amour du prochain,
Au souci du dernier,
À la rencontre sans barrière ;
Peuple choisi par grâce,
N’entends-tu pas
La voix de l’Esprit Saint
Dans la rumeur du monde ?

6.
Peuple pour la louange,
Ton Dieu t’invite aux combats de la paix,
Au partage des biens
Et au pardon de toute offense ;
Peuple qui es son peuple,
N’entends-tu pas
La voix de l’Esprit Saint
Dans la rumeur du monde ?