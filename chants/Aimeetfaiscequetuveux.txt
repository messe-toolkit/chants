R.
AIME, AIME, AIME, DELIVRE-TOI, MON FRERE
AIME, AIME, AIME,
ET FAIS CE QUE TU VEUX, (Bis)

1.
Tu as faim de liberté, mais tu veux tout posséder ;
c'est l'argent qui te possède.
Porte donc dans tes greniers des trésors de charité,
et tu seras libéré.

2.
Tu as soif de vérité, mais tu tiens à tes idées ;
Tu t'enfermes dans ta case.
Apprends-toi à écouter patiemment, sans te lasser,
et tu seras libéré,

3.
Tu voudrais tout redresser, le monde et la société ;
mais tu vis dans ta coquille.
Apprends-toi à partager, à vivre en communauté,
et tu seras libéré,

4.
Tu prétends tout expliquer et percer tous les secrets ;
mais tu te cognes la tête.
Laisse toi illuminer par l'Esprit de vérité,
et tu seras libéré.