1.
Jésus Christ...
Il nous mène vers son Père
Dans la nuit, Il est lumière.
Jésus Christ...
Il nous donne un coeur de frère
Il est pain, fruit de la terre.

PRENEZ,
IL EST LA VIE PARTAGÉE,
MANGEZ,
ET EN LUI VOUS VIVREZ.

2.
Jésus Christ...
Par sa croix, Il est semence
En chacun, Il est présence.
Jésus Christ...
Il est source d’espérance
Il est pain en abondance.

3.
Jésus Christ...
Il s’en va et nous appelle
Il nous dit : « Restez fidèles. »
Jésus Christ...
Il revient : bonne nouvelle !
Il est pain, vie éternelle.