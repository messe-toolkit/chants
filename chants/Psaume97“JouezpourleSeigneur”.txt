ACCLAMEZ LE SEIGNEUR CAR IL VIENT,
ACCLAMEZ LE SEIGNEUR CAR IL VIENT
POUR GOUVERNER LA TERRE,
POUR GOUVERNER LE MONDE AVEC JUSTICE.

1.
Jouez pour le Seigneur sur la cithare,
Sur la cithare et tous les instruments.
Au son de la trompette et du cor,
Acclamez votre roi, le Seigneur.

2.
Que résonnent la mer et sa richesse,
Le monde et tous ses habitants.
Que les fleuves battent des mains
Et que les montagnes chantent leur joie.