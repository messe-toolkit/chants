R.
OUVRIER DE PAIX SUR NOTRE TERRE,
DIEU, TU NOUS RASSEMBLES PAR TON FILS.
CRÉATEUR D’AMOUR PARMI NOS FRÈRES,
TU REDONNES SOUFFLE À NOTRE ESPRIT.

1.
Sur la route où Jésus Christ nous a rejoints,
Nous découvrons que tu as bien figure humaine.
Le coeur ouvert aux désespoirs qui nous traversent,
Tu dis les mots qui font surgir des lendemains.

2.
Habités par la parole du Sauveur,
Nous bâtirons sur le rocher de la justice.
Avec ta force un monde neuf est à construire ;
Nous proclamons que sur le mal tu es vainqueur.

3.
Tu promets qu’un ciel nouveau pourra s’ouvrir ;
Nous choisissons le parti pris de l’espérance.
Comment tenir sur cette voie de résistance ?
Que notre vie soit à l’image de ton Christ !