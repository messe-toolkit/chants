1.
Marie, mère de Jésus-Christ,
Femme entre toutes bénies,
Eclaire notre monde !
Que brille sur nous ton visage
A l’heure où nul n’échappe à la nuit.

2.
Marie, temple de l’Esprit Saint,
Arche d’alliance en chemin,
Visite notre monde !
Fais naître la joie sans partage
A l’heure où paraîtra le matin.

3.
Marie, vierge et mère du Roi,
Reine au secret de la foi,
Protège notre monde !
Que sont les puissants et les sages
Si Dieu, des plus petits, fait sa joie ?

4.
Marie, forte au milieu des pleurs,
Forte et blessée de douleur,
Console notre monde !
Ravive en chacun le courage
Et prie pour le pardon des pécheurs.

5.
Marie, pure de tout péché,
Eve au jardin recréée,
Enfante notre monde !
Par toi recommence un autre âge :
L’Eglise entre tes mains s’est confiée.