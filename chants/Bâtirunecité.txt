R.
Bâtir une cité aujourd'hui,
Où Dieu se fait le maçon.
Bâtir une cité d'avenir,
Dont Il fera les fondations.

1.
Bâtir une cité fraternelle
Terre promise, terre nouvelle !
Bâtir une cité fraternelle
Où son Amour envahira nos maisons.

2.
Bâtir une cité d'espérance
Terre promise, terre nouvelle !
Bâtir une cité d'espérance
Où son Amour envahira nos maisons.

3.
Bâtir une cité de lumière
Terre promise, terre nouvelle !
Bâtir une cité de lumière
Où son Amour envahira nos maisons.

4.
Bâtir une cité de partage
Terre promise, terre nouvelle !
Bâtir une cité de partage
Où son Amour envahira nos maisons.

5.
Bâtir une cité de verdure
Terre promise, terre nouvelle !
Bâtir une cité de verdure
Où son Amour envahira nos maisons.