R.
COMME À CANA,
Ô MARIE,
COMME À CANA,
VIENNE JÉSUS,
VIENNE LA JOIE !

1.
Mère de Jésus,
Quand nous n’avons plus rien à chanter,
Qu’il rende la flamme
Aux tables sans âme
De notre banquet !

2.
Mère des vivants,
Quand se meurt le chant sous notre ciel,
Obtiens-nous encore
À pleines amphores
Ce vin de vermeil !

3.
Mère du Sauveur
Ouvre notre coeur, donne la foi,
Que l’eau de nos peines
Devienne fontaine
D’amour et de joie !