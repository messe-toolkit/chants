R.
AVEC JOIE, TRANSMETS LA PAROLE,
AVEC JOIE, TRANSMETS LA PAROLE,
AVEC JOIE, TRANSMETS LA PAROLE,
QUE TU REÇOIS, QUE TU REÇOIS.

1.
C’est un feu dans la nuit qui réchauffe dans le froid.
La Parole qui te dit : « Tu comptes beaucoup pour moi. »
Pour moi, pour moi…

2.
C’est un phare qui luit et qui rassure ton pas.
La Parole qui te dit : « Je suis là tout près de toi. »
De toi, de toi…

3.
C’est l’eau qui rafraîchit qui revivifie ton pas
La Parole qui te dit : « Je t’aime et je crois en toi. »
En toi, en toi…

4.
C’est le pain qui nourrit et c’est le pain du repas
La Parole qui te dit : « Tout homme est fait pour la joie. »
La joie, la joie…