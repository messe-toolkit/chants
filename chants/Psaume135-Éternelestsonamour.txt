R.
ETERNEL EST SON AMOUR !
ETERNEL EST SON AMOUR !

1.
Rendez grâce au Seigneur : il est bon.
Il fit sortir Israël de l'Egypte,
d'une main forte et d'un bras vigoureux.

2.
Lui qui fendit la mer rouge en deux parts,
et fit passer Israël en son milieu,
y rejetant Pharaon et ses armées.

3.
Lui qui mena son peuple au désert,
pour donner la terre en héritage,
en héritage à Israël, son serviteur.

4.
Il se souvient de nous, les humiliés,
il nous tira de la main des oppresseurs.
Rendez grâce au Dieu du ciel.