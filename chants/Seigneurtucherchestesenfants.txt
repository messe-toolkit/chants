R.
Seigneur, Seigneur
Oh ! Prends dans ton Église
Tous nos frères, de la terre
Dans un même amour

1.
Seigneur tu cherches tes enfants,
car tu es l'amour
Tu veux unir tous les vivants,
grâce à ton amour

2.
Seigneur tu sauves par ta mort,
car tu es l'amour
Fais-nous les membres de ton corps,
grâce à ton amour

3.
Seigneur tu calmes notre faim,
car tu es l'amour
Partage à tous le même pain,
grâce à ton amour

4.
Seigneur tu vois le monde entier,
car tu es l'amour
Fais-lui trouver son unité,
grâce à ton amour

5.
Seigneur tu nous promis la paix,
car tu es l'amour
Étends son règne désormais,
grâce à ton amour