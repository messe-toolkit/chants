1.
Dans nos cités de Samarie
Tu nous rejoins au plein midi
De notre soif.
Es-tu celui qui peut puiser
Au fond du coeur le plus troublé
L’eau vive d’un espoir ?

R.
TOI QUI FAIS JAILLIR L’EAU VIVE,
DONNE-NOUS TON ESPRIT SAINT !

2.
Viens nous donner cette eau jaillie
Aux sources mêmes de ta vie :
La Vérité.
Tu es celui dont le regard
Au fond du coeur nous aide à voir
Un peu de vraie clarté.

3.
Toi le Prophète, éveille-nous,
Dis-nous l’endroit du rendez-vous
Avec l’Esprit.
L’heure est venue d’adorer Dieu
Au fond du coeur où ton grand Feu
Nous fait eucharistie.

4.
Toi le passant, tu nous connais ;
Qui serais-tu sinon la paix
Venue d’en haut ?
Viens la semer comme un froment
Au fond des coeurs où Dieu attend
La joie des blés nouveaux.

5.
Sur ta parole nous croyons ;
Resteras-tu dans nos maisons,
Soleil du jour ?
Dans nos modernes Samarie,
Au fond du coeur tu es le puits,
Fontaine de l’amour.