1.
Seigneur Jésus, tu es présent dans ton Eucharistie,
Dans cette hostie nous t´adorons et nous te magnifions.

2.
Toi qui es Dieu, toi qui es Roi, tu nous as tout donné.
Tu es le Christ, tu es l´Agneau immolé sur la croix.

3.
Dans ta passion tu as porté chacun de nos péchés.
Ton sang versé nous a lavés et nous a rachetés.

4.
Saint Jean a vu le sang et l´eau jaillir de ton côté.
Ton Esprit Saint nous est donné comme un fleuve d´eau vive.

5.
Oui, nous croyons à ta victoire par ta résurrection.
Oui, nous croyons que dans ta gloire à jamais nous vivrons.