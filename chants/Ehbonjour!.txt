Bonjour, bonjour, bonjour, bonjour.
Bonjour, bonjour, bonjour, comment vas-tu ?
Bonjour, bonjour, bonjour, bonjour.
Bonjour, bonjour, bonjour, comment vas-tu ?

R.
EH, BONJOUR, COMMENT VAS-TU ? REJOINS TES AMIS, TU ES LE BIENVENU. EH, BONJOUR, COMMENT VAS-TU ? REJOINS TES AMIS POUR ÉCOUTER JÉSUS.

1.
Avec nos mains, avec nos mains,
On sait accueillir, on sait accueillir,
Et déjà les sourires ont jailli.
Avec ce chant, avec ce chant,
On a fait danser, on a fait danser,
Au fond de nos cœurs, une amitié.

2.
Et chaque fois, et chaque fois,
Que l’on se verra, que l’on se verra,
On partagera des moments de joie.
Et tout à l’heure, et tout à l’heure,
Quand on partira, quand on partira,
On se dira : “à la prochaine fois”.

Bonjour, bonjour, bonjour, bonjour.
Bonjour, bonjour, bonjour, comment vas-tu ?
Bonjour, bonjour, bonjour, bonjour.
Bonjour, bonjour, bonjour, comment vas-tu ?