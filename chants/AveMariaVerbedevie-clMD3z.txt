1.
Marie, mère de Dieu et mère des hommes,

2.
Marie, Vierge bénie entre toutes les vierges,

3.
Marie, mère de l’Amour et mère de l’espérance,

4.
Marie, Vierge très humble et Vierge très pure,

6.
Marie, étoile du matin et porte du Ciel,

7.
Marie, reine des anges et reine des Saints,

9.
Marie, secours des chrétiens et salut des malades,

F.
Avec toi, nous sommes dans la joie, avec toi nous louons notre Roi,
Ave, Ave, Ave Maria, Ave, Ave, Ave Maria !