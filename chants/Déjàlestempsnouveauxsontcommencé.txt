1.
Déjà les temps nouveaux sont commencés !
Voyez ces mains qui partagent,
Ces cœurs qui pardonnent.
Au Seigneur rendez gloire :
Les temps nouveaux sont commencés !

2.
Pourquoi douter de l'œuvre de l'esprit ?
En vous sa force et sa grâce,
Le cri vers le Père !
Accueillez sa présence :
Les temps nouveaux sont commencés !

3.
Le pauvre et l'humilié seront bénis !
L'amour tiendra sa promesse,
Qu'ils gardent confiance.
Qu'ils relèvent la tête :
Les temps nouveaux sont commencés !

4.
Le jour de proche en proche se répand !
La nuit du monde s'achève,
La grâce l'emporte.
Le Seigneur vient en gloire :
Les temps nouveaux sont commencés !