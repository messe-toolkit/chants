R.
Tu as besoin de nous
Pour que ton Règne vienne,
Tu as besoin de nous,
Tes enfants ;
Tu veilles sur nos vies
Et ton amour nous mène,
Tu veilles sur nos vies,
Tendrement !

1.
Nous avons pris rendez vous avec toi,
Tu nous veux tous ensemble,
Autour du Livre où résonne ta voix,
Seigneur, tu nous rassembles.

2.
Autour de toi, les petits et les grands,
Tu nous veux tous ensemble,
Pour accueillir ceux qui sont différents
Ou ceux qui nous ressemblent.

3.
La terre entière est encore en labours,
Tu nous veux tous ensemble,
Pour y semer ta promesse d’Amour,
Le grain de ta présence !