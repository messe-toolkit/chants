1.
Dis-nous, Marie de Magdala,
Ce que toi-même as vu là-bas !
Au petit jour tu es allée
Près du tombeau du Bien-Aimé :
Voici que la pierre était roulée ! (bis)

2.
Tu vas trouver Simon et Jean ;
Tous deux arrivent en courant.
Ils voient la tombe où restent seuls
Les bandelettes et le linceul,
Et Jean dit sa foi dans le Vivant. (bis)

3.
Mais toi, Marie, près du tombeau,
Tu es livrée à tes sanglots !
Vêtus de blanc, deux messagers
Te parlent au coeur : « Pourquoi pleurer ? »
Alors ta détresse est apaisée. (bis)

4.
Auprès de toi quelqu’un paraît,
Tu vois en lui le jardinier.
Quand il te dit « Qui cherches-tu ? »
Ton être crie le mot Jésus.
Alors il t’appelle par ton nom. (bis)

5.
Jésus s’adresse à toi, Marie,
Et tu lui clames « Rabbouni ! »
Mais tu ne peux le retenir ;
Vers Dieu son Père il doit partir.
Debout ! Va de suite l’annoncer ! (bis)