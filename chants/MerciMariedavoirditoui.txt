RIEN N’EST IMPOSSIBLE À DIEU
RIEN N’EST IMPOSSIBLE
C’EST L’ANGE GABRIEL
QUI T’A DONNÉ DES AILES
MERCI MARIE
D’AVOIR DIT OUI

1.
Tu fais pousser des fleurs
Où manque la lumière
Tu entrouvres les coeurs de pierre
Tu vas trouver la faille
D’où peut jaillir l’Amour
Tu recherches toujours

2.
Tu nous montres un chemin
Où la simplicité
Est le plus beau moyen d’aimer
Tu agis en silence
Dans un monde bruyant
Ta présence est un chant

3.
Tu habilles le soir
Comme un ciel étoilé
Pour redonner l’espoir brisé
Tu te penches sur nous
Tout près de nos blessures
Ton regard nous rassure

4.
Tu attends patiemment
Quand nous sommes trop loin
Et toujours tu nous tends la main
Tu veux nous laisser libres
En nous faisant trouver
Le chemin pour aimer