1.
Si tu es mon ami
Et je crois que tu l’es
Nous pourrons nous parler
Sans jamais nous renier
De mon peuple trop fier
Du tien qui a tant souffert
J’ai appris ta mémoire
Tu connais mon histoire

2.
Si tu es mon ami
Et je crois que tu l’es
Nous pourrons nous parler
Sans nous ex-communier
Du ciel auquel tu crois
Dont je doute quelquefois
Du Dieu qui est le tien
Et qui ressemble au mien

3.
L’amitié nous protège
De tous les sortilèges
La raison devient sage
Autant que le courage
Et nous dissiperons
Au soleil de nos pardons
Les nuages d’antan
Les ombres du présent

4.
Si tu es mon ami
Et je crois que tu l’es
Nous pourrons nous parler
Sans nous assassiner
Du rêve féminin
De la soif et de la faim
Des lois de la cité
De la vraie liberté

5.
Si tu es mon ami
Et je crois que tu l’es
Nous pourrons nous parler
Sans nous exterminer
À nous de partager
Les plus belles palmeraies
Les plus riches vergers
Sans nous les arracher

6.
Si tu es mon ami
Et je crois que tu l’es
Nous pourrons nous parler
En toute vérité
De mon peuple trop fier
Du tien qui a tant souffert
J’ai appris ta mémoire
Tu connais mon histoire