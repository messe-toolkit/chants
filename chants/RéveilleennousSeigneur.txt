Réveille en nous, Seigneur
La vie de Ton Esprit,
Le don de vérité,
La joie de Ton amour.