1.
Merci merci mon Dieu
Pour les matins en fête
Le soleil plein les cieux
Et la joie dans ma tête

R.
CHANTEZ DIEU AVEC VOS CHANSONS
SON AMOUR EST VRAIMENT UNIQUE
CHANTEZ DIEU AVEC VOS MUSIQUES
CÉLÉBREZ DIEU FILLES ET GARÇONS

2.
Merci merci mon Dieu
Pour les fleurs et les plantes
Les fous rires et les jeux
Et pour la vie qui chante

3.
Merci merci mon Dieu
Pour le grand tronc des chênes
Et pour les grands sapins bleus
Pour le sang dans mes veines

4.
Merci merci mon Dieu
Pour tous ceux de mon âge
Et dans le fond de mes yeux
Les autres ont ton visage