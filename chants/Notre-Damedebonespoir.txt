NOTRE-DAME DE BON-ESPOIR,
NOS COEURS TE GUETTENT DANS LA NUIT,
NOS YEUX ESPÈRENT TON REGARD,
SOIS NOTRE ÉTOILE D’AUJOURD’HUI,
SOIS NOTRE ÉTOILE.

1.
Prisonniers, nous cherchons une issue,
Ô Marie, conduis-nous vers Jésus !
Ton enfant brisera les verrous.
La joie de Dieu, c’est l’homme debout !

2.
Humiliés, nous tombons dans l’oubli,
Ô Marie, conduis-nous vers la Vie !
Du tombeau, Jésus-Christ s’est levé.
La joie de Dieu, c’est l’homme sauvé !

3.
Assoiffés, nous marchons sans appui,
Ô Marie, conduis-nous vers le puits !
Un Amour infini nous attend.
La joie de Dieu, c’est l’homme vivant !