R.
LES MAINS OFFERTES SEIGNEUR DEVANT TOI.
LES MAINS OUVERTES NOUS VENONS À TOI.

1.
Nous avons cherché, tout essayé et tant peiné.
Rien, désolé, dans nos filets, étais-tu là ?
Comment expliquer ce besoin vrai d’être associé
Besoin de paix, besoin d’aimer, besoin de Toi.

2.
Nous allons marcher vers ta clarté, vers ton été
Comme “envoyés” en peuple aimé, aimé de Toi.
Nous sommes restés pour partager ce Pain sacré
De Vérité « Venez, voyez ! » Le Pain, c’est Toi.

3.
Nous irons chanter, semer le blé et moissonner,
Et récolter, et partager, Tu seras là !
Nous irons chanter au monde entier tes chants de paix,
De vérité, réconciliés, et avec Toi.