INVITATOIRE
Dieu, viens à mon aide ! Seigneur à notre secours !
Gloire au Père et au Fils et au Saint Esprit, au Dieu qui est, qui était et qui vient,
Pour les siècles des siècles. Amen ! Alléluia !
HYMNE

1.
Splendeur du paradis en fête
Devant le trône de Dieu,
Les anges par myriades,
Illuminés de la gloire du Christ.

2.
Partis depuis les jours antiques
Chantant l'amour du Seigneur,
Prophètes, patriarches
Sont arrivés au pays de leur foi.

3.
Les pauvres, les chercheurs du règne
Tourné vers le Fils de David,
Annoncent la victoire
Du sang divin qui les a rassemblés.

4.
Servante rayonnant la gloire
Du Dieu qu'elle a enfanté,
La Vierge souveraine
Présente au Roi les pécheurs purifiés.

5.
Au cœur de la cité nouvelle
La voix des fils du Très-Haut
Résonne près du fleuve
Pour célébrer leur Sauveur et leur Dieu.

ANTIENNE 1
Je vis une foule immense que nul ne pouvait dénombrer :
Un peuple formé de toutes les nations se tenait devant le trône de Dieu.

PSAUME 109

1.
Oracle du Seigneur à mon Seigneur : « siège à ma droite
Et je ferai de tes ennemis le marchepied de ton trône. »

2.
De Sion le Seigneur te présente le sceptre de ta force :
Domine jusqu'au cœur de l'ennemi.

3.
Le jour où paraît ta puissance, tu es prince éblouissant de sainteté :
Comme la rosée qui naît de l'aurore je t'ai engendré.

4.
Le Seigneur l'a juré dans un serment irrévocable :
« Tu es prêtre à jamais selon l'ordre du roi Melchisédek ».

5..

5.ta droite se tient le Seigneur ; il brise les rois au jour de sa colère

6.
Au torrent il s'abreuve en chemin, c'est pourquoi il redresse la tête.
7.
Gloire au Père et au Fils et au Saint-Esprit, pour les siècles des siècles. Amen.

ANTIENNE 2
Dieu les a mis à l'épreuve et les a trouvés fidèles,
Il leur a donné la gloire du Royaume.

PSAUME 115

1.5

1. crois et je parlerai, moi qui ai beaucoup souffert.5

1.5

1.i qui ai dit dans mon trouble : « l'homme n'est que mensonge ».

3.5

1.mment rendrai-je au Seigneur tout le bien qu'il m?

4.
J'élèverai la coupe du salut, j'invoquerai le nom du Seigneur.
5.
Je rendrai mes promesses au Seigneur, oui, devant tout son peuple.
6.
Il en coûte au Seigneur de voir mourir les siens.
7.
Ne suis-je pas, Seigneur, ton serviteur, ton serviteur, le fils de ta servante,
Moi, dont tu brisas les chaînes.

8..

8. t'offrirai le sacrifice d'action de grâce, j'invoquerai le nom du Seigneur.
9.
Je tiendrai mes promesses au Seigneur, oui, devant tout son peuple.
10.
À l'entrée de la maison du Seigneur, au milieu de Jérusalem.
11.
Gloire au Père et au Fils et au Saint-Esprit, pour les siècles des siècles Amen !

ANTIENNE 3
Par ton sang, Seigneur Jésus, tu nous as rachetés pour Dieu,
De toute race, langue et nation ; tu as fait de nous un royaume de prêtres.

CANTIQUE BIBLIQUE NT9 (Ap-4-5)

1.)

1. es digne, Seigneur notre Dieu, de recevoir l'honneur, la gloire et .

2.)

1.est toi qui créas l'univers ; tu as voulu qu'il soit, il fut créé.
3.
Tu es digne, Christ et Seigneur, de prendre le Livre et d'en ouvrir les sceaux.
4.
Car tu fus immolé, rachetant pour Dieu, au prix de ton sang
Des hommes de toute tribu, langue, peuple et nation.

5.
Tu as fait de nous pour notre Dieu un royaume et des prêtres,
Et nous régnerons sur la terre

6.
Il est digne l'Agneau immolé, de recevoir puissance et richesse, sagesse et force,
Honneur, gloire et louange.

7.
Gloire au Père et au Fils et au Saint-Esprit, pour les siècles des siècles. Amen.

RÉPONS

R.S

R.eu des vivants, louange et gloire à toi !S

R.S

R. les as purifiés par ton sang répandu, enfants du Père, ils te rendent grâce.
2.
Tu les as nourris du pain de la vie, vainqueurs de la mort, ils t'acclament.
3.
Tu as partagé leurs épreuves, vivants, ressuscités, ils chantent.
CANTIQUE ÉVANGÉLIQUE

ANTIENNE
Heureux les saints dans le royaume du Christ :
Vêtus de robes blanches, ils suivent l'Agneau et partagent sa joie.

CANTIQUE DE MARIE

1.E

1.n âme exalte le Seigneur, exulte mon esprit en Dieu mon Sauveur !E

1.E

1. s'est penché sur son humble servante ; désormais tous les âges me diront bienheureuse.
3.
Le Puissant fit pour moi des merveilles ; saint est son nom !
4.
Son amour s'étend d'âge en âge sur ceux qui le craignent ;
Déployant la force de son bras, il disperse les superbes.

5..

5. renverse les puissants de leurs trônes, il élève les humbles..

5.
Il comble de biens les affamés, renvoie les riches les mains vides.
7.
Il relève Israël son serviteur, il se souvient de son amour,
De la promesse faite à nos pères en faveur d'Abraham et de sa race à jamais.

8.
Gloire au Père et au Fils et au Saint-Esprit pour les siècles des siècles. Amen !

INTERCESSION

R.N

R.eu saint, à Toi la puissance et la gloire.N

R.N

R.ur la Vierge Marie, la Mère de Jésus, pour saint Joseph le charpentier de Nazareth.N

R.N

R.ur les patriarches et les prophètes, pour les apôtres et les évangélistes.
3.
Pour tous ceux qui ont subi le martyre, pour les hommes et les femmes qui ont suivi le Christ.
4.
Pour tous les membres glorieux du corps du Christ, pour la foule innombrable des saints connus de Toi seul.
5.
Accueille nos frères qui sont morts, qu'ils partagent le sort des saints dans ta lumière.