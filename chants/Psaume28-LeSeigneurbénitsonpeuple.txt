LE SEIGNEUR BÉNIT SON PEUPLE
EN LUI DONNANT LA PAIX.

1.
Rendez au Seigneur, vous, les dieux,
rendez au Seigneur gloire et puissance.
Rendez au Seigneur la gloire de son nom,
adorez le Seigneur, éblouissant de sainteté.

2.
La voix du Seigneur domine les eaux,
le Seigneur domine la masse des eaux.
Voix du Seigneur dans sa force,
voix du Seigneur qui éblouit.

3.
Le Dieu de la gloire déchaîne le tonnerre,
et tous dans son temple s’écrient : « Gloire ! »
Au déluge le Seigneur a siégé ;
il siège, le Seigneur, il est roi pour toujours !