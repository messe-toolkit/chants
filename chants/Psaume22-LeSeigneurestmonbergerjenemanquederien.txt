R.
Le Seigneur est mon berger, rien ne saurait me manquer.

1.
Le Seigneur est mon berger?: je ne manque de rien.
Sur des prés d'herbe fraîche, il me fait reposer.

2.
Il me mène vers les eaux tranquilles et me fait revivre,
Il me conduit par le juste chemin pour l'honneur de son nom.

3.
Si je traverse les ravins de la mort, je ne crains aucun mal,
Car tu es avec moi, ton bâton me guide et me rassure.

4.
Tu prépares la table pour moi devant mes ennemis?;
Tu répands le parfum sur ma tête, ma coupe est débordante.

5.
Grâce et bonheur m'accompagnent tous les jours de ma vie?;
J'habiterai la maison du Seigneur pour la durée de mes jours.