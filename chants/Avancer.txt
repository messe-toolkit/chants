AVANCER,
AVANCER VERS LE LARGE
ET JETER SES FILETS QUELQUE PART
ENSEMBLE ET MALGRÉ TOUT.
AVANCER,
AVANCER VERS LE LARGE
ET JETER SES FILETS QUELQUE PART
ENSEMBLE ET JUSQU’AU BOUT.

1.
Partir vers l’inconnu, quelque part
Entrevoir l’invisible
Sans un sou risquer tout, quelque part
Car nous sommes un peu fous.
Nous n’avons qu’un seul but, un seul phare :
L’étoile inaccessible
Casse-cou, rendez-vous, quelque part
Nous irons jusqu’au bout.

2.
C’est un choix un combat, quelque part
C’est au coeur de nos villes
Un désir, de bâtir, quelque part
De croire en l’avenir
C’est l’appel, personnel, quelque part
À suivre l’indicible.
C’est vouloir se construire, quelque part
Et se découvrir libre.