R.
À LA LOUANGE DE TA GLOIRE
TES MAINS, SEIGNEUR, NOUS ONT PÉTRIS.
TU ES LE MAÎTRE DE L’HISTOIRE,
DIEU CRÉATEUR QUI NOUS CHOISIS.

1.
Un mot de toi, une étincelle
Et l’univers s’est embrasé :
Milliards d’étoiles et de planètes,
Que nul regard ne peut compter.

2.
Millions d’années sur notre terre
Et premiers germes de la vie…
Les plantes boivent la lumière
Et l’animal émet son cri.

3.
Voici que l’homme enfin se dresse
Pour l’aventure de l’esprit ;
Au pas à pas des découvertes
Il sort des brumes de la nuit.

4.
Son art s’éveille au fond des grottes ;
Béni soit l’homme de Lascaux,
Et tous ses frères qui nous portent
À regarder toujours plus haut !

5.
De temples d’or en cathédrales
La terre s’orne de joyaux :
Trésors qui disent au long des âges
La longue quête vers le beau.

6.
Fleurons des villes et des villages
Sortis des mains des bâtisseurs…
Leurs murs délivrent le message
Des grands vivants qui ont du coeur !