POUR RÉVEILLER SON COEUR JONAS A SU CHANGER,
EN TRAVERSANT LA PEUR, JONAS EST TRANSFORMÉ. (bis)

1.
Comme lui je peux refuser, je m’endors et j’oublie d’aimer.
Je deviens comme prisonnier, dire non, ah ça me connaît !

2.
Comme lui je peux m’échapper, je sais fuir, me faire oublier,
M’isoler, pour parfois bouder, les caprices, ah ça me connaît !

3.
Comme lui je peux m’enfermer, être seul et tout refuser.
De colères en méchancetés, les tempêtes, ah ça me connaît !

4.
Comme lui je peux écouter, dire un « oui » qui sait regretter.
Pour grandir il me faut changer, « obéir », ah ça me connaît !