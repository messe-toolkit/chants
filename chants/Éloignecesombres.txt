ÉCOUTE, ÉCOUTE
MÊME SI J’AI DU MAL À PARLER
IL FAIT NOIR SUR MA ROUTE,
MON COEUR VOUDRAIT TROUVER LA PAIX.

1.
Éloigne ces ombres
Qui viennent planer sur ma vie
Ces ombres de la mort
Ces ombres de la nuit
Ombres de mes faiblesses
Ombres de ma lâcheté.

2.
Éloigne ces ombres
Qui enchaînent ma liberté
Ces ombres déchirées
Ces ombres desséchées
Ombres de ma violence
Ombres de ma pauvreté.

3.
Éloigne ces ombres
Qui viennent détruire mon amour,
Ces ombres trop fanées
Ces ombres sans contour,
Ombres de mes silences,
Ombres de mes mains fermées.