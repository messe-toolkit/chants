R.
Ouvre nos yeux, Seigneur,
Fais-nous connaître nos fautes ;
Pour la gloire de ton nom,
Seigneur délivre-nous !

1.
Ne retiens pas contre nous
Les péchés de nos ancêtres
Que nous vienne bientôt la tendresse,
Car nous sommes à bout de force !

2.
Aide-nous, Dieu notre Sauveur,
Pour la gloire de ton Nom !
Délivre-nous, efface nos fautes,
Pour la cause de ton nom !