R.
Ô vive Flamme !
Vive Flamme d'Amour !
Ô vive Flamme,
Esprit Saint embrase-nous !

1.
Toi, le don du Père,
Toi, la source des eaux vives,
Toi, qui répands la grâce,
Viens en nos cœurs !

2.
Toi, père des pauvres,
Prends pitié de nos faiblesses,
Toi, qui donnes la force,
Viens en nos cœurs !

3
.
Esprit de sagesse,
Toi, l'Esprit de vérité,
Toi, qui nous illumines,
Viens en nos cœurs !