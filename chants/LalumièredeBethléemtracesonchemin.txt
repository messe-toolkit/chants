LA LUMIÈRE DE BETHLÉEM
TRACE SON CHEMIN
DE GRANDS PROJETS ESSENTIELS
EN TOUT PETITS RIENS
QUE L’ON CROIE TRÈS FORT AU CIEL
QUE L’ON N’Y CROIE MOINS
LA LUMIÈRE DE BETHLÉEM
PASSE PAR NOS MAINS.

1.
Un regard peut changer le monde
Un regard peut changer chacun
Dans le froid c’est la chaleur
Dans la nuit, une lueur
Et chaque maison devient
Maison du partage et du pain.

2.
Écouter peut changer le monde
Écouter peut changer chacun
Dans le froid c’est la chaleur
Dans la nuit, une lueur
Et chaque maison devient
Maison du partage et du pain.

3.
Se parler peut changer le monde…

4.
Une action peut changer le monde…

5.
Notre foi peut changer le monde…