1.
Au long du jour, le Seigneur m’envoie son amour,
Pendant la nuit, il veille, le Dieu de ma vie
Au sanctuaire, j’ai vu sa gloire et sa puissance
La joie aux lèvres, je dirai sans fin sa louange.

MON ÂME A SOIF DE TOI
JE TE CHERCHE, TOI MON DIEU
MON ÂME A SOIF DE TOI
VIENS, TOI QUE J’AIME, VIENS !

2.
Je crie de joie, tu es venu à mon secours.
Tu me soutiens, et mon âme s’attache à toi
Toute ma vie je vais te bénir, te louer
Lever les mains, en invoquant ton nom très saint.

3.
Tu es mon Dieu et je te cherche dès l’aurore
Oui, tout mon être aspire à vivre en ta présence
J’espère en toi, ô Dieu vivant, ô mon Sauveur
Mieux que la vie, ton amour comblera mon coeur.