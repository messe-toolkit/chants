R.
Mon Dieu, je te rends grâce.

1.
Quand j'ai crié vers toi, Seigneur,
Mon Dieu, tu m'as guéri ;
Seigneur, tu m'as fait remonter de l'abîme
Et revivre quand je descendais à la fosse.

2.
Fêtez le Seigneur, vous, ses fidèles,
Rendez grâce en rappelant son nom très saint.
Sa colère ne dure qu'un instant,
Sa bonté, toute la vie ;

3.
Avec le soir, viennent les larmes,
Mais au matin, les cris de joie.
Tu as changé mon deuil en une danse,
Mes habits funèbres en parure de joie.

4.
Que mon cœur ne se taise pas,
Qu'il soit en fête pour toi,
Et que sans fin Seigneur, mon Dieu,
Je te rende grâce !