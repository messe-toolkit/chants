Jésus paraît. Il vient au fleuve.
Il dit à Jean saisi d’effroi :
“Baptise-moi”. Les flots s’émeuvent.
Qui donc pourrait laver
Ce Corps sacré ?

Qu’importe, Jean, que tu sois digne ?
Laisse accomplir ce qu’à l’instant
Nul ne comprend.
Il donne un signe
Pour vivre dès ce jour
Sa loi d’amour.

Il prend sur lui le mal du monde.
Il descend nu, tel un pécheur,
Lui le Seigneur,
Dans l’eau profonde.
Il va, martre du sort,
Chercher la mort.

Pourquoi frémir, abîmes sombres,
Quand le dragon qui vous hantait
Se voit défait ?
Au cœur de l’ombre
Descend le Fils de Dieu,
Lumière et Feu.

Jésus remonte sur la rive.
D’avoir baigné ses membres saints,
Tes flots, Jourdain,
Sont flots d’eau vive.
En toi le monde entier
Est rénové.

Eau sépulcrale et maternelle
D’où le vieil homme enseveli
A resurgi
En vie nouvelle
Pour être proclamé
Fils bien-aimé.