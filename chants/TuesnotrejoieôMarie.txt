1.
Tu es notre joie, ô Marie,
Servante du Seigneur.
Tu as obéi sans réserve à son choix.
Tu as accueilli sa Parole en ta foi.
Reine des fidèles,
Mère de Dieu.

2.
Tu es notre joie, ô Marie,
Demeure de l’Esprit.
Tu as enfanté pour le monde un Sauveur.
Tu as médité ce mystère en ton coeur,
Reine des apôtres,
Mère de Dieu !