R.
TA VIE, NUL NE LA PREND,
MAIS C’EST TOI QUI LA DONNES.
TON AMOUR EST SI GRAND
QU’IL ATTIRE TOUT HOMME.

1.
Et tu te laisses faire
Sans un cri sans un mot,
Habillé de lumière
L’amour est ton manteau.

2.
Familier des souffrances,
Enveloppé de paix,
Habillé de silence,
Tu livres ton secret.

3.
Témoin de l’espérance
Jusqu’à ton dernier cri,
Habillé de confiance,
Tu remets ton esprit.