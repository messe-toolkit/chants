R.
La joie de notre coeur
Vient du Seigneur,
Alleluia, Alleluia !

1.
Lancez des cris de joie pour le Seigneur,
Applaudissez,
Sans fin criez de joie pour le Seigneur,
Vous qui l’aimez !

2.
Jouez de vos guitares, de vos violons,
Jouez pour lui !
Inventez des musiques et des chansons,
Chantez merci !

3.
Il tient parole, il fait ce qu’il promet
A ses amis,
Pour ceux qui sont fidèles, son Amour
Est infini !

4.
D’un souffle il a fait naître l’Univers,
Le bleu du ciel,
Les océans qui dansent avec la terre
Et le soleil.

5.
Que toute la planète soit saisie
Par son regard,
Les habitants du monde trembleront
Devant sa gloire !

6.
Heureux son peuple, heureuse la nation
Qu’il s’est choisis,
Du haut du ciel il veille tendrement
Sur ses amis.

7.
C’est Dieu qui nous délivre de la mort
Jour après jour,
Dieu seul est notre force et nous puisons
Dans son amour !

8.
Il est comme un rempart, un bouclier
Dans notre vie,
La Joie qui étincelle dans nos coeurs,
Nous vient de Lui !