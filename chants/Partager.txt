1.
Notre terre est une table ronde
Tout autour des milliards de copains
Partageons et partout dans le monde
Il y aura à manger pour chacun.

PARTAGER, IL NOUS FAUT PARTAGER.
CE QUE L’ON A DE TROP:
Y GAGNER, CHACUN VA Y GAGNER.
UN MONDE BIEN PLUS BEAU.

2.
Notre terre tourne comme une ronde
Dansent autour des milliards de voisins
Invitons chacun dans cette ronde
Tenons-nous tous la main dans la main.

3.
Têtes noires crépues ou têtes blondes
Différents, mais tous un peu pareils
Tous égaux, enfants d’un même monde
Éclairés par un même soleil.