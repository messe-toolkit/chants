Alléluia. Alléluia. Alléluia.

Dans la maison du Seigneur
Celui qui demande recevra ;
Celui qui cherche trouvera ;
Pour celui qui frappe, la porte s'ouvrira.

Alléluia. Alléluia. Alléluia.