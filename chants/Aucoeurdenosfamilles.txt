R.
Au cœur de nos familles,
Toi, Jésus, tout donné,
Tu mets la joie de vivre l'heure
Où tes invités viennent manger le pain
Que partagent tes mains.

1.
Laissez venir à moi
Les tout petits enfants
Et voyez dans leurs yeux
Le paradis de Dieu.

2.
Laissez monter vers moi
Les hymnes des enfants ;
Prenez part à leur joie
De grandir dans la foi.

3.
Laissez tout près de moi
Sourire vos enfants ;
Bâtissez ici bas
Le monde d'au-delà.

4.
Laissez tomber mon choix
Sur l'un de vos enfants
Et priez qu'en ce jour
S'éveille un grand amour.