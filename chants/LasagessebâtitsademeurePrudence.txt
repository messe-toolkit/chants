1.
La Sagesse bâtit sa demeure
En Marie, la Fille de Sion :
Ce sont tes mains qui m’ont faite et façonnée,
La nuit, je me rappelle ton Nom !
Le Verbe à l’aurore visite son peuple.

2.
La Sagesse bâtit sa demeure
En Marie, la Vierge d’Israël :
Au profond de mon coeur, il y a ta loi,
À ta parole, je veux m’abandonner !
Le Verbe prend chair d’une enfant de la terre.

3.
La Sagesse bâtit sa demeure
En Marie, Servante du Très-Haut :
Sur la route de tes préceptes, je me hâte,
Je marche toujours sous ton regard !
Le Verbe est venu pour servir tous les hommes.

4.
La Sagesse bâtit sa demeure
En Marie, la Mère de Jésus :
Ta parole retentit, c’est la lumière :
Mes lèvres t’offrent la louange, reçois-la, Seigneur !
Le Verbe est porté dans les bras d’une femme.

5.
La Sagesse bâtit sa demeure
En Marie, la Mère des vivants :
De ton amour, Seigneur, la terre est pleine,
Quand tu parles, Seigneur, c’est pour l’éternité !
Le Verbe nous donne la Vierge pour mère.

6.
La Sagesse bâtit sa demeure
En Marie, l’Épouse de l’Agneau :
Je suis à toi, sauve-moi,
J’aspire au salut que tu me donneras !
Le Verbe de Dieu illumine le monde.