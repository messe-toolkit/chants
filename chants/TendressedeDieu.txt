TENDRESSE DE DIEU,
SAINTE MARIE,
ÉCOUTE MON CRI
EN PLEINE NUIT.
TENDRESSE DE DIEU,
SAINTE MARIE,
ÉLOIGNE MES PEURS,
APAISE MON COEUR.

1.
Quand le mal de vivre
Emprisonne mon coeur
Dans le doute et la peur :
Je voudrais me tourner vers toi,

2.
Les couleurs s’effacent
Au tableau de ma vie,
Laissant place à la nuit :
Je voudrais t’appeler maman,

3.
Dans ce monde fragile
Où l’amour est blessé,
L’espérance troublée :
Je voudrais m’approcher de toi,