1.
Seigneur Jésus,
Fils de Dieu venu dans le monde,
Pour partager nos peines et nos joies,
ou bien
Seigneur Jésus,
Par ton sang versé sur la croix,
Tu effaces nos péchés.

R.
Prends pitié de nous. (ter)

2.
Christ,
Mort sur la croix,
Pour vaincre en nous la mort et le péché,
ou bien
Ô Christ,
Par ta résurrection du tombeau,
Tu nous arraches à la mort.

3.
Seigneur Jésus,
Ressuscité d'entre les morts,
Pour nous ouvrir le chemin de la vie
ou bien
Seigneur Jésus,
Par ton entrée dans la gloire,
Tu nous ouvres la vie,