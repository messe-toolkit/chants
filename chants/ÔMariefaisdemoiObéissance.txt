1.
Ô Marie, fais de moi l’enfant de mon Dieu !
À Bethléem,
Porte-moi silencieux,
Pour que je vienne à la lumière :
Première Église, ma Mère,
Donne-moi le jour !

2.
Que ma nuit doucement s’éveille à ta voix !
Mes premiers mots,
Forme-les avec moi
Pour que le Verbe se découvre :
Église sage, ma source,
Parle-moi de Dieu !

3.
De Jésus, mon Sauveur, dis-moi le chemin !
Mes premiers pas,
Conduis-les de ta main,
Pour que toujours je sois disciple :
Église sûre, mon guide,
Montre-moi la voie !

4.
Dans tes bras, ô Marie, je vois un Enfant.
Il est mon Roi,
L’Oméga du vivant,
Le Tout Puissant dans la faiblesse :
D’une servante, sa Mère,
Il a tout appris.