1.
Le premier a dit
Voici un peu d’or
C’est là mon cadeau
C’est là mon trésor
Pour dire à l’enfant
Qu’il est tout pour moi
Et à sa maman
Qu’il est déjà Roi

NOËL C’EST AUJOURD’HUI
NOËL C’EST TOUS LES JOURS
ET MON CADEAU JOLI
C’EST T’AIMER.
T’AIMER À MON TOUR.

2.
Le second a dit
Moi c’est du parfum
C’est là mon cadeau
Et je n’en ai qu’un
Pour dire à l’enfant
Qu’il est tant précieux
Et à sa maman
Qu’il est l’homme – Dieu

3.
Le troisième a dit
Moi, dans un coffret
J’ai mis mon cadeau
Et c’est un secret
Pour dire à l’enfant
Qu’il n’a pas de prix
Et à sa maman
Qu’il sera la Vie

4.
Et quelqu’un a dit
À ce moment-là
Était-ce un berger
Était-ce ma voix
Rien n’est assez beau
Rien n’est assez grand
Voici mon sourire
Et voici mon chant