1.
Sur toutes nos frontières souffle un vent frondeur.
TEL EST L’ESPRIT DE DIEU.
Sans trêve il nous surprend, nous libérant de nos peurs.
VIENNE L’ESPRIT DE FEU. (bis)

2.
S’enflamme le bois mort ! Le feu remplit le four !
TEL EST L’ESPRIT DE DIEU.
Il brûle nos tiédeurs, nous consumant par amour.
VIENNE L’ESPRIT DE FEU. (bis)

3.
As-tu jamais goûté à l’eau d’un clair torrent ?
TEL EST L’ESPRIT DE DIEU.
Il rafraîchit tes lèvres sur la route en marchant.
VIENNE L’ESPRIT DE FEU. (bis)

4.
Le baume, en profondeur, apaise les douleurs.
TEL EST L’ESPRIT DE DIEU.
Il rétablit les membres fatigués et les coeurs.
VIENNE L’ESPRIT DE FEU. (bis)

5.
L’éclat radieux du vin fait pétiller les yeux !
TEL EST L’ESPRIT DE DIEU.
Pour annoncer la fête, il nous envoie deux par deux.
VIENNE L’ESPRIT DE FEU. (bis)