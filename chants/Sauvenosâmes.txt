1.
Des profondeurs je crie vers Toi, Seigneur entends ma voix !
Prête l'oreille à mon cri, Seigneur ! Écoute ma prière !
ANTIENNE
Sauve nos âmes,
Sauve-les du péché !
Ô Dieu quand viendra la mort
Qu'elles reposent en paix.

2.
Si du péché, tu te souviens, qui donc pourrait tenir ?
Mais le pardon est auprès de Toi afin que l'on te craigne.

3.
Mon âme espère en Toi, Seigneur, j'espère en ta parole
Comme un veilleur qui attend le jour vers Dieu je tends mon âme !

4.
Mets ton espoir dans le Seigneur la grâce est près de Lui.
Que son amour vienne pardonner les fautes de son peuple !

5.
À Dieu le Père tout-puissant A Jésus-Christ le Seigneur
À l'Esprit-Saint qui vit dans nos cœurs honneur et gloire éternelle !