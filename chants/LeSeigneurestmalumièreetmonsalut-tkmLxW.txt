R.
Le Seigneur est ma lumière et mon salut ;
De qui aurais-je crainte ?
Le Seigneur est le rempart de ma vie ;
Devant qui tremblerais-je ?

1.
J’ai demandé une chose au Seigneur,
La seule que je cherche :
Habiter la maison du Seigneur
Tous les jours de ma vie.

2.
Habiter la maison du Seigneur,
Pour t’admirer en ta bonté
Et m’attacher à ton Eglise Seigneur,
M’attacher à ton Eglise, Seigneur.

3.
J’en suis sûr, je verrai,
les bontés du Seigneur
Sur la terre des vivants.
“Espère, sois fort et prends courage;
Espère, espère le Seigneur.”