R.
Allons, bénissez le Seigneur !
Vous les serviteurs du Seigneur.

1.
Vous qui servez debout dans sa Maison,
Au long des heures de la nuit.

2.
Vers le sanctuaire élevez les mains
En bénissant le Seigneur.

3.
De Sion que le Seigneur vous bénisse,
Lui qui a fait le ciel et la terre.