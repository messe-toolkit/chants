VOUS ÊTES BÉNIS,
BÉNIS D’AVOIR AIMÉ,
BÉNIS D’AVOIR SUIVI
JÉSUS-CHRIST.

1.
Vous avez nourri les affamés,
Vous avez vêtu les humiliés,
Vous avez reçu les étrangers,
Saints et Saintes de Dieu ! (bis)

2.
Vous avez aimé le mal-aimé,
Vous avez fait place à l’exilé,
Vous avez pris soin de l’affligé,
Saints et Saintes de Dieu ! (bis)

3.
Vous avez parlé pour les muets,
Vous avez plaidé pour l’égaré,
Vous avez donné et pardonné,
Saints et Saintes de Dieu ! (bis)