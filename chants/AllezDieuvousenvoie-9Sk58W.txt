R.
Allez, Dieu vous envoie,
Vous êtes dans le monde
Les membres d’un seul corps.
Allez, Dieu vous envoie,
Vous êtes dans le monde
Les membres d’un seul corps.

1.
Par vous il veut aimer
Et rencontrer les hommes,
L’amour dont vous vous aimerez
Sera le signe de l’alliance de Dieu
Jour après jour.

2.
Par vous il veut parler
Et rejoindre les hommes,
Lorsque vous aurez à parler
Soyez sans crainte, l’Esprit témoignera
Par votre voix.

3.
Par vous il veut guérir
Et consoler les hommes,
Car son règne s’est approché.
Gardez courage, vous trouverez en lui
Votre repos.

4.
Par vous il veut sauver
Et relever les hommes,
Car il est l’agneau immolé.
La vie triomphe par le nom de Jésus
Ressuscité.

5.
Par vous il veut unir
Et rassembler les hommes,
Il vous envoie pour l’annoncer.
Il est la vigne qui donnera le fruit
De l’unité.