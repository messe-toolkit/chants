ÉCOUTE CETTE PRÉSENCE AU FOND DE TOI

ÉCOUTE CETTE PRÉSENCE AU FOND DE TOI
QUI TE MURMURE À CHAQUE INSTANT :
"MOI JE SUIS TOUJOURS AVEC TOI !"
ÉCOUTE CELUI QUI VIT AU FOND DE TOI
QUI T’ACCOMPAGNE À CHAQUE INSTANT
ET TE DIT : "REGARDE EN AVANT, MAINTENANT !"

1.
C’est comme un chant de source
Qui jamais ne s’arrête,
Un réconfort secret dans la nuit.

2.
Fragile et frêle étoile,
Lumière imperceptible,
Elle ouvre des chemins dans ton cœur.

3.
Irrésistible sève
Qui se fraie un passage
Pour te donner de croire au printemps.

4.
Quand tu cherches à comprendre
Pourquoi tant de souffrance,
Laisse-toi visiter par le ciel !