R.
ALLÉLUIA, ALLÉLUIA,
LOUANGE À TOI, JÉSUS SEIGNEUR!
ALLÉLUIA, ALLÉLUIA,
LOUANGE À TOI QUI PARLES AU COEUR!

Tu nous apprends ta vérité,
lumière et joie, chemin de vie !
Nous te chantons, Ressuscité,
Dieu qui fais signe par Marie !