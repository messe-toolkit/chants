R.
MON PAUVRE SAPIN,
ON T’A COUPÉ LE PIED,
MON PAUVRE SAPIN,
POUR TE VENDRE AU MARCHÉ
ET DEPUIS CE MATIN
DANS LA SALLE À MANGER.
TU SÈMES TES ÉPINES
ET TON PARFUM.

1.
Tu es arrivé chez moi
Ficelé comme un paquet
J’ai vu danser avec toi
Tout le ciel de la forêt.

2.
Alors je t’ai déguisé
Sous de longs cheveux d’argent
Fleuri d’étoiles en papier
Et de petits anges blancs.

3.
Quand Noël s’est envolé
On t’a mis dans le jardin
Tu venais de nous donner
Ta pauvre vie de sapin.