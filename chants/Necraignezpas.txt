Stance

Ne craignez pas : bientôt le Seigneur sera près de vous !

R.

Ad te Jesu Christe levavi animam meam.
Salvator mundi, in te speravi.

Versets

Ps 84.

1.
Tu as ôté le péché de ton peuple,
Tu as couvert toute sa faute ;
Tu as mis fin à toutes tes colères,
Tu es revenu de ta grande fureur.

2.
Fais-nous revenir, Dieu, notre salut,
Oublie ton ressentiment contre nous.
Seras-tu toujours irrité contre nous,
Maintiendras-tu ta colère d'âge en âge ?

3.
N'est-ce pas toi qui reviendras nous faire vivre
Et qui seras la joie de ton peuple ?
Fais-nous voir, Seigneur, ton amour,
Et donne-nous ton salut.

4.
Amour et vérité se rencontrent,
Justice et paix s'embrassent ;
La vérité germera de la terre
Et du ciel se penchera la justice.

5.
Le Seigneur donnera ses bienfaits,
Et notre terre donnera son fruit.
La justice marchera devant lui,
Et ses pas traceront le chemin.

Ou bien

Ps 24.

1.
Seigneur, enseigne-moi tes voies,
Fais-moi connaître ta route.
Dirige-moi par ta vérité, enseigne-moi,
Car tu es le Dieu qui me sauve.

2.
Il est droit, il est bon, le Seigneur,
Lui qui montre aux pécheurs le chemin.
Sa justice dirige les humbles,
Il enseigne aux humbles son chemin.

3.
Les voies du Seigneur sont amour et vérité
Pour qui veille à son alliance et à ses lois.
Le secret du Seigneur est pour ceux qui le
craignent ;
À ceux-là, il fait connaître son alliance.