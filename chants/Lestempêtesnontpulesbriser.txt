1.
Les tempêtes n'ont pu les briser
Ni les rigueurs les dessécher :
Toute la sève, de chaque instant,
Et sa croissance et son élan,
Le sarment les reçoit du cep;
Ainsi pour la dernière vendange,
Ils portent leur fruit de louange.

2.
C'est la croix qui saisit votre vie,
Témoins du Christ et ses amis :
Avec amour vous portez son poids,
Pourtant c'est elle, dans la foi,
Qui vous porte au-delà du seuil :
La mort ne trouve rien à vous prendre
Car tout est donné en offrande.

3.
Esprit Saint, dans ces coeurs de désir
Tu es semence d'avenir :
Tu leur rappelles la vraie patrie,
Déjà tu combles d'énergie
La faiblesse qu'ils t'ont confiée.
Heureux ces hommes qui, pour leurs frères,
Ont su refléter ta lumière.