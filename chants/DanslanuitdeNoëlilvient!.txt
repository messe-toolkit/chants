Refrain :
Dans la nuit de Noël
L’aube vient d’apparaître,
Un enfant est à naître,
Il vient comme un soleil,
Voici l’Emmanuel !

1.
Il vient au rendez-vous,
Nos coeurs sont une crèche,
Son cri ouvre la brèche,
Noël devient chez nous !

2.
Il vient à Bethléem
S’offrir dans la mangeoire,
Pour nourrir notre histoire,
Noël devient le pain !

3.
Il offre à notre nuit
L’étoile des rois mages
Pour montrer le passage,
Noël devient la vie !