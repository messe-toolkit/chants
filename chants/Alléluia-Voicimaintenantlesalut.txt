ALLÉLUIA, ALLÉLUIA.

Voici maintenant le salut de notre Dieu
et la puissance de son Christ :
le Fils de l’homme élevé sur la croix
attire à lui tous les hommes.

ALLÉLUIA.