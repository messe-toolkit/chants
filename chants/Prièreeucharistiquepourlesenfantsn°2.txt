1.
Dieu qui nous aime louange à Toi,
Louange à Toi, louange à Toi.
Dieu de la vie louange à Toi,
Louange à Toi louange à Toi.
Dieu notre Père, louange à Toi,
Louange à Toi, louange à Toi.

2.
Saint le Seigneur, Dieu de l'univers
Hosanna, Hosanna,
Qu'il soit béni, celui qui vient !
Hosanna, Hosanna,
Hosanna au plus haut des cieux,
Gloire et louange à Dieu, Gloire et louange à Dieu.

3.
Seigneur Jésus tu es mort pour nous
Louange à Toi, louange à Toi,
Tu es vivant, ressuscité !
Louange à Toi, louange à Toi.
Nous attendons le jour de ton retour,
Louange à Toi, louange à toi.

4.
Dieu qui nous aime, rassemble-nous,,
Louange à Toi, louange à Toi.
Dieu de la vie, accueille-nous!
Louange à Toi, louange à Toi.
Dieu notre Père, louange à Toi,
Louange à Toi, louange à Toi.

5.
Agneau de Dieu, vainqueur du mal,
Délivre-nous, délivre-nous.
Agneau de Dieu, vainqueur du mal,
Délivre-nous, délivre-nous.
Agneau de Dieu, vainqueur du mal,
Donne-nous la Paix, donne-nous la Paix.