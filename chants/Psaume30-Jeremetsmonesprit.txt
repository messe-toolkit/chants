R.
O PERE, DANS TES MAINS,
JE REMETS MON ESPRIT.

1.
En toi Seigneur j'ai mon refuge ;
garde-moi d'être humilié pour toujours.
En tes mains, je remets mon esprit ;
tu me rachètes, Seigneur, Dieu de vérité.

2.
Je suis la risée de mes adversaires
et même de mes voisins ;
je fais peur à mes amis,
s'ils me voient dans la rue, ils me fuient.

3.
On m'ignore comme un mort oublié,
comme une chose qu'on jette.
J'entends les calomnies de la foule ;
ils s'accordent pour m'oter la vie.

4.
Moi, je suis sur de toi, Seigneur,
je dis : "Tu es mon Dieu !"
Mes jours sont dans ta main :
Délivre-moi des mains hostiles qui s'acharnent.

5.
Sur ton serviteur, que s'illumine ta face ;
sauve-moi par ton amour.
Soyez forts, prenez courage,
vous tous qui espérez le Seigneur !