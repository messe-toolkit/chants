1.
Je n´ai pas Seigneur un cœur hautain
Et devant toi mon regard se fait humble.
Je n´ai pas pris des voies de grandeur.

R.
Espère Israël, espère dans le Seigneur
Maintenant et à jamais.
Espère Israël, espère dans le Seigneur
Maintenant et à jamais.

2.
Mon âme est en moi, comme un enfant,
Comme un enfant dans les bras de sa mère.
Je tiens mon âme en paix, en repos.