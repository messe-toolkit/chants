R.
Vienne l'Esprit qui nous dira-:
L'Amour existe et Dieu est là-!
Vienne l'Esprit qui nous dira-:
Le Dieu vivant demeure en toi-!

1.
Comment, Seigneur, t'appeler Père,
Le Père de tous les humains-?
Dans un monde au visage inhumain
Montent les cris d'innombrables misères.
Quand donc verrons-nous ton amour
Donner à l'homme son plein jour-?

2.
Comment, Seigneur, t'appeler Père,
Celui qui engendre la vie-?
Tant de morts sont pour toi un défi,
Et nul n'a vu si quelqu'un les libère.
Où donc rencontrer le Sauveur
Qui donne à l'homme sa grandeur-?

3.
Comment, Seigneur, t'appeler Mère,
Tendresse et douceur infinie-?
Ton Église l'annonce aujourd'hui
Aux orphelins dont le coeur désespère.
Bientôt seront-ils des vivants
Qui croient que l'homme est ton enfant-?