STANCE
Vous étiez loin de la source de vie, errants et dispersés, désormais réjouissez-vous :
L'Agneau par sa croix rassemble le troupeau et tous ceux qui le suivent proclament :

R.
Jésus, notre berger, tu nous conduis au Père.

1..

1.us aviez faim et soif, accourez tous : la justice du Père vous est révélée..

1.
Les murs seront détruits, abattues les barrières : les hommes sont réconciliés par le sang de l'Agneau.
3.
Joyeuse nouvelle pour ceux qui étaient loin et pour ceux qui étaient proches : Un seul corps les unit.