1.
Le fil de la vie mon ami si tu peines,
Entends l’infini t’apporter son haleine
Vent grandissant d’où peut naître une Espérance
Feu pris au creux de tes mains toi l’être humain
Il est entré dans un grand jardin de blé
Avec l’été, le moment de moissonner
Il est entré dans un grand jardin de blé
Semeur d’Amour sur champs de liberté.

AÏ ! SUR LE VITRAIL ! PAS DE RIPAILLES !
JUSTE UN PEU DE PAILLE, UN PEU DE PAILLE
OH! SI C’EST PAS CHAUD! SI C’EST PAS HAUT!
DIEU QUE C’EST BEAU!

2.
La joie si tu pleures en ton coeur peut renaître
L’espoir c’est de voir de nouveau la lumière
L’eau jaillissante à la source qui renouvelle
Signe éclatant d’une route toujours plus belle
Il a plongé dans le fleuve du verger
Avec l’été, le moment d’immerger
Il a plongé dans le fleuve du verger
Témoin choisi sur fond de vérité.

3.
Des couleurs, des fleurs en tes yeux apparaissent
Et tous ces oiseaux chanteurs de tendresse
Paix annoncée à tout homme qui sommeille
Chant retrouvé lorsque l’être se réveille
Ils marcheront au côté d’un peuple aimé
Avec l’été, le moment de se lever
Ils marcheront au côté d’un peuple aimé
“Justes” brûlants de foi, de charité.