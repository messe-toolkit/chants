R.
Le Seigneur fit pour moi des merveilles,
ma — gni — fi — cat
et mon coeur exulte de joie.
ma– gni– fi — cat
En ma chair s’accomplit la promesse,
ma — gni — fi — cat
Alléluia, Alléluia:
ma-gni-fi-cat

1.
Mon âme exalte le seigneur,
Exulte mon esprit en Dieu mon Sauveur !
Il s’est penché sur son humble servante;
désormais tous les âges me diront bienheureuse.
Le puissant fit pour moi des merveilles ;
Saint est son nom !

2.
Son amour s’étend d’âge en âge,
sur ceux qui le craignent.
Déployant la force de son bras,
il disperse les superbes.
Il renverse les puissants de leur
trône, il élève les humbles.

3.
Il comble de biens les affamés,
renvoie les riches les mains vides.
Il relève Israël son serviteur,
il se souvient de son amour.
De la promesse faite à nos pères,
en faveur d’Abraham et de sa race à jamais.

Gloire au Père et au fils et au Saint Esprit,
Pour les siècles des siècles. Amen