QU’ELLE VIENNE EN TOI
LA PAIX DE DIEU
QU’ELLE VIVE EN TOI
LA PAIX DE DIEU
QU’ELLE CHANTE EN TOI
QU’ELLE AIME EN TOI !
LA PAIX DE DIEU

1.
Comme un cadeau, un présent
Que tu reçois ou que tu donnes :
Comme le fruit d’un pardon
La paix libère (bis)

2.
Comme une fleur dans ta main
Que tu reçois ou que tu donnes :
Comme l’amour dans ton coeur
La paix fait vivre (bis)

3.
Comme un sourire, un soleil
Que tu reçois ou que tu donnes
Comme un poème, un refrain
La paix se donne (bis)