Ô MON PÈRE, ENTRE TES MAINS,
JE REMETS MON ESPRIT,
ENTRE TES MAINS,
MON ESPRIT, MON ESPRIT.

1.
Je n’en peux plus de souffrir
Je m’abandonne à Toi.
J’ai soif et je vais mourir
Suspendu à la croix.

2.
Près de la croix, elle est là,
Marie maman en pleurs.
Jean la soutient dans ses bras
Pour calmer sa douleur.

3.
Il est trois heures aujourd’hui
La Parole s’accomplit
Je te remets mon esprit
Tout le ciel s’assombrit.