TU ES LE DIEU DE L’ALLIANCE,
LE DIEU QUI MONTRE SON AMOUR,
AMEN, LOUÉ SOIT L’ÉTERNEL,
LE DIEU DU PEUPLE D’ISRAËL !

1.
Comme pour Adam le premier homme,
Tu ouvres les portes du paradis,
Toi le seul Seigneur tu vois ces hommes,
Qui oublient que tu es Dieu de la Vie !
L’Alliance est brisée, mais tu pardonnes,
Toi le Dieu créateur tu es béni !

2.
Du temps de Noé, quand la pluie tombe,
Tu le vois, ils sont loin, tous tes enfants,
Alors tu lui dis : « Va, construis l’arche :
Animaux, provisions, emportes-les »,
L’Alliance est brisée, mais tu patientes,
Toi le Dieu créateur passionnément !

3.
Au vieil Abraham, quelle promesse :
Un pays loin d’ici, rempli de miel,
C’est un temps nouveau, une jeunesse :
Tu seras si nombreux, ô Israël,
L’Alliance est brisée, mais tu répares,
Toi le Dieu créateur qui vit au ciel !

4.
Toi Moïse écoute Dieu t’envoie :
« Va chez Pharaon, dis-lui qu’il est temps !
De mener le peuple vers la Terre,
Au pays où ruissellent l’eau et le miel ! »
L’Alliance est brisée, mais tu libères,
Toi le Dieu créateur toujours fidèle !