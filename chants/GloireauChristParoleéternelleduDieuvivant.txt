Gloire au Christ,
Parole éternelle du Dieu Vivant
Gloire à toi, Seigneur.

Du sein de la nuée resplendissante,
la voix du Père a retenti :
« Voici mon Fils, mon bien-aimé,
écoutez-le ! »