1.
Toi qui veilles en la nuit,
Joyeuse est ta lumière ;
Tu es la lampe
Qui brûle et qui luit
Jusqu'à l'aube attendue
Depuis des siècles.

2.
Tu précèdes le jour,
Tu portes l'espérance ;
Éclaire l'homme
En sa quête d'amour
Et ramène son cœur à l'innocence.

3.
À la Pâque de Dieu
Prépare notre terre !
Tu nous annonces
Un baptême de Feu :
Qu'il embrasse la vie
De tous les êtres !

4.
Ta lumière décroît,
Une Autre se révèle ;
C'est Dieu qui monte
Et devance tes pas :
Dans l'aurore du Christ
La joie parfaite.