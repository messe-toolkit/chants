1.
Que mon Esprit soit sur vous,
Que votre joie soit parfaite. (bis)
Demeurez en mon amour,
Gardez mon commandement.
Celui qui demeure en moi
Portera beaucoup de fruit.

2.
Ce qu´au Père vous demandez,
En mon Nom il vous le donne. (bis)
Demandez et vous recevrez,
Votre joie sera parfaite.
Par l´Esprit de vérité,
Allez et portez du fruit.

3.
C´est moi qui vous ai choisis,
De moi vous serez témoins. (bis)
Ma parole est vérité,
Je suis le chemin de vie
Comme je vous ai aimés,
Aimez-vous les uns les autres.