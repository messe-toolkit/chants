R.
Viens, Esprit Saint !
Viens en nos cœurs et sauve-nous !

1.
Viens, Esprit Saint, en nos cœurs
Et envoie du haut du ciel un rayon de ta lumière.
Viens en nous, père des pauvres,
Viens, dispensateur des dons, viens, lumière de nos cœurs.

R.
Viens, Esprit Saint !
Viens en nos cœurs, éclaire-nous !

2.
Consolateur souverain,
Hôte très doux de nos âmes, adoucissante fraîcheur.
Dans le labeur, le repos,
Dans la fièvre, la fraîcheur, dans les pleurs, le réconfort.

R.
Viens, Esprit Saint !
Viens en nos cœurs, console-nous !

3. Ô lumière bienheureuse,
Viens remplir jusqu´à l´intime le cœur de tous tes fidèles.
Sans ta puissance divine,
Il n´est rien en aucun homme, rien qui ne soit perverti.

R.
Viens, Esprit Saint !
Viens en nos cœurs et guide-nous !

4.
Lave ce qui est souillé,
Baigne ce qui est aride, guéris ce qui est blessé.
Assouplis ce qui est raide,
Réchauffe ce qui est froid, rends droit ce qui est faussé.

R.
Viens, Esprit Saint !
Viens en nos cœurs, purifie-nous !

5. À tous ceux qui ont la foi,
Et qui en toi se confient, donne tes sept dons sacrés.
Donne mérite et vertu,
Donne le salut final, donne la joie éternelle.

R.
Viens, Esprit Saint !
Viens en nos cœurs, embrase-nous !