LA LUMIÈRE AUJOURD’HUI A RESPLENDI SUR NOUS :
UN SAUVEUR NOUS EST NÉ !

1.
Le Seigneur est roi ! Exulte la terre !
Joie pour les îles sans nombre !
Les cieux ont proclamé sa justice,
et tous les peuples ont vu sa gloire.

2.
Une lumière est semée pour le juste,
et pour le coeur simple, une joie.
Que le Seigneur soit votre joie, hommes justes ;
rendez grâce en rappelant son nom très saint.