R.
Paris, Paris, Paris d'Avenir
Pour le meilleur par ici mes frères
Aux quatre coins du monde pour agir
Donnons du cœur restons solidaires
Paris, Paris, Paris d'Avenir
Pour le meilleur par ici mes frères
Aux quatre coins du monde pour agir
Paris…     Pour la terre

1.
L'inconnu ne me fait pas peur
M'ouvre les yeux, m'ouvre le cœur
C'est grâce à toi
Que je chemine vers la paix
Que je suis fort et abreuvé d'humanité.
Ouvrir les portes, briser les murs,
Fendre nos masques, nos armures
Nous sommes tous d'un même pays
Où l'homme se différencie.
J'ai la soif de te connaitre
J'ai la soif de te connaitre
J'ai la soif de te connaitre

2.
Le futur ne me fait pas peur
Il sera juste et en couleur
C'est pour demain
Que j'ai le désir de changer
D'offrir mes bras et mes idées
Pour avancer
Prendre le temps pour inventer
Une nouvelle façon d'habiter
Autant de temps pour innover
Que de temps passé à rêver
La terre nous appelle à changer
La terre nous appelle à changer
La terre nous appelle à changer

3.
L'engagement ne me fait pas peur
Je fais des choix et des erreurs
Qui font de moi
Un homme avec identité
Bien tamponnés
Lâcher l'aiguille et ces quadrants
Les sabliers voleurs de temps
Oser la vie et les projets
Pour enfin me réaliser
Prendre un chemin de vérité
Prendre un chemin de vérité
Prendre un chemin de vérité