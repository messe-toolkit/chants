1.
Pour l’honneur qui nous est fait, pour la joie qui est donnée
de porter le nom du Christ
Pour le lien qui nous unit quand il nous fait ses amis,
Gloire à Dieu, sur terre et dans les cieux !

Qui d’entre nous aura la grâce, pécheur et pardonné,
D’être introduit dans le mystère de l’Amour Trinité ?
Plongé en Dieu, aimant le monde, en toute chose il cherchera
Où Dieu l’appelle et le dispose à mieux servir et mieux aimer.

Saint Ignace de Loyola, PRIEZ POUR NOUS !
Maîtres et compagnons à l’école du Christ, PRIEZ POUR NOUS !
Tous nos « amis dans le Seigneur », PRIEZ POUR NOUS !

2.
Quand plus rien ne nous retient, pour être envoyés au loin,
annoncer le nom du Christ
L’amitié qui nous unit est un don de son Esprit.
Gloire à Dieu, sur terre et dans les cieux !

La terre est vaste où l’Évangile n’est jamais entendu.
Combien de peuples par le monde n’en ont toujours rien su ?
Et nos efforts qui se dispersent, et nos élans désorientés…
Vienne l’Esprit qui est Sagesse et feu brûlant pour nous lancer.

St François-Xavier, PRIEZ POUR NOUS !
Maîtres et compagnons à l’école du Christ, PRIEZ POUR NOUS !
Tous nos « amis dans le Seigneur », PRIEZ POUR NOUS !

3.
Au chantier qui nous attend d’unifier tous les croyants
dans l’amour du nom du Christ
Que grandisse la douceur d’être amis dans le Seigneur.
Gloire à Dieu, sur terre et dans les cieux !

Certains sont faits pour la rencontre, l’écoute et le pardon,
Donnant le juste témoignage d’un coeur honnête et bon.
Mais qui saura par quelle épreuve au fond de l’âme ils sont passés
Conduits par Dieu dans la prière à leur si belle humanité ?

Bienheureux Pierre Favre, PRIEZ POUR NOUS !
Maîtres et compagnons à l’école du Christ, PRIEZ POUR NOUS !
Tous nos « amis dans le Seigneur », PRIEZ POUR NOUS !

4.
De Marie nous apprendrons la plus belle et vraie façon
de porter le nom du Christ
De courir joyeusement et laisser monter nos chants.
Gloire à Dieu, sur terre et dans les cieux !

En nous tournant vers Notre Dame, nous lui demanderons
Que sa prière nous obtienne de son Fils et Seigneur
Qu’il nous reçoive auprès de lui dans sa mission de serviteur
Et nous instruise en pauvreté pour mieux le suivre et mieux l’aimer.

Sainte Marie, Mère de Dieu, PRIEZ POUR NOUS !
Les saints apôtres de Jésus, PRIEZ POUR NOUS !
Tous nos « amis dans le Seigneur », PRIEZ POUR NOUS !

5.
Ceux qui portent ce trésor vont le graver sur leur corps
en disant le nom du Christ
En priant le Saint Esprit de venir changer leur vie.
Gloire à Dieu sur terre et dans les cieux !

En communion pour faire Église, nous sommes rassemblés,
Famille unie qui fait mémoire du Christ ressuscité ;
Dans un repas, dans une fête, en partageant le pain de vie,
Nous rendons grâce à notre Père et lui offrons le monde entier.

Saint Pierre et Saint Paul, PRIEZ POUR NOUS !
Saints nombreux qui portez l’évangile, PRIEZ POUR NOUS !
Tous nos « amis dans le Seigneur », PRIEZ POUR NOUS !