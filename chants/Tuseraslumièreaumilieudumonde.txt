R.
TU SERAS LUMIÈRE AU MILIEU DU MONDE,
FLAMME QUI RAYONNE ET QUI LUIT.
TU SERAS LUMIÈRE AU MILIEU DU MONDE,
FLAMME DU SEIGNEUR JÉSUS CHRIST.

1.
Tu seras le feu que le Maître veut répandre,
Dieu t’a réveillé par le souffle de l’Esprit.
Donne ta chaleur quand la nuit vient à descendre,
Toi qui as reçu l’étincelle de la Vie !

2.
Tu seras la flamme qui chasse les ténèbres,
Dieu fera de toi un flambeau dans sa maison.
Donne à tout vivant le reflet de sa tendresse,
Toi qui as reçu la brûlure du pardon.

3.
Tu seras la braise qui veille sous la cendre,
Dieu te gardera de mourir à tout jamais.
Donne ton secret à qui cherche une espérance,
Toi qui as reçu la promesse de la paix.