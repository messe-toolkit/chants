1.
Seigneur, je viens à toi,
J’implore ton secours.
Jésus, si tu le veux,
Révèle ton amour !
Pitié pour un lépreux !
Guéris-moi, tu le peux.

2.
Ta main posée sur moi
Efface toute plaie.
Vraiment, Seigneur, tu veux
Que j’entre dans la paix.
Par toi je suis heureux,
Jésus Christ, Fils de Dieu !

3.
Impur j’étais exclu
Du monde des vivants.
Seigneur, tu m’as sauvé,
Sauvé en me touchant.
Je marche libéré,
Tous mes liens sont brisés.

4.
Seigneur qui m’as guéri,
Pour moi, qui donc es-tu?
Je veux donner ma foi
Au Dieu qui m’a reçu.
L’Amour vivant, c’est toi,
Ô Jésus, je le crois.

5.
Ton peuple me reçoit,
L’Amour fleurit mon coeur.
L’Alliance me conduit
Sur tes chemins, Seigneur,
Tu m’aimes pour la vie,
Nuit et jour je le dis.