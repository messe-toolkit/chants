GRANDE JOIE AU CIEL ET SUR TERRE
QUAND TU METS AU MONDE UN SAUVEUR !
Ô MARIE TON ENFANT DE LUMIÈRE
NOUS CONDUIT OÙ LÈVE LE JOUR,
IL NOUS DIT QUE DIEU EST AMOUR.

1.
Dans Bethléem, en pleine nuit,
Voici chez nous le Fils promis !

2.
En serviteur du Premier-Né,
Joseph est là, silence et paix !

3.
Vous les bergers, ne craignez pas !
Allez partout porter sa joie !

4.
Messie de Dieu, il est Seigneur,
Berger du monde et roi des coeurs.

5.
Glorifiez Dieu à pleine voix,
Que monte en vous un chant de foi !