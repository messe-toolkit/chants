Tous : La sagesse a conduit les saints
sur un chemin de merveilles
en récompense de leur peine.

Soliste ou schola : Le jour elle fut pour eux un abri
et la nuit une clarté d’étoiles.

Tous : en récompense de leur peine.
Soliste ou schola : Gloire au Père et au Fils et au Saint Esprit

Tous : La sagesse a conduit les saints
sur un chemin de merveilles
en récompense de leur peine.