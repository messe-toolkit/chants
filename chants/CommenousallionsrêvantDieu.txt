1.
Comme nous allions rêvant Dieu,
Une voix venue du grand creux
Des fonds de l’Homme
Nous a surpris :Veillez ici,
Veillez et priez cette nuit
Qui entre toutes vous est bonne.

2.
C’était au secret de nos coeurs,
Au tombeau vide du Seigneur,
La voix de l’Ange !
Elle ajouta : Que cherchez-vous ?
Le corps du Seigneur est chez vous,
Restez ses hommes de confiance !

3.
Devant le caveau grand ouvert,
Retour du Seigneur des enfers,
Chantez son hymne !
Ce lieu profond, il est à Dieu !
Nul ne le sonde avec des yeux
Qui ne sont pas faits pour l’abîme.

4.
Le Seigneur vous a précédés
Dans la mort qui vous obsédait,
Vos morts futures ;
Allez donc sans crainte à la vie !
Jésus vous a déjà ravi
Dans sa Passion vos sépultures.