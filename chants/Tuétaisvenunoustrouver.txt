R.
Parole d'amour, Jésus-Christ!
Tu es le pain des hommes
Sois notre vie sois notre joie.
Tu es l'espoir des hommes
Sois notre vie sois notre joie.

1.
Tu étais venu nous trouver
Pour inventer la vie Pour inventer l'amour.
Tu étais venu nous trouver
Pour inventer la vie Pour inventer l'amour.
Mais nous avons tourné nos pas
Vers des prophètes de tristesse.

2.
Tu étais venu nous choisir
Pour annoncer ta vie
Pour annoncer l'amour.
Mais nous avons jeté aux chiens
Le pain de ta Bonne Nouvelle.

3.
Tu étais venu nous parler
Pour libérer nos vies,
Pour libérer nos coeurs.
Mais nous avons cherché des lois
Pour emprisonner tes paroles.

4.
Tu étais venu nous chercher
Pour dénoncer la guerre,
Pour dénoncer l'argent.
Mais nous avons suivi Judas
Et les soldats de nos défaites.

5.
Tu étais venu nous servir
Pour révéler le Père,
Pour révéler son nom.
Mais nous avons jugé l'amour
Du Dieu venu nous rendre libres.