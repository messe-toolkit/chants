Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !

Réjouis-toi, rayonnement de joie !
Réjouis-toi, par qui le mal a disparu !
Réjouis-toi, tu relèves Adam de sa chute !
Réjouis-toi, par toi Eve ne pleure plus.
Réjouis-toi, montagne inaccessible aux pensées des hommes !
Réjouis-toi, miracle proclamé par les anges !
Réjouis-toi, car tu deviens le trône et le palais du roi !
Réjouis-toi, toi qui portes celui qui porte tout !

Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !

Réjouis-toi, mère de l’agneau et du pasteur !
Réjouis-toi, bergerie de l’unique troupeau !
Réjouis-toi, mystère de la sagesse divine !
Réjouis-toi, mère de la lumière sans déclin !

Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !

Réjouis-toi, temple du Dieu de toute immensité !
Réjouis-toi, écrin où repose la divine sagesse !
Réjouis-toi, terre de la promesse !
Réjouis-toi, tabernacle du Dieu vivant !
Réjouis-toi, arche dorée par l’Esprit !
Réjouis-toi, trésor inépuisable de la vie !
Réjouis-toi, tu nous donnes le Dieu d’amour !
Réjouis-toi, tu nous ouvres les portes du paradis !

Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !

Réjouis-toi, chambre nuptiale du Verbe de Dieu !
Réjouis-toi, tu unis les croyants au Seigneur !
Réjouis-toi, servante du festin des noces !
Réjouis-toi, éblouissante porte de la lumière !

Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !

Réjouis-toi, étoile annonciatrice du soleil levant !
Réjouis-toi, par qui Dieu devient petit enfant !
Réjouis-toi, car tu renouvelles toute créature !
Réjouis-toi, en toi nous adorons le créateur !
Réjouis toi, commencement des merveilles du Christ !
Réjouis-toi, qui nous ouvres au secret ineffable !
Réjouis-toi, foi de ceux qui prient en silence !
Réjouis-toi, abîme impénétrable même aux Anges !

Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !

Réjouis-toi, rayonnement du soleil véritable !
Réjouis-toi, éclat de la lumière sans couchant !
Réjouis-toi, flambeau qui porte la lumière inaccessible !
Réjouis-toi, car tu illumines nos coeurs !

Réjouis-toi, ô Mère du Sauveur !
Alleluia, alleluia, alleluia !