R.
Il est grand le bonheur de donner,
Bien plus grand que la joie de recevoir.
Le Seigneur s’est lui-même donné,
Bienheureux qui le suivra ! (bis)

1.
Donner le jour aux enfants de la nuit,
Donner le feu quand le froid les surprend,
Donner la flamme qui brûle et qui luit,
Donner l’espoir aux marcheurs de ce temps.

2.
Donner le pain dans un monde affamé,
Donner l’eau vive puisée près de Dieu,
Donner de croire au festin partagé,
Donner le sel et le vin généreux.

3.
Donner le souffle à tout homme blessé,
Donner le sang qui réveille sa vie,
Donner de vivre debout dans la paix,
Donner l’audace envoyée par l’Esprit.

4.
Donner le fruit du travail de nos mains,
Donner d’apprendre à chercher un trésor,
Donner l’envie de s’ouvrir un chemin,
Donner l’amour qui peut vaincre la mort.

5.
Donner la soif de connaître Jésus,
Donner les mots qui pourront le chanter,
Donner d’aller par des voies inconnues,
Donner la force d’un coeur libéré.