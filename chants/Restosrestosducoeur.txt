Refrain :
Restos - restos - restos du coeur
Morceau - morceau - morceau d’bonheur
Un p’tit brin d’amitié
De solidarité
Pour ainsi transformer
Un hiver en été.

1.
C’est l’HISTOIRE d’un mec, “UN COEUR GROS COMME ÇA”.
Avec des rêves cons, voulait changer la vie,
Pour le système D, le sans-gêne, c’était le Roi,
“UN COEUR GROS COMME ÇA !” pour tous les sans-abri.

2.
C’est l’HISTOIRE d’un mec, un peu comme toi, NORMAL
Mais qui ne supportait, ni le froid, ni la faim.
Qui ne supportait pas le destin ANORMAL,
De tous ceux qui galèrent, des exclus, des sans-rien.

3.
C’est l’HISTOIRE d’un mec et celle de ses potes,
Qui redonnent un SOLEIL et un peu de CIEL BLEU,
Aux paumés, aux cassés, ceux qui en ont plein les bottes.
Les RESTOS, c’est un peu, une famille, un chez-eux.