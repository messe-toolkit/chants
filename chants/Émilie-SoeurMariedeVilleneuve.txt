1.
Elle nous redit
Le sens de l’Évangile,
Elle nous supplie
D’être morceau d’argile
Entre les mains d’un Dieu-Amour.
Trouver le sens, le vrai, le jour
Dans les petits, les mendiants de l’amour.

R.
ÉMILIE,
MARIE DE VILLENEUVE,
TU NOUS AS TRACÉ LE PARCOURS.
ÉMILIE,
NOTRE SOEUR BIENHEUREUSE,
NOUS VOICI
POUR CHANTER DIEU-AMOUR.

2.
Elle nous redit
Le goût de l’Évangile,
Porter du fruit
Au coeur d’un monde hostile.
Et témoigner de Dieu-Amour,
Et respecter ainsi toujours
Les “plus petits”, les mendiants de l’amour.

3.
Elle nous redit
La joie de l’Évangile,
Malgré nos vies
Si futiles et fragiles.
Elle nous redit le Dieu-Amour
Veillant sur nous la nuit, le jour.
Elle nous redit : « Suivez-Le sans détour. »