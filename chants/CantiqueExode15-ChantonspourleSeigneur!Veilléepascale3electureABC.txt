CHANTONS POUR LE SEIGNEUR !
ÉCLATANTE EST SA GLOIRE !

1.
Je chanterai pour le Seigneur !
Éclatante est sa gloire :
il a jeté dans la mer
cheval et cavalier.

2.
Ma force et mon chant, c’est le Seigneur :
il est pour moi le salut.
Il est mon Dieu, je le célèbre ;
j’exalte le Dieu de mon père.

3.
Le Seigneur est le guerrier des combats ;
son nom est « Le Seigneur ».
Les chars du Pharaon et ses armées, il les lance dans la mer.
L’élite de leurs chefs a sombré dans la mer Rouge.

4.
L’abîme les recouvre :
ils descendent, comme la pierre, au fond des eaux.
Ta droite, Seigneur, magnifique en sa force,
ta droite, Seigneur, écrase l’ennemi.

5.
Tu les amènes, tu les plantes sur la montagne, ton héritage,
le lieu que tu as fait, Seigneur, pour l’habiter,
le sanctuaire, Seigneur, fondé par tes mains.
Le Seigneur régnera pour les siècles des siècles.