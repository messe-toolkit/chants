R.
Préparons le chemin du Seigneur,
Un chemin de justice et de paix ;
Sur la voie qui libère les coeurs
Nous verrons le bonheur que Dieu promet.

1.
Dieu nous parle aujourd’hui de marcher :
Marcher vers sa lumière et croire à son aurore,
S’ouvrir au feu qui prend depuis le Fils de l’homme.
Voici qu’il vient, l’Emmanuel,
Levons les yeux vers son jour !

2.
Dieu nous parle aujourd’hui de monter :
Monter vers la montagne où chante la prière ;
Sommet qui nous attend, rencontre avec le Père.
Viendra le temps de jubiler,
Levons les yeux vers son jour !

3.
Dieu nous parle aujourd’hui de forger :
Forger des lendemains qui brisent toute guerre ;
Le fer de nos épées ne peut sauver la terre.
Viendra le temps d’un monde neuf,
Levons les yeux vers son jour !

4.
Dieu nous parle aujourd’hui de changer :
Changer le sol aride en terre d’abondance,
Semer des grains d’amour et vivre d’espérance.
Viendra le temps de la moisson,
Levons les yeux vers son jour !
15630.

5.
Dieu nous parle aujourd’hui de veiller:
Veiller dans nos prisons, combattre les ténèbres,
Les signes du Messie délivrent la Nouvelle.
C’est lui, Jésus, qui doit venir,
Levons les yeux vers son jour !

6.
Dieu nous parle aujourd’hui de l’aimer :
L’aimer comme il nous aime, il est un Dieu tendresse ;
Son Verbe devient chair, Marie le porte en elle.
Voici chez nous l’Emmanuel,
Levons les yeux vers son jour !