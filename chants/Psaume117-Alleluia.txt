R.
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA !
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA !

1.
Rendez grâce au Seigneur, il est bon,
Éternel est son amour !
Qu’ils le disent, ceux qui craignent le Seigneur,
Éternel est son amour !

2.
Le bras du Seigneur a fait merveille,
Le bras du Seigneur est fort,
Non, je ne mourrai pas, je vivrai,
Pour proclamer les actions du Seigneur.

3.
La pierre rejetée des bâtisseurs
Est devenue la pierre d’angle,
C’est là l’oeuvre du Seigneur,
La merveille devant nos yeux.