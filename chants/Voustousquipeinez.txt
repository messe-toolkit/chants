R.
Alleluia, alleluia, alleluia, alleluia !

1.
Vous tous qui peinez
Au chemin de la vie,
Venez au Seigneur,
Il vous soulagera !

2.
Vous tous qui ployez
Sous le poids du péché,
Venez au Seigneur,
Il vous pardonnera !

3.
Vous tous qui craignez
De céder à la peur,
Venez au Seigneur,
Il vous fortifiera !