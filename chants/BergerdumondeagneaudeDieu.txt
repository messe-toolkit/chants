BERGER DU MONDE, AGNEAU DE DIEU,
TU NOUS CONDUIS VERS LES EAUX VIVES.
HEUREUX CELUI QUI MARCHERA
VERS LA SOURCE DE TA VIE !

1.
Dieu lui-même nous prendra dans sa demeure :
Plus de soif ni plus de faim,
la brûlure du soleil ne sera plus !
À jamais, Jésus, Seigneur,
tu seras notre Pasteur
À la gloire du Père.

2.
Dieu lui-même comblera notre espérance :
Plus de larme dans nos yeux,
Nos visages tourmentés refleuriront !
A jamais, Jésus Seigneur,
Tu seras notre Pasteur
Pour la joie de tes frères.

3.
Dieu lui-même nous dira ce que nous sommes :
Des enfants bénis de Lui !
Nous aurons un nom nouveau qui chantera.
A jamais, Jésus Seigneur,
Tu seras notre Pasteur
Au pays des lumières.

4.
Dieu lui-même nous éveille à son écoute :
A sa voix répondrons-nous
En fidèles pèlerins du peuple saint ?
A jamais, Jésus Seigneur,
Tu seras notre Pasteur
Jusqu'au bout de la terre.

5.
Dieu lui-même nous apprend la route à suivre :
Plus de crainte dans sa main,
Les blessures sous nos pas se guériront.
A jamais, Jésus Seigneur,
Tu nous guides en bon Pasteur,
Ton amour fait renaître.