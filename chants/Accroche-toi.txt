R.
Accroche-toi,
La vie vaut le coup,
Cramponne-toi donc aux bras du Seigneur,
Accroche-toi,
La vie vaut le coup,
Et tu verras ce que c'est le bonheur.

1.
Savent-ils combien tu as souffert
Et pourquoi ton Être semble de pierre
Dieu te comprend Lui parc'qu'Il t'aime,
Gratuitement, oui pour toi-même.
Ne crois jamais être oublié,
Empêche-les de t'entraîner,
Offre ta nuit à Sa lumière,
Renverse donc toutes les barrières.

2.
Ne t'en fais pas si tu as trébuché
Sa main est là pour te relever
Tu vas exister et renaître par Lui
Chaque jour redessiner la Vie.
On n't'a pas compris, non mais jette ta haine,
Car Lui, Il sait bien que tu en vaux la peine.
Relève la tête, mais cherche au creux de toi,
L'Ami, le Père qui sera toujours là.

3.
Regarde la fleur qui te sourit,
L'Autre qui attend de devenir Ton Ami.
Le feu devant toi qui crépite de Joie,
La terre frémit d'Amour sous tes pieds maladroits.
Marche vers Lui, Dieu sait aimer,
Tu peux choisir de t'en aller
À chaque instant sans t'excuser
Parc'que son Amour c'est ta liberté.

4.
Fais-lui confiance, laisse ton cœur,
Être le puits de Sa douceur,
Prie pour tous ceux qui t'ont blessé,
Il t'apprendra à tous les pardonner.
Respire à fond les merveilles de Dieu,
Elles s'anim'ront par toi si tu ouvres les yeux.
Tu es aimé, viens émerveille-toi,
Craque pour le Seigneur, et chante donc avec moi.