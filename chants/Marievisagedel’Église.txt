BÉNIE SOIS-TU MARIE, FILLE DE SION,
PAR TOI, DIEU A CHOISI DE VENIR HABITER SON PEUPLE.
VISAGE DE L’ÉGLISE D’HIER ET D’AUJOURD’HUI,
VISAGE DE DEMAIN, TOUJOURS À NAÎTRE.
BÉNIE SOIS-TU MARIE.

1.
Pour que le monde vienne puiser
À l’eau de la fontaine,
Tu as dit “oui”, tu as dit “oui”.
Et de ton sein a pu naître Celui
Qui est source de vie.

2.
Pour que le monde sache l’amour
De ce Dieu qui nous aime,
Tu as dit “oui”, tu as dit “oui”.
Et de ton sein a pu naître Celui
Qui est chemin de vie.

3.
Pour que le monde croie en l’Esprit
Qui nous tient sous son aile,
Tu as dit “oui”, tu as dit “oui”.
Et de ton sein a pu naître Celui
Qui est souffle de vie.

4.
Pour que le monde garde espérance
Et ne crie plus la haine,
Tu as dit “oui”, tu as dit “oui”.
Et de ton sein a pu naître Celui
Qui est force de vie.