1.
Verbe éternel,
Tu as pris chair
Parmi les fils d’Israël,
Et tu voulais être l’un d’eux.
Mais aujourd’hui,
Que la Parole chez tous les peuples
Ait sa demeure !

2.
Verbe de vie,
Tu as parlé
La langue de ton pays
Pour annoncer l’amour de Dieu.
Mais aujourd’hui,
Que toute langue soit messagère
De la sagesse !

3.
Verbe de Dieu,
Tu as prié
Mêlant aux pauvres ta voix
Et partageant les mêmes mots.
Fais aujourd’hui
Que toute langue soit la servante
De la louange !