VIENS CHANTER, VIENS DANSER,
VIENS LOUER LE SEIGNEUR !
IL EST GRAND, IL EST SAINT,
IL EST NOTRE SAUVEUR!
VIENS CHANTER, VIENS DANSER,
VIENS LOUER LE SEIGNEUR !
MAGNIFIQUE EST SON NOM À JAMAIS!

1.
Que ses oeuvres sont grandes et merveilles pour nous !
Que le ciel et la terre le chantent !
Sa bonté, son amour sont visibles partout
Et sa gloire illumine nos coeurs !

2.
Que nos voix le célèbrent et proclament son Nom !
Que nos pas soient remplis d’allégresse !
Sa grandeur est chantée au milieu des nations !
Et sa gloire illumine nos coeurs !

3.
Que le chant des oiseaux et le bruit de la mer,
que le souffle du vent le bénissent !
Sa puissance infinie envahit l’univers
Et sa gloire illumine nos coeurs !