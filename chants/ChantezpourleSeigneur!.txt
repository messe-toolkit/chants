CHANTEZ POUR LE SEIGNEUR !

CHANTEZ POUR LE SEIGNEUR !
ÉCLATEZ EN CRIS DE JOIE CAR DIEU EST AVEC NOUS !
SPLENDEUR DE L'UNIVERS,
DIEU TIENT LA PROMESSE QU'IL FIT À NOS PÈRES :
LE RESSUSCITÉ NOUS SAISIT DANS SA GLOIRE !

1.
Un signe immense est apparu : une femme !
Elle est habillée du Soleil,
La Lune sous ses pieds,
Et couronnée d'étoiles !

2.
L'enfant qu'elle a donné aux hommes : c'est un fils !
Roi des nations et de la Terre !
C'est lui l'agneau de Dieu,
Venu pour nous sauver !

3.
Voici la mère des vivants dans la gloire !
Elle est à l'œuvre de son fils,
Et rassemble les hommes
Sur le trône de Dieu !