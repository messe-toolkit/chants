Stance :
Au coeur des ténèbres du monde,
une grande lumière a surgi de la faiblesse d’un Enfant.

R.
ET CET ENFANT C’EST LE VERBE FAIT CHAIR.
QUE TOUS LES PEUPLES DE LA TERRE
ADORENT LE SEIGNEUR.

1.
Marie présente son Enfant aux grands de ce monde.
ET CET ENFANT C’EST LE VERBE FAIT CHAIR.

2.
La Promesse est accomplie, de Bethléem vient le Roi d’Israël.
ET CET ENFANT C’EST LE VERBE FAIT CHAIR.

3.
Craignant pour sa royauté, Hérode veut tuer l’Enfant.
ET CET ENFANT C’EST LE VERBE FAIT CHAIR.

4.
Voici la gloire du Seigneur, les nations s’uniront un jour en lui.
ET CET ENFANT C’EST LE VERBE FAIT CHAIR.

5.
Témoins de ce grand Mystère, soyons les messagers de son amour.
ET CET ENFANT C’EST LE VERBE FAIT CHAIR.