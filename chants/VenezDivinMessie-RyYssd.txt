R.
Venez, divin Messie nous rendre espoir et nous sauver !
Vous êtes notre vie ! Venez, venez, venez !

1.
Ô Fils de Dieu, ne tardez pas,
Par votre corps, donnez la joie à notre monde en désarroi.
Redites-nous encore de quel amour vous nous aimez;
Tant d’hommes vous ignorent, venez, venez, venez !

2.
À Bethléem, les cieux chantaient
Que le meilleur de vos bienfaits c’était le don de votre paix.
Le monde la dédaigne : partout les cœurs sont divisés !
Qu’arrive votre règne, venez, venez, venez !

3.
Vous êtes né pour les pécheurs.
Que votre grâce, ô Dieu Sauveur, dissipe en nous la nuit, la peur !
Seigneur, que votre enfance nous fasse vivre en la clarté.
Soyez la délivrance, venez, venez, venez !

4.
Quand vous viendrez au dernier jour
Juger le monde sur l’amour, que nous veillions pour ce retour !
Que votre main nous prenne dans le Royaume des sauvés !
Que meure enfin la haine, venez, venez, venez !