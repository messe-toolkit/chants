R.
Restez en tenue de service, et gardez
vos lampes allumées !
Soyez comme des gens qui attendent
leur maître à son retour.

1.
Soyez comme des gens
qui attendent leur maître
à son retour des noces pour lui ouvrir
dès qu’il frappera à la porte.

2.
Heureux les serviteurs
que le maître, à son arrivée,
trouvera en train de veiller.

3.
Amen, je vous le dis,
il prendra leur tenue de service,
les fera passer à table
et les servira chacun à son tour. 