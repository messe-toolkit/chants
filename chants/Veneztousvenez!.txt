R.
VENEZ TOUS, VENEZ ! VENEZ TOUS, VENEZ,
AU PUITS DE LA RENCONTRE !
VENEZ TOUS, VENEZ ! VENEZ TOUS, VENEZ,
LE SEIGNEUR NOUS ATTEND :
C’EST LUI QUI DONNE L’EAU VIVE,
C’EST LUI QUI EST LA VIE ! (bis)

« Venez et vous verrez. » (Jean 1, 39)

1.
Au puits des Origines,
Il rejoint notre histoire :
Voyez, croyez en Lui,
Il nous donne Son Nom !

« Écoutez et vous vivrez ! » (Isaïe 55,3)

2.
Au puits de la Parole,
Il s’inscrit dans les coeurs :
Goûtez, ouvrez le Livre,
Il nous donne Sa Loi !

« Venez à l’écart et reposez-vous un peu » (Marc 6, 31)

3.
Au puits de la Sagesse
Il appelle sans cesse :
Priez et contemplez,
Il nous donne Sa Paix !

« Laissez-vous réconcilier avec Dieu. » (Corinthiens 2 ; 5,20)

4.
Au puits de la Tendresse,
Il pardonne à chacun :
Vivez, séchez vos larmes,
Il nous donne Sa Joie !

« Chaque fois que vous en boirez, faites cela en mémoire de moi » (Corinthiens 1 ; 11, 25)

5.
Au puits de son Alliance,
Il invite à puiser :
Prenez, buvez Son eau,
Il nous donne Sa Vie !

« Comme je vous ai aimés, aimez-vous les uns les autres » (Jean 13, 34)

6.
Au puits de Charité,
Il nous tourne vers l’autre.
Aimez, servez vos frères,
Il nous donne Son Coeur !

« Il nous faut travailler aux oeuvres de Celui qui m’a envoyé. » (Jean 9, 4)

7.
Au puits de la Mission,
Il nous veut ouvriers :
Semez et moissonnez,
Il nous donne Sa Force !

« Proclamez la Bonne Nouvelle à toute la création ! » (Marc 16, 15)

8.
Au puits de l’Espérance,
Il nous nomme prophètes :
Osez, criez Son Nom,
Il nous donne Sa Voix !

« Allez dans le monde entier. » (Marc 16, 15)

9.
Du puits de la Promesse,
Il envoie dans le monde :
Allez, portez l’Eau Vive,
Il nous donne l’Esprit !