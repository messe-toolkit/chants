R.
Jésus, ressuscité des morts,
L´univers te chante sa joie,
Jésus ressuscité des morts,
L´univers te chante alléluia !

1.
Au matin de Pâques,
Sans faire de bruit,
Alors que pour nous l´espoir s´était enfui,
Christ est ressuscité !

2.
Qu´exultent la terre et l´univers entier !
La mort est vaincue et l´enfer dévasté,
Christ est ressuscité !

3.
Toi, le Bien-aimé, pour nous tu fus blessé,
Mais de ton sommeil, oui, tu t´es relevé,
Christ est ressuscité !

4.
Chantez au Seigneur un cantique nouveau,
Nous sommes sauvés par le sang de l´Agneau,
Christ est ressuscité !

5.
Que les arbres dansent, que crient les vallées,
Que jubile en Dieu tout ce qui fut créé,
Christ est ressuscité !