R.
Un astre nouveau s'est levé,
Ils sont partis dans la nuit
Sur le sentier des âges,
Pour adorer le Roi.
Un enfant les attend,
Il ouvre ses bras au monde entier…
Joie de l'univers, Ô joie de l'univers,
Voici l'aurore du royaume !

1.
Allons nous prosterner
Devant le Seigneur,
Le Créateur des étoiles.

2.
Offrons nos vies au Prince de la Paix,
Soleil qui vient nous visiter !