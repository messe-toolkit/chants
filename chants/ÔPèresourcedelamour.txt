1.
Ô Père, Source de l'amour,
Tu nous as gardés en ce jour dans ta tendresse.
Si je n'ai pas Compris ta voix,
ce soir je rentre auprès de toi,
et ton pardon me sauvera de la tristesse.

2.
Seigneur, Étoile sans déclin,
Toi qui vis aux siècles sans fin, près de ton Père !
Ta main, ce jour, nous a conduits,
Ton corps, ton sang nous ont nourris
Reste avec nous en cette nuit, Sainte lumière.

3.
Seigneur, Esprit de vérité,
ne refuse pas ta clarté à tous les hommes.
Éteins la haine dans les cœurs,
et que les pauvres qui ont peur
D'un lendemain sans vrai bonheur en paix s'endorment.

4.
Seigneur, reviendras-tu ce soir
pour combler enfin notre espoir par ta présence ?
La table est mise en ta maison,
où près de toi nous mangerons.
Pour ton retour, nous veillerons pleins d'espérance.