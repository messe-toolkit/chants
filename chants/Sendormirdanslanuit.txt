1.
S'endormir dans la nuit,
Se réveiller dans ta lumière,
Les doigts serrés sur une pierre,
Petit caillou bien ordinaire,
Et pourtant
Précieux et blanc,
Qu'un jour tu m'as remis.

2.
S'endormir dans la paix,
Ouvrir les yeux sur ton visage,
Et découvrir que ton image,
Presqu'effacée du coeur trop lâche,
Est pourtant
Fidèlement
L'eau vive où je renais.

3.
S'endormir dans tes mains,
Se trouver en ta présence
À balbutier avec patience
Un nom secret, plein de silence,
Et pourtant
Tu le comprends,
Ce nom, car c'est le tien.

4.
S'endormir sous la croix,
Se reconnaître dans ta gloire,
Alors mon coeur, si lent à croire,
Verra surgir de sa mémoire
Ce matin
Où tu survins
Pour vivre sous mon toit.

5.
Le caillou singulier,
Le nom caché, la source alerte,
Ils sont ta vie toujours offerte,
Ils sont ma vie la plus secrète ;
Tu seras
En ce jour-là,
Toi seul, ma liberté.