Aimez vos ennemis, et priez pour ceux qui vous persécutent,
afin d’être vraiment les fils de votre Père qui est aux cieux.