Antienne
TU VISITES LA TERRE, SEIGNEUR,
TU BÉNIS SES SEMENCES.

1.
Tu visites la terre et tu l’abreuves,
tu la combles de richesses ;

Répons A
À TOI, LA LOUANGE !
Les ruisseaux de Dieu regorgent d’eau,
tu prépares les moissons.

Répons B
À TOI, LA LOUANGE POUR TA PAROLE !

2.
Ainsi, tu prépares la terre,
tu arroses les sillons ;
À TOI, LA LOUANGE !
Tu aplanis le sol, tu le détrempes sous les pluies,
tu bénis les semailles.
À TOI, LA LOUANGE POUR TA PAROLE !

Antienne

3.
Tu couronnes une année de bienfaits,
sur ton passage, ruisselle l’abondance.
À TOI, LA LOUANGE !
Au désert, les pâturages ruissellent,
les collines débordent d’allégresse.
À TOI, LA LOUANGE POUR TA PAROLE !

4.
Sur ton passage ruisselle l’abondance.
Les herbages se parent de troupeaux.
À TOI, LA LOUANGE !
Et les plaines se couvrent de blé.
Tout exulte et chante !
À TOI, LA LOUANGE POUR TA PAROLE !

Antienne