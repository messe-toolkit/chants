1.
Voici la foule immense
Des saints vêtus de blanc,
Tous rassemblés dans l’allégresse
Au festin du royaume des cieux.

2.
Heureux les coeurs de pauvres,
Lavés de tout péché :
Ils ont reçu en héritage
Le Seigneur qu’ils aimaient sans le voir.

3.
Pour eux plus de ténèbres,
Le Christ a resplendi ;
Il s’est levé, le seul visage
Espéré du profond de leur nuit.

4.
Les larmes de détresse
Ne brûlent plus leurs yeux ;
Ils sont finis les jours de peine,
Ils vivront dans l’amour près de Dieu.

5.
Jamais ils ne sommeillent,
Jamais ne vient le soir ;
Sans fin s’élève leur louange
Vers celui qui les a rachetés.

6.
Ils chantent sur la harpe
La gloire de l’Agneau :
Il est vainqueur, le Fils de l’Homme,
Dans les fils délivrés de la mort.

7.
Nos chants, comme une offrande,
Rejoignent ceux du ciel,
Pour acclamer dans tous les siècles
Le Vivant, le Très-Haut, le seul Saint.

8.
Voici l’immense Église,
Le Corps du Dieu fait chair
Qui nous guérit par ses blessures
Et nous sauve en son sang répandu.