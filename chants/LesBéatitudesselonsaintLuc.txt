Jésus descendit de la montagne avec ses disciples
Et s´adressa à la foule qui le suivait en disant :

1.
"Heureux, vous, les pauvres : le Royaume de Dieu est à vous !
Heureux, vous qui avez faim maintenant : vous serez rassasiés !"

Réjouissez-vous et bondissez de joie,
Car votre récompense est grande dans le ciel !
Réjouissez-vous et bondissez de joie,
Car votre récompense est grande dans le ciel !

2.
"Heureux, vous, les hommes qui pleurez maintenant : vous rirez !
Heureux les persécutés, les bannis qui m´auront annoncé !"

3.
"Ainsi que mon Père est miséricordieux pour tout homme,
Soyez, vous aussi, remplis de bonté, miséricordieux !"

4.
"Tout homme qui garde ma Parole et la met en pratique,
Vraiment, cet homme a bâti sa maison sur le roc", dit Jésus.