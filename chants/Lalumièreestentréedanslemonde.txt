La lumière est entrée dans le monde,
Sion, prépare ta demeure
pour accueillir ton Roi, le Christ.
LUMIÈRE DU CHRIST,
NOUS TE BÉNISSONS.

En Marie, Vierge toute pure,
Le Verbe de Dieu s’est fait chair pour nous.
LUMIÈRE DU CHRIST,
PURE LUMIÈRE,
NOUS TE GLORIFIONS.

Aujourd’hui, la Mère du Seigneur s’avance,
la voici qui porte le Roi de gloire,
Fils de Dieu engendré avant l’aurore.
LUMIÈRE DU CHRIST,
LUMIÈRE NÉE DE LA LUMIÈRE,
JOIE ÉTERNELLE DANS NOS COEURS,
NOUS T’ACCLAMONS.

Siméon, conduit par l’Esprit,
se hâte vers le Temple à la rencontre du Messie.
LUMIÈRE DU CHRIST,
NOUS TE BÉNISSONS.

L’Emmanuel, Seigneur souverain,
pénètre dans le sanctuaire;
les temps sont accomplis.
LUMIÈRE DU CHRIST,
PURE LUMIÈRE,
NOUS TE GLORIFIONS.

Un jour nouveau éclaire le Prophète,
Il voit de ses yeux le Sauveur,
il l’annonce à toutes les nations.
LUMIÈRE DU CHRIST,
LUMIÈRE NÉE DE LA LUMIÈRE,
JOIE ÉTERNELLE DANS NOS COEURS,
NOUS T’ACCLAMONS.

Voici dans le Temple
une source de lumière et de feu
pour purifier le monde pécheur.
LUMIÈRE DU CHRIST,
NOUS TE BÉNISSONS.

De l’Orient, de l’Occident,
accourez dans l’allégresse
à la rencontre du Roi Sauveur.
LUMIÈRE DU CHRIST,
PURE LUMIÈRE,
NOUS TE GLORIFIONS.

Approchez de lui vous tous qui l’aimez
et vous serez illuminés.
LUMIÈRE DU CHRIST,
LUMIÈRE NÉE DE LA LUMIÈRE,
JOIE ÉTERNELLE DANS NOS COEURS,
NOUS T’ACCLAMONS.

Tenez en main vos lampes allumées;
enfants de Dieu qui le cherchez
vous ne marcherez plus dans les ténèbres.
LUMIÈRE DU CHRIST,
NOUS TE BÉNISSONS.

Car voici la plénitude des temps,
tous les peuples sont appelés
à l’espérance bienheureuse de la résurrection.
LUMIÈRE DU CHRIST,
PURE LUMIÈRE,
NOUS TE GLORIFIONS.

Le Christ, hier, aujourd’hui et toujours,
Maître de la mort et de la vie,
il demeure pour les siècles de siècles.
LUMIÈRE DU CHRIST,
LUMIÈRE NÉE DE LA LUMIÈRE,
JOIE ÉTERNELLE DANS NOS COEURS,
NOUS T’ACCLAMONS.
JOIE ÉTERNELLE DANS NOS COEURS,
NOUS T’ACCLAMONS.
JOIE ÉTERNELLE DANS NOS COEURS,
NOUS T’ACCLAMONS.

NOUS T’ACCLAMONS,
NOUS T’ACCLAMONS.
NOUS T’ACCLAMONS,
NOUS T’ACCLAMONS,
NOUS T’ACCLAMONS.