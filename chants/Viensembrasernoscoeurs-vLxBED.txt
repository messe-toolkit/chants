R.
Viens, Esprit Saint, viens embraser nos cœurs
Viens au secours de nos faiblesses.
Viens, Esprit Saint,
viens Esprit consolateur,
Emplis-nous de joie et d’allégresse !

1.
Viens en nos âmes lasses, Esprit de sainteté.
Viens nous combler de grâce
et viens nous sanctifier.
Viens guérir nos blessures,
toi, le Consolateur,
Viens, Source vive et pure,
apaiser notre cœur !

2.
Envoyé par le Père, tu viens nous visiter,
Tu fais de nous des frères,
peuple de baptisés.
Enfants de la lumière,
membres de Jésus-Christ,
Nous pouvons crier “Père”
d’un seul et même Esprit.

3.
En nos cœurs viens répandre
les dons de ton amour,
Viens inspirer nos langues
pour chanter Dieu toujours.
\n
Viens, Esprit de sagesse,
viens prier en nos cœurs.
Viens, et redis sans cesse :
Jésus-Christ est Seigneur !