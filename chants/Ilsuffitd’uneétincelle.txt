QUELLE JOIE DE PORTER LA BONNE NOUVELLE
LA PAROLE QUI REND HEUREUX
PUISQU’IL SUFFIT D’UNE ÉTINCELLE
POUR QU’UN JOURNAL ALLUME UN FEU

1.
Cette Parole est l’amour
C’est Jésus Christ qui s’est offert
Et qui s’offre à nous chaque jour
En des chapitres à ciel ouvert

2.
Cette Parole est semée
Et à cueillir au fond des yeux
Dans les regards écarquillés
Par un petit coin de ciel bleu

3.
Cette Parole est servie
Comme on se partage le pain
Elle donne en bouche un goût de vie
Des geste ouverts à pleines mains

4.
Cette Parole est la paix
Elle est une bénédiction
Elle est pour les coeurs aux aguets
Comme un appel à la mission