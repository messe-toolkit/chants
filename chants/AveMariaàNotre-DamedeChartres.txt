R.
Ave Ave Maria
Ave Ave Maria

1.
Je te salue, Marie, pleine de grâce,
Le Seigneur est avec toi,
Tu es bénie, entre toutes les femmes
Et Jésus, le fruit de tes entrailles, est béni.

2.
Sainte Marie, Mère de Dieu
Prie pour nous pauvres pécheurs
Maintenant et à l’heure de notre mort.