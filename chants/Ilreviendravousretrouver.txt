1.
Une nuée vient te cacher
Devant leurs yeux tout étonnés
Le regard figé vers le ciel
Ils te cherchent dans le soleil.

HOMMES DE GALILÉE
DISAIENT CES GENS
VÊTUS DE BLANC
POURQUOI VOUS ATTRISTER.
POURQUOI RESTER.
LÀ À PLEURER.
COMME VOUS L’AVEZ VU S’EN ALLER.
IL REVIENDRA VOUS RETROUVER.

2.
Tu me demandes à ma façon
D’annoncer ta résurrection
C’est pas facile de témoigner
Donne-moi la force de parler.

3.
Après le baptême de l’Eau
Qui fit de moi l’Enfant nouveau
Envoie le baptême du Feu
La force de l’Esprit de Dieu.