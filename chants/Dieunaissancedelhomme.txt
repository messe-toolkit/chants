1.
Dieu, naissance de l’homme,
Dieu, nouvelle d’espoir !
Tu prends visage d’enfant,
Quand le vieux monde
Croule entre ses remparts
Et se ferme à l’avenir.
Dieu, naissance de l’homme,
Dieu, nouvelle d’espoir
Au cœur du monde.

2.
Dieu, aurore de l’homme,
Dieu, nouvelle du jour,
Tu prends le cri d’un enfant
Quand le vieux monde
Guette au fond de sa nuit
Un matin qui ne vient plus,
Dieu, aurore de l’homme,
Dieu, nouvelle du jour
Au cœur du monde.

3.
Dieu, promesse de l’homme,
Dieu, nouvelle d’amour,
Tu brilles aux yeux d’un enfant
Quand le vieux monde
Cherche de nouveaux puits
Aux mirages de sa soif
Dieu promesse de l’homme,
Dieu nouvelle d’amour
Au cœur du monde.

4.
Dieu, tendresse de l’homme,
Dieu, nouvelle de paix,
Tu bats au cœur d’un enfant
Quand le vieux monde
Tisse des barbelés
Aux frontières verrouillées
Dieu, tendresse de l’homme,
Dieu nouvelle de paix
Au cœur du monde.

5.
Dieu, Noël de tout homme,
Dieu nouvelle de joie,
Tu danses aux pas d’un enfant
Quand le vieux monde
Pleure le temps perdu
À garder tous ses trésors
Dieu, Noël de tout homme,
Dieu, nouvelle de joie
Au cœur du monde.