LA TENDRESSE ET LA PRIÈRE
ONT FAIT DE TOI, UN PÈLERIN
QUI RECHERCHE LA LUMIÈRE
À CHAQUE INSTANT DU QUOTIDIEN (bis)

1.
Merci pour tant d'Amour et de beauté
Qui naissent dans ton coeur et dans tes mains
Tu mets des mots d'étoiles et de soleil
Dans les images que tu peins !

2.
Merci pour tes poèmes au fils des jours,
Comme des grains semés dans notre vie
Quand tes talents d'artiste font rêver
Tous ceux qui t'aiment... à l'infini !

3.
Merci pour les moissons de votre Amour
Il a nourri nos coeurs et nos esprits
Tu as comblée de joie ta bien aimée
Soixante années, tu l'as chérie !

4.
Merci pour ce qui brûle au fond de toi
Ce feu qui vient d'en haut nous éclairer
La Foi qui te fait vivre et t'a conduit
En haut du Mont des Oliviers !

5.
Merci de nous avoir donné la vie,
Tes descendants se pressent autour de toi,
Comme des grains de sable et de soleil,
Ils sont nombreux, ils sont heureux !