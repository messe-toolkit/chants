LE RÈGNE DE DIEU EST PROCHE
IL EST PARMI VOUS!
LE RÈGNE DE DIEU EST PROCHE
IL EST PARMI NOUS!
ALLÉLUIA ! ALLÉLUIA ! SHALOM ALLELOU!
ALLÉLUIA ! ALLÉLUIA ! SHALOM ALLELOU!

1.
Il ressemble à un homme
qui a semé la semence
dans son champ.
Et le jour, comme la nuit,
qu’il soit debout ou qu’il dorme,
le blé germe et grandit.
La terre produit d’elle-même
l’herbe et l’épi.
Puis on moissonne à la faucille,
dans la joie et les chants.

2.
Il ressemble à une graine,
la plus petite de toutes :
le sénevé.
On la sème, elle grandit
bien plus que toutes les autres ;
elle les dépasse toutes
Elle met de longues branches
pour abriter
Les oiseaux qui cherchent son ombre
et viennent faire leur nid.

3.
Il ressemble à un brin,
une toute petite mesure
de levain.
Une femme l’enfouit
et lentement la mélange
à beaucoup de farine.
Bientôt la pâte bouge,
gonfle et s’anime,
Et le petit brin de levure
fait lever tout le pain.