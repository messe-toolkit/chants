TÉMOINS DE TA TENDRESSE DANS NOS VIES,
SEIGNEUR, NOUS TE CHANTONS: MERCI !
NOUS VOULONS DIRE ENSEMBLE, DANS LA JOIE :
EN TOI NOTRE ESPÉRANCE ET NOTRE FOI.

1.
Seigneur, plein de tendresse Alléluia !
Je suis sûr que tu m’aimes Alléluia !
Pour Toi je suis très important,
Tu me connais par mon prénom.
Avec tous mes amis, je veux te louer en chantant.

2.
Pour mes parents, mes frères Alléluia !
Et pour tous ceux qui m’aiment Alléluia !
Pour les sourires partagés
La gentillesse et l’amitié.
Avec tous mes amis, je veux te louer en chantant.

3.
Pour toutes mes réussites Alléluia !
Mes progrès, mes victoires Alléluia !
Pour ce travail dont je suis fier
Les choses que j’arrive à faire.
Avec tous mes amis, je veux te louer en chantant.

4.
Pour la vie que tu donnes Alléluia !
Pour tant de choses bonnes Alléluia !
Et pour les peines partagées
Qui sont plus faciles à porter.
Avec tous mes amis, je veux te louer en chantant.

5.
Pour la terre si belle Alléluia !
Le ciel et la lumière Alléluia !
Pour la pluie, la clarté de l’eau
Et pour la chanson des oiseaux.
Avec tous mes amis, je veux te louer en chantant.