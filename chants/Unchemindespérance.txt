R.
Inventons un chemin d’espérance
Au creux de la nuit
Pour ouvrir les premiers pas de danse
Aux rythmes de l’Esprit.

1.
Une poignée de jours
Que tu jettes en nos vies,
Une poignée d’amour
Que tu sèmes en nos nuits,
Bientôt la moisson lèvera
Au soleil de l’Esprit.

2.
Une poignée de paix
Que tu caches en nos peurs,
Poignée de vérité
Qui germe en notre cœur,
Bientôt la moisson lèvera
Au grand vent de l’Esprit.

3.
Une poignée de mains
Qui s’ouvrent sur la croix,
Une poignée de grains
Qui fait bondir la joie,
Bientôt la moisson lèvera
Au souffle de l’Esprit.