1.
Ne voyez-vous pas ces infirmes qui marchent
Ne voyez-vous pas ces lépreux guéris ?
Ces gens accablés qui se remettent en marche
Et ces morts vivants qui reprennent vie ?
Allez dire à tous les hommes le temps de Dieu est arrivé
Voici venir son royaume il est déjà commencé.

2.
Regardez les yeux des aveugles qui s’ouvrent
Dans leur nuit se lève une aurore de foi
Ceux qui étaient sourds aux autres découvrent
Ces appels à l’aide qu’ils n’entendaient pas.

3.
Ne voyez-vous pas ces montagnes qui tombent
Les ravins de haine sont enfin comblés
On a démoli les murs qui nous encombrent
Les gens de tous bords vont se rencontrer.

4.
Ouvrez les prisons et rasez les bastilles
Pour que les captifs soient tous libérés
Drogues et alcools passent derrière les grilles
Et nos chaînes d’or il faut les donner.

5.
Froissez les épis des doctrines en « isme »
Pour que tous les hommes vivant libres enfin
Brisez les écorces de vos égoïsmes
Et chacun pourra manger à sa faim.

6.
Voyez ce banquet où le Seigneur appelle
Les saints, les pécheurs, riches et mendiants
Où les pauvres entendent la Bonne Nouvelle
Les théologiens parlent simplement.