STANCE
Un messager a couru devant,
Pour annoncer aux pauvres :
Soyez dans la joie, le désert va fleurir,
Les captifs de Sion bientôt vont revenir,

R.
Soyez dans la joie, le Seigneur vient,
Alléluia, le Royaume est tout proche !

1.
Il y eut un homme, envoyé de Dieu ; son nom était Jean.
Il est venu comme témoin de la lumière.

2.
Jean était la voix qui crie dans le désert :
«Préparez le chemin du Seigneur !»

3.
A ceux qu’il baptisait dans l’eau du Jourdain, Jean disait :
«Il vient, celui qui vous baptisera dans l’Esprit Saint !’