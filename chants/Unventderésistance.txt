UN VENT DE RÉSISTANCE NOUS RÉVEILLE,
L'AVENIR EST UN COMBAT.
UN SOUFFLE D'ESPÉRANCE NOUS ENTRAÎNE :
AVEC NOUS LE SEIGNEUR COMBAT !

1.
Quand la violence nous agresse,
Ensemble bâtissons la maison de paix !
Ouvrons la porte à la sagesse,
Que cesse la folie dans les coeurs fermés !

2.
Quand les tempêtes se déchaînent,
Qui donc est assez fort face à l'ouragan ?
Soyons des frères solidaires,
Le monde connaîtra de nouveaux élans.

3.
Sur notre terre en mal de vivre,
La soif de liberté nous conduit très loin.
Offrons à tous un peu d'eau vive,
L'espoir de cheminer vers des lendemains.

4.
L'humanité cherche une étoile,
Le Christ est le Vivant qui nous guidera.
Au long des temps il met sa gloire
À nous accompagner toujours au-delà.