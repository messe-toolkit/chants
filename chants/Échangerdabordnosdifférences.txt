R.
Échanger d’abord nos différences,
Partager ensuite nos talents,
Echanger aussi nos ressemblances,
Partager nos rêves les plus grands.

1.
Transformer nos mystères en lumière,
Transformer même nos nuits en jour,
Effacer nos étroites frontières,
Percevoir le grand vent de l’amour.

2.
Retrouver le goût de l’Espérance,
Accueillir “les enfants des bougies”,
Questionner et comprendre un silence,
Exister comme un arbre et ses fruits.

3.
Découvrir la trace provisoire,
Ecouter un nomade muet,
Devenir un “Signe” dans l’Histoire,
Rallumer la flamme inanimée.