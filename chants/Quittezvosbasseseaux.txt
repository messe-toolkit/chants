R.
QUITTEZ VOS BASSES EAUX
LES STEPPES DE VOS BAGNES
RAS-DE-TERRE ET TOMBEAUX
VENEZ SUR LA MONTAGNE!

1.
Il faut tout un désert
Parlant en paraboles
Pour qu’au silence ouvert
Fleurisse une Parole.

2.
Une question est née
Tout au fond de moi-même,
Certitude étonnée
Qu’il existe un « je t’aime »

3.
Aujourd’hui j’étais mort :
J’entends la vie qui craque,
J’entends la vie qui sort,
Je choisis une Pâque !

4.
Suis-je donc assez fou
Pour croire une présence :
Dieu comme un rendez-vous,
L’homme comme une chance ?