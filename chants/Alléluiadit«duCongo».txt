Alléluia, alléluia, alléluia, alléluia.
Alléluia, alléluia, Alléluia !

Sango elamu to ndima e
Nganga Nzambe toyokaye
Sango elamu to ndima e
Alléluia !

Acclamons la Parole de Dieu !
Acclamons la Bonne Nouvelle !
Acclamons : Jésus est vivant !
Alléluia !

Jésus Christ est notre Seigneur.
Jésus Christ est notre sauveur !
Sa Parole nous donne la vie.
Alléluia !

Gloire à Toi, Parole du Père !
Gloire à Toi, Jésus le Seigneur !
Donnenous l’Esprit qui libère.
Alléluia !