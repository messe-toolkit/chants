R.
Jubilez, criez de joie,
par le Fils et dans l'Esprit,
Dieu nous ouvre un avenir !
Jubilez, criez de joie,
Il nous donne par la foi
un amour qui fait grandir.

1.
Rendons grâce à notre Père
car de lui vient toute vie,
pour ce temps de notre terre
rayonnant de son Esprit.

2.
Acclamons tous la victoire
de Jésus ressuscité,
il s’élève dans la gloire
où nous sommes appelés.

3.
A l’Esprit qui nous éclaire,
la louange des vivants,
il nous mène vers le Père
qui nous prend pour ses enfants.

4.
Pour l'Eglise des apôtres,
pour l'Eglise des martyrs,
Pour l'amour donné aux autres,
seul chemin vers l'avenir.

5.
Pour l'Eglise au long des âges,
assemblée des bienheureux,
Qui annonce le message
de l'amour de notre Dieu.

6.
Pour l'Eglise encore en marche
prenant vie en Jésus Christ,
Que par elle l'homme sache
le bonheur qui est promis.

7.
Avec tous ceux qui rayonnent
de la vie des baptisés,
Avec tous ceux qui pardonnent
à ceux qui les ont blessés.