AUTOUR DE LA TABLE

AUTOUR DE LA TABLE, LAISSONS-NOUS RASSEMBLER,
CELUI QUI NOUS INVITE A SOIF DE SE DONNER,
CELUI QUI NOUS INVITE VIENT TOUJOURS NOUS RENCONTRER.

1.
Sauveur de tout homme, Jésus-Christ
C'est toi qui nous aimes le premier.
Présence discrète dans nos vies
Tu viens marcher sur tous nos chemins.

2.Élan de tendresse, Jésus-Christ
Tu offres l'abondance de ta vie.
Source vivifiante de l'Esprit,
Tu ouvres nos cœurs à l'infini.

3.
Bien-Aimé du Père, Jésus-Christ
Ta vie est lumière pour le monde.
Parole brûlante qui saisit
Tu mets en nous ton souffle de paix !