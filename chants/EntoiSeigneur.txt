1.
En toi, Seigneur, nos vies reposent
Et prennent force dans la nuit ;
Tu nous prépares à ton aurore
Et tu nous gardes dans l'Esprit.

2.
Déjà levé sur d'autres terres,
Le jour éveille les cités ;
Ami des hommes, vois leur peine
Et donne leur la joie d'aimer.

3.
Vainqueur du mal et des ténèbres,
Ô Fils de Dieu ressuscité ;
Délivre-nous de l'adversaire
Et conduis-nous vers la clarté.