Il y a toujours un chemin
Malgré la peur et la violence
Il y a toujours un chemin
Malgré la mort et la souffrance
Il y a toujours un chemin
Car Dieu nous donne une espérance.

Je t’ai cherché Seigneur
quand j’ai vu la guerre déchirer des familles, démembrer des pays,
Je t’ai cherché
quand j’ai vu des enfants martyrisés, meurtris, exploités,
Je t’ai cherché
quand j’ai vu des femmes violées, battues ou maltraitées
Je t’ai cherché
quand j’ai vu le pauvre ou l’étranger, rejetés, humiliés, désemparé
Je t’ai cherché
quand j’ai vu la terre trembler et les eaux tout emporter.

Je t’ai cherché parmi les morts et les vivants
parmi tous ceux qui souffrent, qui luttent et qui espèrent
parmi tous ceux qui rêvent ou qui n’ont plus d’espoir.

Je t’ai cherché, Seigneur, dans les décombres de ma vie,
dans mon malheur, dans ma peine ou ma tristesse
dans mes échecs et mes impasses.

Je t’ai cherché… sans savoir que tu m’avais déjà trouvé
et que la vie allait devant
et que la voie était ouverte !
Vivant parmi les vivants, tu es venu vers moi comme un mendiant
Tu m’as délivré de mes angoisses et de mes doutes…
Tu m’as donné la main, tu m’as ouvert la route
Tu m’as remis debout et tu m’as dit d’avancer et de marcher.

Il y a toujours un chemin
Malgré la peur et la violence
Il y a toujours un chemin
Malgré la mort et la souffrance
Il y a toujours un chemin
Car Dieu nous donne une espérance.