1.
L’Evangile du Seigneur Jésus,
Parmi les tiens tu l’as reçu ;
Dans ton coeur il est Bonne Nouvelle.
Tu nous l’écris en mots fidèles
Et tu le signes de ta vie.

2.
Pas à pas le Christ est révélé,
Avec sa chair d’humanité :
Quel bonheur de marcher près du Maître !
Par toi, Jésus se fait connaître
Et son message nous séduit.

3.
Tu contemples ton Sauveur en croix,
Scandale offert à nos pourquoi,
Et tu chantes l’aurore pascale.
Pour toi aussi survient la Pâque
Et ta naissance à l'Infini.

4.
La lumière du Ressuscité
Rejoint encor nos Galilée,
Sa Parole aujourd'hui nous délivre.
Pour chaque page que tu livres,
Nous rendons grâce à Jésus Christ.