HEUREUX QUI A TROUVÉ LA SAGESSE
L’ARBRE DE VIE C’EST ELLE
POUR CEUX QUI LA SAISISSENT
ET BIENHEUREUX CEUX QUI LA TIENNENT

OU

HEUREUX L’HOMME QUI A TROUVÉ LA SAGESSE
ELLE EST L’ARBRE DE VIE
POUR CEUX QUI LA SAISISSENT
ET CEUX QUI LA POSSÈDENT SONT HEUREUX

La loi du Seigneur est parfaite,
qui redonne vie ;
la charte du Seigneur est sûre,
qui rend sages les simples.

Les préceptes du Seigneur sont droits,
ils réjouissent le coeur ;
le commandement du Seigneur est limpide,
il clarifie le regard.

La crainte qu’il inspire est pure,
elle est là pour toujours ;
les décisions du Seigneur sont justes
et vraiment équitables :

plus désirables que l’or,
qu’une masse d’or fin,
plus savoureuses que le miel
qui coule des rayons.

Aussi ton serviteur en est illuminé ;
à les garder, il trouve son profit.

Qui peut discerner ses erreurs ?
Purifie-moi de celles qui m’échappent.

Préserve aussi ton serviteur de l’orgueil :
qu’il n’ait sur moi aucune emprise.
Alors je serai sans reproche,
pur d’un grand péché.

Accueille les paroles de ma bouche,
le murmure de mon coeur ;
qu’ils parviennent devant toi,
Seigneur, mon rocher, mon défenseur !