R.
Pars vers Dieu qui t'appelle :
Il sauve les pécheurs ;
Dans la vie éternelle rejoins ton rédempteur.

1.
Celui que Dieu protège,
Que peut sur lui la mort ?
Celui que Dieu protège,
Paisiblement s'endort.

2.
Celui que Dieu regarde,
Connaît sa pauvreté ;
Celui que Dieu regarde,
Qui donc peut le cacher ?

3.
Celui que Dieu pardonne,
Sa paix ne finit pas ;
Celui que Dieu pardonne,
Sa joie demeurera.