LA MAISON AUX COULEURS DE LA PAIX,
DIEU L’HABITE ET SA FLAMME L’ÉCLAIRE ;
SES DEMEURES EN TOUT TEMPS SONT LUMIÈRE,
JÉSUS CHRIST S’Y RÉVÈLE À JAMAIS.

1.
Frêle demeure avec ses murs d’humanité,
Dieu l’enrichit de ses trésors et ses merveilles ;
Nous rendons grâce pour les dons de sa tendresse,
Il nous engage dans un monde à rénover.

2.
Libre demeure avec la fête du pardon,
Dieu nous accueille et nous reçoit tels que nous sommes.
Au long des jours sa main nous guide et nous façonne ;
De ses passages dans nos vies nous témoignons.

3.
Belle demeure où la Parole doit fleurir,
Dieu nous appelle à des semailles de justice :
Bon grain tombé pour les moissons de l’Évangile ;
Dans nos sillons, qu’il voie le jour et porte fruit !

4.
Sainte demeure avec le souffle de l’Esprit,
Dieu nous secoue par l’ouragan de Pentecôte.
Son vent nous mène sur la route des Apôtres,
Le même feu brûlant de Pâques nous saisit.

5.
Forte demeure où la violence est rejetée,
Dieu nous entraîne sur sa voie de non-violence :
Chemin de foi qui fait grandir dans l’espérance,
Heureux celui qui gravira pareil sommet !

6.
Vaste demeure où le partage fait la Loi,
Dieu nous invite à proposer la table ouverte.
Pour les aveugles et les boiteux il fait de même,
Le monde entier peut devenir festin de joie.

7.
Noble demeure où le soleil resplendira !
Dieu met sa gloire à nous combler à sa mesure,
Et cet amour produit la paix qui transfigure,
Notre avenir est sa Maison dans l’au-delà.