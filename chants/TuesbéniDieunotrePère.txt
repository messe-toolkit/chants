TU ES BÉNI, DIEU NOTRE PÈRE (Matthieu 5,3-10 et Luc 10,21)

TU ES BÉNI, DIEU NOTRE PÈRE, SEIGNEUR DE L'UNIVERS,
TOI QUI RÉVÈLES AUX PETITS LES MYSTÈRES DU ROYAUME !

1.
Heureux les hommes au cœur de pauvre,
Car le royaume des cieux est à eux !
Heureux les hommes au cœur docile,
Car ils auront la Terre en héritage !

2.
Heureux ceux qui pleurent les larmes de Dieu,
Car ils seront consolés !
Heureux ceux qui ont faim et soif de justice,
Car ils seront rassasiés !

3.
Heureux ceux qui sont miséricordieux,
Il leur sera fait miséricorde !
Heureux ceux qui sont des cœurs purs
Car ils l'auront la lumière dans leurs yeux !

4.
Heureux les artisans de paix
Car ils seront appelés fils de Dieu !
Heureux ceux qui souffrent pour la justice,
Car le royaume des cieux est à eux.