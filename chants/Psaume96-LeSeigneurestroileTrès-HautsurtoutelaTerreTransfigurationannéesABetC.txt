LE SEIGNEUR EST ROI,
LE TRÈS-HAUT SUR TOUTE LA TERRE !

1.
Le Seigneur est roi ! Exulte la terre !
Joie pour les îles sans nombre !
Ténèbre et nuée l’entourent,
justice et droit sont l’appui de son trône.

2.
Quand ses éclairs illuminèrent le monde,
la terre le vit et s’affola ;
les montagnes fondaient comme cire devant le Seigneur,
devant le Maître de toute la terre.

3.
Les cieux ont proclamé sa justice,
et tous les peuples ont vu sa gloire.
Tu es, Seigneur, le Très-Haut
sur toute la terre :
tu domines de haut tous les dieux.

ALLÉLUIA. ALLÉLUIA.

Celui-ci est mon Fils bien-aimé,
en qui je trouve ma joie :
écoutez-le !