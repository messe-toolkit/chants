1.
Son nom béni,
Le Seigneur l'écrivit
Dans le signe du buisson, alléluia,
Flamme vive sans raison, alléluia,
Présence ardente illuminant la création.

2.
Sa loi de vie,
Le Seigneur l'écrivit
Sur la pierre et sur nos fronts, alléluia,
Grâce offerte à la raison, alléluia,
Bonheur à faire en notre chair au plus profond.

3.
Sa propre vie,
Le Seigneur la soumit
À l'humaine condition, alléluia,
Par divine déraison, alléluia,
Toute sagesse en cette grâce se confond.

4.
La mort aussi,
Le Seigneur la souffrit
Pour briser notre prison, alléluia,
Pour qu'en lui nous revivions, alléluia,
“L'amour est fort plus que la mort”, tel est son nom,
Alléluia, alléluia !