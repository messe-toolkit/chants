JE TE RENDS GRÂCE Ô MON DIEU,
POUR TANT DE MERVEILLES.

1.
Tu me scrutes, Seigneur, et tu sais !
Tu sais quand je m’assois, quand je me lève ;
de très loin, tu pénètres mes pensées.
tous mes chemins te sont familiers.

2.
C’est toi qui as créé mes reins,
qui m’as tissé dans le sein de ma mère.
Je reconnais devant toi le prodige,
l’être étonnant que je suis.

3.
Étonnantes sont tes oeuvres
toute mon âme le sait.
Mes os n’étaient pas cachés pour toi
quand j’étais façonné dans le secret.