Ils sont venus de l’Orient
Les mages guidés par l’étoile?!
Ils sont venus manifester
L’enfant des âges?:
Celui qu’honorent leurs présents
Vient pour régner en s’abaissant.

Jésus paraît près du Jourdain,
Il vient se soumettre au Baptiste?!
Jésus paraît pour accomplir
Toute justice?:
Abandonner son rang divin,
En partageant notre destin.

C’est à Cana de Galilée
Qu’advient le premier des miracles?!
Jésus prévient dans ce repas
Le don de Pâques?:
De l’eau en vin est transformée,
Le vin sera son sang versé.

L’heure est venue, nous le savons,
De suivre le Maître qui passe,
L’heure est venue, c’est maintenant,
De rendre grâce.
À Israël et aux nations
Le Fils dévoile sa mission.