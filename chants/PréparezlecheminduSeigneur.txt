R.
PRÉPAREZ LE CHEMIN DU SEIGNEUR.
PRÉPAREZ VOS COEURS
PRÉPAREZ LE CHEMIN DU SEIGNEUR.
CHANGEZ DE VIE !

1.
Annoncez-le
Criez son nom
Toute la terre
Annoncez-le
Par les nations
Il va venir !

2.
Ouvrez les yeux
Témoins brûlants
De sa Lumière
Ouvrez les yeux
Sur votre temps
Il nous rejoint

3.
Plongez-en vous
Changez de vie
Le jour est proche
Plongez-en vous
Changez pour Lui
Il est l’Amour

Pont
Nous déplacerons les montagnes
Nous porterons haut la flamme
Le feu qui ne consume pas
l’Amour qui ne passera pas

4.
Laissez souffler
L’Esprit de Dieu
Sur votre coeur
Laissez souffler
L’Esprit de Dieu
Il est devant