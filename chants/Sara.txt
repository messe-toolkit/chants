R.
C’est vrai que j’ai ri,
C’est vrai que j’ai ri
Aux paroles d’Abraham,
Quand tu lui as dit
Quand tu lui as dit
Que j’allais avoir un enfant de lui.

1.
J’ai souvent désiré
L’enfant de notre amour,
Que mon ventre muet
Me refusait toujours.

Et puis j’ai tant pleuré
Que mes yeux se font lourds.
Tu m’entendais, Yahvé.
Mais tu faisais le sourd !

2.
Il y a si longtemps
Que mon heure est passée
D’abriter un enfant
D’oser même y penser ;
Et c’est toi maintenant
Qui reviens m’annoncer
Que je serai maman
Au bout de tant d’années !

3.
J’étais comme un désert
À l’instant de vieillir,
Quand les flancs de ma terre
Ont appris à frémir.

Fécondant mes hivers
Un enfant va fleurir
Et du fruit de ma chair,
Tout un peuple à venir.

DERNIER REFRAIN
C’est vrai que j’ai ri,
C’est vrai que j’ai ri
Aux paroles d’Abraham,
L’enfant qui viendra,
Tu l’appelleras
L’enfant de mon rire
Et tu l’aimeras.