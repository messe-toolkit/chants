JE PASSERAI MA VIE
À DIRE À TOUS MES FRÈRES
QUE TU ES BON, OUI !
JE PASSERAI MA VIE
À DIRE À TOUS MES FRÈRES
QUE TU ES BON

1.
Oh! Donne à mon coeur
Le rythme de ta joie
Ô Seigneur avec moi !
Oh! Donne à ma joie
Le rythme de ton coeur
Ô Seigneur !

2.
Donne à ma chanson
Le rythme de ta vie
Ô Seigneur mon ami !
Oh! Donne à ma vie
Le rythme des chansons
De moisson !