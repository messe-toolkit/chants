SANS FIN, JE PROCLAMERAI TA VICTOIRE
ET TON SALUT.

1.
Seigneur mon Dieu, tu es mon espérance,
mon appui dès ma jeunesse.
Toi, mon soutien dès avant ma naissance,
tu m’as choisi dès le ventre de ma mère.

2.
Pour beaucoup, je fus comme un prodige ;
tu as été mon secours et ma force.
Je n’avais que ta louange à la bouche,
tout le jour, ta splendeur.

3.
Ma bouche annonce tout le jour
tes actes de justice et de salut.
Mon Dieu, tu m’as instruit dès ma jeunesse,
jusqu’à présent, j’ai proclamé tes merveilles.

4.
Si haute est ta justice, mon Dieu,
toi qui as fait de grandes choses :
Dieu, qui donc est comme toi ?
Tu seras ma louange toujours !