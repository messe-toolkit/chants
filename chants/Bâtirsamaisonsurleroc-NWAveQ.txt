R.
Bâtir sa maison sur le roc,
C'est bâtir sa vie sur le Christ,
Se mettre à son école,
En écoutant sa parole.

1.
Qui écoute ses paroles
Et qui les met en pratique
Est comme un homme avisé
Qui a bâti sa maison sur le roc :
Les vents se sont déchaînés
Et la pluie est tombée,
Les torrents ont déferlé
Contre cette maison,
Et rien n'a pu l'ébranler.

2.
Qui écoute ses paroles
Et ne les pratique pas
Est comme un homme insensé
Qui a bâti sa maison sur le sable :
Les vents se sont déchaînés
Et la pluie est tombée,
Les torrents ont déferlé
Contre cette maison,
Très vite elle s'est écroulée.