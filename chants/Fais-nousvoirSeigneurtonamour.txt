R.
Fais-nous voir, Seigneur, ton amour, et donne-nous ton salut

Psaume 84.
J'écoute: Que dira le Seigneur Dieu?
Ce qu'il dit, c'est la paix pour son peuple.
Son salut est proche de ceux qui le craignent,
et la gloire habitera notre terre.

Amour et vérité se rencontrent,
justice et paix s'embrassent;
la vérité germera de la terre
et du ciel se penchera la justice.

Le Seigneur donnera ses bienfaits,
et notre terre donnera son fruit.
La justice marchera devant lui,
et ses pas traceront le chemin.