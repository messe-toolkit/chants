1.
Les brumes s’enlacent
Au coeur de ma vie
Et revient l’image
De ceux que j’oublie
De ceux qui un jour
Se sont endormis
Comme meurent les flammes
Au seuil de la nuit

2.
Ceux qui n’avaient rien
Que les secrets du coeur
Qui vous aident à grandir
Et ceux qui connaissaient
Les chemins du bonheur
Sans les mots pour le dire
Ceux qui étaient blessés
Par une vie trop rude
Qu’ils aimaient tant pourtant
Et qui les a trompés
Comme les feuilles sont fauchées
Dans un grand coup de vent

3.
Les brumes s’enlacent
Au coeur de ma vie
Et revient l’image
De ceux que j’oublie
De ceux qui un jour
Se sont endormis
Comme meurent les flammes
Sous un ciel de pluie

4.
Tous ceux qui un jour
Se sont endormis
Les reverrons-nous
Dans d’autres pays ?
Nous les reverrons
Dans l’autre pays.