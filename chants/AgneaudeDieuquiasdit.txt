1.
Agneau de Dieu, qui as dit :
« Je vous donne ma vie »,
Prends pitié de nous,
Seigneur, guéris-nous.
PRENDS PITIÉ DE NOUS,
SEIGNEUR, GUÉRIS-NOUS.

2.
Agneau de Dieu, qui as dit :
« Je vous donne l’Esprit »,
Prends pitié de nous,
Seigneur, guide-nous.
PRENDS PITIÉ DE NOUS,
SEIGNEUR, GUIDE-NOUS.

3.
Agneau de Dieu, qui disais :
« Je vous donne ma paix »,
Prends pitié de nous,
Seigneur, unis-nous.
PRENDS PITIÉ DE NOUS,
SEIGNEUR, UNIS-NOUS.