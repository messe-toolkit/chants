R.
CHANTONS LE CHEMIN D'ÉVANGILE,
ENSEMBLE, LA ROUTE JÉSUS-CHRIST;
PRENONS LE CHEMIN D'ÉVANGILE,

1.
Il est venu chez nous, vivre parmi les hommes,
il est devenu grain caché pour ton Royaume
TON AMI SEIGNEUR.
Aujourd'hui, tu nous dis:
"Je suis lumière et vie".

2.
Il a semé pour toi, sur l'espace des monts,
le grain de ta Parole: l'amour et le pardon
TON AMI SEIGNEUR.
Aujourd'hui, tu nous dis:
"Heureux, vous, mes amis".

3.
Il a laissé l'enfant venir dans sa maison,
il accueille le pauvre, dans la joie nous chantons
TON AMI SEIGNEUR.
Aujourd'hui, tu nous dis:
"Bienheureux les petits".

4.
Il a voulu, Seigneur, de la crèche à la croix,
parcourir le chemin où tu donnes le pain
TON AMI SEIGNEUR.
Aujourd'hui, tu nous dis:
"Je suis le pain de vie".

5.
Comme pierres vivantes, au souffle de l'Esprit,
il rassemble ses frères, priant avec Marie
TON AMI SEIGNEUR.
Aujourd'hui, tu nous dis:
"Prends avec toi Marie".

6.
Dieu nous rassemble tous, c'est lui qui nous choisit
pour devenir des frères, sur les chemins de vie
TES AMIS SEIGNEUR.
Aujourd'hui, nous voici:
Famille de Marie.