Réjouis-toi Marie comblée de grâce,
Le Seigneur est avec toi
Tu es bénie entre toutes les femmes,
Et Jésus le fruit de ton sein est béni !
\n
Sainte Marie, ô mère de Dieu,
Prie pour nous, pauvres pécheurs,
Dès maintenant et à l’heure de la mort,
Amen !