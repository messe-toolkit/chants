R.
DIEU QUI NOUS LIBÈRES, EN TON NOM,
NOUS ACCUEILLERONS LES HUMBLES ET LES PAUVRES.

1.
Heureux qui s’appuie sur le Seigneur son Dieu ;
il garde à jamais sa fidélité,
il fait justice aux opprimés,
aux affamés, il donne le pain.

2.
Le Seigneur délie les enchaînés,
le Seigneur ouvre les yeux des aveugles,
Le Seigneur redresse les accablés,
Le Seigneur aime les justes.

3.
Le Seigneur protège l’étranger,
il soutient la veuve et l’orphelin,
il égare les pas du méchant.
D’âge en âge, le Seigneur régnera !