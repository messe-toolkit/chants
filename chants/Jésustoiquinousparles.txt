R.
Jésus, toi qui nous parles,
Tu es vraiment notre Sauveur !

Pour les cinq dimanches de Carême années A B C

1.
Force auprès du faible pour la vie éternelle,
grâce à toi, nous pouvons lutter.

2.
Fils aimé du Père pour la vie éternelle,
grâce à toi, nous pouvons chanter.

Pour les dimanches de Carême année A

3.
Source jaillissante pour la vie éternelle,
grâce à toi, nous n'avons plus soif.

4.
Source de lumière pour la vie éternelle,
grâce à toi, nous pouvons marcher.

5.
Sève printanière pour la vie éternelle,
grâce à toi, nous pouvons grandir.

Pour les dimanches de Carême année B

3.
Route vers le Père pour la vie éternelle,
grâce à toi, nous pouvons prier.

4.
Signe d'alliance pour la vie éternelle,
grâce à toi, nous n'avons plus peur.

5.
Signe d'espérance pour la vie éternelle,
grâce à toi, nous vivons en paix.

Pour les dimanches de Carême année C

3.
Maître de patience pour la vie éternelle,
grâce à toi, nous changeons nos coeurs.

4.
Fleuve de tendresse pour la vie éternelle,
grâce à toi, nous pouvons aimer.

5.
Frère du plus pauvre pour la vie éternelle,
grâce à toi, nous n'avons plus faim.