1.
Seigneur, mon Dieu, je m'éveille à toi,
depuis longtemps tu me fais si gne ;
ton coeur de Père ne se lasse pas
quand il attend le fils prodigue.

R.
Peu à peu, je t'ai rencontré
Dans le bruit, au coeur du silence,
J'ai marché, rempli d'espérance,
Pas à pas, je t'ai trouvé !

2.
Seigneur, mon Dieu, je reviens à toi,
le coeur ouvert à ta Parole ;
l'Esprit de Paix a pris racine en moi,
je sais combien tu me pardonnes !

R.
Peu à peu, je t'ai rencontré
Dans le bruit, au coeur du silence,
J'ai marché, rempli d'espérance,
Pas à pas, je t'ai trouvé !