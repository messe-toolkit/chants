R.
TOUS LES GENS DE LA TERRE ONT LES MEMES DROITS,
LE MEME SOLEIL QUE TOI ET MOI.
TOUS LES GENS DE LA TERRE ONT LES MEMES DROITS,
LE MEME SOLEIL QUE TOI ET MOI.
OUVERTURE
Frère de Varsovie ou de Santiago,
De Pékin, Nouméa, Kabbul ou Soweto,
A quelque deux mille ans de la Bonne Nouvelle,
A quelque deux cents ans de cet espoir nouveau,
Je t’offre ma chanson, comme on fait un cadeau:

1.
Que tu sois homme ou femme, enfant ou bien vieillard,
Laid ou beau, riche ou pauvre, jaune, blanc, rouge ou noir,
Au-delà de ta langue et de tes opinions
De ton lieu de naissance et de ta religion,
Tu as le même droit au bonheur, à la paix,
A vivre en liberté et en sécurité.

2.
Tu ne seras esclave, enfermé sans raison
Dans les murs d’un asile ou ceux d’une prison.
Tu ne seras puni sans être défendu
Ni arbitrairement exilé, détenu.
Tu ne seras soumis à un sort dégradant,
La torture et la mort, la contrainte et le sang.

3.
Tu es libre d’avoir ton pays, ta maison,
De parcourir le monde, de choisir ta nation.
Tu es libre d’avoir un foyer, des enfants,
D’aimer et d’être aimé, de vivre décemment.
Tu es libre de dire ce qui te semble droit,
De prier au grand jour le Dieu auquel tu crois.