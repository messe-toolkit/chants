1.
Où donc vas-tu, Samaritaine,
Avec ta cruche sur les reins ?
Elle est profonde et si lointaine
L’eau qui t’épuise au quotidien…
Tu es ma sœur, mon âme en peine,
Invite-moi sur ton chemin…

SUR LES SENTIERS DE L’IMPRÉVU,
LOIN DES AUTOROUTES À PÉAGE,
J’AIMERAIS TANT COURIR PIEDS NUS
D’AUTRES RIVAGES…
D’AUTRES VISAGES.

2.
Vers quels pays tes pas te mènent,
Toujours errant, Samaritain ?
Passent les jours et les semaines,
Tu marches seul vers ton destin…
Tu es mon frère un peu bohème :
Invite-moi sur ton chemin…

3.
Samaritain, Samaritaine,
Mon masculin, mon féminin,
Aux carrefours et aux fontaines
Nous nous croisons parfois soudain…
Simple prochain, libre prochaine,
Invitez-moi sur vos chemins…

4.
Toi Cléophas, dont les pieds traînent
Tant de boulets, tant de chagrins,
Le premier soir de la semaine
Vois-tu venir Celui qui vient ?
Il fait nos routes plus humaines :
Invitons-le sur nos chemins…