1.
Tu leur as dit : « prenez ma vie !
Aucune femme ne m’attend
Que vous importe, moi ou lui ?
Laissez-moi répondre présent ! »

AINSI PARLAIT, À SES GARDIENS
MAXIMILIEN
QUI S’EN ALLAIT MOURIR DE FAIM
AINSI PARLAIT, MAXIMILIEN
LE FRANCISCAIN

2.
Tu leur as dit : « j’ai la prière
Pour apaiser tous les tourments
Je suis l’otage volontaire
Et j’offre un père à des enfants »

3.
Tu leur as dit : « j’aimais la terre
À en pleurer, je vous l’avoue
Malgré l’horreur et la misère
Que Dieu prenne pitié de vous »

4.
Survivra-t-il aux barbelés ?
Accordez-moi ce bel espoir !
Juste un peu moins de cruauté
Même un bourreau peut s’émouvoir