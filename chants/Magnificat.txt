MAGNIFICAT, MAGNIFICAT,
MAGNIFICAT ANIMA MEA !
MAGNIFICAT, MAGNIFICAT,
ANIMA MEA DOMINUM !

1.
Mon âme exalte le Seigneur,
exulte mon esprit en Dieu, mon Sauveur !
Il s’est penché sur son humble servante ;
désormais, tous les âges me diront bienheureuse.

2.
Le Puissant fit pour moi des merveilles ;
Saint est son nom !
Sa miséricorde s’étend d’âge en âge
Sur ceux qui le craignent.

3.
Déployant la force de son bras,
il disperse les superbes.
il renverse les puissants de leurs trônes,
il élève les humbles.

4.
Il comble de biens les affamés,
renvoie les riches les mains vides.
Il relève Israël son serviteur,
il se souvient de son amour,

5.
de la promesse faite à nos pères,
en faveur d’Abraham et sa descendance à jamais.
Gloire au Père, et au Fils, et au Saint-Esprit,
pour les siècles des siècles. Amen.