1.
Pour l’Église qui t’annonce aux quatre vents de notre monde,
Pour les témoins de ton amour, nous te prions, Seigneur.

R.
TON PEUPLE, SEIGNEUR, SE TOURNE VERS TOI,
TON PEUPLE, SEIGNEUR TE CONFIE SA VIE !
ENTENDS LE CRI DE NOS PRIÈRES,
ENTENDS LE CRI DE NOTRE TERRE !

2.
Pour les hommes qui bâtissent l’amitié entre les peuples,
Pour les semeurs de liberté, nous te prions, Seigneur.

3.
Pour les pauvres et les malades qui espèrent en ta Parole,
Pour les exclus, les sans-abri, nous te prions, Seigneur.

4.
Pour nous tous qui recevrons ton Corps sacré à cette table,
Pour ta famille rassemblée, nous te prions, Seigneur.