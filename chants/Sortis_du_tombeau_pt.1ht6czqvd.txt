Sortis du tombeau, Alléluia,
Nous marchons vers la lumière,
Au cœur de nos vies, Alléluia,
Ta présence nous éclaire.

Il est vraiment ressuscité, alléluia !
Il est vraiment ressuscité, alléluia !

Il est vivant
parmi nous aujourd'hui, alléluia !
Il est vivant
parmi nous aujourd'hui, alléluia !