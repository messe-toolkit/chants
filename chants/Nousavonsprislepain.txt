Nous avons pris le pain,
Le pain de Dieu ;
Ce pain vivant
Transforme en lui
Toutes les forces de la terre.

Corps du Christ,
Tu es notre univers !

Nous avons bu le vin,
Le vin de Dieu,
Ce vin nouveau
En qui la mort
Devient la coupe de la Vie.

Sang du Christ,
Tu sauves notre espoir !

Nous accueillons Jésus,
Le Fils de Dieu ;
Nouvel Adam,
Il passe au feu
Tous ces désirs qui nous soulèvent.

Cœur du Christ,
Que vive ton amour !