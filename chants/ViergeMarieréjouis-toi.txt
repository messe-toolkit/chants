REFRAIN 1
Vierge Marie, réjouis-toi
Car le Seigneur est avec toi.

1.
L'Esprit de Dieu sur toi reposera
Le Très-Haut te prendra sous son ombre.

2.
Tu es bénie entre toutes les femmes
Et Jésus, ton enfant est béni.

3.
Heureuse es-tu, Toi qui crus la parole
Annoncée de la part du Seigneur.
REFRAIN 2
Sainte Marie, mère de Dieu,
Prie le Seigneur pour nous pécheurs

4.
Sois avec nous aux chemins de nos vies
Jusqu'au jour où viendra notre mort ?

5.
Avec ton Fils élevé dans la gloire
Reçois-nous au Royaume des cieux.