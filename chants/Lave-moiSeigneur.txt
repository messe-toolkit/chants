PITIE POUR MOI DANS TA BONTE, SEIGNEUR,
PURIFIE MOI DE TON PECHE.

1.
Lave moi, Seigneur, lave moi tout entier
Purifie-moi, je serai blanc plus que la neige,
Libére-moi, Dieu d'amour et de pitié
Conduis mes pas vers les eaux qui font renaître.