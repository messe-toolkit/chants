LA PAIX, ELLE COMMENCE EN TOI-MÊME,
ELLE COMMENCE EN SILENCE,
AU SECRET DU PARDON.

1.
Pas besoin d’un grand palais pour l’habiter, la paix,
Il suffit de s’ouvrir au silence.
Grand besoin de l’olivier pour la bâtir, la paix,
Besoin de l’olivier.

2.
Pas besoin d’un grand jardin pour la fleurir, la paix,
Il suffit de semer la justice.
Grand besoin d’un champ de blé pour moissonner, la paix,
Besoin d’un champ de blé.

3.
Pas besoin d’un grand discours pour la crier, la paix,
Il suffit d’écouter sa musique.
Grand besoin d’un vol d’oiseau pour l’annoncer, la paix,
Besoin d’un vol d’oiseau.