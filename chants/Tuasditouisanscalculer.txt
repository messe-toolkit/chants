1.
Tu as dit oui sans calculer
Malgré la peur d’être jugée
Mise à l’écart ou lapidée
Risquant ta vie sans hésiter.
Joseph a cru en toi, Marie
Il t’aimait tant, il t’a choisie
Sur ton chemin, il t’a suivie
L’Esprit de Dieu était en lui.

R.
PAR TOI, MARIE,
L’AMOUR S’EST DONNÉ
PAR JÉSUS-CHRIST,
NOUS SOMMES SAUVÉS
MAGNIFICAT, MAGNIFICAT
DIEU SOIT BÉNI
PAR TOI, MARIE.

2.
Voici Jésus, Marie, maman
Tu l’as chéri petit enfant
Même s’il était déconcertant
Tu le suivais fidèlement.
Quand il est mort tu étais là
Tu l’as tenu entre tes bras
Pleurant d’amour pleurant de foi
Qu’adviendrait-il après la croix ?

3.
Tu l’as revu ressuscité
Ton fils béni, ton premier-né
Matin de Pâques, tout a changé
Dans un printemps d’éternité.