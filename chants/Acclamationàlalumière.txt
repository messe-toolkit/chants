Lumière du Christ !
Nous rendons grâce à Dieu !
Colonne lumineuse qui conduisait le peuple au désert.
Lumière du Christ !
Nous rendons grâce à Dieu !
Lumière qui éclaire le monde et l’Église.
Lumière du Christ !
Nous rendons grâce à Dieu !
Astre du matin qui ne connaît pas de couchant.
Lumière du Christ !
Nous rendons grâce à Dieu !
Lumière du Christ !
Nous rendons grâce à Dieu !
REFRAIN (possible)
Ô nuit de grande lumière !
Nuit de louange du Père pour tous les âges !