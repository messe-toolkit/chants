I.
Comme au soir de la Cène,
Cette fête nous mène
Jusqu'au bout de l'amour.

V.1.
Toi, bien-aimé Seigneur,
Tu es chemin de vie
À travers nos déserts.

R.1.
Voici l'heure de grâce :
Tu nous ouvres passage.

V.2.
Toi, bien-aimé Seigneur,
Tu fais jaillir la vie
Pour un peuple assoiffé.

R.2.
Voici l'heure de grâce :
Tu nous donnes ta gloire.

V.3.
Toi, bien-aimé Seigneur,
Tu es le pain de vie
Chaque jour partagé.

R.3.
Voici l'heure de grâce :
Nos mains dressent la table.

V.4.
Toi, bien-aimé Seigneur,
Tu es nouvelle vie
Surgissant de la mort.

R.4.
Voici l'heure de grâce !
Voici l'aube de Pâques !

CONCLUSION
Comme au soir de la Cène,
Cette fête nous mène
Jusqu'au bout de l'amour.