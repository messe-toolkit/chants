R.
PAROLE DANS NOS VIES !
TU N’AS D’ABRI CONTRE L’OUBLI
QUE NOTRE VIE QUI TE FÉCONDE.
TU N’AS DE FEU QUE DANS NOS VOIX,
ET DANS LES VOIX QUI TE RÉPONDENT.

1.
Parole en germe dans nos vies,
Tu prends racine en nos silences,
Pour habiter notre souffrance
Au plus caché de notre nuit.
Parole enfermée dans nos livres,
Qui de nous saura te partager ?
Que ferons-nous pour t’annoncer
À tous ceux que tu ferais revivre ?
Parole dans nos vies !

2.
Parole en germe dans nos vies,
Tu nous guéris, tu nous emportes
Pour purifier nos lèvres mortes
Au souffle brûlant de l’esprit…
Parole étouffée par la lettre,
Qui de nous saura te délivrer ?
Qu’attendons-nous pour témoigner
Que c’est toi qui nous as fait renaître ?
Parole dans nos vies !

3.
Parole en germe dans nos coeurs,
Tu te révèles en transparence
Pour préparer notre naissance
À la récolte du semeur…
Parole creusée par l’absence,
Nous accueillerons au plus secret
Les mots de vie qu’il a laissés,
Nous serons témoins de sa présence.
Parole dans nos coeurs !