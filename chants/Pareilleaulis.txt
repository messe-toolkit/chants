STANCE
Pareille au lis des champs que Dieu revêt de sa beauté,
Claire a fleuri au grand soleil ;
pure simplicité, elle chante et s'émerveille :

R.
Béni sois-tu, Seigneur, de m'avoir créée !

VERSETS

1.
Terre de vérité,
l’Evangile m’a nourrie de sagesse profonde.

2.
Assoiffées de Toi,
mes racines secrètes ont bu l’eau vive.

3.
Dépouillé par le vent,
mon être tout entier vibre de joie.