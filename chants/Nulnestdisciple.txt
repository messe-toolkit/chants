1.
Nul n’est disciple
Hormis le Serviteur.
Nul n’est lumière
Sans l’amour indicible
Qui dans le frère
Découvre le Seigneur.

2.
Nul ne console
À moins d’avoir souffert.
Nul ne témoigne
S’il ne vit la Parole
Où l’homme gagne
Sa joie quand il se perd.

3.
Nul n’est tendresse
À moins d’être blessé.
Nul ne pardonne
S’il n’a vu sa faiblesse
Qui l’abandonne
Aux mains du Transpercé.

4.
Nul ne partage
S’il n’a donné son tout.
Nul ne peut dire
La folie du message,
S’il ne se livre
Lui-même jusqu'au bout.

5.
Nul n’est semence
À moins d’être semeur :
Point de récolte
Sans le temps du silence,
Car tout apôtre
Devient le grain qui meurt.