R.
IL A DIT
“JE M’EN VAIS
MAIS PAS TOUT A FAIT:
JE VOUS LAISSE L’ESPRIT
JE VOUS LAISSE L’ESPRIT
MON ESPRIT”

1.
Mon Esprit soufflera
sur vos jours,
Mon Esprit vous gardera
dans l’Amour !
Si vous le laissez vous emporter.

2.
Mon Esprit brûlera
dans vos yeux,
Mon Esprit s’éveillera
comme un feu!
Si vous le laissez
vous embraser !

3.
Mon Esprit parlera
en couleurs,
Mon Esprit vous conduira
par le coeur !
Si vous le laissez
vous envahir !