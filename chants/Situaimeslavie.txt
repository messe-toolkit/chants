R.
SI TU AIMES LA VIE
SI TU CHERCHES L'AMOUR
PRENDS TA FORCE AU GRAND JOUR
DANS LE FEU DE L'ESPRIT

1.
La liberté te viendra
Elle est encore en semence
A l'intérieur de toi
La liberté commence…
Elle grandira peu à peu
A la clarté de Dieu.

2.
Un nouveau monde naîtra
Qui, dans ton coeur, est en germe
A l'intérieur de toi
Ce monde étend son règne…
Tu le construis peu à peu
A la clarté de Dieu.

3.
Et la tiédeur s'enfuira
De tes pensées, tes paroles
A l'intérieur de toi
La tiédeur t'abandonne…
Elle s'éteindra peu à peu
A la clarté de Dieu.