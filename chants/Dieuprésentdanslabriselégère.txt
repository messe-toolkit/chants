R.
Dieu présent dans la brise légère,
Ouvre-nous à ton Souffle de vie.
Tu maîtrises les flots de la mer,
Tu nous donnes la paix de l’Esprit.

1.
Tu n’es pas un Seigneur de violence,
L’ouragan qui suscite la peur.
Dans la nuit fais souffler la confiance,
Notre foi chantera le Sauveur.

2.
Ta Parole a créé les montagnes,
À ta voix devraient-elles trembler ?
Dans la nuit viens chasser les orages
Qui nous cachent ton ciel étoilé.

3.
Toi qui es Dieu de flamme et de braise,
Brûle-nous de ton feu dévorant !
Dans la nuit fais briller ta lumière,
Que pour toi nous soyons des vivants !

4.
En Jésus nous voyons ta puissance
Quand Simon tremble au vent de la peur.
Dans la nuit sur les eaux il s’avance,
Tu réponds à son cri de frayeur.

5.
Dieu plus fort que le vent des tempêtes,
Tu nous vois sur les flots déchaînés.
Dans la nuit, par Jésus tu nous mènes
Vers la rive d’un monde apaisé.