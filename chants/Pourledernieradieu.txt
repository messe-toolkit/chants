R.
PRENDS AVEC TOI, SEIGNEUR,
CELUI QUE NOUS AIMONS.

1.
Quand la nuit de la mort le retire à nos yeux,
Que se lève pour lui ton soleil sans déclin.

2.
Il n’est plus parmi nous, qu’il soit auprès de toi !
Nous savons ta tendresse, tu accueilles et pardonnes.

3.
N’est-il pas ton enfant depuis ce premier jour
Où les eaux du baptême lui ont donné ta vie ?

4.
Par l’amour de ton Fils, il t’appartient déjà :
Qu’il vive en ta présence et partage ta gloire.