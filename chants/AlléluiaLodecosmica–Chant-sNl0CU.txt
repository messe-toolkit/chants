Alleluia, alleluia!
Alleluia, alleluia!
Alle_luia, alleluia!
Alleluia, alleluia! [ x 2 ]

Lodino il Signor i cieli,             
lodino il Signor i mari,              
gli angeli, i cieli dei cieli:        
il Suo nome è grande e sublime.       
Sole, luna e stelle ardenti,          
Neve, pioggia, nebbia, e fuoco        
lodino il Suo nome in eterno!         
Sia lode al Signor!                   
Sia lode al Suo nome                  
Sia lode al Signor
                    
Lodino il Signor le terre,            
lodino il Signor i monti,             
il vento della tempesta               
che obbedisce alla Sua voce,          
giudici, sovrani tutti,               
giovani, fanciulle, vecchi            
lodino il Suo nome in eterno!         
Sia lode al Signor!                   
Sia lode al Suo nome!             
Sia lode al  Signor!                 

Que les cieux louent le Seigneur,
que les mers louent le Seigneur,
les anges, les cieux des cieux:
Son nom est grand et sublime.
Que le soleil, la lune et les étoiles ardentes,
la neige, la pluie, les nuages et le feu louent
son nom éternellement.
Que soit loué le Seigneur!
Que soit loué son nom!
Que soit loué le Seigneur!

Que les terres louent le Seigneur.
Que  louent le Seigneur les montagnes,
le vent de la tempête
qui obéit à Sa voix.   
Que les juges , tous les souverains,
jeunes, enfants, vieux
louent son nom dans l’éternité!
Que soit loué le  Seigneur!
Que soit loué son nom!
Que soit loué le Seigneur!