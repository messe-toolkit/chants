ALORS TA VOIX DEVIENT PAROLE
DANS NOS DÉSERTS ET DANS NOS CIEUX
NOUS LA PORTONS COMME ELLE NOUS PORTE
POUR ANNONCER LES TEMPS DE DIEU.

1.
Toujours jaillie des vrais silences,
Matin des eaux, nuit des bergers,
Quand la vie, aux souffles d’enfance,
Est ouverte à l’éternité.

2.
Si les bénis de ton Alliance
Siècle après siècle ravivée
De murmures en cris d’espérance
Tracent les chemins détournés.

3.
Quand l’aujourd’hui de l’Évangile
Convie la houle des priants
À soutenir ce corps fragile
D’une Église en commencement.

4.
Si, à la croisée des rencontres
Et des attentes en devenir
Nous savons dire ta présence
Et témoigner de ton désir.