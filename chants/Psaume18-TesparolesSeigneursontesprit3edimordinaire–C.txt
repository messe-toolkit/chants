TES PAROLES, SEIGNEUR, SONT ESPRIT
ET ELLES SONT VIE.

1.
La loi du Seigneur est parfaite,
qui redonne vie ;
la charte du Seigneur est sûre,
qui rend sages les simples.

2.
Les préceptes du Seigneur sont droits,
ils réjouissent le coeur ;
le commandement du Seigneur est limpide,
il clarifie le regard.

3.
La crainte qu’il inspire est pure,
elle est là pour toujours ;
les décisions du Seigneur sont justes
et vraiment équitables.

4.
Accueille les paroles de ma bouche,
le murmure de mon coeur ;
qu’ils parviennent devant toi,
Seigneur, mon rocher, mon défenseur !