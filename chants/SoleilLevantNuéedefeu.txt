Nuée de feu sur ceux qui marchent dans la nuit,
Tu es venu pour montrer le chemin vers Dieu,
Et ton calvaire ouvrit le ciel.
Ô viens Seigneur Jésus !
Présence de ton Père,
Que nous chantions pour ton retour.

Béni soit au nom du Seigneur
Celui qui vient nourrir son peuple.

Ô Fils de Dieu sur qui repose l’Esprit Saint,
Tu es venu comme un feu qui consume tout,
Et l’univers s’embrase en toi.
Ô viens Seigneur Jésus !
Demeure de la Gloire,
Que nous chantions pour ton retour.