DONNE NOUS NOTRE PAIN DE CE JOUR.
DONNE-NOUS TON AMOUR INFINI
VIENS REMPLIR NOTRE VIE DE TA VIE
DONNE-NOUS SEIGNEUR, TON PAIN D ’AMOUR.

1.
Donne-nous de ce pain quotidien
Il devient le soleil d’un festin
Déposé tendrement dans nos mains
Il fera notre paix en chemin

2.
Donne-nous de ce pain du matin
Il devient un partage sans fin
Déposé humblement dans nos mains
Il dira ton visage en chemin

3.
Donne-nous aujourd’hui de ce pain
Il viendra soulever les chagrins
Déposé simplement dans nos mains
Il sera notre force en chemin