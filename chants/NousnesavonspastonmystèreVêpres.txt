1.
Nous ne savons pas ton mystère,
Amour infini ;
Mais tu as un coeur,
Toi qui cherches le fils perdu,
Et tu tiens contre toi
Cet enfant difficile
Qu'est le monde des humains.

2.
Nous ne voyons pas ton visage,
Amour infini ;
Mais tu as des yeux,
Car tu pleures dans l'opprimé,
Et tu poses sur nous
Ce regard de lumière
Qui révèle ton pardon.

3.
Nous ne voyons pas ton ouvrage,
Amour infini ;
Mais tu as des mains
Qui allègent notre labeur,
Et tu peines avec nous
Pour tracer sur la terre
Un chemin vers ton repos.

4.
Nous ne savons pas ton langage,
Amour infini ;
Mais tu es le cri
Que nos frères lancent vers nous,
Et l'appel du pécheur
S'élevant de l'abîme
Vers le Dieu de liberté.