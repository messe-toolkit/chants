R.
JE NE SUIS QU'UNE PIERRE
QUI ROULE, ROULE
DANS LE TORRENT
ET
JUSQU'À LA RIVIERE
JE DEBOULE
TOUT EN ME COGNANT
A D'AUTRES PIERRES
QUI ROULENT, ROULENT
JUSQU'À L'OCEAN

1.
Je suis né au printemps
Je suis né d'un rocher
d'un coup de gel, d'un coup de vent
qui m'a décroché

2.
Je roule sans savoir
et sans me reposer
j'étais tranchant comme un rasoir
je me suis émoussé

3.
En sable transformé
je redonne la vie
Tout doucement mais bien usé
J'approche du pays
où je pourrai dormir