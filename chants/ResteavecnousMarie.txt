RESTE AVEC NOUS MARIE,
PRENDS-NOUS CONTRE TON COEUR
AVEC NOS VIES QUI PEINENT,
RESTE AVEC NOUS MARIE,
RECUEILLE NOS DÉSIRS,
ILS SONT POSÉS MARIE,
DANS TON COEUR DE MÈRE.

1.
Regarde notre corps,
Il traîne tout son poids
Il a baissé les bras
Et reste seul sans voix,
Soulage-le Marie
Et prends-nous dans tes bras
Qu’aujourd’hui près de toi
S’éclairent enfin nos pas.

2.
Regarde notre coeur,
Le voici devant toi
Il porte tant de peurs,
Son souffle en est si froid,
Soulage-le Marie
Et prends-le contre toi
Qu’aujourd’hui près de toi
Dieu éclaire nos pas.

3.
Regarde-nous Marie
Le jour veut s’en aller
Le ciel est tout chargé
La nuit s’est installée,
Réveille en nous la Vie
Allume notre foi
Qu’aujourd’hui soit un oui
Qui soulève nos pas.