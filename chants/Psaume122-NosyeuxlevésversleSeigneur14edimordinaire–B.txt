NOS YEUX, LEVÉS VERS LE SEIGNEUR,
ATTENDENT SA PITIÉ.

1.
Vers toi j’ai les yeux levés,
vers toi qui es au ciel,
comme les yeux de l’esclave
vers la main de son maître.

2.
Comme les yeux de la servante
vers la main de sa maîtresse,
nos yeux, levés vers le Seigneur notre Dieu,
attendent sa pitié.

3.
Pitié pour nous, Seigneur, pitié pour nous :
notre âme est rassasiée de mépris.
C’en est trop, nous sommes rassasiés du rire des satisfaits,
du mépris des orgueilleux !