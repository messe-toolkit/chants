1.
Dans les moments de doutes,
Les moments de déroute,
Je me cherche, je me replie sur moi
M’arrêter, me poser,
Prendre un temps pour prier,
Passer seul un moment avec toi

R.
CAR QUAND PLUS RIEN NE VA
QUE JE BAISSE LES BRAS
JE ME TOURNE VERS TOI
TA PAROLE ME GUIDE
JE ME SENS MOINS FRAGILE
SEIGNEUR, TU ES LÀ

2.
Dépité, fatigué,
D’avancer sans gagner,
J’ai envie de tout laisser tomber
M’arrêter, me poser,
Marcher à tes côtés,
Passer seul un moment avec toi

3.
Une amitié brisée
Pourrait recommencer,
Mais je n’ose pas faire le premier pas
M’arrêter, me poser,
Apprendre à pardonner,
Passer seul un moment avec toi

CODA:
Dieu ma force, mon appui,
Moi qui suis si petit,
Je place ma confiance en toi
Console-moi ! Guide-moi !
Et mon coeur se réjouira.