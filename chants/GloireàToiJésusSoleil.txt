R.
Gloire à toi, Jésus, soleil de justice !
Brille dans nos coeurs, lumière d´en haut !

1.
Quand tu es né à Bethléem,
Tout le ciel s´est illuminé,
Et les bergers ensommeillés
S´éveillèrent à sa clarté.

2.
En Orient, trois rois savants,
Observaient un astre éclatant ;
L´ayant suivi, il les conduit,
Vers ta crèche, petit enfant !

3.
Lorsque Joseph avec Marie
Offrent au temple le nouveau-né,
Saint Syméon le prend et dit :
"Oui, mes yeux ont vu le Messie !"

4.
Quand tu priais dans le Jourdain
Où saint Jean t´avait baptisé,
Du ciel ouvert, vint une voix :
"Oui, c´est toi mon Fils bien-aimé !"

5.
Sur la montagne où tu montais,
Tu parais, baigné de clarté,
En vêtements resplendissants,
Le visage transfiguré.

6.
J´entends ta voix, ô mon berger :
"Si tu m´aimes, viens après moi" ;
"Je suis la lampe et le chemin" ;
"Qui me suit ne tombera pas".

7.
Quittant la nuit de son tombeau,
Mon Seigneur est ressuscité :
"Tu étais mort ; tu es vivant !"
Roi de gloire, louange à toi !

8.
Quand tu t´élèves dans les cieux,
La nuée te cache à nos yeux ;
Mais ton Esprit nous garde heureux :
Joie, lumière, bouquet de feu !