R.
Vienne ton règne, Dieu, notre Père !
Vienne ton règne, sur notre terre !
Vienne ton règne, au coeur de nos frères !

1.
Pour que soient consolés
Ceux qui ont perdu tout espoir
Et que soient éclairés
Ceux qui marchent dans le noir.

2.
Pour que soient accueillis
Ceux qui n’ont plus rien à donner
Et que soient affranchis
Ceux qu’on garde prisonniers.

3.
Pour que soient revêtus
Ceux qui tremblent sur les trottoirs
Et que soient défendus
Ceux qui n’ont pas de pouvoir.

4.
Pour que soient rassemblés
Ceux qui se réclament de toi
Et que soient oubliées
Tant de luttes pour la foi.

5.
Pour que soit retrouvée
La beauté première du ciel
Et que soient purifiés
Les torrents originels.