1.
O Jésus, ma joie parfaite,
Tu m’appelles à vivre en toi.
Joie divine, en moi tu restes,
Dans la nuit soutiens ma foi.
Fils de Dieu, vainqueur du monde,
Ta clarté dissipe l’ombre ;
Fais briller des jours d’espoir,
Que ta flamme brûle en moi !

2.
Quel bonheur quand tu m’invites,
Je découvre ton Festin !
Pain du ciel qui fais revivre,
Tu sauras creuser ma faim.
Nourris-moi de ta parole,
Que ton Souffle soit ma force !
Toi, Jésus, le vrai Chemin,
Montre-moi le Dieu très saint !

3.
Christ en nous, Reflet du Père,
Gloire à toi, Ressuscité !
Sois lumière à qui te cherche,
O soleil d’humanité.
Premier-Né, Sauveur des hommes,
Ouvre-nous à ton Royaume !
Par l’Esprit de vérité
Fais lever les temps nouveaux !