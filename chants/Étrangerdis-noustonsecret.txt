R.
Étranger, étranger ! dis-nous quel est ton secret.

1.
Parle-nous de l'ailleurs qui se cache dans tes yeux –
cet ailleurs que ton regard nous a dévoilé.

2.
Parle-nous des enfants qui dessinent l'avenir –
ces enfants que ton royaume a émerveillés.

3.
Parle-nous de l'espoir qui nous garde au long des nuits –
cet espoir que ton matin a ensoleillé.

4.
Parle-nous de la paix qui attend au creux des mains –
cette paix que ton pardon a renouvelée.

5.
Parle-nous de l'amour qui n'est vrai qu'au sang versé –
cet amour que ton passage a transfiguré.

6.
Parle-nous de la vie qui respire dans la mort –
cette vie que ta passion a ressuscitée.