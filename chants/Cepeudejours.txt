1.
Ce peu de jours
Entre nos mains
Trouvera-t-il asile
En ta mémoire aimante ?
Dieu qui voit nos errances,
N’oublie pas ta patience.

2.
Demain sera
Un autre jour.
Cette heure si fragile
Quel veilleur l’oriente ?
Dieu de toutes existences,
Tiens-nous dans la confiance.

3.
Nos jours noués
Aux jours suivants,
Comment prendre racine
En terre d’humble attente ?
Dieu par qui tout commence,
Aide aussi la croissance.