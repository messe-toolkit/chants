1.
Seigneur Jésus, envoyé par le Père
pour sauver notre terre,
prends pitié de nous,
prends pitié de nous.

2.
O Christ, venu dans le monde
pour défendre les pauvres,
prends pitié de nous,
prends pitié de nous.

3.
Seigneur, élevé dans la gloire du Père
pour tout renouveler par ton Esprit,
prends pitié de nous,
prends pitié de nous.