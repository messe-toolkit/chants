1.
Vienne la lumière dissiper les ténèbres
Qui menacent l’Église
Quand Saül va son chemin,
Vers Damas, sous le soleil.
La Loi est sa justice,
Son orgueil et son combat,
Glaive du Dieu de gloire.

2.
Vienne dans son âme tout l’éclat de la grâce
Du Seigneur de l’Église :
Jésus vit dans les siens.
Ô mystère éblouissant !
Celui qu’il persécute,
C’est Jésus ressuscité,
Tête du Corps de gloire.

3.
Dieu qui se révèle reconduit au silence
L’ennemi de l’Église :
La nuit couvre ses yeux.
L’Esprit parle à son esprit.
Dans l’eau de son baptême,
Il est mort avec le Christ
Pour une vie nouvelle.

4.
Fort de l’Évangile, il en dit les merveilles
Aux confins de l’Église ;
Et Paul va son chemin
Vers le proche et le lointain.
L’Esprit ôte le voile
Quand l’aveugle est fait témoin :
Gloire du Dieu de grâce !