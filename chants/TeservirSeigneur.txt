1.
Te servir, Seigneur, par nos hymnes de louange, Célébrer ta Vie dans un monde à libérer,
Suivre les chemins où le Maître nous devance, Lui qui donne sens à nos cris d’humanité !

2.
Par nos mots de chair et nos voix où tu respires C’est ton Souffle saint qui s’exprime en créateur.
Nous sentons vibrer l’univers que tu fais vivre
Et ton chant nouveau se répand dans tous les cœurs.

3.
Par ton Bien-Aimé tu nous dis ton nom de Père,
Tu n’es pas un Dieu devant qui tout homme a peur.
Tu nous mets debout en enfants de la lumière
Pour te rendre grâce et chanter l’Agneau vainqueur.

4.
Gloire à toi qui viens révéler le chant de fête Que nous redirons à la table du Festin !
C’est le vin de joie dont toi seul connais l’ivresse Et qui comblera toute soif et toute faim.