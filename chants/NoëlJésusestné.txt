R.
Noël, Noël Jésus est né, c’est la fête

1.
Dans le ciel une étoile a brillé
Les bergers se sont mis à chanter.

2.
Dans l’étable au milieu de la nuit
Ils ont vu l’enfant, pauvre et petit.

3.
Un enfant nous est né aujourd’hui
Sa maman près de lui, c’est Marie.

4.
La lumière est venue dans nos cœurs
Dans la joie nous chantons le Seigneur.