1.
Un homme au cœur de feu
Qui est venu du Père
Et qui retourne à Lui,
Jésus, le Premier-né,
Un homme au cœur de feu,
Nous invite à le suivre en son retournement,
Jusqu’à renaître au jour irradiant de Pâque.
Jésus, le Premier-né, nous invite à le suivre.

R.
Pour la gloire de Dieu et sa haute louange,
Pour la gloire de Dieu et le salut du monde !

2.
Un homme sous l’Esprit,
À l’œuvre au sein du monde en mal d’enfantement,
Jésus, Maître et Seigneur
Un homme sous l’Esprit,
Nous invite à le suivre au rang des serviteurs,
À servir aux chantiers où il poursuit sa Pâque.
Jésus, Maître et Seigneur, nous invite à le suivre.

3.
Un homme épris de Dieu,
Le Fils obéissant jusqu’à mourir en croix,
Jésus le Bien-aimé
Un homme épris de Dieu,
Nous invite à le suivre en son abaissement,
À marcher au chemin orienté vers Pâque.
Jésus, le Bien-aimé, nous invite à le suivre.

4.
Un homme au cœur de chair
Qui veut réconcilier la terre avec le ciel,
Jésus, Verbe de Vie,
Un homme au cœur de chair,
Nous invite au bonheur que donne son amour :
La joie qui vient de lui vient témoigner de Pâque.
Jésus, Verbe de Vie, nous invite au bonheur.