1.
Justice de Dieu, courbe-toi vers nos vies.
Pitié de Dieu, fais justice à nos coeurs.
Sagesse de Dieu, redresse nos routes.
Éternité de Dieu, dans nos mains qui peinent,
Pétris ce monde, pour une aube nouvelle !

2.
Sourire de Dieu, viens réchauffer nos peurs.
Pardon de Dieu, fais renaître nos coeurs.
Tendresse de Dieu, refais nos visages.
Chaleur du coeur de Dieu, dans nos mains qui s’ouvrent,
Nourris ce monde d’une sève nouvelle !

3.
Souffrance de Dieu, viens consoler nos pleurs.
Passion de Dieu, violence d’amour.
Présence de Dieu, réveille nos forces.
Don de la vie de Dieu, dans nos mains qui s’aiment,
Conduis ce monde d’une main fraternelle !