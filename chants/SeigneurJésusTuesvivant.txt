R.
SEIGNEUR JESUS,
TU ES VIVANT !
EN TOI LA JOIE ETERNELLE !

1.
Tu es vivant, Seigneur, Alléluia,
Aujourd'hui comme hier demain et toujours,
Alléluia !

2.
Tu es vivant, ô Christ, Alléluia,
Toujours auprès de Dieu, toujours parmi-nous,
Alléluia !

3.
Béni sois-tu, Seigneur, Alléluia,
Par nos travaux, nos joies, le poids de nos vies,
Alléluia !

4.
Parole du Seigneur, Alléluia,
En toi, l'Amour de Dieu nous est révélé,
Alléluia !

5.
Nous te voyons déjà, Alléluia,
Tout l'univers devient visage du Christ,
Alléluia !

6.
Nous te verrons un jour, Alléluia,
Tu reviendras chez nous, toujours notre joie,
Alléluia !

7.
Louange à toi, ô Christ, Alléluia,
Louange au Dieu vivant, louange à l'Esprit,
Alléluia !