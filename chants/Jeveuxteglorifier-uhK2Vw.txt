R.
Je veux te glorifier, Dieu de tendresse et d'amour,
Tu as agi en moi, tu m'as transformé,
Tu as été fidèle, par ta voix tu m'as conduit.
Que mon cœur soit ouvert à ta volonté.

1.
Donne-moi de saisir
Ta lumière dans ma vie,
Viens éclairer ma nuit, ta présence me suffit.
Donne-moi de garder ta loi, tes commandements.
Viens répondre à mon cri, toi le Verbe de Vie.

2.
Que mon âme te loue :
Mon Dieu, tu m'as relevé,
Et sur toi je m'appuie, car tu es mon Bien-aimé.
Je veux chanter sans fin ta fidélité mon Roi,
Et entendre ici-bas la beauté de ta voix.

3.
Donne-moi de t'aimer,
De me laisser façonner,
Ta présence est pour moi un débordement de joie.
Je veux vivre de toi, contempler ton cœur blessé,
Reposer près de toi pour la vie éternelle.