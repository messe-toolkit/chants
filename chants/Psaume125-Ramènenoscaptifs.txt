R.
Quelles merveilles fait pour eux le Seigneur !
Quelles merveilles le Seigneur fit pour nous.

Quand le Seigneur ramena les captifs à Sion,
nous étions comme en rêve !
Alors notre bouche était pleine de rires,
nous poussions des cris de joie ;

Ramène, Seigneur, nos captifs,
comme les torrents au désert.
Qui sème dans les larmes
moissonne dans la joie :

Il s'en va, il s'en va en pleurant,
il jette la semence ;
il s'en vient, il s'en vient dans la joie,
il rapporte les gerbes.