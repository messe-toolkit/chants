1.
Il a passé la mort, le Vainqueur qui t'appelle et te dit
« Viens dehors, créature de Dieu, créature nouvelle, viens à la pure lumière ! »

R.
Il est le Messie, le Fils de Dieu,
Le maître et le Seigneur qui ouvre les tombeaux.
Il a vaincu le monde ;
Il est la résurrection et la vie !

2.
Il a passé la nuit, le Veilleur qui l'éveille et te dit
« Aujourd'hui, le Soleil s'est levé ! Le Seigneur fait merveille,
Sors de la froide ténèbre ! »

3.
Il a passé la mer, le Lutteur qui délivre et qui dit à la chair
« C'est l'Esprit qui fait corps ! C'est l'Esprit qui fait vivre !
Laisse mon souffle te prendre ! »