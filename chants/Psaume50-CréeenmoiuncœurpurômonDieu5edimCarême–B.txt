CRÉE EN MOI UN COEUR PUR, Ô MON DIEU.

1.
Pitié pour moi, mon Dieu, dans ton amour,
selon ta grande miséricorde, efface mon péché.
Lave-moi tout entier de ma faute,
purifie-moi de mon offense.

2.
Crée en moi un coeur pur, ô mon Dieu,
renouvelle et raffermis au fond de moi mon esprit.
Ne me chasse pas loin de ta face,
ne me reprends pas ton esprit saint.

3.
Rends-moi la joie d’être sauvé ;
que l’esprit généreux me soutienne.
Aux pécheurs, j’enseignerai tes chemins ;
vers toi, reviendront les égarés.