HEUREUX QUI CRAINT LE SEIGNEUR !

1.
Heureux qui craint le Seigneur
et marche selon ses voies !
Tu te nourriras du travail de tes mains :
Heureux es-tu ! À toi, le bonheur !

2.
Ta femme sera dans ta maison
comme une vigne généreuse,
et tes fils, autour de la table,
comme des plants d’olivier.

3.
Voilà comment sera béni
l’homme qui craint le Seigneur.
De Sion, que le Seigneur te bénisse !
Tu verras le bonheur de Jérusalem tous les jours de ta vie.