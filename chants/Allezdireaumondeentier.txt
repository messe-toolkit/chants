R.
Allez dire au monde entier les merveilles de Dieu !

Psaume 116.
Louez le Seigneur, tous les peuples;
fêtez-le, tous les pays!

Son amour envers nous s'est montré le plus fort;
éternelle est la fidélité du Seigneur!