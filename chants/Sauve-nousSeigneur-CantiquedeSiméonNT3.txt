ANTIENNE
Sauve-nous, Seigneur, quand nous veillons,
Garde-nous, Seigneur, quand nous dormons.
Et nous veillerons avec le Christ,
Et nous reposerons en paix.

29 Maintenant, ô Maître souverain,
tu peux laisser ton serviteur s’en aller
en paix, selon ta parole.
30 Car mes yeux ont vu le salut
31 que tu préparais à la face des peuples :
32 lumière qui se révèle aux nations
et donne gloire à ton peuple Israël.
Gloire au Père et au Fils et au Saint-Esprit,
pour les siècles des siècles. Amen.