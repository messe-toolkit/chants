Stance

Le soleil s'est levé :
ne cherchez plus parmi les morts le Fils de l'homme :
il a brisé les verrous de la mort, alleluia !

R.

Christus resurrexit,
Christus resurrexit, Alleluia, alleluia !

Versets

Ps 99.

1.
Acclamez le Seigneur, terre entière,
Servez le Seigneur dans l'allégresse,
Venez à lui avec des chants de joie !

2.
Reconnaissez que le Seigneur est Dieu :
Il nous a faits, et nous sommes à lui,
Nous, son peuple, son troupeau.

3.
Venez dans sa maison lui rendre grâce,
Dans sa demeure chanter ses louanges ;
Rendez-lui grâce et bénissez son nom !

4.
Oui, le Seigneur est bon,
Éternel est son amour,
Sa fidélité demeure d'âge en âge.