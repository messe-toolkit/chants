Refrain :
Saint Jean-Baptiste de la Salle
Nous découvrirons les sentiers,
Afin que la flamme pascale
Vive dans nos coeurs à jamais…

1.
Nous rechercherons nos racines
Pour y découvrir l’avenir,
Nous rechercherons nos racines,
Puisqu’ Éduquer s’écrit SERVIR.

2.
Nous partirons à l’aventure
Car Dieu appelle et Dieu promet,
Nous partirons à l’aventure
Puisqu’ Éduquer reste un PROJET.

3.
Nous discernerons la rencontre
Comme un matin à “Parménie”
Nous discernerons la rencontre
Puisqu’ Éduquer nous vient de LUI.

4.
Nous échangerons le mot : “FRÈRE”
Le monde en sera transformé
Nous échangerons le mot : “FRÈRE”
Puisqu’ Éduquer s’écrit AIMER.

5.
Pour que SA volonté soit “Fête”
Je pose “tout”, je retiens Dieu,
Pour que SA volonté soit FAITE
Puisqu’ Éduquer est AUDACIEUX.