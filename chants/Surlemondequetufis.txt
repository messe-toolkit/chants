SOUFFLE DE VIE, ESPRIT DE DIEU,
VIENS EN NOS COEURS, ESPRIT DE VÉRITÉ.
SOUFFLE DE VIE, ESPRIT DE PAIX,
VIENS EN NOS COEURS, ESPRIT DE SAINTETÉ !

1.
Sur le monde que tu fis, oeuvre de tes mains,
Qu'en toute âme, pour ta gloire,
Chante ton Esprit.
Pour que germent tous les fruits, comme le levain,
Qu'en tout âge, sur la terre,
Vienne l'Esprit !

2.
Sur l'enfant qui vient à toi, au jour du baptême,
Qu'en son âme, pour ta gloire,
Souffle ton Esprit.
Pour que germent tous les dons, comme la récolte,
Qu'en sa vie, sur sa route,
Vienne l'Esprit !

3.
Sur le pain que tu nous donnes, signe de ta vie,
Qu'en toute âme, pour ta gloire,
Vienne ton Esprit.
Pour que germe le Royaume, comme la promesse,
Qu'en nos vies, sur le monde,
Vienne l'Esprit !

4.
Sur l'Église que tu fis, au temps du Salut,
Qu'en toute âme, pour ta gloire,
Brille ton Esprit.
Pour que germent dans les coeurs, la Bonne Nouvelle,
Qu'en nos vies, sur les hommes,
Vienne l'Esprit !