1.
Ô croix dressée sur le monde,
Ô croix de Jésus-Christ ! (bis)
Fleuve dont l'eau féconde
Du cœur ouvert a jailli,
Par toi la vie surabonde,
Ô croix de Jésus-Christ !

2.
Ô croix, sublime folie,
Ô croix de Jésus-Christ ! (bis)
Dieu rend par toi la vie
Et nous rachète à grand prix :
L'amour de Dieu est folie,
Ô croix de Jésus-Christ !

3.
Ô croix, sagesse suprême,
Ô croix de Jésus-Christ ! (bis)
Le Fils de Dieu lui-même
Jusqu'à la mort obéit ;
Ton dénuement est extrême,
Ô croix de Jésus-Christ !

4.
Ô croix, victoire éclatante,
Ô croix de Jésus-Christ ! (bis)
Tu jugeras le monde
Au jour que Dieu s'est choisi.
Croix à jamais triomphante,
Ô croix de Jésus-Christ !