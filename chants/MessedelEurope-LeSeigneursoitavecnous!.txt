R.
SEIGNEUR, NOTRE BERGER,
TON ESPRIT NOUS RASSEMBLE ;
SEIGNEUR, DIEU D'UNITE,
APPRENDS-NOUS A VIVRE ENSEMBLE,
APPRENDS-NOUS A VIVRE ENSEMBLE !

1.
Le Seigneur soit avec nous,
sa maison nous est ouverte !
Le Seigneur soit avec nous
comme un feu pour notre fête !

2.
Le Seigneur soit avec nous
pour la paix d'une rencontre !
Le Seigneur soit avec nous
comme un fleuve qui féconde !

3.
Le Seigneur soit avec nous,
lui qui sait notre faiblesse !
Le Seigneur soit avec nous
comme un souffle de tendresse !