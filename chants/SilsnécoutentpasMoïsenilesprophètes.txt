Antienne - stance
S'ils n'écoutent pas Moïse ni les prophètes, croiront-ils en celui qui se lève d'entre les morts ?

R.
Sit nomen Domini benedictum.
Nunc et in saecula.

Versets

Ps 71

1.
Dieu, donne au roi tes pouvoirs,
à ce fils de roi ta justice.
Qu'il gouverne ton peuple avec justice,
qu'il fasse droit aux malheureux !

2.
Montagnes, portez au peuple la paix,
collines, portez-lui la justice !
Qu'il fasse droit aux malheureux de son peuple,
qu'il sauve les pauvres gens, qu'il écrase l'oppresseur !

3.
Il délivrera le pauvre qui appelle
et le malheureux sans recours.
Il aura souci du faible et du pauvre,
du pauvre dont il sauve la vie.

4.
Il les rachète à l'oppression, à la violence ;
leur sang est d'un grand prix à ses yeux.
En lui, que soient bénies toutes les familles de la terre ;
que tous les pays le disent bienheureux !

Ou bien

Ps 33

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m'entendent et soient en fête !

2.
Magnifiez avec moi le Seigneur
exaltons tous ensemble son nom.
Je cherche le Seigneur, il me répond :
de toutes mes frayeurs, il me délivre.

3.
Qui regarde vers lui resplendira,
sans ombre ni trouble au visage.
Un pauvre crie ; le Seigneur entend :
il le sauve de toutes ses angoisses.

4.
L'ange du Seigneur campe à l'entour
pour libérer ceux qui le craignent.
Goûtez et voyez : le Seigneur est bon !
Heureux qui trouve en lui son refuge !