1.
Ô Père, me voici,
Le temps d'étirer mes deux bras
Dans la lumière qui est là
Garde-moi tout au long du jour,
Très attentif à ton amour.

2.
Ô Père, me voici,
Dans chaque instant qui est présent,
Dans ces journées si bien remplies
Où tu nous donnes tant de vie,
Nous tes enfants, nous tes amis.

3.
Ô Père, me voici,
En me couchant je pense à Toi,
Je viens pour te parler de moi
Et te donner cet aujourd'hui
Qui va finir avec la nuit.

4.
Ô Père, me voici,
Jésus l'a dit bien avant moi,
Grâce à lui je peux moi aussi
Te dire ; « Père, je suis là ;
Je suis heureux d'être avec Toi. »