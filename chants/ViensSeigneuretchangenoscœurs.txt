VIENS SEIGNEUR ET CHANGE NOS CŒURS

1.
Des hommes pleurent,
Des hommes crient leur souffrance,
Entendrons-nous le cri de Dieu ?
La Terre est desséchée…

VIENS, SEIGNEUR ET CHANGE NOS CŒURS,
QUE TON AMOUR NOUS IMPRÈGNE COMME LA ROSÉE !

2.
L'homme s'agite,
Inquiet de son avenir,
Connaîtra-t-il la paix de Dieu ?
La Terre est desséchée…

3.
L'homme est séduit
Par tant de fausses puissances,
Qui lui rendra sa liberté ?
La Terre est desséchée…

4.
Le monde espère,
Sans bien savoir où il va,
Recevra-t-il un Dieu enfant ?
La Terre est desséchée…