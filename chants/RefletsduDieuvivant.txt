1.
Reflets du Dieu vivant,
Saints et saintes aux noms bénis,
Humbles vies devenues flammes
Par l’Esprit des coeurs brûlants,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

R.
Louange au Dieu très saint
Qui vous prend dans sa lumière !
Bienheureux qui vous suivra
Sur la Terre de sa joie !

2.
Pécheurs et pèlerins,
Saints et saintes aux noms bénis,
Vous avez connu la grâce
De trouver le vrai Chemin,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

3.
Chercheurs de vérité,
Saints et saintes aux noms bénis,
Accourus près des fontaines
Où l’eau vive est à puiser,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

4.
Témoins du Jour levant,
Saints et saintes aux noms bénis,
Baptisés au feu de Pâques
Dans un peuple de croyants,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

5.
Apôtres du Sauveur,
Saints et saintes aux noms bénis,
Envoyés parmi vos frères
Dire un Dieu qui parle au coeur,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

6.
Prophètes pour nos temps,
Saints et saintes aux noms bénis,
Éveilleurs de voies nouvelles
Au prix fort des combattants,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

7.
Semeurs de liberté,
Saints et saintes aux noms bénis,
Arrachés aux lourdes chaînes
Qui nous tiennent prisonniers,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.

8.
Ferments d’humanité,
Saints et saintes aux noms bénis,
Habités par la tendresse
Dans un monde à rénover,
Saints d’hier et d’aujourd’hui,
Le Seigneur vous a conduits.