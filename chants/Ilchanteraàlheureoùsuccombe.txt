1.
Il chantera à l'heure où succombe son corps de chair offert au supplice.
Il dansera au bord de la tombe la seule gloire du divin Fils.

2.
Devant le feu, les fers et les armes, devant la mort, il prie et résiste,
Et dans sa foi brillant en ses larmes il participe au combat du Christ.

3.
Son sang féconde un monde nouveau où fleurira la Croix du Sauveur,
Germe sacré pour que vienne l'heure de l'unité en un corps plus beau.

4.
En son martyre se poursuit sans fin la rédemption de l'humanité
Et sur son front Dieu pose la main pour qu'il soit saint pour l'éternité.

5.
Que notre voix honore son nom qui porte en lui la gloire du Père,
Que l'Esprit Saint qui nous fit ce don avec le Christ soit notre prière.