R.
A TON APPEL, DIEU PARMI NOUS,
A TON APPEL, NOUS SOMMES DEBOUT,
PEUPLE DE CROYANTS
TEMOINS DE JESUS CHRIST,
PEUPLE DE CROYANTS
BATISSEURS DE L’AVENIR.

Comment bâtir sur le rocher de la confiance ?
Tant de soleils à l’horizon se sont voilés !
Sous les nuages est-il encore un ciel d’été ?
SOURCE D’ ESPERANCE,
Donne-nous l’audace d’avancer.

2.
Dans notre voix,
entends le cri de tout un peuple !
Les travailleurs vont-ils connaître le désert ?
Révèle-nous comment changer cet univers.
DIEU TOUJOURS A L’OEUVRE,
Fais de nous des êtres au coeur ouvert.

3.
Semeurs de vie, nous le serons à ton image,
Comme Jésus auprès des foules d'affamés.
Par nos labeurs le pain sera multiplié.
SOURCE DU PARTAGE,
Sur la terre envoie tes ouvriers !

4.
Toi notre Dieu, libérateur et solidaire,
Tu veux bâtir un monde neuf
pour tes enfants :
Maison de paix où chaque pierre est un vivant.
DIEU BRISEUR DE GUERRES,
Nous serons à l'oeuvre dans ton champ.