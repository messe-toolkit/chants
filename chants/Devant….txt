1.
Partir c’est mourir un peu,
Dit un nomade en partance
Chaque jour est un adieu,
Que résiste notre alliance.
Partir c’est mourir un peu :
La fleur, le fruit, la semence.
Ces larmes au fond de mes yeux,
C’est encore notre alliance.

DEVANT, DEVANT…
SURTOUT N’ÉTEINT PAS CE FEU
DE NOTRE ESPÉRANCE
DEVANT, DEVANT…
QUE BRÛLE TOUJOURS CE FEU,
L’AMOUR NOUS DEVANCE!

2.
Partir c’est mourir un peu,
Chaque matin : « Je commence. »
L’horloge égraine ses voeux
J’en ai fait ma romance
Partir c’est mourir un peu
Présence au coeur de l’Absence.
Le temps est aux amoureux
C’est encore ma romance.

3.
Partir c’est mourir un peu
Chaque instant une naissance
Si le temps reste pluvieux
Je n’oublie pas ma chance
Partir c’est mourir un peu
Le lilas est en avance
Le chat attend près du feu.
Je saisirai ma chance.