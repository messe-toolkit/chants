R.
Oser entrer dans la lumière
Se lever, agir et être fier
Rêver avant que la nuit n's'achève,
Rassemblés, on vit nos rêves !

1 Des questions compliquées,
Des avis partagés. Moi au milieu des gens !
La terre est effrayée, les hommes ont gaspillé.
Moi au milieu des gens !
FIER ! De savoir faire des choix, qui déplaisent parfois,
Je refuse de me taire.
FIER ! De donner mes idées, je construis des projets
Un avenir pour la terre.
VIS TES REVES ! VIS TES REVES !

2 On reste mal éveillé, devant notre télé.
On s'évade sur une chaise.
Et quand tout est chiffré, les dollars, les idées.
J'ai pas d'place pour le rêve.
J'REVE ! En couleur, en HD, d'un monde plus coloré,
Et plein de décibels.
J'REVE ! Avec tous mes amis, d'un futur où la vie
Chaque jour est plus belle
VIS TES REVES ! VIS TES REVES !

3 Des ombres qui se pressent,
Des regards qui se baissent. Moi au milieu des gens !
Des hommes comme des machines,
Qui se suivent sur une ligne. Moi au milieu des gens !
J'OSE ! Interpeller les grands et sourire aux passants.
Aussi tendre la main.
J'OSE ! Apprivoiser mes doutes ,
Et puis prendre la route, guidé par mon instinct.
VIS TES REVES ! VIS TES REVES !