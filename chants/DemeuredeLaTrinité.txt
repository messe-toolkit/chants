Stance
Dans le coeur de l’Église,
J’entendis la voix du Bien-Aimé qui me dit :
« Viens Épouse du Christ » !

R.
LE CHRIST A AIMÉ L’EGLISE ET S’EST LIVRÉ POUR ELLE,
IL LA VOULAIT RESPLENDISSANTE
POUR LA LOUANGE DE SA GLOIRE !
DANS CE GRAND MYSTÈRE, NOTRE VOCATION EST D’ÊTRE « MAISON DE DIEU »,
« DEMEURE DE LA TRINITÉ »

Versets

1.
Être Demeure de La Trinité,
c’est être livré comme le Christ s’est livré ;
C’est un coeur à coeur pour toute une vie,
C’est se reposer de tout en Lui,
Pour Lui permettre de se reposer
de tout en notre âme.

2.
Être Demeure de La Trinité,
c’est ne plus savoir qu’aimer ;
Aimer en adorant, aimer en réparant,
Aimer en priant, aimer en demandant,
Aimer en s’oubliant, aimer en tout.

3.
Être Demeure de La Trinité,
c’est fixer son regard sur le Christ ;
avoir les yeux dans les siens,
Le coeur tout pris, tout envahi,
entrer dans toutes ses joies
et partager toutes ses tristesses.

4.
Être Demeure de La Trinité,
c’est enfanter les âmes à la grâce ;
multiplier les adoptés du Père, les rachetés du Christ.
C’est le Père, le Verbe et l’Esprit envahissant l’âme,
La consommant en l’Un par l’amour.