ALLONS VITE À LA CRÈCHE,
CAR UN SAUVEUR NOUS EST NÉ!
DANSONS LA FARANDOLE,
CAR L’ENFANT-DIEU NOUS EST DONNÉ!

1.
Voici venir les pâtres, jouant de leurs flûtiaux,
Offrant de leurs fromages, du lait et des agneaux !
Et puis de la farine, portée par le meunier,
Car Dieu, sur la colline, sera le Pain donné !

2.
Durant la nuit entière, les femmes au tricot,
Ont fait belle layette, pour vêtir le petiot !
Les enfants du village ont tous voulu danser.
Jésus, qui paraît sage, leur tient sa Croix cachée !

3.
Allons vite à la crèche, en famille allons-y !
Dansons la farandole avec tous nos amis !
Tout le monde se presse et bénit cet enfant
Qui est toute Lumière, et qui nous aime tant !