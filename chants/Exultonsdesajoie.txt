Voici les cieux nouveaux et la nouvelle terre :
Amour et Vérité habitent parmi nous !
Voici les cieux nouveaux et la nouvelle terre :
La Justice et la Paix nous ouvrent des chemins
Vers le Seigneur de Pâques !

1.
Dieu se fait une joie
de nous créer par sa Parole,
de nous vouloir à son image,
de nous soumettre l'univers ;
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de nous choisir pour sa louange,
de nous choisir pour son service,
de nous choisir pour son Alliance,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de travailler par son Église,
de la bâtir au cœur du monde,
de voir son œuvre s'accomplir,
Bénissons-le ! Exultons de sa joie !

2.
Dieu se fait une joie
de nous tirer de nos impasses,
de nous guider aux lieux déserts,
de nous conduire en son Royaume,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de nous sauver par un Baptême,
de nous guérir de nos blessures,
de nous combler de son Esprit,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
que nous luttions contre la haine,
que nous vivions de l'Évangile,
que nous servions les droits de l'homme,
Bénissons-le ! Exultons de sa joie !

3.
Dieu se fait une joie
de susciter des êtres libres,
de rendre heureux tous les humains,
de leur apprendre à rendre grâce,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de rassembler tous ses enfants,
de les nourrir d'un même pain,
de les unir en un seul corps,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
que nous soyons le feu, le sel,
que nous voulions être un ferment,
que nous brûlions sur le boisseau,
Bénissons-le ! Exultons de sa joie !

4.
Dieu se fait une joie
de faire à tous miséricorde,
de nous remettre nos péchés,
de nous former un cœur de chair,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de nous tenir en sa mémoire,
de nous garder devant ses yeux,
de contempler en nous son Fils,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de n'embaucher que des pécheurs,
de préférer ceux qui n'ont rien,
de nous aimer jusqu'à la croix,
Bénissons-le ! Exultons de sa joie !

5.
Dieu se fait une joie
de révéler qu'il est l'Amour,
de nous montrer comment on aime,
de nous donner son propre Fils,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de secourir les opprimés,
de consoler tous ceux qui pleurent,
d'associer l'homme à son ouvrage,
Bénissons-le ! Exultons de sa joie !
Dieu se fait une joie
de rassasier ceux qui partagent,
de pardonner à qui pardonne,
de faire grâce à qui fait grâce,
Bénissons-le ! Exultons de sa joie !