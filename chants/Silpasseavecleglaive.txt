S’il passe avec le glaive,
S’il passe avec le feu,
Peut-être pourrons-nous
Tenir devant lui.

Mais s’il vient les mains nues,
S’il vient donner sa vie,
Comment lui refuser la nôtre ?