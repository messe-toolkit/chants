Au plus haut du ciel, la gloire de Dieu !
Sur toute la terre, aux hommes la paix !
Au plus haut du ciel, la gloire de Dieu !
Sur toute la terre, aux hommes la paix !

1.
Jusqu'aux cieux, ta splendeur est chantée
Par la bouche des tout petits !

2.
Qui donc est l'homme pour que tu penses à lui ?
Qui donc est l'homme pour que tu l'aimes ?

3.
Tu l'établis sur les œuvres de tes mains
Et tu as mis toute chose à ses pieds.