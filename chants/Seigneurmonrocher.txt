1.
J’ai fait de toi mon refuge, Seigneur.
Sois le rocher qui m’abrite et qui me sauve.
Tu es ma citadelle, mon espoir est en toi.

2.
Pitié pour moi, Seigneur en ta bonté.
Purifie-moi, je serai plus blanc que neige.
Lave-moi de tout mal, pardonne-moi, mon Dieu.

3.
Vers toi je crie, tout le jour et la nuit.
Rappelle-toi les merveilles de ta grâce.
Tu es notre secours ; mon salut vient de toi.

4.
Toi qui connais chacun de mes tourments,
rends-moi le son de la joie et de la fête.
À l’ombre de tes ailes, cache-moi, ô mon Dieu.

5.
Je suis entré dans l’abîme des eaux,
écoute-moi : n’attends pas pour me répondre.
Mes jours sont dans ta main, mon Dieu ne tarde pas !

6.
D’un grand espoir j’espérais le Seigneur.
Il s’est penché pour entendre ma prière.
Comment rendre à mon Dieu tout le bien qu’il m’a fait ?

7.
Tu as changé mon deuil en une danse, [et tu]
as déposé sur mes lèvres la louange.
Mon salut vient de toi et mon âme est en fête.

8.
J’acclamerai ton amour au matin,
ta sainteté resplendit dans ta demeure.
J’ai choisi d’être là, au parvis de ton Temple.

9.
En ta présence est la joie sans mesure
et tu m’enseignes la route de la vie.
Tu m’éclaires ta Loi, je redis tes promesses.
10.
J’ai annoncé une bonne nouvel [le, Sei-]
gneur tu le sais, je ne retiens pas mes lèvres.
J’ai chanté ton amour à la grande assemblée.
11.
Et tu seras l’allégresse et la joie
du peuple immense de ceux qui te recherchent.
Toujours, ils chanteront tes merveilles, Seigneur.
12.
Dansez pour Dieu et jouez pour son Nom,
acclamez-le car il a fait des merveilles.
Cithare et tambourins, dansez devant sa face.