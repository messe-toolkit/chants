1.
Une femme aura un fils,
Pure flamme en pleine nuit.
De quel nom sera nommé
Ce garçon, ce nouveau-né ?

R.
DIEU DONNÉ, EMMANUEL!
PREMIER-NÉ, ENFANT NOËL!
NOËL!

2.
“Oui” a dit Sainte-Marie,
Disons-lui tous nos mercis.
L’Esprit Saint est sur les deux,
Terre et grain bénis de Dieu.

3.
Conservez, comme Marie,
Ce secret dans votre vie
Et chantez avec Marie
La beauté de Jésus-Christ.