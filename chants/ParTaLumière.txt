R.
Par ta lumière, nous voyons la lumière,
Seigneur Jésus plein de lumière.

1.
Tu es la vraie lumière qui éclaire toute créature,
Éclaire-nous de ta belle lumière,
Ô image parfaite du Père des cieux !

2.
Éloigne-nous de toute voix mauvaise,
Ô pur et Saint qui habites les sphères de la lumière,
Et donne-nous d’accomplir avec un cœur pur
Les œuvres de justice.