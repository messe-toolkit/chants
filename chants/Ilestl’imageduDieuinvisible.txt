IL EST L'IMAGE DU DIEU INVISIBLE
(Colossiens 1,15-17)

IL EST L'IMAGE DU DIEU INVISIBLE,
PREMIER-NÉ DE TOUTE CRÉATURE !
C'EST EN LUI QUE SONT CRÉÉES
TOUTES CHOSES DANS LES CIEUX ET SUR LA TERRE !
TOUT FUT PAR LUI ET POUR LUI !
IL EST AVANT TOUTES CHOSES ET TOUT SUBSISTE EN LUI !

1.
Venez à la table du royaume où se livre le fils bien-aimé,
Il attire à lui tout l'Univers,
Celui qui l'accueillera brûlera du même feu !

2.
Prenez et mangez le pain de Dieu, seul festin qui nous rassasie,
Pain du ciel envoyé par le Père,
Celui qui s'en nourrira vivra pour l'éternité !

3.
Voyez dans vos mains le corps du Christ qui nous donne la gloire du ciel !
En faisant de nous un peuple saint,
La création toute entière sera enfin libérée !

4.
Entrez dans la joie du fils de Dieu ! Recevez son souffle de vie,
Pour célébrer la nouvelle alliance,
Bonheur de la multitude rassemblée en un seul corps !