R.
Le Christ nous a aimés et s'est livré pour nous.

1.
Il a porté lui-même nos fautes sur la croix.
R- Il s'est livré pour nous.

2.
C'est par ses meurtrissures que nous sommes guéris.
R- Il s'est livré pour nous.

3.
Il a souffert pour nous, nous laissant un exemple
Pour que nous suivions ses traces.