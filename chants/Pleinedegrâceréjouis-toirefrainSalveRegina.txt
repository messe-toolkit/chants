SALVE, SALVE, REGINA !

1.
Pleine de grâce, réjouis-toi !
L’Emmanuel a trouvé place
dans ta demeure illuminée.
Par toi la grâce a rayonné
pour le salut de notre race.

2.
Arche d’Alliance, réjouis-toi !
Sur toi repose la présence
du Dieu caché dans la nuée
Par toi, la route est éclairée
dans le désert où l’homme avance.

3.
Vierge fidèle, réjouis-toi !
Dans la ténèbre où Dieu t’appelle,
tu fais briller si haut ta foi
que tu reflètes sur nos croix
la paix du Christ et sa lumière.

4.
Reine des anges, réjouis-toi !
Déjà l’Église en toi contemple
la création transfigurée :
fais-nous la joie de partager
l’exultation de ta louange !

Couplet complémentaire à l'occasion de la messe de Lourdes 2020 sur le thème de l’Immaculée Conception :

5.
« Allez boire à la source et vous y laver ! »
« Je suis l’Immaculée-Conception. »
« Pénitence, pénitence, pénitence ! »
« Priez Dieu pour les pécheurs ! »
Paroles de Notre-Dame.