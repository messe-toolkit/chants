Slam

1.
Plus de cent ans déjà
Se taisait une guerre :
Mondiale était son nom !
Elle était la première
À s’appeler ainsi.
A la fin de la guerre,
Ils avaient déclaré :
« Elle sera la dernière ! »
« Der des Ders », disaient-ils …
Mais une autre viendrait.
Le pardon est fragile
Quand il n’est pas nourri
De la Paix d’Évangile.

Refrain 1
MOI, JE CONNAIS QUELQU’UN
DONT LA PAIX EST SI SÛRE
QUE SA FORCE D’AIMER
PEUT GUÉRIR NOS BLESSURES.

Choeur 1
TOI, TU CONNAIS QUELQU’UN
DONT LA PAIX EST SI SÛRE
QUE SA FORCE D’AIMER
PEUT GUÉRIR NOS BLESSURES.

2.
Son Nom est bien connu :
C’est le Nom de Jésus.
Il nous dit Dieu sauveur.
Ce Nom, vous le savez.
Pas d’autre Nom sur terre
Qui puisse nous sauver !
Alors, Seigneur, je viens,
Je viens crier vers toi,
Je viens cueillir la paix
Sur l’arbre de la croix.

Refrain et choeur 2
TOI, SEIGNEUR DE LA PAIX,
TON AMOUR EST SI SÛR !
ET TA FORCE D’AIMER
PEUT GUÉRIR NOS BLESSURES. (bis)

3.
Je regarde vers Toi.
Nous t’avons transpercé
Sur deux poutres de bois.
Nous t’avons transpercé,
Et nous buvons la paix
À ton côté blessé.
Sur deux poutres de bois
Tu brises toute haine,
Tu détruis les barrières,
Tu brises tous les murs.

Refrain et choeur 2

4.
Sur deux poutres de bois,
Ton amour est si grand
Que tu deviens l’aimant
Qui attire tout homme.
Et c’est toi qui pardonnes !
Car Seigneur, c’est si bon
De semer le pardon.
Tu rebâtis des ponts
Entre Dieu et les hommes,
Entre les hommes entre eux.
Toi, tu es le Chemin…
Qui nous conduit vers Dieu.

Refrain et choeur 2