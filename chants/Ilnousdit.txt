1.
Quand l’instant abîme notre histoire,
Quand le rêve étouffe le présent,
Quand, nous deux, on ne peut plus y croire,
Quand “ toujours ” n’est jamais maintenant.

Refrain :
Il nous dit : “ passez sur l’autre rive ! ”
Le soleil est au bout de la nuit.
Je suis là pour vous quoiqu’il arrive,
Il nous dit : “ je t’aime à l’infini ! ”

2.
Quand le vrai n’est pas très vraisemblable,
Quand le faux n’est pas toujours certain,
Quand l’affront nous fait quitter la table,
Quand la croix modifie le destin.

3.
Quand on est en quête de soi-même,
Quand chez soi, on rentre sans désir,
Quand, soudain, on brûle ses poèmes,
Quand les pleurs empêchent de choisir.