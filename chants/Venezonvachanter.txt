VENEZ, ON VA CHANTER…
VENEZ, ON VA DANSER
VENEZ, ON VA PRIER…
ÊTRE UNIS C’EST ESPÉRER!

1.
Venez tous, apportez vos chansons
Vos rires et vos cris, pour bâtir une fête
Venez tous, ensemble c’est si bon
Jusqu’à la nuit nous chanterons.

2.
Venez tous, peuples battez des mains
Il est temps d’accueillir, de vivre la rencontre
Venez tous, ensemble on va plus loin
Pour danser le nouveau matin.