1.
Le Seigneur nous aime tant
Nous qui sommes ses enfants,
Il nous gardera toujours
Au soleil de son amour.
Il nous gardera toujours
Au soleil de son amour.

2.
Le Seigneur nous a sauvés,
Rien ne pourra nous manquer,
Et nous chanterons pour lui,
Chaque jour de notre vie.
Et nous chanterons pour lui,
Chaque jour de notre vie.

3.
Le Seigneur guide nos pas,
Il nous invite au repas,
Tout le long de nos chemins,
Il nous partage son pain.
Tout le long de nos chemins,
Il nous partage son pain.

4.
Le Seigneur est notre ami,
Il nous a donné sa vie.
Il nous a donné son corps,
Il nous a fait mourir la mort.
Il nous a donné son corps,
Il nous a fait mourir la mort.