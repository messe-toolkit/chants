VOIR, SENTIR, TOUCHER, GOÛTER,
ENTENDRE SIFFLER LE VENT.
VOIR, SENTIR, TOUCHER, GOÛTER,
MORDRE LA VIE À PLEINES DENTS !

1.
Promesse du matin :
S’ouvre à nouveau le chemin !
L’oiseau s’est éveillé,
Tout peut recommencer.

2.
Premier baiser du jour :
Monte l’appel de l’amour !
Le beau soleil qui brille
Réchauffe notre vie.

3.
Murmure dans nos coeurs :
Il est tout près, le Seigneur !
Quel bonheur de laisser
Son chant nous appeler !