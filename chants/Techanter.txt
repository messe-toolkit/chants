LEVER LES MAINS, T’ACCUEILLIR
FRAPPER DES MAINS, T’APPLAUDIR
TENDRE NOS MAINS ET TE PRIER
OUVRIR NOS MAINS ET TE CHANTER

1.
Te chanter, te dire bravo
Dès le matin, bravo
Te prier, te dire bravo
Sur le chemin, bravo

2.
Te chanter, te dire merci
Parfois tout bas, merci
Te prier, te dire merci
À pleine voix, merci

3.
Te chanter, te dire je t’aime
Avec le coeur, je t’aime
Te prier, te dire je t’aime
Sans avoir peur, je t’aime