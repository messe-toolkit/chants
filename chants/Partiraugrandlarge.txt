PARTIR AU GRAND LARGE,
QUELQU’UN NOUS ATTEND
BRISER LES AMARRES,
ALLER DROIT DEVANT
MALGRÉ LES ORAGES,
LA PLUIE ET LES VENTS
PARTIR AU GRAND LARGE
QUELQU’UN NOUS ATTEND.

1.
Là-bas c’est l’image
D’un jour de printemps
Ça vaut le voyage
La danse et les chants
Rebelles et sauvages
Allons droit devant
N’ayons pour bagage
Qu’un soleil levant.

2.
Puisqu’on a tous l’âge
D’un rêve d’enfant
Puisque nos rivages
S’appellent le temps
Mettons du courage
Sur nos peurs de “grands”
Et sur nos visages
Le “soleil levant !”

3.
Ouvrons un sillage
Brisons les carcans
Marchons vers la plage
En peuple vivant
Comme des “Rois Mages”
Ou bien des mendiants
N’ayons qu’un “message”
Le soleil levant.