1.
Dieu toujours au-delà,
Nul ne va vers Toi
Qui ne passes par la nuit :
Nuit plus dense,
Nuit plus profonde,
Plus lumineuse aussi
Que soleil de midi.

2.
Dieu toujours au-delà,
Nul ne sait de Toi
Que l’appel qui l’a saisi :
Rien à prendre,
Rien qu’on n’entende,
Mais dans ce rien frémit
La venue de l’Ami.

3.
Dieu toujours au-delà,
Qui combat pour Toi
Prend les armes contre lui :
Guerre pauvre,
Sans nulle gloire,
Où la victoire aussi
Se révèle ennemie.

4.
Dieu toujours au-delà,
Nul ne sait de Toi
Que le rien dont tu l’instruis :
Rien à prendre,
Rien qu’on entende,
Mais dans ce rien frémit
La venue de l’Ami.

5.
Dieu toujours au-delà,
Tu n’égares pas
L’homme épris de l’Infini :
Vive flamme,
Brûlure douce,
C’est ton amour pour lui
Qui traverse sa nuit.