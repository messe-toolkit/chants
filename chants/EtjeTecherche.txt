R.
Et je te cherche
Et je te cherche
Comme on cherche à porter sa vie
Vers la lumière.

1.
Quand tous les grands discours se perdent
Quand la tendresse manque d’amour
Au creux même des plus grandes peines
Quand le bonheur a le cœur lourd.

2.
Au feu de la clarté de l’eau
Aux trésors faits de quelques riens
Un cœur ouvert à quelques mots
Et l’on espère un peu plus loin.

3.
Aux lueurs de tous les regards
Aux mots prières de chaque instant
Quand le feu brûle encore très tard
Qu’on se sent vivre infiniment.