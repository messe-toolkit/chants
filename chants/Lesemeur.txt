TOI, LE SEMEUR DE LA TERRE,
SÈME TA VIE DANS CE MONDE.

1.
Toi qui sèmes la vie,
Donne à mon coeur
Ta semence de vie. (bis)

2.
Toi qui sèmes l’amour,
Donne à mon coeur
Ta semence d’amour. (bis)

3.
Toi qui sèmes la paix,
Donne à mon coeur
Ta semence de paix. (bis)

4.
Toi qui sèmes l’espoir,
Donne à mon coeur
Ta semence d’espoir. (bis)

5.
Toi qui sèmes la joie,
Donne à mon coeur
Ta semence de joie. (bis)

TOI, LE SEMEUR DE LA TERRE,
PRENDS MA VIE POUR CE MONDE.