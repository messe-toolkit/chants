1.
J’n’ai pas choisi de naître ce jour-là,
Je n’ai choisi ni l’année ni le mois,
J’n’ai pas choisi le nom de mon étoile,
Ni même le port où j’ai hissé la voile...

ALORS, ALORS, NE T’EN VA PAS
QUAND JE M’APPROCHE !
ALORS, ALORS, NE T’EN VA PAS
QUAND JE M’APPROCHE DE TOI, DE TOI !

2.
J’ n’ai pas choisi d’avoir ces parents-là,
J’ n’ai pas choisi de naître sans un toit,
J’ n’ai pas choisi le nom qu’ils m’ont donné,
Je n’ai pas choisi d’être abandonné...

3.
J’ n’ai pas choisi la couleur de ma peau,
J’ n’ai pas choisi ce que tu crois qu’elle vaut,
J’ n’ai pas choisi le poids de ma douleur,
Me vois-tu noir jusqu’au fond de mon coeur ?

4.
J’ n’ai pas choisi de naître pas très droit,
Plutôt tordu dans un corps maladroit,
J’ n’ai pas choisi, mais c’est mieux comme ça
Je veux vivre debout comme toi !