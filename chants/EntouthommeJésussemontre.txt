Refrain :
Tout homme est Jésus qui se montre,
Venez tous vous laver les yeux,
À nos partages et vos rencontres.
Chaque homme est visage de Dieu. (bis)

1.
Trop de mépris bafouent les gens,
Regardons l’homme à taille humaine ;
Quittez vos vieux aveuglements :
Lavez vos yeux à la fontaine !
Trop de noyés sont sans rivage,
Qui n’ont plus droit à nos égards,
Il y a bien trop de naufrages ;
L’espérance est dans nos regards.

2.
Il y a bien trop de chômeurs :
Regardons l’homme à taille humaine,
L’homme est enfoui dans ses malheurs :
Lavez vos yeux à la fontaine !
Il y a bien trop de malades,
Qui n’ont plus droit à nos égards,
Trop d’exclus sont restés en rade :
L’espérance est dans nos regards.

3.
On ne voit plus les affamés :
Regardons l’homme à taille humaine ;
Trop de gens sont des mal-aimés :
Lavez vos yeux à la fontaine !
Ceux qui ont pris vingt ans de tôle,
Qui n’ont plus droits à nos égards,
Tous ceux que la misère enrôle :
L’espérance est dans nos regards.

4.
Il y a bien trop de drogués :
Regardons l’homme à taille humaine ;
Le sida fait des relégués :
Lavez vos yeux à la fontaine !
Tous ces lépreux des solitudes,
Qui n’ont plus droit à nos égards,
Inventons les béatitudes :
L’espérance est dans nos regards.