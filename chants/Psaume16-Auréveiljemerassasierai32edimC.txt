AU RÉVEIL, JE ME RASSASIERAI
DE TON VISAGE, SEIGNEUR.

1.
Seigneur, écoute la justice !
Entends ma plainte, accueille ma prière.
Tu sondes mon coeur, tu me visites la nuit,
tu m’éprouves, sans rien trouver.

2.
J’ai tenu mes pas sur tes traces,
jamais mon pied n’a trébuché.
Je t’appelle, toi, le Dieu qui répond :
écoute-moi, entends ce que je dis.

3.
Garde-moi comme la prunelle de l’oeil ;
à l’ombre de tes ailes, cache-moi.
Et moi, par ta justice, je verrai ta face :
au réveil, je me rassasierai de ton visage.