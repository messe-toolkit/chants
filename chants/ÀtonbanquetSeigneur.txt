À TON BANQUET, SEIGNEUR,
LES JUSTES SONT EN FÊTE :
ILS TRESSAILLENT D’ALLÉGRESSE !

1.
Comme les témoins de ta Parole,
Saurons-nous partager
Le Pain de la Vie ?

2.
Comme les veilleurs de ton Royaume,
Saurons-nous accueillir,
Donner sans compter ?

3.
Comme des disciples de lumière,
Saurons-nous devenir
Le Corps du Seigneur ?

4.
Comme des marcheurs vers ton Aurore,
Saurons-nous annoncer
La joie de servir ?

5.
Comme les porteurs de ta justice,
Saurons-nous établir
La paix de Jésus ?