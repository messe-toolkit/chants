R.
MARIE, NOTRE MÈRE, MERVEILLE DE L’AMOUR !

1.
Fraîcheur de la source, icône de la grâce,
miroir de l’éternelle jeunesse,
clarté du jour nouveau !

2.
Étincelle de feu où transparaît la présence,
voie lumineuse qui mène à la rencontre.
Silence qui donne jour à la parole !

3.
Douceur de la gloire, reflet de l’innocence,
sourire de l’infinie tendresse,
parfum de la beauté !

4.
Solitude virginale où chante le Royaume,
perle d’amour, trésor de l’Éternel !
Transparence de l’ineffable splendeur !