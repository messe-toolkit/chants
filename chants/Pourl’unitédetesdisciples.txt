R.
Par ton Esprit rassemble-nous,
Que le monde croie au Dieu d’amour !

1.
Pour l’unité de tes disciples,
Seigneur Jésus, tu as prié.
Il était l’heure où sans réserve un coeur se livre,
Tu leur as dit de quel amour ils sont aimés.

2.
Comme toi-même avec ton Père,
Que tous nos coeurs ne fassent qu’un !
Ton Evangile nous invite à vivre en frères,
Et c’est ainsi que nous serons de vrais témoins.

3.
Comment nous dire tes disciples
Si nos paroles sonnent faux ?
Nous avons tous porté la main sur ta tunique,
Jetant l’opprobre sur l’Icône du Très-Haut.

4.
La profondeur de ta blessure,
Accorde-nous de mieux la voir.
Tu as souffert de nos multiples déchirures,
Libère-nous des divisions qui tuent la foi.

5.
Dans la rencontre en ta demeure
Nous cheminons vers l’unité.
Viendra le jour de te chanter en un seul peuple,
L’Esprit nous mène à découvrir ta vérité.

6.
Cette unité que tu désires,
Fais-la germer comme tu veux.
Ton Souffle passe à la croisée de nos Églises ;
Qu’il nous travaille et nous éclaire de son feu !