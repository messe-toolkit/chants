LAUDATO SI', MI SIGNORE !
LOUÉ SOIS-TU, MON SEIGNEUR !
LAUDATO SI', MI SIGNORE !
POUR TOUTES LES CRÉATURES
QUI NOUS PARLENT DE TOI,
QUI NOUS DISENT QUI TU ES.
LAUDATO SI', MI SIGNORE !

1.
Chante la création !
Dieu y met les hommes et leur donne vie.
Chante la création !
Elle nous maintient, nous porte, nous réconforte.

2.
Chante la création !
La maison commune s'ouvre pour nous tous.
Chante la création !
Mère notre soeur la terre nie les frontières.

3.
Pleure la création !
Elle vient au monde dans le désarroi.
Pleure la création !
Que se brise l'arrogance et la violence.

4.
Pleure la création !
Sans honte on la souille sans s'interroger.
Pleure la création !
Que s'arrêtent le pillage et l'esclavage.

5.
Rêve la création !
Que l'homme y répare les dégâts commis.
Rêve la création !
Ses demeures soient durables et favorables.

6.
Rêve la création !
Que les pauvres exultent, leur Sauveur survient.
Rêve la création !
Il rabat l'orgueil des riches pour la justice.

7.
Vive la création !
Qu'un cri d'espérance calme sa douleur.
Vive la création !
Qu'un soleil radieux renaisse en sa jeunesse.