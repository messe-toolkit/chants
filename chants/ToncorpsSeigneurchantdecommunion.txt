R.
AIMONS-NOUS LES UNS LES AUTRES,
AINSI, DANS L’UNITÉ,
NOUS POUVONS CONFESSER LE PÈRE LE FILS ET L’ESPRIT SAINT.

1.
Ton corps, Seigneur, est une nourriture inépuisable
qui s’unit à notre chair humaine.

2.
Ton corps, Seigneur, donné gratuitement
abreuve la source de mon coeur.

3.
Ton corps, Seigneur, me purifie de toute souillure
et me redonne vie.

4.
Ton corps, Seigneur, est comme un vêtement
qui me protège de tout mal.

5.
Ton corps, Seigneur, est ta présence
accordée à ceux qui viennent à toi.