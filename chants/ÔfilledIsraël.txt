O fille d’Israël,
Héritière des promesse,
La grâce de l’Esprit
T’a découvert en Jésus Christ
le chemin, la vérité, la vie !

A l’ombre du Carmel,
Dans la paix et le silence,
L’amour grandit en toi,
Et le mystère de la croix
T’enveloppe illuminant ta foi.

Viendra le temps de deuil
Où ton peuple est pris au piège.
Dieu seul est ton soutien,
Tu t’es remise entre ses mains,
Le coeur ferme, tu rejoins les tiens.

La haine est sans merci
En ce lieu de mort, de cendre.
Tu offres ta douceur
Comme un rempart contre la peur,
Comme un signe de l’amour vainqueur.

O fille d’Israël,
Héritière des promesses.
Tu entres dans la vie,
Ton sacrifice est accompli,
Rien ne peut te séparer du Christ.

Rien ne peut te séparer du Christ.
Ton sacrifice est accompli,
Tu entres dans la vie,
Héritière des promesses,
O fille d’Israël !