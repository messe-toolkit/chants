1.
Comme un homme
Qui regarde le soleil,
Tes yeux sont brûlés.
Aujourd’hui, tu les ouvres
Sur une autre lumière
Ta seule clarté.

2.
Comme un pauvre
Dont le coeur s’est dénudé
Au souffle de son Dieu,
Tu rends grâce et récoltes
Les moissons de la terre
Et celle des cieux.

3.
Comme un frère
Du Seigneur ressuscité,
Tes mains n’ont rien gardé.
Tu reçois du Père
Le bonheur en partage
Pour l’éternité.

4.
Et le fleuve
Va rejoindre à l’orient
La source dont il vient.
On entend son murmure…
C’est le chant de l’eau vive,
Il n’a pas de fin.