Voix de celui qui crie dans le désert :
« Préparez le chemin du Seigneur,
Rendez droits ses sentiers.
Tout ravin sera comblé,
Toute montagne et toute colline seront abaissées ;
Les passages tortueux deviendront droits,
Les chemins rocailleux seront aplanis ;
Et tout être vivant verra le salut de Dieu. »