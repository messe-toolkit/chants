Stance
Prêtez l’oreille ! une voix crie dans le désert :
Voici votre Sauveur.
Retournez vos chemins vers la source :
Vous serez baptisés dans l’eau et dans le feu.

LES TEMPS SONT ACCOMPLIS,
LES TEMPS SONT ACCOMPLIS:
VIENS, SEIGNEUR JÉSUS,
LES TEMPS SONT ACCOMPLIS,
VIENS, SEIGNEUR JÉSUS.

1.
Ce jour-là, tout ravin sera comblé,
toute colline abaissée.

2.
Ce jour-là, le loup habitera avec l’agneau,
l’enfant et le serpent joueront ensemble.

3.
Ce jour-là, mon peuple se réjouira,
il marchera dans ma lumière.

4.
Ce jour-là, le captif sera libéré,
un temps de grâce proclamé.