Au grand large tu m’entraînes
Paroles et musique : Ulrich HagemannNo. 19-05.
D'après Ps 18.

R.
Au grand large tu m’entraînes,
Ta présence a dissipé ma nuit.
Je te loue mon roc et ma force,
Ô mon Dieu, le rempart de ma vie !

Au grand large tu m’entraînes,
Devant toi la ténèbre n’est plus !
Je te loue mon roc et ma force,
Ô mon Dieu, mon chemin, mon Salut !

1.
Toi mon rocher, ma forteresse,
Tu es celui en qui je m’appuie.
Tu m’as sauvé des filets des ténèbres,
Et de la mort, tu m’as délivré !

2.
Quand j’ai crié, dans la détresse,
Du fond des mers je t’ai appelé,
Tu m’as tiré des tréfonds de l’abîme,
Tu m’as saisi par ton bras puissant !

3.
Tu me revêts de ta lumière,
Par ton amour tu me fortifies.
Seigneur je chante et je te loue sans cesse,
Et en tout lieu, je publie ton nom !

4.
Pour toi mon Dieu, rien d’impossible,
Car tes chemins dépassent nos voies.
Avec toi seul je franchis les murailles,
Toi mon espoir, j’ai confiance en toi !