RENDEZ GRÂCE AU SEIGNEUR : IL EST BON !
ÉTERNEL EST SON AMOUR !

1.
Oui, que le dise Israël :
Éternel est son amour !
Oui, que le dise la maison d’Aaron :
Éternel est son amour !
Qu’ils le disent, ceux qui craignent le Seigneur :
Éternel est son amour !

2.
La pierre qu’ont rejetée les bâtisseurs
est devenue la pierre d’angle :
c’est là l’oeuvre du Seigneur,
la merveille devant nos yeux.
Voici le jour que fit le Seigneur,
qu’il soit pour nous jour de fête et de joie !

3.
Donne, Seigneur, donne le salut !
Donne, Seigneur, donne la victoire !
Béni soit au nom du Seigneur celui qui vient !
De la maison du Seigneur, nous vous bénissons !
Dieu, le Seigneur, nous illumine.