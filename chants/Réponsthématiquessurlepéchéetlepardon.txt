26
Refrain : Guéris-moi, Dieu très saint,
* car j’ai péché contre toi.
verset : Prends pitié de moi, relève-moi. *
doxologie : Gloire au Père et au Fils et au Saint Esprit.
27
Refrain : Nous sommes ton peuple, Seigneur,
* pitié, nous crions vers toi !
verset 1 : Nous ployons sous le poids de nos fautes. *
verset 2 : Écrasés de maux et de peines.
28
Refrain : Le sang de Jésus nous purifie de tout péché.
verset 1 : Venez et voyez les hauts faits de Dieu.
verset 2 : Acclamez Dieu, toute la terre !
29
Refrain : Le Christ nous a aimés,
* il s’est livré pour nous
verset : Il a porté lui-même nos fautes sur la croix *
doxologie : Gloire au Père et au Fils et au Saint Esprit.