R.
Répands ton feu sur la terre Esprit de Dieu,
Répands ton feu dans nos cœurs Esprit d'amour
Que brille ta flamme dans nos vies,
Dans le monde, viens Saint-Esprit !

1.
Esprit de Dieu, Esprit d'amour,
Viens Esprit-Saint embrase-nous.
Vent créateur, doigt du Père,
Viens Esprit-Saint embrase-nous.
\n
Ombre féconde, Brise légère,
Viens Esprit-Saint embrase-nous.
Sève du monde, don gratuit,
Viens Esprit-Saint embrase-nous.
Viens Esprit-Saint embrase-nous.

3.
Source très pure, Torrent de vie,
Viens Esprit-Saint embrase-nous.
Fleuve puissant de la grâce,
Viens Esprit-Saint embrase-nous.
\n
Clarté d'en haut, Langue de feu,
Viens Esprit-Saint embrase-nous.
Flambeau de Gloire, feu vivant,
Viens Esprit-Saint embrase-nous.
Viens Esprit-Saint embrase-nous.

5.
Vrai Défenseur, Consolateur,
Viens Esprit-Saint embrase-nous.
Joie de l’Église, souffle pur
Viens Esprit-Saint embrase-nous.
\n
Onction royale, force et douceur,
Viens Esprit-Saint embrase-nous.
Sainte lumière des prophètes,
Viens Esprit-Saint embrase-nous.
Viens Esprit-Saint embrase-nous.

7.
Trésor de bien, douce vigueur,
Viens Esprit-Saint embrase-nous.
Hôte intérieur, Don suprême,
Viens Esprit-Saint embrase-nous.
\n
Père des pauvres, humble richesse,
Viens Esprit-Saint embrase-nous.
Beauté radieuse sanctifiante,
Viens Esprit-Saint embrase-nous.
Viens Esprit-Saint embrase-nous.

9.
Bon Conseiller, paix en nos âmes,
Viens Esprit-Saint embrase-nous.
Voix de silence, vraie sagesse,
Viens Esprit-Saint embrase-nous.
\n
Brasier ardent de notre foi,
Viens Esprit-Saint embrase-nous.
Mémoire vive des croyants,
Viens Esprit-Saint embrase-nous.
Viens Esprit-Saint embrase-nous.

11.
Splendeur divine, Bonté de Dieu,
Viens Esprit-Saint embrase-nous.
Vie éternelle, vie nouvelle,
Viens Esprit-Saint embrase-nous.
\n
Esprit du Père, Esprit du Fils,
Viens Esprit-Saint embrase-nous.
Rayon d’amour glorieux,
Viens Esprit-Saint embrase-nous.
Viens Esprit-Saint embrase-nous.