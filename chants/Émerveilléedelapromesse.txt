Stance
Émerveillée de la Promesse,
Élisabeth laisse chanter l’Esprit.
Son enfant a bondi d’allégresse ;
voici Marie : le Seigneur vient !

R.
Mère du Sauveur, partage-nous ta joie,
Alléluia !

Versets

1.
Bénie sois-tu, humble servante,
car le Seigneur a comblé ta pauvreté !

2.
Bénie sois-tu, Mère de l’Emmanuel,
car tu viens visiter notre demeure !

3.
Bénie sois-tu, fille d’Israël,
car tu accueilles la Parole éternelle !