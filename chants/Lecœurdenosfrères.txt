LE COEUR DE NOS FRÈRES
EN APPELLE AU COEUR DE DIEU:
RESTERAS-TU LONGTEMPS CACHÉ?
FAIS NOUS VOIR TON VISAGE DE JUSTICE.

1.
Hommes aux chemins multiples,
Nous n’avons pas laissé nos frères aux frontières.
Toi qui nous rassembles en ton nom de Dieu fidèle,
Dans nos coeurs, écoute battre le coeur de l’homme,
Aujourd’hui !

2.
Nos chemins vont dans la nuit,
Et nos questions, nos incroyances nous habitent.
Toi qui nous suscites en témoins de ta présence,
Fais monter comme une sève la foi des hommes,
Aujourd’hui !

3.
Nous marchons dans l’impuissance,
La pauvreté du témoignage nous désarme.
Mets en nous ta force, raffermis notre courage
Dans nos vies, pour que ton peuple en nous se réveille
Aujourd’hui !

4.
Témoins d’un peuple en recherche,
Nous avons soif qu’un monde change par nos luttes.
Dieu, notre convive, rejoins l’homme dans sa marche.
Fais de nous désir et sève aux grandeurs de l’homme,
Aujourd’hui !