POURQUOI CHERCHES-TU LE VIVANT PARMI LES MORTS,
TOI QUI ASPIRES À LA JUSTICE, À LA PAIX ?

Cet avertissement nous aide à sortir de notre bulle de tristesse
et nous ouvre aux horizons de la joie et de l’espérance.