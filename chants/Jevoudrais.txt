R.
Je voudrais qu'en vous voyant vivre,
Étonnés, les gens puissent dire :
Voyez comme ils s'aiment !
Voyez leur bonheur !

1.
Qu'il y ait dans votre langage
Plein d'amour et de vérité ;
Qu'il soit clair simple et sans ambage ;
Qu'il soit bon comme un soir d'été !
Comme un soir d'été !

2.
Mais plus forts que bien des paroles,
Vos regards toucheront des coeurs.
La plus belle des paraboles
C'est le chant de votre bonheur.
De votre bonheur.

3.
Et devant tout ce que vous êtes,
Ils pourront voir les traits de Dieu.
En voyant tout ce que vous faites,
Ils sauront qu'il est avec eux.
Qu'il est avec eux.

4.
S'il est vrai qu'on reconnaît l'arbre
En voyant la beauté des fruits,
Je voudrais, quand ils vous regardent,
Qu'ils y voient les fruits de l'Esprit.
Les fruits de l'Esprit.