R.
C'EST VRAI JE GRANDIS, (bis)
PEU A PEU LE MONDE CHANGE
SUR LES CHEMINS DE MA VIE, (bis)
C'EST VRAI JE GRANDIS.

1.
Je m'en vais seul à l'école
avec mes copains,
les voitures en farandole
passent leur chemin.

2.
Dans ma tête et ma mémoire,
plein de mots nouveaux
me racontent des histoires,
où le monde est beau.

3.
J'ai appris beaucoup de choses
depuis l'an passé,
tantôt grises, tantôt roses,
comme les idées.

4.
Je sais casser les noisettes
et couper le pain,
et tenir beaucoup d'assiettes
entre mes deux mains.