Ô SEIGNEUR, ENVOIE TON ESPRIT
QUI RENOUVELLE LA FACE DE LA TERRE !

1.
Bénis le Seigneur, ô mon âme :
Seigneur mon Dieu, tu es si grand !
Revêtu de magnificence,
tu as pour manteau la lumière !
Tu as donné son assise à la terre :
qu’elle reste inébranlable, au cours des temps.
Tu l’as vêtue de l’abîme des mers,
les eaux couvraient même les montagnes.

2.
Dans les ravins tu fais jaillir des sources
et l’eau chemine aux creux des montagnes ;
les oiseaux séjournent près d’elle :
dans le feuillage on entend leurs cris.
De tes demeures tu abreuves les montagnes,
et la terre se rassasie du fruit de tes oeuvres ;
tu fais pousser les prairies pour les troupeaux,
et les champs pour l’homme qui travaille.

3.
Quelle profusion dans tes oeuvres, Seigneur !
Tout cela, ta sagesse l’a fait :
la terre s’emplit de tes biens.
Bénis le Seigneur, ô mon âme !