Seigneur Jésus, envoyé par le Père
pour guérir et sauver les hommes,

KYRIE ELÉISON, KYRIE ELÉISON, KYRIE ELÉISON.

Ô Christ, venu dans le monde
appeler tous les pécheurs,

CHRISTE ELÉISON, CHRISTE ELÉISON, CHRISTE ELÉISON.

Seigneur, élevé dans la gloire du Père
où tu intercèdes pour nous,

KYRIE ELÉISON, KYRIE ELÉISON, KYRIE ELÉISON.