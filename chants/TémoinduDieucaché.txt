1.
Témoin du Dieu caché,
Jésus de Nazareth,
Sur toi le voile est retombé ;
Tu es parti dans la nuée.
Au tombeau vide Pierre et Jean
Ont vu deux anges leur disant :
« Il vous précède en Galilée ».
... En Galilée
Où donc es-tu, Ressuscité ?

2.
Tes signes par milliers,
Jésus de Nazareth,
Ne seraient-ils que du passé ?
Tant de boiteux voudraient marcher !
Lazare est-il encor vivant
Dans un Royaume florissant
Comme un jardin d’humanité?
... L’humanité,
Ta main, Seigneur, l’a relevée.

3.
Qui donc saura te voir,
Jésus de Nazareth,
Dans un regard brillant d’espoir,
Dans un humain vibrant de foi ?
Nous entendons des messagers
Qui n’ont pas peur de te nommer.
Que diront-ils de la vraie joie ?
... De la vraie joie
Promise au coeur qui s’ouvre à toi.