R.
TOUS LES CHEMINS D’ICI
NE MÈNENT PAS À ROME
TOUS LES CHEMINS DE VIE
NE MÈNENT PAS À ROME
MAIS ILS PASSENT PAR L’HOMME

1.
Qui protège la rose
Autant que son enfant
La nature blessée
Qui nous a tant donné
Nous avons jardiné
Depuis la nuit des temps
Qui pourrait l’oublier ?

2.
Créature qui ose
Inventer d’autres chants
Plus subtils et plus beaux
Que le chant des oiseaux
Pour s’envoler bien haut
Il ordonne le vent
Le feu, la terre et l’eau

3.
Dans cette oeuvre grandiose
Comment ne pas trahir
L’invisible horloger
D’un monde inachevé ?
Qu’il nous faut dévoiler
De soupir en désir
Sans jamais nous renier

4.
Qui protège la rose
Autant que son enfant
La nature blessée
Qui nous a tant donné
Nous avons récolté
Depuis la nuit des temps
Qui pourrait l’oublier ?