Antienne - stance
Qui perd sa vie à cause du Seigneur et de l'Évangile la sauvera.

R.
Benedictus Deus et Pater Domini nostri Iesu Christi.

Versets
Ps 24, 4-6 ; 8-9

1.
Seigneur, enseigne-moi tes voies,
Fais-moi connaître ta route.
Dirige-moi par ta vérité, enseigne-moi,
Car tu es le Dieu qui me sauve.

2.
C'est toi que j'espère tout le jour
En raison de ta bonté, Seigneur.
Rappelle-toi, Seigneur, ta tendresse,
Ton amour qui est de toujours.

3.
Il est droit, il est bon, le Seigneur,
Lui qui montre aux pécheurs le chemin.
Sa justice dirige les humbles,
Il enseigne aux humbles son chemin.

Ou bien

Ps 102, 1-4 ; 8 ; 10 ; 12-13

1.
Bénis le Seigneur, ô mon âme,
Bénis son nom très saint, tout mon être !
Bénis le Seigneur, ô mon âme,
N'oublie aucun de ses bienfaits !

2.
Car il pardonne toutes tes offenses
Et te guérit de toute maladie ;
Il réclame ta vie à la tombe
Et te couronne d'amour et de tendresse ;

3.
Le Seigneur est tendresse et pitié,
Lent à la colère et plein d'amour ;
Il n'agit pas envers nous selon nos fautes,
Ne nous rend pas selon nos offenses.

4.
Aussi loin qu'est l'orient de l'occident,
Il met loin de nous nos péchés ;
Comme la tendresse du père pour ses fils,
La tendresse du Seigneur pour qui le craint !