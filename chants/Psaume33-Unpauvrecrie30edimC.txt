UN PAUVRE CRIE ;
LE SEIGNEUR ENTEND.

1.
Je bénirai le Seigneur en tout temps,
sa louange sans cesse à mes lèvres.
Je me glorifierai dans le Seigneur :
que les pauvres m’entendent et soient en fête !

2.
Le Seigneur regarde les justes,
il écoute, attentif à leurs cris.
Le Seigneur entend ceux qui l’appellent :
de toutes leurs angoisses, il les délivre.

3.
Il est proche du coeur brisé,
il sauve l’esprit abattu.
Le Seigneur rachètera ses serviteurs :
pas de châtiment pour qui trouve en lui son refuge.