STANCE
Tu n’avais pas un lieu où reposer la tête,
O toi qui nous invites
A partager ta pauvreté.
Tu seras notre joie,
Vivante plénitude !
Ne laisse pas s’éteindre en nous
Le Feu qui doit tout consumer.

R.
Grave en nos coeurs
Ta ressemblance !

1.
Pour nous tu t’es fait pauvre jusqu’à donner ta vie
Saurons-nous perdre la nôtre ?

2.
Tu nous offres en partage un amour infini,
Comment l’accueillir chaque jour ?

3.
Sans l’Esprit qui nous fait contempler ton visage,
Pourrons-nous jusqu’au bout courir l’épreuve ?