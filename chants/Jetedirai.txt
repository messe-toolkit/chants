1.
Bien avant qu’au réveil, deux paupières mi-closes
Ne s’ouvrent à la vie, ne découvrent le jour
Bien avant que l’oiseau, bien avant que les roses
Ne viennent t’apporter trois notes de velours
Je te dirai…

JE TE DIRAI CETTE MUSIQUE
EN MOI DEPUIS TOUJOURS
PETITE LIGNE MÉLODIQUE
QUI N’ATTENDAIT QUE TOI
POUR VOIR ENFIN LE JOUR.
POUR VOIR ENFIN L’AMOUR.

2.
Bien avant qu’un rayon n’éblouisse tes songes
Et qu’au petit matin, le temps prenne son tour
Bien avant que ces mots en phrases se prolongent
Et qu’un battement d’aile les rende un peu moins lourd
Je te dirai…

3.
Bien avant que la pluie ne pleure sur ta vitre
Ne flic-flac sur le sol, ne glisse sur ta joue
Bien avant que les vents n’emportent un peu trop vite
Les milliers de projets, les rêves, les mots d’amour
Je te dirai…