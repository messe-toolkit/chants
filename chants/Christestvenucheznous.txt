1.
Christ est venu chez nous, il a choisi d'habiter notre terre.
Christ est venu chez nous, il a choisi d'habiter notre terre.
Lui, le Verbe, le Fils de l'Eternel aux sentiers de nos jours,
Il a porté nos peines et vibré à nos fêtes ;
Sa vie fut chant d'amour.

R.
Christ au milieu de nous !
Sa voix résonne en chacun de ses frères :
Paroles qui relèvent !
C'est Lui l'espoir du monde !

2.
Christ a souffert pour nous, il a gravi la montée du calvaire.
Christ a souffert pour nous, il a gravi la montée du calvaire.
Lui, le Maître, celui qui nous ouvrait le chemin du bonheur,
Pourquoi le condamner à la mort des prophètes ?
Son Dieu est-il sauveur ?

R.
Christ au milieu de nous !
Son cri renverse les murs de la haine :
Parole qui libère,
Salut pour notre monde !

3.
Christ est lumière en nous, il s'est levé au matin de la Pâque.
Christ est lumière en nous, il s'est levé au matin de la Pâque.
Joie des hommes, voici dans l'univers la clarté du Vivant !
Qui donc révèlera le secret de sa flamme,
Sinon le coeur brûlant ?

R.
Christ au milieu de nous !
Son Souffle passe aujourd'hui sur nos terres :
Parole qui réveille,
Printemps pour notre monde !