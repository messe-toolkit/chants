R.
Levez-vous, bergers et bergères,
Allumez flûtiaux et pipeaux,
Réveillez sonnailles et troupeaux,
aujourd'hui, Dieu naît sur la terre (bis)

1.
Où donc voulez-vous que Dieu aille ?
Puisque pour mieux avoir raison,
Les riches ont fermé leur maison,
Où donc voulez-vous que Dieu aille ?
Les riches ont fermé leur maison,
et Jésus naît sur un peu de paille...

2.
Où voulez-vous que Dieu s'attable ?
Tant de gens n'ont pas à manger,
La faim réveille les bergers,
Où voulez-vous que Dieu s'attable ?
La faim réveille les bergers,
et Jésus naît au fond d'une étable...

3.
Où voulez-vous que Dieu se sauve ?
Déjà il est tant d'étrangers,
Dieu ne sait pas où se loger
Où voulez-vous que Dieu se sauve ?
Dieu ne sait pas où se loger
Et Jésus naît enfant des plus pauvres...