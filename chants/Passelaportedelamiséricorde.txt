PASSE LA PORTE
DE LA MISÉRICORDE !
CROIS À L’AMOUR DE DIEU
QUI CONSOLE ET QUI PARDONNE !

1.
Souviens-toi de l’enfant prodigue !
Il revient et le Père court à son devant !

2.
Souviens-toi du repas de fête !
Aie confiance ! Le ciel accueille le pécheur !

3.
L’Esprit saint aujourd'hui t’appelle !
Sois témoin devant tous du Dieu de compassion !

4.
Que ta vie chaque jour témoigne :
Le premier Dieu franchit la porte et nous rejoint