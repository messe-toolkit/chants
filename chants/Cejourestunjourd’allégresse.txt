Stance
Ce jour est un jour d’allégresse
tout entier consacré au Seigneur.
Peuple de Dieu, n’endurcis pas ton coeur,
aujourd’hui s’accomplit la promesse :
un temps de grâce est accordé par le Père.

R.
Que la Bonne Nouvelle retentisse
par toute la terre !

1.
Jésus est venu chez les siens,
et l’Esprit repose sur lui.

2.
Le Serviteur porte aux aveugles la lumière,
il nous ouvre les Écritures.

3.
Le Seigneur parmi les hommes appelle des témoins
et les envoie en messagers de paix.