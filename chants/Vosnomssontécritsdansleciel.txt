STANCE
Vos noms sont écrits dans le ciel,
Apôtres du Seigneur, alleluia,
Que partout sur la terre
Se répande votre joie.

I
CHOEUR A
Vous avez peiné en jetant le filet
Et vous partagez le repas sur la grève,
Vous partagez avec Jésus !

CHOEUR B
Vous avez semé l’évangile de paix,
Et vous engrangez des moissons d’allégresse,
Vous engrangez près de Jésus.

R.
Que partout sur la terre
Se répande votre joie.

II
CHOEUR A
Vous avez guidé le troupeau dans la nuit
et vous le menez sur la terre promise,
Vous le menez avec Jésus !

CHOEUR B
Vous avez délié du péché les captifs,
Et vous exultez au Soleil de justice,
Vous exultez près de Jésus.

III
CHOEUR A
Vous avez passé le ravin de la mort,
Et vous habitez où s’éveille l’aurore,
Vous habitez avec Jésus !

CHOEUR B
Vous avez donné votre vie par amour,
Vous nous recevrez où jaillissent les sources.
Vous nous recevrez près de Jésus.