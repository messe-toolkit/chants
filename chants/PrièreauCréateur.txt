Seigneur et Père de l’humanité,
toi qui as créé tous les êtres humains avec la même dignité,
insuffle en nos coeurs un esprit de frères et soeurs.
Inspire-nous un rêve de rencontre,
de dialogue, de justice et de paix.
Aide-nous à créer des sociétés plus saines
et un monde plus digne,
sans faim, sans pauvreté, sans violence, sans guerres.

Que notre coeur s’ouvre
à tous les peuples et nations de la terre,
pour reconnaître le bien et la beauté
que tu as semés en chacun,
pour forger des liens d’unité, des projets communs,
des espérances partagées.

Amen !