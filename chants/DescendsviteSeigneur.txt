R.
Remplis nos vies de ta lumière,
Ne sois pas loin, Seigneur Jésus,
Et ton chemin sera connu
Au plus lointain de notre terre.

1.
Descends vite, Seigneur descends vite, (bis)
Ne tarde pas, habille nos matins de tes merveilles.
Parle à notre nuit, parle, (bis)
Nous chanterons la paix de ton visage.

2.
Descends vite, Seigneur, descends vite, (bis)
Comme la pluie,
Irrigue nos déserts en abondance,
Parle à notre soif, parle, (bis)
Nous goûterons au puits
De ton eau vive.

3.
Descends vite, Seigneur, descends vite, (bis)
En nos cœurs froids,
Allume le brasier de ta tendresse,
Parle à notre cœur, parle, (bis)
Nous porterons le feu
De l'Évangile.