Ô Père Saint, tu révèles ta bonté.
Ton Fils, Jésus, par sa mort nous a sauvés.
Tu l’as relevé.
Par Ton Esprit,
Tu donnes vie pour qu’advienne ton Amour.