1.
Seigneur, nous déposons sur la table garnie
Quelques fruits de la terre en signe de merci.
Le froment fut moulu, la pâte fut pétrie
Pour devenir enfin le pain qui fortifie.

2.
Seigneur, nous te chantons notre reconnaissance
Pour le jeu des saisons, le raisin qui mûrit.
Le soleil a oeuvré avec persévérance
Pour nous offrir enfin le vin qui nous sourit.

3.
Seigneur, nous te prions : donne-nous la patience
De veiller chaque jour à préserver la vie.
Il est urgent d’aimer et semer l’espérance
Des hommes de demain, des enfants d’aujourd’hui.