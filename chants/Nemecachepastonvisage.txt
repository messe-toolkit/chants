R.
Faut-il marcher jusqu'au désert pour te prier?
Faut-il aller vers l'inconnu te rencontrer en plein désert?

1.
Ne me cache pas ton visage,
O mon Dieu, O mon Dieu!
Je suis entouré de mirages,
Viens à mon aide!
Si tu détournes les yeux
Quand vient la soif,
Qui donc me guidera
Jusqu'aux frontières de la vie?
Qui donc me guidera
Jusqu'aux frontières de la vie?

2.
Ne me refuse pas ta grâce,
O mon Dieu :
Mon coeur est prisonnier des glaces,
Viens à mon aide...
Si tu m'éloignes du feu
Quand vient le froid.
Qui donc me conduira
Jusqu'à la chaleur de l'été?
Faut-il marcher jusqu'à l'hiver
Pour te prier?
Faut-il aller vers l'inconnu
Te rencontrer en plein hiver?

3.
N'éteins pas la mèche qui fume,
O mon Dieu :
Je suis enveloppé de brumes,
Viens à mon aide...
Si tu dérobes ton coeur
Quand vient le soir,
Qui donc m'emmenera
Vers le soleil et sa lumière?
Faut-il marcher jusqu'à la nuit
Pour te prier?
Faut-il aller vers l'inconnu
Te rencontrer en pleine nuit?

4.
Ne me laisse pas dans la peine,
O mon Dieu :
Mon coeur est entouré de haine,
Viens à mon aide...
Si tu t'écartes de moi
Quand vient la peur,
Qui donc m'apportera
Tous les trésors de sa tendresse?
Faut-il marcher jusqu'à la vie
Pour t'annoncer?
Faut-il aller vers l'inconnu
Te rencontrer en pleine vie?

5.
Ne me laisse pas dans le vide,
O mon Dieu :
Mon coeur est une terre aride,
Viens à mon aide...
Si tu refuses ton corps
Quand vient la faim,
Qui donc me donnera
Le pain de vie pour me nourrir?
Faut-il marcher jusqu'à la faim
Pour te prier?
Faut-il aller vers l'inconnu
Te rencontrer malgré la faim?

6.
Ne laisse pas brûler mes larmes,
O mon Dieu :
Mon coeur est entouré de flammes,
Viens à mon aide...
Si tu t'en vas loin de moi
Quand vient la mort,
Qui donc me montrera
Les chemins de nouvelle vie?
Faut-il marcher jusqu'à la mort
Pour en renaître?
Faut-il aller vers l'inconnu
Te rencontrer après la mort?