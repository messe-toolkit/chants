Minuit ! Chrétiens, c'est l'heure solennelle
Où l'homme Dieu descendit jusqu'à nous,
Pour effacer la tache originelle
Et de son père arrêter le courroux :
Le monde entier tressaille d'espérance
A cette nuit qui lui donne un sauveur.
Peuple, à genoux, attends ta délivrance,
Noël ! Noël ! Voici le Rédempteur ! (bis)

Le Rédempteur a brisé tout entrave,
La terre est libre et le ciel est ouvert.
Il voit un frère où n'était qu'un esclave ;
L'amour unit ceux qu'enchaînait le fer ;
Qui lui dira notre reconnaissance ?
C'est pour nous tous qu'il naît, qu'il souffre et meurt :
Peuple, debout ! Chante ta délivrance ;
Noël ! Noël ! Chantons le Rédempteur ! (bis)