R.
Ô Esprit de Feu, Toi notre Seigneur !
Viens, sois le maître en nos cœurs,
Viens Esprit de Dieu !

1.
Viens, Esprit de Sainteté,
Viens, Esprit de vérité !
Viens, Esprit de charité,
Viens, nous recréer !

2.
Viens, Esprit consolateur,
Viens, Toi qui connais nos peurs !
Viens, apaise notre cœur,
Toi, le Défenseur !

3.
Viens, et brille dans la nuit,
Viens, réchauffe et purifie !
Viens, feu qui nous es promis,
Transforme nos vies !