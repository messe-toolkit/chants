VOICI QUE TU FAIS
UN MONDE NOUVEAU,
LA TERRE ARC-EN-CIEL,
UN MONDE PLUS BEAU.
IL GERME DÉJÀ,
SON JOUR GRANDIRA,
IL GERME DÉJÀ,
NE LE VOYONS-NOUS PAS?

1.
Un monde en germe sur nos terres travaillées,
Il va profond puiser les eaux d’humanité.
Nous le voyons ouvrir des voies de non-violence,
S’aventurer sur le terrain de la confiance.

2.
Un monde en germe qui combat contre l’ivraie
En espérant que paraîtra le temps des blés.
Tu nous appelles à tout risquer pour la récolte ;
Réveille en nous l’amour qui lutte et qui se donne.

3.
Un monde en germe qui éclaire l’avenir ;
Quittons sans crainte le champ clos des souvenirs !
Justice et paix sont l’horizon qui nous habite,
Que par nos mains ce monde naisse et qu’il grandisse !