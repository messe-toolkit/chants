I.
Je bénirai le Seigneur, lui qui m’a délivré.
En lui la joie de mon cœur,
Je viens le célébrer.
Mon Seigneur, j’ai vu ta gloire,
Tu t’es manifesté,
Dans ton temple, je jubilerai.

R.
Béni soit ton nom, Seigneur Jésus,
À toi, la louange !
Béni soit ton nom, Seigneur Jésus,
Toi, le prince de Paix ! (bis)

1.
Ton amour, Seigneur, me donne la vraie joie.
Que jamais je ne sois séparé de toi.
Tu es mon seul Sauveur,
Mon cœur est tout à toi,
Toi, ma seule espérance,
Mon Seigneur et mon Roi !

2. Ô Jésus, ta miséricorde est ma joie
Je veux accourir, me plonger dans tes bras
Car ton cœur bat pour moi,
Je ne suis rien sans toi,
Toi, ma seule espérance,
Mon Seigneur et mon Roi !