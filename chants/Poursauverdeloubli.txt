R.
Puisqu'il vit avec nous tous les jours de malheur,
Avec lui nous pouvons espérer.
Puisqu'il vit avec nous au secret de nos cœurs,
Rien ne peut nous le faire oublier.

1.
Pour sauver de l'oubli l'amour du Bien-Aimé,
Nous voici rassemblés.
Pour ouvrir notre cœur à tous les cœurs blessés,
Nous voici rassemblés.

2.
Pour sauver de l'oubli la voix du Bien-Aimé,
Nous voici rassemblés.
Pour entendre celui qui vient nous réveiller,
Nous voici rassemblés.

3.
Pour sauver de l'oubli la mort du Bien-Aimé,
Nous voici rassemblés.
Pour entrer dans la vie du Christ ressuscité,
Nous voici rassemblés.

4.
Pour sauver de l'oubli la joie du Bien-Aimé
Nous voici rassemblés.
Pour chanter avec lui puisqu'il nous a sauvés,
Nous voici rassemblés.