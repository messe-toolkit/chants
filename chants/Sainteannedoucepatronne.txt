R.
SAINTE ANNE,
DOUCE PATRONNE,
PROTÈGE-NOUS!
SAINTE ANNE,
MÈRE SI BONNE,
VEILLE SUR NOUS,
VEILLE SUR NOUS.

1.
Tu as porté Marie
Dans l’attente du jour
Où ses yeux pleins de vie,
S’ouvriraient à l’amour !

2.
Tu l’as conduite au Temple
Pour l’offrir au Seigneur.
Tu lui donnas l’exemple
D’une mère au grand coeur !

3.
Marie, dès son enfance,
Désira servir Dieu.
Sa joie, son innocence
Éclataient dans ses yeux !

4.
Marie, choisie par grâce
Pour porter le Sauveur,
Lui offrit tout l’espace,
Qu’elle avait dans le coeur.

5.
Accueille nos prières
Merveilleuse maman.
Présente-les au Père,
Par Marie, ton enfant.