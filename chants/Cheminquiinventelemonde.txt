R.

1.
CHEMIN QUI INVENTE LE MONDE
SI J'AI L'AUDACE DE M'AIMER.
SI J'AI L'AUDACE DE T'AIMER.

2.
LA PAIX QUI INVENTE LE MONDE

3.
CHANSON QUI INVENTE LE MONDE

4.
SOLEIL QUI INVENTE LE MONDE

5.
MERCI QUI INVENTE LE MONDE

1.
Marcher longtemps, partir au loin,
Braver l’orage et la tempête:
Je voudrais tant prendre la route
Mais c’est ma vie qui est chemin, chemin!

2.
Donner un peu mon amitié
Prendre et comprendre les épreuves:
Je voudrais tant que mes mains s’ouvrent
Mais c’est ma vie qui fait la paix, la paix!

3.
Tisser des liens sans exclusion,
Laisser fleurir les différences:
Je voudrais tant me faire entendre
Mais c’est ma vie qui est chanson, chanson!

4.
Sortir un jour de mes sommeils,
Ouvrir les yeux, enfin renaître
Je voudrais tant voir la lumiére
Mais c'est ma vie qui est soleil, soleil!

5.
Oser m’aimer tel que je suis,
Et puis risquer tous les dialogues:
Je voudrais tant prendre parole
Et c’est ma vie qui dit Merci, Merci!