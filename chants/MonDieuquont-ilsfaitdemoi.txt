R.
MON DIEU, MON DIEU
QU'ONT-ILS FAI T DE MOI ?
MON DIEU, MON DIEU
NE M'ABANDONNE PAS !

1.
Ils ont arraché mes deux poutres
De la pile de bois
Les ont tirées sur la grand-route
En ont fait une croix
C'était un vendredi matin
Le printemps n'était pas bien loin

2.
Une foule était en colère
Le ciel était en gris
C'était des loups et des vipères
Qui aboyaient leurs cris
L'homme était là, tout déchiré
Ils le raillaient et l'injuriaient

3.
Quand ils l'ont cloué sur les poutres
Mon bois a tressailli
Rempli d'horreur, rempli de doute
Lorsque l'homme a gémi
Ainsi donc il était trop tard
Le rêve devenait cauchemar

RECITANT
Pourtant le dimanche qui suivit fut éclatant de lumière.
La joie du printemps jaillissait partout alors qu'on ne l'attendait plus. Le troisième arbre n'en croyait pas ses yeux
Désormais, chaque fois qu'on le regarderait on penserait à Dieu.
Chacun des rêves des trois arbres s'était réalisé.
Le temps était venu pour la fête...