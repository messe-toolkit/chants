RACONTEZ À TOUS LES PEUPLES
LES MERVEILLES DU SEIGNEUR !

1.
Chantez au Seigneur un chant nouveau,
chantez au Seigneur, terre entière,
chantez au Seigneur et bénissez son nom !

2.
De jour en jour, proclamez son salut,
racontez à tous les peuples sa gloire,
à toutes les nations ses merveilles !

3.
Rendez au Seigneur, familles des peuples,
rendez au Seigneur, la gloire et la puissance,
rendez au Seigneur la gloire de son nom.

4.
Adorez le Seigneur, éblouissant de sainteté.
Allez dire aux nations : « Le Seigneur est roi ! »
Il gouverne les peuples avec droiture.

ALLÉLUIA. ALLÉLUIA.

Dieu nous a appelés par l’Évangile
à entrer en possession de la gloire
de notre Seigneur Jésus Christ.