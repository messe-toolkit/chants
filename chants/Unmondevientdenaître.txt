R.
UN MONDE VIENT DE NAITRE DE LA CHAIR DU SEIGNEUR
SURGISSANT DE LA TERRE, DIEU DEMEURE PARMI NOUS
CHANTONS NOEL, LE SEIGNEUR EST PARMI NOUS,
CHANTONS L'EMMANUEL ! DIEU AVEC NOUS.

1.
Chantons au Seigneur un chant nouveau :
voici qu'il a fait des merveilles.
car son salut a resplendi sur notre terre
et son amour sur tous les peuples.

2.
Chantons au Seigneur un chant nouveau :
voici qu'il naît sur notre terre.
En notre chair, Dieu a pris chair pour nous sauver ;
Il nous fait vivre en sa lumière.

3.
Chantons au Seigneur un chant nouveau :
Il nous a fait miséricorde.
Le Fils de Dieu vient nous combler de son Esprit
et consoler tous ceux qui pleurent.

4.
Chantons au Seigneur un chant nouveau :
toute la création exulte !
Le Fils de Dieu vient partager la vie des hommes
pour que nous partagions sa gloire.

5.
Chantons au Seigneur un chant nouveau :
les cieux célèbrent sa louange.
Tout ce que Dieu avait promis par les prophètes
s'est accompli pour notre joie !

6.
Chantons au Seigneur un chant nouveau :
sa gloire habite notre terre.
le Dieu vivant a fait de nous des fils de Dieu
et nous conduit vers son Royaume.