ANTIENNE
Cherchez d'abord le Royaume et sa justice,
et le surcroît vous sera donné. (Mt 6, 33)

R.
Cantate Domino canticum novum. Alléluia, (alléluia).
Cantate omino omnis terra. Alléluia, (alléluia)

Ps 26, 1 ; 4-5.

Le Seigneur est ma lumière et mon salut ;
De qui aurais-je crainte ?
Le Seigneur est le rempart de ma vie ;
Devant qui tremblerais-je ?

J'ai demandé une chose au Seigneur, la seule que je cherche :
Habiter la maison du Seigneur tous les jours de ma vie,
Pour admirer le Seigneur dans sa beauté
Et m'attacher à son temple.

Qu'il me réserve un lieu sûr
Au jour du malheur ;
Il me cache au plus secret de sa tente,
Il m'élève sur le roc.

Ps 6, 1 ; 4 ; 13-14.

Le Seigneur est ma lumière et mon salut ;
De qui aurais-je crainte ?
Le Seigneur est le rempart de ma vie ;
Devant qui tremblerais-je ?

J'ai demandé une chose au Seigneur, la seule que je cherche :
Habiter la maison du Seigneur tous les jours de ma vie,
Pour admirer le Seigneur dans sa beauté
Et m'attacher à son temple.

Mais j'en suis sûr, je verrai les bontés du Seigneur
Sur la terre des vivants.
Espère le Seigneur, sois fort et prends courage ;
Espère le Seigneur.