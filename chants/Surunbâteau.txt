R.
SUR UN BATEAU
GLISSANT SUR L’EAU,
JE T’OFFRE MON SOLEIL.
NOUS VOGUERONS
SUR L’OCÉAN
JUSQU’À LA FIN DES TEMPS.

1.
Il est temps
D’oublier le passé.
Il est temps
De déchirer les pages
De tes souvenirs :
La vie
Ne coule pas derrière,
Mais devant
Vers de nouveaux rivages.

2.
Il est temps
De sourire au présent
Il est temps
D’embrasser la planète,
De vivre ta vie.
C’est bien :
Tu as lutté pour d’autres ;
Aujourd’hui
Pense un peu à toi-même.

3.
Il est temps
D’inventer l’avenir.
Il est temps
De t’enivrer de rêves,
De croire au bonheur.
La vie,
C’est apprendre à se taire ;
Écouter
La voix qui dit : « je t’aime. »