NOËL ! NOËL ! NOËL

1.
Sans abri, Jésus nait dans une étable
Protégé par Joseph et puis Marie
Près d’un âne et d’un boeuf éveillés
DANS LA NUIT ÉTOILÉE !

2.
Des brebis, leurs bergers ont accouru
Ils ont vu « l’enfant Dieu » et ils ont cru
De leurs coeurs, bien des drames ont coulés
Il VIENT POUR NOUS SAUVER !

3.
Les fragiles, les petits, les délaissés
Sont par Lui, consolés, privilégiés
Son amour pour toujours est donné
IL VIENT À NOS CÔTÉS !

4.
Hâtons-nous, de monter sur la colline
Pour le voir, l’adorer jouer des hymnes
Joie des anges ! Quand ils chantent ses louanges !
IL NOUS EST RÉVÉLÉ !