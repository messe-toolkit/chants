R.
Élargis ton cœur, ouvre tes frontières
À d’autres racines, à d’autres couleurs
Élargis ton cœur, ouvre tes frontières
Élargis ton cœur, agrandis ta terre,
Et tu auras trouvé, la joie de partager.
Élargis ton cœur, élargis ton cœur

1.
L’avenir ouvre ses bras au métissage.
Il saura mélanger les hommes et les idées
L’avenir fera chanter nos différences
Comme une symphonie aux milliers d’harmonies!

2.
Des migrants de tous les pays
Frappent à nos portes
Ils n’ont plus qu’un désir :
Leur vie à rebâtir!
Avec eux nous deviendrons un nouveau peuple.
Faiseur de lendemains,
plus justes et plus humains.

3.
Notre Dieu nous fait cadeau de sa tendresse.
Pour délier nos mains, pour apaiser la faim.
Notre Dieu nous donne envie de vivre ensemble
Et d’être au fil des jours,
des éveilleurs d’Amour
