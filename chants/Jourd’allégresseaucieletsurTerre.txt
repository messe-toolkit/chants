JOUR D'ALLÉGRESSE AU CIEL ET SUR TERRE

JOUR D'ALLÉGRESSE AU CIEL ET SUR TERRE,
DIEU MET EN NOUS SON SOUFFLE DE JOIE !
CRIS DE VICTOIRE ET CHANTS DE BONHEUR.
POUR CELUI QUI NOUS AIME À TOUT JAMAIS !

1.
Heureux celui qui, dans le secret, entend la voix du ressuscité
LE ROYAUME DES CIEUX EST À LUI
ET C'EST LA FÊTE DANS LE CŒUR DU PÈRE !

2.
Heureux celui qui donne sa vie sans rien garder de ce qu'il reçoit,
LE ROYAUME DES CIEUX EST À LUI ET C'EST LA FÊTE…

3.
Heureux celui qui part en avant dans la confiance des tout-petits,
LE ROYAUME DES CIEUX EST À LUI ET C'EST LA FÊTE…

4.
Heureux celui pour qui la parole est la lumière éclairant ses pas,
LE ROYAUME DES CIEUX EST À LUI ET C'EST LA FÊTE…

5.
Heureux celui qui est assoiffé de voir surgir la beauté des peuples,
LE ROYAUME DES CIEUX EST À LUI ET C'EST LA FÊTE…

6.
Heureux celui qui est sans repos tant que l'amour n'est pas aimé,
LE ROYAUME DES CIEUX EST À LUI ET C'EST LA FÊTE…