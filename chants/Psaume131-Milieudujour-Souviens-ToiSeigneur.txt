1.
Souviens-toi, Seigneur, de David
et de sa grande soumission

2.
quand il fit au Seigneur un serment,
une promesse au Puissant de Jacob :

3.
«Jamais je n'entrerai sous ma tente,
et jamais ne m'étendrai sur mon lit,

4.
j'interdirai tout sommeil à mes yeux
et tout répit à mes paupières,

5.
avant d'avoir trouvé un lieu pour le Seigneur,
une demeure pour le Puissant de Jacob.»

6.
Voici qu'on nous l'annonce à Ephrata,
nous l'avons trouvée près de Yagar.

7.
Entrons dans la demeure de Dieu,
prosternons-nous aux pieds de son trône.

8.
Monte, Seigneur, vers le lieu de ton repos,
toi, et l'arche de ta force !

9.
Que tes prêtres soient vêtus de justice,
que tes fidèles crient de joie !
10.
Pour l'amour de David, ton serviteur,
ne repousse pas la face de ton messie.
11.
Le Seigneur l'a juré à David,
et jamais il ne reprendra sa parole :
12.
«C'est un homme issu de toi
que je placerai sur ton trône.
13.
«Si ses fils gardent mon alliance,
les volontés que je leur fais connaître,
14.
leurs fils, eux aussi, à tout jamais,
siègeront sur le trône dressé pour toi.»
15.
Car le Seigneur a fait choix de Sion ;
elle est le séjour qu'il désire.
16.
«Voilà mon repos à tout jamais,
c'est le séjour que j'avais désiré.
17.
«Je bénirai, je bénirai ses récoltes
pour rassasier de pain ses pauvres.
18.
Je vêtirai de gloire ses prêtres,
et ses fidèles crieront, crieront de joie.
19.
«Là, je ferai germer la force de David ;
pour mon messie, j'ai allumé une lampe.
20.
Je vêtirai ses ennemis de honte,
mais, sur lui, la couronne fleurira.»