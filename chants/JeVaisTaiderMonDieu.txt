1.
Au plus profond de moi
Le puits est très profond
Et dans ce puits il y a Dieu,
Lorsque j'écoute en moi
Dieu écoute Dieu.

R.
Je vais t'aider mon Dieu
A ne pas t'éteindre en moi,
Je vais t'aider mon Dieu
A ne pas t'éteindre en moi. (bis)

2.
Si tu me prends la main
Je te suivrai partout,
Fais-moi gouter un peu de paix
et sans me révolter
J'accepterai tout.

3.
Toi qui m'as enrichie
Permets moi de donner,
Je me recueille en toi mon Dieu,
Je t'offre tout de moi,
Ma vie, ma prière.

4.
Je trouve la vie belle,
si merveilleuse et grande,
Moi qui n'ai rien que trop d'amour
Pour irradier le monde
Au feu de ta joie !