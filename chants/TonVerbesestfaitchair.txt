R.
Ton Verbe s'est fait chair ; il a demeuré parmi nous.

1.
Dans la profondeur du silence, au milieu de la nuit,
Ta Parole est venue du ciel, elle a parcouru l'univers.

2.
Et nous avons vu sa Gloire, gloire qu'il tient de toi,
Comme Fils unique, plein de grâce et de vérité.

3.
Gloire à toi, Dieu de l'univers, par Jésus-Christ
Ta Parole éternelle, dans l'unité du Saint-Esprit, Alléluia, Alléluia !