EN CHRIST NOTRE FOI ! EN CHRIST NOTRE FOI !
CHEMINONS DANS LE SEIGNEUR QUI NOUS FAIT VIVRE,
CHRIST, NOTRE JOIE !
SEIGNEUR, GLOIRE À TOI! SEIGNEUR, GLOIRE À TOI!
COMPAGNON SUR NOTRE ROUTE,
Ô CHRIST, GLOIRE À TOI!

1.
Amour qui prends racine en notre terre,
Amour qui tends les bras et nous relèves,
Ton Corps est pain rompu qui nous rassemble,
Ton Sang versé est signe de l’Alliance,
Jésus, Toi notre frère,
Ami qui nous libères,
Christ et Seigneur, affermis notre foi !
Affermis notre foi !

2.
Pardon qui purifies et transfigures,
Pardon qui cicatrises nos blessures,
Ta main veut soulager notre faiblesse,
Marchons sous ton regard plein de tendresse,
Jésus, Toi notre frère,
Ami qui nous libères,
Christ et Seigneur, affermis notre foi !
Affermis notre foi !

3.
Sauveur qui nous invites à l’espérance,
Sauveur qui t’es chargé de nos souffrances,
Ton Coeur ouvert est source de l’eau vive,
Répands sur nous le Souffle qui anime,
Jésus, Toi notre frère,
Ami qui nous libères,
Christ et Seigneur, affermis notre foi !
Affermis notre foi !

4.
Visage de l’amour de notre Père,
Visage qui révèles la Lumière,
Ta mort en Croix délivre des ténèbres
La chair que tu recrées par le baptême.
Jésus, Toi notre frère,
Ami qui nous libères,
Christ et Seigneur, affermis notre foi !
Affermis notre foi !

5.
Ta gloire est l’avenir de tous les hommes,
Ta gloire que la grâce en nous façonne.
Ton Corps est devenu la pierre d’angle,
L’Église où tu accueilles un peuple immense,
Jésus, Toi notre frère,
Ami qui nous libères,
Christ et Seigneur, affermis notre foi !
Affermis notre foi !

6.
Chemin où nous marchons avec Marie,
Chemin où, jeunes, nous cherchons la Vie,
Ta Pâque est le sommet de notre Histoire,
Que toute langue chante ta victoire !
Jésus, Toi notre frère,
Ami qui nous libères,
Christ et Seigneur, affermis notre foi !
Affermis notre foi !