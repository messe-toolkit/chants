MARQUEZ TOUT DU SCEAU DE L’AMOUR.
À CHAQUE INSTANT DU TEMPS QUI PASSE,
MARQUEZ TOUT DU SCEAU DE L’AMOUR,
DIEU DONNE TOUT, RENDEZ-LUI GRÂCE.

1.
En toutes vos nuits, un soleil s’imprime
Chaque fois que vous aimez.
Comme en plein midi, sa clarté domine
Chaque fois que vous aimez.

2.
Aux sentiers de paix, le pardon chemine
Chaque fois que vous aimez.
Voyez l’arc-en-ciel que l’Esprit dessine
Chaque fois que vous aimez.

3.
Dans les cœurs transis, le feu se ranime
Chaque fois que vous aimez.
Dieu se réjouit, et la joie culmine
Chaque fois que vous aimez.