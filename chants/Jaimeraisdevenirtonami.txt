R.
Je voudrais te rencontrer, moi aussi,
J’aimerais devenir ton ami. (bis)

1.
On m’a raconté, en me parlant de toi,
Que tu as été un jeune comme moi ;
Avec tes copains, tu riais, tu jouais ;
En grandissant, tu apprenais, tu priais.
Et puis, un jour, tu as quitté tes parents,
Pour aller, de ville en ville, dire aux gens :
« Je veux chasser la tristesse de vos cœurs,
Je viens apporter l’espoir et le bonheur. »

2.
Quelques jeunes gens, un jour, t’ont rencontré
Ils s’appelaient Jacques et Jean, Pierre ou André.
Ils avaient envie de réussir leur vie ;
Tu leur as montré la route, ils t’ont suivi.
Quand les jeunes et les enfants venaient vers toi,
Tu disais : « surtout, ne les repoussez pas.
On ne comprend rien à l’amour fou de Dieu,
Quand on a l’âme ridée, le cœur trop vieux. »

3.
Tout ça dérangeait les gens bien installés,
Dans leurs habitudes et dans leurs préjugés.
Tes grands horizons ne les attiraient pas.
Ils ont préféré te clouer sur la croix.
Mais, depuis des millions de gens ont compris,
Que, toi seul, tu donnes un sens à notre vie.
Ceux qui, sur leur route, un jour, t’ont rencontré,
Disent tous qu’ils n’ont jamais pu t’oublier.