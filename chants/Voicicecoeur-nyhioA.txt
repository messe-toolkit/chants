R.
Voici ce cœur,
Qui a tant aimé les hommes.
Voici ce cœur,
Qui s’est livré pour le monde.

1.
J’ai aimé Israël dès son enfance,
C’est moi qui lui apprenais à marcher,
Je le soutenais par les bras
Et ils n’ont pas compris que je prenais soin d’eux !

2.
Mon cœur en moi est bouleversé,
Toutes mes entrailles frémissent.
Je n’agirai pas selon l’ardeur de ma colère,
Car je suis Dieu et non pas homme.

3.
Ce n’est pas nous qui avons aimé Dieu
Mais c’est lui qui nous a aimés le premier.
Car Dieu a tant aimé le monde
Qu’il nous a livré son Fils unique.

4.
Venez à moi, vous tous qui peinez
Et je vous procurerai le repos.
Prenez mon joug et mettez-vous à mon école,
Car je suis doux et humble de cœur.

5.
Si quelqu’un a soif, qu’il vienne à moi
Et qu’il boive, celui qui croit en moi,
Selon le mot de l’Écriture :
“De son sein couleront des fleuves d’eau vive.”

6.
Quand les soldats vinrent à Jésus, il était déjà mort.
L’un d’eux avec sa lance lui perça le côté
Et aussitôt il en sortit du sang et de l’eau.
“Ils regarderont vers celui qu’ils ont transpercé.”