Si nous aimons le Seigneur,
Nous resterons fidèles à sa parole.
Le Père nous enverra
L'esprit de vérité.

Alléluia, alléluia,
Dieu est pour toujours avec nous !
Alléluia.

Le Seigneur a fait connaître sa victoire
et révélé sa justice aux nations. (Ps. 97, 2)

Que Dieu nous bénisse,
et que la Terre tout entière l'adore. (Ps. 66, 6)