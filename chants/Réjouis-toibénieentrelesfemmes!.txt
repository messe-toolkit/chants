RÉJOUIS-TOI, BÉNIE ENTRE LES FEMMES !

RÉJOUIS-TOI, BÉNIE ENTRE LES FEMMES,
TON SEIGNEUR EST AVEC TOI !
HEUREUSE ES-TU, CROYANTE INDÉFECTIBLE,
IL ACCOMPLIT CE QU'IL A DIT !

1.
Chante de joie, Marie d'Israël
Car voici qu'il vient !
Chante de joie, Marie d'Israël
Il fait sa demeure en toi !

2.
Réjouis-toi, guetteuse de l'aurore,
Voici que ton roi vient à toi !
Réjouis-toi, guetteuse de l'aurore,
Il porte sa paix aux nations !

3.
Pousse des cris de joie, fille des prophètes,
Pour toi, il exulte de joie !
Pousse des cris de joie, fille des prophètes,
Son amour te couronnera !

4.
Ton cœur soit en fête, enfant de Galilée,
Ton Dieu dansera pour toi !
Ton cœur soit en fête, enfant de Galilée
Il t'entraîne dans son allégresse !