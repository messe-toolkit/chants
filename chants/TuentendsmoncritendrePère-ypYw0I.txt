R.
Tu entends mon cri, tendre Père,
Toi l’infinie miséricorde
Je m’appuie sur toi, je t’espère,
Parle Seigneur, mon coeur est prêt.

1.
Comme l’or au creuset, purifie mon coeur
Ne m’abandonne pas.
Par l’eau et par le feu, renouvelle-moi,
Revêts-moi de ta joie.

2.
Sans crainte devant toi, je remets ma vie,
Ne m’abandonne pas.
Montre moi ton chemin affermis mes pas,
Revêts-moi de ta joie.

3.
Au plus fort de la nuit reste près de moi,
Ne m’abandonne pas.
Je choisis la clarté car j’aime ta loi
Revêts-moi de ta joie.

4.
Attentif à ma voie, tu veilles toujours
Ne m’abandonne pas.
Ma bouche redira sans fin ton amour
Revêts-moi de ta joie.