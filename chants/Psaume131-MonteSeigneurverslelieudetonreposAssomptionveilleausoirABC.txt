MONTE, SEIGNEUR, VERS LE LIEU DE TON REPOS,
TOI, ET L’ARCHE DE TA FORCE !

1.
Entrons dans la demeure de Dieu,
prosternons-nous aux pieds de son trône.
Monte, Seigneur, vers le lieu de ton repos,
toi, et l’arche de ta force !

2.
Que tes prêtres soient vêtus de justice,
que tes fidèles crient de joie !
Pour l’amour de David, ton serviteur,
ne repousse pas la face de ton messie.

3.
Car le Seigneur a fait choix de Sion ;
elle est le séjour qu’il désire :
« Voilà mon repos à tout jamais,
c’est le séjour que j’avais désiré. »