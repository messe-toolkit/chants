1.
Plus de lumière dans les flaques
Plus de rêves sous les plis du temps
Des fantômes se croisent en criant
La guerre a éteint le ciel.

R.
ESSAYER L’AMOUR.
OFFRIR L’IMPOSSIBLE EN SOURIANT
ESSAYER L’AMOUR.
SIMPLEMENT, SANS DÉTOUR.
ESSAYER L’AMOUR.

2.
Le regard droit, il tend la main
De la vie, il se sent exclu
Invisible au coeur des humains
L’espoir a quitté la rue.

3.
La solitude étend ses voiles
Et au loin la mer s’est perdue
Le ciel tombe épuisé d’étoiles
Silence ! Un homme n’est plus.

4.
Des mots s’envolent et font du bruit
Chacun parle à un coeur fermé
Des voix montent sans mélodie
Le temps mesure les regrets.

5.
Des murs se lèvent à Belfast
Murs de pierres à Jérusalem
Murs de honte, les hommes se séparent
La peur fait lever la haine.