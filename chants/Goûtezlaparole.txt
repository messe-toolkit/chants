R.
GOÛTEZ LA PAROLE,
SAVOUREZ LES MOTS DE DIEU ;
PLUS QUE MIEL EN NOTRE
BOUCHE,
MOTS DE SEL ET MOTS DE FEU ;
PAROLE DU SEIGNEUR,
UN CRI DE VÉRITÉ,
PAROLE DU SEIGNEUR,
UN CHANT DE LIBERTÉ !

1.
Parole de lumière
Près de Dieu depuis toujours,
Tu vis avec le Père,
Dans le Souffle de l’Amour.
Ô Verbe fait chair,
Ta demeure est parmi nous,
Dieu saint révélé
Dans l’humain de chaque jour,
Bienheureux qui te reçoit,
Bienheureux qui te donne sa foi !

2.
Parole qui fait vivre
Depuis l’aube de nos temps,
Genèse qui préside
À l’éveil de nos printemps,
Le ciel est rempli
Des merveilles que tu crées,
Tu portes du fruit
Sur la terre où tu te plais.
Bienheureux les yeux ouverts,
Bienheureux, en voyant l’univers !

3.
(en entrée)
Parole en notre Église,
Ton appel nous réunit.
C’est toi qui nous invites,
Nous chantons notre merci.
« Venez et voyez,
Prenez place à mon festin. »
Venez partager
La saveur de ce vrai Pain.
Bienheureux qui entrera,
Bienheureux à la table avec toi !

4.
(méditation)
Parole de sagesse,
Tu annonces le bonheur ;
Fais sourdre la tendresse
Dans le puits de notre coeur.
Prenez mes chemins,
Ils conduisent vers la Vie ;
Criez votre faim
De revivre dans l’Esprit !
Bienheureux qui entendra,
Bienheureux de s’ouvrir à ta voix !

5.
Parole d’espérance,
Tu délivres de la nuit,
Tes mots nous font violence,
Notre flamme rajeunit.
La mort est vaincue,
« Levez-vous, n’ayez pas peur ! »
Le temps est venu
D’être signes du Sauveur !
Bienheureux celui qui croit,
Bienheureux de marcher sur tes pas !

6.
(eucharistie, communion)
Parole qui rend grâce
Dans le peuple des vivants,
Par toi, Seigneur de Pâques,
Notre coeur est tout brûlant.
« Prenez et mangez,
C’est mon corps livré pour vous,
Prenez et buvez,
C’est mon sang versé pour vous. »
Bienheureux qui te prendra,
Bienheureux qui fait corps avec toi !

7.
(service au coeur du monde ; mission)
Parole de justice
Pour le pauvre et l’opprimé,
Tu veux à ton service
Des prophètes libérés :
« Que tombent les liens,
Que les chaînes soient brisées ! »
« Soyez mes témoins »
Dans les luttes pour la paix !
Bienheureux qui le fera,
Bienheureux qui pour toi servira !

8.
Parole pour le monde,
Nous voulons te proclamer.
Partout, qu’ils te répondent,
Ceux qui cherchent à te nommer !
L’Esprit nous envoie
Sur les places et dans les rues,
Qu’il ouvre des voies
Où ton Nom sera connu !
Bienheureux qui vient vers toi,
Bienheureux, car ton Jour brillera !