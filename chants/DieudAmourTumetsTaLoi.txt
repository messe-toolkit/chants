STANCE
Dieu d'amour, tu mets ta Loi
Au profond de nos demeures.
Tu inscris dans notre coeur
Une Alliance pour des siècles.
A jamais tu seras notre Dieu,
A jamais nous serons ton Peuple !

R.
Béni sois-tu, Seigneur !
Ton Esprit nous apprend à te connaître.
Béni sois-tu, Seigneur !
Ton Alliance en Jésus nous renouvelle.

VERSETS

1.
Des plus petits jusqu'aux plus grands,
Tous tes enfants te connaîtront.
Dieu de tendresse et de pardon,
Tu veux sauver tous les vivants.

2.
Du Coeur ouvert une eau jaillit ;
L'esprit nouveau nous est donné.
Louange à toi qui nous recrées !
Ton chant de source est notre vie.