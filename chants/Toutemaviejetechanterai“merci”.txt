R.
TOUTE MA VIE, JE TE CHANTERAI "MERCI".
MERCI, SEIGNEUR, DE TOUT MON COEUR.

1.
Pour tous les pays visités,
les nombreux amis rencontrés ;
Toutes les prières chantées
au long des journées, des veillées.

2.
Pour l'amour reçu et donné,
les peines et les joies partagées.
Pour l'espoir qui naît et renaît
en Jésus venu nous aimer.

3.
Pour la foi de tous les croyants,
de tous les petits et les grands.
Et pour tous les chants des priants
dans les langues des continents.

4.
Pour tant de visages montrés,
de regards connus et aimés.
Et pour ces personnes nommées
nous parlant au coeur d'amitié ;

5.
Pour tous les élans suscités,
les gestes d'amour partagés.
Et pour les regards transformés,
les coeurs et les mains libérés.

6.
Pour les musiciens et auteurs
qui ont dit leur foi au Seigneur.
Pour les guitaristes et chanteurs
qui ont enchantés notre coeur.

7.
Pour ce que demain va offrir
dans le même souci de servir.
Pour ce qu'il faudra découvrir
de l'amour de Dieu à venir.