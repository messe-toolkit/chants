R.
Bientôt de l’autre côté
Voyez le soleil va bientôt se lever
Bientôt de l’autre côté
Bientôt.

1.
J‘entends un cliquetis de chaîne
C’est une entrave à mon espoir
Et ce vieux boulet que je traîne
Ma nuit est bien trop noire
Ma ville est basse au ras du sol
Et ton discours beaucoup trop haut
Mais t’as pas les yeux brûlés d’alcool
Quand tu dis que bientôt.

2.
Je ne vois derrière tous ces murs crasses
Aucun autre côté
Le ciel est déjà blafard
Et ton soleil couché
J’ai la marque gravée sur le front
D’un éternel esclave
J’ai des lèvres de sueurs noires
Et tu veux que je marche.

3.
Je respire à leurs fleurs maudites
Des bouffées de gasoil
Et cette fille-là sur l’affiche
M’offre son cœur beaucoup trop sale
Mais toi t’as pas les yeux qui parlent tristes
Tu dis que moi j’existe bien
Et de l’autre côté de moi-même
Tu veux que j’aille plus loin.

4.
J’ai quitté l’ombre des poubelles
Je ne dors plus parmi les rats
J’ai gribouillé ma liberté
Sur un bout de papier gras
Sur quatre accords jazz étouffé
Un peuple se met en marche
Je ne sais s’ils savent où ils vont
Mais l’essentiel c’est bien qu’ils marchent.