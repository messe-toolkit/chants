R.
Dieu de toute bonté,
Tu es notre Père.
En Jésus le Christ,
Nous sommes tes enfants.

1.
Tu fis le ciel et la terre
Pour nous les donner.
Tu as créé l'univers
Pour notre bonheur.

2.
Tu nous envoies comme un frère,
Ton Fils bien-aimé.
Il a vaincu toute mort
Pour nous libérer.

3.
Tu viens au cœur de l'Histoire,
Pour nous rencontrer.
Tu es le Dieu de l'Alliance,
Toujours avec nous.

4.
Tu fais de nous un seul peuple,
Nous t'avons dit oui.
Tu nous as pris par la main,
Nous n'avons plus peur.

5.
Nous devinons ta présence
Sur tous nos chemins.
Nous avançons en confiance,
Forts de ton amour.