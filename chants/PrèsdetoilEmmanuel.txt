1.
Près de toi, l’Emmanuel,
Nous venons de loin, de très loin ;
Près de toi, dans Bethléem,
Tu nous as montré le chemin.
Une étoile a traversé la nuit ;
Près de toi, Jésus, nous voici.

R.
NOUS SOMMES VENUS T’ADORER,
FILS DE DIEU, NOTRE FRÈRE ;
NOUS SOMMES VENUS T’ADORER,
FILS DE DIEU, NOTRE PAIX.

2.
Près de toi, le seul Sauveur,
Nous venons de loin, de très loin ;
Près de toi qui parles au coeur
Et qui peux combler notre faim.
Ta parole a réveillé la nuit :
Près de toi, Jésus, nous voici.

3.
Près de toi, le Serviteur,
Nous venons de loin, de très loin ;
Près de toi dont les douleurs
Ont brûlé tes pieds et tes mains.
Ton calvaire a-t-il changé la nuit ?
Près de toi, Jésus, nous voici.

4.
Près de toi, le Pain Vivant,
Nous venons de loin, de très loin ;
Près de toi qui nous attends
Pour offrir la joie d’un festin.
Cette table éclaire notre nuit :
Près de toi, Jésus, nous voici.

5.
Près de toi, notre avenir,
Nous venons de loin, de très loin ;
Près de toi qui donnes vie
Quand tu fais souffler l’Esprit Saint.
Notre chant résonne dans la nuit :
Près de toi, Jésus, nous voici.