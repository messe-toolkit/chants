R.
Seigneur, fais de nous des ouvriers de paix ;
Seigneur, fais de nous des bâtisseurs d'amour.

1.
Là où demeure la haine,
Que nous apportions l'amour.
Là où se trouve l'offense,
Que nous mettions le pardon.

2.
Là où grandit la discorde,
Que nous fassions l'unité.
Là où séjourne l'erreur,
Que nous mettions la vérité.

3.
Là où persistent les ténèbres,
Que nous mettions la lumière.
Là où règne la tristesse,
Que nous fassions chanter la joie.

4.
Là où s'attarde le doute,
Que nous apportions la foi.
Sur les chemins du désespoir,
Que nous portions l'espérance.

5.
Donne-nous de consoler,
Plutôt que d'être consolés.
Donne-nous de comprendre,
Plus souvent que d'être compris.

6.
Car il faut savoir donner,
Pour pouvoir être comblés.
Car il faut s'oublier,
Pour pouvoir se retrouver.

7.
Il faut savoir pardonner,
Pour obtenir le pardon.
Il faut apprendre à mourir,
Pour obtenir l'éternelle vie.