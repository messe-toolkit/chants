1.
Tout simplement une prière,
Tout simplement besoin de toi.
Toi qu’on appelle notre Père,
Besoin de me parler de Toi.
TOUT SIMPLEMENT, C’EST MA PRIÈRE,
Le cri de mon coeur jusqu’à toi.

2.
Tout simplement une prière,
Tout simplement désir de toi.
Toi dont le seul nom me libère,
Désir d’entendre enfin ta voix.
Tout simplement, c’est ma prière,
La peur de rester loin de toi.

3.
Tout simplement une prière,
Tout simplement rêver de toi,
Toi qui mets le ciel sur la terre,
Rêver reposer dans tes bras.
Tout simplement, c’est ma prière,
Être heureux d’être aimé de toi.

4.
Tout simplement une prière,
Tout simplement vivre de toi.
Toi le Père de tous les pères,
Vivre ta paix, vivre ta joie.
Tout simplement, c’est ma prière,
Me voici de retour vers toi.

5.
Tout simplement une prière,
Tout simplement écoute-moi.
Toi qu’on appelle notre Père,
Mon coeur a faim mon Dieu de toi.
Tout simplement, c’est ma prière,
Les mots qui me viennent de toi.