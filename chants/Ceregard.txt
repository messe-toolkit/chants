CE REGARD… (bis)
PLEIN D’AMOUR… (bis)
C’EST LE PHARE… (bis)
QUI TOUJOURS… (bis)
ME GUIDE DANS LA VIE… (bis)

1.
Il m’a regardé comme une personne
Il a pris le temps
Il m’a regardé comme personne
L’avait fait avant
Il a mis en moi une Espérance
Un élan pour gagner
Il a mis en moi cette confiance
Qui invite à donner.

2.
Il m’a regardé comme fait l’enfant
Les yeux dans les yeux
Il m’a regardé, me montrant
Dans l’horizon, le bleu
Il a mis en moi un avenir
Une raison d’avancer
Il a mis en moi un devenir
La force d’exister.

3.
Il m’a regardé sans mettre le blâme
Sur mes erreurs passées
Il m’a regardé rallumant la flamme
De mon coeur blessé
Il a mis en moi tellement d’Amour
Tellement de poésie
Il a mis en moi du soleil pour
Redonner l’envie.

4.
Il m’a regardé avec bienveillance
En ouvrant les bras
Il m’a regardé avec l’assurance
D’un Homme de foi
Il a mis en moi cette volonté
Pour aller de l’avant
Il a mis en moi la joie d’aimer
Qui rend l’Homme vivant.