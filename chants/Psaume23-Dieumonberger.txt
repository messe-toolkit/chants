1.
Dieu mon berger me conduit et me garde.
J’entends sa voix et vers lui je regarde.
Il me fait paître en de verts pâturages
au long des eaux, sous la paix des ombrages ;
Et pour qu’en moi son amour s’accomplisse,
il me conduit aux sentiers de justice.

2.
Quand il faudra marcher dans la nuit sombre,
quand de la mort je traverserai l’ombre,
je n’aurai point de peur en ma détresse ;
car tu te tiens auprès de moi sans cesse ;
Même au travers de la vallée obscure
c’est ton bâton, mon Dieu, qui me rassure.

3.
Tu viens dresser la table de la fête,
l’huile odorante a parfumé ma tête,
un vin de joie en ma coupe déborde ;
Nul n’ôtera ces biens que tu m’accordes.
Accompagné chaque jour, d’heure en heure,
dans ta maison je ferai ma demeure.