1.
Au matin, des clameurs de victoire :
Ciel et terre éclatent en cris de joie.
Au son des trompettes et des tambours,
Tout l’univers jubile en ce jour.

R.
Alléluia, le Christ est vivant,
Jésus est vraiment ressuscité !
Alléluia, le Christ est vivant,
Jésus est vraiment ressuscité !

2.
Alléluia, louons le Seigneur,
Jésus a terrassé l’ennemi.
Au milieu de la nuit de la mort,
La lumière du Christ a jailli !

3.
Alléluia la mort est vaincue,
En sa chair, Jésus l’a crucifiée !
Le pouvoir des ténèbres est détruit,
Jésus nous fait le don de sa vie !

P.
La mort est vaincue et l’enfer dévasté,
Jésus, le Seigneur, est ressuscité !