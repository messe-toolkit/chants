DIEU VIENT SUR LA TERRE TOUT SIMPLEMENT,
TOUT SIMPLEMENT, TOUT SIMPLEMENT,
DIEU VIENT SUR LA TERRE TOUT SIMPLEMENT,
TOUT SIMPLEMENT, COMME UN ENFANT.

1.
Pas de palais, comme à Versailles,
Pour tout berceau : un peu de paille,
Pas de draps fins, mais quelques langes,
Un roi est né dans une grange.

2.
Pas de soldats, pas de noblesse,
De courtisans et de princesses,
Un âne gris, un boeuf très doux,
Quelques bergers sont à genoux.

3.
Pas de ministres, pas de fanfares
Pour accueillir le Roi de gloire.
Des pauvres gens, des animaux,
Quelques bergers jouent du pipeau.

4.
Pas de radios, de journalistes,
De caméras, de publicistes,
Pour réussir son arrivée
Dieu a choisi de se cacher.