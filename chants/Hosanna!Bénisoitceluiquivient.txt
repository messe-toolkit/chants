R.
Béni soit celui qui vient au nom du Seigneur ! (bis)

1.
Toi qui viens de Dieu pour le salut des hommes,
Hosanna, hosanna !
Tu leur a montré à quel point Dieu les aime,
Hosanna, hosanna !

2.
Toi qui es venu nous parler de ton père,
Hosanna, hosanna !
Tu nous as montré qu'il est toute tendresse,
Hosanna, hosanna !

3.
Toi qui es venu vivre la vie des hommes,
Hosanna, hosanna !
Tu as accueilli les petits et les pauvres,
Hosanna, hosanna !

4.
Toi qui ne viens pas pour juger les personnes,
Hosanna, hosanna !
Tu as révélé l'amour en Madeleine,
Hosanna, hosanna !

5.
Toi qui ne viens pas pour ceux qui se croient justes,
Hosanna, hosanna !
Tu as pardonné à la femme adultère,
Hosanna, hosanna !

6.
Toi qui es venu pour ceux qui sont perdus,
Hosanna, hosanna !
Tu as mis Zachée sur un chemin de vie,
Hosanna, hosanna !

7.
Toi qui es venu pour ceux qui sont malades,
Hosanna, hosanna !
Tu as mis debout les boiteux, les infirmes,
Hosanna, hosanna !

8.
Toi qui es venu pour être la lumière,
Hosanna, hosanna !
Tu as redonné la Lumière aux aveugles,
Hosanna, hosanna !

9.
Toi qui es pour nous résurrection et vie,
Hosanna, hosanna !
Tu as fais sortir Lazare de son tombeau,
Hosanna, hosanna !

10.
Toi qui es venu pour servir tous les hommes,
Hosanna, hosanna
Tu leur as donné l'exemple du service,
Hosanna, hosanna !

11.
Toi qui es venu pour que tous aient la Vie,
Hosanna, hosanna !
Tu leur as offert ta Vie en nourriture,
Hosanna, hosanna !

12.
Toi qui es venu pour servir tous les hommes,
Hosanna, hosanna !
Tu nous as aimé jusqu'à donner ta vie,
Hosanna, hosanna !

13.
Toi qui t'es montré vivant à tes apôtres,
Hosanna, hosanna !
Tu leur as donné l'esprit de Vérité,
Hosanna, hosanna !

14.
Toi qui es venu nous conduire au royaume,
Hosanna, hosanna !
Tu nous fais passer de la mort à la Vie,
Hosanna, hosanna !