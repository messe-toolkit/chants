ALLÉLUIA, ALLÉLUIA.

Dieu seul est mon rocher, mon salut :
d’en haut, il tend la main pour me saisir,
il me retire du gouffre des eaux.

ALLÉLUIA.