1.
Qui es-tu, Dieu Créateur
D’avoir osé le Monde
Et d’avoir osé l’Homme
Temple de liberté?
Jusqu’où va ton audace
D’élever la poussière
Au rang de ta dignité?
Qui es-tu, Dieu Créateur
Pour avoir voulu l’Homme à tes côtés ?

2.
Qui es-tu, Dieu Créateur
D’avoir osé le Monde
Et d’avoir osé l’Homme
Dans ses diversités ?
Jusqu’où va ton audace
De lui confier la terre
Garant de son unité?
Qui es-tu, Dieu Créateur
De vivre à son sommet l’humilité?

3.
Qui es-tu, Dieu Créateur
D’avoir osé le Monde
Et d’avoir osé l’Homme
Aussi inachevé?
Jusqu’où va ton audace
Ta tendresse de père
Confiant en ton nouveau-né ?
Qui es-tu, Dieu Créateur
Qui conjugue à l’amour l’éternité?