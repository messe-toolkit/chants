BÉNI SOIS-TU DIEU NOTRE PÈRE !
BÉNI SOIS-TU PAR JÉSUS-CHRIST !

1.
Avant le jour du monde
Tu nous avais choisis
EN JÉSUS-CHRIST.
Pour vivre ta présence,
Pour être tes amis.
PAR JÉSUS-CHRIST.

2.
Dans tes vouloirs d’amour
Tu nous as destinés
EN JÉSUS-CHRIST.
À devenir un jour
Enfants de liberté.
PAR JÉSUS-CHRIST.

3.
Merveille que tu donnes
En ton fils bien-aimé
EN JÉSUS-CHRIST.
Par lui tu nous rachètes.
Tu laves nos péchés.
PAR JÉSUS-CHRIST.

4.
Sagesse inépuisable
Tu livres ton secret
EN JÉSUS-CHRIST.
Ramener terre et ciel
Aux sources d’unité.
PAR JÉSUS-CHRIST.

5.
Tu fais de nous ton peuple
Où chante l’espérance
EN JÉSUS-CHRIST.
Porteurs de la nouvelle,
Nous crions délivrance.
PAR JÉSUS-CHRIST.

6.
L’Esprit de la promesse
Nous marque de la vie
EN JÉSUS-CHRIST.
Il ouvre le royaume,
Où l’homme porte fruit.
PAR JÉSUS-CHRIST.