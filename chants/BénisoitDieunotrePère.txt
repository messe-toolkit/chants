ANTIENNE
Béni soit Dieu !
Béni soit Dieu !
Béni soit Dieu notre Père !
Il nous a bénis !
Il nous a bénis !
Il nous a bénis dans le Christ !

R.
À la louange de sa gloire !
Louange et gloire !

1.
Qu'il soit béni, le Dieu et Père de notre Seigneur Jésus, le Christ !
Il nous a bénis et comblés des bénédictions de l'Esprit, au ciel, dans le Christ.

2.
Il nous choisit, dans le Christ, avant que le monde fût créé,
Pour être saints et sans péchés devant sa face grâce à son amour.

3.
Il nous dévoile ainsi le mystère de sa volonté,
Selon que sa bonté l'avait prévu dans le Christ :
Pour mener les temps à leur plénitude,
Récapituler toutes choses dans le Christ,
Celles du ciel et celles de la terre.

4.
En lui aussi, nous avons été mis à part et désignés d'avance,
Selon le projet de celui qui mène toutes choses au gré de sa volonté,
Pour être, à la louange de sa gloire, ceux qui ont d'avance espéré dans le Christ.