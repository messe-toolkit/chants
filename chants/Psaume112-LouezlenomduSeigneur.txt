LOUEZ LE NOM DU SEIGNEUR :
DE LA POUSSIÈRE IL RELÈVE LE FAIBLE.

ou ALLÉLUIA !

1.
Louez, serviteurs du Seigneur,
louez le nom du Seigneur !
Béni soit le nom du Seigneur,
maintenant et pour les siècles des siècles !

2.
Qui est semblable au Seigneur notre Dieu ?
Lui, il siège là-haut.
Mais il abaisse son regard
vers le ciel et vers la terre.

3.
De la poussière il relève le faible,
il retire le pauvre de la cendre
pour qu’il siège parmi les princes,
parmi les princes de son peuple.