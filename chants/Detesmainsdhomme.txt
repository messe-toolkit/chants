LA PAROLE SE GLISSE DANS TES GESTES,
L’AMOUR PREND CORPS JUSQU’AU BOUT DE TES DOIGTS.
IL NOUS EMPORTE DANS TA GRÂCE,
DANS TA GRÂCE.

1.
De tes mains d’Homme
tu viens creuser la source du baptême
et le désert s’emplit d’offrandes.

2.
De tes mains d’Homme
tu viens toucher le front de nos ténèbres
et nous saisir dans ta lumière.

3.
De tes mains d’Homme
tu viens planter un arbre de justice
enraciné sur notre terre.

4.
De tes mains d’Homme
tu viens tenir la mort à sa frontière
clouée au bois de ta souffrance.