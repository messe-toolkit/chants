1.
Comme il est beau ce monde merveilleux, ces beaux vallons, les plaines et les forêts,
Et je me dis : « Bravo pour les couleurs ! », sauvegardons notre planète terre !

2.
Comme il est fort le soleil de midi, ou dans le vent, la pluie sur les chemins,
Et je me dis : « Combien je suis petit ! », sauvegardons notre planète terre !

3.
Comme il triste ce monde pollué, ces marées noires et les forêts brûlées,
Et je me dis : « Il faut que cela change ! », sauvegardons notre planète terre !

4.
Comme il temps qu’ensemble pour demain, tous les enfants respirent l’air marin,
Et je me dis : « Ensemble on peut changer ! », sauvegardons notre planète terre !

5.
Comme il est bon, de protéger la vie, les animaux, les plantes et les humains,
Et je me dis : « Changeons nos habitudes ! », sauvegardons notre planète terre !

6.
Comme il nous faut admirer, s’étonner, de ces trésors qui font vibrer nos coeurs,
Et je me dis : « Ensemble dans le monde », sauvegardons notre planète terre !

7.
Comme un dessin, rempli de mille fleurs, prenons bien soin, soyons tous responsables,
Et je me dis : « Oh habitants du monde », sauvegardons notre planète terre !