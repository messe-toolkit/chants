STANCE
Dieu qui donnes paix,
Dieu de Jésus Christ,
C'est la paix que tu dis
Pour ton peuple et tes fidèles;
Qu'ils ne reviennent plus jamais
À la folie des jours de guerre !
Qu'ils te découvrent Dieu de vie,
Seigneur de la paix infinie !

R.
Vienne sur le monde
La paix de ton Royaume !
Vienne dans nos coeurs
La paix de ton Esprit !

VERSETS
(psaume 84)

1.
Amour et vérité se rencontrent,
justice et paix s'embrassent ;
la vérité germera de la terre
et du ciel se penchera la justice.
au Refrain

2.
Le Seigneur donnera ses bienfaits,
et notre terre donnera son fruit.
La justice marchera devant lui,
et ses pas traceront le chemin.
au Refrain puis Stance et Refrain