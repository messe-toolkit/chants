R.
Donne à la terre un nouveau printemps
Donne à la terre le souffle des vivants.
Comme à Bethléem il y a deux mille ans
Donne à la terre Jésus, ton enfant.

1.
Père, envoie sur nous ta sagesse,
Père, envoie sur nous ta vie.
Nous inventerons enfin
Le monde pour demain !

2.
Père, envoie sur nous ta lumière,
Père, envoie sur nous ton feu.
Nous enflammerons bientôt
Le monde pour demain !

3.
Père, envoie sur nous ton silence,
Père, envoie sur nous ta paix.
Nous déposerons en toi
Le monde pour demain !

4.
Père, envoie sur nous ta parole,
Père, envoie sur nous ta joie.
Nous enchanterons vraiment
Le monde pour demain !