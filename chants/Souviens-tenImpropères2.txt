R.
Oui, tu es mon Seigneur et mon Dieu

1.
Homme souviens-t’en : je suis venu chez toi
te montrer un visage d’enfant
maintenant tu t’approches de moi,
et me craches au visage trente ans
souviens-t’en : je suis venu chez toi
et j’ai ceint à ta guise mes reins
maintenant, tu t’approches de moi,
tu flagelles et me brises les reins
souviens-t’en : je suis venu chez toi
fatiguer sur la planche mes mains.
Maintenant, tu t’approches de moi
et me cloues sur la planche les mains.

2.
Peuple, souviens-t’en : j’ai demeuré chez toi
j’ai levé des litières tes morts
maintenant tu t’empares de moi,
tu me lèves au calvaire le corps
souviens-t’en : j’ai demeuré chez toi
j’ai offert pour tes noces bon vin
maintenant, tu t’empares de moi
et tends le plus atroce des vins
souviens-t’en : j’ai demeuré chez toi,
j’ai guéri tes blessures souvent.
Maintenant, tu t’empares de moi
et tu fends de lance dure mon flanc.

3.
Monde, souviens-t’en : j’ai voulu que chez toi
soient criées l’espérance, la joie
maintenant regarde, me voilà :
il n’y a que silence en crois
souviens-t’en : j’ai voulu que pour toi
soient ouvertes les choses d’en haut
maintenant regarde me voilà :
sur moi la porte est close : tombeau
souviens-t’en : j’ai voulu que de toi
soit aimé le plus pauvre des tiens.
Maintenant regarde, me voilà :
tu ne vois pas le pauvre qui vient.