TOI JÉSUS, NOTRE VIE !

TOI JÉSUS, NOTRE VIE, TU NOUS OUVRES LE CIEL
TA LUMIÈRE A SAISI NOTRE TERRE,
TU ES CHRIST ET SEIGNEUR, VRAI VISAGE DU PÈRE,
TU N'AS POUR PUISSANCE QUE L'AMOUR !

1.
En nos mains, voici le pain de notre terre,
En tes mains, il devient le pain de la vie
Comblant notre faim d'éternité !

2.
Par la croix, tu réconcilies l'Univers,
Révélant l'amour infini de ton Père,
L'Esprit de Dieu pénètre le monde.

3.
Nous voici délivrés de la mort par ta grâce,
Gloire au Père qui nous a donné un tel fils,
Le fils unique en qui nous croyons.

4.
Comme un fleuve, l'Esprit se répand sur le monde,
C'est par lui que justice et paix se retrouvent,
amour et vérité se rencontrent !