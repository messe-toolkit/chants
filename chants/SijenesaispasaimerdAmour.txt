1.
Je peux parler toutes les langues
Avec les peuples de la terre,
Imaginer celle des anges
Et tout savoir de l'univers,

SI JE NE SAIS PAS AIMER D'AMOUR,
TOUT MON SAVOIR NE SERT A RIEN ;
SI JE NE SAIS PAS AIMER D'AMOUR,
CE QUE J'APPRENDS NE SERT A RIEN.

2.
Je peux rêver de tout connaître
Et dévoiler tous les mystères,
Avoir la foi des grands prophètes
Qui fait refleurir le désert,

SI JE NE SAIS PAS AIMER D'AMOUR,
TOUT MON SAVOIR NE SERT A RIEN ;
SI JE NE SAIS PAS AIMER D'AMOUR,
MEME LA FOI NE SERT A RIEN.

3.
Je peux combler de biens les pauvres,
Porter secours à mon prochain,
Livrer mon corps en holocauste,
Donner tout ce qui m'appartient,

SI JE NE SAIS PAS AIMER D'AMOUR,
TOUT MON SAVOIR NE SERT A RIEN ;
SI JE NE SAIS PAS AIMER D'AMOUR,
MEME EN DONNANT JE NE SUIS RIEN.