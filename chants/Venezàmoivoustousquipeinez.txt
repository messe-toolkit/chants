R.
VENEZ À MOI, VOUS TOUS QUI PEINEZ SOUS LE POIDS DU FARDEAU,
ET MOI, JE VOUS PROCURERAI LE REPOS.

1.
Prenez sur vous mon joug,
devenez mes disciples.

2.
Car je suis doux et humble de coeur,
et vous trouverez le repos pour votre âme.