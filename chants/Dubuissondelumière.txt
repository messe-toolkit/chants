1.
Du buisson de lumière
Et de feu,
Une voix
La grande voix de Dieu
Qui l’habite,
Nous révèles sa gloire
Et son nom.

2.
Du buisson de lumière
Et de feu,
Le dessein,
Le grand dessein de Dieu,
Se dévoile :
C’est la pâque du Verbe
Crucifié.

3.
Du buisson de lumière
Et de feu,
Son amour,
Le grand amour de Dieu
Qui l’embrase,
Se répand sur le monde
Par l’Esprit.

4.
Du buisson de lumière
Et de feu,
Paix et joie,
La grande joie de Dieu
Prend sur terre :
La voici de lumière
Et de feu!