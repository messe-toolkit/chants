VERS TOI, SEIGNEUR, J’ÉLÈVE MON ÂME,
VERS TOI, MON DIEU.

1.
Seigneur, enseigne-moi tes voies,
fais-moi connaître ta route.
Dirige-moi par ta vérité, enseigne-moi,
car tu es le Dieu qui me sauve.

2.
Il est droit, il est bon, le Seigneur,
lui qui montre aux pécheurs le chemin.
Sa justice dirige les humbles,
il enseigne aux humbles son chemin.

3.
Les voies du Seigneur sont amour et vérité
pour qui veille à son alliance et à ses lois.
Le secret du Seigneur est pour ceux qui le craignent ;
à ceux-là, il fait connaître son alliance.