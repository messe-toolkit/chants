VIENS SEIGNEUR HABITER NOTRE DOULEUR,
NOURRIR NOS VIES DE TA PRÉSENCE,
OUVRIR NOS COEURS À L’ESPÉRANCE.

1.
Tu nous vois, attristés sur le chemin.
Tu t’approches, sans bruit tu nous rejoins.
Tes mots si doux portent notre chagrin.
Quand tu nous parles, on voit un peu plus loin.

2.
Notre foi assombrie par le chagrin,
Tu l’éclaires au Livre des Anciens ;
Et le Messie devient simple chemin,
Pour qu’à la gloire chacun parvienne enfin.

3.
Le soir vient et le jour baisse déjà.
Entre ici partager notre repas.
Tu romps le pain : nos yeux s’ouvrent à la joie,
Notre coeur brûle, nous savons que c’est toi.