UNE FETE, UNE DANSE
UNE LUMIERE EN NOS MAINS
AU RYTHME DE L’ESPERANCE
FARANDOLE DES TEMOINS

1.
Une fête à inventer dans notre vie
Un jardin dans notre coeur
Pour inviter tes enfants à Te danser
Inviter nos voix à Te chanter

2.
Un accueil à inventer dans notre vie
Un sourire dans notre coeur
Pour inviter un ami à Te connaî tre
Inviter un soleil à renaître

3.
Florilège à inventer dans notre vie
Poésie dans notre coeur
Pour inviter tous nos rêves à naviguer
Inviter la joie à embarquer

4.
Espérance à inventer dans notre vie
Un partage dans notre coeur
Pour inviter nos mains fermées à fleurir
Inviter nos regards à s’ouvrir

5.
Symphonie à inventer dans notre vie
Mélodie dans notre coeur
Pour inviter le monde à ressusciter
Inviter la Lumière à entrer

6.
L’Evangile à retrouver dans notre vie
Sa Présence dans notre coeur
Pour inviter sur la place du marché
Inviter la planète à danser