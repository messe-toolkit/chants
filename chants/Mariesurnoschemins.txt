MARIE, PLEINE DE GRÂCE,
MARIE, NOTRE MÈRE,
ENTENDS MA PRIÈRE.

1.
Marie de nos souffrances,
Marie de nos prisons,
Marie de nos méfiances,
Marie de nos passions,
Marie de nos naissances,
Marie de nos pardons.

2.
Marie de nos semailles,
Marie de nos chemins,
Marie des épousailles,
Marie de nos matins,
Marie de nos batailles,
Marie des nuits sans fin.

3.
Marie de nos victoires,
Marie de nos amours,
Marie de nos espoirs,
Marie des carrefours,
Marie de nos brouillards,
Marie des longs détours.

4.
Marie de nos attentes,
Marie de nos conseils,
Marie de nos tourmentes,
Marie des longues veilles,
Marie des peines ardentes,
Marie de nos réveils.