R.
Bercé par la tendresse de Marie de Joseph,
L’enfant Jésus s’endort et le salut s’éveille
Gloria in excelsis Deo

1.
Toute peur, tout espoir
devant ta paix s’efface,
Plus de trouble en mon cœur,
ta grandeur me dépasse !

2.
N’aie crainte mon enfant
contemple le mystère,
D’un Dieu fait tout petit
pour porter ta misère.

3.
Si pauvre parmi les pauvres,
vêtu d’humilité,
Tu mendies mon amour,
Jésus je veux t’aimer !

4.
Regarde bien ma mère,
écoute son silence,
Toute pleine de grâces
vers Dieu son cœur s’élance.

5.
Seigneur comment te suivre ?
Si petit démuni,
Seul ton regard me sauve
car il est infini !

6.
Contre toute évidence,
je suis ton espérance,
L’Eternel en silence
est entré dans le temps ! 