R.
C´est à nous de prendre sa place aujourd'hui.
Pour que rien de lui ne s'efface.

1.
L'homme qui prit le pain n´est plus devant nos yeux
Pour saisir en ses mains le don de Dieu.

2.
L'homme qui prit le vin n'est plus devant nos yeux
Pour donner en festin l'amour de Dieu.

3.
L'homme qui prit la mort n'est plus devant nos yeux
Pour offrir en son corps le monde à Dieu.

4.
L'homme qui prit tombeau n'est plus devant nos yeux
Pour prouver à nouveau la vie de Dieu.