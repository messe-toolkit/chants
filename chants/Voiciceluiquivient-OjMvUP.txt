R.
Voici celui qui vient au Nom du Seigneur.
Acclamons notre Roi,
Hosanna ! (bis)

1.
Portes, levez vos frontons.
Levez-vous, portes éternelles.
Qu´il entre le Roi de gloire.

2.
Honneur et gloire à ton Nom
Roi des rois, Seigneur des puissances
Jésus, que ton règne vienne.

3.
Venez, rameaux à la main.
Célébrez le Dieu qui vous sauve :
Aujourd´hui s´ouvre son règne.

4.
Jésus, roi d´humilité,
Souviens-toi de nous dans ton règne,
Accueille-nous dans ta gloire.