R.
Marie, toute simple et lumineuse,
Tu reflètes la Trinité.
Viens en nos cœurs pour chanter Dieu,
Mets en nous la paix de Dieu.

1.
Marie, par ta beauté, tu attires le ciel,
En toi Dieu se complaît, l’Esprit en toi descend,
Et le Verbe devient ton enfant aimé.

2.
Marie, mère de grâce, adorante et fidèle,
Toi seule as pénétré le mystère du Fils
Et peut former en nous l’image du Premier-né.

3.
Marie, mère très douce, reine au cœur transpercé.
Notre mère debout, dans la force, la vaillance,
Apprends-nous à souffrir avec notre Sauveur.

4.
Marie, mère très belle, saint reflet de l’Amour,
Louange de gloire parfaite du Dieu trois fois saint,
Vivante porte du ciel au seuil du jour sans fin.