1.
Qui aurait cru qu’un homme, appelé Jésus,
viendrait chez nous d’auprès de Dieu ?
Qui aurait cru qu’un Dieu, en l’Homme Jésus,
habiterait parmi les hommes ?

HEUREUX, HEUREUX, LES YEUX
QUI VOIENT CE QUE VOUS VOYEZ !
HEUREUX, HEUREUX, TOUS CEUX
QUI CROIENT SANS AVOIR VU !

2.
Qui aurait cru qu’un homme, appelé Jésus,
nous parlerait au nom de Dieu ?
Qui aurait cru qu’un Dieu, en l’Homme Jésus,
ferait alliance avec les hommes ?

3.
Qui aurait cru qu’un homme, appelé Jésus,
nous ouvrirait les bras de Dieu ?
Qui aurait cru qu’un Dieu, en l’Homme Jésus,
se livrerait aux mains des hommes ?

4.
Qui aurait cru qu’un homme, appelé Jésus,
ferait de nous des fils de Dieu ?
Qui aurait cru qu’un Dieu, en l’Homme Jésus,
mettrait sa joie au coeur des hommes ?

5.
Qui aurait cru qu’un homme, appelé Jésus,
serait pour nous le Pain de Dieu ?
Qui aurait cru qu’un Dieu, en l’Homme Jésus,
apaiserait la faim des hommes ?

6.
Nous avons cru qu’un homme, appelé Jésus,
est dans la gloire auprès de Dieu ;
Nous avons cru qu’un Dieu, en l’Homme Jésus,
est aujourd’hui parmi les hommes.