1.
On peut vivre sans richesses, presque sans le sou,
Des seigneurs et des princesses, y'en a plus beaucoup,
Mais vivre sans tendresse, on ne le pourrait pas,
Non, non, non, non, on ne le pourrait pas.

2.
On peut vivre sans la gloire qui ne prouve rien,
Être inconnu dans l'histoire et s'en trouver bien,
Mais vivre sans tendresse, il n'en est pas question,
Non, non, non, non, il n'en est pas question.

3.
Quelle douce faiblesse, quel joli sentiment
Ce besoin de tendresse qui nous vient en naissant
Vraiment, vraiment, vraiment…
Dans le feu de la jeunesse naissent les plaisirs,
Et l'amour fait des prouesses pour nous éblouir,
Oui mais sans la tendresse, l'amour ne serait rien,
Non, non, non, non, l'amour ne serait rien.

4.
Un enfant nous embrasse, parc'qu'on le rend heureux
Tous nos chagrins s'effacent, on a les larmes aux yeux
Mon Dieu, mon Dieu, mon Dieu…
Dans votre immense sagesse, immense ferveur,
Faites donc pleuvoir sans cesse au fond de nos cœurs,
Des torrents de tendresse, pour que règne l'amour,
Règne l'amour, jusqu'à la fin des jours.