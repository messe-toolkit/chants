R.
Jésus-Christ, splendeur de la gloire du Père,
Effigie de sa substance, soutenant l'univers par sa Parole puissante,
Accomplissant la purification des péchés,
A daigné aujourd'hui apparaître dans sa gloire sur la montagne sainte.

1.
Le Nord et le Midi, c'est toi qui les créas ;
Le Thabor et l'Hermon à ton nom crient de joie ;
Heureux le peuple qui sait acclamation ;
À la clarté de ta face ils iront sur la montagne sainte.

2.
Survint une nuée suit les prit sous son ombre.
Et de la nuée sortit une voix.
« Celui-ci est mon Fils, mon élu, écoutez sa Parole. »