R.
QU'ELLE EST LONGUE LA FARANDOLE,
VOICI LA RONDE DES PRENOMS
POUR TOI, L'AMI JESUS.

1.
Agathe court à quat' pattes
Edouard dort comme un loir
Ignace coiff' sa tignasse
Oscar nag' dans la mar'.

2.
Pascale range des malles
Grégoir' frott' le miroir
Suzanne boit sa tisane
Thomas croqu' du nougat.

3.
Jérôme traqu' les fantômes
Bruno taille un pipeau
Carole briqu' les cass'roles
Nestor rêv' de castors.

4.
Fanette pouss' la brouette
Daniel mang' tout le miel
Hélène pêch' la baleine
Noël mont' à l'échelle.

5.
Camille cueill' des myrtilles
Cyril fronc' les sourcils
Alice joue à l'actrice
Karim grimp' sur les cîm'.