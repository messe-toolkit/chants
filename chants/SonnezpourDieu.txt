R.
ALLÉLUIA, ALLÉLUIA,
VIBREZ DE JOIE POUR LE SEIGNEUR!
ALLÉLUIA, ALLÉLUIA,
FÊTEZ L’AMOUR DU DIEU SAUVEUR! (bis)

1.
Sonnez pour Dieu l’alléluia,
Chantez la paix sur notre ville !
Sonnez pour Dieu l’alléluia,
Lancez l’appel à faire Église !
Joyeux réveil, alléluia !
Brisons le mur de nos silences !
Joyeux réveil, alléluia,
Cherchons Celui qui nous rassemble !

2.
Sonnez pour Dieu, l’alléluia,
Chantez plein ciel qu’il est dimanche!
Sonnez pour Dieu l’alléluia,
Ouvrez les coeurs à sa louange !
Carillonnez, alléluia !
La Pâque luit sur notre terre.
Carillonnez, alléluia !
C’est jour de grâce et de lumière.

3.
Sonnez pour Dieu l’alléluia,
Chantez au loin votre message !
Sonnez pour Dieu l’alléluia,
Rythmez le temps de ses passages !
Dans sa demeure, alléluia,
Jésus le Maître nous invite.
Dans sa demeure, alléluia,
L’Agneau pascal nous ressuscite !

4.
Sonnez pour Dieu l’alléluia,
Chantez l’écho de notre monde !
Sonnez pour Dieu l’alléluia,
Portez la voix de nos réponses :
Des mots d’espoir, alléluia,
Plus forts que toutes les tempêtes ;
Des mots d’espoir, alléluia,
Pour que nos vies soient une fête !