ANTIENNE
Il est l’agneau et le pasteur,
Il est le roi, le serviteur !

1.
Le Seigneur est mon berger :
Je ne manque de rien.

2.
Sur des prés d’herbe fraîche,
Il me fait reposer.

3.
Il me mène vers les eaux tranquilles
Et me fait revivre ;
Il me conduit par le juste chemin
Pour l’honneur de son nom.

4.
Si je traverse les ravins de la mort,
Je ne crains aucun mal,
Car tu es avec moi :
Ton bâton me guide et me rassure.

5.
Tu prépares la table pour moi
Devant mes ennemis ;
Tu répands le parfum sur ma tête,
Ma coupe est débordante.

6.
Grâce et bonheur m’accompagnent
Tous les jours de ma vie ;
J’habiterai la maison du Seigneur
Pour la durée de mes jours.