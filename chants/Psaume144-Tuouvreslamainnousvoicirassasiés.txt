TU OUVRES LA MAIN:
NOUS VOICI RASSASIÉS.

1.
Que tes oeuvres, Seigneur, te rendent grâce
et que tes fidèles te bénissent !
Ils diront la gloire de ton règne,
ils parleront de tes exploits.

2.
Les yeux sur toi, tous, ils espèrent :
tu leur donnes la nourriture au temps voulu ;
tu ouvres ta main :
tu rassasies avec bonté tout ce qui vit.

3.
Le Seigneur est juste en toutes ses voies,
fidèle en tout ce qu’il fait.
Il est proche de ceux qui l’invoquent,
de tous ceux qui l’invoquent en vérité.