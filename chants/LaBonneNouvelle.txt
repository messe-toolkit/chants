R.
La Bonne Nouvelle est annoncée aux pauvres !
Le Seigneur sauve son peuple, Alléluia

1.
Venez à moi vous tous qui peinez,
Venez à moi vous tous qui souffrez :
En moi vous trouverez le repos et la paix.

2.
Venez à moi au pied de la croix :
Avec Marie, la mère de Dieu,
Amis, vous trouverez le salut et la vie.

3.
Venez à moi, vous tous qui pleurez.
Votre douleur un jour finira :
En moi, vous trouverez le bonheur et la joie.

4.
Voici venir le Règne de Dieu :
Ne craignez plus, soyez dans la joie.
Amis, plus de tristesse, en vos cœurs, aujourd'hui !

5.
Proclamez-le dans tout l'univers :
Dieu est fidèle dans son amour.
Pour nous, il fait merveille et nous donne sa joie.

6.
Et maintenant, soyons les témoins
De Jésus Christ, amour infini.
En lui nous revivons le mystère pascal.

7.
Si nous souffrons avec Jésus Christ,
Nous régnerons avec Jésus Christ :
Il est notre salut, notre espoir, notre vie.