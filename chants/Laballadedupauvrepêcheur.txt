1.
C’était un très pauvre pêcheur
Qui n’avait rien que son bateau
Point de foyer, point de hameau
Rien qu’un filet, et puis son coeur
Mais pour accomgner sa peine
Il avait trouvé comme amie
Une enfant sans amour ni veine
Et toujours seule dans la vie
Quand la mer était bell’ l’enfant chantait
Quand la mer était fort’ l’enfant riait
Quand la mer était foll’ l’enfant pleurait pleurait.

2.
Le soir quand ils quittaient le port
Sur l’onde sombre des grands fonds
Ils laissaient un sillage d’or
Qui dessinait une chanson.
Le vieux filet ne ramenait
Jamais ni bête, ni poisson
Mais les étoiles qu’il pêchait
Étaient plus riche cargaison.
Quand la pêche était bonn’ l’enfant chantait
Quand la pêche était rud’ l’enfant riait
Quand la pêch’ était pauvr’ l’enfant pleurait pleurait.

3.
Un soir de tonnerre et d’éclairs
Dans la tempête a disparu
La gosse avec ses yeux trop clairs
Et le pêcheur ne rentra plus.
Le jour, la nuit, l’été, l’hiver
Son bateau cherche au gré des vagues
Dans quels jardins perdus en mer
L’enfant danse au milieu des algues.
Quand la pêche était rich’ l’enfant chantait
Quand la pêche était bell’ l’enfant riait
Quand la nuit était pur’ l’enfant dansait dansait.
Mais le bateau n’a plus de voiles
Et le filet n’a plus d’étoiles.