ALLÉ, ALLÉLUIA, ALLÉ, ALLÉLUIA
ALLÉ, ALLÉLUIA, ALLÉLUIA, ALLÉLUIA

Toi, petit enfant,
tu seras appelé prophète du Très-Haut :
tu marcheras devant, en présence du Seigneur,
et tu prépareras ses chemins.