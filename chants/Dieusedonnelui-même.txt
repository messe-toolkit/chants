R.
Mon cœur et ma chair sont un cri vers le Dieu de vie?!
Mon cœur et ma chair sont un cri vers le Dieu vivant?!

1.
Dieu se donne lui-même,
Dieu nous donne la vie?;
Dieu se donne lui-même,
Dieu nous donne l'Esprit?;
Dieu se donne lui-même,
Créateur de la vie?!

2.
Eau vive qui donne la vie,
Eau qui vivifie les eaux mortes?;
Eau vive qui vient de la vie,
Eau qui purifie l'eau stagnante.
Eau vive qui rend à la vie,
Eau qui réveille l'eau dormante?!

3.
Messie de Dieu,
Plongé dans l'eau du Jourdain?;
Messie de Dieu,
Sur qui repose l'Esprit?:
Messie de Dieu,
Consacré prêtre à jamais?!

4.
Sauveur du monde,
Il arrache à l'esclavage tous les peuples?;
Sauveur du monde,
Il apporte le salut à tous les hommes.
Sauveur du monde,
Il annonce à toute chair une espérance?!

5.
Celui qui boira de cette eau
Ne connaîtra jamais la soif.
Celui qui boira de cette eau
Reçoit l'Esprit qui le fait vivre.
Celui qui boira de cette eau
Deviendra source jaillissante?!