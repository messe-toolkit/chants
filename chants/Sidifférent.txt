1.
Toi, tu te crois moins bien
Pas assez comme-ci ou comme-ça
On dit que t’es un moins que rien
Que personne ne compte sur toi
Alors j’te vois souvent pleurer
Mais sèche tes larmes
Si t’es pas standardisé
Ta différence est ton arme

NOS DIFFÉRENCES SONT LÀ, NE LES NIE PAS
LE MONDE A PLUS DE COEUR, AVEC TANT DE COULEURS
ÊTRE ENFIN SOI-MÊME, POUR NE PLUS ÊTRE LE MÊME

2.
Y’en a qui enferment leur coeur
Dans des châteaux forts
L’inconnu leur fait peur
Y’en a d’autres qui se croient plus forts
Qui se prennent pour des rois
Ils te regardent mais de haut
Tant de choses qu’ils ne voient pas
Montre leur ce que tu vaux

3.
Y’en a qui s’permettent de juger
Critiquer, cracher au visage
T’es pas assez copié-collé
Par rapport à leur image
Mais montre leur l’amour
Le tien, le mien, le leur
Celui de là-haut pour toujours
Afin que germe le bonheur