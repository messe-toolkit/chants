1.
Ô viens, Sagesse éternelle,
Repos et Gloire de Dieu ;
Par toi il porte les siècles,
Créant la terre et les cieux.
Seigneur, la terre entière
Regarde à l'Orient ;
Fais luire la lumière
De ton avènement !

2.
Descends, Nuée lumineuse,
Guider ton peuple au désert ;
Chez nous fixant ta demeure,
Viens prendre en toi notre chair.

3.
Paradis, Soleil de justice,
Brisant les liens de la mort ;
Qu'enfin sur nous resplendisse
L'amour du Dieu juste et fort !

4.
Gardien de la Cité sainte,
Jésus, vrai roi d'Israël,
Toi seul nous ouvres l'enceinte,
Toi seul nous donnes le ciel.