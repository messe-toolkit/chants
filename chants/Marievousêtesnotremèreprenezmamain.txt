1.
Marie, vous êtes une Lumière
Pour nous montrer le Vrai chemin.
Puisque vous êtes la première,
Gardez ma main dans votre main.

MARIE VOUS ÊTES NOTRE MÈRE,
PRENEZ MA MAIN.
PUISQUE VOUS ÊTES LA PREMIÈRE
SUR MON CHEMIN.

2.
Marie, accueillez ma prière,
Puisque vous êtes le vrai Lien.
Puisque vous êtes la première
Entre l’humain et le Divin.

3.
Marie, à Cana, quel mystère,
Lorsque se change l’eau en vin !
Ainsi vous êtes la première
À prendre en main notre destin.