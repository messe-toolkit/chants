1.
Si tu choisis les chemins de Dieu
quand tu combats la souffrance,
si tu choisis la douceur de Dieu
pour étouffer la violence,
Alors la paix viendra,
Fragile au fond de toi,
Alors la paix viendra
Et Dieu sera ta joie !

2.
Si tu choisis les chemins de Dieu
loin des sentiers de la guerre,
si tu choisis la douceur de Dieu
pour empêcher la colère,
Alors la paix viendra,
Fragile au fond de toi,
Alors la paix viendra
Et Dieu sera ta joie !