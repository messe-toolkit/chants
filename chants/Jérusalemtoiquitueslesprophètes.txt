JÉRUSALEM, JÉRUSALEM, TOI QUI TUES LES PROPHÈTES,
TU N’AS PAS RECONNU LE TEMPS DE TA VISITE !

1.
Nous tournons notre visage vers toi, Seigneur,
Nous t’implorons dans le jeûne et la supplication :
non en raison de nos oeuvres mais de ta seule miséricorde,
car ton nom est invoqué sur ton peuple.

2.
Toi, Seigneur, qui gardes l’alliance pour ceux qui t’aiment,
écoute la prière de ton peuple qui crie vers toi !
Nous avons péché, nous avons commis le mal,
nous nous sommes détournés loin de ta parole.

3.
Nous n’avons pas écouté tes serviteurs les prophètes,
par qui tu nous appelais à nous tourner vers toi.
À toi, mon Dieu, la justice, à nous la honte au visage,
car nous avons péché contre toi, Seigneur !

4.
À toi, Seigneur, la miséricorde et le pardon !
Tu nous as châtiés à cause de nos fautes,
et nous n’avons pas cherché à apaiser ta face
en revenant de tout coeur à ta vérité.

5.
Toi qui as délivré ton peuple de l’Égypte,
par la force de ton bras et par ta main puissante,
détourne ta colère et ta fureur de Jérusalem,
car la détresse qui pèse sur nous est trop lourde.

6.
Ouvre les yeux, Seigneur, et vois notre désolation !
Que ta face illumine ta ville sainte de sa lumière !
Seigneur, écoute ! ’ Seigneur, pardonne !
Seigneur, ne tarde pas !

7.
Gloire à toi, Père des miséricordes,
tu as fait resplendir sur ton peuple le nom de Jésus, ton Fils ;
par ton Esprit, tu fais jaillir en nous le repentir :
gloire et louange à Dieu dans les siècles des siècles !