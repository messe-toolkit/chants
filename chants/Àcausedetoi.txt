1.
Tu me cherchais depuis longtemps,
Je t’ai trouvé de temps en temps,
Je rêvais d’autre chose
Que d’une vie en rose !
Sur le foutoir de mes envies,
Mes amitiés de comédie,
Ta Parole a fait mouche
Ton Amour a fait souche !

C’EST À CAUSE DE TOI
QUE L’ON M’A MIS AU BAN
DE CEUX QUI GAGNENT
C’EST À CAUSE DE TOI
QUE J’AI RATÉ LES MÂTS
DE MES COCAGNES
OUI C’EST D’AVOIR CÉDÉ SOUVENT
À TON AMOUR, COMME UN TORRENT!

2.
Jour après jour, tu m’as donné
La chance d’être bousculé
Par tous ceux qui m’entourent,
Par des mots qui labourent.
J’ai vu renaître au fil de toi,
Jusqu’aux musiques dans ma voix
Ta Parole est promesse,
Ton Amour est caresse !

MAIS C’EST À CAUSE DE TOI
…

3.
J’ai mis mes pas et mes pensées
Dans le ton que tu m’as soufflé
Pour chanter mes “je t’aime”,
Te les dire en poèmes.
Et je découvre ton miroir
Dans le miracle d’un regard.
Quand ta Parole change
Quand ton Amour dérange !

Coda / Refrain
(CAR) C’EST À CAUSE DE TOI…
QUE J’AI FORCÉ LES RANGS
DE CEUX QUI GAGNENT
CAR C’EST À CAUSE DE TOI
QUE J’AI GRAVIS LES MÂTS
DE MES COCAGNES
OUI C’EST D’AVOIR CÉDÉ SOUVENT
À TON AMOUR, COMME UN TORRENT!