R.
PAROLE ET LUMIÈRE QUI ÉCLAIRENT LE MONDE
PAROLE ET LUMIÈRE JUSQU’AU COEUR DE NOS VIES
PAROLE ET LUMIÈRE SOURCE DE JOIE PROFONDE
OH OUI! C’EST TOI DIEU, AUJOURD’HUI.

Le peuple choisi

1.
Quarante ans au désert
Et pour quarante années
La manne du matin
Les tables de la Loi
La source de la vie
Qui jaillit du rocher
Mon Dieu je me souviens
Lorsque j’ai faim de toi.

Le Fils

2.
Tu te fais l’un de nous
En terre de Galilée
Et tu donnes ta vie
Aimant jusqu’à la croix
Un sang d’homme nouveau
Coule au creux de la plaie
Mon Dieu je me souviens
Lorsque j’ai soif de toi.

L’Église

3.
Ta parole est un pont
Entre nous et le ciel
Ton Esprit Ô Seigneur
Affermit notre foi
Nous voici rassemblés
Nourris de ton soleil
Mon Dieu je me souviens
Lorsque j’espère en toi.

Chacun de nous

4.
De ma vie naît un chant
Et si parfois je crie
Tu me murmures encor
Que tu es avec moi
Ainsi le serviteur
A fait place à l’ami
Mon Dieu je me souviens
Lorsque je viens vers toi.