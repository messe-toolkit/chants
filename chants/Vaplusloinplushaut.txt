R.
VA PLUS LOIN, PLUS HAUT
ET CHERCHE L’ÉTINCELLE
QUI BRILLE DANS TES LIENS
VA PLUS LOIN, PLUS HAUT
ET GARDE LA MÉMOIRE
DE CE FEU QUI T’ÉTREINT

1.
Revois donc cet ami
Qui peu à peu s’isole
Qui peine sous la vie
Et lentement s’immole
Regagne sa confiance
Doucement et sans bruit
Entends ses confidences
Ses nuits.

2.
Rejoins les opprimés
Au fond de leur prison
Les derniers des derniers
Les parias les “sans nom”
Réinvente l’Amour
Pour tous ces mal aimés
Sois le soleil des jours
Blessés.

3.
Ne t’arrête jamais
Sur un premier regard
Sur le moindre méfait
D’un paumé, d’un routard
Comprends sa dépendance
Ses refus, ses retraits
Et sois comme une chance
De paix.

4.
Accueille tous chemins
Même les “sans issue”
Avance et prends la main
De ceux qui n’en peuvent plus
Ne choisis pas ton camp
Parmi ceux qui vont bien
Sois proche des perdants
Sans fin.