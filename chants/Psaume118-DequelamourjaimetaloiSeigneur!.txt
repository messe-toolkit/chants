R.
De quel amour j'aime ta loi, Seigneur !

1.
Mon partage, Seigneur, je l’ai dit,
c’est d’observer tes paroles.
Mon bonheur, c’est la loi de ta bouche,
plus qu’un monceau d’or ou d’argent.

2.
Que j’aie pour consolation ton amour
selon tes promesses à ton serviteur !
Que vienne à moi ta tendresse, et je vivrai :
ta loi fait mon plaisir.

3.
Aussi j’aime tes volontés,
plus que l’or le plus précieux.
Je me règle sur chacun de tes préceptes,
je hais tout chemin de mensonge.

4.
Quelle merveille, tes exigences,
aussi mon âme les garde !
Déchiffrer ta parole illumine,
et les simples comprennent.
