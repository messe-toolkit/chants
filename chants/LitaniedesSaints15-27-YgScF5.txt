Kyrie eleison, Kyrie eleison
Christe eleison, Christe eleison
Kyrie eleison, Kyrie eleison

1.
Sainte Marie, Mère de Dieu,
Tous : Priez pour nous !
Saint Michel, saints Anges de Dieu
Tous : Priez pour nous !
Saint Jean Baptiste, saint Joseph
Tous : Priez pour nous !

2.
Saint Pierre et saint Paul,
Tous : Priez pour nous !
Saint André, saint Jean,
Tous : Priez pour nous !
Sainte Marie Madeleine,
Tous : Priez pour nous !

3.
Saint Etienne,
Tous : Priez pour nous !
Saint Ignace d'Antioche, saint Laurent,
Tous : Priez pour nous !
Sainte Perpétue et sainte Félicité, sainte Agnès,
Tous : Priez pour nous !

4.
Saint Grégoire, saint Augustin,
Tous : Priez pour nous !
Saint Athanase, saint Basile
Tous : Priez pour nous !
Saint Martin, saint Benoît
Tous : Priez pour nous !

5.
Saint François et saint Dominique,
Tous : Priez pour nous !
Saint François-Xavier,
Tous : Priez pour nous !
Saint Jean-Marie Vianney,
Tous : Priez pour nous !

6.
Sainte Catherine de Sienne,
Tous : Priez pour nous !
Saint Thérèse d'Avila,
Tous : Priez pour nous !
Vous tous, saints et saintes de Dieu,
Tous : Priez pour nous !

Si baptême, ordination, consécration ou profession religieuse
On peut terminer la partie précédente sans ajouter
Et vous tous, saints et saintes de Dieu
Dans ce cas on rajoute le "couplet" suivant.

7.
Saints (suivent les saints patrons pas encore nommés dans la litanie)
Tous : Priez pour nous !
Idem
Tous : Priez pour nous !
Vous tous, saints et saintes de Dieu
Tous : Priez pour nous !

Montre-toi favorable,
Tous : Délivre-nous, Seigneur !
De tout mal,
Tous : Délivre-nous, Seigneur !
De tout péché,
Tous : Délivre-nous, Seigneur !
De la mort éternelle,
Tous : Délivre-nous, Seigneur !

Par ton Incarnation,
Tous : Délivre-nous, Seigneur !
Par ta mort et par ta Résurrection,
Tous : Délivre-nous, Seigneur !
Par le don de l'Esprit Saint,
Tous : Délivre-nous, Seigneur !

Nous qui sommes pécheurs,
Tous : De grâce, écoute-nous !

(S'il y a des baptêmes : )

Pour qu'il te plaise de faire vivre de ta vie
Ceux que tu appelles au baptême,
Tous : De grâce, écoute-nous !

(Si l'on bénit l'eau baptismale,
Sans toutefois célébrer de baptême : )

Pour qu'il te plaise de sanctifier cette eau
D'où naîtront pour toi de nouveaux enfants,
Tous : De grâce, écoute-nous !

Jésus, Fils du Dieu vivant,
Tous : De grâce, écoute-nous !

Ô Christ, écoute-nous,
Tous : Ô Christ, écoute-nous !

Ô Christ, exauce-nous,
Tous : Ô Christ, exauce-nous !