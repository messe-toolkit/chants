R.
JE VIENS VERS TOI DU LOINTAIN DE MA NUIT,
JE VEUX TE VOIR, SEIGNEUR, AUJOURD’HUI!
JE VIENS VERS TOI AVEC CE QUE JE SUIS,
FAIS QUE JE VOIE, AIE PITIÉ DE MOI.
FAIS QUE JE VOIE, AIE PITIÉ DE MOI.

1.
Pitié pour moi dans ton amour,
Je ne sais plus qui je suis,
Change mon coeur, fais que je voie,
Ouvre mes yeux et je vivrai.

2.
Pitié pour moi dans ton amour,
Je ne sais plus où je vais,
Change mon coeur, fais que je voie,
Ouvre mes yeux et je vivrai.

3.
Pitié pour moi dans ton amour,
Je ne sais plus partager,
Change mon coeur, fais que je voie,
Ouvre mes yeux et je vivrai.

4.
Pitié pour moi dans ton amour,
Je ne sais plus pardonner,
Change mon coeur, fais que je voie,
Ouvre mes yeux et je vivrai.

5.
Pitié pour moi dans ton amour,
Je ne sais plus écouter,
Change mon coeur, fais que je voie,
Ouvre mes yeux et je vivrai.