Antienne
L’ESPRIT DU SEIGNEUR A REMPLI L’UNIVERS,
ET LA TERRE ENTIÈRE BRÛLE DE SON FEU.

1.
Bénis le Seigneur, ô mon âme ;
Seigneur mon Dieu, tu es si grand !
Quelle profusion dans tes oeuvres, Seigneur !
La terre est remplie de tes créations.

2.
Tu reprends leur souffle, elles expirent
et retournent à leur poussière.
Tu envoies ton souffle : elles sont créées ;
tu renouvelles la face de la terre.

3.
Gloire au Seigneur à tout jamais !
Que Dieu se réjouisse en ses oeuvres !
Que mon poème lui soit agréable ;
moi, je me réjouis dans le Seigneur.