Stance
L'heure est venue pour le Maître
de passer du monde à son Père :
" Je ne vous quitte pas,
je vous trace la route
et vous prépare une demeure.
Vous me suivrez
pour partager ma joie. "

R.
Ô Christ ressuscité,
tu es le chemin, la vérité, la vie.

Versets
1° Vous savez où je vais,
vous en connaissez le chemin.
2° Depuis si longtemps je suis avec vous :
qui m'a vu a vu le Père.
3° Croyez-moi : je suis dans le Père
et le Père est en moi.