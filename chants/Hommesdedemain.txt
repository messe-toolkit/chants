R.
Hommes de demain
C’est pour vous que je chante
Hommes de demain
C’est pour vous ce refrain
Hommes de demain
C’est pour vous que je chante
Hommes de demain
C’est pour vous ce refrain.

1.
Mais vous hommes d’aujourd’hui
Écoutez bien ce cri
« Allez ranger vos bombes
Aux catacombes de la paix »
Mais vous hommes d’aujourd’hui
Accueillez donc la vie
Cadeau toujours offert
Aux mendiants de la terre.

2.
Mais vous hommes d’aujourd’hui
Écoutez bien ce cri :
« Accrochez donc vos armes
La ceinture de la paix »,
Mais vous hommes d’aujourd’hui,
Recueillez donc la vie,
Une source jaillit
Au secret de la terre.

3.
Mais vous, hommes d’aujourd’hui,
Écoutez bien ce cri :
« Désarmez donc vos guerres
Dans le vestiaire de la paix »,
Mais vous hommes d’aujourd’hui,
Habillez donc la vie,
Manteau toujours ouvert
Aux pauvres de la terre.