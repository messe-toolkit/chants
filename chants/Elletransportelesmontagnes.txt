1.
Elle transporte les montagnes
À la rencontre du Seigneur,
Marie, en toute hâte,
Elle qui porte dans sa chair,
Immobile,
L’axe du monde,
Le coeur profond de l’univers.

2.
Elle est le lieu de la Présence,
L’arche vivante du Vivant,
Marie, la toute sainte,
Elle qui n’offre rien à voir,
Ô merveille,
Que son attente,
Une espérance emplie de joie.

3.
Elle est venue comme servante,
Et pour la grâce d’un salut,
Marie, la toute proche,
Elle qui porte dans sa voix,
Indicible,
L’écho du Verbe,
L’Esprit de Dieu qu’on ne voit pas.

4.
En donnant chair à l’Invisible,
Elle est la mère de la foi,
Marie, pour tous les âges,
Elle qui marche en éclaireur,
Portant l’ombre
Vers la lumière,
Toute la terre à son Sauveur.