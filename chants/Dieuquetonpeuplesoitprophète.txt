R.
Dieu, que ton peuple soit prophète !
Tu l’as marqué de ton Esprit :
Souffle puissant que rien n’arrête,
Grande est son oeuvre en toute vie,
En toute vie !

1.
Bien au-delà de nos maisons
Ton Vent travaille notre terre.
Il nous libère des démons
Qui créent des murs et des frontières.
Nos yeux découvrent l’univers
De ceux qui ont le coeur ouvert.

2.
Heureux celui qui donne l’eau
À qui proclame ta Parole !
Il connaîtra le chant nouveau
Des invités dans ton Royaume;
Ton Fils Jésus l’accueillera,
En lui l’eau vive jaillira.

3.
Ta loi d’amour nous mènera
Par des chemins qui déconcertent.
Comment, Seigneur, quitter la voie
Du mal qui blesse et nous entraîne ?
Accorde-nous de tout risquer
Pour te servir en vérité.