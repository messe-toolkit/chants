R.
Seigneur, nous marchons dans la nuit !

1.
Pourquoi craignez-vous
Hommes de peu de foi ?
Ne fallait-il pas
Immoler l'Agneau
Pour manger cette Pâque ?

2.
Pourquoi craignez-vous
Hommes de peu de foi ?
Ne fallait-il pas
Traverser la mort
Pour entrer dans la gloire ?

3.
Pourquoi craignez-vous
Hommes de peu de foi ?
Ne fallait-il pas
Vous donner le pain
Qui me fait reconnaître ?

4.
Pourquoi craignez-vous
Hommes de peu de foi ?
Ne fallait-il pas
Que je rende vie
À tous ceux qui espèrent ?