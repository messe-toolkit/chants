Proclamons le mystère de la foi :
Gloire à Toi qui étais mort,
Gloire à Toi qui es vivant,
Notre Sauveur et notre Dieu :
Viens, Seigneur Jésus !