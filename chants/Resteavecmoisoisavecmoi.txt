1.
Reste avec moi
Sois avec moi
Sois tout au fond de moi
Sois au-devant de moi
Sois aussi derrière moi
Reste pour m’apaiser
Pour me calmer, pour me soigner
Pour me réconforter
Reste avec moi.

2.
Reste avec moi
Sois avec moi
Sois tout autour de moi
Sois au-dessus de moi
Sois au-dessous de moi
Reste pour me guider
Pour m’escorter dans le danger
Et pour me protéger
Reste avec moi.

Pont
Sois dans le coeur de tous ceux que j’aime
Sois dans le coeur de tous ceux qui m’aiment
Sois au fond du noir de la nuit
Sois dans le froid, sois dans le bruit
Sois au fond du ciel plein d’étoiles
Des grands soleils, des aubes pâles
Reste avec moi.

3.
Reste avec moi
Sois avec moi
Sois à côté de moi
Accompagne mes pas
Ne m’abandonne pas
Reste pour m’entourer
Pour me sauver, pour m’abriter
Tu es ma sûreté
Reste avec moi.