LA TRINITÉ, VOILA NOTRE « CHEZ NOUS »,
NOTRE MAISON PATERNELLE,
LA TRINITÉ NOUS DONNE RENDEZ-VOUS,
LE DIEU VIVANT NOUS APPELLE.

1.
Si quelqu’un t’aime,
Seigneur Jésus,
Il gardera ta Parole,
Et vous viendrez chez lui,
Vous serez son abri,
Toi, le Père, et l’Esprit.

2.
Si quelqu’un t’aime,
Seigneur Jésus,
Il est aimé de ton Père,
Vous demeurez en lui,
Soleil qui éblouit,
Toi, le Père, et l’Esprit.

3.
Si quelqu’un t’aime,
Seigneur Jésus,
Il fait la joie de ton Père,
Et vous chantez en lui,
Tendresse qui guérit,
Toi, le Père, et l’Esprit.