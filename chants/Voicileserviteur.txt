R.
Voici le serviteur, le Fils, le bien-aimé.
Il a offert sa vie afin de nous sauver.

1.
La multitude avait été consternée en le voyant,
car il était si défiguré qu’il ne ressemblait plus à un homme.
Devant lui les rois resteront bouche bée, (…)
ils découvriront ce dont ils n’avaient jamais entendu parler.

2.
Qui aurait cru ce que nous avons entendu ?
À qui la puissance du Seigneur a-t-elle été ainsi révélée ?
Devant Dieu, le serviteur a poussé comme une plante chétive,
enracinée dans une terre aride.

3.
Il était ni beau ni brillant pour attirer nos regards,
son extérieur n’avait rien pour nous plaire.
Il était méprisé, abandonné de tous,
homme de douleurs, familier de la souffrance.

4.
Semblable aux lépreux dont on se détourne ;
et nous l’avons méprisé, compté pour rien.
Pourtant, c’étaient nos souffrances qu’il portait,
nos douleurs dont il était chargé.

5.
Et nous, nous pensions qu’il était châtié,
frappé par Dieu, humilié.
Or, c’est à cause de nos fautes qu’il a été transpercé,
c’est par nos péchés qu’il a été broyé,

6.
Le châtiment qui nous obtient la paix est tombé sur lui,
et c’est par ses blessures que nous sommes guéris.
Nous étions errants comme des brebis, (…)
Mais le Seigneur a fait retomber sur lui nos fautes à nous tous.

7.
Maltraité, il s’humilie, il n’ouvre pas la bouche.
Comme un agneau conduit à l’abattoir,
comme une brebis muette devant les tondeurs,
il n’ouvre pas la bouche.

8.
Arrêté, puis jugé, il a été supprimé.
Qui donc s’est soucié de son destin ?
Il a été retranché de la terre des vivants,
frappé à cause des péchés de son peuple.

9.
On l’a enterré avec les mécréants,
son tombeau est avec ceux des enrichis ;
et pourtant il n’a jamais commis l’injustice, ni proféré le mensonge.
Broyé par la souffrance, il a plu au Seigneur.

10.
Mais s’il fait de sa vie un sacrifice d’expiation,
il verra sa descendance,
il prolongera ses jours :
par lui s’accomplira la volonté du Seigneur.

11.
À cause de ses souffrances, il verra la lumière, il sera comblé.
Parce qu’il a connu la souffrance,
le juste, mon serviteur, justifiera les multitudes,
il se chargera de leurs péchés.

12.
C’est pourquoi je lui donnerai les multitudes en partage,
les puissants seront la part qu’il recevra,
car il s’est dépouillé lui-même jusqu’à la mort,
il a été compté avec les pécheurs,
alors qu’il portait le péché des multitudes
et qu’il intercédait pour les pécheurs.