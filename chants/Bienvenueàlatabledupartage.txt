BIENVENUE À LA TABLE DU PARTAGE
CELLE DES FÊTES
DES NOCES ET DES FESTINS
BIENVENUE À LA TABLE DU PARTAGE
CELLE OÙ LE PAUVRE
PLUS JAMAIS N’AURA FAIM

1.
Marchant dans les pas du Serviteur
Qui nous dit de voir en l’autre un frère
Accueillir, protéger
Cette foule affamée
« Donnez-leur vous-mêmes à manger »

2.
Marchant dans les pas du Serviteur
Qui nous dit de rester vulnérables
Le mendiant est ton Roi
Son cri sera ma voix
« Et cela, en mémoire de moi »

3.
Marchant dans les pas du Serviteur
Qui nous dit de croire à la rencontre
L’avenir se construit
Au rythme des petits
« Étranger, vous m’avez accueilli »

4.
Marchant dans les pas du Serviteur
Qui nous dit de se laisser surprendre
Des talents révélés
A s’en émerveiller
« Un exemple que je vous ai laissé »

5.
Marchant dans les pas du Serviteur
Qui nous dit que le Royaume est proche
Nous avons rendez-vous
Éveillés et debout
« Que ma Paix soit toujours avec vous »