R.
Peuple de baptisés, marche vers ta lumière :
Le Christ est ressuscité !
Alléluia ! Alléluia !

1.
Notre Père nous aime avec tendresse,
Et cet amour est vivant pour les siècles.
Que son peuple le dise à l´univers.
Il rachète et rassemble tous les hommes.

2.
A tous ceux qui marchaient dans la tristesse,
La solitude, la faim, les ténèbres,
Le Seigneur a donné son réconfort,
Les guidant sur sa route de lumière.

3.
Proclamons la bonté de notre Père,
Et les merveilles de Dieu pour les hommes.
Plus de faim, plus de soif et plus de peur :
Car sans cesse Il nous comble avec largesse.

4.
Et tous ceux qui lui disent leur détresse,
En invoquant son secours et sa grâce,
Le Seigneur les délivre de la peur,
Les tirant de la mort et des ténèbres.

5.
Et tous ceux qui demeurent dans l´angoisse,
Ou déprimés, accablés par leurs fautes,
Le Seigneur les guérit, leur donne vie,
Leur envoie son pardon et sa Parole.

6.
Rendons gloire et louange à notre Père,
A Jésus Christ qui rachète les hommes,
A l´Esprit qui demeure dans nos cœurs,
Maintenant, pour toujours
et dans les siècles.