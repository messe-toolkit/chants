R.
Seigneur, rassemble-nous
Dans la paix de ton amour.

1.
Nos fautes nous séparent,
Ta grâce nous unit.
La joie de ta victoire
Éclaire notre nuit.

2.
Tu es notre espérance
Parmi nos divisions
Plus haut que nos offenses
S'élève ton pardon.

3.
Seigneur, vois la misère
Des hommes affamés !
Partage à tous nos frères
Le pain de l'unité.

4.
Heureux le cœur des pauvres
Qui cherchent l'unité
Heureux dans ton royaume
Les frères retrouvés

5.
Fais croître en notre attente
L'amour de ta maison !
L'Esprit, dans le silence
Fait notre communion.

6.
Ta croix est la lumière
Qui nous as rassemblés :
Ô joie de notre terre,
Tu nous as rachetés

7.
La mort est engloutie,
Nous sommes délivrés
Qu'éclate en nous ta vie,
Seigneur, ressuscité