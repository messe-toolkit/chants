R.
DONNE-NOUS SEIGNEUR
CE QUE NOS COEURS ATTENDENT.
DONNE-NOUS SEIGNEUR
LES FRUITS DE TA PROMESSE.

1.
Tu l’as promis, Seigneur,
Par la voix des prophètes :
Je vous rassemblerai
De tous les horizons,
Vous serez mon peuple
Et moi votre Dieu.

2.
Tu l’as promis, Seigneur,
Par la voix des prophètes :
Je vous baptiserai
D’eau pure et de l’Esprit,
Et vous n’aurez plus
D’idoles de mort.

3.
Tu l’as promis, Seigneur,
Par la voix des prophètes :
Je changerai vos coeurs,
Ma loi ne sera plus
Gravée dans la pierre
Mais au fond de vous.

4.
Tu l’as promis, Seigneur,
Par la voix des prophètes :
Vous aurez un Messie,
Jésus, ton Bien-Aimé
S’est livré pour nous ;
Par Lui nous prions.