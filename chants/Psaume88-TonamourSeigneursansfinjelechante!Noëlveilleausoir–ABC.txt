TON AMOUR, SEIGNEUR,
SANS FIN JE LE CHANTE !

1.
« Avec mon élu, j’ai fait une alliance,
j’ai juré à David, mon serviteur :
j’établirai ta dynastie pour toujours,
je te bâtis un trône pour la suite des âges. »

2.
Heureux le peuple qui connaît l’ovation !
Seigneur, il marche à la lumière de ta face ;
tout le jour, à ton nom il danse de joie,
fier de ton juste pouvoir.

3.
« Il me dira : “Tu es mon Père,
mon Dieu, mon roc et mon salut !”
Sans fin je lui garderai mon amour,
mon alliance avec lui sera fidèle. »