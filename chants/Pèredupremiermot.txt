1.
Père du premier mot
jailli dans le premier silence
où l’homme a commencé,
Entends monter vers toi,
comme en écho, nos voix
mêlées aux chants que lance
ton Bien-Aimé.

2.
Père du premier jour
levé sur les premières terres
au souffle de l’Esprit,
Voici devant tes yeux,
comme en retour, le feu
qui prend au coeur les frères
de Jésus-Christ.

3.
Père du premier fruit
gonflé de la première sève
au monde ensemencé,
Reçois le sang des grains
qui ont mûri et viens
remplir les mains qui cherchent
ton premier né.