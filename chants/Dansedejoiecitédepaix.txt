R.
REJOUIS-TOI, DANSE DE JOIE, JERUSALEM, CITE DE PAIX !
REJOUIS-TOI, DANSE DE JOIE :
TON DIEU VIENT DEMEURER CHEZ TOI !

1.
Ecoute la voix des guetteurs sur tes murs, ô Jérusalem !
Ils crient de joie quand se lève l'aurore
pour éveiller les habitants de la cité :

2.
Ecoute le cri de la Bonne Nouvelle, ô Jérusalem !
Les messagers qui s'avancent vers toi
vont annoncer la paix de Dieu qui vient sur toi :

3.
Ecoute ce chant de douceur dans ton coeur, ô Jérusalem !
Oui, c'est ton Dieu qui prend chair dans ta chair
pour consoler dans son amour tous tes enfants.

4.
Ecoute l'appel du veilleur dans la nuit, ô Jérusalem !
C'est de ses yeux qu'il a vu le Seigneur,
car Dieu revient dans la cité de son bonheur !

5.
Ecoute les pleurs d'un enfant innocent, ô Jérusalem !
C'est dans sa chair que Dieu vient te sauver,
renouvelant pour toi l'amour des premiers jours.

6.
Ecoute le chant de l'Epoux qui approche, ô Jérusalem !
C'est dans sa chair que Dieu vient t'épouser
pour ce bonheur où tu vivras ressuscitée.