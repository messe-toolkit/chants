1.
Du bleu, du vert
Couleurs de mer
Un peu de sable et de poussière
Faut tant de choses
Pour faire le monde !

Du marron clair
Couleur de terre
Beaucoup d’argile, un peu de pierres
Faut tant de choses
Pour faire le monde !

R.
AVEC TES MAINS
AVEC MES MAINS
ENSEMBLE
DÉCORONS UN BEAU JARDIN
OÙ LE MONDE SERA BIEN!

AVEC TES MAINS
AVEC MES MAINS
ENSEMBLE
COLORIONS UN BEAU CHEMIN
OÙ LE MONDE SERA BIEN!

ENSEMBLE, AVEC NOS MAINS
ENSEMBLE, POUR DEMAIN!

2.
Du gris, du bleu
Couleurs des cieux
Quelques nuages en plein milieu
Faut tant de choses
Pour faire le monde !

Le jaune-vermeil
Pour le soleil
Tant de couleurs qui étincellent
Faut tant de choses
Pour faire le monde !

3.
Du vert nature
Couleur verdure
Arbres et plantes, terre en pâture
Faut tant de vie
Pour faire le monde !

Zébrés, tachés
Peaux colorées
Tous les oiseaux, les animaux
Faut tant de vie
Pour faire le monde !

Pont
C’est Toi, Seigneur qui as mis dans mes mains
Ce monde créé de Tes mains.
C’est Toi, Seigneur, qui as mis dans nos mains
Ce monde à créer pour demain.

4.
Jaunes, noirs et blancs
Pour les vivants
Hommes et femmes, tous les enfants
Faut tant de vie
Pour faire le monde !

Tous ces visages
Comme un tissage
Beauté du monde, à tous les âges
Faut tant de vie
Pour faire le monde !