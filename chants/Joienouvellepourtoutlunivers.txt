1.
Joie nouvelle pour tout l’univers
Le Seigneur est ressuscité,
Alléluia !
Pourquoi chercher au tombeau vide
Le Vivant ?
Jésus se lève d’entre les morts,
Libre et vainqueur !

R.
Ouvrons nos coeurs au souffle nouveau
et nous vivrons, alléluia !

Dimanche de Pâques A, B, C

2.
Chant de Pâques jailli dans la nuit,
Le Seigneur est ressuscité,
Alléluia !
Dans sa victoire il nous emmène
Vers le Jour.
O mort où donc est ton aiguillon,
Christ est vivant !

2ème dimanche de Pâques A, B, C

1.
Joie nouvelle pour tout l’univers…

2.
Portes closes, il rejoint les siens
Et leur donne la paix de Dieu,
Alléluia !
Heureux sont-ils de reconnaître
Jésus Christ !
Heureux tous ceux qui croiront en lui
Sans l’avoir vu.

3ème dimanche de Pâques A

1.
Joie nouvelle pour tout l’univers…

2.
Qui s’approche des deux voyageurs
Et leur parle sur le chemin ?
Alléluia !
Paroles vives qui les brûlent
Comme un feu !
Le pain rompu éclaire leurs yeux,
C’est le Seigneur !

3ème dimanche de Pâques B

1.
Joie nouvelle pour tout l’univers…

2.
Sceau de gloire sur le corps du Christ
Les blessures de sa passion,
Alléluia !
Il a franchi la porte étroite
De la mort :
Les Écritures sont accomplies,
L’homme est sauvé !

3ème dimanche de Pâques C

1.
Joie nouvelle pour tout l’univers…

2.
Qui appelle sinon le Seigneur
Au repas qu’il a préparé,
Alléluia ?
Sur le rivage il nous devance
Aujourd’hui :
Pâque du Christ où nous partageons
Le Pain de vie.

4ème dimanche de Pâques A, B, C

1.
Joie nouvelle pour tout l’univers…

2.
Peuple en marche vers le Dieu vivant,
Jésus Christ est le bon Berger
Alléluia !
Prends le chemin de son passage,
N’aie pas peur !
Sa voix t’appelle jour après jour
Et te conduit.

5ème dimanche de Pâques A

1.
Joie nouvelle pour tout l’univers…

2.
Gloire au Père qui s’est révélé
En Jésus son Fils bien-aimé.
Alléluia !
Levons les yeux vers sa lumière,
Et croyons !
C’est lui la voie et la vérité,
C’est lui la vie !

5ème dimanche de Pâques B

1.
Joie nouvelle pour tout l’univers…

2.
Tout s’éveille dans le jour du Christ,
L’espérance avec lui renaît,
Alléluia !
Chaque sarment reçoit la sève
Du Vivant :
Printemps de Dieu annonçant les fruits
De l’Esprit Saint.

5ème dimanche de Pâques C

1.
Joie nouvelle pour tout l’univers…

2.
Tout est grâce, promesse de vie,
Dans l’amour du Seigneur Jésus
Alléluia !
Prêtons l’oreille à la parole
Du Sauveur :
Aimons nos frères pour témoigner
Qu’il est amour.

6ème dimanche de Pâques A, B, C

1.
Joie nouvelle pour tout l’univers…

2.
Roi de gloire, Messie humilié,
Jésus Christ est monté aux cieux,
Alléluia !
Dans son amour, il intercède
Près de Dieu.
Le Défenseur qu’il nous a promis.
Viendra bientôt.

7ème dimanche de Pâques A, B, C

1.
Joie nouvelle pour tout l’univers…

2.
Vienne encore l’Esprit du Très Haut
Dans l’Église de Jésus Christ
Alléluia !
Et que le feu de Pentecôte
Brûle en nous,
Alors le monde reconnaîtra
L’amour de Dieu !