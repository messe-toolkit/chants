Stance : Humble Sauveur,
pour quelle gloire
entres-tu dans la ville ?
" Hosanna ! Hosanna ! "
La foule crie sa joie.
Mais tu t'avances déjà vers la colline,
de l'autre côté de la ville,
portant ta croix…

Refrain :
Fils de David,
ouvre nos yeux et nous verrons ta gloire !

1.
Jérusalem, Jérusalem,
que de fois j’ai voulu rassembler tes enfants
et tu n'as pas voulu !

2.
Jérusalem, Jérusalem,
si tu avais compris mon message de paix…
mais il est demeuré caché à tes yeux !

3.
Jérusalem, Jérusalem,
toi qui tues et lapides les prophètes,
tu n'as pas reconnu le temps de ma visite !