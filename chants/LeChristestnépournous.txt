R.
Le Christ est né pour nous, alléluia !
Venez, venez, adorons le Seigneur !

1.
Le Fils de l'homme est né Noël !
Jésus nous est donné.
Jour de notre grâce :
L'étable accueille un Dieu caché ;
Rebut de notre race,
Il vient sauver le monde entier.
Paix à ceux qu'il aime,
Dieu soit glorifié !

2.
Le Fils de l'homme est né Noël !
Jésus nous est livré.
Pain pour notre table :
La terre s'ouvre au grain jeté ;
Broyé pour les coupables,
Il vient nourrir les corps lassés,
Paix à ceux qu'il aime,
Dieu soit exalté !

3.
Le Fils de l'homme est né Noël !
Jésus nous est livré.
Joie pour les convives :
La coupe attend le sang versé ;
Fontaine des eaux vives,
Il vient laver les corps souillés,
Paix à ceux qu'il aime,
Dieu soit magnifié !