R.
VIENS, ESPRIT SAINT, QUE LA TERRE SOIT PLUS BELLE,
TU METS DEVANT NOUS UN AVENIR DE PAIX !
TU NOUS OUVRES DES CHEMINS DE VIE NOUVELLE
ET TU NOUS CONDUIS VERS LA JOIE D’AIMER !

1.
La création est encore en souffrance,
Comme une femme met au monde un enfant,
Soyons ensemble signes du Vivant !

2.
La terre entière subit la violence,
Les injustices, les abus des nantis.
Ouvre-nous au respect de toute vie !

3.
Il est trop dur, le chemin de la guerre ;
Les hommes ont soif de connaître la paix.
Qui posera un geste le premier ?

4.
Tu vois des pauvres qui n’ont pas la parole,
Tous ceux qui portent les fardeaux de la vie.
Aurons-nous la patience d’un ami ?

5.
Chacun de nous a besoin de tendresse,
Chacun de nous a besoin d’être aimé,
De rencontrer des êtres de bonté.