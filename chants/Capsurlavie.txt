Refrain :
Cap sur la vie,
Coeurs réunis,
Pleins d’énergie,
Signes d’aujourd’hui.

1.
À coups de coeur,
Brisons nos peurs ;
Soyons semeurs
Du vrai bonheur.

2.
Pour le chercher,
Pour l’écouter,
S’apprivoiser,
Le rencontrer.

3.
Il faut partir,
Il faut agir,
Il faut ouvrir
Un avenir.

4.
Un goût de miel,
Un bout de ciel
Vers le soleil,
Vers l’essentiel.