R.
Tourne les yeux vers ton Seigneur en croix
Car il est ta lumière.
Tourne ton cœur vers ton Seigneur en croix
Car il est ta lumière.

1.
Je suis la lumière du monde.
Qui me suit, ne marchera pas dans la nuit.

2.
Je suis le chemin vers le Père.
Qui me suit, verra la lumière de Dieu.

3.
Je suis l'Amour de Dieu pour l'homme.
Qui me suit, verra le chemin de la vie.

4.
Je suis libération de l'homme.
Qui me suit, sera lumière dans la nuit.

5.
Je suis Résurrection et vie.
Qui me suit, ne connaîtra jamais la mort.