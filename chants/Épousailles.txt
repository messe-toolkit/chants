R.
AINSI VA LA VIE, AINSI COURT LE TEMPS
ET MONSIEUR LE VENT QUI SE PREND POUR L’ESPRIT
AINSI COURT LA VIE, AINSI VA LE TEMPS
ET MONSIEUR LE VENT QUI SE PREND POUR L’ESPRIT.

1.
La mer avait creusé la terre
À force de patience, de ressac et d’Amour
À force de coups de colère
Dans ses trois pas de danse
De voyage au long cours.

2.
L’eau et la terre qui s’épousent
À force de calanques ou dans les ports creusés
De coups de coeur en coups de blues
C’est la marée qui tangue
L’eau et la terre en paix.

3.
Puisque c’est Dieu qui crée le monde
À force de patience, de tendresse et d’Amour
Pourquoi donc la colère gronde
Dans les instants d’absence
Ou quand je fais le sourd ?

4.
Dieu et les hommes qui s’épousent
À force de patience, de tendresse et d’Amour
L’humanité devient jalouse
Et crie son impatience
De renaître au grand jour.