R.
Gloire à toi, Dieu très grand,
Tu nous veux pour a mis :
Fais de nous tes enfants,
Frères de Jésus Christ.

1.
Ta voix nous appelle, nous venons vers toi,
Ta Croix nous rassemble nous venons vers toi,
En ce jour de fête nous venons vers toi,
Jusqu'à ta montagne nous venons vers toi.

2.
Tu as fait le monde et cela est bon,
L'air et la lumière, et cela est bon,
La mer et les fleuves, et cela est bon,
Vallées et montagnes, et cela est bon.

3.
Tout ce qui respire est vivant pour toi,
La fleur ou le cèdre, est vivant pour toi,
Le poisson qui nage, est vivant pour toi,
Et l'oiseau qui vole, est vivant pour toi.

4.
A ta ressemblance, capable d'aimer,
Tu façonnes l'homme, capable d'aimer,
Un homme, une femme, capable d'aimer,
Une vraie famille, capable d'aimer.

5.
Tu choisis un peuple : il apprend ton nom,
Près de la montagne, il apprend ton nom,
Dans les solitudes, il apprend ton nom,
Il franchit les vagues, il apprend ton nom.

6.
Dans le cours des âges comme l'un de nous,
Est né d'une femme, comme l'un de nous,
Un Enfant des hommes, comme l'un de nous,
Parmi d'autres hommes, comme l'un de nous.

7.
Il est la Parole qui nous parle au cœur,
Il fait la Promesse, qui nous parle au cœur,
Il dit la Merveille, qui nous parle au cœur,
C'est l'Amour du Père, qui nous parle au cœur.

8.
Il offre sa vie, par amour pour nous,
Meurt sur le Calvaire, par amour pour nous,
Est mis dans la tombe, par amour pour nous,
Mais il se relève, par amour pour nous.