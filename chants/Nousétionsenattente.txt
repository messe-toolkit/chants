R.
NOUS ÉTIONS EN ATTENTE
PERDUS DANS L’INCONNU
SUR NOS CHEMINS D’ERRANCE
LA LUMIÈRE EST APPARUE

1.
Veilleurs en marche
L’étoile est devant nous
Dieu nous fait signe
Allons au rendez-vous

2.
Guetteurs de l’aube
Voici le jour, Il vient
Dieu nous délivre
La nuit touche à sa fin

3.
Sauveur du monde
Jésus vient parmi nous
Dieu se révèle
Voici l’homme debout

4.
Bonne Nouvelle
La paix s’éveille en nous
Dieu prend naissance
L’amour vaut plus que tout