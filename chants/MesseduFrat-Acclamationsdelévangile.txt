1.
Gloire à toi qui nous libères
Ta Parole est vérité

2.
Gloire à toi qui nous fait vivre
Ta Parole est notre joie

3.
Gloire à toi qui nous relève
Ta Parole est délivrance

4.
Gloire à toi Dieu notre Père
Par ton fils Jésus Christ

5.
Gloire à toi par ton Esprit
Pour toujours et à jamais