1.
Ô Dieu qui fis jaillir de l’ombre
Le monde en son premier matin,
Tu fais briller dans notre nuit
La connaissance de ta gloire.

2.
Tu es l’image de ton Père
Et la splendeur de sa beauté ;
Sur ton visage, ô Jésus-Christ,
Brille à jamais la joie du monde.

3.
Tu es toi-même la lumière
Qui luit au fond d’un lieu obscur ;
Tu es la lampe de nos pas
Sur une route de ténèbres.

4.
Quand tout décline, Tu demeures ;
Quand tout s’efface, Tu es là !
Le soir descend, Tu resplendis
An coeur de toute créature.

5.
Et quand l’aurore qui s’annonce
Se lèvera sur l’univers,
Tu régneras dans la cité
Où disparaissent les ténèbres.