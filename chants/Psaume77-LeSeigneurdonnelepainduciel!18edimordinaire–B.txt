LE SEIGNEUR DONNE LE PAIN DU CIEL !

1.
Nous avons entendu et nous savons
ce que nos pères nous ont raconté ;
nous redirons à l’âge qui vient
les titres de gloire du Seigneur.

2.
Il commande aux nuées là-haut,
il ouvre les écluses du ciel :
pour les nourrir il fait pleuvoir la manne,
il leur donne le froment du ciel.

3.
Chacun se nourrit du pain des Forts,
il les pourvoit de vivres à satiété.
Tel un berger, il conduit son peuple.
Il le fait entrer dans son domaine sacré.