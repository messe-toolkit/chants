ALLÉLUIA, ALLÉLUIA, ALLÉLUIA,
ALLÉLUIA, ALLÉLUIA, ALLÉLUIA. (bis)

1.
Ta Parole est la lumière qui éclaire notre coeur
Ta Parole nous libère, bénis sois-tu Seigneur !

2.
Ta Parole nous délivre de nos doutes et de nos peurs
Ta Parole nous fait vivre, bénis sois-tu Seigneur !