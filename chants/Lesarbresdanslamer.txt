1.
Regardez ! la Vierge a un enfant,
Un homme est né de Dieu,
le Ciel est parmi nous,
le peuple n’est plus seul !
IL NE FAUDRAIT QU’UN BRIN DE FOI
ET VOUS VERRIEZ LES ARBRES DANS LA MER.
Les mendiants qui sont rois
LES MENDIANTS QUI SONT ROIS
Les puissants renversés,
LES PUISSANTS RENVERSÉS,
LES TRÉSORS QU’ON PARTAGE!

2.
Regardez ! de l’eau se change en vin,
le vin devient du sang,
les pains se multiplient,
le peuple n’a plus faim !
IL NE FAUDRAIT QU’UN BRIN DE FOI
ET VOUS VERRIEZ LES ARBRES DANS LA MER.
Les déserts pleins de fleurs
LES DÉSERTS PLEINS DE FLEURS
Des moissons en hiver
DES MOISSONS EN HIVER.
DES GRENIERS QUI DÉBORDENT!

3.
Regardez ! l’infirme peut marcher
l’aveugle voit le jour
les sourds sont délivrés
le peuple n’a plus mal !
IL NE FAUDRAIT QU’UN BRIN DE FOI
ET VOUS VERRIEZ LES ARBRES DANS LA MER.
Les bourreaux sans travail
LES BOURREAUX SANS TRAVAIL
Les menottes rouillées
LES MENOTTES ROUILLÉES
LES PRISONS INUTILES !

4.
Regardez ! La croix est vide et nue,
Vos tombes sont crevées,
et l’homme tient debout
le peuple n’a plus peur !
IL NE FAUDRAIT QU’UN BRIN DE FOI
ET VOUS VERRIEZ LES ARBRES DANS LA MER.
Les fusils enterrés
LES FUSILS ENTERRÉS
Les armées au rebut
LES ARMÉES AU REBUT
LES MONTAGNES QUI DANSENT!