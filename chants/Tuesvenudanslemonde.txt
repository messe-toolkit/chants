STANCE
Tu es venu dans le monde et le monde ne t'a pas reconnu…
Au milieu de nous, tu accomplis les signes du salut :
Sauve-nous, Seigneur Jésus !

R.!

R.ls de Dieu, plein de grâce et de vérité, fais paraître ta gloire, alléluia !
1.
Dans le pays de Galilée, une parole a retenti,
Sur le séjour de l'ombre et de la mort, une lumière a resplendi !

2.
Voici le Serviteur de Dieu, sur lui repose l'Esprit,
Il ne brise pas le roseau froissé, il n'éteint pas la mèche qui faiblit !

3.
Il parle au cœur de Jérusalem, il proclame sa libération,
Il est la gloire d'Israël, la lumière des nations !

4.
Il impose silence à la tempête, et c'est le calme sur les eaux,
Sa paix dissipe notre peur, il nous conduit vers son repos !