L'Église annonce ton retour,
Et chaque instant nous en rapproche :
Qu'il nous soit fait miséricorde,
Seigneur Jésus, lorsque ton jour
Apparaîtra.

En prenant chair tu répondis
Au cri perdu de la détresse :
Tu pris sur toi notre faiblesse,
Et maintenant Dieu t'a remis
Le jugement.

Quand tu viendras avec le feu
Rappelle-toi ce que nous sommes :
Que ton amour, Seigneur, nous donne
Cet amour même que tu veux
Trouver en nous.