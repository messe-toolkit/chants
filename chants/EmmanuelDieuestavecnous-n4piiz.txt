R.
Emmanuel, Dieu est avec nous,
Emmanuel, il est vivant,
Emmanuel, Dieu est avec nous,
Emmanuel, il est le Seigneur, il nous aime.

1.
Un enfant est né, un fils nous est donné
Et sera nommé : Emmanuel.
Il est aimé du Père, qui l´a envoyé
Et, pour nous, donnera sa vie.

2.
Le troisième jour, il est ressuscité,
Lui qui est la vie, vainquit la mort.
Oui, célébrons le jour de notre salut,
Ensemble, exultons de joie !