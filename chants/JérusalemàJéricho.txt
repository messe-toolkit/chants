R.
JÉRUSALEM À JÉRICHO
JÉRUSALEM À JÉRICHO
JÉRUSALEM À JÉRICHO
JÉRUSALEM À JÉRICHO

1.
Un homme descendait sur le chemin de Jéricho,
Tournant le dos au soleil de sa vie,
La ville de lumière : Jérusalem !
Des brigands l’ont dépouillé
Sur le chemin de Jéricho.

2.
Un homme allait mourir sur le chemin de Jéricho
Car les docteurs de la loi n’ouvraient pas
Leurs yeux à la lumière : Jérusalem !
La violence avait gagné
Sur le chemin de Jéricho.

3.
Mais un étranger en voyage,
Un pèlerin solidaire,
S’approcha malgré la loi.
Il soigna ses plaies, ses blessures
Avec de l’huile et du vin,
Le chargea sur sa monture.

4.
Un homme reposait sur le chemin de Jéricho
Dans un hôtel à l’abri des dangers,
Le coeur dans la lumière : Jérusalem !
L’étranger avait payé
Une chambre pour soigner
Un ami désespéré
Sur le chemin de Jéricho.