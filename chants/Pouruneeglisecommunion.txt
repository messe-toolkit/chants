1.
Pour une Église communion
Témoin du Dieu qui fait Alliance,
Église jeune et résistante
Face à l’hiver des désunions,

R.
SEIGNEUR JÉSUS, NOUS TE PRIONS :
ENVOIE TON SOUFFLE, QU’ELLE VIVE !

2.
Pour une Église d’avenir
Marquée d’espoir et de courage
Dans les cités, dans les villages,
Église prête à rebondir,
SEIGNEUR JÉSUS…

3.
Pour une Église aimant la vie,
Chantant sa joie en mots de fête,
Ouvrant sa porte et ses fenêtres
Au monde en quête d’harmonie,
SEIGNEUR JÉSUS…

4.
Pour une Église d’Emmaüs
Rythmant ses pas sur ceux du Maître,
Jusqu’à la Pâque de lumière
Dans la maison du pain rompu,
SEIGNEUR JÉSUS…