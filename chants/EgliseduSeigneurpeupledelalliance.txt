R.
ÉGLISE DU SEIGNEUR,
PEUPLE DE L’ALLIANCE,
JUBILE POUR TON DIEU,
CHANTE L’ESPÉRANCE,
JUBILE POUR TON DIEU,
TON DIEU SAUVEUR!

1.
Va porter la flamme
Que le Christ allume en toi !
Feu du jour de Pâques
Pour la nuit des sans-espoir.
Par l’Esprit de Pentecôte,
Sois lumière du Royaume!

2.
Va puiser l’eau vive
Pour la terre desséchée !
Que partout revivent
Les déserts d’humanité !
Par l’Esprit qui fait renaître,
Que le monde se réveille !

3.
Fais lever le souffle
Qui franchit les océans !
Que les hommes s’ouvrent
À la brise du Vivant !
Par l’Esprit qui mène au large
Ils verront l’autre rivage.