1.
Toi seul es notre Père,
Dieu d’amour qui es aux cieux.
Tu nous révèles ta grandeur
En Jésus Christ ton Bien-aimé.

2.
Lui seul est notre Maître,
Tu l’as fait pour nous Seigneur.
Mais sur la voie de sa grandeur,
Qui donc choisit de s’avancer ?

3.
Pour lui nous sommes frères,
Et lui seul est Premier-Né.
Il a choisi de s’abaisser
Jusqu’à la croix du Serviteur.

4.
Dieu fort, tu le relèves,
Le voici ressuscité.
Accorde-nous d’en témoigner
Par notre joie de serviteurs.