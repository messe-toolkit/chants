PARLE SEIGNEUR, CAR TON SERVITEUR ÉCOUTE !

1.
Lorsque le jeune Samuel
Servait au temple de Silo,
Il y priait, il y dormait,
Tout près de l'Arche du Seigneur.

2.
Un soir où le Grand-Prêtre Eli
était couché et endormi,
Dieu appela le jeune enfant
et Samuel dit : « Me voici ! »

3.
Il accourut auprès d’Eli :
« Tu m’as appelé. Me voici ! »
Eli répond: « Va te coucher
car je ne t’ai pas appelé. »

4.
Dieu, de nouveau, dit : « Samuel ! »
et l’enfant revint près d’Eli,
car il ne savait pas encore
que c’était la voix du Seigneur.

5.
Mais après le troisième appel,
Eli sut qu’il venait du ciel.
Alors il dit à Samuel :
« Si l’on t’appelle, tu diras :

6.
Il était retourné au lit,
quand Dieu se tint auprès de lui.
Dès qu’il l’appelle par son nom,
sans hésiter l’enfant répond :

7.
Le petit Samuel grandit
et le Seigneur fut avec lui.
Il a parlé au nom de Dieu,
car il a su écouter Dieu.