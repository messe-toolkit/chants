Refrain
JOIE ET LUMIÈRE
DE LA GLOIRE ÉTERNELLE DU PÈRE,
LE TRÈS-HAUT, LE TRÈS-SAINT !
Ô JÉ-SUS CHRIST !

Verset 1.
Oui, tu es digne d'être chanté
dans tous les temps par des voix sanctifiées,
Fils de Dieu qui donnes vie :
Tout l'univers te rend gloire.

Verset 2.
Parve-nus à la fin du jour,
Contemplant cette clarté dans le soir,
Nous chantons le Père et le Fils
Et le Saint-Esprit de Dieu.