1.
Avec la foule qui fait la fêt’, je chante « Hosanna ! »
Avec la foule qui se rassemble, je chante « Hosanna ! »

OUI QU’IL SOIT BÉNI, AU PLUS HAUT DES CIEUX,
L’ENVOYÉ DU PÈRE, LE FILS DE DAVID !
OUI QU’IL SOIT BÉNI, LE MESSIE DE DIEU,
CRIONS « HOSANNA ! », AU FILS DE DAVID !

2.
Avec les pauvres qui n’en peuv’ plus, je chante « Hosanna ! »
Avec les pauvres qui l’accompagnent, je chante « Hosanna ! »

3.
Avec les faibles qui l’aiment tant, je chante « Hosanna ! »
Avec les faibles qui l’interpellent, je chante « Hosanna ! »

4.
Avec l’Église qui se souvient, je chante « Hosanna ! »
Avec l’Église qui le célèbre, je chante « Hosanna ! »