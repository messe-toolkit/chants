Adam s'éloigne,
Il a quitté les rives du pays d'Eden.
Il n'entend plus
La voix qui l'appelait dans le jardin.
Penché sur les eaux sombres, les yeux ouverts,
Il ne voit plus l'image de son Dieu.

L'enfant tressaille :
Un chant nouveau
L’éveille aux profondeurs du sein ;
Les yeux fermés,
Il danse quand s'approche le Seigneur :
Dès l'ombre originelle, le Saint-Esprit
L'a consacré prophète du Très-Haut.

Tout va renaître :
Dans le désert un cri a devancé le jour,
Pour annoncer
Celui qui sanctifie toutes les eaux.
Debout au bord du fleuve, rempli de joie,
Le Précurseur attend l'Élu de Dieu.

Adam s'approche,
Il a rejoint les rives du pays d'exil.
Les eaux refluent
Avant de se livrer au Feu vivant,
Lumière en qui s'épousent le ciel et l'eau,
Resplendissante image de son Dieu.