1.
Les anges dans nos campagnes
Ont entonné l'hymne des Cieux,
Et l'écho de nos montagnes
Redit ce chant mélodieux.

R.
Gloria in excelsis Deo. (bis)

2.
Bergers, quittez vos retraites
Unissez-vous à leurs concerts,
Et que vos tendres musettes
Fassent retentir dans les airs :

3.
Il est né le roi céleste,
Le seul Très-Haut, le seul Seigneur
En lui Dieu se manifeste
Pour nous donner le vrai bonheur.

4.
Il apporte à tout le monde
La paix, ce bien si précieux
Aujourd'hui nos cœurs répondent
Pour accueillir notre Dieu.

5.
Ils annoncent la naissance
Du libérateur d’Israël
Et pleins de reconnaissance
Chantent en ce jour solennel.

7.
Cherchons tous l’heureux village
Qui l’a vu naître sous ses toits
Offrons-lui le tendre hommage
Et de nos cœurs et de nos voix.