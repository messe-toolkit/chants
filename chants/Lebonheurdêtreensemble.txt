1.
Toi l’étranger, toi l’ami de toujours
Ensemble nous enflammerons le monde entier
Notre espérance est un horizon sans fin
Notre espérance est un lendemain, un lendemain
Ouvrons nos coeurs, nous chanterons…

R.
LE BONHEUR D’ÊTRE ENSEMBLE
ET DE CROIRE EN L’AMOUR.
L’AMOUR DU DIEU LUMIÈRE, DIEU SOLEIL LEVANT (bis)

2.
Tu viens de loin ou tu es de ma rue
Ensemble nous enflammerons le monde entier
Nos différences, nos couleurs sont une chance
Nos différences forment un même pain, un même pain
Ouvrons nos coeurs, nous chanterons…

3.
Tu ris de tout ou ton coeur est trop lourd
Ensemble nous enflammerons le monde entier
Notre espérance est un Amour infini
Notre espérance est chemin de Vie, chemin de Vie
Ouvrons nos coeurs, nous chanterons…

4.
Nous sommes frères, enfants d’un même Père
Ensemble nous enflammerons le monde entier
Rassemblons-nous avec nos joies, nos misères
Rassemblons-nous sous la même Lumière, la même Lumière
Ouvrons nos coeurs, nous chanterons…