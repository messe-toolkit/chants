1.
Acceptes-tu de mourir avec moi, toi à qui je donne vie,
N´aie pas peur de ce chemin devant toi,
Moi, je précède ton pas.
Demeure en moi pour trouver la vraie vieHors de moi tu ne peux rien.
\n
Laisse-moi étreindre ton cœur et ta vie,
Afin de porter du fruit (bis)

2.
Mon cœur brulant veut s´épancher en toi
Source d´où jaillit la vie,
Laisse se creuser mon désir en toi pour renaitre de l´Esprit.
Si le grain tombé en terre ne meurt
Il ne peut porter du fruit
\n
Laisse mon cœur se consumer en toi
De l´Amour qui donne vie. (bis)

3.
Laisse-moi façonner ton cœur à mon gré,
Peu à peu dans le secret
Tiens-le offert en silence près du mien
Coupe fragile de mon Sang
Laisse mes yeux éclairer ton regard
Pour en porter le reflet
\n
Sois l´instrument de mon cœur, de ma joie
Va je suis là, ne crains pas !
Sois l´instrument de mon cœur, de ma joie
Va je suis là près de toi !