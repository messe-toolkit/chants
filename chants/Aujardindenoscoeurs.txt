R.
ACCLAMONS LE SEIGNEUR!
IL NOUS DONNE SA VIE.
QUE GERME LE BONHEUR.
AU JARDIN DE NOS COEURS!

1.
Au jardin de la joie,
Des rameaux s’agitent sans fin.
Jésus nous ouvre le chemin
Fêté comme un grand roi :
Hosanna ! Hosanna !

2.
Au jardin de l’amour,
La vigne donne du bon vin.
Jésus nous partage son pain.
Il se fait serviteur,
Lui le Dieu, le Seigneur.

3.
Au jardin de la nuit,
Le grain qui meurt pourra germer.
Jésus nous invite à veiller
Pour ne plus avoir peur,
Car l’amour est vainqueur.

4.
Au jardin de la mort,
La croix est comme un arbre sec.
Jésus pardonne nos échecs.
Ses amis sont très loin,
Mais Marie tient sa main.

5.
Au jardin de la vie,
Le printemps ouvre les bourgeons.
Jésus nous envoie en mission.
Il est ressuscité !
Le soleil s’est levé !