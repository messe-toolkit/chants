Oh Jésus,
toi qui mérites tant d’être aimé,
fais-moi aller pour toi partout
où tu veux que j’aille !
Fais-moi me renoncer,
porter ma croix chaque jour et te suivre !
Fais-moi être et faire à tout instant
ce qu’il te plaît le plus
que je sois et fasse.