IL EST RESSUSCITÉ

IL EST RESSUSCITÉ, LE SEIGNEUR DE L'UNIVERS
ET DE SON SOUFFLE IL NOUS ENVAHIT, A – ALLELU – UIA !

1.
Il vient nous imprégner de sa paix,
Dans un monde qui a peur,
Il nous murmure : "C’est moi, ne craignez pas !"

2.
Il est le feu ardent qui surgit
Quand le doute nous envahit,
Par sa lumière, il vient chasser la nuit !

3.
Il vient nous partager sa victoire
Nous faire vivre de son élan,
Passion d’amour plus forte que la mort !

4.
Il vient nous emporter dans sa joie,
Elle sera comme un torrent
Qui se jouera de toutes les frontières !