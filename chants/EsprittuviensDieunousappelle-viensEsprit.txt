1.
Esprit, Toi le feu de la terre,
Esprit, Toi le feu de l'amour.
Tu viens embraser de lumière,
Tu viens réchauffer nos coeurs lourds.
Esprit, Toi le feu de la terre,
Esprit, Toi le feu de d'amour.

2.
Esprit, Toi le vent de la terre,
Esprit, Toi le vent de l’espoir.
Tu viens bousculer nos misères,
Tu viens transformer notre histoire…

3.
Esprit, Toi la vie de la terre,
Esprit, Toi qui es vie de Dieu.
Tu viens inspirer nos prières,
Tu viens pour répondre à nos voeux…

4.
Esprit, Toi la joie de la terre,
Esprit, Toi la joie de nos coeurs.
Tu viens pour la fête des frères,
Tu viens partager le bonheur…

5.
Esprit, Toi la paix de la terre,
Esprit, Toi la paix chaque jour.
Tu viens pour que cessent les guerres
Tu viens pour unir dans l'amour…