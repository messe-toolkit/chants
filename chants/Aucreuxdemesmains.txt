R.
AU CREUX DE MES MAINS
J’AI PREPARE LA PLACE
POUR ACCUEILLIR LE PAIN
A LA TABLE DU PARTAGE

1.
Accueillir dans ce pain
Tout le soleil du monde
Dans ces milliers de grains de blé
Voici la vie, multipliée.

2.
Accueillir dans ce pain
Tout le travail des hommes
De mains en mains il est passé
Il porte en lui l’humanité.

3.
Accueillir dans ce pain
L’Amour que Dieu nous donne
Avec Jésus voici la vie
Donnée sans fin pour ses amis.

4.
Accueillir dans ce pain
Le peuple qui s’approche
Dans l’avenir qui se construit
Est un levain rempli de vie.