Antienne - stance
Saisis d'une grande crainte, les disciples disaient :
Qui donc est cet homme ? Même le vent et la mer lui obéissent !

R.
Dominus Iesus Christus !
gloriam Dei Patris.

Versets
Ps 147, 12-13 ; 15-18

1.
Glorifie le Seigneur, Jérusalem !
Célèbre ton Dieu, ô Sion !
Il a consolidé les barres de tes portes,
Dans tes murs il a béni tes enfants ;

2.
Il envoie sa parole sur la terre :
Rapide, son verbe la parcourt.
Il étale une toison de neige,
Il sème une poussière de givre.

3.
Il jette à poignées des glaçons ;
Devant ce froid, qui pourrait tenir ?
Il envoie sa parole : survient le dégel ;
Il répand son soli e : les eaux coulent.

Ou bien

Ps 62, 2-8

1.
Dieu, tu es mon Dieu, je te cherche dès l'aube :
Mon âme a soif de toi ;
Après toi languit ma chair,
Terre aride, altérée, sans eau.

2.
Je t'ai contemplé au sanctuaire,
J'ai vu ta force et ta gloire.
Ton amour vaut mieux que la vie :
Tu seras la louange de mes lèvres !

3.
Toute ma vie je vais te bénir,
Lever les mains en invoquant ton nom.
Comme par un festin je serai rassasié ;
La joie sur les lèvres, je dirai ta louange.

4.
Dans la nuit, je me souviens de toi
Et je reste des heures à te parler.
Oui, tu es venu à mon secours :
Je crie de joie à l'ombre de tes ailes.