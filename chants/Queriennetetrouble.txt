1.
Que rien ne te trouble,
que rien ne t’effraie.
Tout passe,
Dieu ne change pas.

2.
Par la patience,
tout s’obtient.
Qui a Dieu
ne manque de rien.

3.
Dieu seul suffit.
Dieu seul suffit.