LÈVE-TOI, LÈVE-TOI
ALLEZ LÈVE-TOI ET VOGUE
SUR LES OCÉANS DU MONDE
LÈVE-TOI, LÈVE-TOI
LÈVE-TOI ET PREND LA VAGUE
VIRE AU VENT
LE VENT DES EAUX PROFONDES.

1.
Ensemble au large
Avancer, bel équipage
Et partir, pari sur la confiance
Là où rien n’est gagné d’avance
Et jeter les filets
Une pêche est à remonter
Nous avons échoué
Coeurs et paniers vidés
Fatigués, épuisés
Mais le vent a tourné
Une voix s’est levée
Aujourd’hui tout va changer.

2.
Ensemble, au large
Tournés vers d’autres rivages
Naviguer à vue de l’insouciance
Et nos cales à plein d’espérance
Et craquer les filets
D’une pêche surabondée
Nous avions échoué
Coeurs et paniers vidés
Fatigués, résignés
Mais le vent a tourné
Une voix s’est levée
Aujourd’hui tout a changé.