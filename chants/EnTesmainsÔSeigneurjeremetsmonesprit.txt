Refrain :
En tes mains, ô Seigneur,
Je remets mon esprit ;
En tes mains, ô Seigneur,
Je remets ma vie.

1.
En tes mains, vois notre souffrance,
Notre espérance, notre amour, nos désirs,
En tes mains nos jours sans confiance,
Façonne nos vies en tes mains !

2.
En tes mains, vois notre misère,
Notre prière, notre amour, nos désirs ;
En tes mains, nos jours sans lumière,
Façonne nos vies en tes mains.

3.
En tes mains, vois notre faiblesse,
Notre tendresse, notre amour, nos désirs ;
En tes mains, nos jours de détresse,
Façonne nos vies en tes mains.