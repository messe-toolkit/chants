1.
Un vieux nègre de Brooklyn
Naviguait comme une épave
Dans le coton et le gin
Dont il était né l’esclave.

R.
SES DIX DOIGTS SUR UN PIANO
LANCENT UNE COURSE DE CHEVAUX
ET LES CHEVAUX SONT TOUS NOIRS
PARC’QUE C’EST NOËL CE SOIR.

2.
Ce nègre de casino
Promenait ses mains jalouses
En caresses de piano
Pour nous inventer le blues.

3.
Son clavier de domino
A cassé sa tire-lire
Sa bouche n’est qu’un piano
Pour swinguer ses éclats de rire.

4.
Le nègre a vu son Sauveur
En oubliant sa mémoire
Tous les blancs sont des menteurs
Jésus avait la peau noire.