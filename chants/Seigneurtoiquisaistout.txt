SEIGNEUR, TOI QUI SAIS TOUT,
TU SAIS BIEN QUE JE T’AIME ! (bis)

1.
C’est au début du jour,
Premier de la semaine ;
La nuit est comme un four,
La pêche a été vaine.
Quand au petit matin,
Jésus sur le rivage,
À Simon il enjoint
De retourner au large.

2.
C’est après le repas
Servi par Jésus même,
Qu’à Pierre il demanda
“Est-ce que vraiment tu m’aimes ?”
Et Pierre au rendez-vous
Où son Maître le mène
Répondit “Tu sais tout,
Tu sais bien que je t’aime !”

3.
Jésus, Ressuscité
Passant l’histoire ancienne
À Pierre va confier
La mission qui est sienne.
“Tu seras le Pasteur,
Tu guideras tes frères
Par les chemins du coeur
Vers l’amour de mon Père !”

4.
Depuis, jour après jour,
Annonçant l’Évangile
À chaque carrefour,
À travers chaque ville,
À Rome il vient planter
La croix comme une assise
Pour Celui qu’il aimait,
Fondement de l’Église.