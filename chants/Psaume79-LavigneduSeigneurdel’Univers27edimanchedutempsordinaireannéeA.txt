LA VIGNE DU SEIGNEUR DE L’UNIVERS,
C’EST LA MAISON D’ISRAËL.

1.
La vigne que tu as prise à l’Égypte,
tu la replantes en chassant des nations.
Elle étendait ses sarments jusqu’à la mer,
et ses rejets, jusqu’au Fleuve.

2.
Pourquoi as-tu percé sa clôture ?
Tous les passants y grappillent en chemin ;
le sanglier des forêts la ravage
et les bêtes des champs la broutent.

3.
Dieu de l’univers, reviens !
Du haut des cieux, regarde et vois :
visite cette vigne, protège-la,
celle qu’a plantée ta main puissante.

4.
Jamais plus nous n’irons loin de toi :
fais-nous vivre et invoquer ton nom !
Seigneur, Dieu de l’univers, fais-nous revenir ;
que ton visage s’éclaire, et nous serons sauvés.