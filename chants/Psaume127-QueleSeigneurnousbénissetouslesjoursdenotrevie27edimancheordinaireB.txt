QUE LE SEIGNEUR NOUS BÉNISSE
TOUS LES JOURS DE NOTRE VIE !

1.
Heureux qui craint le Seigneur
et marche selon ses voies !
Tu te nourriras du travail de tes mains :
Heureux es-tu ! À toi, le bonheur !

2.
Ta femme sera dans ta maison
comme une vigne généreuse,
et tes fils, autour de la table,
comme des plants d’olivier.

3.
Voilà comment sera béni
l’homme qui craint le Seigneur.
Que le Seigneur te bénisse tous les jours de ta vie,
et tu verras les fils de tes fils.