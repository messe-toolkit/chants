SOUMIS AUX VENTS CONTRAIRES,
BRISÉ SUR UN RIVAGE DE MISÈRE,
J'ÉTAIS INCARCÉRÉ, DÉBOUSSOLÉ,
VOUS M'AVEZ VISITÉ.

1.
Tu cognes de tes poings les murs de la prison ;
Les plus sombres remords se changent en obsession.
Les détenus se moquent en te montrant du doigt.
Rejeté de tes proches tu es aux abois.

2.
La loi de ce milieu est celle du plus fort.
Tu vois que le violent qui s'impose s'en sort.
Mais toi tu cherches un peu de solidarité,
Une entraide exprimant un début d'amitié.

3.
On t'a coupé de ta famille, des amis,
Tu deviens comme fou. Le chagrin t'envahit.
Tu pries de tout ton coeur, mais Dieu paraît si loin...
Comment faire avec lui un morceau du chemin ?

4.
Si tu as fait beaucoup de tort à ton prochain,
Sois heureux du pardon s'il te tend une main.
Et toi, si tu oublies, tu connaîtras la paix ;
Autrement, la rancoeur va toujours te ronger.