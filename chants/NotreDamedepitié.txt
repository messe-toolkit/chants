1.
Quand la route est difficile,
Sans balise et sans repère,
L’avenir est si fragile :
J’en appelle à ton coeur de mère.

R.
NOTRE DAME DE PITIÉ
PROTÈGE-NOUS,
NOTRE DAME DE BONTÉ
VEILLE SUR NOUS.

2.
Quand le doute a trop d’emprise
Dans la nuit de nos misères,
Pour attendre la vie promise :
J’en appelle à ton coeur de mère.

3.
Toi qui portes nos souffrances,
Nos épreuves et nos cancers,
Pour tenir dans l’espérance :
J’en appelle à ton coeur de mère.

4.
Toi qui vis dans la lumière,
Conduis-nous vers notre Père,
Pour entrer dans la prière :
J’en appelle à ton coeur de mère.