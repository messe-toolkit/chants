Seigneur, prends pitié. (ter)
Ô Christ, prends pitié. (ter)
Seigneur, prends pitié. (ter)

R.
Gloire à Dieu au plus haut des cieux ! (bis)

R.
Alléluia ! Alléluia !

Saint ! Saint ! Saint, le Seigneur, Dieu de l'univers !
Le ciel et la terre sont remplis de ta gloire
Hosanna, hosanna au plus haut des cieux. (bis)
Béni soit celui qui vient au nom du Seigneur.
Hosanna, hosanna au plus haut des cieux. (bis)

Agneau de Dieu qui enlèves le péché du monde, prends pitié de nous. (bis)
Agneau de Dieu qui enlèves le péché du monde, donne-nous la paix.