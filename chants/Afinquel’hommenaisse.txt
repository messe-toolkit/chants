1.
Afin que l'homme naisse
A lui-même et à toi,
Qu'il s'ouvre à ta promesse
En murmurant ta loi,
Ton humaine tendresse
Est la source et la voix.
Toi qui parles à son coeur,
Père,
Unifie-le en toi.

2.
Afin que l'homme vive,
En lui-même, avec toi,
Qu'il t'aime, qu'il bénisse
En revêtant ta joie,
Ta lumière l'habille,
Et il danse pour toi
Toi qui es son bonheur,
Père,
Enchante-le de toi.

3.
Afin que l'homme meure
A ce monde et en toi,
Qu'il veille jusqu'à l'heure
En te donnant sa foi,
Dans sa terre intérieure
Où de nuit le blé croît.
Toi qui fus le semeur,
Père,
Engrange-le chez toi.