R.
Jésus est le chemin
Qui nous mène droit vers le Père,
C´est lui qui est la Vérité,
Il est la vie !

1.
Jamais homme n´a parlé ainsi que celui-là,
Non personne ne peut faire les signes qu´il accomplit,
Dieu est avec lui

2.
Jean-Baptiste nous a dit : ´Voici l´Agneau de Dieu´,
Car c´est lui que le Père a marqué de son sceau,
Venez et voyez.

3.
Celui qui croit en moi a la vie éternelle,
Qui me suit marchera, non pas dans les ténèbres,
Mais dans la lumière !

4.
En gardant ma parole, vous serez mes disciples,
Alors vous connaîtrez vraiment la vérité
Qui vous rendra libres.

5.
Qui ne prend pas sa croix, ne peut suivre mes pas,
Mais qui perdra sa vie pour moi la sauvera,
Marchez à ma suite !

6.
Mon Royaume n´est pas de ce monde ici-bas
Je suis Roi et quiconque est de la vérité,
Écoute ma voix.

7. À nouveau je viendrai, vous prendrai avec moi,
Et du lieu où je vais vous savez le chemin,
Ayez foi en moi.

8.
´De son sein couleront, oui, des fleuves d´eaux vives, ´
Recevez l´Esprit Saint pour être dans le monde,
Témoins de l´amour !