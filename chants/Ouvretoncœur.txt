OUVRE LES YEUX
OUVRE LES MAINS
OUVRE TON COEUR.
À LA LUMIÈRE QUI VIENT
OUVRE LES YEUX
OUVRE LES MAINS
OUVRE TON COEUR.
À LA LUMIÈRE QUI VIENT

1.
Ouvre les yeux
Dieu te fait signe
Et rejoins ceux
Qui sont là dans sa vigne
Ouvre les mains
À sa promesse
Cueille en chemin
Les fruits de sa tendresse

2.
Ouvre ton coeur
Dieu y dépose
Pour ton bonheur
L’amour sur toute chose
Ouvre ta vie
À l’espérance
Heureux celui
Qui marche en sa Présence