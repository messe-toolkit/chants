R.
Tu es Saint Dieu, tu es Saint fort,
Saint immortel,
Prends pitié de nous !

1.
Tu as revêtu notre faiblesse
Et tu nous as revêtus de ta splendeur ;
Tu as fait entrer l’Église, ton épouse,
Dans la chambre nuptiale du royaume nouveau.

2.
Seigneur, ne regarde pas nos péchés,
Mais la foi de ton Église ;
Prends-nous en ta douce pitié,
Toi qui es plus grand que notre cœur.

3.
De mon cœur jaillit la louange du soir :
Je chante mon poème en l’honneur de mon Roi ;
Que parle en moi ton Esprit Saint,
Qu’il me conduise vers la splendeur de ton aurore.

4.
Aujourd’hui nous avons contemplé le Seigneur Christ,
Nous avons bu son sang : il nous a pardonnés ;
Nous avons célébré la louange de sa gloire,
Et nous veillons dans l’espérance de son retour.

5.
Jésus, lumière sans déclin,
Fais-nous connaître la beauté de ta croix.
Par tes mains étendues, rassemble tous les peuples,
Pour les donner au Père en te donnant à lui.

6.
Père invisible, Source de la lumière,
Fontaine de la vie et de toute vérité,
Donne-nous l’Esprit Saint, l’Illuminateur,
Que nous te connaissions, toi et ton Fils bien-aimé.

7.
Chantons la bienveillance de Dieu pour les hommes
Et l’abondance de sa miséricorde :
Marie, terre en qui Dieu a semé la joie du monde,
Attente de notre désir, encens de notre prière.