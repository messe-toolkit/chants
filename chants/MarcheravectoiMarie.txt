R.
MARCHER AVEC TOI MARIE
ACCUEILLIR LE CHEMIN DE VIE,
AVEC POUR SEUL PRÉSENT
LE DON DE TON ENFANT. (bis)

1.
Marie, comme une étoile
Tu guides nos matins,
C’est toi qui, nous dévoiles
Un “oui” pour nos chemins.

2.
Marie, comme une flamme
Tu veilles à nos chagrins,
Transforme chaque larme
En fruit sur nos chemins.

3.
Marie, comme une source
Tu fais ouvrir les mains,
Accueille chaque doute
En soif pour nos chemins.

4.
Marie, dans chaque fête
Tu veilles à nos besoins
Ta joie reste parfaite
De nuit sur le chemin.

5.
Marie, sur notre route
Tu nous rends pèlerins,
Ouverts à son écoute
Jésus devient Chemin.