COMME LE PÈRE M’A AIMÉ

COMME LE PÈRE M’A AIMÉ, MOI AUSSI JE VOUS AI AIMÉS
RESTEZ DONC EN MON AMOUR.
ET GARDEZ TOUTES MES PAROLES. (Jean 15,9)

1.
Je vous donne un commandement nouveau :
Que vous vous aimiez les uns les autres ;
Si vous avez de l’amour entre vous,
Tous sauront que vous êtes mes disciples. (Jean 13,34…)

2.
Voyez quel amour nous a donné le Père
Pour que nous soyons appelés ses enfants. (1 Jean 3,1)
Mes bien-aimés, aimons-nous les uns les autres
Car celui qui aime est né de Dieu. (1 Jean 4,7)

3.
Dieu nous a envoyé son fils unique
Pour que nous ayons la vraie vie par lui.
Mes bien-aimés, si Dieu nous a tant aimés,
Nous aussi, nous devons nous aimer les uns les autres. (1 Jean 4,9 et 11)