1.
Ô Maître
Doux et humble de coeur,
Tu tressailles de joie dans l’Esprit saint
Car Dieu révèle aux pauvres ton mystère.
Donne-nous part à cette grâce :
Rends notre coeur pareil au tien
Et nous te connaîtrons.

2.
Jésus,
Notre unique Pasteur,
Qu’une seule brebis vienne à manquer
Tu laisses tout, tu pars à sa recherche.
L’amour affronte les ténèbres :
Le coeur du Père dans le tien
Rejoint l’enfant perdu.

3.
Ô Roi
Devenu serviteur,
De ton coeur ont jailli l’eau et le sang,
Tu as livré ta vie jusqu’à l’extrême.
Tu t’es remis aux mains du Père :
Il a fait naître de ta mort
Un peuple au coeur nouveau.

4.
Ô Christ,
Fils de l’homme et Seigneur,
Que tressaille de joie dans l’Esprit saint,
Le peuple né pour vivre en ta lumière.
Ouvre nos yeux à l’invisible :
Dans ton coeur brûle et resplendit
La gloire du Dieu saint.