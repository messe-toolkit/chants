Gloria Gloria, Alléluia.
Gloria Gloria, Alléluia.

Gloire à Dieu
Au plus haut des cieux,
Et paix sur la terre, aux hommes qu’Il aime

Nous te louons,
Nous te bénissons,
Nous t’adorons,
Nous te glorifions,
Nous te rendons grâce
Pour ton immense gloire

Seigneur Dieu,Roi du ciel,
Dieu le Père tout puissant.
Seigneur fils unique Jésus Christ
Seigneur Dieu Agneau de Dieu
Le fils du Père

Toi qui enlèves le péché du monde
Prends pitié de nous.
Toi qui enlèves le péché du monde,
Reçois notre prière.
Toi qui est assis à la droite du Père,
Prends pitié de nous

Car Toi seul es saint,
Toi seul es Seigneur,
Toi seul es le Très-Haut
Jésus Christ avec le Saint Esprit
Dans la gloire de Dieu le Père
Amen

Gloria Gloria, Alléluia.
Gloria Gloria, Alléluia.