Quand nous mangeons ce pain
et buvons à cette coupe,
nous célébrons le mystère de la foi :

Nous rappelons ta mort, Seigneur ressuscité,
et tous attendons que tu viennes.
Et nous attendons que tu viennes.