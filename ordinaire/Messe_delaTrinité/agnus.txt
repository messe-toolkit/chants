Agneau de Dieu, envoyé par le Père,
Tu nous sauves du péché,
Prends pitié de nous, Seigneur,
Prends pitié de nous, Seigneur.

Agneau de Dieu, emportant notre mort,
Tu nous donnes ta vie,
Prends pitié de nous, Seigneur,
Prends pitié de nous, Seigneur.

Agneau de Dieu, dans l’Amour de l’Esprit,
Tu apaises notre cœur,
Donne-nous la paix, Seigneur,
Donne-nous la paix, Seigneur.