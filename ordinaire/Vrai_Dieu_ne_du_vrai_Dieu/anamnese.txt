Vrai Dieu, né du Vrai Dieu,
Lumière née de la Lumière
Jésus, cloué au bois de la croix
Ressuscité, nous proclamons ton retour
dans la gloire !