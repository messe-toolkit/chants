R.
Gloire à Dieu, paix aux Hommes,
Joie du Ciel sur la terre !

1.
Pour tes merveilles, Seigneur Dieu,
ton peuple te rend grâce.
Ami des hommes, sois béni
pour ton Règne qui vient !

2.
À toi les chants de fête,
par ton Fils bien-aimé, dans l’Esprit.
Sauveur du monde, Jésus Christ,
écoute nos prières !

3.
Agneau de Dieu, vainqueur du mal,
sauve-nous du péché.
Dieu saint, splendeur du Père,
Dieu vivant, le Très-Haut, le Seigneur !