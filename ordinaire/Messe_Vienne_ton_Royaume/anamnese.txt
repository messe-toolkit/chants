Jésus-Christ, mort en croix
Pour notre vie,
Libéré du tombeau,
Premier-né des temps nouveaux,
Vienne ton Royaume
Pour la joie du monde !

Alléluia, Maranatha !
Alléluia, Maranatha !