Sanctus, Sanctus, Sanctus
Dominus Deus Sabaoth. (bis)

Le ciel et la te-erre
Sont remplis de ta gloire.
Hosanna hosa-a-anna au plus haut des cieux.
Béni soit celui qui-i vient
Au nom du Seigneur.
Hosanna hosa-a-anna au plus haut des cieux.