Gloire à Dieu, gloire à Dieu au plus haut des cieux
Gloire à Dieu, gloire à Dieu au plus haut des cieux (bis)

Et paix sur la terre aux hommes qu’il aime.
Nous te louons, nous te bénissons, nous t’adorons, nous te glorifions,
Nous te rendons grâce, pour ton immense gloire,
Seigneur Dieu, Roi du ciel, Dieu le Père tout-puissant,

Gloire à Dieu, gloire à Dieu au plus haut des cieux
Gloire à Dieu, gloire à Dieu au plus haut des cieux (bis)

Seigneur Fils unique, Jésus-Christ,
Seigneur Dieu, Agneau de Dieu, le Fils du Père,
Toi qui enlèves les péchés du monde, prends pitié de nous;

Gloire à Dieu, gloire à Dieu au plus haut des cieux
Gloire à Dieu, gloire à Dieu au plus haut des cieux (bis)

Toi qui enlèves les péchés du monde, reçois notre prière;
Toi qui es assis à la droite du Père, prends pitié de nous.

Gloire à Dieu, gloire à Dieu au plus haut des cieux
Gloire à Dieu, gloire à Dieu au plus haut des cieux (bis)

Car toi seul es Saint,
toi seul es Seigneur, toi seul es le Très-Haut: Jésus-Christ,
Avec le Saint-Esprit dans la gloire de Dieu le Père.
Amen.

Gloire à Dieu, gloire à Dieu au plus haut des cieux
Gloire à Dieu, gloire à Dieu au plus haut des cieux (bis)