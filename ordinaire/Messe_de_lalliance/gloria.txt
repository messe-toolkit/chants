R.
Gloire à Dieu notre Père,
Gloire à Dieu par l'Esprit,
Joie du ciel sur la terre
Paix du Christ dans nos vies.

1.
Créateur du Monde,
Jeunesse des vivants (bis)
Tu nous as faits à ton image
Louange à Toi dans l'univers (bis)
Tes merveilles proclament ton nom (bis)

2.
Dieu Sauveur du monde
Lumière des vivants ! (bis)
Tu nous relèves au jour de Pâques
Louange à Toi, Ressuscité ! (bis)
Fils de l'homme, avec toi nous chantons (bis)

3.
Souffle sur le monde,
Sagesse des vivants ! (bis)
Tu nous choisis pour ta demeure,
Louange à Toi qui nous conduis ! (bis)
D'un seul cœur avec toi nous chantons (bis)