Christ et Sauveur, mort sur la croix,
Dieu plus puissant que nos tombeaux ! (bis)

Gloire à toi, le soleil du jour de Pâques,
Fils de Dieu, tu viendras dans la gloire !