Gloire à Dieu, au plus haut des cieux,
Et paix sur la terre aux hommes qu’il aime.

Nous te louons, nous te bénissons,
Nous t’adorons, nous te glorifions,
Nous te rendons grâce,
Pour ton immense gloire,
Seigneur Dieu, Roi du ciel,
Dieu le Père Tout-puissant !

Seigneur, Fils unique, Jésus-Christ,
Seigneur Dieu, Agneau de Dieu,
Le Fils du Père,
Toi qui enlèves le péché du monde,
Prends pitié de nous ;

Toi qui enlèves le péché du monde,
Reçois notre prière ;
Toi qui es assis à la droite
Du Père, prends pitié de nous !

Car toi seul es saint,
Toi seul es Seigneur,
Toi seul es le Très-Haut, Jésus Christ,
Avec le Saint-Esprit,
Dans la gloire de Dieu le Père.
Amen. Amen. Amen.
Amen. Amen. Amen.