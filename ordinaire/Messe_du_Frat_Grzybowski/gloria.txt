R.
Gloire à Dieu au plus haut des cieux,
Paix aux Hommes qu'Il aime
Gloire à Dieu au plus haut des cieux,
Gloire, gloire à Dieu !

1.
Nous te louons, nous te bénissons;
Nous t’adorons, nous te glorifions,
Nous te rendons grâce,
Pour ton immense gloire,
Gloire, gloire à Dieu!

2.
Seigneur Dieu, Roi du ciel,
Dieu le Père tout-puissant
Seigneur Fils unique, Jésus Christ
Gloire, gloire a toi!

3.
Seigneur Dieu, agneau de Dieu
Seigneur, le fils du Père
Toi qui enlèves le péché du monde,
Prends pitié de nous.

4.
Toi qui enlèves le péché du monde
Reçois notre prière, prends pitié
Toi qui es assis a la droite du Père
Prends pitié de nous.

5.
Car toi seul es saint, toi seul es Seigneur,
Toi seul es le Très Haut, Jésus-Christ,
Avec le Saint-Esprit dans la gloire du Père.
Gloire, gloire à Dieu!
