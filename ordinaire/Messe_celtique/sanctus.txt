Saint ! Saint ! Saint, le Seigneur,
Dieu de l’univers ! (bis)

Le ciel et la terre
sont remplis de ta gloire.
\s
Hosanna au plus haut des cieux. (bis)

Béni soit celui qui vient
au nom du Seigneur.
\s
Hosanna au plus haut des cieux. (bis)