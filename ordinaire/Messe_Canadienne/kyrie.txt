Seigneur, prends pitié,
Seigneur, prends pitié.
Nous avons manqué, d'amour.
Seigneur, prends pitié,
Seigneur, prends pitié.

Ô Christ, prends pitié,
Ô Christ, prends pitié.
Nous avons manqué, d'espoir.
Ô Christ, prends pitié,
Ô Christ, prends pitié.

Seigneur, prends pitié,
Seigneur, prends pitié.
Nous avons manqué, de foi.
Seigneur, prends pitié,
Seigneur, prends pitié.