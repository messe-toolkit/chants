R.
Gloire à Dieu au plus haut des cieux,
et paix sur la terre
aux hommes qu’il aime (bis)

1.
Nous te louons, nous te bénissons,
nous t’adorons, nous te glorifions,
Nous te rendons grâce pour ton immense gloire

2.
Seigneur Dieu, roi du ciel,
Dieu le Père tout puissant,
Seigneur fils unique, Jésus Christ,
Seigneur Dieu, agneau de Dieu,
le fils du Père;

3.
Toi qui enlèves les péchés du monde,
prends pitié de nous.
Toi qui enlèves les péchés du monde,
reçois notre prière;
Toi qui est assis à la droite du Père,
prends pitié de nous.

4.
Car toi seul est saint, toi seul est Seigneur
Toi seul est le Très Haut, Jésus Christ,
Avec le Saint Esprit,
dans la gloire de Dieu le Père.
Amen, Amen, Amen, Amen !
