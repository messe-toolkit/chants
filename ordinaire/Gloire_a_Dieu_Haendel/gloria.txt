R.
Gloire à Dieu au plus haut des cieux,
paix sur la terre aux hommes qu'il aime.
Gloire à Dieu au plus haut des cieux,
paix sur la terre, joie de l'univers !

1.
Pour tes merveilles, Seigneur Dieu,
ton peuple te rend grâce !
Ami des hommes, sois béni
pour ton règne qui vient !
À toi, les chants de Fête,
par ton Fils Bien-Aimé, dans l'Esprit.

2.
Sauveur du monde, Jésus Christ
écoute nos prières !
Agneau de Dieu, vainqueur du mal
sauve nous du péché !
Dieu saint, splendeur du Père, Dieu vivant
le Très-Haut, le Seigneur.