Gloria, gloria,in excelsis Deo.
Gloria, gloria,in excelsis Deo.

1.
Et paix sur la terre aux hommes qu’il aime.
Nous te louons, nous te bénissons, nous t’adorons,
Nous te glorifions, nous te rendons grâce,
Pour ton immense gloire,
Seigneur Dieu, Roi du ciel, Dieu le Père tout-puissant.
Seigneur, Fils unique, Jésus Christ,
Seigneur Dieu, Agneau de Dieu, le Fils du Père ;

2.
Toi qui enlèves les péchés du monde, prends pitié de nous ;
Toi qui enlèves les péchés du monde, reçois notre prière ;
Toi qui es assis à la droite du Père, prends pitié de nous.

3.
Car toi seul es saint, toi seul es Seigneur,
Toi seul es le Très-Haut : Jésus Christ,
avec le Saint-Esprit
Dans la gloire de Dieu le Père. Amen.