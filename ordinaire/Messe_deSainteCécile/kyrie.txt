1.
Seigneur Jésus, envoyé par le Père,
pour guérir et sauver les hommes,
prends pitié de nous.
Kyrie, Kyrie, Eleison.

2.
Ô Christ, venu dans le monde,
appeler tous les pécheurs,
prends pitié de nous.
Christe, Christe, Eleison.

3.
Seigneur, élevé dans la gloire du Père,
où tu intercèdes pour nous,
prends pitié de nous.
Kyrie, Kyrie, Eleison.