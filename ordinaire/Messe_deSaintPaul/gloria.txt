R.
Gloria in excelsis Deo,
Gloria Deo Domino (bis)

1.
Gloire à Dieu au plus haut des cieux
Et paix sur la terre aux hommes qu’il aime.
Nous te louons, nous te bénissons,
Nous t’adorons, nous te glorifions,
\n
Nous te rendons grâce,
Pour ton immense gloire,
Seigneur Dieu, Roi du ciel,
Dieu le Père tout-puissant.

2.
Seigneur, Fils unique, Jésus Christ,
Seigneur Dieu, Agneau de Dieu,
le Fils du Père;
\n
Toi qui enlèves les péchés du monde,
prends pitié de nous ;
Toi qui enlèves les péchés du monde,
reçois notre prière ;
Toi qui es assis à la droite du Père,
prends pitié de nous.

3.
Car toi seul es saint, toi seul es Seigneur,
Toi seul es le Très-Haut :
Jésus Christ, Avec le Saint-Esprit
Dans la gloire de Dieu le Père.